Snap packaging
==============


Creating the snap
-----------------

This folder contains a part of the assets needed to package MRF in a
snap package. You need to provide yourself Optix 6.5.

- `optix/NVIDIA-OptiX-SDK-6.5.0-linux64.sh`

This file cannot be provided in the repo since it needs to be accessed
through a NVIDIA developer account (free to create).

Then, to build the snap, you need to execute the following command:

```bash
snapcraft
```

This produces a `.snap` file (`malia_1.0.0_amd64.snap`) which you can
install on your system with:

```bash
snap install malia_1.0.0_amd64.snap --dangerous
```


Store page
----------

Upload to Snapcraft is done with:

```bash
snapcraft upload malia_1.0.0_amd64.snap
```

The Snapcraft page is currently unlisted but publicly accessible
through http://snapcraft.io/malia


Executable
----------

The snap provides the following executable:

- `malia.renderer` referring to the spectral renderer
- `malia.renderer-rgb` referring to the RGB renderer
- `malia.mic`
- `malia.rgb2spec`
- `malia.mdiff`
