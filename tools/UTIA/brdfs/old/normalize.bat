@echo off

REM start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_9_direct.exr"
REM start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_9_ndotl.exr"

REM start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_9_robust_direct.exr"
REM start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_9_robust_ndotl.exr"

start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\1_4_direct.exr"
start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\1_4_ndotl.exr"

start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\1_5_direct.exr"
start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\1_5_ndotl.exr"

start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\1_6_direct.exr"
start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\1_6_ndotl.exr"

start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\5_6_direct.exr"
start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\5_6_ndotl.exr"

start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_4_direct.exr"
start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_4_ndotl.exr"

start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_5_direct.exr"
start /wait brdf_slice_normalization.exe "C:\Dev\DATA\brdfs\7_5_ndotl.exr"
