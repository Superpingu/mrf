@echo off

for %%a in ("C:\Dev\Musaq\*.msf") do (
echo Processing %%a
start /wait malia_rgb.exe -samples 2000 -logging 3 -scene %%a
echo Finished renders for %%a
if not exist "C:\Dev\Musaq\video_%%~na\" mkdir C:\Dev\Musaq\video_%%~na
if not exist "C:\Dev\Musaq\video_%%~na\frames_exr" mkdir C:\Dev\Musaq\video_%%~na\frames_exr
move /Y "C:\Dev\Musaq\*%%~na*.exr" "C:\Dev\Musaq\video_%%~na\frames_exr\"
echo Moved frames to C:\Dev\Musaq\video_%%~na\frames_exr\

if not exist "C:\Dev\Musaq\video_%%~na\frames_png" mkdir C:\Dev\Musaq\video_%%~na\frames_png
for %%b in ("C:\Dev\Musaq\video_%%~na\frames_exr\*") do start /wait SpectralViewer.exe %%b -o "C:\Dev\Musaq\video_%%~na\frames_png\%%~nb.png"
echo Converted frames in C:\Dev\Musaq\video_%%~na\frames_png\

python "C:\Dev\MRF\tools\UTIA\video\make_video.py" --input "C:\Dev\Musaq\video_%%~na\frames_png" --output "%%~na"
echo Created video %%~na
)

pause
