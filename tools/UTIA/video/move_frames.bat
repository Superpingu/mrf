@echo off

move /Y ".\..\*_envPhi*.exr" ".\frames_exr"
move /Y ".\*.exr" ".\frames_exr"
move /Y ".\*.png" ".\frames_png"
move /Y ".\frames_exr\*.png" ".\frames_png"

pause
