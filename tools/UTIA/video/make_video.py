import cv2
import numpy as np
import os,glob

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--input", help="Image input folder")
parser.add_argument("--output", help="Video output name")
args = parser.parse_args()


file_path = args.input
png_path = os.path.join(file_path, '*.png')
img_array = []
size = (0,0)
for filename in glob.glob(png_path):
    img = cv2.imread(filename)
    height, width, layers = img.shape
    size = (width,height)
    img_array.append(img)


os.chdir(args.input + '/..')
video_name = args.output + '.avi'
out = cv2.VideoWriter(video_name,cv2.VideoWriter_fourcc(*'DIVX'), 24, size)
 
for i in range(len(img_array)):
    out.write(img_array[i])
out.release()