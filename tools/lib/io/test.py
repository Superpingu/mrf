

import np2envi as envi
import numpy as np

def main():
    A =  np.zeros((100,20,50)) #black image, 100 wl, heigth = 20, width = 50
    A[50,10,:] = 1
    envi.export_array(A,"test",range(600,700),data_type = 5) # create envi image

if __name__ =="__main__" :
    main()

