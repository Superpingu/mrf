import os,sys
import bpy
import subprocess
import time
import platform
import importlib
import xml.etree.ElementTree as ET



blend_file_path = bpy.data.filepath
directory = os.path.dirname(blend_file_path)
blender_file_name = os.path.basename(bpy.data.filepath)
blender_file_name = os.path.splitext(blender_file_name)[0]



#MODIFY ONLY THIS 6 VARIABLES TO CUSTOMIZE SCRIPT
#by default uses the blender filename
mrf_filename = os.path.join(directory,"./"+blender_file_name+".msf")
camera_filename = os.path.join(directory,"./"+blender_file_name+".mcf")
obj_sub_directory_name = blender_file_name+"_assets"
nb_samples = "10"
wavelength = "380:780:50"
spectral_rendering = False
renderer = "OPTIX"
#renderer = "IRAY"
interactive_renderer = True

# CHECK THAT MANDATORY ENV VARIABLES are set
if os.environ.get("MRF_DIR") is None:
    print("[ERROR] THE MRF environment variable is not set. FIX THAT!!!")
    exit(-1)
    
MRF_DIR = os.environ.get('MRF_DIR')

dir_python = MRF_DIR+"/tools"

#add mrf/tools in sys path for submodules
if not dir_python in sys.path:
  sys.path.append(dir_python)

bpy.utils.load_scripts(True)

if bpy.app.version < (2,80,0):
	from lib import libblenderlegacy as libblender2msf
else:
	from lib import libblender2_8 as libblender2msf
importlib.reload(libblender2msf)

libblender2msf.export(mrf_filename,camera_filename,obj_sub_directory_name,False)



####render using os.system
#renderer = MRF_DIR+"/bin/IRAY_RENDERER_SPECTRAL.exe "
#samples = " -samples "+ nb_samples+" "
#scene = "-scene " + '"' + mrf_filename + '"'
#wavelength = " -wr " + wavelength

#cmd = renderer + scene + samples + wavelength
#os.system(cmd)



if renderer =="OPTIX":
  if spectral_rendering:
    renderer_exec = MRF_DIR+"/bin/OPTIX_RENDERER_SPECTRAL"
  else:
    renderer_exec = MRF_DIR+"/bin/OPTIX_RENDERER"
else:
  interactive_renderer = False
  ####render using subprocess.Popen
  if spectral_rendering:
    renderer_exec = MRF_DIR+"/apps/iray_renderer/IRAY_RENDERER_SPECTRAL"
  else:
    renderer_exec = MRF_DIR+"/apps/iray_renderer/IRAY_RENDERER"
    

if platform.system()=="Windows":
  renderer_exec = renderer_exec + ".exe"


renderer_args = []

renderer_args.append(renderer_exec)
renderer_args.append("-scene")
renderer_args.append(mrf_filename)
renderer_args.append("-samples")
renderer_args.append(nb_samples)

if spectral_rendering:
  renderer_args.append("-wr")
  renderer_args.append(wavelength)

if renderer =="OPTIX" and interactive_renderer:
  renderer_args.append("-i")

process_renderer = subprocess.Popen(renderer_args)



if not interactive_renderer:

  while process_renderer.poll() is None:
    #print('Still rendering')
    time.sleep(5)
  print("END RENDER Exited with returncode %d" % process_renderer.returncode)


  ####Open viewer


  #nb_cameras = libblender2msf.count_cameras()
  camera_names = libblender2msf.cameraNamesFromMCF( mrf_filename[:-4]+".mcf" ) 
  nb_cameras = len(camera_names)

  for camera_name in camera_names:
    image_file = os.path.basename(mrf_filename)
    image_file = os.path.splitext(image_file)[0]


    if(nb_cameras>1):
      image_file = camera_name + "_" + image_file +"_"+ nb_samples +"spp.exr"
    else:
      image_file = image_file +"_"+ nb_samples +"spp.exr"

    if spectral_rendering:
      image_file = image_file[:-4] +".hdr"
      

    image_file = os.path.join(directory,image_file) 


    if os.path.isfile(image_file):
      if platform.system()=="Windows":
        viewer = "SpectralViewer"
        process_viewer = subprocess.Popen((viewer,image_file))
      else:
        viewer = "SpectralViewer"
        #viewer = MRF_DIR+"/apps/SpectralViewer"
        process_viewer = subprocess.Popen((viewer,image_file))
    else:
      print("Cannot open image file:"+image_file)



  # while process.poll() is None:
  #   print('Still sleeping')
  #   time.sleep(1)

  # print('Not sleeping any longer.  Exited with returncode %d' % process.returncode)
