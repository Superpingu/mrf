import os
import yaml
import sys

import yaml_to_spd


def convertFolder(input_folder_name,recurse):

	file_list = os.listdir(input_folder_name)

	#print(file_list)
	for file in file_list:

		#print(file)

		file_name, file_extension = os.path.splitext(file)

		#print(file_name)
		#print(file_extension)

		input_file = input_folder_name + "/" + file

		if(os.path.isdir(input_file) and recurse):
			print("Going into "+input_file+"\n")
			convertFolder(input_file,recurse)
		else:
			if file_extension==".yml" or file_extension==".yaml":
				print("Converting " + input_file)
				yaml_to_spd.yamlToSpd(input_file,False)
				print("")



input_folder_name = sys.argv[1]
convertFolder(input_folder_name,True)