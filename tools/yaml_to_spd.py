import os
import yaml
import write_spd
import sys
import math

#MRF_DIR = os.environ.get('MRF_DIR')
#print(MRF_DIR)
#input_file_name = MRF_DIR + "/data/ior/main/Ag/Johnson.yml"

def parseTabulatedNK(values,spectrum_eta,spectrum_kappa):
  if 'data' not in values.keys():
    return
    
  data = values['data']
  data_lines = data.split("\n")

  for line in data_lines:
    #print(line)
    line_splitted = line.split(" ") 
    if(len(line_splitted[0])):
      #print(float(line_splitted[0]))
      #print(line_splitted[1])
      #print(line_splitted[2])
      wavelength = float(line_splitted[0]) * 1000
      spectrum_eta.append( [wavelength, float(line_splitted[1])] )
      spectrum_kappa.append( [wavelength, float(line_splitted[2])] )

def parseTabulatedNorK(values,spectrum):
  if 'data' not in values.keys():
    return
    
  data = values['data']
  data_lines = data.split("\n")

  for line in data_lines:
    #print(line)
    line_splitted = line.split(" ") 
    if(len(line_splitted[0])):
      #print(float(line_splitted[0]))
      #print(line_splitted[1])
      #print(line_splitted[2])
      wavelength = float(line_splitted[0]) * 1000
      spectrum.append( [wavelength, float(line_splitted[1])] )


def parseFormula1(values,spectrum_eta):
  wave_start = 0
  wave_end = 0
  if 'range' not in values.keys():
    return
  if 'coefficients' not in values.keys():
    return 

  if len(values['range'])>1:

    line_splitted = values['range'].split(" ")
    if len(line_splitted)>=2:

      wave_start = int(float(line_splitted[0]) *1000)
      wave_end = int(float(line_splitted[1]) *1000)
      print(wave_start)
      print(wave_end)

  coeff = []
  #for i in range(0,6):
  #  coeff.append( float(1.0) )

  line_splitted = values['coefficients'].split(" ")
  if len(line_splitted)>1:
    for i in range(0,len(line_splitted)):
      if float(line_splitted[i]) > 0:
        coeff.append( float(line_splitted[i]) )
  else:
    return

  for wavelength in range(wave_start, wave_end+1):
    x = float(wavelength)/float(1000.0)
    
    eta = coeff[0]
    for i in range(1,len(coeff),2):
      eta = eta + coeff[i]/(1-coeff[i+1]/x**2)

    eta = math.sqrt(1+eta)

    #eta = (1+coeff[0]/(1-coeff[1]/x**2)+coeff[2]/(1-coeff[3]/x**2)+coeff[4]/(1-coeff[5]/x**2))**.5
    spectrum_eta.append( [wavelength, eta] )


def parseFormula2(values,spectrum_eta):
  wave_start = 0
  wave_end = 0
  if 'range' not in values.keys():
    return
  if 'coefficients' not in values.keys():
    return 

  if len(values['range'])>1:

    line_splitted = values['range'].split(" ")
    if len(line_splitted)>=2:

      wave_start = int(float(line_splitted[0]) *1000)
      wave_end = int(float(line_splitted[1]) *1000)
      print(wave_start)
      print(wave_end)

  coeff = []
  #for i in range(0,6):
  #  coeff.append( float(1.0) )

  line_splitted = values['coefficients'].split(" ")
  if len(line_splitted)>1:
    for i in range(0,len(line_splitted)):
      if float(line_splitted[i]) > 0:
        coeff.append( float(line_splitted[i]) )
  else:
    return

  for wavelength in range(wave_start, wave_end+1):
    x = float(wavelength)/float(1000.0)
    
    eta = 0
    for i in range(0,len(coeff),2):
      eta = eta + coeff[i]/(1-coeff[i+1]/x**2)

    eta = math.sqrt(1+eta)

    #eta = (1+coeff[0]/(1-coeff[1]/x**2)+coeff[2]/(1-coeff[3]/x**2)+coeff[4]/(1-coeff[5]/x**2))**.5
    spectrum_eta.append( [wavelength, eta] )


def yamlToSpd(inputFileName,verbose):

  outputFileName, file_extension = os.path.splitext(inputFileName)
  outputFileName_eta = outputFileName + "_eta.spd"
  outputFileName_kappa = outputFileName + "_kappa.spd"

  spectrum_eta = []
  spectrum_kappa = []

  parse_kappa = False

  with open(inputFileName, "r") as stream:
    try :
      docs = yaml.load_all(stream)
      for doc in docs:
        for k,v in doc.items():
          if k.startswith("DATA"):
            #print( k, "->", v)
            #temp = v[0].split(',')
            # print(v)
            # print(v[0])
            # print(v[0]['data'])
            # print(type(v[0]['data']))

            for values in v:
              if 'type' in values.keys():
                data_type = values['type'].rstrip()


                print(data_type)
  
                if data_type == "tabulated nk":
                  parseTabulatedNK(values,spectrum_eta,spectrum_kappa)
                  
                elif data_type == "tabulated n":
                  parseTabulatedNorK(values,spectrum_eta)

                elif data_type == "tabulated k":
                  parseTabulatedNorK(values,spectrum_kappa)

                elif data_type == "formula 1":
                  parseFormula1(values,spectrum_eta)

                elif data_type == "formula 2":
                  parseFormula2(values,spectrum_eta)
                  

                else:
                  #print("Wrong format")
                  continue
              else:
                continue
  
    except:
      print("exception yaml")

  if(len(spectrum_eta)):
    write_spd.writeSpd(outputFileName_eta, spectrum_eta)
    print("Wrote "+str(len(spectrum_eta))+" eta values")

  if(len(spectrum_kappa)):
    write_spd.writeSpd(outputFileName_kappa, spectrum_kappa)
    print("Wrote "+str(len(spectrum_kappa))+" kappa values")

  if verbose:
    print(spectrum_eta)
    print(spectrum_kappa)




#inputFileName = sys.argv[1]
#yamlToSpd(inputFileName)