import os,sys
import bpy
import subprocess
import time
import platform
import importlib



blend_file_path = bpy.data.filepath
directory = os.path.dirname(blend_file_path)
blender_file_name = os.path.basename(bpy.data.filepath)
blender_file_name = os.path.splitext(blender_file_name)[0]



#MODIFY ONLY THIS THREE VARIABLES TO CUSTOMIZE SCRIPT
#by default uses the blender filename
mrf_filename = os.path.join(directory,"./"+blender_file_name+".msf")
camera_filename = os.path.join(directory,"./"+blender_file_name+".mcf")
obj_sub_directory_name = blender_file_name+"_assets"


# CHECK THAT MANDATORY ENV VARIABLES are set
if os.environ.get("MRF_DIR") is None:
    print("[ERROR] THE MRF environment variable is not set. FIX THAT!!!")
    exit(-1)
    
MRF_DIR = os.environ.get('MRF_DIR')

dir_python = MRF_DIR+"/tools"

#add mrf/tools in sys path for submodules
if not dir_python in sys.path:
  sys.path.append(dir_python )

bpy.utils.load_scripts(True)

if bpy.app.version < (2,80,0):
	from lib import libblenderlegacy as libblender2msf
else:
	from lib import libblender2_8 as libblender2msf
importlib.reload(libblender2msf)

libblender2msf.export(mrf_filename,camera_filename,obj_sub_directory_name,False)

