#!/bin/bash
#SBATCH -J TEST
#SBATCH -p long_sirocco
#SBATCH --exclusive
#SBATCH -w sirocco09
#SBATCH -o log_ggx%j.out
#SBATCH -e log_ggx%j.err
#SBATCH -N 1
#SBATCH --time=0-24:00:00
module purge
module load slurm/17.11
module load language/python/3.6.5
export IRAY_ROOT=/home/adufay/iray
export LD_LIBRARY_PATH=/cm/shared/apps/gcc/7.3.0/lib64:/home/adufay/iray/linux-x86-64/lib
export MRF_DIR=/home/adufay
echo "IRAY_ROOT = " $IRAY_ROOT
echo "LD = " $LD_LIBRARY_PATH
echo "MRF_DIR = " $MRF_DIR

python render_ggx_folder.py
