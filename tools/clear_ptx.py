#!python3

#########################################################################################
#
# Author:  David Murray       Copyright CNRS  : 2021
#
#########################################################################################

######################NON-REGRESSION RENDERING TESTS#####################################
#
#
#########################################################################################

import os
import argparse
import glob

parser = argparse.ArgumentParser()
parser.add_argument("--app_path", help="Path to the malia executable", default="/bin/")
args = parser.parse_args()

ptx_list = glob.glob(args.app_path + "/precompiled_ptx/**/*.ptx", recursive=True)

for ptx in ptx_list:
	try:
		os.remove(ptx)
		print("Deleted " + ptx)
	except:
		print("Error while deleting file : ", ptx)