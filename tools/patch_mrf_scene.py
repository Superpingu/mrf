import os,sys
import time
import platform
import importlib
import xml.etree.ElementTree as ET


# CHECK THAT MANDATORY ENV VARIABLES are set
if os.environ.get("MRF_DIR") is None:
    print("[ERROR] THE MRF environment variable is not set. FIX THAT!!!")
    exit(-1)
    
MRF_DIR = os.environ.get('MRF_DIR')

dir_python = MRF_DIR+"/tools"

#add mrf/tools in sys path for submodules
if not dir_python in sys.path:
  sys.path.append(dir_python)

dir_python = MRF_DIR+"/tools/lib"

#add mrf/tools in sys path for submodules
if not dir_python in sys.path:
  sys.path.append(dir_python)

from libpatchmrf import patch_mrf_scene_after_export
from libmrfxml import indent_xml

def patch_mrf_scene(mrf_filename):
	tree = ET.parse(mrf_filename)
	root = tree.getroot()

	patch_mrf_scene_after_export(root, mrf_filename[:-4]+".mpf")

	#indent and save scene
	indent_xml(root)
	tree.write(mrf_filename)

patch_mrf_scene(sys.argv[1])