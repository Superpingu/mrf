import csv

with open('D65_interpolated.csv', 'w') as csvfileOut:
  writer = csv.writer(csvfileOut, delimiter=',', lineterminator='\n')
  
  with open('D65.csv', 'r') as csvfileIn:
    reader = csv.reader(csvfileIn, delimiter=',')

    numRow = 0
    lastWavelength = 0
    lastValue = 0

    csv_data = list(reader)
    print(csv_data[5])
    print(len(csv_data))

    for i in range(0,5*(len(csv_data)-1)+1):
      outputRow = []
      outputWavelength = int(csv_data[0][0])+i
      outputRow.append(outputWavelength)

      if(i%5==0):
      	value = float(csv_data[int(i/5)][1])
      else:
      	lastValue = float(csv_data[int(i/5)][1])
      	value = float(csv_data[int(i/5)+1][1])
      	value = lastValue + (value - lastValue)/5.0*float(i) 
      
      outputRow.append(value)
      writer.writerow(outputRow)
