###########################################MERGE ENVI ##################################
#
#
#  Merge two ENVI files
#  
#
#  Script input parameters are: path_to_first_header path_to_second_header path_to_output
#
#  
#  The script merges the files wavelengths data, not the pixel data.
#  For instance two files with 500spp and with the wavelengths
#  (500,510,520) and (530,540,550) will output an ENVI file
#  at 500spp with wavelengths (500,510,520,530,540,550).
#
#  The ENVI files should have the same pixel resolution.
#    
#  The number of wavelengths sampled in each file can vary.
#
#  The ENVI files should not have any wavelength overlap
#  for instance merging two files with the wavelengths
#  (500,550,600) and (510,520,530) will NOT work.
#
#
#########################################################################################

import sys
import os

import envi_parsing as ev



def merge_envi(path_1,path_2,path_3):
  header_1 = ev.parse_envi_header(path_1)
  header_2 = ev.parse_envi_header(path_2)

  header = header_1 + header_2
  header.path = path_3

  if header is not None:
    print(header.waves)

    header.write_file()
    merge_data(header_1, header_2, header)

  else:
    print("ERROR CAN'T MERGE HEADERS")


def merge_data(header_1,header_2,header_3):
  path_1 = os.path.splitext(header_1.path)[0] + ".raw"
  path_2 = os.path.splitext(header_2.path)[0] + ".raw"
  path_3 = os.path.splitext(header_3.path)[0] + ".raw"


  f1 = open(path_1,"rb")
  f1_contents = f1.read()
  f1.close()

  f2 = open(path_2,"rb")
  f2_contents = f2.read()
  f2.close()

  f3 = open(path_3, "wb") # open in `w` mode to write
  f3.write(f1_contents + f2_contents) # concatenate the contents
  f3.close()

  # f1 = open(path_1,"rb")
  # f2 = open(path_2,"rb")
  # f3 = open(path_3, "wb")

  # data_size = 0

  # if header.data_type == 1:
  #   data_size = 1
  # elif header.data_type == 4:
  #   data_size = 4
  # elif header.data_type == 5:
  #   data_size = 8
  # elif header.data_type == 12:
  #   data_size = 2
  # elif header.data_type == 13:
  #   data_size = 4
  # elif header.data_type == 15:
  #   data_size = 8

  # data_size = header.width * head


#  if (std::is_same<T,unsigned char>::value)
#    header_file << "data type = 1" << std::endl;
#  else if (std::is_same<T, float>::value)
#    header_file << "data type = 4" << std::endl;
#  else if (std::is_same<T, double>::value)
#    header_file << "data type = 5" << std::endl;
#  else if (std::is_same<T, unsigned short int>::value)
#    header_file << "data type = 12" << std::endl;
#  else if (std::is_same<T, unsigned int>::value)
#    header_file << "data type = 13" << std::endl;
#  else if (std::is_same<T, unsigned long long>::value)
#    header_file << "data type = 15" << std::endl;

  # i = 0
  # j = 0
  # while i < len(header_1.waves) and j < len(header_2.waves):
  #   w1 = header_1.waves[i]
  #   w2 = header_2.waves[j]
  #   if w1 <= w2:
  #     data_buffer = f1.read()



  
  # f3.write(f1_contents + f2_contents) # concatenate the contents
  
  # f1.close()
  # f2.close()
  # f3.close()


def help():
  print("Usage: "+sys.argv[0]+" file_1.hdr file_2.hdr file_out.hdr")


if(len(sys.argv)!=4):
  help()
else:
  merge_envi(sys.argv[1],sys.argv[2],sys.argv[3])

