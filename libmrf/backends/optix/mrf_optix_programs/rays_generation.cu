/*
 * Copyright (c) 2018 NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <optixu/optixu_math_namespace.h>
#include "include/random.h"

#include "include/path_tracer_definitions.h"

#include "include/cameras.h"

using namespace optix;
using mrf::spectrum::COLOR;
using mrf::spectrum::COLOR_B;

// Scene wide variables
rtDeclareVariable(float, scene_epsilon, , );
rtDeclareVariable(rtObject, top_object, , );
rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );

rtDeclareVariable(PerRayData_pathtrace, current_prd, rtPayload, );

// In non multiplexed mode, the random lambda offset within a bin is defined
// on the CPU. We need to keep track of it for accumulating computed radiance
// in the framebuffer.
#ifdef MRF_RENDERING_MODE_SPECTRAL
rtDeclareVariable(float, wavelength_offset, , );
#endif

rtDeclareVariable(unsigned int, frame_number, , );
rtDeclareVariable(unsigned int, per_pixel_frame_number, , );
rtDeclareVariable(unsigned int, num_samples_per_frame, , );
rtDeclareVariable(unsigned int, rr_begin_depth, , );
rtDeclareVariable(unsigned int, max_path_length, , );
rtDeclareVariable(unsigned int, pathtrace_ray_type, , );
rtDeclareVariable(unsigned int, pathtrace_shadow_ray_type, , );

rtDeclareVariable(unsigned int, _spatial_factor, , );
rtDeclareVariable(unsigned int, rng_seed, , );

#ifdef MRF_RENDERING_MODE_SPECTRAL
// Spectral mode
rtBuffer<float, 3> output_buffer;
rtBuffer<float, 3> variance_buffer;
rtDeclareVariable(unsigned int, first_wave_index, , );
rtDeclareVariable(unsigned int, last_wave_index, , );
rtDeclareVariable(unsigned int, nb_wavelenths, , );
#else
// RGB Mode
rtBuffer<float3, 3> output_buffer;
rtBuffer<float3, 3> variance_buffer;
#endif   // MRF_RENDERING_MODE_SPECTRAL


optix::uint2 effective_index(optix::uint2 index, optix::uint frame_number, optix::uint factor)
{
  if (factor == 1) return index;
  //Returns a value between 0 and factor^2 - 1 to access the offset buffer.
  optix::uint offset = (frame_number - 1) % (factor * factor);

  optix::uint2 pixel_offset;
  pixel_offset.x = offset % factor;
  pixel_offset.y = offset / factor;

  //Remap to buffer size.
  optix::uint2 full_index = index * factor;

  return full_index + pixel_offset;
}

// ---------------------------------------------------------------------------
// Accumulate value to rendering buffers
// ---------------------------------------------------------------------------

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

// Spectral + multiplexed

void accumulateToBuffers(
    COLOR        pixel_color,
    optix::uint2 index,
    optix::uint  per_pixel_frame_number,
    optix::uint  factor,
    float        offset)
{
  uint3      pixel_idx    = make_uint3(index, 0);
  const bool log_variance = variance_buffer.size().x * variance_buffer.size().y > 1;

  // Nearest
  // for (pixel_idx.z = first_wave_index; pixel_idx.z <= last_wave_index; pixel_idx.z++) {
  // const float pixel_color_wl = pixel_color[pixel_idx.z - first_wave_index];

  // output_buffer[pixel_idx] += pixel_color_wl;
  // if (log_variance) variance_buffer[pixel_idx] += pixel_color_wl * pixel_color_wl;
  // }

  // Filtered (linear)
  const float weight_low  = 1.0 - abs(0.5 + offset);
  const float weight_mid  = 1.0 - abs(0.5 - offset);
  const float weight_high = 1.0 - abs(1.5 - offset);

  for (pixel_idx.z = first_wave_index; pixel_idx.z <= last_wave_index; pixel_idx.z++)
  {
    const uint  local_idx      = pixel_idx.z - first_wave_index;
    const float pixel_color_wl = pixel_color[local_idx];

    // The sampled wavelengths are in the lower section of the bins
    if (offset < 0.5)
    {
      // In case we can accumulate also on the previous bin, we propagate
      // the energy with weigthing
      if (pixel_idx.z > 0)
      {
        const uint3 pixel_idx_low = make_uint3(index, pixel_idx.z - 1);
        output_buffer[pixel_idx_low] += weight_low * pixel_color_wl;
        output_buffer[pixel_idx] += weight_mid * pixel_color_wl;
      }
      // When there is no previous bin, we do not weight the pixel color
      else
      {
        output_buffer[pixel_idx] += pixel_color_wl;
      }
    }
    // The sampled wavelengths are in the higher section of the bins
    else if (offset > 0.5)
    {
      // In case we can accumulate also on the next bin, we propagate the
      // energy with weigthing
      if (pixel_idx.z < output_buffer.size().z - 1)
      {
        const uint3 pixel_idx_high = make_uint3(index, pixel_idx.z + 1);
        output_buffer[pixel_idx_high] += weight_high * pixel_color_wl;
        output_buffer[pixel_idx] += weight_mid * pixel_color_wl;
      }
      // Whe there is no next bin, we do not weight the pixel color
      else
      {
        output_buffer[pixel_idx] += pixel_color_wl;
      }
    }

    // The accumulation for the variance is exactly the same as for the output
    // buffer above.
    if (log_variance)
    {
      const float pixel_color_wl_sqr = pixel_color[local_idx] * pixel_color[local_idx];

      if (offset < 0.5)
      {
        if (pixel_idx.z > 0)
        {
          const uint3 pixel_idx_low = make_uint3(index, pixel_idx.z - 1);
          variance_buffer[pixel_idx_low] += weight_low * pixel_color_wl_sqr;
          variance_buffer[pixel_idx] += weight_mid * pixel_color_wl_sqr;
        }
        else
        {
          variance_buffer[pixel_idx] += pixel_color_wl_sqr;
        }
      }
      else if (offset > 0.5)
      {
        if (pixel_idx.z < output_buffer.size().z - 1)
        {
          const uint3 pixel_idx_high = make_uint3(index, pixel_idx.z + 1);
          variance_buffer[pixel_idx_high] += weight_high * pixel_color_wl_sqr;
          variance_buffer[pixel_idx] += weight_mid * pixel_color_wl_sqr;
        }
        else
        {
          variance_buffer[pixel_idx] += pixel_color_wl_sqr;
        }
      }
    }
  }
}

#else

// Spectral (monochromatic) or RGB

void accumulateToBuffers(COLOR pixel_color, optix::uint2 index, optix::uint per_pixel_frame_number, optix::uint factor)
{
  const bool  log_variance = variance_buffer.size().x * variance_buffer.size().y > 1;

#  ifdef MRF_RENDERING_MODE_SPECTRAL
  const uint3 pixel_idx    = make_uint3(index, first_wave_index);
  output_buffer[pixel_idx] += pixel_color;

  if (log_variance)
  {
    variance_buffer[pixel_idx] += pixel_color * pixel_color;
  }
#  else
  const uint3 pixel_idx = make_uint3(index, 0);
  const float a         = 1.0f / (float)per_pixel_frame_number;

  output_buffer[pixel_idx] = optix::lerp(output_buffer[pixel_idx], pixel_color.value, a);
  if (log_variance)
  {
    variance_buffer[pixel_idx] = optix::lerp(variance_buffer[pixel_idx], pixel_color.value * pixel_color.value, a);
  }
#  endif
}

#endif

// ---------------------------------------------------------------------------
// Clear rendering buffers at given xy index
// ---------------------------------------------------------------------------

void clearBuffers(optix::uint2 index)
{
  uint3      pixel_idx    = make_uint3(index, 0);
  const bool log_variance = variance_buffer.size().x * variance_buffer.size().y > 1;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  for (pixel_idx.z = 0; pixel_idx.z < output_buffer.size().z; pixel_idx.z++)
  {
    output_buffer[pixel_idx] = 0.;
    if (log_variance) variance_buffer[pixel_idx] = 0.;
  }
#else
  COLOR reset_color(0.);
  output_buffer[pixel_idx] = reset_color.value;
  if (log_variance) variance_buffer[pixel_idx] = reset_color.value;
#endif
}


// ---------------------------------------------------------------------------
//  Camera program -- main ray tracing loop
// ---------------------------------------------------------------------------

RT_PROGRAM void pathtrace_camera()
{
  size_t2 screen;
  screen.x = output_buffer.size().x;
  screen.y = output_buffer.size().y;

  float2 inv_screen       = 1.0f / make_float2(screen) * 2.f;
  uint2  pixel_index      = effective_index(launch_index, frame_number, _spatial_factor);
  float2 pixel            = (make_float2(pixel_index)) * inv_screen - 1.f;
  float  sqrt_num_samples = sqrtf(num_samples_per_frame);
  float2 jitter_scale     = inv_screen / sqrt_num_samples;

  //Sanity check to handle out of boundary when tiling
  if (pixel_index.x > screen.x - 1 || pixel_index.y > screen.y - 1) return;

  if (per_pixel_frame_number == 0)
  {
    clearBuffers(pixel_index);
    return;
  }

#ifndef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  COLOR result(0.f);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  unsigned int seed
      = tea<16>(screen.x * pixel_index.y + pixel_index.x, (first_wave_index + 1) * per_pixel_frame_number);
#else
  unsigned int seed = tea<16>(screen.x * pixel_index.y + pixel_index.x, per_pixel_frame_number);
#endif

  //sqrt_num_samples*sqrt_num_samples;
  for (unsigned int samples_per_pixel = num_samples_per_frame; samples_per_pixel > 0; --samples_per_pixel)
  {
    float3 ray_origin, ray_direction;

    ray_generation(pixel, jitter_scale, sqrt_num_samples, samples_per_pixel, seed, ray_origin, ray_direction);

    // Initialze per-ray data
    PerRayData_pathtrace prd;

    prd.result         = COLOR(0.f);
    prd.attenuation    = COLOR(1.f);
    prd.countEmitted   = true;
    prd.done           = false;
    prd.is_inside      = false;
    prd.shadow_catcher = 0;
    prd.seed           = seed;
    prd.depth          = 0;
    prd.id_wavelength  = -1;   // Used for multiplexing randomly choosing a wavelength for wavelength depending paths
    prd.ior            = COLOR_B(1.f);   // Air ior
    prd.num_sample = (per_pixel_frame_number - 1) * num_samples_per_frame + (num_samples_per_frame - samples_per_pixel);
    prd.offset_sampling = getOffsetSampling(screen.x * pixel_index.y + pixel_index.x);
    //prd.backface = false;
    //prd.pathCounter = 0;
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    prd.wavelength_offset = rnd(seed);   // Subsampling wavelengths
#endif

    // Each iteration is a segment of the ray path.  The closest hit will
    // return new segments to be traced here.
    for (;;)
    {
      Ray ray = make_Ray(ray_origin, ray_direction, pathtrace_ray_type, scene_epsilon, RT_DEFAULT_MAX);
      rtTrace(top_object, ray, prd);

      if (prd.done)   // || mrf::spectrum::avg(prd.attenuation) <= 0.0001f)
      {
        break;
      }

      //Russian roulette termination
      /* if(prd.depth >= rr_begin_depth)
       {
           float pcont = fmaxf(prd.attenuation);
           if(rnd(prd.seed) >= pcont)
               break;
           prd.attenuation /= pcont;
       }*/

      //WARNING recursivity bias
      if (prd.depth >= max_path_length)
      {
        break;
      }
      //if(prd.depth >= rr_begin_depth)
      //    break;


      //backface culling
      /*if(prd.depth==0 && prd.backface)
      if(prd.backface)
      {
          ray_origin = prd.origin + 0.01f*ray_direction;
          //ray_direction = prd.direction;
      }
      else*/
      {
        // Update ray data for the next path segment
        ray_origin    = prd.origin;
        ray_direction = prd.direction;
      }

      prd.depth++;
    }

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    // When multiplexing, we accumulate directly the ray energy to the output
    // buffer: we can filter the sample and use the offset used by this very
    // specific ray.
    mrf::spectrum::clamp_negative_color(prd.result);
    accumulateToBuffers(prd.result, pixel_index, per_pixel_frame_number, _spatial_factor, prd.wavelength_offset);
#else
    result += prd.result;
#endif
    seed = prd.seed;
  }

#ifndef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  // When not multiplexing, we accumulate the result of the multiple rays in
  // the output buffer.
  COLOR pixel_color = result / float(num_samples_per_frame);
  mrf::spectrum::clamp_negative_color(pixel_color);

  accumulateToBuffers(pixel_color, pixel_index, per_pixel_frame_number, _spatial_factor);
#endif
}

// ---------------------------------------------------------------------------
//  Shadow any-hit
// ---------------------------------------------------------------------------

rtDeclareVariable(PerRayData_pathtrace_shadow, current_prd_shadow, rtPayload, );

RT_PROGRAM void shadow()
{
  current_prd_shadow.inShadow = true;
  rtTerminateRay();
}
