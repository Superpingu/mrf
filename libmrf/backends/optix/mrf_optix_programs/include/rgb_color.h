namespace mrf
{
namespace spectrum
{
// typedef float3 COLOR;
struct COLOR
{
  float3 value;
  COLOR() {}
  COLOR(float f) { value = make_float3(f); }
  COLOR(float3 f) { value = f; }

  COLOR &operator=(float f);
  COLOR &operator=(COLOR const &color_2);

  COLOR operator+(float f);
  COLOR operator+(COLOR const &color_2);
  void  operator+=(float f);
  void  operator+=(COLOR const &color_2);

  COLOR operator-(float f);
  COLOR operator-(COLOR const &color_2);
  void  operator-=(float f);
  void  operator-=(COLOR const &color_2);

  COLOR operator*(float f);
  COLOR operator*(COLOR const &color_2);
  void  operator*=(float f);
  void  operator*=(COLOR const &color_2);

  COLOR operator/(float f);
  COLOR operator/(COLOR const &color_2);
  void  operator/=(float f);
  void  operator/=(COLOR const &color_2);

  float &operator[](int index);

  float avg();
};

__forceinline__ __device__ COLOR &COLOR::operator=(float f)
{
  value = make_float3(f);
  return *this;
}

__forceinline__ __device__ COLOR &COLOR::operator=(COLOR const &color_2)
{
  value = color_2.value;
  return *this;
}

__forceinline__ __device__ COLOR COLOR::operator+(float f)
{
  return COLOR(value + make_float3(f));
}

__forceinline__ __device__ COLOR COLOR::operator+(COLOR const &color_2)
{
  return COLOR(value + color_2.value);
}

__device__ void COLOR::operator+=(float f)
{
  value += make_float3(f);
}

__device__ void COLOR::operator+=(COLOR const &color_2)
{
  value += color_2.value;
}

__forceinline__ __device__ COLOR COLOR::operator-(float f)
{
  return COLOR(value - make_float3(f));
}

__forceinline__ __device__ COLOR COLOR::operator-(COLOR const &color_2)
{
  return COLOR(value - color_2.value);
}

__device__ void COLOR::operator-=(float f)
{
  value -= make_float3(f);
}

__device__ void COLOR::operator-=(COLOR const &color_2)
{
  value -= color_2.value;
}

__forceinline__ __device__ COLOR COLOR::operator*(float f)
{
  return COLOR(value * make_float3(f));
}

__forceinline__ __device__ COLOR COLOR::operator*(COLOR const &color_2)
{
  return COLOR(value * color_2.value);
}

__device__ void COLOR::operator*=(float f)
{
  value *= make_float3(f);
}

__device__ void COLOR::operator*=(COLOR const &color_2)
{
  value *= color_2.value;
}

__forceinline__ __device__ COLOR COLOR::operator/(float f)
{
  return COLOR(value / make_float3(f));
}

__forceinline__ __device__ COLOR COLOR::operator/(COLOR const &color_2)
{
  return COLOR(value / color_2.value);
}

__forceinline__ __device__ void COLOR::operator/=(float f)
{
  value = value / make_float3(f);
}

__forceinline__ __device__ void COLOR::operator/=(COLOR const &color_2)
{
  value = value / color_2.value;
}

__forceinline__ __device__ float &COLOR::operator[](int index)
{
  return (index == 0) ? value.x : (index == 1) ? value.y : value.z;
}

float avg(COLOR value)
{
  return (value[0] + value[1] + value[2]) / 3.f;
}

void clamp_negative_color(COLOR &color)
{
  color.value = optix::clamp(color.value, make_float3(0.f), color.value);
}

COLOR sqrt(COLOR color)
{
  return make_float3(sqrtf(color.value.x), sqrtf(color.value.y), sqrtf(color.value.z));
}

// COLOR sinf(COLOR color)
// {
// return make_float3(sinf(color.value.x), sinf(color.value.y), sinf(color.value.z));
// }

// COLOR cosf(COLOR color)
// {
// return make_float3(cosf(color.value.x), cosf(color.value.y), cosf(color.value.z));
// }


// COLOR powf(COLOR color, float e)
// {
// return make_float3(powf(color.value.x, e), powf(color.value.y, e), powf(color.value.z, e));
// }

COLOR lerp(COLOR color_1, COLOR color_2, float a)
{
  //Need explicitly optix namespace here.
  return optix::lerp(color_1.value, color_2.value, a);
}

COLOR bilerp(COLOR c_00, COLOR c_01, COLOR c_10, COLOR c_11, float dx, float dy)
{
  return lerp(lerp(c_00, c_01, dy), lerp(c_10, c_11, dx), dx);
}

COLOR trilerp(
    COLOR c_000,
    COLOR c_001,
    COLOR c_010,
    COLOR c_011,
    COLOR c_100,
    COLOR c_101,
    COLOR c_110,
    COLOR c_111,
    float dx,
    float dy,
    float dz)
{
  return lerp(bilerp(c_000, c_001, c_010, c_011, dy, dz), bilerp(c_100, c_101, c_110, c_111, dy, dz), dx);
}


COLOR expf(COLOR color)
{
  //Need explicitly optix namespace here.
  return optix::expf(color.value);
}

typedef COLOR COLOR_B;

}   // namespace spectrum
}   // namespace mrf