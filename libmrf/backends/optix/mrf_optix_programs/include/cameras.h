/*
 *
 * author : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#pragma once

#include <optixu/optixu_math_namespace.h>
#include "include/path_tracer_definitions.h"
#include "include/random.h"

using namespace optix;

rtDeclareVariable(float3, eye, , );
rtDeclareVariable(float3, U, , );
rtDeclareVariable(float3, V, , );
rtDeclareVariable(float3, W, , );

#if defined(CAM_PINHOLE)

#elif defined(CAM_THIN_LENS)
rtDeclareVariable(float, aperture, , );
rtDeclareVariable(float3, blur_dir_x, , );
rtDeclareVariable(float3, blur_dir_y, , );

#elif defined(CAM_ORTHOGRAPHIC)
rtDeclareVariable(float2, sensor_size, , );

#elif defined(CAM_GONIO)

#elif defined(CAM_ENVIRONMENT)
rtDeclareVariable(float, theta, , );
rtDeclareVariable(float, phi, , );


#endif

void ray_generation(
    const float2 &      pixel,
    const float2 &      jitter_scale,
    const float &       sqrt_num_samples,
    const unsigned int &current_sample,
    unsigned int &      seed,
    float3 &            ray_origin,
    float3 &            ray_direction)
{
  unsigned int x = current_sample % int(sqrt_num_samples);
  unsigned int y = current_sample / int(sqrt_num_samples);

// CAMERA PINHOLE
#if defined(CAM_PINHOLE)
  float2 jitter = make_float2(x - rnd(seed), y - rnd(seed));
  float2 d      = pixel + jitter * jitter_scale;
  ray_origin    = eye;
  ray_direction = normalize(d.x * U + d.y * V + W);

// CAMERA THIN LENS
#elif defined(CAM_THIN_LENS)
  float  rad    = sqrt(rnd(seed)) * aperture * 0.5;
  float  the    = 6.28318531 * rnd(seed);
  float3 offset = (blur_dir_x * cos(the) + blur_dir_y * sin(the)) * rad;

  float2 jitter = make_float2(x - rnd(seed), y - rnd(seed));
  float2 d      = pixel + jitter * jitter_scale;
  ray_origin    = eye + offset;
  ray_direction = normalize(W + U * d.x + V * d.y - offset);

// CAMERA ORTHO
#elif defined(CAM_ORTHOGRAPHIC)
  float2 jitter = make_float2(x - rnd(seed), y - rnd(seed));
  float2 d      = pixel + jitter * jitter_scale;
  ray_origin    = sensor_size.x * d.x * U + sensor_size.y * d.y * V + eye;
  ray_direction = normalize(W);

// CAMERA GONIO
#elif defined(CAM_GONIO)
  float2 jitter = make_float2(x - rnd(seed), y - rnd(seed));
  float2 d      = pixel + jitter * jitter_scale;
  ray_origin    = d.x * U + d.y * V + eye;
  ray_direction = normalize(W);

// CAMERA ENVIRONMENT
#elif defined(CAM_ENVIRONMENT)
  float2 jitter = make_float2(x - rnd(seed), y - rnd(seed));
  float2 d      = pixel + jitter * jitter_scale;
  d.y           = -d.y;
  d.y *= 0.5 * 3.141593;
  d.y += theta;
  //if(d.y < 0.5 * 3.141593)
  //    d.y = 0.5 * 3.141593 - d.y;
  d.x *= 3.141593;
  d.x += phi;
  ray_origin    = eye;
  ray_direction = make_float3(sin(d.y) * cos(d.x), cos(d.y), sin(d.x) * sin(d.y));

// DEFAULT CAMERA
#else
  float2 jitter = make_float2(x - rnd(seed), y - rnd(seed));
  float2 d      = pixel + jitter * jitter_scale;
  ray_origin    = eye;
  ray_direction = make_float3(0., 0., 1.);   //normalize(d.x * U + d.y * V + W);
#endif
}