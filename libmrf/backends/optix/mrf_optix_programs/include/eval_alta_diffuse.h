COLOR computeColorFromDirection(float3 normal, float3 tangent, float3 bitangent, float3 view, float3 light)
{
  size_t3 data_size = brdf_data.size();

  int total_size = data_size.x * data_size.y * data_size.z;
  //if no diffuse data is present in the scene,
  // a dummy buffer of size [1,1,1] is passed to this kernel
  if (total_size <= 1)
  {
    return COLOR(0.f);
  }

  float nDotL   = dot(light, normal);
  float nDotV   = dot(view, normal);
  float d_phi   = M_PIf;
  float EPSILON = 0.0001f;

  if (nDotL < 1.f - EPSILON && nDotV < 1.f - EPSILON)
  {
    //METHOD 1
    /*
        float2 lp = normalize(make_float2(light - nDotL*normal));
        float2 vp = normalize(make_float2(view - nDotV*normal));

        //BEWARE : D_PHI IS IN [0,PI] only valid for bilateral symmetric BRDF
        // CLAMP IS USED FOR STABILITY
        d_phi = acos( clamp( dot(lp,vp), -1., 1.) );
    */


    //METHOD 2
    // float phi_out = atan2f(clamp(-dot(view, bitangent), -1.f, 1.f), clamp(-dot(view, tangent), -1.f, 1.f));
    // float phi_in = atan2f(clamp(-dot(light, bitangent), -1.f, 1.f), clamp(-dot(light, tangent), -1.f, 1.f));
    // float phi_diff = phi_out - phi_in;
    // d_phi = abs(phi_diff);


    //METHOD 3
    float3 local_light = toLocalFrame(light, normal, tangent, bitangent);
    float3 local_view  = toLocalFrame(view, normal, tangent, bitangent);

    // //float theta_light = acos( local_light.z );
    // //float theta_view  = acos( local_view.z  );
    d_phi = abs(azimuthAngle2(local_light) - azimuthAngle2(local_view));
  }

  float theta_view  = acos(nDotV);
  float theta_light = acos(nDotL);

  float3 grid_min = make_float3(0.f, 0.f, 0.f);
  float3 grid_max
      = make_float3(M_PIf / 2.f - M_PIf / 360.f, M_PIf / 2.f - M_PIf / 360.f, 2.f * M_PIf - 2.f * M_PIf / 360.f);



  theta_view  = min(theta_view / (grid_max.x - grid_min.x), 1.0);
  theta_light = min(theta_light / (grid_max.y - grid_min.y), 1.0);
  d_phi       = min(d_phi / (grid_max.z - grid_min.z), 1.0);

  float X = theta_view * (data_size.x - 1);
  float Y = theta_light * (data_size.y - 1);
  float Z = d_phi * (data_size.z - 1);

  int idX = int(X);
  int idY = int(Y);
  int idZ = int(Z);

  // #ifdef INTERPOLATE_DIFFUS // Maybe one day

  uint3   id000 = make_uint3(idX, idY, idZ);
  COLOR_B brdf  = brdf_data[id000];


  if (_interpolation == 1)
  {
    int idX2 = min((idX + 1), int(data_size.x - 1));
    int idY2 = min((idY + 1), int(data_size.y - 1));
    int idZ2 = min((idZ + 1), int(data_size.z - 1));

    float dx    = X - idX;
    float dy    = Y - idY;
    float dz    = Z - idZ;
    uint3 id001 = make_uint3(idX, idY, idZ2);
    uint3 id010 = make_uint3(idX, idY2, idZ);
    uint3 id011 = make_uint3(idX, idY2, idZ2);
    uint3 id100 = make_uint3(idX2, idY, idZ);
    uint3 id101 = make_uint3(idX2, idY, idZ2);
    uint3 id110 = make_uint3(idX2, idY2, idZ);
    uint3 id111 = make_uint3(idX2, idY2, idZ2);

    brdf = trilerp(
        brdf,
        brdf_data[id001],
        brdf_data[id010],
        brdf_data[id011],
        brdf_data[id100],
        brdf_data[id101],
        brdf_data[id110],
        brdf_data[id111],
        dx,
        dy,
        dz);
  }

//#endif
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  return colorFromAsset(brdf, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
  return brdf;
#endif
}
