float3 toLocalFrame(float3 dir, float3 normal)
{
  float3 res;

  res.x = clamp(dot(dir, tangent), -1.f, 1.f);
  res.y = clamp(dot(dir, bitangent), -1.f, 1.f);
  res.z = clamp(dot(dir, normal), -1.f, 1.f);

  return res;
}

float ggx_G1(float cos_theta, float sqr_alpha_g)
{
  float sqr_tan_theta = (1.f - cos_theta * cos_theta) / (cos_theta * cos_theta);
  return 2.f / (1.f + sqrtf(1.f + sqr_alpha_g * sqr_tan_theta));
}

float ggx_G1(float alpha_x, float alpha_y, float cosX, float cosY, float cos_theta)
{
  float xterm     = cosX * alpha_x;
  float yterm     = cosY * alpha_y;
  float sqr_theta = cos_theta * cos_theta;
  return 1.f / (cos_theta + sqrtf(xterm * xterm + yterm * yterm + sqr_theta));
}

float ggx_geometricTerm(float3 h, float3 n, float3 v, float3 l, float2 alpha_g)
{
  float alpha_gX  = alpha_g.x;
  float alpha_gY  = alpha_g.y;
  bool  isotropic = alpha_gX == alpha_gY;

  float dot_NV = dot(n, v);
  float dot_NL = dot(n, l);
  float dot_HV = dot(h, v);
  float dot_HL = dot(h, l);

  //TODO better check on validity here
  if ((dot_HL / dot_NL <= 0.0f) || (dot_HV / dot_NV <= 0.0f)) return 0.f;

  float g1_v, g1_l;
  if (isotropic)
  {
    g1_l = ggx_G1(dot_NL, alpha_gX * alpha_gX);
    g1_v = ggx_G1(dot_NV, alpha_gX * alpha_gX);
  }
  else
  {
    float dot_NV = dot(n, v);
    float dot_NL = dot(n, l);

    float3 local_v = toLocalFrame(v, n);
    float3 local_l = toLocalFrame(l, n);

    g1_v = ggx_G1(alpha_gX, alpha_gY, local_v.x, local_v.y, local_v.z);
    g1_l = ggx_G1(alpha_gX, alpha_gY, local_l.x, local_l.y, local_l.z);
  }
  return g1_v * g1_l;
}

//GTR2 isotropic variant
float ggx_distribution(float sqr_alpha_g, float cos_theta_h)
{
  if (cos_theta_h < 0.f) return 0.f;
  float sqr_cos_theta_h = cos_theta_h * cos_theta_h;
  float sqr_term        = sqr_cos_theta_h * (sqr_alpha_g - 1.f) + 1.f;
  return sqr_alpha_g / (M_PIf * sqr_term * sqr_term);
}

//GTR2 variant anisotropic
float ggx_distribution(float alpha_x, float alpha_y, float dot_HX, float dot_HY, float dot_NH)
{
  if (dot_NH < 0.f) return 0.f;

  float sqr_cos_theta_h = dot_NH * dot_NH;

  float x_comp     = (dot_HX * dot_HX) / (alpha_x * alpha_x);
  float y_comp     = (dot_HY * dot_HY) / (alpha_y * alpha_y);
  float aniso_term = sqr_cos_theta_h + x_comp + y_comp;

  return 1.f / (M_PIf * alpha_x * alpha_y * aniso_term * aniso_term);
}

//Return halfVector.
float3 ggx_sample(float e1, float e2, float3 in_dir, float3 normal, float2 alpha_g)
{
  float3 out_dir   = make_float3(0.f);
  float  alpha_gX  = alpha_g.x;
  float  alpha_gY  = alpha_g.y;
  bool   isotropic = alpha_gX == alpha_gY;

  float const phi_H  = 2.f * M_PIf * e2;
  float       cos_pH = cosf(phi_H);
  float       sin_pH = sinf(phi_H);

  float theta_H;
  if (isotropic)
    theta_H = atan2f(alpha_gX * sqrtf(e1), sqrtf(1.f - e1));
  else
  {
    float x_comp = (cos_pH * cos_pH) / (alpha_gX * alpha_gX);
    float y_comp = (sin_pH * sin_pH) / (alpha_gY * alpha_gY);
    theta_H      = atan2f(sqrtf(e1), sqrtf(1.f - e1) * (x_comp + y_comp));
  }

  //Generate H locally then globally
  float cos_theta_H = cosf(theta_H);
  float sin_theta_H = sinf(theta_H);

  optix::Onb onb(normal);

  return cos_pH * sin_theta_H * onb.m_tangent + sin_pH * sin_theta_H * onb.m_binormal + cos_theta_H * onb.m_normal;
}

float vnggx_geometricTerm(float dot_H, float dot_N, float sqr_alpha_g)
{
  return (dot_H / dot_N <= 0.f) ? 0.f : ggx_G1(dot_N, sqr_alpha_g);
}

float vnggx_distribution(float sqr_alpha_g, float cos_theta_h, float dot_NV, float dot_NL)
{
  float sqr_cos_theta_h = cos_theta_h * cos_theta_h;
  float sqr_term        = sqr_cos_theta_h * (sqr_alpha_g - 1.f) + 1.f;

  float ggx = sqr_alpha_g / (M_PIf * sqr_term * sqr_term);
  return ggx_G1(dot_NV, sqr_alpha_g) * ggx;
}


// Input in_dir: view direction
// Input e1, e2: uniform random numbers
// Output Ne: normal sampled with PDF D_Ve(Ne) = G1(Ve) * max(0, dot(Ve, Ne)) * D(Ne) / Ve.z
float3 vnggx_sample(float e1, float e2, float3 in_dir, float3 n, float2 alpha_g)
{
  //TODO FIX ME !!!

  float alpha_gX = alpha_g.x;
  float alpha_gY = alpha_g.y;

  // Section 3.2: transforming the view direction to the hemisphere configuration
  float3 Vh = make_float3(alpha_gX * in_dir.x, alpha_gY * in_dir.y, in_dir.z);
  // Section 4.1: orthonormal basis (with special case if cross product is zero)
  float normVh = length(Vh);
  if (normVh < 0.000001f) return n;
  Vh /= normVh;

  float  lensq = Vh.x * Vh.x + Vh.y * Vh.y;
  float3 T1    = (lensq > 0.f) ? make_float3(-Vh.y, Vh.x, 0.f) / sqrtf(lensq) : make_float3(1.f, 0.f, 0.f);
  float3 T2    = normalize(cross(Vh, T1));

  // optix::Onb onb_v(Vh);
  // float3 T1 = onb_v.m_tangent;
  // float3 T2 = onb_v.m_binormal;

  // Section 4.2: parameterization of the projected area
  float r   = sqrtf(e1);
  float phi = 2.f * M_PIf * e2;
  float t1  = r * cos(phi);
  float t2  = r * sin(phi);
  float s   = 0.5 * (1.f + Vh.z);
  t2        = (1.f - s) * sqrtf(1.f - t1 * t1) + s * t2;
  // Section 4.3: reprojection onto hemisphere
  float3 Nh = t1 * T1 + t2 * T2 + sqrtf(max(0.f, 1.f - t1 * t1 - t2 * t2)) * Vh;
  // Section 3.4: transforming the normal back to the ellipsoid configuration
  float3 Ne     = make_float3(alpha_gX * Nh.x, alpha_gY * Nh.y, max(0.f, Nh.z));
  float  normNe = length(Ne);
  if (normNe < 0.000001f) return n;

  Ne /= normNe;

  // optix::Onb onb(n);
  // onb.inverse_transform(Ne);

  return Ne;
}
