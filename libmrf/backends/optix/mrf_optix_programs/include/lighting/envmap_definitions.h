#ifndef ENVMAP_DEFINITIONS
#define ENVMAP_DEFINITIONS

#include "optixu_matrix_namespace.h"

using namespace optix;
using namespace mrf::spectrum;

rtDeclareVariable(int, use_envmap, , );

rtBuffer<float>    envmap_cdf_rows;
rtBuffer<float, 2> envmap_cdf_cols;
rtBuffer<float>    envmap_pdf_rows;
rtBuffer<float, 2> envmap_pdf_cols;

rtDeclareVariable(optix::Matrix3x3, envmap_rotation_matrix, , );
rtDeclareVariable(optix::Matrix3x3, envmap_inv_rotation_matrix, , );


//rtTextureSampler<float4, 2> envmap;
#ifdef MRF_RENDERING_MODE_SPECTRAL
rtBuffer<COLOR_B, 2> envmap;
rtDeclareVariable(float, envmap_avg_lum_over_horizon, , );
#else
rtBuffer<float4, 2> envmap;
rtDeclareVariable(float3, envmap_avg_lum_over_horizon, , );
#endif



#ifdef MRF_ENVMAP_IS
/*
float findCdfInverse(float rand, rtBuffer<float> cdf_buffer)
{
  int id=0;
  while(id<cdf_buffer.size() && rand < cdf_buffer[id])
  {
    id++;
  }
  return id;
}
*/
__host__ __device__ int findCdfInverse(float rand, int begin, int end)
{
  /*
    int id=begin;
    while(id<end && rand > envmap_cdf_rows[id])
    {
      id++;
    }
    return id;
  */

  uint count = end - begin;
  while (count > 0)
  {
    //value = envmap_cdf_rows[begin];
    uint step = count / 2;
    uint id   = begin + step;
    if (envmap_cdf_rows[id] < rand)
    {
      begin = id + 1;
      count = count - (step + 1);
    }
    else
    {
      count = step;
    }
  }
  return begin;
}

__host__ __device__ int findCdf2dInverse(float rand, uint row, int begin, int end)
{
  /*
  uint2 id;
  id.y = row;
  id.x = begin;
  while(id.x<end && rand > envmap_cdf_cols[id])
  {
    id.x++;
  }
  return id.x;
*/


  uint2 id;
  id.y = row;

  uint count = end - begin;
  while (count > 0)
  {
    //value = envmap_cdf_rows[begin];
    uint step = count / 2;
    //uint id = begin + step;
    id.x = begin + step;
    if (envmap_cdf_cols[id] < rand)
    {
      begin = id.x + 1;
      count = count - (step + 1);
    }
    else
    {
      count = step;
    }
  }
  return begin;
}
#endif

__host__ __device__ void envmapDirectionToPixel(float3 direction, uint2 &pixel_pos)
{
  size_t2 envmap_size = envmap.size();

  direction = envmap_inv_rotation_matrix * direction;

  float phi   = atan2f(direction.x, -direction.z);
  float theta = acosf(direction.y);

  float u = (1.f + (phi * M_1_PIf)) * 0.5f;
  float v = (theta * M_1_PIf);

  pixel_pos.x = clamp(uint(u * (envmap_size.x - 1)), uint(0), uint(envmap_size.x - 1));
  pixel_pos.y = clamp(uint(v * (envmap_size.y - 1)), uint(0), uint(envmap_size.y - 1));
}

__host__ __device__ void envmapPixelToDirection(uint2 pixel_pos, float3 &direction)
{
  size_t2 envmap_size = envmap.size();
  float   u           = float(pixel_pos.x) / float(envmap_size.x - 1);
  float   v           = float(pixel_pos.y) / float(envmap_size.y - 1);

  float phi   = (2.f * u - 1.f) * M_PIf;
  float theta = v * M_PIf;

  direction.x = sinf(theta) * sinf(phi);
  direction.y = cosf(theta);
  direction.z = -sinf(theta) * cosf(phi);

  direction = envmap_rotation_matrix * direction;
}

__host__ __device__ void envmapPixelToDirection(uint2 pixel_pos, float3 &direction, float &theta, float &phi)
{
  size_t2 envmap_size = envmap.size();
  float   u           = float(pixel_pos.x) / float(envmap_size.x - 1);
  float   v           = float(pixel_pos.y) / float(envmap_size.y - 1);

  phi   = (2.f * u - 1.f) * M_PIf;
  theta = v * M_PIf;

  direction.x = sinf(theta) * sinf(phi);
  direction.y = cosf(theta);
  direction.z = -sinf(theta) * cosf(phi);

  direction = envmap_rotation_matrix * direction;
}

__host__ __device__ COLOR_B envmap_lookup(uint2 pixel_pos)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  return envmap[pixel_pos];
#else
  return make_float3(envmap[pixel_pos]);
#endif
}

__host__ __device__ COLOR_B envmap_lookup(float3 direction)
{
  uint2 pixel_pos;
  envmapDirectionToPixel(direction, pixel_pos);
  return envmap_lookup(pixel_pos);
}

//OLD METHOD
/*
#ifdef MRF_RENDERING_MODE_SPECTRAL
__host__ __device__ float envmap_lookup_interpolated(float3 old_direction, float3 new_direction, float interpolation_value)
#else
__host__ __device__ float3 envmap_lookup_interpolated(float3 old_direction, float3 new_direction, float interpolation_value)
#endif
{

  size_t2 envmap_size = envmap.size();

  new_direction = interpolation_value * old_direction + (1.f-interpolation_value)*new_direction;
  interpolation_value = 0.f;

  float theta = atan2f( old_direction.x, -old_direction.z );
  float rotation = 0.5f*M_PIf;

  theta -= rotation;

  if(theta<-M_PIf)
    theta += 2.f*M_PIf;
  if(theta>M_PIf)
    theta -= 2.f*M_PIf;

  float phi = acosf( old_direction.y );


  float new_theta = atan2f( new_direction.x, -new_direction.z );
  new_theta -= rotation;

  if(new_theta<-M_PIf)
    new_theta += 2.f*M_PIf;
  if(new_theta>M_PIf)
    new_theta -= 2.f*M_PIf;

  float new_phi = acosf(new_direction.y);



  theta = interpolation_value * theta + (1.f-interpolation_value)*new_theta;
  phi = interpolation_value * phi + (1.f-interpolation_value)*new_phi;
*/
/*
  if(theta<-M_PIf)
    theta += 2.f*M_PIf;
  if(theta>M_PIf)
    theta -= 2.f*M_PIf;
*/
/*
  float u  = (1.f + ( theta * M_1_PIf ))*0.5f;
  float v  = ( phi * M_1_PIf );

  uint2 pixel_pos;
  pixel_pos.x = clamp(uint(u*(envmap_size.x-1)),uint(0),uint(envmap_size.x-1));
  pixel_pos.y = clamp(uint(v*(envmap_size.y-1)),uint(0),uint(envmap_size.y-1));

#ifdef MRF_RENDERING_MODE_SPECTRAL
  return envmap[pixel_pos];
#else
  return make_float3(envmap[pixel_pos]);
#endif
}
*/


__host__ __device__ float envmap_pdf(float3 L, float3 n)
{
#ifdef MRF_ENVMAP_IS
  float temp_theta;   //, temp_phi;
  uint2 temp_pixel_pos;
  envmapDirectionToPixel(L, temp_pixel_pos);
  int   row_envmap = temp_pixel_pos.y;
  int   col_envmap = temp_pixel_pos.x;
  uint2 id_pdf;
  id_pdf.y      = row_envmap;
  id_pdf.x      = col_envmap;
  float pdf_col = envmap_pdf_cols[id_pdf];   //pdf size==envmap.width
  float pdf_row = envmap_pdf_rows[row_envmap];

  temp_theta = acosf(L.y);
  return pdf_row * pdf_col / (2.f * M_PIf * M_PIf * sin(temp_theta));
#else
  return dot(n, L) / M_PIf;
#endif
}

__host__ __device__ float4 sampleEnvmap(float r1, float r2, float3 normal)
{
  float3 L;
#ifdef MRF_ENVMAP_IS
  size_t2 envmap_size = envmap.size();

  uint row_envmap = findCdfInverse(r1, 0, envmap_cdf_rows.size());
  row_envmap      = clamp(row_envmap, uint(0), uint(envmap_size.y - 1));
  float pdf_row   = envmap_pdf_rows[row_envmap];

  uint col_envmap = findCdf2dInverse(r2, row_envmap, 0, envmap_size.x + 1);
  col_envmap      = clamp(col_envmap, uint(0), uint(envmap_size.x - 1));

  uint2 pixel_pos;
  pixel_pos.x = col_envmap;
  pixel_pos.y = row_envmap;
  float temp_theta, temp_phi;

  envmapPixelToDirection(pixel_pos, L, temp_theta, temp_phi);
#else
  float3 temp_p;
  cosine_sample_hemisphere(r1, r2, temp_p);
  optix::Onb onb(normal);
  onb.inverse_transform(temp_p);
  L = temp_p;
#endif
  return make_float4(normalize(L), RT_DEFAULT_MAX);
}

__host__ __device__ void
sampleEnvmap(float3 normal, float r1, float r2, float &Ldist, float3 &L, COLOR_B &light_emission, float &light_pdf)
{
#ifdef MRF_ENVMAP_IS

  size_t2 envmap_size = envmap.size();

  uint row_envmap = findCdfInverse(r1, 0, envmap_cdf_rows.size());
  row_envmap      = clamp(row_envmap, uint(0), uint(envmap_size.y - 1));
  float pdf_row   = envmap_pdf_rows[row_envmap];

  uint col_envmap = findCdf2dInverse(r2, row_envmap, 0, envmap_size.x + 1);
  col_envmap      = clamp(col_envmap, uint(0), uint(envmap_size.x - 1));

  uint2 pixel_pos;
  pixel_pos.x = col_envmap;
  pixel_pos.y = row_envmap;

  float pdf_col = envmap_pdf_cols[pixel_pos];   //pdf size==envmap.width

  float temp_theta, temp_phi;

  Ldist = RT_DEFAULT_MAX;
  envmapPixelToDirection(pixel_pos, L, temp_theta, temp_phi);
  L = normalize(L);


  // FROM PBRT Compute PDF for sampled infinite light direction
  //*pdf = mapPdf / (2 * Pi * Pi * sinTheta);
  light_pdf      = pdf_row * pdf_col / (2.0 * M_PIf * M_PIf * sin(temp_theta));
  light_emission = envmap_lookup(pixel_pos);

#else
  //Cosine sampling envmap
  float3 temp_p;
  cosine_sample_hemisphere(r1, r2, temp_p);
  optix::Onb onb(normal);
  onb.inverse_transform(temp_p);
  L         = temp_p;
  light_pdf = dot(normal, L) / M_PIf;
  //nDl   = dot( ffnormal, L );
  //LnDl = 1.f;
  light_emission = envmap_lookup(L);
  Ldist          = RT_DEFAULT_MAX;
#endif
}

// SAMPLING IN IMAGE SPACE
/*
        uint row_envmap = z3 * (envmap_size.y-1);
        float pdf_row = 1.f/float(envmap_size.y);

        uint col_envmap = z4 * (envmap_size.x-1);
        float pdf_col = 1.f/float(envmap_size.x);

        uint2 pixel_pos;
        pixel_pos.x = col_envmap;
        pixel_pos.y = row_envmap;


        float temp_theta, temp_phi;

        Ldist = RT_DEFAULT_MAX;
        envmapPixelToDirection(pixel_pos, L, temp_theta, temp_phi);
        L = normalize(L);
        nDl   = dot( ffnormal, L );
        LnDl = 1.f;
        //TO DO DIVIDE SIN THETA ???
        //light_pdf = pdf_row*pdf_col;// / sin(temp_theta);
        light_pdf = nDl /M_PIf;// / sin(temp_theta);
        light_emission = envmap_lookup(pixel_pos);
  */
//  SOME TESTS
/*
float4 temp_color = display_buffer[pixel_pos];

if(sin(temp_theta)<0)
{
  temp_color.y = 0.f;
  temp_color.x = -sin(temp_theta);
}
else
{
  temp_color.y = sin(temp_theta);
  temp_color.x = 0.f;
}
*/
//temp_color.y = sin(temp_theta);
//temp_color.x = -sin(temp_theta);
//display_buffer[pixel_pos] = temp_color;
//display_buffer[pixel_pos] = make_float3(sin(temp_theta),-sin(temp_theta),0,1.0);

//  SOME TESTS
/*
float4 temp_color = display_buffer[pixel_pos];
temp_color.x =  0.01f;
temp_color.z =  0.01f;
temp_color.w =  1.f;
temp_color.y = temp_color.y + 0.001f;
display_buffer[pixel_pos] = temp_color;
*/
/*
if(launch_index.x==100 && launch_index.y==256)
{
  //display_buffer[pixel_pos] = make_float4(0.0,1.0,0.0, 1.f);
  display_buffer[pixel_pos] = envmap[pixel_pos];
}
*/


__host__ __device__ void
envmap_eval_and_pdf(float3 brdf_direction, float &Ldist, COLOR_B &light_emission, float &light_pdf)
{
#ifdef MRF_ENVMAP_IS
  //IS envmap
  float temp_theta;   //, temp_phi;
  uint2 temp_pixel_pos;
  envmapDirectionToPixel(brdf_direction, temp_pixel_pos);
  int   row_envmap = temp_pixel_pos.y;
  int   col_envmap = temp_pixel_pos.x;
  uint2 id_pdf;
  id_pdf.y            = row_envmap;
  id_pdf.x            = col_envmap;
  float   pdf_col     = envmap_pdf_cols[id_pdf];   //pdf size==envmap.width
  float   pdf_row     = envmap_pdf_rows[row_envmap];
  size_t2 envmap_size = envmap.size();

  temp_theta     = acosf(brdf_direction.y);
  light_pdf      = pdf_row * pdf_col / (2.f * M_PIf * M_PIf * sin(temp_theta));
  Ldist          = RT_DEFAULT_MAX;
  light_emission = envmap_lookup(temp_pixel_pos);

#else
  light_pdf      = 1.f;
  Ldist          = RT_DEFAULT_MAX;
  light_emission = envmap_lookup(brdf_direction);
#endif
}

#endif
