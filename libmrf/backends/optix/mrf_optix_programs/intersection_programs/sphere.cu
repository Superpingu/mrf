/*
 * Copyright (c) 2018 NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <optix_world.h>
#include "intersection_refinement.h"

using namespace optix;

rtDeclareVariable(float3, center, , );
rtDeclareVariable(float, radius, , );
rtDeclareVariable(int, lgt_instance, , ) = {0};

rtDeclareVariable(float3, tangent, attribute tangent, );
rtDeclareVariable(float3, bitangent, attribute bitangent, );
rtDeclareVariable(float3, texcoord, attribute texcoord, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );
rtDeclareVariable(float3, back_hit_point, attribute back_hit_point, );
rtDeclareVariable(float3, front_hit_point, attribute front_hit_point, );

rtDeclareVariable(int, lgt_idx, attribute lgt_idx, );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

RT_PROGRAM void intersect(int primIdx)
{
  float3 oc  = center - ray.origin;
  float  tca = dot(ray.direction, oc);
  if (tca > 0.f)
  {
    float3 oh       = ray.direction * tca;
    float  d_square = dot(oc, oc) - tca * tca;
    if (d_square < radius * radius)
    {
      float thc = sqrt(radius * radius - d_square);
      float t0  = tca - thc;
      float t1  = tca + thc;

      //swap value if needed
      if (t0 > t1)
      {
        float temp = t0;
        t0         = t1;
        t1         = temp;
      }

      if (t0 < 0.f)
      {
        t0 = t1;                // if t0 is negative, let's use t1 instead
        if (t0 < 0.f) return;   // both t0 and t1 are negative NO INTERSECTION
      }

      float t = t0;

      if (rtPotentialIntersection(t))
      {
        float3 p = ray.origin + ray.direction * t;
        float3 n = normalize(p - center);

        tangent = abs(shading_normal.x) < 0.999 ? make_float3(1, 0, 0) : make_float3(0, 1, 0);
        tangent = normalize(cross(shading_normal, tangent));

        refine_and_offset_hitpoint(
            ray.origin + t * ray.direction,
            ray.direction,
            n,
            p,
            back_hit_point,
            front_hit_point);

        texcoord = make_float3(atan2f(n.x, n.z) / (2 * M_PIf) + 0.5, n.y * 0.5 + 0.5, 0);

        shading_normal = geometric_normal = n;
        if (n.z < -0.9999f)   //Singularity
        {
          tangent = make_float3(0.f, 1.f, 0.f);
        }
        else
        {
          tangent = normalize(cross(make_float3(0.f, 1.f, 0.f), n));
        }
        bitangent = normalize(cross(n, tangent));

        lgt_idx = lgt_instance;

        rtReportIntersection(0);
      }
    }
  }
}

RT_PROGRAM void bounds(int, float result[6])
{
  optix::Aabb *aabb = (optix::Aabb *)result;

  aabb->m_min = center + radius * make_float3(-1.0, -1.0, -1.0);
  aabb->m_max = center + radius * make_float3(1.0, 1.0, 1.0);
}
