/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend optix renderer
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>

#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/mat4.hpp>
#include <mrf_core/math/vec3.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/image/color_image.hpp>
#include <mrf_core/image/uniform_spectral_image.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_matrix_namespace.h>

// Default catch block
#define OPTIX_CATCH(ctx)                                                                                               \
  catch (mrf::optix_backend::APIError & e) { mrf::optix_backend::handleError(ctx, e.code, e.file.c_str(), e.line); }   \
  catch (std::exception & e)                                                                                           \
  {                                                                                                                    \
    mrf::optix_backend::reportErrorMessage(e.what());                                                                  \
    exit(1);                                                                                                           \
  }

// Error check/report helper for users of the C API
#define RT_CHECK_ERROR(func)                                                                                           \
  do                                                                                                                   \
  {                                                                                                                    \
    RTresult code = func;                                                                                              \
    if (code != RT_SUCCESS)                                                                                            \
    {                                                                                                                  \
      std::cout << code << std::endl;                                                                                  \
      throw mrf::optix_backend::APIError(code, __FILE__, __LINE__);                                                    \
    }                                                                                                                  \
  } while (0)

#define STRINGIFY(x)  STRINGIFY2(x)
#define STRINGIFY2(x) #x
#define LINE_STR      STRINGIFY(__LINE__)

// Error check/report helper for users of the C API
#define NVRTC_CHECK_ERROR(func)                                                                                        \
  do                                                                                                                   \
  {                                                                                                                    \
    nvrtcResult code = func;                                                                                           \
    if (code != NVRTC_SUCCESS)                                                                                         \
    {                                                                                                                  \
      std::cout << "Error in NVRTC" << std::string(nvrtcGetErrorString(code));                                         \
      throw Exception("ERROR: " __FILE__ "(" LINE_STR "): " + std::string(nvrtcGetErrorString(code)));                 \
    }                                                                                                                  \
  } while (0)

namespace mrf
{
namespace optix_backend
{
inline optix::float3 mrfToOptix(mrf::math::Vec3f vec)
{
  optix::float3 res;
  res.x = vec.x();
  res.y = vec.y();
  res.z = vec.z();
  return res;
}

inline mrf::math::Vec3f optixToMRF(optix::float3 vec)
{
  mrf::math::Vec3f res;
  res.setX(vec.x);
  res.setY(vec.y);
  res.setZ(vec.z);
  return res;
}

inline optix::float3 mrfToOptix(mrf::color::Color vec)
{
  optix::float3 res;
  res.x = vec.r();
  res.y = vec.g();
  res.z = vec.b();
  return res;
}

inline optix::float3 make_float3(const float *a)
{
  return optix::make_float3(a[0], a[1], a[2]);
}

inline optix::Matrix3x3 mrfToOptix(const mrf::math::Mat3f &mat)
{
  return optix::Matrix3x3(mat.ptr());
}

inline mrf::math::Mat3f optixToMRF(const optix::Matrix3x3 &mat)
{
  mrf::math::Mat3f res;
  res.setColumn(0, optixToMRF(mat.getCol(0)));
  res.setColumn(1, optixToMRF(mat.getCol(1)));
  res.setColumn(2, optixToMRF(mat.getCol(2)));
  return res;
}

MRF_OPTIX_EXPORT bool update1DTextureFromData(std::vector<float> data, optix::TextureSampler &texture);

MRF_OPTIX_EXPORT bool updateTextureFromImage(mrf::image::ColorImage const &image, optix::TextureSampler &texture);

MRF_OPTIX_EXPORT bool updateTextureFromSpectralImage(
    mrf::image::UniformSpectralImage const &image,
    optix::TextureSampler &                 texture,
    std::vector<uint>                       wavelengths);

MRF_OPTIX_EXPORT optix::TextureSampler
                 createClampedOptixTexture(optix::Context &context, RTformat, size_t width, size_t height = 0, size_t depth = 0);

MRF_OPTIX_EXPORT optix::TextureSampler
                 createRepeatedOptixTexture(optix::Context &context, RTformat, size_t width, size_t height = 0, size_t depth = 0);

// Calculate appropriate U,V,W for pinhole_camera shader.
void calculateCameraVariables(
    optix::float3  eye,            // Camera eye position
    optix::float3  lookat,         // Point in scene camera looks at
    optix::float3  up,             // Up direction
    float          fov,            // Horizontal or vertical field of view (assumed horizontal, see boolean below)
    float          aspect_ratio,   // Pixel aspect ratio (width/height)
    optix::float3 &U,              // [out] U coord for camera program
    optix::float3 &V,              // [out] V coord for camera program
    optix::float3 &W,              // [out] W coord for camera program
    bool           fov_is_vertical = false);

// Apply fov effect on U,V vector
void applyFov(
    optix::float3 &U,   // [modify]
    optix::float3 &V,   // [modify]
    const float &  fov,
    const float &  aspect_ratio,
    const bool &   fov_is_vertical);

#ifdef RENDERER_INTERACTIVE

enum bufferPixelFormat
{
  BUFFER_PIXEL_FORMAT_DEFAULT,   // The default depending on the buffer type
  BUFFER_PIXEL_FORMAT_RGB,       // The buffer is RGB or RGBA
  BUFFER_PIXEL_FORMAT_BGR,       // The buffer is BGR or BGRA
};

// Display contents of buffer, where the OpenGL/GLUT context is managed by caller.
void displayBufferGL(
    optix::Buffer     buffer,   // Buffer to be displayed
    bufferPixelFormat format
    = BUFFER_PIXEL_FORMAT_DEFAULT,   // The pixel format of the buffer or 0 to use the default for the pixel type
    bool disable_srgb_conversion = false);

// Returns a GL Texture ID of a buffer, where the OpenGL/GLUT context is managed by caller.
unsigned int bufferToGLTextureID(
    optix::Buffer     buffer,   // Buffer to be displayed
    bufferPixelFormat format
    = BUFFER_PIXEL_FORMAT_DEFAULT,   // The pixel format of the buffer or 0 to use the default for the pixel type
    bool         disable_srgb_conversion = false,
    unsigned int tex_id                  = 0);

// Display frames per second, where the OpenGL/GLUT context
// is managed by the caller.
void displayFps(unsigned total_frame_count, bool on_pause);

// Display a short string starting at x,y.
void displayText(const char *text, float x, float y);

#endif

//Fill a uniform image from an OptiX buffer
bool optixBufferToImage(optix::Buffer buffer, mrf::image::ColorImage &image);

//Fill a uniform spectral image from an OptiX buffer
bool optixBufferToSpectralImage(
    optix::Buffer                     buffer,
    mrf::image::UniformSpectralImage &image,
    std::vector<uint>                 wavelengths,
    uint                              n_samples);

// Create an output buffer with given specifications
optix::Buffer createOutputBuffer(
    optix::Context context,   // optix context
    RTformat       format,    // Pixel format (must be ubyte4 for pbo)
    unsigned       width,     // Buffer width
    unsigned       height,    // Buffer height
    bool           use_pbo);            // Buffer type

// Resize a Buffer and its underlying GLBO if necessary
void resizeBuffer(
    optix::Buffer buffer,   // Buffer to be modified
    unsigned      width,    // New buffer width
    unsigned      height);       // New buffer height

// Exeption to be thrown by RT_CHECK_ERROR macro
struct APIError
{
  APIError(RTresult c, const std::string &f, int l): code(c), file(f), line(l) {}
  RTresult    code;
  std::string file;
  int         line;
};

// Display error message
void reportErrorMessage(const char *message);   // Error mssg to be displayed

// Queries provided RTcontext for last error message, displays it, then exits.
void handleError(
    RTcontext   context,   // Context associated with the error
    RTresult    code,      // Code returned by OptiX API call
    const char *file,      // Filename for error reporting
    int         line);             // File lineno for error reporting




}   // namespace optix_backend
}   // namespace mrf
