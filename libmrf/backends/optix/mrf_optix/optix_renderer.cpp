/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/

#include <limits>

#include <mrf_core/io/scene_parser.hpp>
#include <mrf_core/io/camera_parser.hpp>

#include <mrf_core/data_struct/array2d.hpp>

#include <mrf_core/sampling/random_generator.hpp>
#include <mrf_core/sampling/sobol.hpp>

#include <mrf_core/radiometry/illuminants.hpp>
#include <mrf_core/rendering/color_background_manager.hpp>

#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>

#include <mrf_core/lighting/light.hpp>

#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/math/math.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <array>

using mrf::optix_backend::mrfToOptix;

namespace mrf
{
namespace optix_backend
{
OptixRenderer::OptixRenderer(OptixMaterialPlugins &mplugins, OptixLightPlugins &lplugins)
  : Renderer(nullptr, mrf::rendering::RENDERER_RNG::HALTON, static_cast<unsigned long long>(time(NULL)))
  , _context(0)
  , _light_handler(nullptr)
  , _material_handler(nullptr)
  , _geometry_handler(nullptr)
  , _rr_begin_depth(1)
  , _cuda_renderer_filename("rays_generation.cu")
  , _mat_plugins(mplugins)
  , _light_plugins(lplugins)
  , _ptx_config(PTXConfig())
{}

OptixRenderer::OptixRenderer(
    OptixMaterialPlugins &mplugins,
    OptixLightPlugins &   lplugins,
    bool                  rt_ptx,
    bool                  overwrite_ptx)
  : Renderer(nullptr, mrf::rendering::RENDERER_RNG::HALTON, static_cast<unsigned long long>(time(NULL)))
  , _context(0)
  , _light_handler(nullptr)
  , _material_handler(nullptr)
  , _geometry_handler(nullptr)
  , _rr_begin_depth(1)
  , _cuda_renderer_filename("rays_generation.cu")
  , _mat_plugins(mplugins)
  , _light_plugins(lplugins)
  , _ptx_config(PTXConfig(rt_ptx, overwrite_ptx))
{}


OptixRenderer::~OptixRenderer()
{
  delete _light_handler;
  delete _material_handler;
  delete _geometry_handler;
}


#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixRenderer::init(bool interactive, uint nb_wavelengths_per_pass)
{
  createRenderContext(interactive, nb_wavelengths_per_pass);
}
#else
void OptixRenderer::init(bool interactive)
{
  createRenderContext(interactive);
}
#endif


void OptixRenderer::shutdown()
{
  if (_context) destroyRenderContext();
}


void OptixRenderer::fetchImage()
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  optixBufferToSpectralImage(_context["output_buffer"]->getBuffer(), *_output_buffer_image, _wavelengths, _spp);
  if (_save_variance)
    optixBufferToSpectralImage(_context["variance_buffer"]->getBuffer(), *_variance_buffer_image, _wavelengths, _spp);
#else
  optixBufferToImage(_context["output_buffer"]->getBuffer(), *_output_buffer_image);
  if (_save_variance) optixBufferToImage(_context["variance_buffer"]->getBuffer(), *_variance_buffer_image);
#endif
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixRenderer::debugSpectralOptixBuffer(
    std::string const &prepend_to_filename,
    mrf::uint          wavelength_pass_number,
    mrf::uint          frame_number)
{
  using namespace mrf;
  using namespace mrf::image;
  using namespace mrf::util;

  auto ogpu_buffer = _context["output_buffer"]->getBuffer()->get();

  RTsize buffer_width, buffer_height, buffer_depth;

  void *imageData;
  RT_CHECK_ERROR(rtBufferMap(ogpu_buffer, &imageData));

  float *flt_data = (float *)(imageData);
  RT_CHECK_ERROR(rtBufferGetSize2D(ogpu_buffer, &buffer_width, &buffer_height));
  RT_CHECK_ERROR(rtBufferGetSize3D(ogpu_buffer, &buffer_width, &buffer_height, &buffer_depth));

  RTsize elementSize;
  rtBufferGetElementSize(ogpu_buffer, &elementSize);

  std::vector<uint> const &current_wavelengths = _wavelengths_per_passes[wavelength_pass_number];

#  ifdef MRF_RENDERING_DEBUG
  std::cout << " (images can be upside down here) ENTERING " << __FILE__ << " " << __LINE__
            << "  Argument of functions Wavelength Pass Number" << wavelength_pass_number << " Num Frame "
            << frame_number << std::endl;
  std::cout << " THE OPTIX BUFFER DIMENSIONS ARE (width,height,buffer_depth):  " << buffer_width << " " << buffer_height
            << " " << buffer_depth << " and element size = " << elementSize << std::endl;
  std::cout << " Saving Spectral images from wavelength " << current_wavelengths.front() << " TO "
            << current_wavelengths.back() << std::endl;
#  endif

  // A current UniformSpectral Image to save it
  // TODO: add constructor in Spectral Image
  EnviSpectralImage tmp_spectral_image(
      static_cast<size_t>(buffer_width),
      static_cast<size_t>(buffer_height),
      current_wavelengths);

  uint const layer_size_in_bytes = static_cast<uint>(buffer_width * buffer_width * sizeof(float));
  uint const layer_size          = static_cast<uint>(buffer_width * buffer_width);

  std::vector<mrf::data_struct::Array2D> &spectral_data_per_layer = tmp_spectral_image.data();

  for (uint wave_index = 0; wave_index < current_wavelengths.size(); wave_index++)
  {
    //std::cout << "wave_index = " << wave_index << std::endl;

    mrf::data_struct::Array2D &current_layer = spectral_data_per_layer[wave_index];

    float *ptr_to_current_layer = current_layer.data();

    memcpy(ptr_to_current_layer, flt_data, layer_size_in_bytes);

    flt_data += layer_size;
  }

  // Unmap BUFFER NOW
  RT_CHECK_ERROR(rtBufferUnmap(ogpu_buffer));

  // SAVE TMP Image to DISK
  std::string const filename_debug_hdr = prepend_to_filename + "_output_optix_buffer_at_frame"
                                         + std::to_string(frame_number) + "_debug_image_spectral_pass"
                                         + std::to_string(wavelength_pass_number) + ".hdr";
  std::string const filename_debug_raw = prepend_to_filename + "_output_optix_buffer_at_frame"
                                         + std::to_string(frame_number) + "_debug_image_spectral_pass"
                                         + std::to_string(wavelength_pass_number) + ".raw";

  tmp_spectral_image.save(filename_debug_hdr);

#  ifdef MRF_RENDERING_DEBUG
  std::cout << " EXITING  debugSpectralOptixBuffer at " << __FILE__ << " " << __LINE__ << std::endl;
#  endif
}
#endif


std::string OptixRenderer::getInfos() const
{
  std::stringstream ss;
  ss << Renderer::getInfos() << std::endl;
  ss << "  Rendering using OptiX backend";

  return ss.str();
}


void OptixRenderer::setRotationPhi(float phi)
{
  Renderer::setRotationPhi(phi);

  // Update the Envmap C++ Object
  _envmap->updateRotationAngles(_envmap_rotation_phi, _envmap_rotation_theta, 0.f);

  //Now update the Optix Contet
  updateOptixContextForEnvmapRotation();
}


void OptixRenderer::setRotationTheta(float theta)
{
  Renderer::setRotationTheta(theta);

  // Update the Envmap C++ Object
  _envmap->updateRotationAngles(_envmap_rotation_theta, _envmap_rotation_theta, 0.f);

  //Now update the Optix Context
  updateOptixContextForEnvmapRotation();
}


void OptixRenderer::updateOptixContextForEnvmapRotation()
{
  //Now update the Optix Contet
  _context["envmap_rotation_matrix"]->setMatrix3x3fv(false, _envmap->rotationMatrix().ptr());

  //TODO faster inverse by just using inverse angles ?
  _context["envmap_inv_rotation_matrix"]->setMatrix3x3fv(false, _envmap->rotationMatrix().inverse().ptr());
}


#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixRenderer::createRenderContext(bool interactive_mode, uint nb_wavelengths_per_pass)
#else
void OptixRenderer::createRenderContext(bool interactive_mode)
#endif
{
  _is_interactive = interactive_mode;

  switch (_rng_type)
  {
    case mrf::rendering::RENDERER_RNG::SOBOL:
      _cuda_compile_options.push_back("-DSOBOL_RNG");
      break;

    case mrf::rendering::RENDERER_RNG::OPTIX:
      _cuda_compile_options.push_back("-DOPTIX_RNG");
      break;

    case mrf::rendering::RENDERER_RNG::HALTON:
      _cuda_compile_options.push_back("-DHALTON_RNG");
      break;
  }

  if (_is_interactive)
  {
    _cuda_compile_options.push_back("-DINTERACTIVE_MODE");
  }

  if (_envmap_is)
  {
    _cuda_compile_options.push_back("-DMRF_ENVMAP_IS");
  }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  _cuda_compile_options.push_back("-DMRF_RENDERING_MODE_SPECTRAL");

  _nb_max_wavelengths_per_pass = nb_wavelengths_per_pass;

  if (_nb_max_wavelengths_per_pass > 1)
  {
    _cuda_compile_options.push_back("-DMRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED");
    _cuda_compile_options.push_back("-DNB_MAX_WAVES_MULTIPLEXED=" + std::to_string(_nb_max_wavelengths_per_pass));
  }
#endif

  // Setup programs
  try
  {
    // Set RTX global attribute
    const int RTX = true;
    if (rtGlobalSetAttribute(RT_GLOBAL_ATTRIBUTE_ENABLE_RTX, sizeof(RTX), &RTX) != RT_SUCCESS)
      mrf::gui::fb::Loger::getInstance()->warn("Error setting RTX mode.");
    else
      mrf::gui::fb::Loger::getInstance()->info("OptiX RTX execution mode is ", (RTX) ? "on" : "off");

    _context = optix::Context::create();
    mrf::gui::fb::Loger::getInstance()->trace("CONTEXT created");
    _context->setPrintEnabled(true);
    mrf::gui::fb::Loger::getInstance()->info(
        " ### Optix Context Info. Number of Enabled Devices : ",
        _context->getEnabledDeviceCount());
    std::vector<int> device_ids = _context->getEnabledDevices();

    //Init arch to sm_75, the higher supported by cuda 10.2.
    //    (cuda 11.x not compatible with optix 6.5 and below)
    int arch_sm[2] = {7, 5};
    for (size_t i = 0; i < device_ids.size(); i++)
    {
      mrf::gui::fb::Loger::getInstance()->info(
          " Name of device Number " + std::to_string(device_ids[i]) + ": ",
          _context->getDeviceName(device_ids[i]));
      mrf::gui::fb::Loger::getInstance()->info(
          " Available Memory in MB: ",
          _context->getAvailableDeviceMemory(device_ids[i]) / (1024.0 * 1024.0));

      int compute_capabilities[2];
      _context->getDeviceAttribute(
          device_ids[i],
          RT_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY,
          2 * sizeof(int),
          &compute_capabilities);
      mrf::gui::fb::Loger::getInstance()->info(
          " Compute Capability " + std::to_string(compute_capabilities[0]) + " "
          + std::to_string(compute_capabilities[1]));

      //Downgrade arch if GPU does not support it.
      arch_sm[0] = std::min(arch_sm[0], compute_capabilities[0]);
      arch_sm[1] = std::min(arch_sm[1], compute_capabilities[1]);

      int max_texture_count;
      _context->getDeviceAttribute(
          device_ids[i],
          RT_DEVICE_ATTRIBUTE_MAX_HARDWARE_TEXTURE_COUNT,
          sizeof(int),
          &max_texture_count);
      mrf::gui::fb::Loger::getInstance()->info(" Max Hardware Texture Counts:", max_texture_count);
    }
    mrf::gui::fb::Loger::getInstance()->info(" ### ");

    _cuda_compile_options.push_back("-arch=compute_" + std::to_string(arch_sm[0]) + std::to_string(arch_sm[1]));

    //_light_handler.setContext(_context);
    _context->setRayTypeCount(2);
    _context->setEntryPointCount(1);
    _context->setStackSize(1800);

    _context["scene_epsilon"]->setFloat(1.e-3f);
    _context["pathtrace_ray_type"]->setUint(0u);
    _context["pathtrace_shadow_ray_type"]->setUint(1u);
    _context["rr_begin_depth"]->setUint(_rr_begin_depth);
    _context["max_path_length"]->setUint(_max_path_length);
    _context["num_samples_per_frame"]->setUint(_num_samples_per_frame);
    _context["next_event"]->setUint(_next_event_type);
    _context["_spatial_factor"]->setUint(_spatial_subdivision_factor);


#ifdef MRF_RENDERING_MODE_SPECTRAL
    _context["first_wave_index"]->setUint(0);
    _context["last_wave_index"]->setUint(1);
#endif

    mrf::gui::fb::Loger::getInstance()->trace(
        " CONTEXT created. Attempting to compile Path Tracer ",
        _cuda_renderer_filename);

    //const char *ray_ptx = mrf::optix_backend::getPtxString(_cuda_renderer_filename.c_str(), _cuda_compile_options);
    //_context->setRayGenerationProgram(0, _context->createProgramFromPTXString(ray_ptx, "pathtrace_camera"));

    PTXConfig   base_config = _ptx_config;
    OptixKernel prg_renderer(_cuda_renderer_filename.c_str(), _cuda_compile_options, base_config.toMatConfig());
    prg_renderer.compile(_context);
    _context->setRayGenerationProgram(0, prg_renderer.createProgram(_context, "pathtrace_camera"));

    //const char *exception_ptx = mrf::optix_backend::getPtxString("exception.cu", _cuda_compile_options);
    //_context->setExceptionProgram(0, _context->createProgramFromPTXString(exception_ptx, "exception"));

    OptixKernel prg_exception("exception.cu", _cuda_compile_options, base_config.toMatConfig());
    prg_exception.compile(_context);
    _context->setExceptionProgram(0, prg_exception.createProgram(_context, "exception"));

    //const char *miss_ptx = mrf::optix_backend::getPtxString("miss.cu", _cuda_compile_options);
    //_context->setMissProgram(0, _context->createProgramFromPTXString(miss_ptx, "miss"));

    OptixKernel prg_miss("miss.cu", _cuda_compile_options, base_config.toMatConfig());
    prg_miss.compile(_context);
    _context->setMissProgram(0, prg_miss.createProgram(_context, "miss"));
  }
  catch (optix::Exception &e)
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Optix EXCEPTION.  SOMETHING WHEN WRONG !!!! SORRY ");
    mrf::gui::fb::Loger::getInstance()->fatal(e.what());
    throw;
  }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  _context["bad_color"]->setFloat(0.f);
#else
  _context["bad_color"]->setFloat(
      1000000.0f,
      0.0f,
      1000000.0f);   // Super magenta to make sure it doesn't get averaged out in the progressive rendering.
#endif

  //do this after scene import
  //to detect if we need envmap support or not

  //Create additionnal rng buffer
  if (_rng_type == mrf::rendering::RENDERER_RNG::HALTON)
  {
    auto old_max_path = _max_path_length;
    //limit max path length to 10 inmrf::rendering::RENDERER_RNG::HALTON rng
    _max_path_length = std::min(10, _max_path_length);
    if (_max_path_length != old_max_path)
    {
      mrf::gui::fb::Loger::getInstance()->warn(
          "Your max path length (-mpl) has been reduced to 10 due to RNG set tomrf::rendering::RENDERER_RNG::HALTON !");
    }
    _context["max_path_length"]->setUint(_max_path_length);
  }
}

#ifdef RENDERER_INTERACTIVE
bool OptixRenderer::createBufferFromGLBuffer(
    int         GLBufferID,
    std::string buffer_name,
    size_t      frame_width,
    size_t      frame_height,
    int         nb_comp)
{
  if (_scene_imported)
  {
    try
    {
      if (_context[buffer_name]->getBuffer().get()) _context[buffer_name]->getBuffer().get()->destroy();

      _render_width  = frame_width;
      _render_height = frame_height;

      optix::Buffer buffer = nullptr;
#  ifdef MRF_RENDERING_MODE_SPECTRAL
      math::Vec3i size = math::Vec3i(int(_render_width), int(_render_height), int(_wavelengths.size()));
#  else
      math::Vec3i size = math::Vec3i(int(_render_width), int(_render_height), 1);
#  endif

      buffer = _context->createBufferFromGLBO(RT_BUFFER_INPUT_OUTPUT, GLBufferID);

      buffer->setSize(size.x(), size.y(), size.z());
      buffer->setFormat(RTformat(RT_FORMAT_FLOAT + nb_comp - 1));

      _context[buffer_name]->set(buffer);

      return buffer.get() != nullptr;
    }
    OPTIX_CATCH(_context->get())
  }
  return false;
}


bool OptixRenderer::readyBufferForUpdate(int /*GLBufferID*/, std::string buffer_name)
{
  optix::Buffer buffer = _context[buffer_name]->getBuffer();
  RTresult      res    = rtBufferGLUnregister(buffer->get());
  return res == RT_SUCCESS;
}


bool OptixRenderer::readyBufferForRendering(int /*GLBufferID*/, std::string buffer_name)
{
  optix::Buffer buffer = _context[buffer_name]->getBuffer();
  RTresult      res    = rtBufferGLRegister(buffer->get());
  return res == RT_SUCCESS;
}

#endif


void OptixRenderer::createRenderTarget()
{
  optix::Buffer buffer;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  if (_nb_max_wavelengths_per_pass > 1)
  {
    // IMPORTANT MUTLIPLEXING MODE WHERE MULTIPLE SPECTRAL PASSES ARE NEEDED REQUIRES AN INPUT+OUTPUT BUFFER
    buffer = _context->createBuffer(
        RT_BUFFER_INPUT_OUTPUT,
        RT_FORMAT_FLOAT,
        _render_width,
        _render_height,
        _wavelengths.size());
  }
  else
    buffer = _context->createBuffer(RT_BUFFER_INPUT_OUTPUT, RT_FORMAT_FLOAT, _render_width, _render_height, 1);
#else
  buffer          = _context->createBuffer(RT_BUFFER_OUTPUT, RT_FORMAT_FLOAT3, _render_width, _render_height, 1);
#endif
  _context["output_buffer"]->set(buffer);

  optix::Buffer variance_buffer;
#ifdef MRF_RENDERING_MODE_SPECTRAL
  if (_nb_max_wavelengths_per_pass > 1)
  {
    // IMPORTANT MUTLIPLEXING MODE WHERE MULTIPLE SPECTRAL PASSES ARE NEEDED REQUIRES AN INPUT+OUTPUT BUFFER
    variance_buffer = _context->createBuffer(
        RT_BUFFER_INPUT_OUTPUT,
        RT_FORMAT_FLOAT,
        (!_save_variance) ? 1 : _render_width,
        (!_save_variance) ? 1 : _render_height,
        (!_save_variance) ? 1 : _wavelengths.size());
  }
  else
    variance_buffer = _context->createBuffer(
        RT_BUFFER_INPUT_OUTPUT,
        RT_FORMAT_FLOAT,
        (!_save_variance) ? 1 : _render_width,
        (!_save_variance) ? 1 : _render_height,
        1);
#else
  variance_buffer = _context->createBuffer(
      RT_BUFFER_OUTPUT,
      RT_FORMAT_FLOAT3,
      (!_save_variance) ? 1 : _render_width,
      (!_save_variance) ? 1 : _render_height,
      1);
#endif
  _context["variance_buffer"]->set(variance_buffer);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  if (_nb_max_wavelengths_per_pass > 1)
  {
    optix::Buffer wavelength_buffer
        = _context->createBuffer(RT_BUFFER_INPUT_OUTPUT, RT_FORMAT_FLOAT, _wavelengths_bounds.size());

    _context["_wavelengths"]->set(wavelength_buffer);
  }
  else
  {
    _context["_wavelengths"]->setFloat((float)_wavelengths_per_passes[_num_pass][0]);
  }
#endif

  //Create additionnal rng buffer
  if (_rng_type == mrf::rendering::RENDERER_RNG::HALTON || _rng_type == mrf::rendering::RENDERER_RNG::SOBOL)
  {
    //cranley patterson rotation
    optix::Buffer cp_buffer
        = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, 2 * _render_width * _render_height);
    _context["cranley_patterson_buffer"]->setBuffer(cp_buffer);
    updateCranleyPattersonBuffer();
  }

  if (_rng_type == mrf::rendering::RENDERER_RNG::SOBOL)
  {
    optix::Buffer sobol_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_UNSIGNED_INT, 1);
    _context["sobol_buffer"]->setBuffer(sobol_buffer);
    updateSobolRandomNumbers();
  }
}


void OptixRenderer::resizeRenderTarget(size_t width, size_t height)
{
  _render_width  = width;
  _render_height = height;

  //mrf::optix_backend::resizeBuffer(_context["output_buffer"]->getBuffer(), width, height);
#ifdef MRF_RENDERING_MODE_SPECTRAL
  _context["output_buffer"]->getBuffer()->setSize(_render_width, _render_height, _wavelengths.size());
  if (_save_variance)
    _context["variance_buffer"]->getBuffer()->setSize(_render_width, _render_height, _wavelengths.size());
#else
  _context["output_buffer"]->getBuffer()->setSize(_render_width, _render_height, 1);
  if (_save_variance) _context["variance_buffer"]->getBuffer()->setSize(_render_width, _render_height, 1);
#endif
  if (_rng_type == mrf::rendering::RENDERER_RNG::HALTON || _rng_type == mrf::rendering::RENDERER_RNG::SOBOL)
  {
    _context["cranley_patterson_buffer"]->getBuffer()->setSize(2 * _render_width * _render_height);
    updateCranleyPattersonBuffer();
  }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  resizeSpectralOutput();
#endif
}


void OptixRenderer::updateCranleyPattersonBuffer()
{
  void *buffer_ptr = nullptr;

  if (_context && _context["cranley_patterson_buffer"] && _context["cranley_patterson_buffer"]->getBuffer())
    buffer_ptr = _context["cranley_patterson_buffer"]->getBuffer()->map();
  if (!buffer_ptr) return;

  mrf::sampling::RandomGenerator &rng = mrf::sampling::RandomGenerator::Instance();
  rng.setSeed(static_cast<unsigned long>(_rng_seed));

  for (uint i = 0; i < 2 * _render_width * _render_height; i++)
  {
    static_cast<float *>(buffer_ptr)[i] = rng.getFloat();
  }

  _context["cranley_patterson_buffer"]->getBuffer()->unmap();
}


void OptixRenderer::updateSobolRandomNumbers()
{
  /* sobol directions table */
  //int max_samples = 1;
  /*
  if (method == BRANCHED_PATH) {
    foreach(Light *light, scene->lights)
      max_samples = max(max_samples, light->samples);

    max_samples = max(max_samples, max(diffuse_samples, max(glossy_samples, transmission_samples)));
    max_samples = max(max_samples, max(ao_samples, max(mesh_light_samples, subsurface_samples)));
    max_samples = max(max_samples, volume_samples);
  }
  */
  //max_samples *= (max_bounce + transparent_max_bounce + 3 + BSSRDF_MAX_HITS);

  int  max_samples     = (_max_path_length + 1);
  uint PRNG_BASE_NUM   = 4;
  uint PRNG_BOUNCE_NUM = 8;

  int dimensions = PRNG_BASE_NUM + max_samples * PRNG_BOUNCE_NUM;
  dimensions     = std::min(dimensions, SOBOL_MAX_DIMENSIONS);

  //uint *directions = dscene->sobol_directions.alloc(SOBOL_BITS*dimensions);
  //uint *directions = new uint[SOBOL_BITS*dimensions];
  _context["sobol_buffer"]->getBuffer()->setSize(SOBOL_BITS * dimensions);

  //mrf::sampling::sobol_generate_direction_vectors((uint(*)[SOBOL_BITS])directions, dimensions);

  void *buffer_ptr = _context["sobol_buffer"]->getBuffer()->map();

  mrf::sampling::sobol_generate_direction_vectors((uint(*)[SOBOL_BITS])buffer_ptr, dimensions);

  _context["sobol_buffer"]->getBuffer()->unmap();

  //delete[] directions;
}


void OptixRenderer::destroyRenderContext()
{
  if (_context)
  {
    _context->destroy();
    _context = nullptr;
  }
}


void OptixRenderer::importMrfScene(
    mrf::io::SceneParser *scene_loader,
    std::string           camera_file,
    size_t                starting_cam,
    size_t                num_cam)
{
  if (!_context)
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL
    createRenderContext(_is_interactive, _nb_max_wavelengths_per_pass);
#else
    createRenderContext(_is_interactive);
#endif
  }

  _parser = scene_loader;
  if (!scene_loader->loadScene(_mrf_scene))
  {
    mrf::gui::fb::Loger::getInstance()->fatal("FAIL TO LOAD SCENE");
    _scene_imported = false;
    return;
  }

  std::unordered_map<std::string, mrf::rendering::Camera *> cameras;
  mrf::io::CameraParser::loadCameras(camera_file, cameras);

  //TODO :  FIND A MORE SUITABLE CAMERA THAN THIS FIXED ONE !
  if (cameras.size() == 0)
  {
    _all_cameras.push_back(new mrf::rendering::Pinhole(
        mrf::math::Vec3f(0.f, 2.1502713f, 50.f),
        mrf::math::Vec3f(0.f, 2.1502713f, 0.f),
        mrf::math::Vec3f(0.f, 1.f, 0.f),
        50.f,
        0.1f,
        1000.f,
        1280,
        720,
        1));

    _current_camera = 0;
  }
  else
  {
    const size_t total_cam = (num_cam == 0) ? cameras.size() : std::min(num_cam + starting_cam, cameras.size());
    size_t       start_cam = 0;

    for (auto &cam : cameras)
    {
      if (start_cam < starting_cam)   //Look for the first camera that should be added
      {
        if (_is_interactive)   //Add all if interactive.
          _all_cameras.push_back(cam.second);
        ++start_cam;
      }
      else if (
          start_cam < total_cam
          || _is_interactive)   //Add the rest, either desired number if offline, or all if interactive.
      {
        _all_cameras.push_back(cam.second);
        ++start_cam;
      }
      else
      {
        break;
      }
    }
    _current_camera = (_is_interactive) ? starting_cam % cameras.size() : 0;
  }
  _main_camera = _all_cameras[_current_camera]->copy();
  updateBackendCamera();

  setCamera();

  if (_light_handler == nullptr)
  {
    _light_handler = new mrf::optix_backend::OptixLightHandler(_context, _mrf_scene->lights());
  }
  _light_handler->loadPlugins(_light_plugins);

  if (_material_handler == nullptr)
  {
    _material_handler = new mrf::optix_backend::OptixMaterialHandler(_context, _mrf_scene->getAllMaterials());
  }
  _material_handler->loadPlugins(_mat_plugins);

  if (_geometry_handler == nullptr)
  {
    _geometry_handler = new mrf::optix_backend::OptixGeometryHandler(_context);
  }

  // First we treat the background to detect if we need to add MRF_USE_ENVMAP in the cuda compile options
  // of the materials

  // Import background
  if (_mrf_scene->hasBackground())
  {
    mrf::rendering::BckgManager *     bck        = _mrf_scene->background();
    mrf::lighting::EnvMap *           envmap_bck = dynamic_cast<mrf::lighting::EnvMap *>(bck);
    mrf::rendering::ColorBckgManager *color_bck  = dynamic_cast<mrf::rendering::ColorBckgManager *>(bck);

    if (color_bck)
    {
      // Color bck manager are uniform, does not depend on any direction
      auto color = color_bck->colorFromDirection(mrf::math::Vec3f());
#ifdef MRF_RENDERING_MODE_SPECTRAL
      // Don't push value here for spectral, it's done in OptixRenderer::updateBackground()
      if (_nb_max_wavelengths_per_pass > 1)
      {
        _background_color_buffer
            = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, _nb_max_wavelengths_per_pass + 1);
        _context["bg_color"]->setBuffer(_background_color_buffer);
      }
#else
      _context["bg_color"]->setFloat(optix::make_float3(color.r(), color.g(), color.b()));
#endif
    }
    else
    {
#ifdef MRF_RENDERING_MODE_SPECTRAL
      // Don't push value here for spectral, it's done in OptixRenderer::updateBackground()
      if (_nb_max_wavelengths_per_pass > 1)
      {
        auto dummy_color_bg
            = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, _nb_max_wavelengths_per_pass + 1);
        _context["bg_color"]->setBuffer(dummy_color_bg);
      }
#else
      _context["bg_color"]->setFloat(optix::make_float3(0.f));
#endif
    }

    if (envmap_bck)
    {
      _envmap = envmap_bck;

      mrf::gui::fb::Loger::getInstance()->trace(
          "A envmap has been detected, enabling envmap lookup in the miss program.");
      _context["use_envmap"]->setInt(1);

#ifdef MRF_RENDERING_MODE_SPECTRAL
      //uint current_wavelength = _wavelengths[_num_pass];
      mrf::image::UniformSpectralImage const &usi = envmap_bck->image();

      /**
       * WARNING WE DO NOT SUPPORT DYNAMIC SIZE ENVMAPS
       * once the buffers for the envmap are created they are never resized
       **/

      if (!_envmap_buffer.get())
      {
        if (_nb_max_wavelengths_per_pass > 1)
        {
          _envmap_buffer = _context->createBuffer(RT_BUFFER_INPUT);
          _envmap_buffer->setFormat(RT_FORMAT_USER);
          _envmap_buffer->setElementSize(sizeof(float) * (_nb_max_wavelengths_per_pass + 1));
          _envmap_buffer->setSize(usi.width(), usi.height());
        }
        else
        {
          _envmap_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, usi.width(), usi.height());
        }
      }

      // Don't update value here for spectral, it's done in OptixRenderer::updateBackground()
#else
      mrf::image::ColorImage const &envmap_image = envmap_bck->image();
      //optix::TextureSampler texture_sampler;

      if (!_envmap_buffer.get())
        _envmap_buffer
            = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT4, envmap_image.width(), envmap_image.height());

      void *buffer_ptr = _envmap_buffer->map();

      for (uint i = 0; i < envmap_image.width() * envmap_image.height(); i++)
      {
        auto envmap_pixel = envmap_image.getPixel(i);

        float *pixel = &((float *)buffer_ptr)[4 * i];
        pixel[0]     = envmap_pixel.x();
        pixel[1]     = envmap_pixel.y();
        pixel[2]     = envmap_pixel.z();
        pixel[3]     = 1.f;
      }

      _envmap_buffer->unmap();
#endif
      _context["envmap"]->setBuffer(_envmap_buffer);

      //Init rotation matrix and rotation angles for envmap
      _envmap_rotation_phi   = envmap_bck->rotationPhi() * 180.f / M_PIf;
      _envmap_rotation_theta = envmap_bck->rotationTheta() * 180.f / M_PIf;
      _envmap->updateRotationAngles(_envmap_rotation_phi, _envmap_rotation_theta, 0.f);

      updateOptixContextForEnvmapRotation();

      //create cdf and pdf buffers
      if (_envmap_is)
      {
        envmap_bck->computeCDF2D();

        //create and allocat buffer
        if (!_envmap_cdf_rows_buffer.get())
          _envmap_cdf_rows_buffer
              = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, envmap_bck->getCdfRows().size());

        void *envmap_cdf_rows_buffer_ptr = _envmap_cdf_rows_buffer->map();

        //copy data
        auto const &cdf_rows = envmap_bck->getCdfRows();
        for (uint i = 0; i < cdf_rows.size(); i++)
        {
          static_cast<float *>(envmap_cdf_rows_buffer_ptr)[i] = cdf_rows[i];
        }
        _envmap_cdf_rows_buffer->unmap();

        //create and allocat buffer
        if (!_envmap_cdf_cols_buffer.get())
          //_envmap_cdf_cols_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, (envmap_bck->getCdfRows().size() - 1), envmap_bck->getCdfColumns(0).size());
          _envmap_cdf_cols_buffer = _context->createBuffer(
              RT_BUFFER_INPUT,
              RT_FORMAT_FLOAT,
              envmap_bck->getCdfColumns(0).size(),
              (envmap_bck->getCdfRows().size() - 1));
        //_envmap_cdf_cols_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, (envmap_bck->getCdfRows().size()-1) * envmap_bck->getCdfColumns(0).size());

        //copy data
        void *envmap_cdf_cols_buffer_ptr = _envmap_cdf_cols_buffer->map();
        for (uint row_number = 0; row_number < envmap_bck->getCdfRows().size() - 1; row_number++)
        {
          auto const &cdf = envmap_bck->getCdfColumns(row_number);
          for (uint i = 0; i < cdf.size(); i++)
          {
            static_cast<float *>(envmap_cdf_cols_buffer_ptr)[envmap_bck->getCdfColumns(0).size() * row_number + i]
                = cdf[i];
          }
        }
        _envmap_cdf_cols_buffer->unmap();


        //create and allocat buffer
        if (!_envmap_pdf_rows_buffer.get())
        {
          _envmap_pdf_rows_buffer
              = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, envmap_bck->getPdfRows().size());
        }

        //copy data
        void *      _envmap_pdf_rows_buffer_ptr = _envmap_pdf_rows_buffer->map();
        auto const &pdf_rows                    = envmap_bck->getPdfRows();
        for (uint i = 0; i < pdf_rows.size(); i++)
        {
          static_cast<float *>(_envmap_pdf_rows_buffer_ptr)[i] = pdf_rows[i];
        }
        _envmap_pdf_rows_buffer->unmap();

        //create and allocat buffer
        if (!_envmap_pdf_cols_buffer.get())
          //_envmap_pdf_cols_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, (envmap_bck->getPdfRows().size()-1), envmap_bck->getPdfColumns(0).size());
          _envmap_pdf_cols_buffer = _context->createBuffer(
              RT_BUFFER_INPUT,
              RT_FORMAT_FLOAT,
              envmap_bck->getPdfColumns(0).size(),
              envmap_bck->getPdfRows().size());
        //_envmap_pdf_cols_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, (envmap_bck->getPdfRows().size() - 1) * envmap_bck->getPdfColumns(0).size());

        //copy data
        void *_envmap_pdf_cols_buffer_ptr = _envmap_pdf_cols_buffer->map();
        for (uint row_number = 0; row_number < envmap_bck->getPdfRows().size(); row_number++)
        {
          auto const &pdf = envmap_bck->getPdfColumns(row_number);
          for (uint i = 0; i < pdf.size(); i++)
          {
            static_cast<float *>(_envmap_pdf_cols_buffer_ptr)[envmap_bck->getPdfColumns(0).size() * row_number + i]
                = pdf[i];
          }
        }
        _envmap_pdf_cols_buffer->unmap();

        //link buffers to cuda code
        _context["envmap_cdf_rows"]->setBuffer(_envmap_cdf_rows_buffer);
        _context["envmap_cdf_cols"]->setBuffer(_envmap_cdf_cols_buffer);
        _context["envmap_pdf_rows"]->setBuffer(_envmap_pdf_rows_buffer);
        _context["envmap_pdf_cols"]->setBuffer(_envmap_pdf_cols_buffer);
      }
    }
    else
    {
#ifdef MRF_RENDERING_MODE_SPECTRAL
      if (_nb_max_wavelengths_per_pass > 1)
      {
        auto dummy_envmap_buffer = _context->createBuffer(RT_BUFFER_INPUT);
        dummy_envmap_buffer->setFormat(RT_FORMAT_USER);
        dummy_envmap_buffer->setElementSize(sizeof(float) * (_nb_max_wavelengths_per_pass + 1));
        dummy_envmap_buffer->setSize(1, 1);
        _context["envmap"]->setBuffer(dummy_envmap_buffer);
      }
      else
      {
        auto dummy_envmap_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, 1, 1);
        _context["envmap"]->setBuffer(dummy_envmap_buffer);
      }
#else

      auto dummy_envmap_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT4, 1, 1);
      _context["envmap"]->setBuffer(dummy_envmap_buffer);
#endif

      auto dummy_envmap_cdf_rows_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, 1);
      _context["envmap_cdf_rows"]->setBuffer(dummy_envmap_cdf_rows_buffer);
      auto dummy_envmap_cdf_cols_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, 1, 1);
      _context["envmap_cdf_cols"]->setBuffer(dummy_envmap_cdf_cols_buffer);
      auto dummy_envmap_pdf_rows_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, 1);
      _context["envmap_pdf_rows"]->setBuffer(dummy_envmap_pdf_rows_buffer);
      auto dummy_envmap_pdf_cols_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, 1, 1);
      _context["envmap_pdf_cols"]->setBuffer(dummy_envmap_pdf_cols_buffer);

      _context["use_envmap"]->setInt(0);
    }
    //{
    //  mrf::gui::fb::Loger::getInstance()->warn("Optix renderer unsupported background manager detected !");
    //}
  }


  //then cuda compile options are ok, we create the materials
#ifdef MRF_RENDERING_MODE_SPECTRAL
  if (_nb_max_wavelengths_per_pass > 1)
  {
    _material_handler->createMaterials(_cuda_compile_options, _ptx_config, _wavelengths_per_passes_bounds[0]);
  }
  else
  {
    _material_handler->createMaterials(_cuda_compile_options, _ptx_config, _wavelengths_per_passes[0]);
  }
#else
  _material_handler->createMaterials(_cuda_compile_options, _ptx_config);
#endif


  //then the lights
  _light_handler->createLights();

  //materials are now compiled we can instanciate the geometries
  _geometry_handler
      ->importMrfScene(*_mrf_scene, _material_handler[0], _light_handler[0], _cuda_compile_options, _ptx_config);

  createRenderTarget();
#ifdef MRF_RENDERING_MODE_SPECTRAL
  allocateSpectralOutput();
#endif

  updateWavelengths();
  updateMaterials();
  updateLights();
  updateBackground();

  _context->validate();

  _scene_imported = true;

  //Fake first frame so that OptiX can build its acceleration structure without it being count in the rendering time.
  resetRendering(true);
}

void OptixRenderer::importMrfScene(std::string scene_file, std::string camera_file, size_t starting_cam, size_t num_cam)
{
  if (_parser)
  {
    _parser->setPath(scene_file);
    importMrfScene(_parser, camera_file, starting_cam, num_cam);
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Trying to update scene without a parser, aborting !");
  }
}

void OptixRenderer::freeSceneResources()
{
  if (_mrf_scene)
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL
    clearSpectralOutput();
    _num_pass = 0;
#endif
    delete _material_handler;
    _material_handler = nullptr;
    delete _light_handler;
    _light_handler = nullptr;
    delete _geometry_handler;
    _geometry_handler = nullptr;

    if (_envmap)
    {
      _envmap    = nullptr;
      _envmap_is = true;
      //_cuda_compile_options.erase(std::remove(_cuda_compile_options.begin(), _cuda_compile_options.end(), "-DMRF_USE_ENVMAP"), _cuda_compile_options.end());
    }

    _render_width  = 0;
    _render_height = 0;

    _all_cameras.clear();
    //_backup_camera = nullptr;
    _main_camera = nullptr;

    _scene_imported = false;

    _num_frame = 0;

    delete _mrf_scene;
    _mrf_scene = nullptr;

    destroyRenderContext();
    _cuda_compile_options.clear();
  }
}

void OptixRenderer::updateBackendCamera()
{
  std::vector<std::string> full_compile_options = OptixCameraHandler::getCameraCompileOptions(_main_camera);
  full_compile_options.insert(full_compile_options.end(), _cuda_compile_options.begin(), _cuda_compile_options.end());
  PTXConfig   base_config = _ptx_config;
  OptixKernel prg_renderer(_cuda_renderer_filename.c_str(), full_compile_options, base_config.toMatConfig());
  std::string fn, ext;
  mrf::util::StringParsing::getFileExtension(_cuda_renderer_filename, ext);
  mrf::util::StringParsing::getFileNameWithoutExtension(_cuda_renderer_filename, fn);
  // Warning, cam_fn doesn't exist, the extension is only added for compatibily with the .compile(...) method.
  std::string cam_fn = fn + "_" + CameraType2string(_main_camera->getType()) + "." + ext;
  prg_renderer.compile(_context, cam_fn);
  _context->setRayGenerationProgram(0, prg_renderer.createProgram(_context, "pathtrace_camera"));
}

void OptixRenderer::updateCameraParameters()
{
  OptixCameraHandler::setCameraParameters(_main_camera, _context);
}

void OptixRenderer::resetRendering(bool clear)
{
  setNumPass(0);
  _spp = 0;
  //#ifdef RENDERER_INTERACTIVE
  resetNumFrame(1);

  // If subidivision is active, force a full kernel launch to reset the buffer (frame = 0 <=> output filled with black)
  if (_spatial_subdivision_factor > 1 || clear)
  {
    if (_context)
    {
      _context["frame_number"]->setUint(0);
      _context["_spatial_factor"]->setUint(1);
      _context["per_pixel_frame_number"]->setUint(0);
      _context->launch(0, _render_width, _render_height);
      _context["_spatial_factor"]->setUint(_spatial_subdivision_factor);
    }
  }
  //#else
  //  (void)clear;
  //#endif
}

float OptixRenderer::renderFrame(size_t /*width*/, size_t /*height*/, int /*posX*/, int /*posY*/)
{
  mrf::util::PrecisionTimer total_timer;
  total_timer.reset();
  total_timer.start();

  uint effective_width  = static_cast<uint>(ceil(_render_width / _spatial_subdivision_factor + 0.5f));
  uint effective_height = static_cast<uint>(ceil(_render_height / _spatial_subdivision_factor + 0.5f));

  _context["frame_number"]->setUint(_num_frame);

  //Compute and pass the effective number of samples computed per pixel when subdividing.
  int effective_spp = 1 + (_num_frame - 1) / (_spatial_subdivision_factor * _spatial_subdivision_factor);
  _context["per_pixel_frame_number"]->setUint(effective_spp);

  try
  {
    //RGB MODE: just need to launch context and increment num frame
    //No need to updateMaterials or light, already done once at init
#ifndef MRF_RENDERING_MODE_SPECTRAL
    _context->launch(0, effective_width, effective_height);
#else   //SPECTRAL MODE

    // HOW MANY SPECTRAL PASSES DO WE NEED ?
    bool log_time = false;

#  ifdef MALIA_RENDERING_DEBUG
    std::cout << __FILE__ << " " << __LINE__ << std::endl;
    std::cout << "Number of Spectral Pass nb_pass = " << nb_pass << std::endl;
    std::cout << " Frame number = " << _num_frame << std::endl;
#  endif
    // These are the spectral passes (Remember that many wavelengths can be evaluated inside the same pass)
    // When the wavelength per pass is equal to full multiplexing nb_pass = 1
    for (_num_pass = 0; _num_pass < _total_num_passes; _num_pass++)
    {
#  ifdef MALIA_RENDERING_DEBUG
      std::cout << " Processing Spectral Pass Number " << num_pass << std::endl;
#  endif

      const uint first_wave_idx = _num_pass * _nb_max_wavelengths_per_pass;
      const uint last_wave_idx  = first_wave_idx + (uint)_wavelengths_per_passes[_num_pass].size() - 1;
      //AF Remove: std::min<uint>((_num_pass + 1) * _nb_wavelengths_per_pass, uint(_wavelengths.size())) - 1;

      _context["first_wave_index"]->setUint(first_wave_idx);
      _context["last_wave_index"]->setUint(last_wave_idx);
      _context["nb_wavelengths"]->setUint((uint)_wavelengths_per_passes[_num_pass].size());

      // Update wavelengths in monochromatic
      // -----------------------------------
      // In multiplexed mode, the wavelength sampling within a bin is done in the cuda kernel
      // In monochromatic, we obtain an inefficient but reference sampling: each time, the assets must
      // be uploaded to the GPU for the selected wavelength.
      // TODO: an additional value have to be provided to the kernel in order to correctly distribute the
      // computed wavelength in correct bins.
      if (_nb_max_wavelengths_per_pass == 1)
      {
        for (size_t i = 0; i < _wavelengths.size(); i++)
        {
          const float rnd = mrf::sampling::RandomGenerator::Instance().getFloat();
          _context["wavelength_offset"]->setFloat(rnd);
          _wavelengths_per_passes[i][0] = (uint)
              mrf::math::_Math<float>::lerp((float)_wavelengths_bounds[i], (float)_wavelengths_bounds[i + 1], rnd);
        }
      }

      _context["rng_seed"]->setUint(mrf::sampling::RandomGenerator::Instance().getUI(UINT_MAX));
      if (_continuous_sampling)
        _context["wavelength_offset"]->setFloat(2.f);
      else
        _context["wavelength_offset"]->setFloat(0.f);

      mrf::util::PrecisionTimer timer;
      if (log_time) timer.start();

      // Only necessary if we have multiple spectral passes
      // otherwise everything fits on GPU memory and we can skip this update
      // AF: Should not be _num_pass > 1 ?
      if (_total_num_passes > 1)
      {
        if (_num_frame > 1)   // Skip this for the first frame. there is nothing to copy except zero
        {
#  ifdef MALIA_RENDERING_DEBUG
          std::cout << " AFTER FIRST FRAME. NEED TO COPY DATA FROM CPU GO GPU" << std::endl;
#  endif
        }
        //Could we skip this for first frame as well ?
        updateWavelengths();
        updateMaterials();
        updateLights();
        updateBackground();
      }

      if (log_time)
      {
        float elapsed = timer.elapsed();
        mrf::gui::fb::Loger::getInstance()->info("Update lights materials and background: time=", elapsed);
        timer.start();
      }

      // LAUNCH OPTIX RENDERING
      _context->launch(0, effective_width, effective_height);

      if (log_time)
      {
        float elapsed = timer.elapsed();
        mrf::gui::fb::Loger::getInstance()->info("render: time=", elapsed);
      }

#  ifdef MRF_RENDERING_DEBUG
      debugSpectralOptixBuffer("after_rendering_on_gpu", _num_pass, _num_frame);
#  endif
    }

#endif   //endif SPECTRAL mode
    if (_spatial_subdivision_factor == 1)
      ++_spp;
    else if (_num_frame % (_spatial_subdivision_factor * _spatial_subdivision_factor) == 0)
    {
      ++_spp;
    }
    ++_num_frame;
  }
  OPTIX_CATCH(_context->get())

  total_timer.stop();
  float elapsed = total_timer.elapsed();
  _elapsed_time += elapsed;
  return elapsed;
}

float OptixRenderer::renderSamples(int samples, size_t /*width*/, size_t /*height*/, int /*posX*/, int /*posY*/)
{
  // Convenient function to display time in H/m/s
  std::function<std::string(float)> time_str = [](float time_seconds) {
    std::stringstream s;

    if (time_seconds > 3600.f)
      s << int(time_seconds / 3600) << "H " << int(time_seconds) % 3600 / 60 << "min " << int(time_seconds) % 60 << "s";
    else if (time_seconds > 60.f)
      s << int(time_seconds) % 3600 / 60 << "min " << int(time_seconds) % 60 << "s";
    else
      s << int(time_seconds) % 60 << "s";

    return s.str();
  };

  const size_t              total_samples = (samples == 0) ? _max_samples : samples;
  mrf::util::PrecisionTimer total_timer;

  total_timer.reset();
  total_timer.start();

#ifdef MRF_RENDERING_MODE_SPECTRAL

  // ---------------------------------------------------------------------------
  // Spectral rendering mode
  // ---------------------------------------------------------------------------
  mrf::gui::fb::Loger::getInstance()->info("Rendering in spectral mode, spp: ", _max_samples);

  for (_num_pass = 0; _num_pass < _total_num_passes; _num_pass++)
  {
    const uint first_wave_idx = _num_pass * _nb_max_wavelengths_per_pass;
    const uint last_wave_idx  = first_wave_idx + (uint)_wavelengths_per_passes[_num_pass].size() - 1;
    //AF Remove: std::min<uint>((_num_pass + 1) * _nb_wavelengths_per_pass, uint(_wavelengths.size())) - 1;

    _context["first_wave_index"]->setUint(first_wave_idx);
    _context["last_wave_index"]->setUint(last_wave_idx);
    _context["num_samples_per_frame"]->setUint(_num_samples_per_frame);
    _context["nb_wavelengths"]->setUint((uint)_wavelengths_per_passes[_num_pass].size());

    //No subdivision in offline mode !
    setSpatialFactor(1);

    // Display info about this spectral pass
    std::stringstream ss_info_pass;
    ss_info_pass << "spectral pass: " << _num_pass + 1 << " / " << _total_num_passes << " - wavelengths: ";

    for (size_t i = 0; i < _wavelengths_per_passes[_num_pass].size(); i++)
      ss_info_pass << _wavelengths_per_passes[_num_pass][i] << "nm ";

    mrf::gui::fb::Loger::getInstance()->info(ss_info_pass.str());

    // Update assets
    updateWavelengths();

    // AF useless check? -> DM useful if multiple cameras and fully multiplexed
    if (_assets_update_required)
    {
      updateMaterials();
      updateLights();
      updateBackground();
    }

    mrf::util::PrecisionTimer pass_timer;
    pass_timer.start();

    float last_elapsed = 0.f;
    int   frame_number = 1;

    _spp = 0;

    for (uint sample_num = 0; sample_num < total_samples; sample_num += _num_samples_per_frame)
    {
      //No subdivision in offline mode !
      _context["frame_number"]->setUint(frame_number);
      _context["per_pixel_frame_number"]->setUint(frame_number);
      _context["rng_seed"]->setUint(mrf::sampling::RandomGenerator::Instance().getUI(UINT_MAX));

      if (_nb_max_wavelengths_per_pass == 1 && _continuous_sampling)
      {
        // TODO: this have to be cleaned up after deadline:
        // This is a shamefull way to subsample wavelength...
        // We rely on the class member value for updating materials but still need to restore it
        // when accumulating it.
        // Here, the kernel is dumb and we still have to take it back AT EACH sample.
        // Maybe an unified way of accumulation shall be discussed.
        const float toto = (float)_wavelengths_per_passes[_num_pass][0];

        const float rnd = mrf::sampling::RandomGenerator::Instance().getFloat();
        _context["wavelength_offset"]->setFloat(rnd);

        _wavelengths_per_passes[_num_pass][0] = (uint)mrf::math::_Math<float>::lerp(
            (float)_wavelengths_bounds[_num_pass],
            (float)_wavelengths_bounds[_num_pass + size_t(1)],
            rnd);

        updateMaterials();
        updateLights();
        updateBackground();

        // TODO: silently restoring the value, shame on me.
        _wavelengths_per_passes[_num_pass][0] = (uint)toto;
      }
      else
      {
        _context["wavelength_offset"]->setFloat(0.f);
      }

      _context->launch(0, _render_width, _render_height);

      ++frame_number;
      ++_spp;

      // Print advancement
      const float elapsed       = pass_timer.elapsed();
      const float total_elapsed = total_timer.elapsed();

      if (elapsed - last_elapsed > _seconds_between_log_advancement)
      {
        const uint  real_sample_num          = (sample_num == 0) ? _num_samples_per_frame : sample_num;
        const float percentage_done          = float(real_sample_num) / float(total_samples) * 100.f;
        const float time_per_percent         = elapsed / percentage_done;
        const float estimated_remaining_time = (100.f - percentage_done) * time_per_percent;
        const float estimated_time_per_pass
            = (_num_pass > 0) ? (total_elapsed - elapsed) / _num_pass : time_per_percent * 100.f;
        const float estimated_remaining_time_full_image
            = estimated_remaining_time + estimated_time_per_pass * (_total_num_passes - (_num_pass + 1));

        std::stringstream ss_adv;
        ss_adv << "spp done: " << sample_num << " / " << total_samples << " (" << percentage_done << "%)" << std::endl;
        ss_adv << "  pass time:         " << time_str(elapsed) << std::endl;
        ss_adv << "  total time:        " << time_str(total_elapsed) << std::endl;
        ss_adv << "  ETA spectral pass: " << time_str(estimated_remaining_time) << std::endl;
        ss_adv << "  ETA full image:    " << time_str(estimated_remaining_time_full_image);

        mrf::gui::fb::Loger::getInstance()->info(ss_adv.str());

        last_elapsed = elapsed;
      }

      if (_max_rendering_time_seconds > 0 && elapsed > _max_rendering_time_seconds)
      {
        std::stringstream ss_stop;
        ss_stop << "Stop rendering due to max rendering time reached." << std::endl;
        ss_stop << "  done: " << float(sample_num) / float(total_samples) * 100.f << "%";
        ss_stop << "  spp:  " << sample_num << std::endl;
        ss_stop << "  time: " << time_str(elapsed);

        mrf::gui::fb::Loger::getInstance()->info(ss_stop.str());

        break;
      }
    }   //end for-loop on samples per pass (-spf option controls this)

    if (_save_temporary_spectral_images)
    {
      std::stringstream ss_filename;
      ss_filename << "./malia_pass_" << _num_pass << ".exr";
      mrf::gui::fb::Loger::getInstance()->info("Saving image for pass #", _num_pass + 1);

      fetchImage();
      image::save(*_output_buffer_image, ss_filename.str());
    }

    pass_timer.stop();

    const float estimated_remaining_time
        = (_total_num_passes - (_num_pass + 1)) * (total_timer.elapsed() / (_num_pass + 1));

    std::stringstream ss_end;
    ss_end << "End rendering spectral pass" << std::endl;
    ss_end << "  elpased time:       " << time_str(pass_timer.elapsed()) << std::endl;
    ss_end << "  total elapsed time: " << time_str(total_timer.elapsed()) << std::endl;
    ss_end << "  ETA full image:     " << time_str(estimated_remaining_time);

    mrf::gui::fb::Loger::getInstance()->info(ss_end.str());

    // Show debug pixels values
#  ifdef DEBUG_SPECTRAL_PASSES
    for (mrf::uint num_debug_pixel = 0; num_debug_pixel < _debug_pixels.size(); num_debug_pixel++)
    {
      mrf::uint         num_pixel = _debug_pixels[num_debug_pixel];
      mrf::uint         y_pixel   = num_pixel / _render_width;
      mrf::uint         x_pixel   = num_pixel % _render_width;
      mrf::color::Color col       = getPixel(x_pixel, y_pixel);

      std::stringstream ss_deb;
      ss_deb << "Debug pixel: " << num_pixel << " (" << x_pixel << ", " << y_pixel << ") ";
      ss_deb << "[" << col.r() << ", " << col.g() << ", " << col.b() << "]";

      mrf::gui::fb::Loger::getInstance()->info(ss_deb.str());
    }
#  endif

  }   //End pass loop for all wavelengths

#else
  // ---------------------------------------------------------------------------
  // RGB rendering mode
  // ---------------------------------------------------------------------------
  mrf::gui::fb::Loger::getInstance()->info("Rendering in RGB mode, spp: ", _max_samples);

  setNumPass(0);
  setTotalNumPasses(1);

  // AF: necessary?
  if (_assets_update_required)
  {
    updateMaterials();
    updateLights();
    updateBackground();
  }

  mrf::util::PrecisionTimer pass_timer;
  pass_timer.start();
  float last_elapsed = 0.f;
  int   frame_number = 1;

  _context["num_samples_per_frame"]->setUint(_num_samples_per_frame);

  // No subdivision in offline mode !
  setSpatialFactor(1);
  _spp = 0;

  for (uint sample_num = 0; sample_num < total_samples; sample_num += _num_samples_per_frame)
  {
    //No subdivision in offline mode !
    _context["frame_number"]->setUint(frame_number);
    _context["per_pixel_frame_number"]->setUint(frame_number);
    _context->launch(0, _render_width, _render_height);

    ++frame_number;
    ++_spp;

    // Print advancement
    const float elapsed = pass_timer.elapsed();

    if (elapsed - last_elapsed > _seconds_between_log_advancement)
    {
      const uint  real_sample_num          = (sample_num == 0) ? _num_samples_per_frame : sample_num;
      const float percentage_done          = float(real_sample_num) / float(total_samples) * 100.f;
      const float time_per_percent         = elapsed / percentage_done;
      const float estimated_remaining_time = (100.f - percentage_done) * time_per_percent;

      std::stringstream ss_adv;
      ss_adv << "spp done: " << sample_num << " / " << total_samples << " (" << percentage_done << "%)" << std::endl;
      ss_adv << "  total time: " << time_str(elapsed) << std::endl;
      ss_adv << "  ETA:        " << time_str(estimated_remaining_time);

      mrf::gui::fb::Loger::getInstance()->info(ss_adv.str());

      last_elapsed = elapsed;
    }

    if (_max_rendering_time_seconds > 0 && elapsed > _max_rendering_time_seconds)
    {
      std::stringstream ss_stop;
      ss_stop << "Stop rendering due to max rendering time reached." << std::endl;
      ss_stop << "  spp:  " << sample_num << std::endl;
      ss_stop << "  done: " << float(sample_num) / float(total_samples) * 100.f << "%" << std::endl;
      ss_stop << "  time: " << time_str(elapsed);

      mrf::gui::fb::Loger::getInstance()->info(ss_stop.str());

      break;
    }
  }   //end for-loop on samples per pass (-spf option controls this)

#endif

  fetchImage();

  total_timer.stop();
  _elapsed_time = total_timer.elapsed();

  std::stringstream ss_end;
  ss_end << "Rendering finished - elapsed time: " << time_str(_elapsed_time) << " | (" << _elapsed_time << "s)";
  mrf::gui::fb::Loger::getInstance()->info(ss_end.str());


  return _elapsed_time;
}


#if defined(MRF_RENDERING_MODE_SPECTRAL) && defined(RENDERER_INTERACTIVE)
void OptixRenderer::convertOutputSpectralImageToRGBGPUBuffer(optix::Buffer buffer, bool log_time)
{
  mrf::util::PrecisionTimer timer;
  if (log_time) timer.start();

  //mrf::image::ColorImage xyz_image;

  const size_t w = _output_buffer_image->width();
  const size_t h = _output_buffer_image->height();

  std::array<mrf::data_struct::Array2D, 3> xyz;

  _spectrum_converter.emissiveSpectralImageToXYZImage(*_output_buffer_image, xyz[0], xyz[1], xyz[2]);

  if (log_time)
  {
    mrf::gui::fb::Loger::getInstance()->info(
        "Convert to XYZ, wavelengths size=",
        _output_buffer_image->wavelengths().size());
    mrf::gui::fb::Loger::getInstance()->info("Convert to XYZ, time=", timer.elapsed());
  }

  timer.start();

  void *imageData;
  RT_CHECK_ERROR(rtBufferMap(buffer->get(), &imageData));

  RTformat buffer_format;
  RT_CHECK_ERROR(rtBufferGetFormat(buffer->get(), &buffer_format));

  size_t nb_channels = 1;

  switch (buffer_format)
  {
    case RT_FORMAT_FLOAT:
      nb_channels = 1;
      break;

    case RT_FORMAT_FLOAT3:
      nb_channels = 3;
      break;

    case RT_FORMAT_FLOAT4:
      nb_channels = 4;
      break;

    default:
      break;
  }

#  pragma omp parallel for schedule(static)
  for (int j = int(h - 1); j >= 0; --j)
  {
    for (int i = 0; i < int(w); i++)
    {
      mrf::color::Color c(xyz[0](j, i), xyz[1](j, i), xyz[2](j, i));

      c *= std::exp2(_exposure);
      c = c.XYZtoLinearRGB();
      c = mrf::color::getGammaFunction()(c);

      float *display = ((float *)imageData) + (nb_channels * (w * (h - 1 - j) + i));

      display[0] = c.r() / _spp;
      display[1] = c.g() / _spp;
      display[2] = c.b() / _spp;
      if (nb_channels == 4) display[3] = 1.f;
    }
  }
  // Now unmap the buffer
  RT_CHECK_ERROR(rtBufferUnmap(buffer->get()));

  if (log_time) mrf::gui::fb::Loger::getInstance()->info("Convert to RGB, time=", timer.elapsed());
}
#endif

void OptixRenderer::updateWavelengths()
{
#ifndef MRF_RENDERING_MODE_SPECTRAL
  //TODO!!!!!!!!!!!!! Use correct wavelength ? Supposing there are correct wavelengths for RGB...
  _context["_wavelengths"]->setFloat(650.f, 550.f, 450.f);
#else
  if (_nb_max_wavelengths_per_pass > 1)
  {
    void *buffer_ptr = _context["_wavelengths"]->getBuffer()->map();
    //for (uint i = 0; i < _nb_wavelengths_per_pass; i++)
    //{
    //  static_cast<float *>(buffer_ptr)[i] = static_cast<float>(_wavelengths_per_passes[_num_pass][i]);
    //}
    for (uint i = 0; i < _wavelengths_bounds.size(); i++)
    {
      static_cast<float *>(buffer_ptr)[i] = static_cast<float>(_wavelengths_bounds[i]);
    }
    _context["_wavelengths"]->getBuffer()->unmap();
  }
  else
  {
    _context["_wavelengths"]->setFloat((float)_wavelengths_per_passes[_num_pass][0]);
  }
#endif
}

void OptixRenderer::updateMaterials()
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  // If multiplexing
  if (_nb_max_wavelengths_per_pass > 1)
  {
    _material_handler->updateMaterials(_wavelengths_per_passes_bounds[_num_pass]);
    //_material_handler->updateTextures(_wavelengths_per_passes_bounds[_num_pass]);
    //_material_handler->updateGlassCDF(_wavelengths_per_passes_bounds[_num_pass]);
  }
  else
  {
    _material_handler->updateMaterials(_wavelengths_per_passes[_num_pass]);
    //_material_handler->updateTextures(_wavelengths_per_passes[_num_pass]);
  }
#else
  //_material_handler->updateTextures();
  _material_handler->updateMaterials();
#endif

  mrf::gui::fb::Loger::getInstance()->debug("MAT update done");
  applyMaterials();

  mrf::gui::fb::Loger::getInstance()->debug("MAT apply");
}


void OptixRenderer::applyMaterials()
{
  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //  // If multiplexing
  //  if (_nb_max_wavelengths_per_pass > 1)
  //  {
  //    // Apply Material to all Optix::Mesh
  //    for (auto &shape : _geometry_handler->geometryInstances())
  //    {
  //      auto mesh = dynamic_cast<mrf::geom::Mesh *>((shape.second)->mesh().get());
  //
  //      if (mesh)
  //      {
  //        const uint material_id = (shape.second)->materialIndex();
  //        _material_handler->applyMaterial((shape.first.geom_instance), material_id);
  //      }
  //    }
  //
  //    // Now Apply Material for All Analytical Shapes
  //    for (auto &shape : _geometry_handler->analyticalShapes())
  //    {
  //      const uint material_id = shape.second;
  //      _material_handler->applyMaterial(shape.first, material_id);
  //    }
  //  }
  //  // Spectral Monochromatic
  //  else
  //  {
  //    // Apply Material to all Optix::Mesh
  //    for (auto &shape : _geometry_handler->geometryInstances())
  //    {
  //      auto mesh = dynamic_cast<mrf::geom::Mesh *>((shape.second)->mesh().get());
  //
  //      if (mesh)
  //      {
  //        const uint material_id = (shape.second)->materialIndex();
  //        _material_handler->applyMaterial((shape.first.geom_instance), material_id);
  //      }
  //    }
  //
  //    // Now Apply Material for All Analytical Shapes
  //    for (auto &shape : _geometry_handler->analyticalShapes())
  //    {
  //      const uint material_id = shape.second;
  //      _material_handler->applyMaterial(shape.first, material_id);
  //    }
  //  }
  //#else   // RGB mode
  // Apply Material to all Optix::Mesh
  for (auto &shape : _geometry_handler->geometryInstances())
  {
    auto mesh = dynamic_cast<mrf::geom::Mesh *>((shape.second)->mesh().get());

    if (mesh)
    {
      uint material_id = (shape.second)->materialIndex();
      _material_handler->applyMaterial(shape.first, material_id);
    }
  }

  // Now Apply Material for All Analytical Shapes
  for (auto &shape : _geometry_handler->analyticalShapes())
  {
    _material_handler->applyMaterial(shape.first, shape.second);
  }
  //#endif
}

void OptixRenderer::updateLights()
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  if (_nb_max_wavelengths_per_pass > 1)
  {
    _light_handler->updateLights(_material_handler[0], _wavelengths_per_passes_bounds[_num_pass]);
  }
  else
  {
    _light_handler->updateLights(_material_handler[0], _wavelengths_per_passes[_num_pass]);
  }
#else
  _light_handler->updateLights(_material_handler[0]);
#endif
}

void OptixRenderer::updateBackground()
{
  // Nothing to do here in RGB, everything to init envmapp system is already done in
  // OptixRenderer::importMrfScene(...)

#ifdef MRF_RENDERING_MODE_SPECTRAL
  //import background
  if (!_mrf_scene->hasBackground())
  {
    return;
  }

  mrf::rendering::BckgManager *     bck        = _mrf_scene->background();
  mrf::rendering::ColorBckgManager *color_bck  = dynamic_cast<mrf::rendering::ColorBckgManager *>(bck);
  mrf::lighting::EnvMap *           envmap_bck = dynamic_cast<mrf::lighting::EnvMap *>(bck);

  if (color_bck)
  {
    //color bck manager are uniform, does not depend on any direction
    auto color = color_bck->colorFromDirection(mrf::math::Vec3f());

    //if multiplexing
    if (_nb_max_wavelengths_per_pass > 1)
    {
      // TODO: Is this necessary ?
      RTsize buffer_size;
      _background_color_buffer->getSize(buffer_size);
      if (buffer_size != _nb_max_wavelengths_per_pass + 1)
      {
        _background_color_buffer->setSize(_nb_max_wavelengths_per_pass + 1);
      }
      // End TODO

      void * data       = _background_color_buffer->map();
      float *float_data = (float *)data;

      for (uint wave_id = 0; wave_id < _nb_max_wavelengths_per_pass + 1; wave_id++)
      {
        float_data[wave_id]
            = color.findLinearyInterpolatedValue(float(_wavelengths_per_passes_bounds[_num_pass][wave_id]));
      }
      _background_color_buffer->unmap();
    }
    else
    {
      uint  current_wavelength = _wavelengths[_num_pass];
      float val                = color.findLinearyInterpolatedValue(float(current_wavelength));
      _context["bg_color"]->setFloat(val);
    }
  }
  else if (envmap_bck)
  {
    // TODO : CHECK PERFORMANCE ISSUE HERE

    // First update envmap buffer values for this wavelength
    mrf::image::UniformSpectralImage const &usi = envmap_bck->image();

    // Should already have been created in OptixRenderer::importMrfScene()
    void *buffer_ptr = _envmap_buffer->map();

    mrf::radiometry::Spectrum spectrum;
    spectrum.wavelengths() = usi.wavelengths();

    //if multiplexing
    if (_nb_max_wavelengths_per_pass > 1)
    {
      // TODO : OpenMP ! ?
      // TODO: Super slow, find a better solution

      for (size_t i = 0; i < usi.height(); i++)
      {
        for (size_t j = 0; j < usi.width(); j++)
        {
          spectrum.values()      = usi.getPixel(j, i);
          const size_t pixel_num = (i * usi.width() + j) * (_nb_max_wavelengths_per_pass + 1);

          for (size_t channel = 0; channel < _nb_max_wavelengths_per_pass + 1; channel++)
          {
            float const value
                = spectrum.findLinearyInterpolatedValue(_wavelengths_per_passes_bounds[_num_pass][channel]);
            ((float *)buffer_ptr)[pixel_num + channel] = value * envmap_bck->powerScalingFactor();
          }
        }
      }
    }
    else
    {
      // TODO : OpenMP ! ?
      // TODO: Super slow, find a better solution
      for (size_t i = 0; i < usi.height(); i++)
      {
        for (size_t j = 0; j < usi.width(); j++)
        {
          spectrum.values()                = usi.getPixel(j, i);
          const size_t pixel_num           = (i * usi.width() + j);
          const float  value               = spectrum.findLinearyInterpolatedValue(_wavelengths[_num_pass]);
          ((float *)buffer_ptr)[pixel_num] = value * envmap_bck->powerScalingFactor();
        }
      }
    }
    _envmap_buffer->unmap();

    uint current_wavelength = _wavelengths[_num_pass];

    // Then update lum over horizon for shadow catcher
    mrf::lighting::PanoramicEnvMap *panoramic_envmap_bck = dynamic_cast<mrf::lighting::PanoramicEnvMap *>(bck);
    if (panoramic_envmap_bck)
    {
      float lum_over_horizon
          = _envmap_lum_over_horizon.findLinearyInterpolatedValue(static_cast<float>(current_wavelength));
      mrf::gui::fb::Loger::getInstance()->debug("Envmap average luminance over horizon = ", lum_over_horizon);
      _context["envmap_avg_lum_over_horizon"]->setFloat(lum_over_horizon);
    }
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(
          "Cannot get Envmap average luminance over horizon in updateBackground() ! Set it to 1.0");
      _context["envmap_avg_lum_over_horizon"]->setFloat(1.f);
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->warn("Optix renderer unsupported background manager detected !");
  }
#endif
}


void OptixRenderer::setRNGSeed(mrf::rendering::RENDERER_RNG seed)
{
  _rng_seed = seed;

  if (_rng_type == mrf::rendering::RENDERER_RNG::HALTON || _rng_type == mrf::rendering::RENDERER_RNG::SOBOL)
  {
    updateCranleyPattersonBuffer();
  }
}



float OptixRenderer::increaseRotationTheta(float delta)
{
  if (_envmap)
  {
    Renderer::increaseRotationTheta(delta);

    _envmap->updateRotationAngles(_envmap_rotation_phi, _envmap_rotation_theta, 0.f);

    _context["envmap_rotation_matrix"]->setMatrix3x3fv(false, _envmap->rotationMatrix().ptr());
    //TODO faster inverse by just using inverse angles ?
    _context["envmap_inv_rotation_matrix"]->setMatrix3x3fv(false, _envmap->rotationMatrix().inverse().ptr());

    return _envmap_rotation_theta;
  }
  return delta;
}


float OptixRenderer::increaseRotationPhi(float delta)
{
  if (_envmap)
  {
    Renderer::increaseRotationPhi(delta);

    _envmap->updateRotationAngles(_envmap_rotation_phi, _envmap_rotation_theta, 0.f);

    _context["envmap_rotation_matrix"]->setMatrix3x3fv(false, _envmap->rotationMatrix().ptr());
    //TODO faster inverse by just using inverse angles ?
    _context["envmap_inv_rotation_matrix"]->setMatrix3x3fv(false, _envmap->rotationMatrix().inverse().ptr());

    return _envmap_rotation_phi;
  }
  return delta;
}


}   // namespace optix_backend
}   // namespace mrf
