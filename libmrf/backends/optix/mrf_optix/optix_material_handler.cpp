/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/

#include <mrf_optix/optix_material_handler.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_util.hpp>
#include <mrf_optix/optix_renderer.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>

using mrf::optix_backend::mrfToOptix;

namespace mrf
{
namespace optix_backend
{
OptixMaterialHandler::OptixMaterialHandler(
    optix::Context &                           context,
    std::vector<mrf::materials::UMat *> const &mrf_materials)
  : _context(context)
  , _mrf_materials(mrf_materials)
{
  //register_all_mat(_plugins);
}

OptixMaterialHandler::~OptixMaterialHandler()
{
  /*
  **destroy render context already destroy everything
  releaseMemory();
  */
}

void OptixMaterialHandler::releaseMemory()
{
  for (auto it = _materials.begin(); it != _materials.end(); ++it)
  {
    auto mat = it->second;
    //mat->destroy();
    delete mat;
  }
}


std::vector<optix::Material> OptixMaterialHandler::getMaterialFromMRF(unsigned int index) const
{
  auto                         material = _mrf_materials[index];
  std::vector<optix::Material> ret_vec;
  std::vector<unsigned int>    indices;
  auto                         multi_mat = dynamic_cast<mrf::materials::MultiMaterial *>(material);
  if (multi_mat)
  {
    indices = multi_mat->getAllMRFId();
  }
  else
    indices.push_back(index);

  for (unsigned int i = 0; i < indices.size(); ++i)
  {
    material = _mrf_materials[indices[i]];
    ret_vec.push_back(_materials.at(material)->getMat());
  }

  return ret_vec;
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixMaterialHandler::createMaterials(
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
void OptixMaterialHandler::createMaterials(std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_cfg)
#endif
{
  try
  {
    for (int i = 0; i < _mrf_materials.size(); ++i)
    {
      //Multi material is a proxy, no need to compile a specific kernel.
      //All its underlying materials are in general material list and will be compiled normally.
      auto multi_mat = dynamic_cast<mrf::materials::MultiMaterial *>(_mrf_materials[i]);
      if (multi_mat)
      {
        updateMaterialIndex(multi_mat);
        continue;
      }

      if (_mrf_materials[i])
      {
        if (_plugins.find(_mrf_materials[i]->getType()) == _plugins.end())
        {
          mrf::gui::fb::Loger::getInstance()->warn("OptixMaterialHandler: Plugin " + _mrf_materials[i]->getType() + " not found");
          continue;
        }
#ifdef MRF_RENDERING_MODE_SPECTRAL
        auto cur_mat = _plugins[_mrf_materials[i]->getType()]->createMaterialFromMRF(
            _mrf_materials[i],
            _context,
            cuda_compile_options,
            ptx_cfg.toMatConfig(),
            wavelengths);
#else
        auto cur_mat = _plugins[_mrf_materials[i]->getType()]->createMaterialFromMRF(
            _mrf_materials[i],
            _context,
            cuda_compile_options,
            ptx_cfg.toMatConfig());
#endif
        if (cur_mat)
          _materials[_mrf_materials[i]] = cur_mat;
        else
          continue;
        _materials[_mrf_materials[i]]->compile(_context);
      }
    }
  }
  catch (optix::Exception &e)
  {
    std::cout << "[DEBUG] AT " << __FILE__ << " " << __LINE__ << std::endl;
    mrf::gui::fb::Loger::getInstance()->fatal(e.what());
    throw;
  }
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixMaterialHandler::updateMaterials(std::vector<uint> wavelengths)
{
  mrf::gui::fb::Loger::getInstance()->trace("Updating materials.");
  for (auto mat_entry : _materials)
  {
    if (!mat_entry.second) continue;
    mat_entry.second->update(_context, wavelengths);
    mat_entry.second->updateTextures(_context, wavelengths);
  }
}
#else
void OptixMaterialHandler::updateMaterials()
{
  for (auto mat_entry : _materials)
  {
    if (!mat_entry.second) continue;
    mat_entry.second->update(_context);
    mat_entry.second->updateTextures(_context);
  }
}
#endif

void OptixMaterialHandler::updateMaterialIndex(mrf::materials::MultiMaterial *multi_mat)
{
  optix::TextureSampler tex_sampler;

  if (multi_mat->hasTexture())
  {
    multi_mat->normalizeMapIndices("");
    if (multi_mat->getIndexMap().width() > 0 && multi_mat->getIndexMap().height() > 0)
    {
      auto it = _mat_index_maps.find(multi_mat);
      if (it == _mat_index_maps.end())
      {
        tex_sampler = mrf::optix_backend::createClampedOptixTexture(
            _context,
            RT_FORMAT_FLOAT4,
            multi_mat->getIndexMap().width(),
            multi_mat->getIndexMap().height());
        _mat_index_maps[multi_mat] = tex_sampler;

        mrf::optix_backend::updateTextureFromImage(multi_mat->getIndexMap(), tex_sampler);
      }
      else
      {
        tex_sampler = it->second;
      }
    }
  }
}


void OptixMaterialHandler::applyMaterial(optix::GeometryInstance &geom_instance, uint material_index)
{
  //  std::cout << " ENTERING " << __FILE__ << " " << __LINE__ << std::endl;

  auto                      material = _mrf_materials[material_index];
  std::vector<unsigned int> mrf_indices;
  std::vector<unsigned int> mesh_indices;
  auto                      multi_mat = dynamic_cast<mrf::materials::MultiMaterial *>(material);
  if (multi_mat)
  {
    mrf_indices = multi_mat->getAllMRFId();

    auto it = _mat_index_maps.find(multi_mat);
    if (it != _mat_index_maps.end())
    {
      geom_instance["index_tex_id"]->setInt(it->second->getId());
    }
    else
    {
      geom_instance["index_tex_id"]->setInt(-1);
    }
  }
  else
  {
    mrf_indices.push_back(material_index);
    geom_instance["index_tex_id"]->setInt(-1);
  }


  for (unsigned int i = 0; i < mrf_indices.size(); ++i)
  {
    material = _mrf_materials[mrf_indices[i]];
    geom_instance->setMaterial(i, _materials.at(material)->getMat());
  }
}



}   // namespace optix_backend
}   // namespace mrf
