/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * author : Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS 2020
 *
 *
 **/
#include <mrf_optix/optix_util.hpp>

#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/util/precision_timer.hpp>
#include <mrf_core/util/path.hpp>

#include <nvrtc.h>

#include <string>
#include <map>

using optix::Exception;

namespace mrf
{
namespace optix_backend
{
bool update1DTextureFromData(std::vector<float> data, optix::TextureSampler &texture)
{
  auto tex_buffer = texture->getBuffer();
  auto format     = tex_buffer->getFormat();

  if (format != RT_FORMAT_FLOAT) return false;

  std::vector<float> test;
  for (size_t i = 0; i < data.size(); ++i)
    test.push_back(i * 1.f);

  float *tex_data_ptr = static_cast<float *>(tex_buffer->map());
  //std::memcpy(tex_data_ptr, &test[0], data.size() * sizeof(float));
  std::memcpy(tex_data_ptr, &data[0], data.size() * sizeof(float));
  tex_buffer->unmap();

  return true;
}

bool updateTextureFromImage(mrf::image::ColorImage const &image, optix::TextureSampler &texture)
{
  auto tex_buffer = texture->getBuffer();
  auto format     = tex_buffer->getFormat();

  uint nb_channels;
  if (format == RT_FORMAT_FLOAT4)
  {
    nb_channels = 4;
  }
  else if (format == RT_FORMAT_FLOAT)
  {
    nb_channels = 1;
  }
  else
    return false;

  float *tex_data_ptr = static_cast<float *>(tex_buffer->map());
  int    i            = 0;
  for (size_t j = 0; j < image.width() * image.height(); j++)
  {
    const size_t pixel_x = j % image.width();
    //const size_t pixel_y   = image.height() - j / image.width() - 1;
    const size_t pixel_y   = j / image.width();
    const size_t pixel_num = pixel_x + pixel_y * image.width();

    const auto &color = image.getPixel(pixel_num);

    if (nb_channels == 4)
    {
      tex_data_ptr[i++] = color.r();
      tex_data_ptr[i++] = color.g();
      tex_data_ptr[i++] = color.b();
      tex_data_ptr[i++] = 1.0;
    }
    if (nb_channels == 1)
    {
      tex_data_ptr[i++] = color.r();
    }
  }
  tex_buffer->unmap();

  return true;
}

bool updateTextureFromSpectralImage(
    mrf::image::UniformSpectralImage const &image,
    optix::TextureSampler &                 texture,
    std::vector<uint>                       wavelengths)
{
  auto tex_buffer = texture->getBuffer();
  auto format     = tex_buffer->getFormat();

  if (format != RT_FORMAT_FLOAT)
  {
    return false;
  }

  RTsize tex_width, tex_height;
  tex_buffer->getSize(tex_width, tex_height);

  if (image.width() != tex_width || image.height() != tex_height) return false;

  float *tex_data_ptr = static_cast<float *>(tex_buffer->map());

  mrf::radiometry::Spectrum spectrum;
  spectrum.wavelengths() = image.wavelengths();

  /**
   *\todo
   *TODO LINEARY interpolate all the image at once
   **/
  for (size_t j = 0; j < image.width() * image.height(); ++j)
  {
    const size_t pixel_x = j % image.width();
    //const size_t pixel_y = image.height() - uint(j / image.width()) - 1;
    const size_t pixel_y = j / image.width();
    //uint pixel_num = pixel_x + pixel_y * image.width();

    spectrum.values() = image.getPixel(pixel_x, pixel_y);
    for (size_t wave_idx = 0; wave_idx < wavelengths.size(); ++wave_idx)
    {
      tex_data_ptr[j + wave_idx * image.width() * image.height()]
          = spectrum.findLinearyInterpolatedValue(wavelengths[wave_idx]);
    }
  }

  tex_buffer->unmap();
  return true;
}


optix::TextureSampler
createClampedOptixTexture(optix::Context &context, RTformat format, size_t width, size_t height, size_t depth)
{
  /* Create the buffer that represents the texture data */
  optix::Buffer tex_buffer;
  if (height == 0)
    tex_buffer = context->createBuffer(RT_BUFFER_INPUT, format, width);
  else if (depth == 0)
    tex_buffer = context->createBuffer(RT_BUFFER_INPUT, format, width, height);
  else
    tex_buffer = context->createBuffer(RT_BUFFER_INPUT, format, width, height, depth);

  /* Create the texture sampler */
  auto tex_sampler = context->createTextureSampler();
  tex_sampler->setWrapMode(0, RT_WRAP_CLAMP_TO_EDGE);
  tex_sampler->setWrapMode(1, RT_WRAP_CLAMP_TO_EDGE);
  tex_sampler->setFilteringModes(RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE);
  tex_sampler->setIndexingMode(RT_TEXTURE_INDEX_NORMALIZED_COORDINATES);
  tex_sampler->setReadMode(RT_TEXTURE_READ_NORMALIZED_FLOAT);
  tex_sampler->setMaxAnisotropy(1.f);
  tex_sampler->setMipLevelCount(1);
  tex_sampler->setArraySize(1);
  tex_sampler->setBuffer(0, 0, tex_buffer);

  return tex_sampler;
}

optix::TextureSampler
createRepeatedOptixTexture(optix::Context &context, RTformat format, size_t width, size_t height, size_t depth)
{
  /* Create the buffer that represents the texture data */
  optix::Buffer tex_buffer;
  if (height == 0)
    tex_buffer = context->createBuffer(RT_BUFFER_INPUT, format, width);
  else if (depth == 0)
    tex_buffer = context->createBuffer(RT_BUFFER_INPUT, format, width, height);
  else
    tex_buffer = context->createBuffer(RT_BUFFER_INPUT, format, width, height, depth);

  /* Create the texture sampler */
  auto tex_sampler = context->createTextureSampler();
  tex_sampler->setWrapMode(0, RT_WRAP_REPEAT);
  tex_sampler->setWrapMode(1, RT_WRAP_REPEAT);
  tex_sampler->setFilteringModes(RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE);
  tex_sampler->setIndexingMode(RT_TEXTURE_INDEX_NORMALIZED_COORDINATES);
  tex_sampler->setReadMode(RT_TEXTURE_READ_NORMALIZED_FLOAT);
  tex_sampler->setMaxAnisotropy(1.f);
  tex_sampler->setMipLevelCount(1);
  tex_sampler->setArraySize(1);
  tex_sampler->setBuffer(0, 0, tex_buffer);

  return tex_sampler;
}




static bool readSourceFile(std::string &str, const std::string &filename)
{
  // Try to open file
  std::ifstream file(filename.c_str());
  if (file.good())
  {
    // Found usable source file
    std::stringstream source_buffer;
    source_buffer << file.rdbuf();
    str = source_buffer.str();
    return true;
  }
  return false;
}

void calculateCameraVariables(
    optix::float3  eye,
    optix::float3  lookat,
    optix::float3  up,
    float          fov,
    float          aspect_ratio,
    optix::float3 &U,
    optix::float3 &V,
    optix::float3 &W,
    bool           fov_is_vertical)
{
  float ulen, vlen;
  W = lookat - eye;

  //in older version we were computing focal length from
  //W = lookat - eye;
  //focal_length = length(W)

  W = normalize(W);

  U = normalize(cross(W, up));
  V = normalize(cross(U, W));

  if (fov_is_vertical)
  {
    vlen = tanf(0.5f * fov * M_PIf / 180.0f);
    V *= vlen;
    ulen = vlen * aspect_ratio;
    U *= ulen;
  }
  else
  {
    ulen = tanf(0.5f * fov * M_PIf / 180.0f);
    U *= ulen;
    vlen = ulen / aspect_ratio;
    V *= vlen;
  }
}

void applyFov(
    optix::float3 &U,
    optix::float3 &V,
    const float &  fov,
    const float &  aspect_ratio,
    const bool &   fov_is_vertical)
{
  float ulen, vlen;
  if (fov_is_vertical)
  {
    vlen = tanf(0.5f * fov * M_PIf / 180.0f);
    V *= vlen;
    ulen = vlen * aspect_ratio;
    U *= ulen;
  }
  else
  {
    ulen = tanf(0.5f * fov * M_PIf / 180.0f);
    U *= ulen;
    vlen = ulen / aspect_ratio;
    V *= vlen;
  }
}

bool optixBufferToImage(optix::Buffer buffer, mrf::image::ColorImage &image)
{
  auto src_buffer = buffer->get();
  int  width, height;
  //int  depth;
  RTsize buffer_width, buffer_height, buffer_depth;

  void *imageData;
  RT_CHECK_ERROR(rtBufferMap(src_buffer, &imageData));

  RT_CHECK_ERROR(rtBufferGetSize3D(src_buffer, &buffer_width, &buffer_height, &buffer_depth));
  width  = static_cast<int>(buffer_width);
  height = static_cast<int>(buffer_height);
  //depth = static_cast<int>(buffer_depth);

  image.init(width, height);

  RTformat buffer_format;
  RT_CHECK_ERROR(rtBufferGetFormat(src_buffer, &buffer_format));

  // RP: NEVER USED byteSizePerPixel (even if assigned in the switch)
  //int byteSizePerPixel = 1;
  int nbComp = 1;
  switch (buffer_format)
  {
    case RT_FORMAT_UNSIGNED_BYTE4:
    {
      nbComp = 4;
      break;
    }
    case RT_FORMAT_FLOAT:
    {
      nbComp = 1;
      break;
    }
    case RT_FORMAT_FLOAT3:
    {
      nbComp = 3;
      break;
    }
    case RT_FORMAT_FLOAT4:
    default:
    {
      nbComp = 4;
      break;
    }
  }

  for (int j = height - 1; j >= 0; --j)
  {
    for (int i = 0; i < width; ++i)
    {
      mrf::color::Color c;
      float *           display = ((float *)imageData) + (nbComp * (width * (height - 1 - j) + i));
      for (int k = 0; k < 3; ++k)
        c[k] = display[k];
      image.setPixel(i, j, c);
    }
  }

  //std::memcpy((void*)(image.ptr()), imageData, width*height*byteSizePerPixel);

  // Now unmap the buffer
  RT_CHECK_ERROR(rtBufferUnmap(src_buffer));

  return true;
}

bool optixBufferToSpectralImage(
    optix::Buffer                     buffer,
    mrf::image::UniformSpectralImage &image,
    std::vector<uint>                 wavelengths,
    uint                              n_samples)
{
  void * imageData  = nullptr;
  auto   src_buffer = buffer->get();
  RTsize buffer_width, buffer_height, buffer_depth;
  // RTformat buffer_format;

  RT_CHECK_ERROR(rtBufferMap(src_buffer, &imageData));
  RT_CHECK_ERROR(rtBufferGetSize3D(src_buffer, &buffer_width, &buffer_height, &buffer_depth));
  // RT_CHECK_ERROR(rtBufferGetFormat(src_buffer, &buffer_format));

  const size_t width  = static_cast<size_t>(buffer_width);
  const size_t height = static_cast<size_t>(buffer_height);
  const size_t depth  = static_cast<size_t>(buffer_depth);

  image.allocateMemory(width, height, wavelengths);

  for (size_t wave_idx = 0; wave_idx < wavelengths.size(); wave_idx++)
  {
    mrf::data_struct::Array2D &image_channel = image.data()[wave_idx];

    for (size_t y = 0; y < height; y++)
    {
      const float *src = ((float *)imageData) + (width * ((height - 1) - y) + wave_idx * width * height);
      memcpy(&image_channel(width * y), src, sizeof(float) * width);
    }

    image_channel /= (float)n_samples;
  }

  // Now unmap the buffer
  RT_CHECK_ERROR(rtBufferUnmap(src_buffer));

  return true;
}

void reportErrorMessage(const char *message)
{
  std::cerr << "OptiX Error: '" << message << "'\n";
#if defined(_WIN32) && defined(RELEASE_PUBLIC)
  {
    char s[2048];
    sprintf(s, "OptiX Error: %s", message);
    MessageBox(0, s, "OptiX Error", MB_OK | MB_ICONWARNING | MB_SYSTEMMODAL);
  }
#endif
}


void handleError(RTcontext context, RTresult code, const char *file, int line)
{
  const char *message;
  char        s[2048];
  rtContextGetErrorString(context, code, &message);
  sprintf(s, "%s\n(%s:%d)", message, file, line);
  reportErrorMessage(s);
}


}   // namespace optix_backend
}   // namespace mrf
