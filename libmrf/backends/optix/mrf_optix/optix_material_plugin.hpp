/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_kernel.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <vector>
#include <map>


namespace mrf
{
namespace optix_backend
{
class MRF_OPTIX_EXPORT OptixBaseMaterial
{
public:
  OptixBaseMaterial(optix::Material mat);
  OptixBaseMaterial(std::vector<std::string> const &cuda_compile_options);
  OptixBaseMaterial(optix::Material mat, std::vector<std::string> const &cuda_compile_options);
  OptixBaseMaterial(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options);

  virtual ~OptixBaseMaterial();

  virtual void compile(optix::Context &context);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void update(optix::Context &context, std::vector<uint> wavelengths);
#else
  virtual void update(optix::Context &context);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &context, std::vector<uint> wavelengths) = 0;
#else
  virtual void updateTextures(optix::Context &context) = 0;
#endif

  void setMRFMat(mrf::materials::UMat *mat) { _mrf_material = mat; }

  optix::Material &getMat() { return _optix_mat; }

  //WARNING: no "null" value can be return as optix::Buffer. hasBuffer must be used before calling getBuffer.
  optix::Buffer &getBuffer(std::string key) { return _buffers[key]; }


  //WARNING: no "null" value can be return as optix::TextureSampler. hasTexture must be used before calling getTexture.
  optix::TextureSampler &getTexture(std::string key) { return _textures[key]; }

  virtual bool needMeshRefine() = 0;

  bool hasBuffer(std::string key) { return _buffers.find(key) != _buffers.end(); }

  bool hasTexture(std::string key) { return _textures.find(key) != _textures.end(); }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  void updateRadianceGPUBuffer(
      optix::Context &                                  context,
      std::vector<mrf::materials::RADIANCE_TYPE> const &radiances,
      std::vector<std::string> const &                  variable_names,
      std::vector<uint> const &                         wavelengths);
  void setCustomRadianceVariables(
      optix::Context &                                  context,
      std::vector<mrf::materials::RADIANCE_TYPE> const &radiance_values,
      std::vector<std::string> const &                  variable_names,
      std::vector<uint> const &                         wavelengths);
#else
  void         setCustomRadianceVariables(
              std::vector<mrf::materials::RADIANCE_TYPE> const &radiance_values,
              std::vector<std::string> const &                  variable_names);
#endif

protected:
  mrf::materials::UMat *_mrf_material;
  optix::Material       _optix_mat;

  std::map<std::string, optix::TextureSampler> _textures;
  std::map<std::string, optix::Buffer>         _buffers;

  std::vector<mrf::materials::RADIANCE_TYPE> _variable_values;
  std::vector<std::string>                   _variable_names;

  OptixKernel *_program;

  std::string _kernel_path;

  std::vector<std::string> _cuda_compile_options;
};

class MRF_OPTIX_EXPORT OptixBRDFMaterial: public OptixBaseMaterial
{
public:
  OptixBRDFMaterial(optix::Material mat): OptixBaseMaterial(mat) { _program = new OptixKernel("BRDF_kernel.cu"); }
  OptixBRDFMaterial(std::vector<std::string> const &cuda_compile_options): OptixBaseMaterial(cuda_compile_options) {}
  OptixBRDFMaterial(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixBaseMaterial(mat, cuda_compile_options)
  {
    _program = new OptixKernel("BRDF_kernel.cu", cuda_compile_options);
  }
  OptixBRDFMaterial(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options)
    : OptixBaseMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _program = new OptixKernel("BRDF_kernel.cu", cuda_compile_options);
  }

  OptixBRDFMaterial(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
    : OptixBaseMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _program = new OptixKernel("BRDF_kernel.cu", cuda_compile_options, ptx_cfg);
  }

  ~OptixBRDFMaterial() { _optix_mat->destroy(); }

  virtual void compile(optix::Context &context);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void update(optix::Context &context, std::vector<uint> wavelengths);
#else
  virtual void               update(optix::Context &context);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &context, std::vector<uint> wavelengths) = 0;
#else
  virtual void               updateTextures(optix::Context &context) = 0;
#endif


  virtual bool needMeshRefine() { return false; }
};

class MRF_OPTIX_EXPORT OptixBSDFMaterial: public OptixBaseMaterial
{
public:
  OptixBSDFMaterial(optix::Material mat): OptixBaseMaterial(mat) { _program = new OptixKernel("BSDF_kernel.cu"); }
  OptixBSDFMaterial(std::vector<std::string> const &cuda_compile_options): OptixBaseMaterial(cuda_compile_options)
  {
    _program = new OptixKernel("BSDF_kernel.cu", cuda_compile_options);
  }
  OptixBSDFMaterial(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixBaseMaterial(mat, cuda_compile_options)
  {
    _program = new OptixKernel("BSDF_kernel.cu", cuda_compile_options);
  }
  OptixBSDFMaterial(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options)
    : OptixBaseMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _program = new OptixKernel("BSDF_kernel.cu", cuda_compile_options);
  }

  OptixBSDFMaterial(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
    : OptixBaseMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _program = new OptixKernel("BSDF_kernel.cu", cuda_compile_options, ptx_cfg);
  }

  ~OptixBSDFMaterial() { _optix_mat->destroy(); }

  virtual void compile(optix::Context &context);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void update(optix::Context &context, std::vector<uint> wavelengths);
#else
  virtual void               update(optix::Context &context);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &context, std::vector<uint> wavelengths) = 0;
#else
  virtual void               updateTextures(optix::Context &context) = 0;
#endif

  virtual bool needMeshRefine() { return true; }
};

class MRF_OPTIX_EXPORT OptixEmittanceMaterial: public OptixBaseMaterial
{
public:
  OptixEmittanceMaterial(optix::Material mat): OptixBaseMaterial(mat)
  {
    _program = new OptixKernel("diffuse_emitter.cu");
  }
  OptixEmittanceMaterial(std::vector<std::string> const &cuda_compile_options): OptixBaseMaterial(cuda_compile_options)
  {
    _program = new OptixKernel("diffuse_emitter.cu", cuda_compile_options);
  }
  OptixEmittanceMaterial(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixBaseMaterial(mat, cuda_compile_options)
  {
    _program = new OptixKernel("diffuse_emitter.cu", cuda_compile_options);
  }
  OptixEmittanceMaterial(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options)
    : OptixBaseMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _program = new OptixKernel("diffuse_emitter.cu", cuda_compile_options);
  }
  OptixEmittanceMaterial(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
    : OptixBaseMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _program = new OptixKernel("diffuse_emitter.cu", cuda_compile_options, ptx_cfg);
  }

  ~OptixEmittanceMaterial() { _optix_mat->destroy(); }

  virtual void compile(optix::Context &context);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void update(optix::Context &context, std::vector<uint> wavelengths);
#else
  virtual void               update(optix::Context &context);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &context, std::vector<uint> wavelengths) = 0;
#else
  virtual void               updateTextures(optix::Context &context) = 0;
#endif

  virtual bool needMeshRefine() { return false; }
};

///////////////////////////////////////////////////////////////////////////////////
////////////////PLUGINS////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

class MRF_OPTIX_EXPORT OptixMaterialPlugin
{
public:
  OptixMaterialPlugin() {}
  ~OptixMaterialPlugin() {}

  static OptixMaterialPlugin *create() { return nullptr; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg,
      std::vector<uint>               wavelengths)
      = 0;
#else
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
      = 0;
#endif

private:
};

struct OptixMaterialPlugins
{
  std::map<std::string, OptixMaterialPlugin *> fcts;
};



}   // namespace optix_backend
}   // namespace mrf
