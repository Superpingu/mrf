/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend optix
 * Optix light handling
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_material_plugin.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <vector>
#include <map>


namespace mrf
{
namespace optix_backend
{
struct OptiXMesh;

class MRF_OPTIX_EXPORT OptixMaterialHandler
{
public:
  OptixMaterialHandler(optix::Context &context, std::vector<mrf::materials::UMat *> const &mrf_materials);
  ~OptixMaterialHandler();

  void loadPlugins(OptixMaterialPlugins &plugins) { _plugins = plugins.fcts; }

  void releaseMemory();
#ifdef MRF_RENDERING_MODE_SPECTRAL
  void createMaterials(
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_config,
      std::vector<uint>               wavelengths);
#else
  void createMaterials(std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_config);
#endif

  //optix::Material getMaterialFromMRF(unsigned int index);
  std::vector<optix::Material> getMaterialFromMRF(unsigned int index) const;

  optix::Material getMaterialFromUMat(mrf::materials::UMat *material) const
  {
    return _materials.at(material)->getMat();
  }
  //optix::Material & getMaterialFromUMat(mrf::materials::UMat *material) { return _materials.at(material); }

  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //  OptixBaseMaterial *createMaterialFromMRF(
  //      mrf::materials::UMat *          material,
  //      std::vector<std::string> const &cuda_compile_options,
  //      std::vector<uint>               wavelengths);
  //#else
  //  OptixBaseMaterial *
  //       createMaterialFromMRF(mrf::materials::UMat *material, std::vector<std::string> const &cuda_compile_options);
  //#endif

  //void compileBRDF(optix::Material &mat, std::vector<std::string> const &cuda_compile_options);
  //void compileBSDF(optix::Material &mat, std::vector<std::string> const &cuda_compile_options);
  //void compileEmittance(optix::Material &mat, std::vector<std::string> const &cuda_compile_options);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  /**
   * Update all the Optix texture that holds the data of the PhongTextured materials
   * We do this in a separate call to do it only once per material.
   * Doing it in the applyMaterial method would have do it more than once because a
   * material can be use by several shapes and the  applyMaterial is called for each shape
   **/
  void updateMaterials(std::vector<uint> wavelengths);

#else
  void updateMaterials();
#endif

  void applyMaterial(optix::GeometryInstance &geom_instance, mrf::uint material_index);
  void updateMaterialIndex(mrf::materials::MultiMaterial *multi_mat);


  mrf::optix_backend::OptixBaseMaterial *getMaterial(uint material_id)
  {
    auto mat = _mrf_materials[material_id];
    return _materials[mat];
  }


private:
  /**
   * reference to optix context
   **/
  optix::Context &_context;
  /**
   * Reference to mrf materials
   * no need to deallocate memory
   * \todo use a shared pointer
   **/
  std::vector<mrf::materials::UMat *> const &_mrf_materials;

  std::map<mrf::materials::UMat *, mrf::optix_backend::OptixBaseMaterial *> _materials;

  std::map<mrf::materials::UMat *, optix::TextureSampler> _mat_index_maps;

  std::map<std::string, OptixMaterialPlugin *> _plugins;
};


}   // namespace optix_backend
}   // namespace mrf
