/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend iray renderer
 * iray material handling
 *
 **/
#pragma once

#include <mrf/mrf_dll.hpp>
#include <mrf/mrf_types.hpp>

#ifdef IRAY_INSTALL_DIR

#  include <mi/neuraylib.h>

#  ifdef MI_PLATFORM_WINDOWS
#    include <mi/base/miwindows.h>
#  else
#    include <dlfcn.h>
#    include <unistd.h>
#  endif

#  include <vector>

#  include <mrf/,aterials/umat.hpp>
#  include "gui/feedback/loger.hpp"

namespace mrf
{
namespace iray
{
MRF_EXPORT class IrayMaterialHandler
{
public:
  IrayMaterialHandler(std::vector<mrf::materials::UMat *> const &mrf_materials, mrf::gui::fb::Loger const &loger);
  void createMaterials(
      mi::base::Handle<mi::neuraylib::INeuray> &     neuray,
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction);

  bool updateMaterials(
      mi::base::Handle<mi::neuraylib::IMdl_factory> &mdl_factory,
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      uint                                           num_rendering_pass,
      std::vector<uint> const &                      spectral_wavelengths);
  bool updateColor(
      mi::neuraylib::Argument_editor &mdl_material,
      float const &                   factor,
      mrf::materials::COLOR const &   color,
      mrf::uint const &               num_rendering_pass,
      std::vector<uint> const &       spectral_wavelengths,
      std::string const &             iray_material_parameter,
      std::string const &             material_type);
  bool updateIor(
      mi::neuraylib::Argument_editor &mdl_material,
      mrf::materials::COLOR const &   eta,
      mrf::materials::COLOR const &   kappa,
      mrf::uint const &               num_rendering_pass,
      std::vector<uint> const &       spectral_wavelengths,
      std::string const &             iray_material_parameter,
      std::string const &             material_type);
  bool applyMaterial(mi::base::Handle<mi::neuraylib::IInstance> &mesh_instance, uint material_id);

private:
  std::vector<mrf::materials::UMat *> const &_mrf_materials;
  mrf::gui::fb::Loger const &                _loger;
  std::vector<std::string>                   _iray_material_names;
};


}   // namespace iray
}   // namespace mrf

#endif