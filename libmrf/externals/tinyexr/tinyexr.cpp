/*
*
* author : Arthur Dufay @ inria.fr
* Copyright INRIA 2017
*
*
**/

//this need to be done exactly one time
#define TINYEXR_IMPLEMENTATION
#include "externals/tinyexr/tinyexr.h"