/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <externals/termcolor/termcolor.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

namespace mrf
{
namespace gui
{
namespace fb
{
//Singleton class for logging info.
class MRF_CORE_EXPORT Loger
{
public:
  enum LEVEL
  {
    No_Log  = 0,
    Fatal   = 1,
    Warning = 2,
    Info    = 3,
    Trace   = 4,
    Debug   = 5
  };

protected:
  std::string _fatal;
  std::string _debug;
  std::string _warning;
  std::string _info;
  std::string _stats;

  //Token
  std::string _rstroke;
  std::string _stars;

  LEVEL _log_mode;

  static Loger *s_loger;

public:
  //------------------------------------------------------------------//
  inline Loger(LEVEL mode = No_Log);
  inline ~Loger();
  Loger(Loger const &) = delete;
  Loger &operator=(Loger const &) = delete;

  static Loger *getInstance();
  static Loger *getInstance(LEVEL mode);

  inline LEVEL level() const;

  //------------------------------------------------------------------//
  /**
   * This always Displayed.
   */
  inline void message(std::string const &a_message);


  //------------------------------------------------------------------//
  inline std::string const &fatal() const;
  inline void               fatal(char const *message) const;
  inline void               fatal(std::string const &message) const;

  template<typename T>
  inline void fatal(std::string const &message, T const &variable) const;

  template<typename T>
  inline void fatal(char const *message, T const &variable) const;


  //------------------------------------------------------------------//
  inline std::string const &debug() const;
  inline void               debug(std::string const &message) const;
  inline void               debug(char const *message) const;

  template<typename T>
  inline void debug(std::string const &message, T const &variable) const;

  template<typename T>
  inline void debug(char const *message, T const &variable) const;


  //------------------------------------------------------------------//
  inline std::string const &warn() const;
  inline void               warn(char const *message) const;
  inline void               warn(std::string const &message) const;

  template<typename T>
  inline void warn(std::string const &message, T const &variable) const;

  template<typename T>
  inline void warn(char const *message, T const &variable) const;


  //------------------------------------------------------------------//
  inline std::string const &info() const;
  inline void               info(char const *message) const;
  inline void               info(std::string const &message) const;

  template<typename T>
  inline void info(std::string const &message, T const &variable) const;

  template<typename T>
  inline void info(char const *message, T const &variable) const;


  //------------------------------------------------------------------//
  inline std::string const &trace() const;
  inline void               trace(std::string const &message) const;
  inline void               trace(char const *message) const;

  template<typename T>
  inline void trace(std::string const &message, T const &variable) const;

  template<typename T>
  inline void trace(char const *message, T const &variable) const;


  //------------------------------------------------------------------//
  inline std::string const &log() const;
  inline void log(std::string const &message, std::string const &FILE, unsigned int LINE_NUMBER, LEVEL mode = Info);

  inline void log(std::string const &message, LEVEL mode = Info) const;
  inline void log(char const *message, LEVEL mode = Info) const;

protected:
  inline void printLogLevel(LEVEL mode) const;
  inline void printDelimiterStart(LEVEL log_level) const;
  inline void printDelimiterEnd() const;
};


inline void Loger::message(std::string const &a_message)
{
  std::cout << a_message << std::flush;
}


//FATAL Methods
inline std::string const &Loger::fatal() const
{
  return _fatal;
}

inline void Loger::fatal(char const *message) const
{
  if (_log_mode >= Fatal)
  {
    printLogLevel(Fatal);
    std::cout << _rstroke;
    std::cout << std::string(message).c_str() << termcolor::reset << std::endl;
  }
}


inline void Loger::fatal(std::string const &message) const
{
  fatal(message.c_str());
}

template<typename T>
inline void Loger::fatal(char const *message, T const &variable) const
{
  ;
  if (_log_mode >= Fatal)
  {
    printLogLevel(Fatal);
    std::cout << _rstroke;
    std::cout << std::string(message) << variable << termcolor::reset << std::endl;
  }
}

template<typename T>
inline void Loger::fatal(std::string const &message, T const &variable) const
{
  fatal(message.c_str(), variable);
}

//Debug Methods
inline std::string const &Loger::debug() const
{
  return _debug;
}

inline void Loger::debug(char const *message) const
{
  if (_log_mode >= Debug)
  {
    printLogLevel(Debug);
    std::cout << _rstroke;
    std::cout << std::string(message).c_str() << termcolor::reset << std::endl;
  }
}

inline void Loger::debug(std::string const &message) const
{
  debug(message.c_str());
}


template<typename T>
inline void Loger::debug(char const *message, T const &variable) const
{
  if (_log_mode >= Debug)
  {
    printLogLevel(Debug);
    std::cout << _rstroke;
    std::cout << std::string(message) << variable << termcolor::reset << std::endl;
  }
}

template<typename T>
inline void Loger::debug(std::string const &message, T const &variable) const
{
  debug(message.c_str(), variable);
}

//Warn Methods
inline std::string const &Loger::warn() const
{
  return _warning;
}

inline void Loger::warn(char const *message) const
{
  if (_log_mode >= Warning)
  {
    printLogLevel(Warning);
    std::cout << _rstroke;
    std::cout << std::string(message).c_str() << termcolor::reset << std::endl;
  }
}

inline void Loger::warn(std::string const &message) const
{
  warn(message.c_str());
}

template<typename T>
inline void Loger::warn(char const *message, T const &variable) const
{
  if (_log_mode >= Warning)
  {
    printLogLevel(Warning);
    std::cout << _rstroke;
    std::cout << std::string(message) << variable << termcolor::reset << std::endl;
  }
}

template<typename T>
inline void Loger::warn(std::string const &message, T const &variable) const
{
  warn(message.c_str(), variable);
}


//Trace methods
inline std::string const &Loger::trace() const
{
  return _stats;
}

inline void Loger::trace(char const *message) const
{
  if (_log_mode >= Trace)
  {
    printLogLevel(Trace);
    std::cout << _rstroke;
    std::cout << std::string(message).c_str() << termcolor::reset << std::endl;
  }
}

inline void Loger::trace(std::string const &message) const
{
  trace(message.c_str());
}

template<typename T>
inline void Loger::trace(char const *message, T const &variable) const
{
  if (_log_mode >= Trace)
  {
    printLogLevel(Trace);
    std::cout << _rstroke;
    std::cout << std::string(message) << variable << termcolor::reset << std::endl;
  }
}

template<typename T>
inline void Loger::trace(std::string const &message, T const &variable) const
{
  trace(message.c_str(), variable);
}

//Info methods
inline std::string const &Loger::info() const
{
  return _info;
}

inline void Loger::info(char const *message) const
{
  //TODO: print the bracket or arrow
  if (_log_mode >= Info)
  {
    printLogLevel(Info);
    std::cout << _rstroke;
    std::cout << std::string(message).c_str() << termcolor::reset << std::endl;
  }
}

inline void Loger::info(std::string const &message) const
{
  info(message.c_str());
}

template<typename T>
inline void Loger::info(char const *message, T const &variable) const
{
  if (_log_mode >= Info)
  {
    printLogLevel(Info);
    std::cout << _rstroke;
    std::cout << std::string(message) << variable << termcolor::reset << std::endl;
  }
}

template<typename T>
inline void Loger::info(std::string const &message, T const &variable) const
{
  info(message.c_str(), variable);
}



//------------------------------------------------------------------
//GENERIC Output methods
//------------------------------------------------------------------
inline void Loger::log(std::string const &message, std::string const &FILE, unsigned int LINE_NUMBER, LEVEL log_level)
{
  if (_log_mode >= log_level)
  {
    printDelimiterStart(_log_mode);

    std::cout << " Log IN " << FILE << "  AT " << LINE_NUMBER << std::endl;
    std::cout << message << std::endl;

    printDelimiterEnd();
  }
}


inline void Loger::log(std::string const &message, LEVEL /*log_level*/) const
{
  log(message.c_str());
}

inline void Loger::log(const char *message, LEVEL log_level) const
{
  if (_log_mode >= log_level)
  {
    printDelimiterStart(_log_mode);

    std::cout << message << std::endl;

    printDelimiterEnd();
  }
}

inline void Loger::printLogLevel(LEVEL log_level) const
{
  switch (log_level)
  {
    case Fatal:
    {
      std::cout << termcolor::redIntensified << _fatal;
      break;
    }

    case Debug:
    {
      std::cout << termcolor::blueIntensified << _debug;
      break;
    }

    case Warning:
    {
      std::cout << termcolor::yellow << _warning;
      break;
    }

    case Info:
    {
      std::cout << termcolor::green << _info;
      break;
    }

    case Trace:
    {
      std::cout << _stats;
      break;
    }

    case No_Log:
    {
      break;
    }
    default:
      break;
  }
}

inline void Loger::printDelimiterStart(LEVEL log_level) const
{
  std::cout << _stars;
  std::cout << _rstroke;
  printLogLevel(log_level);
}

inline void Loger::printDelimiterEnd() const
{
  std::cout << _stars;
}


//Constructors
inline Loger::Loger(LEVEL mode)
  : _fatal("[FATAL] ")
  , _debug("[DEBUG] ")
  , _warning("[WARNING] ")
  , _info("[INFO] ")
  , _stats("[TRACE] ")
  , _rstroke("======> ")
  , _stars(" ************************************************** \n")
  , _log_mode(mode)
{}

inline Loger::~Loger() {}

//inline Loger::Loger(Loger const &loger)
//  : _fatal("[FATAL] ")
//  , _debug("[DEBUG] ")
//  , _warning("[WARNING] ")
//  , _info("[INFO] ")
//  , _stats("[TRACE] ")
//  , _rstroke("======>")
//  , _stars(" ************************************************** \n")
//  , _log_mode(loger._log_mode)
//{}

//Loger &Loger::operator=(Loger const &a_loger)
//{
//  _log_mode = a_loger.level();
//  return *this;
//}


inline mrf::gui::fb::Loger::LEVEL Loger::level() const
{
  return _log_mode;
}


inline std::ostream &operator<<(std::ostream &os, Loger const &loger)
{
  switch (loger.level())
  {
    case Loger::Fatal:
    {
      os << termcolor::redIntensified << loger.fatal();
      break;
    }
    case Loger::Debug:
    {
      os << termcolor::blueIntensified << loger.debug();
      break;
    }

    case Loger::Warning:
    {
      os << termcolor::yellow << loger.warn();
      break;
    }

    case Loger::Trace:
    {
      os << loger.trace();
      break;
    }

    case Loger::Info:
    {
      os << termcolor::green << loger.info();
      break;
    }

    case Loger::No_Log:
    {
      break;
    }

    default:
      os << " NO LEVEL DEFINED ";
  }
  return os;
}

// inline void
// operator<<( Loger const & loger, char const * msg )
// {
//     std::cout << loger << msg << std::endl;
// }


}   // namespace fb
}   // namespace gui
}   // namespace mrf
