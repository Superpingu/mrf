/*
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/

#include <mrf_core/feedback/loger.hpp>

namespace mrf
{
namespace gui
{
namespace fb
{
Loger *Loger::s_loger = nullptr;


Loger *Loger::getInstance()
{
  /**
   * This is a safer way to create an instance. instance = new Singleton is
   * dangeruous in case two instance threads wants to access at the same time
   */
  if (s_loger == nullptr)
  {
    s_loger = new Loger(No_Log);
  }
  return s_loger;
}
Loger *Loger::getInstance(LEVEL mode)
{
  /**
   * This is a safer way to create an instance. instance = new Singleton is
   * dangeruous in case two instance threads wants to access at the same time
   */
  if (s_loger == nullptr)
  {
    s_loger = new Loger(mode);
  }
  return s_loger;
}

}   // namespace fb
}   // namespace gui
}   // namespace mrf