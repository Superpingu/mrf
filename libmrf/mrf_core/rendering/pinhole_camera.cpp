/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015
 *
 * modifications : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#include <mrf_core/rendering/pinhole_camera.hpp>

namespace mrf
{
namespace rendering
{
using namespace mrf::math;

Pinhole::Pinhole(
    mrf::math::Vec3f const &pos,
    mrf::math::Vec3f const &lookAt,
    mrf::math::Vec3f const &up,
    float                   fovy,
    float                   vpd,
    float                   fpd,
    size_t                  image_width_in_pixels,
    size_t                  image_height_in_pixels,
    unsigned int            nb_samples_per_pixels,
    std::string             id_name)
  : Camera(
      pos,
      lookAt,
      up,
      vpd,
      fpd,
      vpd * std::tan(mrf::math::Math::toRadian(fovy) * 0.5f),
      image_width_in_pixels,
      image_height_in_pixels,
      nb_samples_per_pixels,
      id_name)
  , _fovy(fovy)
{}

mrf::lighting::Ray Pinhole::generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const
{
  Vec3f const pos_on_sensor = _sensor_sampler.positionOnSensor(i, j, k, l);
  Vec3f       ray_dir       = (pos_on_sensor - _position).normal();
  Vec3f       ray_origin    = _position;

  return mrf::lighting::Ray(ray_origin, ray_dir, _view_plane_distance, _far_plane_distance);
}


void Pinhole::generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l, mrf::lighting::Ray &ray)
    const
{
  Vec3f const pos_on_sensor = _sensor_sampler.positionOnSensor(i, j, k, l);
  Vec3f       ray_dir       = (pos_on_sensor - _position).normal();
  Vec3f       ray_origin    = _position;

  ray.setOrigin(_position);
  ray.setValidDistances(_view_plane_distance, _far_plane_distance);
  ray.setDir(ray_dir);


#ifdef MRF_CAMERA_DEBUG
  std::cout << "  (i,j,k,l) = " << i << ", " << j << ", " << k << ", " << l << std::endl;
  std::cout << " Ray = " << ray << std::endl;
#endif
}

mrf::math::Mat4f Pinhole::projMat() const
{
  float invtan = 1.f / static_cast<float>(tan(_fovy * 0.5 * mrf::math::Math::PI / 180));
  float range  = _far_plane_distance - _view_plane_distance;

  //need to be implemented
  return mrf::math::Mat4f(
      invtan / _aspect_ratio,
      0,
      0,
      0,
      0,
      invtan,
      0,
      0,
      0,
      0,
      -(_view_plane_distance + _far_plane_distance) / range,
      -2 * _view_plane_distance * _far_plane_distance / range,
      0,
      0,
      -1,
      0);
}

mrf::math::Mat4f Pinhole::viewMat() const
{
  mrf::math::Vec3f translation = _rotation_matrix * _position;
  return mrf::math::Mat4f(
      _rotation_matrix(0, 0),
      _rotation_matrix(0, 1),
      _rotation_matrix(0, 2),
      -translation[0],
      _rotation_matrix(1, 0),
      _rotation_matrix(1, 1),
      _rotation_matrix(1, 2),
      -translation[1],
      _rotation_matrix(2, 0),
      _rotation_matrix(2, 1),
      _rotation_matrix(2, 2),
      -translation[2],
      0,
      0,
      0,
      1);
}

void Pinhole::fovy(float nfovy)
{
  _fovy = nfovy;
}

float Pinhole::fovy() const
{
  return _fovy;
}

void Pinhole::updateInnerData()
{
  //Suppose to UPDATE THIS: _left, _right, _top, _bottom;

  //TODO: Check ME BECAUSE IT SEEMS WRONG
  // _top = _viewPlaneDistance * tan( RADIAN(_fovy) * 0.5 )
  float h2 = _view_plane_distance * atan2f(_fovy, 2.0f);
  float f  = fovy();
  _top     = h2;
  _bottom  = -h2;


  float w2 = h2 * _aspect_ratio;
  _left    = w2;
  _right   = -w2;
}

}   // namespace rendering

}   // namespace mrf