/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015
 *
 * modifications : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#include <mrf_core/rendering/thin_lens_camera.hpp>
#include <mrf_core/sampling/sampler.hpp>

namespace mrf
{
namespace rendering
{
mrf::lighting::Ray ThinLens::generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const
{
  mrf::lighting::Ray r;
  generateCameraRay(i, j, k, l, r);
  return r;
}

mrf::math::Vec3f ThinLens::sampleLens(float random_e1, float random_e2) const
{
  using namespace mrf::math;
  using namespace mrf::sampling;

  // TODO: Check the Sampling procedure regarding the radius of the disk
  float circle_x;
  float circle_y;
  Sampler::concentricSampleDisk(random_e1, random_e2, circle_x, circle_y);


  // In the local Frame of the Thin-Lens Camera
  Vec3f const point_on_lens_CAM(
      circle_x * static_cast<float>(effectiveAperture()),
      circle_y * static_cast<float>(effectiveAperture()),
      0.f);

#ifdef THIN_LENS_CAMERA_DEBUG
  std::cout << point_on_lens_CAM.x() << " " << point_on_lens_CAM.y() << std::endl;
#endif


  return point_on_lens_CAM;
}


void ThinLens::generateCameraRay(
    unsigned int        i,
    unsigned int        j,
    unsigned int        k,
    unsigned int        l,
    mrf::lighting::Ray &ray) const
{
  using namespace mrf::math;
  using namespace mrf::sampling;

  Vec3f const pos_on_sensor_WORLD = _sensor_sampler.positionOnSensor(i, j, k, l);
  Vec3f const pos_on_sensor_CAM   = _thinLensFrame.worldToLocalMatrix() * pos_on_sensor_WORLD;

  // Step One: Find the intersection between the position (i,j) and the Focal Plane = P_on_f_plane
  float const parametric_dist     = static_cast<float>((_focalDistance - _sensorDistance) / _sensorDistance);
  Vec3f const dir_lens_center_CAM = -pos_on_sensor_CAM;

  Vec3f const point_on_focal_plane_CAM = pos_on_sensor_CAM + dir_lens_center_CAM * parametric_dist;

  // Step Two : Generate a point on the Disk.
  // TODO: Check the Sampling procedure regarding the radius of the disk
  // TODO: How can we ensure good coverage of the lens ?
  // TODO: User Jitter system  ?
  float circle_x;
  float circle_y;
  Sampler::concentricSampleDisk(
      RandomGenerator::Instance().getFloat(),
      RandomGenerator::Instance().getFloat(),
      circle_x,
      circle_y);


  // In the local Frame of the Thin-Lens Camera
  Vec3f const point_on_lens_CAM(
      circle_x * static_cast<float>(effectiveAperture()),
      circle_y * static_cast<float>(effectiveAperture()),
      0.f);


  Vec3f const point_on_focal_plane_WORLD = _thinLensFrame.localToWorldMatrix() * point_on_focal_plane_CAM;


  Vec3f const point_on_lens_WORLD = _thinLensFrame.localToWorldMatrix() * point_on_lens_CAM;
  Vec3f const ray_direction       = (point_on_focal_plane_WORLD - point_on_lens_WORLD).normalize();

#ifdef THIN_LENS_CAMERA_DEBUG
  std::cout << " Circle_x = " << circle_x << " circle_y = " << circle_y << std::endl;
  std::cout << "  Point on Focal Plane WORLD: " << point_on_focal_plane_WORLD
            << "  Point on Lens WORLD: " << point_on_lens_WORLD << "  Point on Lens CAM: " << point_on_lens_CAM
            << "  Point on Sensor WORLD: " << pos_on_sensor_WORLD << std::endl
            << " Ray direction WORLD: " << ray_direction << std::endl;
#endif

  // Step Three: Create a Ray starting from the point on the disk with dir = P_on_f_place  -P_on_disk
  ray.setOrigin(point_on_lens_WORLD);
  ray.setDir(ray_direction);
}


mrf::lighting::Ray
ThinLens::generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l, unsigned int lens_sample_nb)
    const
{
  using namespace mrf::math;
  using namespace mrf::sampling;
  using namespace mrf::lighting;

  //----------------------------------------------------------------------------------------------------------------
  // LENS PART
  //----------------------------------------------------------------------------------------------------------------
  assert(lens_sample_nb < _lensJitterer.totalNbSamples());

  //unsigned int const nb_samples_per_dimension = std::sqrt( _lensJitterer.totalNbSamples() );
  //unsigned int const lens_i = lens_sample_nb /  nb_samples_per_dimension;
  //unsigned int const lens_j = lens_sample_nb % nb_samples_per_dimension;

  float const e1 = _lensJitterer.getRandomFloat(0, lens_sample_nb, RandomGenerator::Instance().getFloat());
  float const e2 = _lensJitterer.getRandomFloat(1, lens_sample_nb, RandomGenerator::Instance().getFloat());

  //float const e1 = RandomGenerator::Instance().getFloat();
  //float const e2 = RandomGenerator::Instance().getFloat();

  Vec3f const point_on_lens_CAM   = sampleLens(e1, e2);
  Vec3f const point_on_lens_WORLD = _thinLensFrame.localToWorldMatrix() * point_on_lens_CAM;
  //----------------------------------------------------------------------------------------------------------------


  Vec3f const pos_on_sensor_WORLD = _sensor_sampler.positionOnSensor(i, j, k, l);
  Vec3f const pos_on_sensor_CAM   = _thinLensFrame.worldToLocalMatrix() * pos_on_sensor_WORLD;

  // Step One: Find the intersection between the position (i,j) and the Focal Plane = P_on_f_plane
  float const parametric_dist     = static_cast<float>((_focalDistance + _sensorDistance) / _sensorDistance);
  Vec3f const dir_lens_center_CAM = -pos_on_sensor_CAM;

  Vec3f const point_on_focal_plane_CAM   = pos_on_sensor_CAM + dir_lens_center_CAM * parametric_dist;
  Vec3f const point_on_focal_plane_WORLD = _thinLensFrame.localToWorldMatrix() * point_on_focal_plane_CAM;

  Vec3f const ray_direction = (point_on_focal_plane_WORLD - point_on_lens_WORLD).normalize();

#ifdef THIN_LENS_CAMERA_DEBUG
  //std::cout << " Circle_x = " << circle_x << " circle_y = " << circle_y << std::endl;
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << " i , j, k, l = " << i << "  "
            << " " << j << " " << k << " " << l << std::endl;
  std::cout << "  Point on Focal Plane WORLD: " << point_on_focal_plane_WORLD
            << " Point on Focal PLane CAM: " << point_on_focal_plane_CAM << " parametric_dist =  " << parametric_dist
            << "  Point on Lens WORLD: " << point_on_lens_WORLD << "  Point on Lens CAM: " << point_on_lens_CAM
            << "  Point on Sensor WORLD: " << pos_on_sensor_WORLD << std::endl
            << "  Point on sensor CAM : " << pos_on_sensor_CAM << std::endl
            << " Ray direction WORLD: " << ray_direction << std::endl
            << " _rightAxis = " << _rightAxis << std::endl
            << " _up = " << _up << std::endl
            << " _thinLensFrame.localToWorldMatrix() = " << _thinLensFrame.localToWorldMatrix() << std::endl
            << " _thinLensFrame.worldToLocalMatrix() = " << _thinLensFrame.worldToLocalMatrix() << std::endl
            << " _thinLensFrame = " << _thinLensFrame << std::endl;
#endif

  // Step Three: Create a Ray starting from the point on the disk with dir = P_on_f_place  -P_on_disk
  Ray ray(point_on_lens_WORLD, ray_direction);

  return ray;
}

unsigned int ThinLens::numberOfLensSamples() const
{
  return _lensJitterer.totalNbSamples();
}

void ThinLens::generateLensBorderSamples(std::vector<mrf::math::Vec3d> &lens_border_positions) const
{
  using namespace mrf::math;

  lens_border_positions.clear();

  // Generate position on the circle of the effective apeture radius
  double PHI_STEP = _Math<double>::TWO_PI / 8.0;
  for (unsigned int k = 0; k < 8; k++)
  {
    // 0 45 90 135 180 225 270 315
    double phi = PHI_STEP * k;

    Vec3d const pos(effectiveAperture() * cos(phi), effectiveAperture() * sin(phi), 0.0);

    lens_border_positions.push_back(_thinLensFrame.localToWorldMatrix() * pos);
  }
}

mrf::math::Mat4f ThinLens::viewMat() const
{
  return mrf::math::Mat4f();
}

mrf::math::Mat4f ThinLens::projMat() const
{
  return mrf::math::Mat4f();
}


void ThinLens::updateInnerData() {}


}   // namespace rendering

}   // namespace mrf