/*
 *
 * author : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#pragma once


#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/sampling/jittering.hpp>
#include <mrf_core/rendering/camera.hpp>

#include <iostream>
#include <iomanip>

namespace mrf
{
namespace rendering
{
class MRF_CORE_EXPORT Orthographic: public Camera
{
public:
  Orthographic(
      mrf::math::Vec3f const &pos,
      mrf::math::Vec3f const &lookAt,
      mrf::math::Vec3f const &up,
      mrf::math::Vec2f        sensor_size,
      float                   vpd,
      float                   fpd,
      size_t                  image_width_in_pixels,
      size_t                  image_height_in_pixels,
      unsigned int            nb_samples_per_pixels,
      std::string             id_name = "NO_ID_NAME_SPECIFIED");


  //------------------------------------------------------------------------------------------
  // Interface from Camera
  //------------------------------------------------------------------------------------------
  mrf::lighting::Ray generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const;

  void generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l, mrf::lighting::Ray &ray) const;


  virtual CameraType getType() const { return CameraType::Orthographic; }
  virtual Camera *   copy() const { return (Camera *)new Orthographic(*this); }

  virtual mrf::math::Mat4f projMat() const;
  virtual mrf::math::Mat4f viewMat() const;

  virtual void updateInnerData();
};

inline std::ostream &operator<<(std::ostream &os, Orthographic const &cam)
{
  return os << " Orthographic camera [ pos : " << cam.position() << " lookat : " << cam.lookAt() << " up : " << cam.up()
            << " vpd : " << cam.viewPlaneDistance() << " vfd : " << cam.farPlaneDistance()
            << " aspectRatio: " << cam.aspectRatio() << " left :  " << cam.left() << " right :  " << cam.right()
            << " top :  " << cam.top() << " bottom :  " << cam.bottom() << std::endl
            << " Sensor :  " << cam.sensor() << std::endl
            << " SensorSampler " << cam.sensorSampler() << " ] " << std::endl;
}

}   // namespace rendering
}   // namespace mrf
