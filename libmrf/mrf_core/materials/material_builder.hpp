/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <map>

#include <mrf_core/io/obj_material.hpp>
#include <mrf_core/materials/materials.hpp>
#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/brdf.hpp>

#include <mrf_core/image/image.hpp>

namespace mrf
{
namespace materials
{
// Creer des foncteurs par type de fichier (OBJ, 3DS etc) et faire en sorte que le MaterialBuilder soit paramétré
// ou parametrer en template le type de fichier du foncteur

//Functor - to encapsulate functions pointers
class MRF_CORE_EXPORT Functor
{
private:
  //int (*ptrFct)(const char*);   // pointer to member function
  mrf::materials::UMat *(*_ptrFct)(OBJMaterial const &gm);

public:
  // constructor - takes pointer to an object and pointer to a member and stores
  // them in two private variables
  Functor(mrf::materials::UMat *(*ptrFct)(OBJMaterial const &gm)) { _ptrFct = ptrFct; };

  // override operator "()"
  mrf::materials::UMat *operator()(OBJMaterial const &gm) { return (*_ptrFct)(gm); };   // execute member function

  // override function "Call" which override ???
  // Code pasted from : http://www.newty.de/fpt/functor.html ??
  mrf::materials::UMat *createMaterial(OBJMaterial const &gm) { return (*_ptrFct)(gm); };   // execute member function
};
// END Functor



// ptr function for GenericMaterial
//typedef mrf::materials::UMat* (*ptrFct)(OBJMaterial const & gm);

///////////////////////////////////
///////////////////////////////////
///////////////////////////////////
class MRF_CORE_EXPORT MaterialBuilder
{
private:
  std::map<std::string, Functor *> _builtInMaterials;

  //Public Methods
public:
  static MaterialBuilder &Instance();



  inline mrf::materials::UMat *createMaterial(std::string const &materialType, OBJMaterial const &gm);


private:
  void init();
  bool registerMaterial(std::string const &materialType, mrf::materials::UMat *(*ptrFct)(OBJMaterial const &gm));

  static mrf::materials::UMat *createLambertMaterial(OBJMaterial const &gm);
  static mrf::materials::UMat *createLPhongMaterial(OBJMaterial const &gm);
  // static mrf::materials::UMat *createASNganMaterial(OBJMaterial const &gm);
  // static mrf::materials::UMat*	createMatusikMaterial( OBJMaterial const & gm );
  // static mrf::materials::UMat *createFBTDFMaterial(OBJMaterial const &gm);
  // static mrf::materials::UMat *createDTextureMaterial(OBJMaterial const &gm, mrf::materials::BRDF &brdf);

  // Singleton
private:
  //Hidden due to Singleton Implementation
  MaterialBuilder();
  virtual ~MaterialBuilder();                            // SHOULD DELETE POINTERS
  MaterialBuilder(MaterialBuilder const &);              // copy ctor is hidden
  MaterialBuilder &operator=(MaterialBuilder const &);   // assign op is hidden
};


inline mrf::materials::UMat *MaterialBuilder::createMaterial(const std::string &materialType, OBJMaterial const &gm)
{
  mrf::materials::UMat *res = NULL;

  // if there is a texture map (for the moment Diffuse Texture) we build a DTexture with the corresponding materialType
  if (gm.hasKdTexture())
  {
    // mrf::materials::BRDF *brdf = NULL;

    // std::map<std::string, Functor *>::iterator it, end;
    // it  = _builtInMaterials.find(materialType);
    // end = _builtInMaterials.end();

    // assert(it != end);

    // brdf = dynamic_cast<mrf::materials::BRDF *>(_builtInMaterials[materialType]->createMaterial(gm));
    // assert(brdf != NULL);
    // res = createDTextureMaterial(gm, *brdf);
  }
  else   // it is a no textured material
  {
    res = _builtInMaterials[materialType]->createMaterial(gm);
  }

  return res;
}



}   // namespace materials
}   // namespace mrf
