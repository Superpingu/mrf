/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2018

 **/

#pragma once

#include <mrf_core/color/color.hpp>

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  include <mrf_core/radiometry/spectrum.hpp>
#endif


namespace mrf
{
namespace materials
{
/**
 *
 * Depending of the rendering engine being compiled the Radiometric values are
 * map to RGB color (not clamped) for malia_rgb and to a Spectrum for malia
 *
 **/

typedef color::Color RGB_RADIANCE;

#ifdef MRF_RENDERING_MODE_SPECTRAL

typedef mrf::radiometry::Spectrum_T<float, float> RADIANCE_TYPE;
typedef mrf::radiometry::Spectrum_T<float, float> COLOR;
typedef mrf::radiometry::Spectrum_T<float, float> RADIOSITY_TYPE;
typedef mrf::radiometry::Spectrum_T<float, float> POWER_TYPE;
//typedef  mrf::radiometry::Spectrum<double> VARIANCE_TYPE;

#else

typedef RGB_RADIANCE RADIANCE_TYPE;
typedef color::Color COLOR;
typedef color::Color RADIOSITY_TYPE;
typedef color::Color POWER_TYPE;
//typedef mrf::rendering::Color3D VARIANCE_TYPE;

#endif

typedef RADIOSITY_TYPE IRRADIANCE_TYPE;


}   // namespace materials
}   // namespace mrf