/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2018
 *
 **/

#include <mrf_core/materials/bsdf.hpp>

namespace mrf
{
namespace materials
{
BSDF::BSDF(std::string const &name): UMat(name) {}
BSDF::BSDF(std::string const &name, std::string const &type): UMat(name, type) {}

BSDF::~BSDF() {}

//-------------------------------------------------------------------------
// Partial Implementation of UMat's methods
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// Default behaviour is to return wrong or 0.0
// This is clearly to save up time when extending from this class
// but one should be careful about it
// TODO: This should be an abstract class (af)
//-------------------------------------------------------------------------
bool BSDF::diffuseComponent() const
{
  assert(0);
  return false;
}

float BSDF::diffuseAlbedo() const
{
  assert(0);
  return 0.0f;
}

float BSDF::specularAlbedo() const
{
  assert(0);
  return 0.0f;
}

bool BSDF::specularComponent() const
{
  assert(0);
  return false;
}

float BSDF::nonDiffuseAlbedo() const
{
  assert(0);
  return 0.0f;
}

bool BSDF::nonDiffuseComponent() const
{
  assert(0);
  return false;
}

float BSDF::diffPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0);
  return 0.0f;
}

float BSDF::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0);
  return 0.0f;
}

float BSDF::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0);
  return 0.0f;
}


float BSDF::diffuseAlbedo(mrf::geom::LocalFrame const &) const
{
  return diffuseAlbedo();
}

float BSDF::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  return specularAlbedo();
}




//--------------------------------------------------------------//
// A BSDF DOES NOT EMIT LIGHT
//--------------------------------------------------------------//
bool BSDF::isEmissive() const
{
  return false;
}


RADIANCE_TYPE
BSDF::radianceEmitted(mrf::math::Vec3f const & /*emit_out_dir*/, mrf::geom::LocalFrame const & /*lf*/) const
{
  return RADIANCE_TYPE();
}


RADIOSITY_TYPE
BSDF::radiosityEmitted() const
{
  return RADIOSITY_TYPE();
}

RADIOSITY_TYPE
BSDF::radiosityEmitted(mrf::geom::LocalFrame const &) const
{
  return RADIOSITY_TYPE();
}

RADIOSITY_TYPE
BSDF::particleEmission(
    mrf::geom::LocalFrame const & /*lf*/,
    float /*random_var_1*/,
    float /*random_var_2*/,
    mrf::math::Vec3f & /*direction*/) const
{
  return RADIOSITY_TYPE();
}


//--------------------------------------------------------------//
// A BSDF DOES TRANSMIT LIGHT BY DEFAULT
//--------------------------------------------------------------//
float BSDF::transmissionAlbedo() const
{
  return 1.0f;
}

float BSDF::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 1.0f;
}

bool BSDF::transmissionComponent() const
{
  assert(0.0);
  return true;
}

float BSDF::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0.0);
  return 1.0f;
}

float BSDF::brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir)
    const
{
  //TODO IMPLEMENT THIS IN color::Color
  RADIOSITY_TYPE c_brdf = this->coloredBRDFValue(lf, in_dir, out_dir);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  return c_brdf.normalizedIntegral();
#else
  //TODO IMPLEMENT THIS IN color::Color
  //return c_brdf.power();
  const float DEFAULT_LUMINANCE_COEFF = 1.0f / 3.0f;
  return (
      DEFAULT_LUMINANCE_COEFF * c_brdf.r() + DEFAULT_LUMINANCE_COEFF * c_brdf.g()
      + DEFAULT_LUMINANCE_COEFF * c_brdf.b());
#endif
}


ScatterEvent::SCATTER_EVT BSDF::generateScatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    RADIANCE_TYPE & /*incident_radiance*/,
    mrf::math::Vec3f &outgoing_dir) const
{
  assert(0);
  float proba_out;
  return UMat::scatteringDirection(lf, in_dir, outgoing_dir, proba_out);
}



}   // namespace materials
}   // namespace mrf