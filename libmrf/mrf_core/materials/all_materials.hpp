/*
 *
 * author : Arthur Dufay
 * Copyright INRIA 2018
 *
 **/

#pragma once

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/brdf.hpp>

//#include <mrf_core/materials/ECone.hpp>
//#include <mrf_core/materials/dirac_emittance.hpp>
//#include <mrf_core/materials/uniform_emittance.hpp>
//#include <mrf_core/materials/diffuse_emittance.hpp>
//#include <mrf_core/materials/conical_emittance.hpp>
//#include <mrf_core/materials/Matusik.hpp>
//#include <mrf_core/materials/MatusikSCML.hpp>
//#include <mrf_core/materials/ASNgan.hpp>
//#include <mrf_core/materials/BTDFTypes.hpp>
//#include <mrf_core/materials/FBTDF.hpp>
//#include <mrf_core/materials/ReboundShader.hpp>
//#include <mrf_core/materials/ASIso.hpp>
//#include <mrf_core/materials/MerlBRDF.hpp>
//#include <mrf_core/materials/Ward.hpp>
//#include <mrf_core/materials/AnisoPhong.hpp>
//#include <mrf_core/materials/AnisoMeasured.hpp>
//#include <mrf_core/materials/DBRDF.hpp>
//#include <mrf_core/materials/ShiftedGammaDist.hpp>
//#include <mrf_core/materials/GGXDiffract.hpp>
//#include <mrf_core/materials/LowSmooth.hpp>
//#include <mrf_core/materials/CTD.hpp>
//#include <mrf_core/materials/ConductorGHS.hpp>
//#include <mrf_core/materials/PlasticGHS.hpp>
//#include <mrf_core/materials/SubsurfaceGHS.hpp>
//#include <mrf_core/materials/He.hpp>
//#include <mrf_core/materials/GenBeckmann.hpp>
//#include <mrf_core/materials/NanoBRDF2D.hpp>
//#include <mrf_core/materials/GGX_comp0.hpp>
//#include <mrf_core/materials/GGX_comp1.hpp>
//#include <mrf_core/materials/hazygloss.hpp>
#include <mrf_core/materials/multi_material.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
// TO DEBUG
static char const *REBOUND = "rebound";

//Perfect or almost Perfect theoritical Materials
static char const *MIRROR = "Mirror";   // Perfectly Mirror  Material

// EMPIRICAL
static char const *PHONG     = "Phong";
static char const *BLINN     = "Blinn";
static char const *WARD      = "Ward";
static char const *LAFORTUNE = "Lafortune";

// MORE PHYSICAL
static char const *MERL    = "Merl";      //  The measured Material from MERL-MIT Data base (2nd version of Matusik)
static char const *MATUSIK = "Matusik";   // The measures material of Matusik 1rst Version (<2006. data from LIGUM)
static char const *ANISOTROPIC_PHONG = "AnisotropicPhong";   // The Ashikhmin-Shirley anisoptropic Phong BRDF
static char const *AS                = "AS";                 // Ashikhmin-Shirley Microfacet generator Model
static char const *AS_NGAN           = "AS_NGAN";            // Ashikhmin-Shirley Microfacet generator
// Model used by Ngan (EGSR 2005/6). This is almost the
// same model as the ANISOTROPIC_PHONG proposed by Ashikhmin and Shirley

static char const *AS_ISO = "AS_ISO";   // JGT 2000 Ashikhmin Shirley BRDF Model but isotropic
static char const *POULIN = "POULIN";   // The anisotropic Poulin-Fournier model

static char const *MEASURED_ANISO_NGAN = "MeasuredAnisoNgan";   // The measured anisotropic materials from NGAN
// (http://people.csail.mit.edu/wojciech/BRDFAnalysis/)

static char const *MEASURED_ANISOTROPIC = "MeasuredAnisotropic";

//Distribution-based BRDF from Ashikhmin and Premoze
static char const *DBRDF = "d-brdf";

static char const *SGD                = "sgd";               //Shifted Gamma Distribution
static char const *GGXD               = "ggxd";              //GGX + Diffraction
static char const *CTD                = "ctd";               // The new Cook-Torrance and Diffraction Model
static char const *LOW_ABC_SMOOTH     = "low_smooth";        // The smooth-surface model by L�w et al. (Church-based)
static char const *LOW_ABC_MICROFACET = "low_microfacet";    // The micro-facet model by L�w et al. (Church-based)
static char const *MFGHS              = "micro_facet_GHS";   // Micro-Facet with GHS Model

static char const *HE           = "he";                     // He Model
static char const *GEN_BECKMANN = "generalized_beckmann";   // Generalized Beckmann Model
static char const *HAZY_GLOSS   = "hazy_gloss";             // Hazy Gloss Model
static char const *GGX_COMP0    = "ggx_comp0";              // COmpositing for Hazy Gloss
static char const *GGX_COMP1    = "ggx_comp1";              // COmpositing for Hazy Gloss

static char const *SMOOTH_DIELECTRIC = "smooth_dielectric";   // Smooth Dielectric
// Rational BRDF
static char const *RATIONAL_HC = "Rational_HC";   //This is Rational BRDF that are hard-coded in C++

//NANO 2D BRDF
static char const *NANO_2D;

static char const *GLASS;

//Proxy material to handle shape containing more than one material.
static char const *MULTI_MAT = "multi_material";
};   // namespace BRDFTypes

namespace BTDFTypes
{
static char const *FBTDF      = "FBTDF";
static char const *DIELECTRIC = "DIELECTRIC";
static char const *CONDUCTOR  = "CONDUCTOR";
};   // namespace BTDFTypes

}   // namespace materials
}   // namespace mrf
