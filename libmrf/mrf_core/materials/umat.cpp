/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/

#include <mrf_core/materials/umat.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/sampling/sampler.hpp>
#include <mrf_core/sampling/jittering.hpp>
#include <mrf_core/util/precision_timer.hpp>


using std::string;

using namespace mrf::math;
using namespace mrf::sampling;
using namespace mrf::geom;

namespace mrf
{
namespace materials
{
UMat::UMat(std::string const &name): _name(name) {}
UMat::UMat(std::string const &name, std::string const &type): _name(name), _type(type) {}

UMat::~UMat() {}


std::vector<RADIANCE_TYPE> UMat::hemiReflectivity(
    unsigned int        nb_samples_along_view_diretions,
    std::vector<float> &view_angles,
    unsigned int        nb_samples_for_integral) const
{
  float const integral_scaling_factor = static_cast<float>(Math::TWO_PI) / static_cast<float>(nb_samples_for_integral);

  std::vector<RADIANCE_TYPE> integral_results;
  LocalFrame                 lf(
      Vec3f(0.f, 0.f, 0.0f),     // Position
      Vec3f(0.0f, 1.0f, 0.0f),   // Normal
      Vec3f(1.0f, 0.0f, 0.0f)    // Tangent
  );

  std::cout << "[INFO] Local  Frame for hemiReflectivity is: " << lf << std::endl;

  // Generate the different light directions that are going to be used
  // to compute the integral

  std::vector<mrf::math::Vec3f> ldirs;
  JitteredSample2D              jitterer(nb_samples_for_integral);

  mrf::util::PrecisionTimer timer;
  timer.start();
  for (unsigned int nl = 0; nl < nb_samples_for_integral; nl++)
  {
    float const e1 = jitterer.getRandomFloat(0, nl, RandomGenerator::Instance().getFloat());
    float const e2 = jitterer.getRandomFloat(1, nl, RandomGenerator::Instance().getFloat());
    ldirs.push_back(randomHemiDirection(lf, e1, e2));
  }
  timer.stop();
  std::cout << " Lights Directions Generated in: " << timer.elapsed() << " seconds. " << std::endl;

  float const rotation_angle = static_cast<float>(Math::PI_2 / (nb_samples_along_view_diretions - 1));
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << " Integral scaling factor = " << integral_scaling_factor << std::endl;
  // std::cout <<   " Rotation angle = " << rotation_angle << std::endl;

  Quatf q;
  q.fromAxisAngle(mrf::math::Vec3f(1.0, 0.0f, 0.0f), rotation_angle);

  mrf::math::Vec3f current_view_dir = lf.normal();

  for (unsigned int nv = 0; nv < nb_samples_along_view_diretions; nv++)
  {
    //std::cout << " Theta View  = " << std::acos( clamp( current_view_dir.dot( lf.normal() ),0.f,1.f) ) * Math::RAD_TO_DEG << std::endl;
    float const tV
        = std::acos(clamp(current_view_dir.dot(lf.normal()), 0.f, 1.f)) * static_cast<float>(Math::RAD_TO_DEG);
    view_angles.push_back(tV);

    RADIANCE_TYPE hemi_refl(0.0f);
    for (unsigned int nl = 0; nl < nb_samples_for_integral; nl++)
    {
      float const cosine_factor = mrf::math::clamp(ldirs[nl].dot(lf.normal()), 0.0f, 1.0f);

      hemi_refl += coloredBRDFValue(lf, -current_view_dir, ldirs[nl]) * cosine_factor;
    }

    current_view_dir = q * current_view_dir;

    hemi_refl *= integral_scaling_factor;

#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
    assert(hemi_refl.isFinite());
#endif

    integral_results.push_back(hemi_refl);
  }
  return integral_results;
}


float UMat::totalAlbedo() const
{
  return diffuseAlbedo() + specularAlbedo() + transmissionAlbedo();
}

//-------------------------------------------------------------------------
//
//-------------------------------------------------------------------------
ScatterEvent::SCATTER_EVT UMat::scatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f &           outgoing_dir,
    float &                      proba_outging_dir) const
{
  float const e1 = RandomGenerator::Instance().getFloat();
  float const e2 = RandomGenerator::Instance().getFloat();

  return scatteringDirection(lf, in_dir, outgoing_dir, proba_outging_dir, e1, e2);
}



ScatterEvent::SCATTER_EVT UMat::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              brdf_corrected_by_pdf) const
{
  float const e1 = RandomGenerator::Instance().getFloat();
  float const e2 = RandomGenerator::Instance().getFloat();
  return scatteringDirectionWithBRDF(lf, in_dir, e1, e2, outgoing_dir, brdf_corrected_by_pdf);
}


ScatterEvent::SCATTER_EVT UMat::scatteringDirectionWithBSDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              bsdf_corrected_by_pdf,
    mrf::materials::IOR const &  in_ior,
    mrf::materials::IOR const &  out_ior) const
{
  float const e1 = RandomGenerator::Instance().getFloat();
  float const e2 = RandomGenerator::Instance().getFloat();

  return scatteringDirectionWithBSDF(lf, in_dir, e1, e2, outgoing_dir, bsdf_corrected_by_pdf, in_ior, out_ior);
}

//-------------------------------------------------------------------------
// Emissivity
//-------------------------------------------------------------------------
RADIOSITY_TYPE
UMat::radiosityEmitted(mrf::geom::LocalFrame const & /*lf*/) const
{
  return radiosityEmitted();
}



//-------------------------------------------------------------------------
// REFLECTION
//-------------------------------------------------------------------------
bool UMat::reflectionIsPossible(
    mrf::math::Vec3f const &normal,
    mrf::math::Vec3f const &incident_dir,
    float &                 dot_Normal_wi) const
{
  float dot_N_incident_dir = incident_dir.dot(normal);

  if (dot_N_incident_dir < -EPSILON)
  {
    dot_Normal_wi = -dot_N_incident_dir;
    return true;
  }
  return false;
}


//-------------------------------------------------------------------------
// REFRACTION
//-------------------------------------------------------------------------
bool UMat::transDirection(
    mrf::math::Vec3f const &normal,
    Vec3f const &           in_dir,
    float                   incoming_index_of_refraction,
    float                   out_index_of_refraction,
    Vec3f &                 trans_dir) const
{
  float const ior_ratio  = incoming_index_of_refraction / out_index_of_refraction;
  float const dot_N_invV = normal.dot(in_dir);

  float const square_ior_ratio = ior_ratio * ior_ratio;

  float const tir_term = 1.0f - ((1.0f - dot_N_invV * dot_N_invV) * square_ior_ratio);

  //#ifdef DEBUG
  //std::cout << "  tir_term = " << tir_term << std::endl ;
  //#endif “‘


  if (static_cast<double>(tir_term) < -DBL_EPSILON)
  {
    //std::cout << " TIR IS ACTIVE " << std::endl;
    return false;
  }

#ifdef DEBUG
  cout << __FILE__ << " " << __LINE__ << endl;
  std::cout << " CLASSICAL TRANSMISSION " << std::endl;
  std::cout << "  tir_term = " << tir_term << std::endl;
  std::cout << "  ior_ratio = " << ior_ratio << std::endl;
  std::cout << "  dot_N_invV = " << dot_N_invV << std::endl;
#endif




  trans_dir = (in_dir - normal * dot_N_invV) * ior_ratio - normal * std::sqrt(tir_term);

#ifdef DEBUG
  float clamped_tir_term = tir_term;
  clamp(clamped_tir_term, 0.0f, 1.0f);
  std::cout << "  clamped_tir_term = " << clamped_tir_term << std::endl;
  std::cout << "  trans_dir = " << trans_dir << std::endl;
#endif

  return true;
}

bool UMat::transDirection(
    LocalFrame const &lf,
    Vec3f const &     in_dir,
    float             incoming_index_of_refraction,
    float             out_index_of_refraction,
    Vec3f &           trans_dir) const
{
  return transDirection(lf.normal(), in_dir, incoming_index_of_refraction, out_index_of_refraction, trans_dir);
}


mrf::math::Vec3f UMat::mirrorDirection(mrf::math::Vec3f const &normal, mrf::math::Vec3f const &in_dir) const
{
  Vec3f const reflected_dir = 2.0f * (-in_dir.dot(normal)) * normal + in_dir;
  return reflected_dir;
}



mrf::math::Vec3f UMat::mirrorDirection(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir) const
{
  //std::cout << __FILE__ << " " << __LINE__ << std::endl ;

  // Vec3f const normal = lf.normal();
  // Vec3f const reflected_dir = 2.0f*(-in_dir.dot(lf.normal()))*lf.normal() + in_dir;
  return mirrorDirection(lf.normal(), in_dir);
}



//-------------------------------------------------------------------------
// STATIC SAMPLING FUNCTIONS
//-------------------------------------------------------------------------

mrf::math::Vec3f UMat::hemiNormalDirection(mrf::geom::LocalFrame const &lf)
{
  float const x = 0.0f;
  float const y = 0.0f;
  float const z = 1.0f;


  Vec3f direction_generated = x * lf.binormal() + y * lf.tangent() + z * lf.normal();
  return direction_generated;
}


mrf::math::Vec3f UMat::randomHemiDirection(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2)
{
  //float const LOCAL_EPSILON =  1.1f*EPSILON;
  float const LOCAL_EPSILON = 0.0f;


  float const z = mrf::math::clamp(random_var_1, LOCAL_EPSILON, 1.0f);

  float const phi = static_cast<float>(_Math<double>::TWO_PI * random_var_2);
  float const r   = std::sqrt(Math::max(LOCAL_EPSILON, 1.0f - z * z));

  float const x = static_cast<float>(mrf::math::clamp(r * std::cos(phi), -1.0f, 1.0f));
  float const y = static_cast<float>(mrf::math::clamp(r * std::sin(phi), -1.0f, 1.0f));


  Vec3f const direction_generated = x * lf.binormal() + y * lf.tangent() + z * lf.normal();
  //direction_generated.normalize();

  //assert( direction_generated.dot(lf.normal() ) > EPSILON  );

  //          if( ! ( direction_generated.dot(lf.normal() ) > EPSILON ) )
  //          {
  // //              std::cout << "  x = " << x << "  y = " << y << "  z = " << z << std::endl ;
  // //              std::cout << "  direction_generated = " << direction_generated << "  lf = " << lf << std::endl ;
  //          }else
  //          {
  //              //assert(0);
  //          }



  return direction_generated;
}


void UMat::randomHemiDirection(
    mrf::geom::LocalFrame const &lf,
    float                        random_var_1,
    float                        random_var_2,
    mrf::math::Vec3f &           dir_generated)
{
  dir_generated = randomHemiDirection(lf, random_var_1, random_var_2);

  //          float const z = random_var_1;
  //          float phi = static_cast<float>( Math::TWO_PI ) * random_var_2;
  //          float const r = std::sqrt( Math::max(0.0f, 1.0f - z*z) );

  //          float const x = r * std::cos( phi );
  //          float const y = r * std::sin( phi );

  //          dir_generated = x*lf.binormal() + y*lf.tangent() + z*lf.normal();
}


mrf::math::Vec3f UMat::randomHemiCosDirection(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2)
{
  Vec3f local_dir;

  // This procedure assumes that the Hemisphere normal is orientated along the Z direction
  // which is not the case for the Local Frame which is in TNB mode !
  Sampler::concentricSampleDisk(random_var_1, random_var_2, local_dir(0), local_dir(1));
  local_dir.setZ(std::sqrt(Math::max(0.0f, 1.0f - local_dir.x() * local_dir.x() - local_dir.y() * local_dir.y())));

  //Why that ? , max should avoid it...
  // if( local_dir.z() < EPSILON )
  // {
  //     local_dir[2] = 0.0f;
  // }


  float const LOCAL_DIR_LENGTH = local_dir.length();
  local_dir /= LOCAL_DIR_LENGTH;
  if (!(LOCAL_DIR_LENGTH < (1.0f + EPSILON)))
  {
    local_dir /= LOCAL_DIR_LENGTH;

    // std::cout << "  local_dir.length() = " << local_dir.length() << std::endl ;
    //std::cout << "  lf = " << lf << std::endl ;
    // std::cout << "  random_var_1 = " << random_var_1 << "  random_var_2 = " << random_var_2 << std::endl ;
    // std::cout << "  local_dir = " << local_dir << std::endl ;
    // std::cerr << __FILE__ << " " << __LINE__ << " and booooooooom " << std::endl;
    // std::cout << EPSILON << std::endl;
    // std::exit(-1);
    // assert(0);
  }

  Vec3f const local_dir2(local_dir[0], local_dir[2], local_dir[1]);


  // =======> THIS IS A BUG  because local_dir has been generate with a procedure that assumes
  // Z = [ 0 0 1] where as LocalFrame assumes [ 0 1 0] !!!! <============
  //  BUT BUT BUT :  local_dir2 is shifted correctly !
  Vec3f dir_generated_from_lf = lf.directionToWorldSpace(local_dir2);

#ifdef UMAT_DEBUG
  if (dir_generated_from_lf.dot(lf.normal()) < 0.0)
  {
    //TODO CHECK ME
    return -dir_generated_from_lf;

    // std::cout << " LOCAL_DIR_LENGTH = " << LOCAL_DIR_LENGTH << std::endl;
    // std::cout << " dir_generated_from_lf =  " << dir_generated_from_lf << std::endl;
    // std::cout << " local_dir2 =  " << local_dir2 << std::endl;
    // std::cout << " local_dir  =  " << local_dir << std::endl;
    // std::cout << " length local_dir = " << local_dir.length() << std::endl;
    // std::cout <<  " lf.normal() = " << lf.normal() << std::endl;
    // std::cout << " dir_generated_from_lf.dot( lf.normal() ) =" << dir_generated_from_lf.dot( lf.normal() )
    //           << std::endl;

    // Vec3f dir_generated = local_dir.y()*lf.binormal() + local_dir.x()*lf.tangent() + local_dir.z()*lf.normal();

    // std::cout << " dir_generated = " << dir_generated << std::endl;
    // std::cout << " dot dir_generated lf.normal  = " << dir_generated.dot(lf.normal()) << std::endl;
    // std::cout << " lf = " << lf << std::endl;
    // std::cout << " lf.tangent.norm " << lf.tangent().length() << std::endl;
    // std::cout << " lf.bitanget.norm " << lf.binormal().length() << std::endl;
    // std::cout << " lf.normal.norm " << lf.normal().length() << std::endl;
    // std::cout << "  dot(normal, tangent) = " << lf.normal().dot( lf.tangent() ) << std::endl;
    // std::cout << "  dot(normal, binormal) = " << lf.binormal().dot( lf.tangent() ) << std::endl;
    // std::cout << "  dot(tangent, binormal) = " << lf.binormal().dot( lf.tangent() ) << std::endl;

    // assert( dir_generated_from_lf.dot( lf.normal() ) >= 0.0 );
  }
#endif

  return dir_generated_from_lf;

  //ORIGINAL FIX
  // Vec3f dir_generated = local_dir.x()*lf.binormal() + local_dir.y()*lf.tangent() + local_dir.z()*lf.normal();

  //TO DEBUG
  // Vec3f dir_generated3 = local_dir.y()*lf.binormal() + local_dir.x()*lf.tangent() + local_dir.z()*lf.normal();

  // std::cout << " ###########################################################################################  " << std::endl;
  // std::cout << "  lf = " << lf << std::endl << local_dir << "  dir_generated = " << dir_generated << std::endl
  //           <<" dir_generated_from_lf = " << dir_generated_from_lf << std::endl
  //           << " dir genereated 3 " << dir_generated3 << std::endl;

  // std::cout << "  Dot  d, N = " << dir_generated.dot( lf.normal() )
  //           << " dot d_from_lf , N  = " << dir_generated_from_lf.dot( lf.normal() )
  //           << std::endl;

  //  mrf::math::Mat3f tnb;
  //  lf.matrixTNB( tnb );
  //  mrf::math::Mat3f inv_tnb = tnb.inverse();

  //  // std::cout << "tnb = " << tnb << std::endl << " inv_tnb " << inv_tnb << " transpose of tnb = " << tnb.transpose() ;
  //  // std::cout << " World Directon = " << inv_tnb * local_dir << std::endl;
  //  std::cout << " World Directon with local_dir2 = " << inv_tnb * local_dir2 << std::endl;

  //  // std::cout << " TEST tnb * lf.normal() = " << tnb * lf.normal() << std::endl;
  //  // std::cout << " TEST inv_tnb * tnb * lf.normal() = " << inv_tnb * tnb * lf.normal() << std::endl;
  // std::cout << " ###########################################################################################  " << std::endl;

  // std::exit(-1);
}



void UMat::randomHemiCosDirection(
    mrf::geom::LocalFrame const &lf,
    float                        random_var_1,
    float                        random_var_2,
    mrf::math::Vec3f &           dir_generated)
{
  dir_generated = randomHemiCosDirection(lf, random_var_1, random_var_2);
}

Vec3f UMat::randomSphereDirection(float random_var_1, float random_var_2)
{
  float z   = 1.0f - 2.0f * random_var_1;
  float r   = sqrtf(Math::max(0.0f, 1.0f - z * z));
  float phi = static_cast<float>(Math::TWO_PI) * random_var_2;
  float x   = r * cosf(phi);
  float y   = r * sinf(phi);

  return Vec3f(x, y, z);
}

void UMat::randomSphereDirection(float random_var_1, float random_var_2, mrf::math::Vec3f &dir_generated)
{
  dir_generated = randomSphereDirection(random_var_1, random_var_2);

  //          float z = 1.0f - 2.0f * random_var_1;
  //          float r = sqrtf( Math::max(0.0f, 1.0f - z*z) );
  //          float phi = Math::TWO_PI * random_var_2;
  //          float x = r * cosf(phi);
  //          float y = r * sinf(phi);

  //          dir_generated.setX( x );
  //          dir_generated.setY( y );
  //          dir_generated.setZ( z );
}

mrf::math::Vec3f UMat::randomHemiPowerCosDirection(
    mrf::math::Vec3f const &dir,
    unsigned int const      exponent,
    float const             random_var_1,
    float const             random_var_2)
{
  mrf::math::Vec3f const X(dir.generateOrthogonal());
  mrf::math::Vec3f const Y(dir.cross(X));

  float const phi = static_cast<float>(Math::TWO_PI) * random_var_1;

  float const z = powf(random_var_2, 1.f / (float)(exponent + 1));
  ;
  float const sin_theta = sqrtf((1.f - z * z) / X.sqrLength());
  float const x         = sin_theta * cosf(phi);
  float const y         = sin_theta * sinf(phi);

  return x * X + y * Y + z * dir;
}

void UMat::randomHemiPowerCosDirection(
    mrf::math::Vec3f const &dir,
    unsigned int const      exponent,
    float const             random_var_1,
    float const             random_var_2,
    mrf::math::Vec3f &      dir_generated)
{
  dir_generated = randomHemiPowerCosDirection(dir, exponent, random_var_1, random_var_2);
}


std::string const &UMat::name() const
{
  return _name;
}

}   // namespace materials
}   // namespace mrf
