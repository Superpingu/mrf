/*
 *
 * author : David Murray @institutoptique.fr
 * Copyright CNRS 2021
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>



namespace mrf
{
namespace materials
{
//-------------------------------------------------------------------------
// Measured material
//-------------------------------------------------------------------------

class MRF_CORE_EXPORT MeasuredMaterial: public BRDF
{
public:
  enum Parameterization
  {
    UTIA_2D,
    UTIA_4D,
    UTIA_4D_H,
    UTIA_4D_H_NL,
    RUSINKIEWICZ_2D,
    RUSINKIEWICZ_2D_COS,
    RUSINKIEWICZ_3D,
    RUSINKIEWICZ_3D_COS,
    RUSINKIEWICZ_4D,
    HEMISPHERICAL_ALBEDO,
  };

protected:
  //No data storage at this level, too many format in inherited classes.

  Parameterization _param;

  bool _interpolation;

public:
  MeasuredMaterial(std::string const &name);
  MeasuredMaterial(std::string const &name, std::string const &type);

  virtual ~MeasuredMaterial();

  unsigned int getParameterization() const { return _param; }
  void         setParameterization(Parameterization value) { _param = value; }

  bool getInterpolate() const { return _interpolation; }
  void setInterpolate(bool value) { _interpolation = value; }
  //-------------------------------------------------------------------------
  // UMat Interface Methods Implementation
  //-------------------------------------------------------------------------

  // virtual float
  // brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  // virtual RADIANCE_TYPE coloredBRDFValue(
  //     mrf::geom::LocalFrame const &lf,
  //     mrf::math::Vec3f const &     in_dir,
  //     mrf::math::Vec3f const &     out_dir) const;


  virtual float diffuseAlbedo() const;
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  diffuseComponent() const;

  virtual float specularAlbedo() const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  specularComponent() const;

  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;


  virtual float totalAlbedo() const;

  //-------------------------------------------------------------------------
  // virtual float
  // diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  // virtual float
  // specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  // virtual float
  // transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  // virtual float
  // nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      float &                      proba_outgoing_dir,
      float                        e1,
      float                        e2) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;
};

}   // namespace materials
}   // namespace mrf
