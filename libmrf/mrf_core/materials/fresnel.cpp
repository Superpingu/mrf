/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/


#include <mrf_core/materials/fresnel.hpp>

namespace mrf
{
namespace materials
{
RADIANCE_TYPE FresnelConductor::evalFromCosThetaDiff(double cos_theta_d) const
{
  //return fresnel_for_conductor( _square_eta, _square_k, cos_theta_d );
  //TODO: CHECK ME !!! why are we storing the square ?
  // return fresnel4( _square_eta.sqrt(), _square_k.sqrt(), cos_theta_d );
  return fresnel(RADIANCE_TYPE(1.0), _square_eta.sqrt(), RADIANCE_TYPE(0.0), _square_k.sqrt(), cos_theta_d);
}

/**
 * @brief      Construct a Index of Refraction / FresnelColor Class
 *             according to two colors.
 *
 *             See GULBRANDSEN O
 *             Artist friendly metallic fresnel.
 *             Journal of Computer Graphics Techniques (JCGT)
 *             3, 4 (December 2014), 64–72. URL: http://jcgt.org/ published/0003/04/03/. 2, 5, 6
 *
 * @param      f0  The color at normal incidence
 * @param      g_color   Edgetint color
 *
 * @return     FresnelConductor
 */
FresnelConductor FresnelConductor::fresnelFromColor(COLOR const &f0, COLOR const &g)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  (void)f0;
  (void)g;
  return FresnelConductor();
#else
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "f0 = " << f0 << std::endl;
  std::cout << "g = " << g << std::endl;


  RADIANCE_TYPE const ONE(1.0f);

  RADIANCE_TYPE eta = ((ONE - g) * (ONE + f0.sqrt()) / (ONE - f0.sqrt())) + (((ONE - f0) / (ONE + f0)) * g);

  RADIANCE_TYPE const sqr_eta_plus_one  = mrf::color::Color(eta + ONE).sqr();
  RADIANCE_TYPE const sqr_eta_minus_one = mrf::color::Color(eta - ONE).sqr();

  //std::cout << "f0 * sqr_eta_plus_one - sqr_eta_minus_one = " << f0 * sqr_eta_plus_one - sqr_eta_minus_one << std::endl;

  // RADIANCE_TYPE numerator = (f0 * sqr_eta_plus_one - sqr_eta_minus_one);
  // numerator.clampNegativeToZero();

  RADIANCE_TYPE kappa
      = (mrf::color::Color(f0 * sqr_eta_plus_one - sqr_eta_minus_one).clampNegativeToZero() / (ONE - f0)).sqrt();



  //std::cout << "eta = " << eta << " kappa = " << kappa << std::endl;

  if (g[0] == 0.0f)
  {
    kappa[0] = 0.0f;
  }
  if (g[1] == 0.0f)
  {
    kappa[1] = 0.0f;
  }
  if (g[2] == 0.0f)
  {
    kappa[2] = 0.0f;
  }

  //std::cout << "eta = " << eta << " kappa = " << kappa << std::endl;


  if (eta.isInfOrNan() || kappa.isInfOrNan())
  {
    //std::cout << "eta = " << eta << " kappa = " << kappa << std::endl;
    assert(0);
  }
  return FresnelConductor(eta, kappa);
#endif
}




}   // namespace materials
}   // namespace mrf