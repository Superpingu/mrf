/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/geometry/aabbox.hpp>
#include <mrf_core/math/vec3.hpp>

namespace mrf
{
namespace geom
{
/**
 * Mother class of all geometrical Object.
 * Stores an Axis Aligned Bounding Box and a
 * default Material index.
 *
 **/
class MRF_CORE_EXPORT Object
{
protected:
  static unsigned int id_counter;   //The counter for ID

protected:
  unsigned int _id;   //Unique ID of each Object
  unsigned int _material_index;
  MRF_AABBox   _bbox;

public:
  Object(unsigned int mat_index);
  Object(unsigned int mat_index, mrf::math::Vec3f const &min_bbox, mrf::math::Vec3f const &max_bbox);
  Object(unsigned int mat_index, MRF_AABBox const &bbox);
  virtual ~Object();


  //-------------------------------------------------------------------------
  // ACCESSORS
  //-------------------------------------------------------------------------
  inline unsigned int id() const;

  inline unsigned int materialIndex() const;
  inline unsigned int materialNumber() const;   //alias for the previous method
  inline void         materialIndex(unsigned int mat_index);
  inline void         setMaterialIndex(unsigned int mat_index);   //alias for the previous method

  inline MRF_AABBox const &bbox() const;
  inline MRF_AABBox &      bbox();

  //-------------------------------------------------------------------------
  // STATISTICS ACCESSORS
  //-------------------------------------------------------------------------
  /**
   * Returns the total (in bytes) amount  of memory
   * used by this Object
   **/
  virtual unsigned int memorySize() const;
};

inline unsigned int Object::id() const
{
  return _id;
}


inline unsigned int Object::materialIndex() const
{
  return _material_index;
}

inline unsigned int Object::materialNumber() const
{
  return materialIndex();
}

inline void Object::materialIndex(unsigned int mat_index)
{
  _material_index = mat_index;
}

inline void Object::setMaterialIndex(unsigned int mat_index)
{
  _material_index = mat_index;
}



inline MRF_AABBox const &Object::bbox() const
{
  return _bbox;
}
inline MRF_AABBox &Object::bbox()
{
  return _bbox;
}

}   // namespace geom
}   // namespace mrf
