/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/vec3.hpp>
#include <mrf_core/math/mat3.hpp>
#include <mrf_core/geometry/aabbox.hpp>
#include <mrf_core/geometry/intersectable.hpp>
#include <mrf_core/geometry/meshable.hpp>
#include <mrf_core/geometry/object.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <cassert>
#include <tuple>

namespace mrf
{
namespace geom
{
/**
 *
 * This class represents a Triangular-Mesh
 * with Material per face,
 * (u,v,w), normals and tangents per vertex as well.
 * stores also a materialPerFaces vector, it can be empty
 **/

enum TangentMethods
{
  withUV,
  withProjection,
  withJCGT,
  asBRDFExplorer
};

template<class VEC3_TYPE>
class _Mesh: public Intersectable
{
private:
  /**
   * The boundix box of the mesh
   * \todo Delete this in future version has we do not apply the transformation
   * on the mesh anymore but we store it in the shape
   **/
  MRF_AABBox _bbox;

  /**
   * Vertices coordinates
   **/
  std::vector<VEC3_TYPE> _vertices;
  /**
   * Normals per vertex
   **/
  std::vector<VEC3_TYPE> _npv;
  /**
   * Tangents per vertex
   **/
  std::vector<VEC3_TYPE> _tpv;
  /**
   * (u,v,w) per vertex
   **/
  std::vector<VEC3_TYPE> _uvs;
  /**
   * index of vertices for each face
   * triangular faces only
   **/
  std::vector<uint> _faces;
  /**
   * material per face
   * the index correspond to a postion in the vector of materials of the Scene object
   **/
  std::vector<unsigned int> _materialPerFaces;
  /**
   * Store the absolut filepath from where the mesh was loaded (obj, ply ...)
   **/
  std::string _mesh_absolute_filepath;
  /**
   * Store the tangent computation method
   **/
  enum TangentMethods _tangent_method;

public:
  /**
   *  Construct A Mesh with the given vertices and normal and  texture coordinates per vertices
   *
   *  All Tangents are fill with (1,0,0) by default
   **/
  _Mesh();

  _Mesh(
      std::vector<VEC3_TYPE> const &   vertices,
      std::vector<VEC3_TYPE> const &   normals,
      std::vector<VEC3_TYPE> const &   uvw,
      std::vector<unsigned int> const &face_indexes,
      std::vector<unsigned int> const &material_per_faces);

  _Mesh(
      std::vector<VEC3_TYPE> const &   vertices,
      std::vector<VEC3_TYPE> const &   normals,
      std::vector<VEC3_TYPE> const &   uvw,
      std::vector<unsigned int> const &face_indexes,
      std::vector<unsigned int> const &material_per_faces,
      enum TangentMethods const &      tangent_method);

  _Mesh(
      std::vector<VEC3_TYPE> &&   vertices,
      std::vector<VEC3_TYPE> &&   normals,
      std::vector<VEC3_TYPE> &&   uvw,
      std::vector<unsigned int> &&face_indexes,
      std::vector<unsigned int> &&material_per_faces);

  _Mesh(
      std::vector<VEC3_TYPE> const &   vertices,
      std::vector<VEC3_TYPE> const &   normals,
      std::vector<VEC3_TYPE> const &   uvw,
      std::vector<VEC3_TYPE> const &   tangents,
      std::vector<unsigned int> const &face_indexes,
      std::vector<unsigned int> const &material_per_faces);

  _Mesh(
      std::vector<VEC3_TYPE> const &   vertices,
      std::vector<VEC3_TYPE> const &   normals,
      std::vector<VEC3_TYPE> const &   uvw,
      std::vector<VEC3_TYPE> const &   tangents,
      std::vector<unsigned int> const &face_indexes,
      std::vector<unsigned int> const &material_per_faces,
      enum TangentMethods const &      tangent_method);

  _Mesh(
      std::vector<VEC3_TYPE> &&   vertices,
      std::vector<VEC3_TYPE> &&   normals,
      std::vector<VEC3_TYPE> &&   uvw,
      std::vector<VEC3_TYPE> &&   tangents,
      std::vector<unsigned int> &&face_indexes,
      std::vector<unsigned int> &&material_per_faces);

  _Mesh(_Mesh const &);

  std::vector<VEC3_TYPE> const &   vertices() const;
  std::vector<VEC3_TYPE> const &   normals() const;
  std::vector<VEC3_TYPE> const &   tangents() const;
  std::vector<VEC3_TYPE> const &   uvs() const;
  std::vector<unsigned int> const &faces() const;
  std::vector<unsigned int> const &materialPerFaces() const;

  mrf::geom::MRF_AABBox const &bbox() const;

  std::string &      absoluteFilepath();
  std::string const &absoluteFilepath() const;

  //----------------------------------------------------------------------
  // Getter and Setter for tangent method
  //----------------------------------------------------------------------

  enum TangentMethods getTangentMethod() const { return _tangent_method; }

  void setTangentMethod(enum TangentMethods a_tangent_method) { _tangent_method = a_tangent_method; }

  //-----------------------------------------------------------------------
  //Operator
  //-----------------------------------------------------------------------

  // _Mesh<VEC3_TYPE> &operator<<(_Mesh<VEC3_TYPE> const &);   //Merge two _Mesh

  template<class OTHER_VEC3_TYPE>
  _Mesh<VEC3_TYPE> &operator=(_Mesh<OTHER_VEC3_TYPE> const &);

  //-----------------------------------------------------------------------
  //Intersectable Interface
  //-----------------------------------------------------------------------
  virtual mrf::geom::MRF_AABBox &bbox();

  virtual bool isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info) const;


  virtual bool
  isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const;

  virtual bool isIntersected(mrf::lighting::Ray const &r) const;

  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info) const;

  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const;

  virtual bool isIntersected(mrf::lighting::Ray const &r, unsigned int sub_object_id) const;


  virtual bool isIntersected(MRF_AABBox const &box, unsigned index_face) const;

  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, unsigned index_face) const;

  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max) const;

  virtual bool
  isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, std::vector<unsigned int> &sub_objects) const;

  virtual bool isIntersected(MRF_AABBox const &box, std::vector<unsigned int> &objects) const;




  virtual unsigned int numSubObjects() const;
  virtual bool         hasSubObjects() const;
  virtual MRF_AABBox   subObjectAABBox(unsigned int subobj_index) const;

  virtual unsigned int intersectionCost() const;
  virtual unsigned int memorySize() const;

  /**
   * @brief      Compute Tangents for each vertex using Lengyel’s Method
   *
   * @param      vertices  The vertices
   * @param      normals   The normals
   * @param      uvw       The uvw
   * @param      faces     The faces
   * @param      tangents  The tangents
   */
  static void computeTangentsFromUV(
      std::vector<VEC3_TYPE> const &   vertices,
      std::vector<VEC3_TYPE> const &   normals,
      std::vector<VEC3_TYPE> const &   uvw,
      std::vector<unsigned int> const &faces,
      std::vector<VEC3_TYPE> &         tangents);

  template<typename T>
  inline VEC3_TYPE interpolateNormal(unsigned int face_index, T u, T v, T w) const;

  template<typename T>
  inline VEC3_TYPE interpolateTangent(unsigned int face_index, T u, T v, T w) const;

  template<typename T>
  inline VEC3_TYPE interpolateUVS(unsigned int face_index, T u, T v, T w) const;

private:
  void initializeMeshData(
      std::vector<VEC3_TYPE> const &   vertices,
      std::vector<VEC3_TYPE> const &   normals,
      std::vector<VEC3_TYPE> const &   uvw,
      std::vector<VEC3_TYPE> const &   tangents,
      std::vector<unsigned int> const &face_indexes,
      std::vector<unsigned int> const &material_per_faces);

  void initializeMeshData(
      std::vector<VEC3_TYPE> &&   vertices,
      std::vector<VEC3_TYPE> &&   normals,
      std::vector<VEC3_TYPE> &&   uvw,
      std::vector<VEC3_TYPE> &&   tangents,
      std::vector<unsigned int> &&face_indexes,
      std::vector<unsigned int> &&material_per_faces);

  inline MRF_AABBox aabboxFromFace(unsigned int index) const;
  inline void       updateAABBox();



  template<typename T>
  inline void updateHitInfoStructure(
      unsigned int     sub_object_id,
      VEC3_TYPE const &point,
      T                u,
      T                v,
      T                w,
      T                intersection_parametric_distance,
      Intersection &   hit_info) const;


  void generateTangentsPerVertex(
      std::vector<VEC3_TYPE> const &   vertices,
      std::vector<VEC3_TYPE> const &   normals,
      std::vector<VEC3_TYPE> const &   uvw,
      std::vector<unsigned int> const &face_indexes,
      std::vector<VEC3_TYPE> &         tangents);

  void checkNormalTangentOrthogonality();


  template<typename T>
  inline VEC3_TYPE computeTangentFromTensor(mrf::math::Mat3<T> const &tenseur, VEC3_TYPE const &init_tangent) const;
};

//Define Here the
typedef _Mesh<mrf::math::Vec3f> Mesh;
//TODO: Eigen VEC3_TYPE MeshE

#include "mesh.cxx"

//External Operator
template<class VEC3_TYPE>
inline std::ostream &operator<<(std::ostream &os, _Mesh<VEC3_TYPE> const &mesh)
{
  os << " [ Mesh Info: #vertices: " << mesh.vertices().size() << " ; "
     << " #faces:" << mesh.faces().size() / 3 << std::endl
     << "   AABBox:" << mesh.bbox() << " ] " << std::endl
     << " file: " << mesh.absoluteFilepath() << std::endl
     << std::endl;

#ifdef MRF_GEOM_MESH_DEBUG
  std::vector<VEC3_TYPE> const &mesh_tangents = mesh.tangents();
  unsigned int const            max_index     = 10;
  for (unsigned int i = 0; i < max_index; i++)
  {
    os << "  tangent [" << i << " ] = " << mesh_tangents[i] << std::endl;
  }

  std::vector<VEC3_TYPE> const &uvw = mesh.uvs();
  for (unsigned int i = 0; i < max_index; i++)
  {
    os << "  uvw [" << i << " ] = " << uvw[i] << std::endl;
  }


#endif


  os << "******************************************************" << std::endl;

  return os;
}

template<class VEC3_TYPE>
void _Mesh<VEC3_TYPE>::computeTangentsFromUV(
    std::vector<VEC3_TYPE> const &   vertices,
    std::vector<VEC3_TYPE> const &   normals,
    std::vector<VEC3_TYPE> const &   uvs,
    std::vector<unsigned int> const &index_faces,
    std::vector<VEC3_TYPE> &         tangents)
{
  using namespace mrf::math;

  tangents.resize(vertices.size());

  for (unsigned int i = 0; i < tangents.size(); ++i)
  {
    tangents[i] = VEC3_TYPE::zero();
  }

  for (unsigned int i = 0; i < index_faces.size(); i += 3)
  {
    // index_faces is non-null because i1, i2, i3 are correctly defined
    unsigned int const i1 = index_faces[i];
    unsigned int const i2 = index_faces[i + 1];
    unsigned int const i3 = index_faces[i + 2];

    // vertices is non-null because v1, v2, v3 are correctly defined
    VEC3_TYPE v1, v2, v3;
    v1 = vertices[i1];
    v2 = vertices[i2];
    v3 = vertices[i3];

    Vec2f w1(uvs[i1].x(), uvs[i1].y());
    Vec2f w2(uvs[i2].x(), uvs[i2].y());
    Vec2f w3(uvs[i3].x(), uvs[i3].y());

    float x1 = v2(0) - v1(0);
    float x2 = v3(0) - v1(0);

    float y1 = v2(1) - v1(1);
    float y2 = v3(1) - v1(1);

    float z1 = v2(2) - v1(2);
    float z2 = v3(2) - v1(2);

    float s1 = w2(0) - w1(0);
    float s2 = w3(0) - w1(0);

    float t1 = w2(1) - w1(1);
    float t2 = w3(1) - w1(1);

    float const r = 1.0f / (s1 * t2 - s2 * t1);

    VEC3_TYPE tangent((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);

    // Add axis to each vertex components.
    tangents[i1] += tangent;
    tangents[i2] += tangent;
    tangents[i3] += tangent;

    if (tangent.isInfOrNan())
    {
      std::cout << "vertices.size() = " << vertices.size() << std::endl;
      std::cout << "faces.size() = " << index_faces.size() << std::endl;
      std::cout << "uvs.size() = " << uvs.size() << std::endl;
      std::cout << " i1 = " << i1 << " i2 = " << i2 << " i3 = " << i3 << std::endl;
      std::cout << " v1 = " << v1 << " v2 = " << v2 << " v3 = " << v3 << std::endl;
      std::cout << " UVs: w1 = " << w1 << " w2 = " << w2 << " w3 = " << w3 << std::endl;
      std::cout << " s1 = " << s1 << " t1 = " << t1 << " t2 = " << t2 << " s2 = " << s2 << std::endl;
      std::cout << " r = " << r << " tangent = " << tangent << std::endl;
      assert(0);
    }
  }

  std::cout << __FILE__ << " " << __LINE__ << std::endl;

  for (unsigned int i = 0; i < vertices.size(); i++)
  {
    const VEC3_TYPE n = normals[i];
    const VEC3_TYPE t = tangents[i];

    // Gram-Schmidt orthonormalization
    tangents[i] = (t - n * n.dot(t)).normal();

    if (tangents[i].isInfOrNan())
    {
      std::cout << "n = " << n << " t = " << tangents[i] << std::endl;
      assert(!tangents[i].isInfOrNan());
    }
  }

}   //end of static method computeTangentsFromUV(...)


}   // namespace geom
}   // namespace mrf
