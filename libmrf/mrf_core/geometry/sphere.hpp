/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>


//MRF
#include <mrf_core/geometry/shape.hpp>
#include <mrf_core/geometry/meshable.hpp>
#include <mrf_core/geometry/mesh.hpp>
#include <mrf_core/geometry/local_frame.hpp>
#include <mrf_core/lighting/i_light.hpp>
#include <mrf_core/math/vec3.hpp>


namespace mrf
{
namespace geom
{
class MRF_CORE_EXPORT Sphere
  : public mrf::geom::Shape
  , public mrf::lighting::ILight
  , public Meshable
  , public Intersectable
{
private:
  float _radius;


public:
  /**
   * Constructs a sphere with a default radius of 1.0
   * and place it by default at the origin of the world
   */
  Sphere(unsigned int material_index, float pos_x = 0.0f, float pos_y = 0.0f, float pos_z = 0.0f, float radius = 1.0f);

  Sphere(unsigned int material_index, mrf::math::Vec3f const &position, float radius);

  virtual ~Sphere();

  inline void  setRadius(float radius);
  inline float radius() const;

  bool minMaxParametricDistanceFromPoint(mrf::math::Vec3f const &position, float &tmin, float &tmax) const;

  //-------------------------------------------------------------------------
  // Meshable Implementation
  //-------------------------------------------------------------------------

  virtual mrf::geom::Mesh createMesh() const;

  //-------------------------------------------------------------------------
  // Intersectable Implementation
  //-------------------------------------------------------------------------
  virtual mrf::geom::MRF_AABBox const &bbox() const;
  virtual mrf::geom::MRF_AABBox &      bbox();

  virtual bool isIntersected(mrf::lighting::Ray const &r) const;
  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info) const;
  virtual bool isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const;
  virtual bool isIntersected(mrf::lighting::Ray const &r, unsigned int sub_object_id) const;

  virtual bool isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info) const;


  virtual bool
  isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int sub_object_id) const;

  virtual unsigned int numSubObjects() const;
  virtual bool         hasSubObjects() const;

  /**
   * Since a sphere does not contain any subojects, this method returns
   * the AABBox of the whole Sphere
   **/
  virtual MRF_AABBox subObjectAABBox(unsigned int subobj_index) const;

  /**
   * This method (and the next one) has no meaning for a Sphere.
   * Therefore this method stops the execution of the program when assertion are enabled
   **/
  virtual bool isIntersected(MRF_AABBox const &box, unsigned index_face) const;
  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, unsigned index_face) const;


  virtual bool isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max) const;



  /**
   * This method (and the next one) has no meaning for a Sphere.
   * Therefore this method stops the execution of the program when assertion are enabled
   **/
  virtual bool
  isIntersected(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, std::vector<unsigned int> &sub_objects) const;

  virtual bool isIntersected(MRF_AABBox const &box, std::vector<unsigned int> &objects) const;


  virtual unsigned int intersectionCost() const;
  virtual unsigned int memorySize() const;

  //-------------------------------------------------------------------------
  // ILight Implementation
  //-------------------------------------------------------------------------
  virtual mrf::math::Vec3f center() const { return position(); };
  virtual float            surfaceArea() const;
  virtual float surfaceSampling(float random_var_1, float random_var_2, mrf::geom::LocalFrame &light_local_frame) const;

  virtual float lightGeometricFactor(mrf::math::Vec3f const &light_vector) const;

  virtual mrf::math::Vec3f lightVector(
      mrf::math::Vec3f const &point_shaded,
      mrf::math::Vec3f const &light_position,
      float &                 distance_point_light,
      float &                 square_distance_point_light) const;


protected:
  //TODO:
  //void fillIntersectionStructure(
};


inline void Sphere::setRadius(float radius)
{
  _radius = radius;
}

inline float Sphere::radius() const
{
  return _radius;
}



inline std::ostream &operator<<(std::ostream &os, Sphere const &s)
{
  return os << " Sphere : position: " << s.position() << "; radius: " << s.radius() << std::endl;
}




}   // namespace geom

}   // namespace mrf
