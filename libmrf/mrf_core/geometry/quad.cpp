#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/geometry/mesh.hpp>

namespace mrf
{
namespace geom
{
using namespace mrf::math;
using namespace mrf::lighting;

RQuad::RQuad(Vec3f const &v1, Vec3f const &v2, Vec3f const &v3, Vec3f const &v4, unsigned int material_index)
  : Shape(v1.x(), v1.y(), v1.z(), material_index)
  , _length((v2 - v1).length())
  , _height((v4 - v1).length())
  , _width_2(_length / 2.0f)
  , _height_2(_height / 2.0f)
{
  Vec3f x = v2 - v1;
  x.normalEq();

  Vec3f z = x.cross(v4 - v1);
  z.normalEq();

  Vec3f y = z.cross(x);
  _cs.setOrientation(x, y, z);

  _bbox.expandBy(v1);
  _bbox.expandBy(v2);
  _bbox.expandBy(v3);
  _bbox.expandBy(v4);

  _d = -z.dot(v1);

  _cs.setPosition((v1 + v3) * 0.5f);

  _bbox.expandBy(_cs.getMatrixFrom() * Vec3f(0.0f, 0.0f, EPSILON));
  _bbox.expandBy(_cs.getMatrixFrom() * Vec3f(0.0f, 0.0f, -EPSILON));

#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << "  v1 = " << v1 << "  v2 = " << v2 << "  v3 = " << v3 << "  v4 = " << v4 << "  _height = " << _height
            << std::endl;
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  print(_bbox);
  std::cout << "  _cs [ X = " << _cs.getOrientation().getAxisX() << "; " << std::endl;
  std::cout << "  Y = " << _cs.getOrientation().getAxisY() << "; " << std::endl;
  std::cout << "  Z = " << _cs.getOrientation().getAxisZ() << " ]" << std::endl;
  std::cout << "  _d = " << _d << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "  _width_2 = " << _width_2 << std::endl;
  std::cout << "  _height_2 = " << _height_2 << std::endl;
  std::cout << " Surface area  " << _length * _height << std::endl;
  std::cout << "  _cs.getPosition() = " << _cs.getPosition() << std::endl;
  std::cout << "  Texture coordinates of center = " << uvByPosition(_cs.getPosition()) << std::endl;
#endif


#ifdef MRF_GEOM_QUAD_DEBUG
  LocalFrame lf;
  surfaceSampling(0.0, 0.0, lf);
  surfaceSampling(1.0, 0.0, lf);
  surfaceSampling(0.0, 1.0, lf);
  surfaceSampling(1.0, 1.0, lf);
  surfaceSampling(0.5, 0.5, lf);
  surfaceSampling(0.1, 0.8, lf);
#endif
}



RQuad::RQuad(
    Vec3f const &origin,
    Vec3f const &down_right_corner,
    Vec3f const &up_right_corner,
    unsigned int material_index)
  : Shape(origin.x(), origin.y(), origin.z(), material_index)
  , _length((down_right_corner - origin).length())
  , _height((up_right_corner - down_right_corner).length())
  , _width_2(_length / 2.0f)
  , _height_2(_height / 2.0f)
{
#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
#endif

  Vec3f x = (down_right_corner - origin);
  x.normalEq();

  Vec3f z = x.cross(up_right_corner - origin);
  z.normalEq();

  Vec3f y = z.cross(x);
  _cs.setOrientation(x, y, z);

#ifdef MRF_GEOM_QUAD_DEBUG
  print(_bbox);
#endif

  _bbox.expandBy(origin);


#ifdef MRF_GEOM_QUAD_DEBUG
  print(_bbox);
#endif

  _bbox.expandBy(down_right_corner);

#ifdef MRF_GEOM_QUAD_DEBUG
  print(_bbox);
#endif

  _bbox.expandBy(up_right_corner);

#ifdef MRF_GEOM_QUAD_DEBUG
  print(_bbox);
  std::cout << " ABOUT TO ADD THE LAST POINTS TO BEND THE BBOX " << std::endl;
#endif

  //For numeric stability
  _bbox.expandBy(_cs.getMatrixFrom() * Vec3f(0.0f, 0.0f, EPSILON));
  _bbox.expandBy(_cs.getMatrixFrom() * Vec3f(0.0f, 0.0f, -EPSILON));

  _d = -z.dot(origin);

  _cs.setPosition((origin + up_right_corner) * 0.5f);

  //Up Left Corner
  Vec3f dx_y(_width_2, -_height_2, 0.0f);
  _bbox.expandBy(_cs.getMatrixFrom() * (-dx_y));



#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  print(_bbox);
  std::cout << "  _cs [ X = " << _cs.getOrientation().getAxisX() << "; " << std::endl;
  std::cout << "  Y = " << _cs.getOrientation().getAxisY() << "; " << std::endl;
  std::cout << "  Z = " << _cs.getOrientation().getAxisZ() << " ]" << std::endl;
  std::cout << "  _d = " << _d << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "  _width_2 = " << _width_2 << std::endl;
  std::cout << "  _height_2 = " << _height_2 << std::endl;
  std::cout << "  _cs.getPosition() = " << _cs.getPosition() << std::endl;
  std::cout << "  Texture coordinates of center = " << uvByPosition(_cs.getPosition()) << std::endl;
#endif


#ifdef MRF_GEOM_QUAD_DEBUG
  LocalFrame lf;
  surfaceSampling(0.0, 0.0, lf);
  surfaceSampling(1.0, 0.0, lf);
  surfaceSampling(0.0, 1.0, lf);
  surfaceSampling(1.0, 1.0, lf);
  surfaceSampling(0.5, 0.5, lf);
  surfaceSampling(0.1, 0.8, lf);
#endif
}

RQuad::RQuad(
    float                   pos_x,
    float                   pos_y,
    float                   pos_z,
    mrf::math::Vec3f const &anormal,
    float                   length,
    float                   height,
    unsigned int            material_index)
  : Shape(pos_x, pos_y, pos_z, material_index)
  , _length(length)
  , _height(height)
  , _width_2(_length / 2.0f)
  , _height_2(_height / 2.0f)

{
  //TODO:
  //CHECK THIS !!
  Quatf rot;
  rot.fromTwoVecs(_cs.getOrientation().getAxisZ(), anormal);
  _cs.setOrientation(_cs.getOrientation() * rot);

  _d = -anormal.dot(_cs.getPosition());

  //Expand the bounding box

  Vec3f dxy(_width_2, _height_2, 0.0f);

  Vec3f dx_y(_width_2, -_height_2, 0.0f);

  //For numeric stability
  _bbox.expandBy(_cs.getMatrixFrom() * Vec3f(0.0f, 0.0f, EPSILON));
  _bbox.expandBy(_cs.getMatrixFrom() * Vec3f(0.0f, 0.0f, -EPSILON));

  //Up Right Corner
  _bbox.expandBy(_cs.getMatrixFrom() * dxy);
  //Down Left Corner
  _bbox.expandBy(_cs.getMatrixFrom() * (-dxy));

  //Down Right Corner
  _bbox.expandBy(_cs.getMatrixFrom() * dx_y);
  //Up Left Corner
  _bbox.expandBy(_cs.getMatrixFrom() * (-dx_y));

#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  print(_bbox);
  std::cout << "  _cs [ X = " << _cs.getOrientation().getAxisX() << "; " << std::endl;
  std::cout << "  Y = " << _cs.getOrientation().getAxisY() << "; " << std::endl;
  std::cout << "  Z = " << _cs.getOrientation().getAxisZ() << " ]" << std::endl;
  std::cout << "  _d = " << _d << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "  _width_2 = " << _width_2 << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "  dxy (in world coordinates)= " << (_cs.getMatrixFrom() * dxy) << std::endl;
#endif

#ifdef MRF_GEOM_QUAD_DEBUG
  LocalFrame lf;

  surfaceSampling(0.0, 0.0, lf);
  surfaceSampling(1.0, 0.0, lf);
  surfaceSampling(0.0, 1.0, lf);
  surfaceSampling(1.0, 1.0, lf);
  surfaceSampling(0.5, 0.5, lf);

  surfaceSampling(0.1, 0.8, lf);
#endif
}

RQuad::RQuad(mrf::math::Reff &ref, mrf::math::Vec3f scale, float length, float height, unsigned int material_index)
  : Shape(ref, scale, material_index)
  , _length(length * scale(0))
  , _height(height * scale(1))
  , _width_2(_length / 2.0f)
  , _height_2(_height / 2.0f)
{
  Vec3f normal2(0, 0, 1);

  //		  normal2 = _cs.getMatrixFrom() * normal2;
  //		  normal2.normalEq();

  // Compute Normal
  Vec3f v1(-_width_2 / 2, _height_2 / 2, 0);
  Vec3f v2(_width_2 / 2, _height_2 / 2, 0);
  Vec3f v3(_width_2 / 2, -_height_2 / 2, 0);

  v1 = _cs.getMatrixFrom() * v1;
  v2 = _cs.getMatrixFrom() * v2;
  v3 = _cs.getMatrixFrom() * v3;

  Vec3f normal = (v3 - v1).cross(v1 - v2);
  normal.normalEq();

#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << "Referentiel " << std::endl;
  std::cout << "X axis = " << _cs.getOrientation().getAxisX() << std::endl;
  std::cout << "Y axis =	" << _cs.getOrientation().getAxisY() << std::endl;
  std::cout << "Z axis =	" << _cs.getOrientation().getAxisZ() << std::endl;
  std::cout << "position =	" << _cs.getPosition() << std::endl;
#endif

  Mat4f tmp = _cs.getMatrixFrom();

#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << "matrix" << std::endl;
  std::cout << tmp << endl;
  std::cout << "normal = " << normal << std::endl;
  std::cout << "normal2 = " << normal2 << std::endl;
#endif


  //Expand the bounding box
  _d = normal.dot(_cs.getPosition());

  Vec3f dxy(_width_2, _height_2, 0.0f);
  Vec3f dx_y(_width_2, -_height_2, 0.0f);

  Mat4f tmpMat = _cs.getMatrixFrom();
  //For numeric stability
  _bbox.expandBy(tmpMat * Vec3f(0.0f, 0.0f, EPSILON));
  _bbox.expandBy(tmpMat * Vec3f(0.0f, 0.0f, -EPSILON));

  //Up Right Corner
  _bbox.expandBy(tmpMat * dxy);
  //Down Left Corner
  _bbox.expandBy(tmpMat * (-dxy));

  //Down Right Corner
  _bbox.expandBy(tmpMat * dx_y);
  //Up Left Corner
  _bbox.expandBy(tmpMat * (-dx_y));

#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  print(_bbox);
  std::cout << "  _cs [ X = " << _cs.getOrientation().getAxisX() << "; " << std::endl;
  std::cout << "  Y = " << _cs.getOrientation().getAxisY() << "; " << std::endl;
  std::cout << "  Z = " << _cs.getOrientation().getAxisZ() << " ]" << std::endl;
  std::cout << "  _d = " << _d << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "  _width_2 = " << _width_2 << std::endl;
  std::cout << "  _height_2 = " << _height_2 << std::endl;
  std::cout << "  _cs.getPosition() = " << _cs.getPosition() << std::endl;
  std::cout << "  Texture coordinates of center = " << uvByPosition(_cs.getPosition()) << std::endl;

#endif
}

RQuad::~RQuad() {}

std::vector<mrf::math::Vec3f> RQuad::getVertices() const
{
  mrf::geom::Vec3f center = position();

  auto             temp_reff = localRef();
  mrf::geom::Vec3f x_axis    = temp_reff.getOrientation().getAxisX();
  mrf::geom::Vec3f y_axis    = temp_reff.getOrientation().getAxisY();

  mrf::geom::Vec3f move_up_left(-x_axis * length(), y_axis * height());
  mrf::geom::Vec3f move_down_left(-x_axis * length(), -y_axis * height());
  mrf::geom::Vec3f move_up_right(x_axis * length(), y_axis * height());
  mrf::geom::Vec3f move_down_right(x_axis * length(), -y_axis * height());

  std::vector<mrf::geom::Vec3f> vertices;
  vertices.push_back(center + move_up_left * 0.5f);
  vertices.push_back(center + move_up_right * 0.5f);
  vertices.push_back(center + move_down_right * 0.5f);
  vertices.push_back(center + move_down_left * 0.5f);

  return vertices;
}


//-------------------------------------------------------------------------
// Meshable Interface implementation
//-------------------------------------------------------------------------

mrf::geom::Mesh RQuad::createMesh() const
{
  auto vertices = getVertices();

  std::vector<mrf::geom::Vec3f> normals;
  normals.push_back((vertices[1] - vertices[0]).cross(vertices[3] - vertices[0]).normalize());
  normals.push_back((vertices[2] - vertices[1]).cross(vertices[0] - vertices[1]).normalize());
  normals.push_back((vertices[3] - vertices[2]).cross(vertices[1] - vertices[2]).normalize());
  normals.push_back((vertices[0] - vertices[3]).cross(vertices[2] - vertices[3]).normalize());

  std::vector<mrf::geom::Vec3f> uvw;
  uvw.push_back(mrf::geom::Vec3f(0, 1, 0));
  uvw.push_back(mrf::geom::Vec3f(1, 1, 0));
  uvw.push_back(mrf::geom::Vec3f(1, 0, 0));
  uvw.push_back(mrf::geom::Vec3f(0, 0, 0));

  std::vector<unsigned int> face_indexes;
  //face_indexes.push_back(Triplet_uint(0,1,2));
  //face_indexes.push_back(Triplet_uint(2,3,0));
  face_indexes.push_back(0);
  face_indexes.push_back(1);
  face_indexes.push_back(2);
  face_indexes.push_back(2);
  face_indexes.push_back(3);
  face_indexes.push_back(0);

  std::vector<unsigned int> material_per_faces;

  return mrf::geom::Mesh(vertices, normals, uvw, face_indexes, material_per_faces);
}

//-------------------------------------------------------------------------
// Intersectable Interface implementation
//-------------------------------------------------------------------------
bool RQuad::isIntersected(mrf::lighting::Ray const &r) const
{
  Vec3f normal = _cs.getOrientation().getAxisZ();

  float dot_normal_ray_dir = normal.dot(r.dir());

  //TODO : Check if this is real Backface culling ?
  if (dot_normal_ray_dir > EPSILON)
  {
    return false;
  }


  //is this ray parallel to plane ?
  if ((dot_normal_ray_dir < EPSILON) && (dot_normal_ray_dir > -EPSILON))
  {
    return false;
  }

  float v0 = -(normal.dot(r.origin()) + _d);
  float t  = v0 / dot_normal_ray_dir;

  if ((t < -EPSILON) || (t < r.tMin()) || (t > r.tMax()))
  {
    return false;
  }
  else
  {
    // NOW TEST if the intersection is inside the Quad
    Vec3f intersection_point = r.origin() + r.dir() * t;
    return isInside(intersection_point);
  }
}

bool RQuad::isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info) const
{
  //#ifdef DEBUG
  //             std::cout << "  r = " << r << std::endl ;
  //             std::cout << __FILE__ << " ENTERING " << __LINE__ << std::endl ;
  //#endif



  Vec3f normal = _cs.getOrientation().getAxisZ();

  //#ifdef DEBUG
  //std::cout << "  normal = " << normal << std::endl ;
  //#endif


  float dot_normal_ray_dir = normal.dot(r.dir());

  if (dot_normal_ray_dir > EPSILON)
  {
    //#ifdef MRF_GEOM_QUAD_DEBUG
    //std::cout << " RQUAD BACKFACE CULLING " << std::endl;
    //#endif

    return false;
  }


  //is this ray parallel to plane ?
  if ((dot_normal_ray_dir < EPSILON) && (dot_normal_ray_dir > -EPSILON))
  {
    //                 std::cout << __FILE__ << " at " << __LINE__ << std::endl ;
    //                 std::cout << " Ray Parallell tp plane " << std::endl;
    return false;
  }



  float v0 = -(normal.dot(r.origin()) + _d);
  float t  = v0 / dot_normal_ray_dir;

  if ((t < -EPSILON) || (t < r.tMin()) || (t > r.tMax()))
  {
    //#ifdef MRF_GEOM_QUAD_DEBUG
    //                 std::cout << __FILE__ << " " << __LINE__ << std::endl ;
    //                 std::cout << "  t = " << t << __FILE__ << " " << __LINE__ << std::endl ;
    //#endif

    return false;
  }

  //NOW TEST if the intersection is inside the Quad
  Vec3f intersection_point = r.origin() + r.dir() * t;

  //#ifdef DEBUG
  //             std::cout << __FILE__ << " " << __LINE__ << std::endl ;
  //             std::cout << "  intersection_point = " << intersection_point << std::endl ;
  //             std::cout << "  t = " << t << std::endl ;
  //#endif



  if (isInside(intersection_point))
  {
    hit_info.point = intersection_point;

    // Back facing
    //We are reversing Normal if intersections occurs...
    //   if ( dot_normal_ray_dir > EPSILON )
    //             {
    //                hit_info.normal = TO_LAIR( - normal );
    //             }
    //             else
    //             {
    hit_info.normal = normal;
    //             }
    hit_info.tangent = _cs.getOrientation().getAxisX();


    hit_info.material = _material_index;
    hit_info.t        = t;
    hit_info.uv       = uvByPosition(intersection_point);
    hit_info.object   = _id;

    // #ifdef MRF_GEOM_QUAD_DEBUG
    //                std::cout << __FILE__ << " INTERSECTION FOUND RETURNING COMPLETE HIT_INFO " << __LINE__ << std::endl ;
    //             std::cout << "  hit_info.normal = " << hit_info.normal << __FILE__ << " " << __LINE__ << std::endl ;
    //             #endif

    return true;
  }
  else
  {
    //               std::cout << __FILE__ << " " << __LINE__ << std::endl ;
    //                 std::cout << " Intersecting plane but not the quad !!! " << std::endl ;

    return false;
  }
}


bool RQuad::isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int /*sub_object_id*/) const
{
  return isIntersected(r, hit_info);
}


bool RQuad::isIntersected(mrf::lighting::Ray const &r, unsigned int /*sub_object_id*/) const
{
  return isIntersected(r);
}

bool RQuad::isIntersectedWNBFC(Ray const &r, Intersection &hit_info) const
{
  Vec3f normal             = _cs.getOrientation().getAxisZ();
  float dot_normal_ray_dir = normal.dot(r.dir());

  //is this ray parallel to plane ?
  if ((dot_normal_ray_dir < EPSILON) && (dot_normal_ray_dir > -EPSILON))
  {
    //std::cout << __FILE__ << " at " << __LINE__ << std::endl ;
    //std::cout << " Ray Parallell tp plane " << std::endl;
    return false;
  }



  float v0 = -(normal.dot(r.origin()) + _d);
  float t  = v0 / dot_normal_ray_dir;

  if ((t < -EPSILON) || (t < r.tMin()) || (t > r.tMax()))
  {
    return false;
  }

  //NOW TEST if the intersection is inside the Quad
  Vec3f intersection_point = r.origin() + r.dir() * t;

  if (isInside(intersection_point))
  {
    hit_info.point  = intersection_point;
    hit_info.normal = normal;

    hit_info.material = _material_index;

    hit_info.t  = t;
    hit_info.uv = uvByPosition(intersection_point);

    hit_info.object = _id;

    return true;
  }


  return false;
}

bool RQuad::isIntersectedWNBFC(Ray const &r, Intersection &hit_info, unsigned int /*sub_object_id*/) const
{
  return isIntersectedWNBFC(r, hit_info);
}




bool RQuad::isIntersected(Vec3f const &aabbox_min, Vec3f const &aabbox_max) const
{
  MRF_AABBox extBBox(aabbox_min, aabbox_max);

  return !_bbox.isOutsideOf(extBBox);
}


bool RQuad::isIntersected(MRF_AABBox const &box, unsigned /*index_face*/) const
{
  // #ifdef MRF_GEOM_QUAD_DEBUG
  //   std::cout << "  index_face = " << index_face << std::endl;
  // #endif

  //
  return !_bbox.isOutsideOf(box);
}


bool RQuad::isIntersected(Vec3f const &min, Vec3f const &max, unsigned /*index_face*/) const
{
  // #ifdef DEBUG
  //   std::cout << "  index_face = " << index_face << std::endl;
  // #endif

  return isIntersected(min, max);
}



bool RQuad::isIntersected(MRF_AABBox const &box, std::vector<unsigned int> & /*objects*/) const
{
  // #ifdef DEBUG
  //   std::cout << "  objects.size() = " << objects.size() << std::endl;
  // #endif

  return isIntersected(box, 0);
}



bool RQuad::isIntersected(
    mrf::math::Vec3f const &min,
    mrf::math::Vec3f const &max,
    std::vector<unsigned int> & /*objects*/) const
{
  // #ifdef MRF_GEOM_QUAD_DEBUG
  //   std::cout << "  objects.size() = " << objects.size() << std::endl;
  // #endif

  return isIntersected(min, max);
}


unsigned int RQuad::intersectionCost() const
{
  return 2;
}

unsigned int RQuad::memorySize() const
{
  //TODO : Finished me
  return 2 * sizeof(float);
}

unsigned int RQuad::numSubObjects() const
{
  return 0;
}
bool RQuad::hasSubObjects() const
{
  return false;
}

MRF_AABBox RQuad::subObjectAABBox(unsigned int /*subobj_index*/) const
{
  // #ifdef MRF_GEOM_QUAD_DEBUG
  //   std::cout << "  subobj_index = " << subobj_index << std::endl;
  // #endif

  return _bbox;
}


mrf::geom::MRF_AABBox const &RQuad::bbox() const
{
  return Shape::bbox();
}

mrf::geom::MRF_AABBox &RQuad::bbox()
{
  return Shape::bbox();
}


//-------------------------------------------------------------------------
// ILight Interface implementation
//-------------------------------------------------------------------------
mrf::math::Vec3f RQuad::center() const
{
  return position();
}

float RQuad::surfaceArea() const
{
  return _length * _height;
}

float RQuad::surfaceSampling(float random_var_1, float random_var_2, mrf::geom::LocalFrame &light_local_frame) const
{
  //  float const width_2 = _length / 2.0f;
  //          float const height_2 = _height / 2.0f;

  light_local_frame.position(
      _cs.getMatrixFrom()
      * Vec3f(
          -_width_2 * (1.0f - random_var_1) + random_var_1 * _width_2,
          -_height_2 * (1.0f - random_var_2) + random_var_2 * _height_2,
          0.0f));

  light_local_frame.normal(_cs.getOrientation().getAxisZ());
  light_local_frame.tangent(_cs.getOrientation().getAxisX());

  //TODO : Check IF
  //IS this really needed ??
  light_local_frame.binormal(_cs.getOrientation().getAxisY());


#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << " Light Sample in world coordinates " << light_local_frame.position() << std::endl;
#endif

#ifdef MRF_GEOM_QUAD_DEBUG
  std::cout << "  surfaceArea() = " << surfaceArea() << std::endl;
#endif


  return 1.f / surfaceArea();
}



//TODO: Fix me
//       mrf::geom::LocalFrame const &
//       RQuad::localFrame() const
//       {
//       }



float RQuad::lightGeometricFactor(mrf::math::Vec3f const &light_vector) const
{
  //#ifdef DEBUG
  //std::cout << "  _cs.getOrientation().getAxisZ() = " << _cs.getOrientation().getAxisZ() << std::endl ;
  //#endif

  //#ifdef DEBUG
  //std::cout << "  light_vector = " << light_vector << std::endl ;
  //#endif


  float dot_N_L = _cs.getOrientation().getAxisZ().dot(light_vector);

  //#ifdef DEBUG
  //std::cout << "  dot_N_L = " << dot_N_L << std::endl ;
  //#endif


  if (dot_N_L <= -EPSILON)
  {
    return -dot_N_L;
  }
  else
  {
    return 0.0f;
  }
}


mrf::math::Vec3f RQuad::lightVector(
    mrf::math::Vec3f const &point_shaded,
    mrf::math::Vec3f const &light_position,
    float &                 distance_point_light,
    float &                 square_distance_point_light) const
{
  //#ifdef DEBUG
  //std::cout << " PROUT DE PROUT " << std::endl ;
  //#endif

  Vec3f light_vector = light_position - point_shaded;

  square_distance_point_light = light_vector.sqrLength();


#ifdef MRF_GEOM_QUAD_DEBUG
  if (square_distance_point_light < DBL_EPSILON)
  {
    std::cerr << __FILE__ << " " << __LINE__ << std::endl;
    std::cerr << " Error distance between point being shaded and light position is to small " << std::endl;
  }
#endif

  distance_point_light = sqrtf(square_distance_point_light);
  light_vector /= distance_point_light;

  //#ifdef DEBUG
  //std::cout << "  light_vector = " << light_vector << std::endl ;
  //#endif


  return light_vector;
}

//-------------------------------------------------------------------------
mrf::math::Vec2f RQuad::uvByPosition(mrf::math::Vec3f const &position) const
{
  Vec3f position_in_local_frame = _cs.getMatrixTo() * position;


  //std::cout << "  position_in_local_frame = " << position_in_local_frame << __FILE__ << " " << __LINE__ << std::endl ;


  float u = position_in_local_frame.x();
  float v = position_in_local_frame.y();


  u += _width_2;
  u /= _length;

  v += _height_2;
  v /= _height;

#ifdef MRF_GEOM_QUAD_DEBUG
  if (u > 1.0)
  {
    std::cout << "  position_in_local_frame = " << u << " " << v << __FILE__ << " " << __LINE__ << std::endl;
    std::cout << " TEXTURE U OUTSIDE RANGE (>1). Clamping. " << __FILE__ << " " << __LINE__ << std::endl;
    u = 1.0f;
  }

  if (u < 0.0)
  {
    std::cout << "  position_in_local_frame = " << u << " " << v << __FILE__ << " " << __LINE__ << std::endl;
    std::cout << " TEXTURE U OUTSIDE RANGE (<0). Clamping. " << __FILE__ << " " << __LINE__ << std::endl;
    u = 0.0f;
  }

  if (v > 1.0)
  {
    std::cout << "  position_in_local_frame = " << u << " " << v << __FILE__ << " " << __LINE__ << std::endl;
    std::cout << " TEXTURE V OUTSIDE RANGE (>1). Clamping. " << __FILE__ << " " << __LINE__ << std::endl;
    v = 1.0f;
  }

  if (v < 0.0)
  {
    std::cout << "  position_in_local_frame = " << u << " " << v << __FILE__ << " " << __LINE__ << std::endl;
    std::cout << " TEXTURE V OUTSIDE RANGE (<0). Clamping. " << __FILE__ << " " << __LINE__ << std::endl;
    v = 0.0f;
  }
#endif

  u = mrf::math::clamp(u, 0.0f, 1.0f);
  v = mrf::math::clamp(v, 0.0f, 1.0f);

  return Vec2f(u, v);
}

// Graphics Gem V Code
bool RQuad::isInside(mrf::math::Vec3f const &point) const
{
  Vec3f position_in_local_frame = _cs.getMatrixTo() * point;


  //            #ifdef  MRF_GEOM_QUADCLASS_DEBUG
  //             std::cout << "  position_in_local_frame = " << position_in_local_frame << __FILE__ << " " << __LINE__ << std::endl ;
  //             std::cout << "  _width_2 = " << _width_2 << __FILE__ << " " << __LINE__ << std::endl ;
  //             std::cout << "  _height_2 = " << _height_2 << __FILE__ << " " << __LINE__ << std::endl ;
  //            #endif

  if ((position_in_local_frame.x() >= -_width_2) && (position_in_local_frame.x() <= _width_2)
      && (position_in_local_frame.y() >= -_height_2) && (position_in_local_frame.y() <= _height_2))
  {
    return true;
  }


  return false;
}



}   // namespace geom
}   // namespace mrf
