/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>

namespace mrf
{
namespace geom
{
/**
 * This class represents a point and its local Frame (i.e. the Frenet Basis)
 *
 * Tangent = [1 0 0 ]
 * Normal  = [0 1 0 ]
 * Bitamgent  = [ 0 0 1]
 *
 * TODO: Change this to   Tangent, Bitangent, Normal order ?
 *
 * The uv vector stores the texture coordinates of the point
 **/
template<class T>
class _LocalFrame
{
protected:
  unsigned int _depth;

  mrf::math::Vec3<T> _position;
  mrf::math::Vec3<T> _normal;
  mrf::math::Vec3<T> _binormal;
  mrf::math::Vec3<T> _tangent;

  mrf::math::Vec2<T> _uv;

public:
  inline _LocalFrame();



  inline _LocalFrame(
      mrf::math::Vec3<T> const &a_position,
      mrf::math::Vec3<T> const &a_normal,
      mrf::math::Vec3<T> const &a_binormal,
      mrf::math::Vec3<T> const &a_tangent,
      mrf::math::Vec2<T> const &an_uv = mrf::math::Vec2<T>(0.0, 0.0));

  /**
   * Construct a Local Frame with a given position and normal
   * The tangent and the binormal are deduced from the the normal
   * This constructor does not used the uv to derive tangent and binormal
   **/
  inline _LocalFrame(
      mrf::math::Vec3<T> const &a_position,
      mrf::math::Vec3<T> const &a_normal,
      mrf::math::Vec2<T> const &an_uv);

  inline _LocalFrame(mrf::math::Vec3<T> const &a_position, mrf::math::Vec3<T> const &a_normal);

  inline _LocalFrame(
      mrf::math::Vec3<T> const &a_position,
      mrf::math::Vec3<T> const &a_normal,
      mrf::math::Vec3<T> const &tangent,
      mrf::math::Vec2<T> const &an_uv = mrf::math::Vec2<T>(0.0, 0.0));



  /**
   * Converts a direction expressed in the world frame into the local frame.
   **/
  inline mrf::math::Vec3<T> localDir(mrf::math::Vec3<T> const &a_world_dir) const;

  /**
   * Return the [Tangent Normal Binormal] Matrix
   * where the first row is the tangent
   * the second row is the normal
   * and the third the binormal
   **/
  inline void matrixTNB(mrf::math::Mat3<T> &mat) const;

  inline void matrixTBN(mrf::math::Mat3<T> &mat) const;

  /**
   * Return the [ Bitangent Tangent Normal ] Matrix
   * where the first row is the bitangent
   * the second the tangent
   * and the last one the normal
   **/
  inline void matrixBTN(mrf::math::Mat3<T> &mat) const;


  //-------------------------------------------------------------------------
  //
  //-------------------------------------------------------------------------

  /**
   * Returns the vector in WorldSpace coordinates
   * of a direction defined in the LocalReferential
   *
   * For example, the normal coordinates are [ 0 1 0 ]  in the
   * local frame but in world space the normal coordinates are
   * equal to the stored vector _normal.
   *
   **/
  inline mrf::math::Vec3<T> directionToWorldSpace(mrf::math::Vec3<T> const &dir) const;



  //-------------------------------------------------------------------------
  // Accessors
  //-------------------------------------------------------------------------
  inline mrf::math::Vec3<T> const &normal() const;
  inline mrf::math::Vec3<T> const &binormal() const;
  inline mrf::math::Vec3<T> const &tangent() const;
  inline mrf::math::Vec3<T> const &position() const;
  inline mrf::math::Vec2<T> const &uv() const;
  inline unsigned int              depth() const;

  //-------------------------------------------------------------------------
  // Setters
  //-------------------------------------------------------------------------
  inline void normal(mrf::math::Vec3<T> const &);
  inline void binormal(mrf::math::Vec3<T> const &);
  inline void tangent(mrf::math::Vec3<T> const &);
  inline void position(mrf::math::Vec3<T> const &);
  inline void uv(mrf::math::Vec2<T> const &);
  inline void depth(unsigned int new_depth);

  /**
   * Does not compute any tangent !!
   **/
  inline void set(mrf::math::Vec3<T> const &pos, mrf::math::Vec3<T> const &norm, mrf::math::Vec2<T> const &uv);


  inline void
  set(mrf::math::Vec3<T> const &pos,
      mrf::math::Vec3<T> const &norm,
      mrf::math::Vec3<T> const &tangent,
      mrf::math::Vec2<T> const &uv);
};


template<class T>
inline _LocalFrame<T>::_LocalFrame(): _depth(0)
{}


template<class T>
inline _LocalFrame<T>::_LocalFrame(
    mrf::math::Vec3<T> const &a_position,
    mrf::math::Vec3<T> const &a_normal,
    mrf::math::Vec3<T> const &a_binormal,
    mrf::math::Vec3<T> const &a_tangent,
    mrf::math::Vec2<T> const &an_uv)
  : _position(a_position)
  , _normal(a_normal)
  , _binormal(a_binormal)
  , _tangent(a_tangent)
  , _uv(an_uv)
{}

template<class T>
inline void _LocalFrame<T>::matrixTBN(mrf::math::Mat3<T> &mat) const
{
  //First row
  mat(0, 0) = _tangent[0];
  mat(0, 1) = _tangent[1];
  mat(0, 2) = _tangent[2];

  //Second row
  mat(1, 0) = _binormal[0];
  mat(1, 1) = _binormal[1];
  mat(1, 2) = _binormal[2];

  //Third row
  mat(2, 0) = _normal[0];
  mat(2, 1) = _normal[1];
  mat(2, 2) = _normal[2];
}


template<class T>
inline void _LocalFrame<T>::matrixTNB(mrf::math::Mat3<T> &mat) const
{
  //First row
  mat(0, 0) = _tangent[0];
  mat(0, 1) = _tangent[1];
  mat(0, 2) = _tangent[2];

  //Second row
  mat(1, 0) = _normal[0];
  mat(1, 1) = _normal[1];
  mat(1, 2) = _normal[2];

  //Third row
  mat(2, 0) = _binormal[0];
  mat(2, 1) = _binormal[1];
  mat(2, 2) = _binormal[2];
}


/**
 * Converts a direction expressed in the world frame into the local frame.
 **/
template<class T>
inline mrf::math::Vec3<T> _LocalFrame<T>::localDir(mrf::math::Vec3<T> const &a_world_dir) const
{
  mrf::math::Mat3f ref;
  matrixTNB(ref);

  mrf::math::Vec3<T> res = ref * a_world_dir;
  T const            y   = res.y();

  if (y < T(EPSILON))
  {
    res[1] = T(0.0);
    res.normalize();
  }

  return res;
}


template<class T>
inline void _LocalFrame<T>::matrixBTN(mrf::math::Mat3<T> &mat) const
{
  //First row
  mat(0, 0) = _binormal[0];
  mat(0, 1) = _binormal[1];
  mat(0, 2) = _binormal[2];

  //Second row
  mat(1, 0) = _tangent[0];
  mat(1, 1) = _tangent[1];
  mat(1, 2) = _tangent[2];

  //Third Row
  mat(2, 0) = _normal[0];
  mat(2, 1) = _normal[1];
  mat(2, 2) = _normal[2];
}


template<class T>
inline mrf::math::Vec3<T> _LocalFrame<T>::directionToWorldSpace(mrf::math::Vec3<T> const &dir) const
{
  // Note: The code below assumes a TNB order
  mrf::math::Vec3<T> const generated_dir = _tangent * dir.x() + _normal * dir.y() + _binormal * dir.z();

#ifdef MRF_GEOM_LOCALFRAME_DEBUG

  if (generated_dir.dot(_normal) < 0.0)
  {
    std::cout << " Tangent = " << _tangent << "  Normal = " << _normal << "  Bitangent =" << _binormal << std::endl;
  }

#endif
  return generated_dir;
}

template<class T>
inline _LocalFrame<T>::_LocalFrame(
    mrf::math::Vec3<T> const &a_position,
    mrf::math::Vec3<T> const &a_normal,
    mrf::math::Vec2<T> const &an_uv)
  : _position(a_position)
  , _normal(a_normal)
  , _uv(an_uv)
{
  mrf::math::Vec3f tangent = _normal.generateOrthogonal();
  tangent.normalEq();   //normalize the tangent
  mrf::math::Vec3f binormal = tangent.cross(_normal);

  _tangent  = tangent;
  _binormal = binormal;
}

template<class T>
inline _LocalFrame<T>::_LocalFrame(
    mrf::math::Vec3<T> const &a_position,
    mrf::math::Vec3<T> const &a_normal,
    mrf::math::Vec3<T> const &tangent,
    mrf::math::Vec2<T> const &an_uv)
  : _position(a_position)
  , _normal(a_normal)
  , _tangent(tangent)
  , _uv(an_uv)

{
  _binormal = _tangent.cross(_normal);
}




template<class T>
inline _LocalFrame<T>::_LocalFrame(mrf::math::Vec3<T> const &a_position, mrf::math::Vec3<T> const &a_normal)
  : _position(a_position)
  , _normal(a_normal)
  , _uv(mrf::math::Vec2f(0.0, 0.0))
{
  std::cout << __FILE__ << " " << __LINE__ << std::endl;

  //TODO :  Test other strategies for the tangent generation
  mrf::math::Vec3f tangent = _normal.generateOrthogonal();
  tangent.normalEq();   //normalize the tangent
  mrf::math::Vec3f binormal = tangent.cross(_normal);

  _tangent  = tangent;
  _binormal = binormal;
}


template<class T>
inline mrf::math::Vec3<T> const &_LocalFrame<T>::normal() const
{
  return _normal;
}

template<class T>
inline mrf::math::Vec3<T> const &_LocalFrame<T>::binormal() const
{
  return _binormal;
}

template<class T>
inline mrf::math::Vec3<T> const &_LocalFrame<T>::tangent() const
{
  return _tangent;
}

template<class T>
inline mrf::math::Vec3<T> const &_LocalFrame<T>::position() const
{
  return _position;
}

template<class T>
inline mrf::math::Vec2<T> const &_LocalFrame<T>::uv() const
{
  return _uv;
}

template<class T>
inline unsigned int _LocalFrame<T>::depth() const
{
  return _depth;
}


template<class T>
inline void _LocalFrame<T>::normal(mrf::math::Vec3<T> const &a_normal)
{
  _normal = a_normal;
}

template<class T>
inline void _LocalFrame<T>::binormal(mrf::math::Vec3<T> const &a_binormal)
{
  _binormal = a_binormal;
}

template<class T>
inline void _LocalFrame<T>::tangent(mrf::math::Vec3<T> const &a_tangent)
{
  _tangent = a_tangent;
}

template<class T>
inline void _LocalFrame<T>::position(mrf::math::Vec3<T> const &position)
{
  _position = position;
}

template<class T>
inline void _LocalFrame<T>::uv(mrf::math::Vec2<T> const &an_uv)
{
  _uv = an_uv;
}

template<class T>
inline void _LocalFrame<T>::depth(unsigned int new_depth)
{
  _depth = new_depth;
}


template<class T>
inline void
_LocalFrame<T>::set(mrf::math::Vec3<T> const &pos, mrf::math::Vec3<T> const &norm, mrf::math::Vec2<T> const &uv)
{
  _position = pos;
  _normal   = norm;
  _uv       = uv;
}

template<class T>
inline void _LocalFrame<T>::set(
    mrf::math::Vec3<T> const &pos,
    mrf::math::Vec3<T> const &norm,
    mrf::math::Vec3<T> const &tangent,
    mrf::math::Vec2<T> const &uv)
{
  _position = pos;
  _normal   = norm;
  _tangent  = tangent;
  _binormal = _tangent.cross(_normal);

#ifdef MRF_GEOM_LOCALFRAME_DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << " binormal.norm = " << _binormal.norm() << " binormal normalized = " << _binormal.normalize().norm()
            << " tangent.norm = " << _tangent.norm() << " normal = " << _normal.norm()
            << " dot( tangent, normal ) = " << _tangent.dot(_normal) << std::endl;
  assert(mrf::math::equals(_tangent.dot(_normal), 0.0f, 0.00001f));
#endif

  _uv = uv;
}


typedef _LocalFrame<float>  LocalFrame;
typedef _LocalFrame<double> LocalFrameD;

template<class T>
inline std::ostream &operator<<(std::ostream &os, _LocalFrame<T> const &lf)
{
  return os << " LocalFrame [ tangent = " << lf.tangent() << " normal = " << lf.normal()
            << " binormal = " << lf.binormal() << std::endl
            << " position = " << lf.position() << std::endl
            << " uv = " << lf.uv() << std::endl;
}


}   // namespace geom
}   // namespace mrf
