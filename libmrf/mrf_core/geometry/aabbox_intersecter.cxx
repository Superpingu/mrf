




//-------------------------------------------------------------------------
// AABBOX Intersecter class
//-------------------------------------------------------------------------



template<class VERTEX_TYPE>
bool _AABBoxIntersecter<VERTEX_TYPE>::intersect(
    VERTEX_TYPE const &box_min,
    VERTEX_TYPE const &box_max,
    VERTEX_TYPE const &ray_origin,
    VERTEX_TYPE const &ray_dir,
    float &            t_min,
    float &            t_max)
{
  t_min = -INFINITY;
  t_max = INFINITY;

  for (unsigned int i = 0; i < 3; i++)
  {
    float const invRayDir = 1.0f / ray_dir[i];
    float       tNear     = (box_min[i] - ray_origin[i]) * invRayDir;
    float       tFar      = (box_max[i] - ray_origin[i]) * invRayDir;

    if (tNear > tFar)
    {
      mrf::math::Math::swap(tNear, tFar);
    }

    t_min = (tNear > t_min) ? (tNear) : (t_min);
    t_max = (tFar < t_max) ? (tFar) : (t_max);

    if (t_min > t_max)
    {
      return false;
    }
  }

  return true;
}




template<class VERTEX_TYPE>
bool _AABBoxIntersecter<VERTEX_TYPE>::intersect(
    VERTEX_TYPE const &       box_min,
    VERTEX_TYPE const &       box_max,
    mrf::lighting::Ray const &ray,
    Intersection &            hitInfo,
    float *                   t_inter_max)
{
  float t0 = ray.tMin();
  float t1 = ray.tMax();

  mrf::math::Vec3f const &dir    = ray.dir();
  mrf::math::Vec3f const &origin = ray.origin();

  for (unsigned int i = 0; i < 3; i++)
  {
    float const invRayDir = 1.0f / dir[i];
    float       tNear     = (box_min[i] - origin[i]) * invRayDir;
    float       tFar      = (box_max[i] - origin[i]) * invRayDir;

    if (tNear > tFar)
    {
      mrf::math::Math::swap(tNear, tFar);
    }

    t0 = (tNear > t0) ? (tNear) : (t0);
    //TODO : CHECK ME ???
    //SEE intersect with particle
    // CLEARLY THIS IS THE BUG  tFar > t1 !!! CHanged it !!!
    t1 = (tFar < t1) ? (tFar) : (t1);

    if (t0 > t1)
    {
      return false;
    }
  }




  hitInfo.point = origin + dir * t0;
  hitInfo.t     = t0;

  if (t_inter_max)
  {
    *t_inter_max = t1;
  }

  return true;
}



template<class VERTEX_TYPE>
bool _AABBoxIntersecter<VERTEX_TYPE>::intersect(
    VERTEX_TYPE const &            box_min,
    VERTEX_TYPE const &            box_max,
    mrf::lighting::Particle const &particle,
    Intersection &                 hitInfo,
    float *                        t_inter_max)
{
  float t0 = EPSILON;
  float t1 = INFINITY;


  mrf::math::Vec3f dir    = particle.direction;
  mrf::math::Vec3f origin = particle.position;

  for (unsigned int i = 0; i < 3; i++)
  {
    float invRayDir = 1.0f / dir[i];
    float tNear     = (box_min[i] - origin[i]) * invRayDir;
    float tFar      = (box_max[i] - origin[i]) * invRayDir;


    if (tNear > tFar)
    {
      mrf::math::Math::swap(tNear, tFar);
    }

    t0 = (tNear > t0) ? (tNear) : (t0);
    t1 = (tFar < t1) ? (tFar) : (t1);

    if (t0 > t1)
    {
      return false;
    }
  }


  hitInfo.point = origin + dir * t0;
  hitInfo.t     = t0;

  if (t_inter_max)
  {
    *t_inter_max = t1;
  }

  return true;
}




template<class VERTEX_TYPE>
bool _AABBoxIntersecter<VERTEX_TYPE>::intersectSegment(
    VERTEX_TYPE const &box_min,
    VERTEX_TYPE const &box_max,
    VERTEX_TYPE const &a,
    VERTEX_TYPE const &b)
{
  float x1 = a[0];
  float x2 = b[0];
  float y1 = a[1];
  float y2 = b[1];
  float z1 = a[2];
  float z2 = b[2];


  float txmin, txmax, tymin, tymax, tzmin, tzmax;

  mrf::math::Vec3f d  = mrf::math::Vec3f(x2, y2, z2) - mrf::math::Vec3f(x1, y1, z1);
  float            tb = d.norm();
  d.normalize();


  if (d[0] >= 0)
  {
    txmin = (box_min[0] - x1) / d[0];
    txmax = (box_max[0] - x1) / d[0];
  }
  else
  {
    txmin = (box_max[0] - x1) / d[0];
    txmax = (box_min[0] - x1) / d[0];
  }


  if (d[1] >= 0)
  {
    tymin = (box_min[1] - y1) / d[1];
    tymax = (box_max[1] - y1) / d[1];
  }
  else
  {
    tymin = (box_max[1] - y1) / d[1];
    tymax = (box_min[1] - y1) / d[1];
  }

  if (d[2] >= 0)
  {
    tzmin = (box_min[2] - z1) / d[2];
    tzmax = (box_max[2] - z1) / d[2];
  }
  else
  {
    tzmin = (box_max[2] - z1) / d[2];
    tzmax = (box_min[2] - z1) / d[2];
  }

  // Checks if the line intersects the box
  // and after checks if the segment intersect
  float t1xy, t2xy;
  if (mrf::math::Math::intervalOverlap(txmin, txmax, tymin, tymax, t1xy, t2xy))
  {
    float t1, t2;
    if (mrf::math::Math::intervalOverlap(t1xy, t2xy, tzmin, tzmax, t1, t2))
    {
      if (mrf::math::Math::intervalOverlap(t1, t2, 0, tb))
      {
        return true;
      }
    }
  }

  return false;
}


#define AKENINE 1
#ifdef AKENINE

#  define AKENINE_X 0
#  define AKENINE_Y 1
#  define AKENINE_Z 2


#  define CROSS(dest, v1, v2)                                                                                          \
    dest[0] = v1[1] * v2[2] - v1[2] * v2[1];                                                                           \
    dest[1] = v1[2] * v2[0] - v1[0] * v2[2];                                                                           \
    dest[2] = v1[0] * v2[1] - v1[1] * v2[0];

#  define DOT(v1, v2) (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2])

#  define SUB(dest, v1, v2)                                                                                            \
    dest[0] = v1[0] - v2[0];                                                                                           \
    dest[1] = v1[1] - v2[1];                                                                                           \
    dest[2] = v1[2] - v2[2];

#  define FINDMINMAX(x0, x1, x2, min, max)                                                                             \
    min = max = x0;                                                                                                    \
    if (x1 < min) min = x1;                                                                                            \
    if (x1 > max) max = x1;                                                                                            \
    if (x2 < min) min = x2;                                                                                            \
    if (x2 > max) max = x2;



/*======================== X-tests ========================*/

#  define AXISTEST_X01(a, b, fa, fb)                                                                                   \
    p0 = a * v0[AKENINE_Y] - b * v0[AKENINE_Z];                                                                        \
    p2 = a * v2[AKENINE_Y] - b * v2[AKENINE_Z];                                                                        \
    if (p0 < p2)                                                                                                       \
    {                                                                                                                  \
      min = p0;                                                                                                        \
      max = p2;                                                                                                        \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
      min = p2;                                                                                                        \
      max = p0;                                                                                                        \
    }                                                                                                                  \
    rad = fa * boxhalfsize[AKENINE_Y] + fb * boxhalfsize[AKENINE_Z];                                                   \
    if (min > rad || max < -rad) return 0;

#  define AXISTEST_X2(a, b, fa, fb)                                                                                    \
    p0 = a * v0[AKENINE_Y] - b * v0[AKENINE_Z];                                                                        \
    p1 = a * v1[AKENINE_Y] - b * v1[AKENINE_Z];                                                                        \
    if (p0 < p1)                                                                                                       \
    {                                                                                                                  \
      min = p0;                                                                                                        \
      max = p1;                                                                                                        \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
      min = p1;                                                                                                        \
      max = p0;                                                                                                        \
    }                                                                                                                  \
    rad = fa * boxhalfsize[AKENINE_Y] + fb * boxhalfsize[AKENINE_Z];                                                   \
    if (min > rad || max < -rad) return 0;




/*======================== Y-tests ========================*/

#  define AXISTEST_Y02(a, b, fa, fb)                                                                                   \
    p0 = -a * v0[AKENINE_X] + b * v0[AKENINE_Z];                                                                       \
    p2 = -a * v2[AKENINE_X] + b * v2[AKENINE_Z];                                                                       \
    if (p0 < p2)                                                                                                       \
    {                                                                                                                  \
      min = p0;                                                                                                        \
      max = p2;                                                                                                        \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
      min = p2;                                                                                                        \
      max = p0;                                                                                                        \
    }                                                                                                                  \
    rad = fa * boxhalfsize[AKENINE_X] + fb * boxhalfsize[AKENINE_Z];                                                   \
    if (min > rad || max < -rad) return 0;



#  define AXISTEST_Y1(a, b, fa, fb)                                                                                    \
    p0 = -a * v0[AKENINE_X] + b * v0[AKENINE_Z];                                                                       \
    p1 = -a * v1[AKENINE_X] + b * v1[AKENINE_Z];                                                                       \
    if (p0 < p1)                                                                                                       \
    {                                                                                                                  \
      min = p0;                                                                                                        \
      max = p1;                                                                                                        \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
      min = p1;                                                                                                        \
      max = p0;                                                                                                        \
    }                                                                                                                  \
    rad = fa * boxhalfsize[AKENINE_X] + fb * boxhalfsize[AKENINE_Z];                                                   \
    if (min > rad || max < -rad) return 0;



/*======================== Z-tests ========================*/
#  define AXISTEST_Z12(a, b, fa, fb)                                                                                   \
    p1 = a * v1[AKENINE_X] - b * v1[AKENINE_Y];                                                                        \
    p2 = a * v2[AKENINE_X] - b * v2[AKENINE_Y];                                                                        \
    if (p2 < p1)                                                                                                       \
    {                                                                                                                  \
      min = p2;                                                                                                        \
      max = p1;                                                                                                        \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
      min = p1;                                                                                                        \
      max = p2;                                                                                                        \
    }                                                                                                                  \
    rad = fa * boxhalfsize[AKENINE_X] + fb * boxhalfsize[AKENINE_Y];                                                   \
    if (min > rad || max < -rad) return 0;



#  define AXISTEST_Z0(a, b, fa, fb)                                                                                    \
    p0 = a * v0[AKENINE_X] - b * v0[AKENINE_Y];                                                                        \
    p1 = a * v1[AKENINE_X] - b * v1[AKENINE_Y];                                                                        \
    if (p0 < p1)                                                                                                       \
    {                                                                                                                  \
      min = p0;                                                                                                        \
      max = p1;                                                                                                        \
    }                                                                                                                  \
    else                                                                                                               \
    {                                                                                                                  \
      min = p1;                                                                                                        \
      max = p0;                                                                                                        \
    }                                                                                                                  \
    rad = fa * boxhalfsize[AKENINE_X] + fb * boxhalfsize[AKENINE_Y];                                                   \
    if (min > rad || max < -rad) return 0;



inline int planeBoxOverlap(float normal[3], float vert[3], float maxbox[3])   // -NJMP-
{
  int   q;
  float vmin[3], vmax[3], v;

  for (q = AKENINE_X; q <= AKENINE_Z; q++)
  {
    v = vert[q];   // -NJMP-
    if (normal[q] > 0.0f)
    {
      vmin[q] = -maxbox[q] - v;   // -NJMP-
      vmax[q] = maxbox[q] - v;    // -NJMP-
    }
    else
    {
      vmin[q] = maxbox[q] - v;    // -NJMP-
      vmax[q] = -maxbox[q] - v;   // -NJMP-
    }
  }

  if (DOT(normal, vmin) > 0.0f) return 0;    // -NJMP-
  if (DOT(normal, vmax) >= 0.0f) return 1;   // -NJMP-

  return 0;
}



// template< class VERTEX_TYPE>
// int
// _AABBoxIntersecter< VERTEX_TYPE>::planeBoxOverlap(VERTEX_TYPE & normal, VERTEX_TYPE & vert, VERTEX_TYPE & maxbox)
// {
//   unsigned int const X = 0;
//   unsigned int const Y = 1;
//   unsigned int const Z = 2;

//   int q;

//   VERTEX_TYPE vmin;
//   VERTEX_TYPE vmax;
//   float v;

//   for(q=X;q<=Z;q++)
//   {
//     v=vert[q];          // -NJMP-

//     if(normal[q]>0.0f)
//     {
//       vmin[q]=-maxbox[q] - v; // -NJMP-
//       vmax[q]= maxbox[q] - v; // -NJMP-
//     }
//     else
//     {
//       vmin[q]= maxbox[q] - v; // -NJMP-
//       vmax[q]=-maxbox[q] - v; // -NJMP-
//     }
//   }


//   if( normal.dot(vmin) > 0.0f )
//   {
//     return 0; // -NJMP-
//   }

//   if( normal.dot(vmax) >= 0.0f)
//   {
//     return 1;  // -NJMP-
//   }

//   return 0;
// }


inline int triBoxOverlap(float boxcenter[3], float boxhalfsize[3], float triverts[3][3])
{
  /*    use separating axis theorem to test overlap between triangle and box */
  /*    need to test for overlap in these directions: */
  /*    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
  /*       we do not even need to test these) */
  /*    2) normal of the triangle */
  /*    3) crossproduct(edge from tri, {x,y,z}-directin) */
  /*       this gives 3x3=9 more tests */

  float v0[3], v1[3], v2[3];

  float min, max, p0, p1, p2, rad, fex, fey, fez;   // -NJMP- "d" local variable removed
  float normal[3], e0[3], e1[3], e2[3];

  /* move everything so that the boxcenter is in (0,0,0) */

  SUB(v0, triverts[0], boxcenter);
  SUB(v1, triverts[1], boxcenter);
  SUB(v2, triverts[2], boxcenter);

  /* compute triangle edges */
  SUB(e0, v1, v0); /* tri edge 0 */
  SUB(e1, v2, v1); /* tri edge 1 */
  SUB(e2, v0, v2); /* tri edge 2 */

  /* Bullet 3:  */
  /*  test the 9 tests first (this was faster) */
  fex = fabsf(e0[AKENINE_X]);
  fey = fabsf(e0[AKENINE_Y]);
  fez = fabsf(e0[AKENINE_Z]);
  AXISTEST_X01(e0[AKENINE_Z], e0[AKENINE_Y], fez, fey);
  AXISTEST_Y02(e0[AKENINE_Z], e0[AKENINE_X], fez, fex);
  AXISTEST_Z12(e0[AKENINE_Y], e0[AKENINE_X], fey, fex);

  fex = fabsf(e1[AKENINE_X]);
  fey = fabsf(e1[AKENINE_Y]);
  fez = fabsf(e1[AKENINE_Z]);

  AXISTEST_X01(e1[AKENINE_Z], e1[AKENINE_Y], fez, fey);
  AXISTEST_Y02(e1[AKENINE_Z], e1[AKENINE_X], fez, fex);
  AXISTEST_Z0(e1[AKENINE_Y], e1[AKENINE_X], fey, fex);

  fex = fabsf(e2[AKENINE_X]);
  fey = fabsf(e2[AKENINE_Y]);
  fez = fabsf(e2[AKENINE_Z]);

  AXISTEST_X2(e2[AKENINE_Z], e2[AKENINE_Y], fez, fey);
  AXISTEST_Y1(e2[AKENINE_Z], e2[AKENINE_X], fez, fex);
  AXISTEST_Z12(e2[AKENINE_Y], e2[AKENINE_X], fey, fex);

  /* Bullet 1: */
  /*  first test overlap in the {x,y,z}-directions */
  /*  find min, max of the triangle each direction, and test for overlap in */
  /*  that direction -- this is equivalent to testing a minimal AABB around */
  /*  the triangle against the AABB */

  /* test in X-direction */
  FINDMINMAX(v0[AKENINE_X], v1[AKENINE_X], v2[AKENINE_X], min, max);
  if (min > boxhalfsize[AKENINE_X] || max < -boxhalfsize[AKENINE_X]) return 0;

  /* test in Y-direction */
  FINDMINMAX(v0[AKENINE_Y], v1[AKENINE_Y], v2[AKENINE_Y], min, max);
  if (min > boxhalfsize[AKENINE_Y] || max < -boxhalfsize[AKENINE_Y]) return 0;

  /* test in Z-direction */
  FINDMINMAX(v0[AKENINE_Z], v1[AKENINE_Z], v2[AKENINE_Z], min, max);
  if (min > boxhalfsize[AKENINE_Z] || max < -boxhalfsize[AKENINE_Z]) return 0;


  /* Bullet 2: */
  /*  test if the box intersects the plane of the triangle */
  /*  compute plane equation of triangle: normal*x+d=0 */
  CROSS(normal, e0, e1);

  // -NJMP- (line removed here)
  if (!planeBoxOverlap(normal, v0, boxhalfsize)) return 0;   // -NJMP-

  return 1; /* box and triangle overlaps */
}



template<class VERTEX_TYPE>
bool _AABBoxIntersecter<VERTEX_TYPE>::intersectTriangle(
    AABBox const &     box,
    VERTEX_TYPE const &v1,
    VERTEX_TYPE const &v2,
    VERTEX_TYPE const &v3)
{
  // Code Adpated from: http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/tribox3.txt
  // Supposed to be much faster and more robust?
  // unsigned int const X = 0;
  // unsigned int const Y = 1;
  // unsigned int const Z = 2;


  VERTEX_TYPE const box_center = box.center();
  float             boxcenter[3];
  boxcenter[0] = box_center.x();
  boxcenter[1] = box_center.y();
  boxcenter[2] = box_center.z();

  float             boxhalfsize[3];
  VERTEX_TYPE const mrf_boxhalfsize = (box.max() - box.min()) * 0.5;
  boxhalfsize[0]                    = mrf_boxhalfsize[0];
  boxhalfsize[1]                    = mrf_boxhalfsize[1];
  boxhalfsize[2]                    = mrf_boxhalfsize[2];


  float triverts[3][3];
  triverts[0][0] = v1.x();
  triverts[0][1] = v1.y();
  triverts[0][2] = v1.z();
  triverts[1][0] = v2.x();
  triverts[1][1] = v2.y();
  triverts[1][2] = v2.z();
  triverts[2][0] = v3.x();
  triverts[2][1] = v3.y();
  triverts[2][2] = v3.z();


  if (triBoxOverlap(boxcenter, boxhalfsize, triverts) == 1)
  {
    return true;
  }
  return false;
}




// BELOW IT THE PREVIOUS MRF IMPLEMENTATION
#else
template<class VERTEX_TYPE>
bool _AABBoxIntersecter<VERTEX_TYPE>::intersectTriangle(
    AABBox const &     box,
    VERTEX_TYPE const &v1,
    VERTEX_TYPE const &v2,
    VERTEX_TYPE const &v3)
{
  //Test if the vertices belongs to the AABBOX
  if (box.contains(v1) || box.contains(v2) || box.contains(v3))
  {
    return true;
  }

  //Test if the segments intersect each others
  if (AABBoxIntersecter::intersectSegment(box.min(), box.max(), v1, v2)
      || AABBoxIntersecter::intersectSegment(box.min(), box.max(), v2, v3)
      || AABBoxIntersecter::intersectSegment(box.min(), box.max(), v1, v3))
  {
    return true;
  }

  //Test if the triangle is completely outside the bbox but still going through it
  std::vector<VERTEX_TYPE> diagos;

  Computation::computeBoxGreatDiagonalV(box.min(), box.max(), diagos);
  VERTEX_TYPE n = (v2 - v1).cross(v3 - v1);

  float        max_dot       = VERTEX_TYPE::dot(n, diagos[0]);
  unsigned int max_dot_index = 0;
  float        current_dot   = 0.0f;

  for (unsigned int i = 1; i < diagos.size(); i++)
  {
    current_dot = Vec3f::dot(n, diagos[i]);

    if (current_dot > max_dot)
    {
      max_dot       = current_dot;
      max_dot_index = i;
    }
  }

  diagos[max_dot_index].normalize();
  mrf::lighting::Ray the_ray(box.min(), diagos[max_dot_index]);

  the_ray.setValidDistances(-INFINITY, INFINITY);

  // Ugly but we need to refactor Ray to have a full template class
  mrf::math::Vec3f new_origin;
  if (max_dot_index == 0)
  {
    //new_origin = box.min();
    the_ray.setOrigin(box.min());
  }
  else if (max_dot_index == 1)
  {
    the_ray.setOrigin(box.max()[0], box.min()[1], box.min()[2]);
  }
  else if (max_dot_index == 2)
  {
    the_ray.setOrigin(box.max()[0], box.min()[1], box.max()[2]);
  }
  else
  {
    the_ray.setOrigin(box.min()[0], box.min()[1], box.max()[2]);
  }

  _Intersection<VERTEX_TYPE> hit_info;

  if (_TriaIntersecter<VERTEX_TYPE>::intersect(the_ray, v1, v2, v3, hit_info))
  {
    if (box.contains(hit_info.point))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  return false;
}
#endif
