/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/vec3.hpp>
#include <mrf_core/lighting/i_light.hpp>


namespace mrf
{
namespace geom
{
/**
 * A simple point which implements the ILight interface.
 * From  surfacic view this can be viewed as a unit sphere
 *
 **/
class MRF_CORE_EXPORT Point: public mrf::lighting::ILight
{
protected:
  mrf::math::Vec3f _position;
  //LocalFrame _lf;
  MRF_AABBox _aabbox;

public:
  Point(float x, float y, float z);
  virtual ~Point();

  virtual mrf::math::Vec3f center() const;


  //-------------------------------------------------------------------------
  // ILight interface implementation
  //-------------------------------------------------------------------------
  virtual mrf::geom::MRF_AABBox &bbox();
  virtual float                  surfaceArea() const;

  virtual float surfaceSampling(float random_var_1, float random_var_2, mrf::geom::LocalFrame &light_local_frame) const;

  //          virtual mrf::geom::LocalFrame const & localFrame() const;


  virtual float lightGeometricFactor(mrf::math::Vec3f const &light_vector) const;

  virtual mrf::math::Vec3f lightVector(
      mrf::math::Vec3f const &point_shaded,
      mrf::math::Vec3f const &light_position,
      float &                 distance_point_light,
      float &                 square_distance_point_light) const;
};



}   // namespace geom
}   // namespace mrf
