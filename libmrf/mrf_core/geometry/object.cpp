#include <mrf_core/geometry/object.hpp>

namespace mrf
{
namespace geom
{
using namespace mrf::math;
using namespace std;

unsigned int Object::id_counter = 1;   //Unique ID of each Object

Object::Object(unsigned int mat_index): _id(id_counter), _material_index(mat_index)
{
#ifdef MRF_GEOM_OBJECT_DEBUG
  cout << __FILE__ << " " << __LINE__ << endl;
  cout << " Object current id " << _id << endl;
#endif


  id_counter++;
}


Object::Object(unsigned int mat_index, Vec3f const &min_bbox, Vec3f const &max_bbox)
  : _id(id_counter)
  , _material_index(mat_index)
  , _bbox(min_bbox, max_bbox)

{
#ifdef MRF_GEOM_OBJECT_DEBUG
  cout << " Object current id " << _id << std::endl;
#endif

  id_counter++;
}




Object::Object(unsigned int mat_index, MRF_AABBox const &bbox): _id(id_counter), _material_index(mat_index), _bbox(bbox)
{
#ifdef MRF_GEOM_OBJECT_DEBUG
  cout << " Object current id " << _id << std::endl;
#endif

  id_counter++;
}


Object::~Object()
{
  id_counter--;
}


unsigned int Object::memorySize() const
{
  return (
      mrf::geom::memorySize(_bbox) + sizeof(unsigned int) +   // material index number
      sizeof(unsigned int));                                  // id
}
}   // namespace geom
}   // namespace mrf
