/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/artraw_spectral_image.hpp>
#include <iomanip>

using mrf::data_struct::Array2D;

namespace mrf
{
namespace image
{
ArtRawSpectralImage::ArtRawSpectralImage(
    size_t width,
    size_t height,
    uint   start_wavelength,
    uint   step_wavelength,
    uint   nb_wavelength)
  : PolarisedSpectralImage(
      width,
      height,
      start_wavelength,
      step_wavelength,
      nb_wavelength,
      mrf::radiometry::SPECTRUM_EMISSIVE)
  , _version(2.4f)
  , _creation_date("08.09.1989 09:00")
  , _program("unknown")
  , _platform("unknown")
  , _command_line("")
  , _render_time("0")
  , _samples_per_pixel("0/0/0")
  , _dpiX(72.f)
  , _dpiY(72.f)
  , _spectral(true)
  , _alphaChannel(width, height)
{}

ArtRawSpectralImage::ArtRawSpectralImage(size_t width, size_t height, const std::vector<uint> &wavelengths)
  : PolarisedSpectralImage(width, height, wavelengths, mrf::radiometry::SPECTRUM_EMISSIVE)
  , _version(2.4f)
  , _creation_date("08.09.1989 09:00")
  , _program("unknown")
  , _platform("unknown")
  , _command_line("")
  , _render_time("0")
  , _samples_per_pixel("0/0/0")
  , _dpiX(72.f)
  , _dpiY(72.f)
  , _spectral(true)
  , _alphaChannel(width, height)
{}

ArtRawSpectralImage::ArtRawSpectralImage(const std::string &filename, std::function<void(int)> progress)
  : PolarisedSpectralImage(mrf::radiometry::SPECTRUM_EMISSIVE)
{
  IMAGE_LOAD_SAVE_FLAGS status = load(filename, progress);

  if (status != MRF_NO_ERROR)
  {
    throw status;
  }
}


ArtRawSpectralImage::ArtRawSpectralImage(std::istream &is, std::function<void(int)> progress): PolarisedSpectralImage()
{
  auto status = load(is, progress);

  if (status != MRF_NO_ERROR)
  {
    throw status;
  }
}


ArtRawSpectralImage::ArtRawSpectralImage(UniformSpectralImage const &other)
  : PolarisedSpectralImage(other, mrf::radiometry::SPECTRUM_EMISSIVE)
  , _version(2.4f)
  , _creation_date("08.09.1989 09:00")
  , _program("unknown")
  , _platform("unknown")
  , _command_line("")
  , _render_time("0")
  , _samples_per_pixel("0/0/0")
  , _dpiX(72.f)
  , _dpiY(72.f)
  , _spectral(true)
  , _alphaChannel(other.width(), other.height())
{
  _alphaChannel.setOnes();
}


ArtRawSpectralImage::~ArtRawSpectralImage() {}


//-------------------------------------------------------------------------
// Save routines
//-------------------------------------------------------------------------

IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::save(
    const std::string &      file_path,
    const std::string &      additional_comments,
    std::function<void(int)> progress) const
{
  std::ofstream ofs(file_path, std::ofstream::out);
  std::cout << "Saving at " << file_path << std::endl;
  IMAGE_LOAD_SAVE_FLAGS status = save(ofs, additional_comments, progress);

  ofs.close();

  return status;
}


IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::save(
    std::ostream &os,
    const std::string & /*additional_comments*/,
    std::function<void(int)> progress) const
{
  // TODO: cannot save polarised images
  if (polarised())
  {
    return MRF_ERROR_SAVING_FILE;
  }

  IMAGE_LOAD_SAVE_FLAGS status;
  status = writeHeader(os);

  if (status != MRF_NO_ERROR)
  {
    return status;
  }

  return writeImageData(os, progress);
}


//-------------------------------------------------------------------------
// Getter / setters metadata
//-------------------------------------------------------------------------

void ArtRawSpectralImage::setPlatformName(const std::string &platform_desc)
{
  _platform = platform_desc;
}


void ArtRawSpectralImage::setCreationProgram(const std::string &creation_program)
{
  _program = creation_program;
}


void ArtRawSpectralImage::setCreationCommandLine(int argc, char *argv[])
{
  _command_line = "";

  for (int i = 0; i < argc; i++)
  {
    _command_line += argv[i];
    _command_line += " ";
  }
}

void ArtRawSpectralImage::updateMetadata()
{
  //auto retVal = UniformSpectralImage::metadata();
  std::stringstream ss;

  ss << std::fixed << std::setprecision(1) << _version;
  _metadata["ART Raw version:"] = ss.str();

  ss.str(std::string());

  _metadata[TOKEN_CREATION_DATE] = _creation_date;
  if (_version < 2.4f)
  {
    _metadata[TOKEN_CREATION_VERSION] = _creation_version;
  }
  else
  {
    _metadata[TOKEN_PROGRAM]           = _program;
    _metadata[TOKEN_PLATFORM]          = _platform;
    _metadata[TOKEN_COMMAND_LINE]      = _command_line;
    _metadata[TOKEN_RENDER_TIME]       = _render_time;
    _metadata[TOKEN_SAMPLES_PER_PIXEL] = _samples_per_pixel;
  }

  ss << width() << " x " << height();
  _metadata["Dimensions:"] = ss.str();
  ss.str(std::string());

  ss << std::fixed << std::setprecision(1) << _dpiX << " x " << std::fixed << std::setprecision(1) << _dpiY;
  _metadata["DPI:"] = ss.str();
  ss.str(std::string());

  ss << (polarised() ? "polarised" : "plain") << " " << (_spectral ? "spectrum" : "CIEXYZ") << " ";
  if (_spectral)
  {
    ss << " with " << _wavelengths.size() << " samples";
  }
  _metadata["Image type:"] = ss.str();
}

//-------------------------------------------------------------------------
// Read routines
//-------------------------------------------------------------------------

IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::load(const std::string &file_path, std::function<void(int)> progress)
{
  std::ifstream ifs(file_path, std::ifstream::in);

  IMAGE_LOAD_SAVE_FLAGS status = load(ifs, progress);

  ifs.close();

  return status;
}


IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::load(std::istream &is, std::function<void(int)> progress)
{
  IMAGE_LOAD_SAVE_FLAGS status;

  size_t              width, height;
  size_t              n_channels;
  std::vector<double> bounds;

  status = readHeader(is, width, height, n_channels, bounds);


  if (status != MRF_NO_ERROR)
  {
    return status;
  }

  status = readImageData(is, width, height, n_channels, bounds, progress);

  //Comes at the end since _wavelengths is resized in readImageData
  updateMetadata();

  return status;
}


IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::readHeader(
    std::istream &       is,
    size_t &             width,
    size_t &             height,
    size_t &             n_channels,
    std::vector<double> &bounds)
{
  std::function<void(std::string & s)> trim = [](std::string &s) {
    size_t p = s.find_first_not_of(" \t\n");
    s.erase(0, p);
    p = s.find_last_not_of(" \t\n");
    if (std::string::npos != p)
    {
      s.erase(p + 1);
    }
  };

  try
  {
    std::string       trash;
    std::string       strBuff;
    std::stringstream strStream;

    std::getline(is, strBuff);
    strStream = std::stringstream(strBuff.substr(strlen("ART RAW image format ")));
    strStream >> _version;

    // Check version
    if (_version != 2.2f && _version != 2.3f && _version != 2.4f && _version != 2.5f)
    {
      return MRF_ERROR_DECODING_FILE;
    }

    // Metadata

    std::getline(is, strBuff);   // <empty>

    if (_version < 2.4f)
    {
      // Created by version:
      std::getline(is, strBuff);
      _creation_version = strBuff.substr(strlen(TOKEN_CREATION_VERSION));
      trim(_creation_version);

      // Creation date:
      std::getline(is, strBuff);
      _creation_date = strBuff.substr(strlen(TOKEN_CREATION_DATE));
      trim(_creation_date);
    }
    else
    {
      // File created by:
      std::getline(is, strBuff);
      _program = strBuff.substr(strlen(TOKEN_PROGRAM));
      trim(_program);

      // Platform:
      std::getline(is, strBuff);
      _platform = strBuff.substr(strlen(TOKEN_PLATFORM));
      trim(_platform);

      // Command line:
      std::getline(is, strBuff);
      _command_line = strBuff.substr(strlen(TOKEN_COMMAND_LINE));
      trim(_command_line);

      // Creation date:
      std::getline(is, strBuff);
      _creation_date = strBuff.substr(strlen(TOKEN_CREATION_DATE));
      trim(_creation_date);

      // Render time:
      std::getline(is, strBuff);
      _render_time = strBuff.substr(strlen(TOKEN_RENDER_TIME));
      trim(_render_time);

      // Samples per pixel:
      std::getline(is, strBuff);
      _samples_per_pixel = strBuff.substr(strlen(TOKEN_SAMPLES_PER_PIXEL));
      trim(_samples_per_pixel);
    }

    // width height
    std::getline(is, strBuff);
    strStream = std::stringstream(strBuff.substr(strlen("Image size:         ")));
    strStream >> width;
    strStream >> trash;
    strStream >> height;
    assert(width > 0 && height > 0);


    std::getline(is, strBuff);

    // DPI if specified, default to 72dpi
    if (strBuff[0] == 'D')
    {
      strStream = std::stringstream(strBuff.substr(strlen("DPI:                ")));
      strStream >> _dpiX;
      strStream >> trash;
      strStream >> _dpiY;
    }

    assert(_dpiX > 0.f && _dpiY > 0.f);

    if (strBuff[0] == 'W')
    {
      // TODO
      return MRF_ERROR_DECODING_FILE;
    }

    // Polarisation & colour information
    std::string polarisationState, dataType;

    std::getline(is, strBuff);
    strStream = std::stringstream(strBuff.substr(strlen("Image type:         ")));
    strStream >> polarisationState;   // [plain / polarised]
    strStream >> dataType;            // [spectrum / CIEXYZ]
    strStream >> trash;               // with
    strStream >> n_channels;          // N
    strStream >> trash;               // samples

    if (polarisationState == "plain")
    {
      _spectrumType = mrf::radiometry::SPECTRUM_EMISSIVE;
    }
    else if (polarisationState == "polarised")
    {
      _spectrumType = mrf::radiometry::SPECTRUM_EMISSIVE | mrf::radiometry::SPECTRUM_POLARISED;
    }
    else
    {
      std::cerr << "Cannot read polarisation state" << std::endl;
      return MRF_ERROR_DECODING_FILE;
    }

    if (dataType == "CIEXYZ")
    {
      _spectral = false;
    }
    else if (dataType == "spectrum")
    {
      _spectral = true;
    }
    else
    {
      std::cerr << "Cannot read data type" << std::endl;
      return MRF_ERROR_DECODING_FILE;
    }

    std::getline(is, strBuff);   // <empty>

    // Wavelength bounds
    std::getline(is, strBuff);

    strStream = std::stringstream(strBuff.substr(strlen("Sample bounds in nanometers: ")));
    bounds.resize(n_channels + 1);

    for (size_t c = 0; c < n_channels + 1; c++)
    {
      strStream >> bounds[c];
    }

    std::getline(is, strBuff);   // \n
    std::getline(is, strBuff);   // Big-endian binary coded IEEE float pixel values in scanline order follow:

    char c;
    is.get(c);   // X

    if (c != 'X')
    {
      return MRF_ERROR_DECODING_FILE;
    }
  }
  catch (std::exception &)
  {
    return MRF_ERROR_DECODING_FILE;
  }

  return MRF_NO_ERROR;
}


IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::readImageData(
    std::istream &             is,
    size_t                     width,
    size_t                     height,
    size_t                     n_channels,
    const std::vector<double> &bounds,
    std::function<void(int)>   progress)
{
  std::function<float(const char *buff)> getBuffer = [](const char *buff) {
    union
    {
      float        f;
      unsigned int i;
    } value;

    value.i = buff[3] & 0xff;
    value.i <<= 8;
    value.i += buff[2] & 0xff;
    value.i <<= 8;
    value.i += buff[1] & 0xff;
    value.i <<= 8;
    value.i += buff[0] & 0xff;

    return value.f;
  };

  // Populate wavelengths
  std::vector<mrf::uint> wavelengths(n_channels);

  for (size_t c = 0; c < n_channels; c++)
  {
    wavelengths[c] = static_cast<uint>((bounds[c] + bounds[c + 1]) / 2);
  }

  // Allocate memory for image
  allocateMemory(width, height, wavelengths);
  zeros();

  // Allocate memory for alpha channel (ART RAW specific)
  _alphaChannel = Array2D(width, height);

  if (polarised())
  {
    char *readBytes = new char[4 * (n_channels * 4 + 1)];   // 4 stokes and 1 alpha

    // Convert pixels bytes to float
    for (size_t y = 0; y < height; y++)
    {
      for (size_t i = 0; i < width / 8 + 1; i++)
      {
        // This flag byte determines if there is any polarisation data for the
        // next 8 pixels
        char flagByte = 0;
        is.read(&flagByte, 1);

        for (size_t j = 0; j < 8; j++)
        {
          const size_t x = i * 8 + j;

          if (x < width)
          {
            const size_t n_stokes_at_px = (flagByte & 0x80) ? 4 : 1;
            is.read(readBytes, 4 * (n_channels * n_stokes_at_px + 1));

            for (size_t lambda = 0; lambda < n_channels; lambda++)
            {
              for (size_t s = 0; s < n_stokes_at_px; s++)
              {
                emissiveFramebuffer(s)[lambda](y, x) = getBuffer(&readBytes[4 * (n_channels * s + lambda)]);
              }
            }

            _alphaChannel(y, x) = getBuffer(&readBytes[4 * (n_channels * n_stokes_at_px)]);
          }

          flagByte = flagByte << 1;
        }
      }
      progress(int(100.f * float(y) / float(height - 1)));
    }

    delete[] readBytes;
  }
  else
  {
    // For non polarised image, we can determine size directly making it simler to read
    const size_t      dataSize = 4 * width * height * (n_channels + 1);
    std::vector<char> bufferedImage(dataSize);

    // Read pixel values
    is.read(bufferedImage.data(), dataSize);

    // Convert pixels bytes to float
    for (size_t y = 0; y < height; y++)
    {
      for (size_t x = 0; x < width; x++)
      {
        for (size_t lambda = 0; lambda < n_channels; lambda++)
        {
          // Notice file nChannels + 1: there is an alpha channel in ARTRAWs
          const size_t fileOffset = (y * width + x) * (n_channels + 1) + lambda;
          _data[lambda](y, x)     = getBuffer(&bufferedImage[4 * fileOffset]);
        }
        _alphaChannel(y, x) = getBuffer(&bufferedImage[4 * ((y * width + x) * (n_channels + 1) + n_channels)]);
      }
      progress(int(100.f * float(y) / float(height - 1)));
    }
  }

  return MRF_NO_ERROR;
}


//-------------------------------------------------------------------------
// Save routines
//-------------------------------------------------------------------------

IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::writeHeader(std::ostream &os) const
{
  time_t    now = time(nullptr);
  struct tm tstruct;
  char      buf[80];
#ifdef WIN32
  localtime_s(&tstruct, &now);
#else
  tstruct = *localtime(&now);
#endif

  strftime(buf, sizeof(buf), "%d.%m.%Y %X", &tstruct);

  os << "ART RAW image format " << std::fixed << std::setprecision(1) << _version << "\n\n";

  if (_version < 2.4f)
  {
    os << "Created by version: " << _creation_version << "\n";
    os << "Creation date:      " << buf << "\n";
  }
  else
  {
    os << "File created by:    " << _program << "\n";
    os << "Platform:           " << _platform << "\n";
    os << "Command line:       " << _command_line << "\n";
    os << "Creation date:      " << buf << "\n";
    os << "Render time:        " << _render_time << " seconds\n";
    os << "Samples per pixel:  " << _samples_per_pixel << "\n";
  }

  os << "Image size:         " << width() << " x " << height() << "\n";

  os << "DPI:                " << std::fixed << std::setprecision(1) << _dpiX << " x " << std::fixed
     << std::setprecision(1) << _dpiY << "\n";

  os << "Image type:         " << (polarised() ? "polarised" : "plain") << " " << (_spectral ? "spectrum" : "CIEXYZ")
     << " ";

  os << "with " << _wavelengths.size() << " samples\n\n";

  // Write sample bounds
  os << "Sample bounds in nanometers:";

  // Retrieve bounds from wavelengths
  uint lambdaStep = _wavelengths[1] - _wavelengths[0];

  // show lower bounds till size+1
  for (auto pLambda = _wavelengths.begin(); pLambda < _wavelengths.end(); ++pLambda)
  {
    os << " " << std::fixed << std::setprecision(2) << float(*pLambda - lambdaStep / 2);
  }
  os << " " << std::fixed << std::setprecision(2) << float(_wavelengths[_wavelengths.size() - 1] + lambdaStep / 2);

  os << "\n\n";

  os << "Big-endian binary coded IEEE float pixel values in scanline order follow:\nX";

  return MRF_NO_ERROR;
}


IMAGE_LOAD_SAVE_FLAGS ArtRawSpectralImage::writeImageData(std::ostream &os, std::function<void(int)> progress) const
{
  std::function<void(char *buff, const float &v)> setBuffer = [](char *buff, const float &v) {
    unsigned long l;
    union
    {
      float        f;
      unsigned int i;
    } value;
    value.f = float(v);
    l       = value.i;

    buff[0] = char(l & 0xff);
    l >>= 8;
    buff[1] = char(l & 0xff);
    l >>= 8;
    buff[2] = char(l & 0xff);
    l >>= 8;
    buff[3] = char(l & 0xff);
  };

  const size_t n_channels    = _wavelengths.size();
  const size_t dataSize      = 4 * width() * height() * (n_channels * nStokesComponents() + 1);
  char *       bufferedImage = new char[dataSize];

  if (polarised())
  {
    // Populate the buffer in the right order
    size_t fileOffset = 0;

    for (size_t y = 0; y < height(); y++)
    {
      for (size_t i = 0; i < width() / 8 + 1; i++)
      {
        // This flag byte determines if there is any polarisation data for the
        // next 8 pixels
        // TODO: Now, we just do not support this compression feature.
        bufferedImage[fileOffset] = char(0xFF);
        fileOffset++;

        for (size_t j = 0; j < 8; j++)
        {
          const size_t x = i * 8 + j;
          for (size_t lambda = 0; lambda < n_channels; lambda++)
          {
            for (size_t s = 0; s < nStokesComponents(); s++)
            {
              setBuffer(&bufferedImage[fileOffset], emissiveFramebuffer(s)[lambda](y, x));
              fileOffset += 4;
            }
          }

          setBuffer(&bufferedImage[fileOffset], _alphaChannel(y, x));
          fileOffset += 4;
        }
      }
      progress(int(100.f * float(y) / float(height() - 1)));
    }
  }
  else
  {
    // Populate the buffer in the right order
    for (size_t y = 0; y < height(); y++)
    {
      for (size_t x = 0; x < width(); x++)
      {
        for (size_t lambda = 0; lambda < n_channels; lambda++)
        {
          // Notice file nChannels + 1: there is an alpha channel in ARTRAWs
          const size_t fileOffset = (y * width() + x) * (n_channels + 1) + lambda;
          setBuffer(&bufferedImage[4 * fileOffset], _data[lambda](y, x));
        }
        setBuffer(&bufferedImage[4 * ((y * width() + x) * (n_channels + 1) + n_channels)], _alphaChannel(y, x));
      }
      progress(int(100.f * float(y) / float(height() - 1)));
    }
  }

  // Write the buffer to file
  os.write(bufferedImage, long(dataSize));

  delete[] bufferedImage;

  return MRF_NO_ERROR;
}
}   // namespace image
}   // namespace mrf
