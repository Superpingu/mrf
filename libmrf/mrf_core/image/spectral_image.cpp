/**
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *
 * Alban Fichet @ institutoptique.fr
 * April 2020
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/radiometry/illuminants.hpp>

#include <lodepng/lodepng.h>

#include <memory>

using mrf::color::SpectrumConverter;
using mrf::radiometry::Spectrum;
using std::string;

namespace mrf
{
namespace image
{
SpectralImage::SpectralImage() {}

IMAGE_LOAD_SAVE_FLAGS SpectralImage::addValuesFromGrayImage(std::string const &file_path, uint wavelength)
{
  std::unique_ptr<ColorImage> temp_image;

  try
  {
    temp_image = loadColorImage(file_path);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    return f;
  }

  if (_pixels.empty())
  {
    init(temp_image->width(), temp_image->height());
  }
  // Spectral image already allocated, check if gray image have the same size
  else if (uint(temp_image->width()) != _width || uint(temp_image->height()) != _height)
  {
    return MRF_ERROR_WRONG_IMAGE_SIZE;
  }

  for (uint i = 0; i < _width * _height; i++)
  {
    _pixels[i].add(wavelength, temp_image->getPixel(i).r());
  }

  return MRF_NO_ERROR;
}
}   // namespace image
}   // namespace mrf
