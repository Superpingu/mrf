/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * September 2020
 * Copyright: CNRS 2020
 *
 **/

#include "polarised_spectral_image.hpp"
#include <exception>

namespace mrf
{
namespace image
{
PolarisedSpectralImage::PolarisedSpectralImage(radiometry::SpectrumType spectrumType)
  : UniformSpectralImage()
  , _spectrumType(spectrumType)
{
  if (emissive() && polarised())
  {
    _emissivePolarisedData.resize(nStokesComponents() - 1);
  }
}


PolarisedSpectralImage::PolarisedSpectralImage(
    size_t                   width,
    size_t                   height,
    uint                     start_wavelength,
    uint                     step_wavelength,
    size_t                   nb_wavelength,
    radiometry::SpectrumType spectrumType)
  : UniformSpectralImage(width, height, start_wavelength, step_wavelength, nb_wavelength)
  , _spectrumType(spectrumType)
{
  allocateMemory(width, height, _wavelengths);
}


PolarisedSpectralImage::PolarisedSpectralImage(
    size_t                       width,
    size_t                       height,
    std::pair<uint, uint> const &start_end_wavelength,
    size_t                       nb_wavelength,
    radiometry::SpectrumType     spectrumType)
  : UniformSpectralImage(width, height, start_end_wavelength, nb_wavelength)
  , _spectrumType(spectrumType)
{
  allocateMemory(width, height, _wavelengths);
}


PolarisedSpectralImage::PolarisedSpectralImage(
    size_t                   width,
    size_t                   height,
    const std::vector<uint> &wavelengths,
    radiometry::SpectrumType spectrumType)
  : UniformSpectralImage(width, height, wavelengths)
  , _spectrumType(spectrumType)
{}


PolarisedSpectralImage::PolarisedSpectralImage(
    const UniformSpectralImage &uniform_spectral_image,
    radiometry::SpectrumType    spectrumType)
  : UniformSpectralImage(uniform_spectral_image)
  , _spectrumType(spectrumType)
{}


PolarisedSpectralImage::~PolarisedSpectralImage() {}


void PolarisedSpectralImage::clearConserveWavelengths()
{
  UniformSpectralImage::clearConserveWavelengths();

  for (auto &polaEmitlData : _emissivePolarisedData)
  {
    polaEmitlData.clear();
  }

  _extraUnpolarisedData.clear();
  _reradiation.clear();
}


void PolarisedSpectralImage::zeros()
{
  UniformSpectralImage::zeros();

  for (auto &polaData : _emissivePolarisedData)
  {
    for (auto &pxData : polaData)
    {
      pxData.setZero();
    }
  }

  for (auto &pxData : _extraUnpolarisedData)
  {
    pxData.setZero();
  }

  for (auto &pxData : _reradiation)
  {
    pxData.setZero();
  }
}


void PolarisedSpectralImage::allocateMemory(size_t width, size_t height, std::vector<uint> wavelengths)
{
  UniformSpectralImage::allocateMemory(width, height, wavelengths);

  if (emissive() && polarised())
  {
    _emissivePolarisedData.resize(nStokesComponents() - 1);

    for (auto &polaData : _emissivePolarisedData)
    {
      // S[1-3](\lambda) per wavelength
      polaData.resize(nSpectralBands());

      for (auto &pxData : polaData)
      {
        // S[1-3](\lambda) per pixel (2D array)
        pxData.resize(height, width);
      }
    }
  }

  // If both emissive and reflective, we need the extra buffer
  if (emissive() && reflective())
  {
    _extraUnpolarisedData.resize(nSpectralBands());

    for (auto &pxData : _extraUnpolarisedData)
    {
      pxData.resize(height, width);
    }
  }

  if (bispectral())
  {
    _reradiation.resize(reradiationSize());

    for (auto &pxData : _reradiation)
    {
      pxData.resize(height, width);
    }
  }
}


size_t PolarisedSpectralImage::nSpectralBands() const
{
  return _wavelengths.size();
}


size_t PolarisedSpectralImage::nStokesComponents() const
{
  if (emissive())
  {
    if (polarised())
    {
      return 4;
    }
    else
    {
      return 1;
    }
  }

  return 0;
}


std::vector<data_struct::Array2D> &PolarisedSpectralImage::emissiveFramebuffer(size_t stokeComponent)
{
  assert(emissive());
  assert(stokeComponent <= (polarised() ? 3 : 0));

  if (stokeComponent > 0)
  {
    return _emissivePolarisedData[stokeComponent - 1];
  }

  return data();
}


const std::vector<data_struct::Array2D> &PolarisedSpectralImage::emissiveFramebuffer(size_t stokeComponent) const
{
  assert(emissive());
  assert(stokeComponent <= (polarised() ? 3 : 0));

  if (stokeComponent > 0)
  {
    return _emissivePolarisedData[stokeComponent - 1];
  }

  return data();
}


std::vector<data_struct::Array2D> &PolarisedSpectralImage::reflectiveFramebuffer()
{
  assert(reflective());

  if (emissive())
  {
    return _extraUnpolarisedData;
  }

  return data();
}


const std::vector<data_struct::Array2D> &PolarisedSpectralImage::reflectiveFramebuffer() const
{
  assert(reflective());

  if (emissive())
  {
    return _extraUnpolarisedData;
  }

  return data();
}


const std::vector<data_struct::Array2D> &PolarisedSpectralImage::reradiation() const
{
  return _reradiation;
}


data_struct::Array2D &PolarisedSpectralImage::reradiationFrameBuffer(size_t wl_idx_from, size_t wl_idx_to)
{
  return _reradiation[idxFromWavelengthIdx(wl_idx_from, wl_idx_to)];
}


const data_struct::Array2D &PolarisedSpectralImage::reradiationFrameBuffer(size_t wl_idx_from, size_t wl_idx_to) const
{
  return _reradiation[idxFromWavelengthIdx(wl_idx_from, wl_idx_to)];
}


size_t PolarisedSpectralImage::reradiationSize() const
{
  return nSpectralBands() * (nSpectralBands() - 1) / 2;
}


size_t PolarisedSpectralImage::idxFromWavelengthIdx(size_t wlFrom_idx, size_t wlTo_idx)
{
  if (wlFrom_idx < wlTo_idx)
  {
    return wlTo_idx * (wlTo_idx - 1) / 2 + wlFrom_idx;
  }
  else
  {
    throw std::invalid_argument("from idx must be less than to idx");
  }
}


void PolarisedSpectralImage::wavelengthsIdxFromIdx(size_t rerad_idx, size_t &wlFrom_idx, size_t &wlTo_idx)
{
  const size_t k = static_cast<size_t>(std::floor((std::sqrt(1.F + 8.F * float(rerad_idx)) - 1.F) / 2.F));

  wlFrom_idx = static_cast<size_t>(rerad_idx - k * (k + 1) / 2.F);
  wlTo_idx   = k + 1;
}

}   // namespace image
}   // namespace mrf
