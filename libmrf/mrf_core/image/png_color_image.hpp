/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core/image/color_image.hpp>

namespace mrf
{
namespace image
{
class MRF_CORE_EXPORT PNGColorImage: public ColorImage
{
public:
  PNGColorImage(size_t width = 0, size_t height = 0);

  PNGColorImage(
      std::string const &      file_path,
      std::function<void(int)> progress =
          [](int) {
          },
      bool loadLinear = false);

  PNGColorImage(ColorImage const &img);

  virtual ~PNGColorImage();

  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const override;

  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      bool                     saveLinear,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const;
};
}   // namespace image
}   // namespace mrf
