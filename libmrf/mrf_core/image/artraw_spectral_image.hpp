/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/image/polarised_spectral_image.hpp>

#include <functional>

namespace mrf
{
namespace image
{
/**
 * @class ART Raw image format
 * ART is a rendering software (Advanced Rendering Toolkit) that generates
 * ART Raws (*.artraw) files among other specific file formats.
 * https://cgg.mff.cuni.cz/ART/
 * This class handles reading and writting spectral non polarised ART Raw
 * files.
 * Read the ART Raw documentation for more information.
 *
 * @todo This is a polarised capable format. The current class does not
 * support polarisation information. This shall be added and the class should
 * consequently inherit from PolarisedSpectralImage.
 */
class MRF_CORE_EXPORT ArtRawSpectralImage: public PolarisedSpectralImage
{
public:
  /**
   * @brief Creates a new empty ART Raw image
   *
   * @param width            Image width.
   * @param height           Image height.
   * @param start_wavelength Start wavelength sampled.
   * @param step_wavelength  Step between wavelengths samples.
   * @param nb_wavelength    Number of wavelength samples.
   */
  ArtRawSpectralImage(
      size_t width            = 0,
      size_t height           = 0,
      uint   start_wavelength = 360,
      uint   step_wavelength  = 1,
      uint   nb_wavelength    = 471);


  ArtRawSpectralImage(size_t width, size_t height, const std::vector<uint> &wavelengths);

  /**
   * @brief Creates a new ART Raw image from a stored file
   *
   * @param file_path Path the the ART Raw file.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   * @throws IMAGE_LOAD_SAVE_FLAGS if unsucessfull
   */
  ArtRawSpectralImage(
      const std::string &      filename,
      std::function<void(int)> progress = [](int) {
      });

  /**
   * @brief Creates a new ART Raw image from an input stream
   *
   * @param file_path Path the the ART Raw file.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   * @throws IMAGE_LOAD_SAVE_FLAGS if unsucessfull
   */
  ArtRawSpectralImage(
      std::istream &           is,
      std::function<void(int)> progress = [](int) {
      });


  /**
   * @brief Creates a new ART Raw image from another Uniform Spectral
   * Image
   *
   * @param cpy Other UniformSpectralImage image object.
   */
  ArtRawSpectralImage(UniformSpectralImage const &other);

  virtual ~ArtRawSpectralImage();

  //---------------------------------------------------------------------
  // Save routines
  //---------------------------------------------------------------------

  /**
   * @brief Saves an ART raw file
   *
   * @param file_path Path to an .artraw file.
   * @param a progress function
   * @param additional information to store the header
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const override;

  /**
   * @brief Saves an ART raw to stream
   *
   * @param os Output stream to an ART Raw.
   * @param progress  A callback to notify the saving progress in
   *                  percents.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  virtual IMAGE_LOAD_SAVE_FLAGS save(
      std::ostream &           os,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const;

  //---------------------------------------------------------------------
  // Getter / setters metadata
  //---------------------------------------------------------------------

  /**
   * @brief Sets the platform name
   */
  void setPlatformName(const std::string &platform_desc);

  /**
   * @brief Gets the platform name
   */
  std::string platformName() const { return _platform; }

  /**
   * @brief Sets the creation program
   */
  void setCreationProgram(const std::string &creation_program);

  /**
   * @brief Gets the creation program
   */
  std::string creationProgramm() const { return _program; }

  /**
   * @brief Sets the command line used
   */
  void setCreationCommandLine(int argc, char *argv[]);

  /**
   * @brief Gets the command line used
   */
  std::string creationCommandLine() const { return _command_line; }

private:
  //---------------------------------------------------------------------
  // Read routines
  //---------------------------------------------------------------------

  /**
   * @brief Loads an ART raw file
   *
   * @param file_path Path to an .artraw file.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   *
   * @return The load state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS load(
      const std::string &      file_path,
      std::function<void(int)> progress = [](int) {
      });


  /**
   * @brief Loads an ART raw from stream
   *
   * @param is        Input stream to an ART Raw.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   *
   * @return The load state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS load(
      std::istream &           is,
      std::function<void(int)> progress = [](int) {
      });

  /**
   * @brief Reads the header from an input stream.
   *
   * @param is         Input stream to the ART Raw.
   * @param width      Width of the image (out).
   * @param height     Heigth of the image (out).
   * @param n_channels Wavelength samples in the image (out).
   * @param bounds     Wavelength sampled bounds (out).
   *
   * @returns The load state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS
  readHeader(std::istream &is, size_t &width, size_t &height, size_t &n_channels, std::vector<double> &bounds);

  /**
   * @brief Reads data from an output stream.
   *
   * @param is Input stream to the ART Raw.
   * @param is         Input stream to the ART Raw.
   * @param width      Width of the image.
   * @param height     Heigth of the image.
   * @param n_channels Wavelength samples in the image.
   * @param bounds     Wavelength sampled bounds.
   * @param progress   A callback to notify the loading progress in
   *                   percents.
   *
   * @returns The load state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS readImageData(
      std::istream &             is,
      size_t                     width,
      size_t                     height,
      size_t                     n_channels,
      const std::vector<double> &bounds,
      std::function<void(int)>   progress = [](int) {
      });

  //---------------------------------------------------------------------
  // Save routines
  //---------------------------------------------------------------------

  /**
   * @brief Write header data from an output stream.
   *
   * @param os Output stream to the ART Raw.
   * @returns The save state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS writeHeader(std::ostream &os) const;

  /**
   * @brief Write image data from an output stream.
   *
   * @param os        Output stream to the ART Raw.
   * @param progress  A callback to notify the saving progress in
   *                  percents.
   *
   * @returns The save state. MRF_NO_ERROR if successfull.
   */
  IMAGE_LOAD_SAVE_FLAGS writeImageData(
      std::ostream &           os,
      std::function<void(int)> progress = [](int) {
      }) const;


  /**
   * @brief Updates internally the metdata associated with this image
   */
  void updateMetadata();

  //---------------------------------------------------------------------
  // Fields
  //---------------------------------------------------------------------

  float _version;

  // Metadata
  std::string _creation_date;
  const char *TOKEN_CREATION_DATE = "Creation date:";

  // v2.4
  std::string _program;
  std::string _platform;
  std::string _command_line;
  std::string _render_time;
  std::string _samples_per_pixel;

  const char *TOKEN_PROGRAM           = "File created by:";
  const char *TOKEN_PLATFORM          = "Platform:";
  const char *TOKEN_COMMAND_LINE      = "Command line:";
  const char *TOKEN_RENDER_TIME       = "Render time:";
  const char *TOKEN_SAMPLES_PER_PIXEL = "Samples per pixel:";

  // < v2.4
  std::string _creation_version;

  const char *TOKEN_CREATION_VERSION = "Created by version:";

  float _dpiX, _dpiY;
  bool  _spectral;

  mrf::data_struct::Array2D _alphaChannel;
};
}   // namespace image
}   // namespace mrf
