/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core/image/polarised_spectral_image.hpp>
#include <tinyexr/tinyexr.h>

namespace mrf
{
namespace image
{
class MRF_CORE_EXPORT EXRSpectralImage: public PolarisedSpectralImage
{
public:
  /**
   * @brief Creates a new empty EXR spectral image
   *
   * @param width            Image width.
   * @param height           Image height.
   * @param start_wavelength Start wavelength sampled.
   * @param step_wavelength  Step between wavelengths samples.
   * @param nb_wavelength    Number of wavelength samples.
   * @param spectrumType     Sets the type of spectrum stored in image pixels.
   */
  EXRSpectralImage(
      size_t width            = 0,
      size_t height           = 0,
      uint   start_wavelength = 360,
      uint   step_wavelength  = 1,
      uint nb_wavelength = 471,
      radiometry::SpectrumType spectrumType     = radiometry::SPECTRUM_EMISSIVE);

  EXRSpectralImage(
      size_t                       width,
      size_t                       height,
      std::pair<uint, uint> const &start_end_wavelength,
      size_t                       nb_wavelength,
      radiometry::SpectrumType     spectrumType = radiometry::SPECTRUM_EMISSIVE);

  /**
   * @brief Creates a new EXR spectral image from a stored file
   *
   * @param file_path Path the the EXR spectral file.
   * @param progress  A callback to notify the loading progress in
   *                  percents.
   * @throws IMAGE_LOAD_SAVE_FLAGS if unsucessfull
   */
  EXRSpectralImage(
      const std::string &      filename,
      std::function<void(int)> progress = [](int) {
      });

  // /**
  //  * @brief Creates a new EXR spectral image from an input stream
  //  *
  //  * @param file_path Path the the EXR spectral file.
  //  * @param progress  A callback to notify the loading progress in
  //  *                  percents.
  //  * @throws IMAGE_LOAD_SAVE_FLAGS if unsucessfull
  //  */
  // EXRSpectralImage(
  //     std::istream &           is,
  //     std::function<void(int)> progress = [](int) {
  //     });


  /**
   * @brief Creates a new EXR spectral image from another Uniform Spectral
   * Image
   *
   * @param other Other UniformSpectralImage image object.
   */
  EXRSpectralImage(const UniformSpectralImage &other);

  virtual ~EXRSpectralImage();

  /**
   * @brief Saves a file
   *
   * @param file_path The path to the file to save.
   * @param progress  A callback to notify the saving progress in
   *                  percents.
   *
   * @return The save state. MRF_NO_ERROR if successfull.
   */
  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const;

  static bool isSpectral(std::string const &filepath);

  static bool isSpectralChannel(
      const std::string &       s,
      int &                     polarisation_component,
      double &                  wavelength_nm,
      double &                  wavelength_to_nm,
      radiometry::SpectrumType &type);


  static double strToNanometers(const double &value, const std::string &prefix, const std::string &units);

protected:
  void updateMetadata(EXRHeader const &exr_header);
};
}   // namespace image
}   // namespace mrf
