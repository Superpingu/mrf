/**
 *
 * author : Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/envi_spectral_image.hpp>
#include <mrf_core/util/string_parsing.hpp>

#include <iomanip>
#include <regex>
#include <cstdint>

namespace mrf
{
namespace image
{
std::ostream &operator<<(std::ostream &os, EnviSpectralImage::InterleavedMode m)
{
  switch (m)
  {
    case EnviSpectralImage::IM_BIL:
      os << "bil";
      break;

    case EnviSpectralImage::IM_BIP:
      os << "bip";
      break;

    case EnviSpectralImage::IM_BSQ:
      os << "bsq";
      break;

    default:
      os.setstate(std::ios_base::failbit);
  }

  return os;
}

std::ostream &operator<<(std::ostream &os, EnviSpectralImage::DataType t)
{
  if (t == EnviSpectralImage::DT_UNDEF)
  {
    os.setstate(std::ios_base::failbit);
  }
  else
  {
    os << static_cast<int>(t);
  }

  return os;
}

EnviSpectralImage::EnviSpectralImage(
    size_t width,
    size_t height,
    uint   start_wavelength,
    uint   step_wavelength,
    size_t nb_wavelength)
  : UniformSpectralImage(width, height, start_wavelength, step_wavelength, nb_wavelength)
  , _header(width, height, nb_wavelength)
{}

EnviSpectralImage::EnviSpectralImage(size_t width, size_t height, const std::vector<uint> &wavelengths)
  : UniformSpectralImage(width, height, wavelengths)
  , _header(width, height, wavelengths.size())
{}

EnviSpectralImage::EnviSpectralImage(
    size_t                       width,
    size_t                       height,
    std::pair<uint, uint> const& start_end_wavelength,
    size_t                       nb_wavelength)
  : UniformSpectralImage(width, height, start_end_wavelength, nb_wavelength)
{}

EnviSpectralImage::EnviSpectralImage(const std::string &file_path, std::function<void(int)> progress)
  : UniformSpectralImage()
  , _header()
{
  IMAGE_LOAD_SAVE_FLAGS status = load(file_path, progress);

  if (status != MRF_NO_ERROR)
  {
    throw status;
  }
}

EnviSpectralImage::EnviSpectralImage(
    const std::string &      file_path_header,
    const std::string &      file_path_data,
    std::function<void(int)> progress)
  : UniformSpectralImage()
  , _header()
{
  IMAGE_LOAD_SAVE_FLAGS status = load(file_path_header, file_path_data, progress);

  if (status != MRF_NO_ERROR)
  {
    throw status;
  }
}

EnviSpectralImage::EnviSpectralImage(const UniformSpectralImage &other)
  : UniformSpectralImage(other)
  , _header(other.width(), other.height(), other.wavelengths().size())
{
  // Populate comments with metadata
  std::stringstream ss;

  for (auto const &el : other.metadata())
  {
    ss << el.first << " " << el.second << "\n";
  }

  _header.comments = ss.str();
}

EnviSpectralImage::~EnviSpectralImage() {}

bool EnviSpectralImage::getFilenames(const std::string &file_path, std::pair<std::string, std::string> &names)
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(file_path, extension);

  std::string base_name;
  mrf::util::StringParsing::getFileNameWithoutExtension(file_path, base_name);

  if (extension == "hdr" || extension == "HDR")
  {
    // User specified the header, we guess the data
    names.first  = file_path;
    names.second = base_name + ".raw";

    return true;
  }
  else if (extension == "raw" || extension == "RAW")
  {
    // User specified the data, we guess the header
    names.first  = base_name + ".hdr";
    names.second = file_path;

    return true;
  }

  return false;
}

//---------------------------------------------------------------------
// Save routines
//---------------------------------------------------------------------

IMAGE_LOAD_SAVE_FLAGS mrf::image::EnviSpectralImage::save(
    const std::string &      file_path,
    const std::string &      additional_comments,
    std::function<void(int)> progress) const
{
  std::pair<std::string, std::string> filenames;

  if (getFilenames(file_path, filenames))
  {
    return save(filenames.first, filenames.second, additional_comments, progress);
  }
  else
  {
    return MRF_ERROR_WRONG_EXTENSION;
  }
}

IMAGE_LOAD_SAVE_FLAGS EnviSpectralImage::save(
    const std::string &      file_path_header,
    const std::string &      file_path_data,
    const std::string &      additional_comments,
    std::function<void(int)> progress) const
{
  IMAGE_LOAD_SAVE_FLAGS status;

  std::ofstream header_ofs(file_path_header, std::ofstream::out);

  if (!header_ofs.is_open())
  {
    return MRF_ERROR_SAVING_FILE;
  }

  status = writeHeader(header_ofs, additional_comments);
  header_ofs.close();

  // If an error occured while writing the header, we stop there
  if (status != MRF_NO_ERROR)
  {
    return status;
  }

  std::ofstream data_ofs(file_path_data, std::ios::binary | std::ios::out);

  if (!data_ofs.is_open())
  {
    return MRF_ERROR_SAVING_FILE;
  }

  switch (_header.data_type)
  {
    case DT_UINT_8:
      status = writeImageData<uint8_t>(data_ofs, progress);
      break;
    case DT_UINT_16:
      status = writeImageData<uint16_t>(data_ofs, progress);
      break;
    case DT_UINT_32:
      status = writeImageData<uint32_t>(data_ofs, progress);
      break;
    case DT_UINT_64:
      status = writeImageData<uint64_t>(data_ofs, progress);
      break;
    case DT_INT_16:
      status = writeImageData<int16_t>(data_ofs, progress);
      break;
    case DT_INT_32:
      status = writeImageData<int32_t>(data_ofs, progress);
      break;
    case DT_INT_64:
      status = writeImageData<int64_t>(data_ofs, progress);
      break;
    case DT_FLOAT:
      status = writeImageData<float>(data_ofs, progress);
      break;
    case DT_DOUBLE:
      status = writeImageData<double>(data_ofs, progress);
      break;
    case DT_CPLX_FLOAT:
    case DT_CPLX_DOUBLE:
    case DT_UNDEF:
      status = MRF_ERROR_WRONG_PIXEL_FORMAT;
      break;
  }

  data_ofs.close();

  return status;
}

//---------------------------------------------------------------------
// Getter / setters metadata
//---------------------------------------------------------------------

void EnviSpectralImage::setInterleavedMode(InterleavedMode mode)
{
  _header.interleave_mode = mode;
}

void EnviSpectralImage::setComments(std::string const &comments)
{
  _header.comments = comments;
}

void EnviSpectralImage::setDataType(DataType dt)
{
  _header.data_type = dt;
}

void EnviSpectralImage::updateMetadata()
{
  std::stringstream ss;
  ss << _header.interleave_mode;

  _metadata["Interleaved mode:"] = ss.str();

  ss.clear();

  std::function<std::string(DataType)> getDataTypeStr = [](DataType t) {
    switch (t)
    {
      case DT_UINT_8:
        return "8-bit unsigned integer";
      case DT_UINT_16:
        return "16-bit unsigned integer";
      case DT_UINT_32:
        return "32-bit unsigned integer";
      case DT_UINT_64:
        return "64-bit unsigned integer";
      case DT_INT_16:
        return "16-bit signed integer";
      case DT_INT_32:
        return "32-bit signed integer";
      case DT_INT_64:
        return "64-bit signed integer";
      case DT_FLOAT:
        return "32-bit floating point single precision";
      case DT_DOUBLE:
        return "64-bit floating point double precision";
      case DT_CPLX_FLOAT:
        return "Complex real-imaginary pair single precision";
      case DT_CPLX_DOUBLE:
        return "Complex real-imaginary pair double precision";
      case DT_UNDEF:
        return "Unknown";
    }
    return "Unknown";
  };

  _metadata["Data type:"] = getDataTypeStr(_header.data_type);
  _metadata["Comments:"]  = _header.comments;
}

// std::map<std::string, std::string> EnviSpectralImage::metadata() const
// {
// }

template<typename T>
void EnviSpectralImage::setDataType()
{
  if (std::is_same<T, uint8_t>::value)
  {
    _header.data_type = DT_UINT_8;
  }
  else if (std::is_same<T, uint16_t>::value)
  {
    _header.data_type = DT_UINT_16;
  }
  else if (std::is_same<T, uint32_t>::value)
  {
    _header.data_type = DT_UINT_32;
  }
  else if (std::is_same<T, uint64_t>::value)
  {
    _header.data_type = DT_UINT_64;
  }
  else if (std::is_same<T, int16_t>::value)
  {
    _header.data_type = DT_INT_16;
  }
  else if (std::is_same<T, int32_t>::value)
  {
    _header.data_type = DT_INT_32;
  }
  else if (std::is_same<T, float>::value)
  {
    _header.data_type = DT_FLOAT;
  }
  else if (std::is_same<T, double>::value)
  {
    _header.data_type = DT_DOUBLE;
  }
  else
  {
    // We do not support DT_CPLX_FLOAT nor DT_CPLX_DOUBLE yet
    _header.data_type = DT_UNDEF;
  }
}

//---------------------------------------------------------------------
// Read routines
//---------------------------------------------------------------------

IMAGE_LOAD_SAVE_FLAGS EnviSpectralImage::load(const std::string &file_path, std::function<void(int)> progress)
{
  std::pair<std::string, std::string> filenames;

  if (getFilenames(file_path, filenames))
  {
    return load(filenames.first, filenames.second, progress);
  }
  else
  {
    return MRF_ERROR_WRONG_EXTENSION;
  }
}

IMAGE_LOAD_SAVE_FLAGS EnviSpectralImage::load(
    std::string const &      file_path_header,
    std::string const &      file_path_image_data,
    std::function<void(int)> progress)
{
  IMAGE_LOAD_SAVE_FLAGS status;

  std::ifstream header_ifs(file_path_header, std::ifstream::in);
  if (!header_ifs.is_open())
  {
    return MRF_ERROR_LOADING_FILE;
  }

  status = readHeader(header_ifs);
  header_ifs.close();
  // Now that header is loaded update the metadata map
  updateMetadata();

  // If an error occured while writing the header, we stop there
  if (status != MRF_NO_ERROR)
  {
    return status;
  }

  std::ifstream data_ifs(file_path_image_data, std::ios::in | std::ios::binary | std::ios::ate);
  if (!data_ifs.is_open())
  {
    return MRF_ERROR_LOADING_FILE;
  }

  switch (_header.data_type)
  {
    case DT_UINT_8:
      status = readImageData<uint8_t>(data_ifs, progress);
      break;
    case DT_UINT_16:
      status = readImageData<uint16_t>(data_ifs, progress);
      break;
    case DT_UINT_32:
      status = readImageData<uint32_t>(data_ifs, progress);
      break;
    case DT_UINT_64:
      status = readImageData<uint64_t>(data_ifs, progress);
      break;
    case DT_INT_16:
      status = readImageData<int16_t>(data_ifs, progress);
      break;
    case DT_INT_32:
      status = readImageData<int32_t>(data_ifs, progress);
      break;
    case DT_INT_64:
      status = readImageData<int64_t>(data_ifs, progress);
      break;
    case DT_FLOAT:
      status = readImageData<float>(data_ifs, progress);
      break;
    case DT_DOUBLE:
      status = readImageData<double>(data_ifs, progress);
      break;
    case DT_CPLX_FLOAT:
    case DT_CPLX_DOUBLE:
    case DT_UNDEF:
      data_ifs.close();
      return MRF_ERROR_WRONG_PIXEL_FORMAT;
  }

  data_ifs.close();

  // Try remove duplicate wavelengths
  std::vector<uint> wave_unique;
  std::unique_copy(_wavelengths.begin(), _wavelengths.end(), std::back_inserter(wave_unique));

  if (wave_unique.size() < _wavelengths.size())
  {
    std::vector<mrf::data_struct::Array2D> temp_data;

    //average duplicate wavelengths
    int wave_index = int(_wavelengths.size()) - 1;
    for (; wave_index >= 0; wave_index--)
    {
      mrf::data_struct::Array2D temp_array = std::move(_data[wave_index]);
      _data.pop_back();

      int norm = 1;

      if (wave_index > 0)
      {
        uint w1 = _wavelengths[wave_index];
        uint w2 = _wavelengths[wave_index - 1];

        while (wave_index > 0 && w2 == w1)
        {
          temp_array += _data[wave_index - 1];
          _data.pop_back();

          norm++;
          wave_index--;
          if (wave_index > 0)
          {
            w1 = _wavelengths[wave_index];
            w2 = _wavelengths[wave_index - 1];
          }
        }
      }

      if (norm > 1) temp_array /= float(norm);

      temp_data.push_back(std::move(temp_array));
    }

    //remove duplicate wavelengths
    _wavelengths = wave_unique;

    //reverse temp array of data
    for (wave_index = int(_wavelengths.size()) - 1; wave_index >= 0; wave_index--)
    {
      _data.push_back(std::move(temp_data.back()));
      temp_data.pop_back();
    }
  }

  return status;
}

IMAGE_LOAD_SAVE_FLAGS EnviSpectralImage::readHeader(std::istream &is)
{
  std::string line;

  std::function<bool(std::string const &, std::string const &)> check_param
      = [](std::string const &line, std::string const &param_name) {
          return line.substr(0, param_name.size()) == param_name;
        };

  std::function<std::string(std::string const &)> get_param_value = [](std::string const &line) {
    std::size_t param_position = line.find_first_of('=');
    std::string param_value;
    if (param_position != std::string::npos)
    {
      param_value = line.substr(param_position + 1);
      param_value = std::regex_replace(param_value, std::regex("^ +"), "$1");
    }
    return param_value;
  };

  while (std::getline(is, line))
  {
    //remove leading spaces
    line                    = std::regex_replace(line, std::regex("^ +"), "$1");
    std::string param_value = "";

    //read description
    if (check_param(line, "description"))
    {
      //std::cout << "DESC\n";
      if (line.find_first_of('{') != std::string::npos)
      {
        line = line.substr(line.find_first_of('{') + 1);
        while (line.find_first_of('}') == std::string::npos && !is.eof())
        {
          //std::cout << line << "\n";
          _header.comments += line + "\n";
          std::getline(is, line);
        }
        std::replace(line.begin(), line.end(), '}', ' ');
        _header.comments += line;
        //std::cout << line << "\n";
      }
    }
    //file type
    else if (check_param(line, "file type"))
    {
      param_value = get_param_value(line);
      //std::cout << "file type: " << param_value << "\n";
      if (!check_param(param_value, "ENVI"))
      {
        //std::cout<<"file type!=ENVI "<<std::endl;
        return MRF_ERROR_WRONG_EXTENSION;
      }
    }
    //interleave method
    else if (check_param(line, "interleave"))
    {
      param_value = get_param_value(line);

      if (check_param(param_value, "bil"))
      {
        _header.interleave_mode = IM_BIL;
      }
      else if (check_param(param_value, "bsq"))
      {
        _header.interleave_mode = IM_BSQ;
      }
      else if (check_param(param_value, "bip"))
      {
        _header.interleave_mode = IM_BIP;
      }
      else
      {
        _header.interleave_mode = IM_UNDEF;
        return MRF_ERROR_WRONG_EXTENSION;
      }
    }
    //
    else if (check_param(line, "samples"))
    {
      param_value   = get_param_value(line);
      _header.width = stoi(param_value);
    }
    //
    else if (check_param(line, "bands"))
    {
      param_value   = get_param_value(line);
      _header.bands = stoi(param_value);
    }
    //
    else if (check_param(line, "lines"))
    {
      param_value    = get_param_value(line);
      _header.height = stoi(param_value);
    }
    else if (check_param(line, "data type"))
    {
      param_value   = get_param_value(line);
      int data_type = stoi(param_value);

      switch (data_type)
      {
        case DT_UINT_8:
        case DT_UINT_16:
        case DT_UINT_32:
        case DT_UINT_64:
        case DT_INT_16:
        case DT_INT_32:
        case DT_INT_64:
        case DT_FLOAT:
        case DT_DOUBLE:
        case DT_CPLX_FLOAT:
        case DT_CPLX_DOUBLE:
          _header.data_type = static_cast<DataType>(data_type);
          break;
        default:
          _header.data_type = DT_UNDEF;
          return MRF_ERROR_WRONG_PIXEL_FORMAT;
      }
    }
    else if (check_param(line, "byte order"))
    {
      param_value = get_param_value(line);
      if (stoi(param_value) != 0)
      {
        return MRF_ERROR_WRONG_PIXEL_FORMAT;
      }
    }
    //read wavelength
    else if (check_param(line, "Wavelength") || check_param(line, "wavelength"))
    {
      if (line.find_first_of('{') != std::string::npos)
      {
        bool end_wave = false;

        while (!end_wave && !is.eof())
        {
          std::getline(is, line);

          if (line.find_first_of('}') != std::string::npos)
          {
            end_wave = true;
            line.erase(std::remove(line.begin(), line.end(), '}'), line.end());
          }

          std::vector<std::string> tokens;
          mrf::util::StringParsing::tokenize(line, tokens, ",");
          auto lambda = [](unsigned char const c) {
            return std::isspace(c);
          };

          for (auto str : tokens)
          {
            str.erase(std::remove_if(str.begin(), str.end(), lambda), str.end());
            if (str.size() > 0)
            {
              float wavelength_f = std::round(stof(str));
              _wavelengths.push_back(static_cast<int>(wavelength_f));
            }
          }
        }
      }
    }
  }

  return MRF_NO_ERROR;
}

template<typename T>
IMAGE_LOAD_SAVE_FLAGS EnviSpectralImage::readImageData(std::istream &is, std::function<void(int)> progress)
{
  _data.reserve(_wavelengths.size());
  for (uint i = 0; i < _wavelengths.size(); i++)
  {
    _data.push_back(mrf::data_struct::Array2D(_header.width, _header.height));
  }

  const size_t buffer_size = _data.size() * _header.width * _header.height;
  auto         buffer      = std::unique_ptr<T>(new T[buffer_size]);
  size_t       read_offset = 0;

  is.seekg(0, is.end);
  size_t stream_size = is.tellg();
  is.seekg(0, is.beg);

  if (stream_size < buffer_size * sizeof(T))
  {
    return MRF_ERROR_LOADING_FILE;
  }

  is.seekg(0, std::ios::beg);
  is.read((char *)(buffer.get()), buffer_size * sizeof(T));

  if (!is)
  {
    return MRF_ERROR_LOADING_FILE;
  }

  switch (_header.interleave_mode)
  {
    case IM_BIL:
      for (uint i = 0; i < height(); i++)
      {
        for (uint slice = 0; slice < _data.size(); slice++)
        {
          for (uint j = 0; j < width(); j++)
          {
            assert(read_offset < buffer_size);

            if (std::is_integral<T>::value)
            {
              _data[slice](i, j)
                  = static_cast<float>(buffer.get()[read_offset++]) / static_cast<float>(std::numeric_limits<T>::max());
            }
            else
            {
              _data[slice](i, j) = static_cast<float>(buffer.get()[read_offset++]);
            }
          }
        }

        progress(int(100.f * float(i) / float(height() - 1)));
      }
      break;

    case IM_BIP:
      for (uint i = 0; i < height(); i++)
      {
        for (uint j = 0; j < width(); j++)
        {
          for (uint slice = 0; slice < _data.size(); slice++)
          {
            assert(read_offset < buffer_size);

            if (std::is_integral<T>::value)
            {
              _data[slice](i, j)
                  = static_cast<float>(buffer.get()[read_offset++]) / static_cast<float>(std::numeric_limits<T>::max());
            }
            else
            {
              _data[slice](i, j) = static_cast<float>(buffer.get()[read_offset++]);
            }
          }
        }

        progress(int(100.f * float(i) / float(height() - 1)));
      }
      break;

    case IM_BSQ:
      for (uint slice = 0; slice < _data.size(); slice++)
      {
        for (uint i = 0; i < height(); i++)
        {
          for (uint j = 0; j < width(); j++)
          {
            assert(read_offset < buffer_size);

            if (std::is_integral<T>::value)
            {
              _data[slice](i, j)
                  = static_cast<float>(buffer.get()[read_offset++]) / static_cast<float>(std::numeric_limits<T>::max());
            }
            else
            {
              _data[slice](i, j) = static_cast<float>(buffer.get()[read_offset++]);
            }
          }
        }

        progress(int(100.f * float(slice) / float(_data.size() - 1)));
      }
      break;

    default:
      return MRF_ERROR_DECODING_FILE;
  }

  return MRF_NO_ERROR;
}

//---------------------------------------------------------------------
// Save routines
//---------------------------------------------------------------------

IMAGE_LOAD_SAVE_FLAGS EnviSpectralImage::writeHeader(std::ostream &os, std::string additional_comments) const
{
  if (_header.data_type == DT_UNDEF)
  {
    return MRF_ERROR_WRONG_PIXEL_FORMAT;
  }

  os << "ENVI" << std::endl
     << "description = {" << _header.comments << " " << additional_comments << " }" << std::endl
     << "file type = ENVI" << std::endl
     << "sensor type = " << std::endl
     << "interleave = " << _header.interleave_mode << std::endl
     << "samples = " << width() << std::endl
     << "lines = " << height() << std::endl
     << "bands = " << _data.size() << std::endl
     << "header offset = 0" << std::endl
     << "data type = " << _header.data_type << std::endl
     << "byte order = 0" << std::endl
     << "x start = 0" << std::endl
     << "y start = 0" << std::endl
     << "errors = {none}" << std::endl;

  os << "Wavelength = {" << std::endl;

  for (size_t i = 0; i < _wavelengths.size(); i++)
  {
    os << _wavelengths[i] << "," << std::endl;
  }

  os << "}" << std::endl;

  return MRF_NO_ERROR;
}

template<typename T>
IMAGE_LOAD_SAVE_FLAGS EnviSpectralImage::writeImageData(std::ostream &os, std::function<void(int)> progress) const
{
  const size_t buffer_size  = _data.size() * width() * height();
  auto         buffer       = std::unique_ptr<T>(new T[buffer_size]);
  size_t       write_offset = 0;

  switch (_header.interleave_mode)
  {
    case IM_BIL:
      for (uint i = 0; i < height(); i++)
      {
        for (uint slice = 0; slice < _data.size(); slice++)
        {
          for (uint j = 0; j < width(); j++)
          {
            assert(write_offset < buffer_size);

            if (std::is_integral<T>::value)
            {
              buffer.get()[write_offset++] = static_cast<T>(_data[slice](i, j)) * std::numeric_limits<T>::max();
            }
            else
            {
              buffer.get()[write_offset++] = static_cast<T>(_data[slice](i, j));
            }
          }
        }

        progress(int(100.f * float(i) / float(height() - 1)));
      }
      break;

    case IM_BIP:
      for (uint i = 0; i < height(); i++)
      {
        for (uint j = 0; j < width(); j++)
        {
          for (uint slice = 0; slice < _data.size(); slice++)
          {
            assert(write_offset < buffer_size);

            if (std::is_integral<T>::value)
            {
              buffer.get()[write_offset++] = static_cast<T>(_data[slice](i, j)) * std::numeric_limits<T>::max();
            }
            else
            {
              buffer.get()[write_offset++] = static_cast<T>(_data[slice](i, j));
            }
          }
        }

        progress(int(100.f * float(i) / float(height() - 1)));
      }
      break;

    case IM_BSQ:
      for (uint slice = 0; slice < _data.size(); slice++)
      {
        for (uint i = 0; i < height(); i++)
        {
          for (uint j = 0; j < width(); j++)
          {
            assert(write_offset < buffer_size);

            if (std::is_integral<T>::value)
            {
              buffer.get()[write_offset++] = static_cast<T>(_data[slice](i, j)) * std::numeric_limits<T>::max();
            }
            else
            {
              buffer.get()[write_offset++] = static_cast<T>(_data[slice](i, j));
            }
          }
        }

        progress(int(100.f * float(slice) / float(_data.size() - 1)));
      }
      break;

    default:
      return MRF_ERROR_DECODING_FILE;
  }

  os.seekp(0, std::ios::beg);
  os.write((char *)(buffer.get()), buffer_size * sizeof(T));

  return MRF_NO_ERROR;
}
}   // namespace image
}   // namespace mrf
