/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/exr_spectral_image.hpp>
#include <mrf_core/image/exr_color_image.hpp>

#include <mrf_core/color/spectrum_converter.hpp>

#include <utility>
#include <sstream>
#include <memory>
#include <regex>
#include <algorithm>

namespace mrf
{
namespace image
{
EXRSpectralImage::EXRSpectralImage(
    size_t                   width,
    size_t                   height,
    uint                     start_wavelength,
    uint                     step_wavelength,
    uint                     nb_wavelength,
    radiometry::SpectrumType spectrumType)
  : PolarisedSpectralImage(
      width,
      height,
      start_wavelength,
      step_wavelength,
      nb_wavelength,
      spectrumType)
{}

EXRSpectralImage::EXRSpectralImage(
    size_t                       width,
    size_t                       height,
    std::pair<uint, uint> const &start_end_wavelength,
    size_t                       nb_wavelength,
    radiometry::SpectrumType     spectrumType)
  : PolarisedSpectralImage(
      width,
      height,
      start_end_wavelength,
      nb_wavelength,
      spectrumType)
{}

EXRSpectralImage::EXRSpectralImage(const std::string &filename, std::function<void(int)> progress)
  : PolarisedSpectralImage(radiometry::SPECTRUM_NONE)
{
  progress(0);

  // -------------------------------------------------------------------------
  // Read EXR version.
  // -------------------------------------------------------------------------

  EXRVersion exr_version;

  int ret = ParseEXRVersionFromFile(&exr_version, filename.c_str());
  if (ret != 0)
  {
    std::cerr << "Invalid EXR file: " << filename << std::endl;
    throw MRF_ERROR_LOADING_FILE;
  }

  if (exr_version.multipart)
  {
    std::cerr << "Multipart EXR not supported: " << filename << std::endl;
    throw MRF_ERROR_DECODING_FILE;
  }

  // -------------------------------------------------------------------------
  // Read EXR header
  // -------------------------------------------------------------------------

  EXRHeader exr_header;
  InitEXRHeader(&exr_header);

  const char *err;
  ret = ParseEXRHeaderFromFile(&exr_header, &exr_version, filename.c_str(), &err);
  if (ret != 0)
  {
    std::cerr << "Parse EXR error: " << err << std::endl;
    throw MRF_ERROR_DECODING_FILE;
  }

  updateMetadata(exr_header);

  std::array<std::vector<std::pair<double, size_t>>, 4>     wavelengths_index_emissive;
  std::vector<std::pair<double, size_t>>                    wavelengths_index_reflective;
  std::vector<std::pair<std::pair<double, double>, size_t>> wavelengths_index_reradiation;

  // -------------------------------------------------------------------------
  // Determine channels position
  // -------------------------------------------------------------------------

  for (int c = 0; c < exr_header.num_channels; c++)
  {
    int                      polarisation_component;
    double                   wl, wl_rerad;
    const std::string        channel_name = exr_header.channels[c].name;
    radiometry::SpectrumType layer_spectrum_type;

    if (isSpectralChannel(channel_name, polarisation_component, wl, wl_rerad, layer_spectrum_type))
    {
      _spectrumType = _spectrumType | layer_spectrum_type;

      if (radiometry::isBispectral(layer_spectrum_type))
      {
        wavelengths_index_reradiation.push_back(std::make_pair(std::make_pair(wl, wl_rerad), c));
      }
      else if (radiometry::isEmissive(layer_spectrum_type))
      {
        wavelengths_index_emissive[polarisation_component].push_back(std::make_pair(wl, c));
      }
      else if (radiometry::isReflective(layer_spectrum_type))
      {
        wavelengths_index_reflective.push_back(std::make_pair(wl, c));
      }
    }
  }

  // -------------------------------------------------------------------------
  // Sanity check
  // -------------------------------------------------------------------------

  // All wavelength array must match
  if (emissive())
  {
    const size_t n_emissive_wavelengths = wavelengths_index_emissive[0].size();
    if (n_emissive_wavelengths == 0) throw MRF_ERROR_WRONG_PIXEL_FORMAT;

    // Now, sort the wavelength_index element based on wavelength
    std::sort(wavelengths_index_emissive[0].begin(), wavelengths_index_emissive[0].end());

    // We need to check the other wavelengths if polarised
    if (polarised())
    {
      for (size_t stokes_idx = 1; stokes_idx < wavelengths_index_emissive.size(); stokes_idx++)
      {
        // Size missmatch. Case of 0 cannot happen there: checked before
        if (wavelengths_index_emissive[stokes_idx].size() != n_emissive_wavelengths) throw MRF_ERROR_WRONG_PIXEL_FORMAT;

        // Now, sort the wavelength_index element based on wavelength
        std::sort(wavelengths_index_emissive[stokes_idx].begin(), wavelengths_index_emissive[stokes_idx].end());

        // We check the wavelengths match
        for (size_t wl_idx = 0; wl_idx < n_emissive_wavelengths; wl_idx++)
        {
          if (wavelengths_index_emissive[0][wl_idx].first != wavelengths_index_emissive[stokes_idx][wl_idx].first)
            throw MRF_ERROR_WRONG_PIXEL_FORMAT;
        }
      }
    }
  }

  std::sort(wavelengths_index_reflective.begin(), wavelengths_index_reflective.end());


  // If both reflective and emissive, we need to perform a last sanity check
  if (emissive() && reflective())
  {
    const size_t n_emissive_wavelengths   = wavelengths_index_emissive[0].size();
    const size_t n_reflective_wavelengths = wavelengths_index_reflective.size();

    if (n_emissive_wavelengths != n_reflective_wavelengths) throw MRF_ERROR_WRONG_PIXEL_FORMAT;

    for (size_t wl_idx = 0; wl_idx < n_emissive_wavelengths; wl_idx++)
    {
      if (wavelengths_index_emissive[0][wl_idx].first != wavelengths_index_reflective[wl_idx].first)
        throw MRF_ERROR_WRONG_PIXEL_FORMAT;
    }
  }

  // Check every single reradiation have all upper wavelength values
  // Note: this shall not be mandatory in the format but, we only support that for now
  if (bispectral())
  {
    // Sorts the reradiation
    struct
    {
      bool
      operator()(std::pair<std::pair<double, double>, size_t> a, std::pair<std::pair<double, double>, size_t> b) const
      {
        if (a.first.first == b.first.first)
        {
          return a.first.second < b.first.second;
        }
        else
        {
          return a.first.first < b.first.first;
        }
      }
    } sortBispectral;
    std::sort(wavelengths_index_reradiation.begin(), wavelengths_index_reradiation.end(), sortBispectral);

    double currDiagWl  = wavelengths_index_reflective[0].first;
    size_t diagonalIdx = 0;
    size_t reradIdx    = 1;

    for (size_t rr = 0; rr < wavelengths_index_reradiation.size(); rr++)
    {
      const auto &rerad = wavelengths_index_reradiation[rr];

      if (rerad.first.first > currDiagWl)
      {
        if (diagonalIdx + reradIdx != wavelengths_index_reflective.size())
        {
          std::cerr << "We need the full spectral reradiation specification" << std::endl;
          // We omit the bispectral part
          _spectrumType = _spectrumType ^ radiometry::SPECTRUM_BISPECTRAL;
          break;
        }
        diagonalIdx++;
        reradIdx = 1;

        currDiagWl = wavelengths_index_reflective[diagonalIdx].first;
      }

      if (rerad.first.first != wavelengths_index_reflective[diagonalIdx].first)
      {
        std::cerr << "We need the full spectral reradiation specification" << std::endl;
        // We omit the bispectral part
        _spectrumType = _spectrumType ^ radiometry::SPECTRUM_BISPECTRAL;
        break;
      }

      if (rerad.first.second != wavelengths_index_reflective[diagonalIdx + reradIdx].first)
      {
        std::cerr << "We need the full spectral reradiation specification" << std::endl;
        // We omit the bispectral part
        _spectrumType = _spectrumType ^ radiometry::SPECTRUM_BISPECTRAL;
        break;
      }

      reradIdx++;
    }
  }

  // -------------------------------------------------------------------------
  // Access image header
  // -------------------------------------------------------------------------

  EXRImage exr_image;
  InitEXRImage(&exr_image);

  // Read HALF channels as FLOAT.
  for (int i = 0; i < exr_header.num_channels; i++)
  {
    if (exr_header.pixel_types[i] == TINYEXR_PIXELTYPE_HALF)
    {
      exr_header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT;
    }
  }

  ret = LoadEXRImageFromFile(&exr_image, &exr_header, filename.c_str(), &err);
  if (ret != 0)
  {
    std::cerr << "Load EXR err: " << err << std::endl;
    throw MRF_ERROR_DECODING_FILE;
  }

  // -----------------------------------------------------------------------
  // Allocate memory
  // -----------------------------------------------------------------------
  std::vector<mrf::uint> wavelengths;

  if (emissive())
  {
    wavelengths.resize(wavelengths_index_emissive[0].size());

    for (size_t wl_idx = 0; wl_idx < wavelengths_index_emissive[0].size(); wl_idx++)
    {
      wavelengths[wl_idx] = static_cast<mrf::uint>(std::round(wavelengths_index_emissive[0][wl_idx].first));
    }
  }
  else
  {
    wavelengths.resize(wavelengths_index_reflective.size());

    for (size_t wl_idx = 0; wl_idx < wavelengths_index_reflective.size(); wl_idx++)
    {
      wavelengths[wl_idx] = static_cast<mrf::uint>(std::round(wavelengths_index_reflective[wl_idx].first));
    }
  }

  allocateMemory(exr_image.width, exr_image.height, wavelengths);

  // -------------------------------------------------------------------------
  // Access image data
  // -------------------------------------------------------------------------

  std::function<bool(const EXRHeader &, const EXRImage &, const size_t, mrf::data_struct::Array2D &)> readEXRImage
      = [this](
            const EXRHeader &          exr_header,
            const EXRImage &           exr_image,
            size_t                     channel_idx,
            mrf::data_struct::Array2D &buffer) {
          if (exr_image.images != NULL)
          {
            switch (exr_header.pixel_types[channel_idx])
            {
              case TINYEXR_PIXELTYPE_FLOAT:
              {
                float *exr_buffer = (float *)(exr_image.images[channel_idx]);
                memcpy(&buffer(0), &exr_buffer[0], width() * height() * sizeof(float));
              }
              break;

              case TINYEXR_PIXELTYPE_UINT:
              {
                unsigned int *exr_buffer = (unsigned int *)(exr_image.images[channel_idx]);

                #pragma omp parallel for schedule(static)
                for (int p = 0; p < int(width() * height()); p++)
                {
                  buffer(p) = float(exr_buffer[p]) / float(std::numeric_limits<unsigned int>::max());
                }
              }
              break;

              default:
                return false;
            }
          }
          else if (exr_image.tiles != NULL)
          {
            // Not sure if necessary...
            // Maybe the width / height are the same for every tile
            int prev_width  = 0;
            int prev_height = 0;

            for (int t = 0; t < exr_image.num_tiles; t++)
            {
              const int tile_width  = exr_image.tiles[t].width;
              const int tile_height = exr_image.tiles[t].height;

              int curr_x = exr_image.tiles[t].offset_x * prev_width;
              int curr_y = exr_image.tiles[t].offset_y * prev_height;

              prev_width  = tile_width;
              prev_height = tile_height;

              for (size_t wl = 0; wl < _wavelengths.size(); wl++)
              {
                switch (exr_header.pixel_types[channel_idx])
                {
                  case TINYEXR_PIXELTYPE_FLOAT:
                  {
                    float *exr_buffer = (float *)(exr_image.tiles[t].images[channel_idx]);

                    for (int y = 0, local_y = curr_y; y < tile_height && local_y < int(height()); y++, local_y++)
                    {
                      memcpy(&buffer(local_y * width()), &exr_buffer[y * tile_width], tile_width * sizeof(float));
                    }
                  }
                  break;

                  case TINYEXR_PIXELTYPE_UINT:
                  {
                    unsigned int *exr_buffer = (unsigned int *)(exr_image.tiles[t].images[channel_idx]);

                    for (int y = 0, local_y = curr_y; y < tile_height && local_y < int(height()); y++, local_y++)
                    {
                      for (int x = 0, local_x = curr_x; x < tile_width && local_x < int(width()); x++, local_x++)
                      {
                        buffer(local_y * width() + local_x)
                            = float(exr_buffer[y * tile_width + x]) / float(std::numeric_limits<unsigned int>::max());
                      }
                    }
                  }
                  break;

                  default:
                    return false;
                }
              }
            }
          }
          else
          {
            return false;
          }

          return true;
        };

  bool read_success = true;
  this->zeros();

  int inc = 0;
  for (size_t wl = 0; wl < nSpectralBands(); wl++)
  {
    for (size_t s = 0; s < nStokesComponents(); s++)
    {
      read_success
          |= readEXRImage(exr_header, exr_image, wavelengths_index_emissive[s][wl].second, emissiveFramebuffer(s)[wl]);
    }

    if (reflective())
    {
      read_success
          |= readEXRImage(exr_header, exr_image, wavelengths_index_reflective[wl].second, reflectiveFramebuffer()[wl]);

      if (bispectral())
      {
        for (size_t rr = wl + 1; rr < nSpectralBands(); rr++)
        {
          const auto &wl_channel = wavelengths_index_reradiation[inc++];
          read_success |= readEXRImage(exr_header, exr_image, wl_channel.second, reradiationFrameBuffer(wl, rr));

#ifdef NDEBUG
          if (wl_channel.first.first != _wavelengths[wl] || wl_channel.first.second != _wavelengths[rr])
          {
            std::cout << "ERROR" << std::endl;
            assert(false);
          }
#endif
        }
      }
    }

    progress(static_cast<int>(float(100 * wl) / float(nSpectralBands() - 1)));
  }

  // -------------------------------------------------------------------------
  // Free image data
  // -------------------------------------------------------------------------

  FreeEXRImage(&exr_image);
  progress(100);

  if (!read_success)
  {
    throw MRF_ERROR_DECODING_FILE;
  }
}


EXRSpectralImage::EXRSpectralImage(UniformSpectralImage const &other): PolarisedSpectralImage(other) {}


EXRSpectralImage::~EXRSpectralImage() {}


IMAGE_LOAD_SAVE_FLAGS EXRSpectralImage::save(
    const std::string &      filepath,
    const std::string &      additional_comments,
    std::function<void(int)> progress) const
{
  progress(0);

  EXRHeader header;
  InitEXRHeader(&header);

  // We need C style allocation to comply with TinyEXR free functions

  // Add custom attributes
  header.num_custom_attributes = 2;
  if (additional_comments != "")
  {
    header.num_custom_attributes++;
  }

  header.custom_attributes = (EXRAttribute *)calloc(header.num_custom_attributes, sizeof(EXRAttribute));

  std::function<void(EXRAttribute &, const char *, const char *)> writeStringAttr
      = [this](EXRAttribute &attribute, const char *attrName, const char *attrValue) {
          strcpy(attribute.name, attrName);
          strcpy(attribute.type, "string");

          attribute.size  = static_cast<int>(strlen(attrValue) + 1);
          attribute.value = (unsigned char *)calloc(attribute.size, sizeof(unsigned char));
          memcpy(attribute.value, attrValue, attribute.size * sizeof(char));
        };

  // Mandatory field to specify the layout version
  writeStringAttr(header.custom_attributes[0], "spectralLayoutVersion", "1.0");

  // Mandatory field to specify radiometric units
  writeStringAttr(header.custom_attributes[1], "emissiveUnits", "W.m^-2.sr^-1");

  if (additional_comments != "")
  {
    // Add in the Header the additional_information string
    writeStringAttr(header.custom_attributes[2], "mrfAdditionalInformation", additional_comments.c_str());
  }

  EXRImage image;
  InitEXRImage(&image);

  image.width        = static_cast<int>(width());
  image.height       = static_cast<int>(height());
  image.num_channels = static_cast<int>(
      3
      + (nStokesComponents() + (reflective() ? 1 : 0) + (bispectral() ? reradiationSize() : 0)) * _wavelengths.size());

  header.num_channels          = image.num_channels;
  header.channels              = (EXRChannelInfo *)calloc(header.num_channels, sizeof(EXRChannelInfo));
  header.pixel_types           = (int *)calloc(header.num_channels, sizeof(int));
  header.requested_pixel_types = (int *)calloc(header.num_channels, sizeof(int));
  header.compression_type      = TINYEXR_COMPRESSIONTYPE_ZIP;

  // -------------------------------------------------------------------------
  // Channel image buffer
  // -------------------------------------------------------------------------

  float **images = (float **)calloc(image.num_channels, sizeof(float **));

  for (int i = 0; i < image.num_channels; i++)
  {
    images[i] = (float *)calloc(image.width * image.height, sizeof(float *));

    header.pixel_types[i]           = TINYEXR_PIXELTYPE_FLOAT;   // pixel type of input image
    header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT;   // pixel type of output image to be stored in .EXR
  }

  size_t header_offset = 0;

  // -------------------------------------------------------------------------
  // RGB
  // -------------------------------------------------------------------------

  // Apply spectral conversion to write R, G, B channels.
  // This allows our image to be opened with a standard image software supporting EXRs.
  std::unique_ptr<ColorImage>   rgb_image(new EXRColorImage);
  mrf::color::SpectrumConverter sc;
  sc.emissiveSpectralImageToRGBImage(*this, *rgb_image);


  // RGB
  for (int c = 0; c < 3; c++, header_offset++)
  {
    header.channels[header_offset].name[0] = "BGR"[c];
    header.channels[header_offset].name[1] = '\0';

    #pragma omp parallel for schedule(static)
    for (int i = 0; i < static_cast<int>(width() * height()); i++)
    {
      images[c][i] = (*rgb_image)[i][c];
    }
  }

  // Write B, G, R instead of R, G, B.
  // OpenEXR official library sorts layers alphabetically
  std::swap(images[0], images[2]);

  for (size_t i = 0; i < _wavelengths.size(); i++)
  {
    if (emissive())
    {
      for (size_t s = 0; s < nStokesComponents(); s++)
      {
        std::stringstream ss;
        ss << "S" << s << "." << _wavelengths[i] << "nm";

        memcpy(
            header.channels[header_offset].name,
            ss.str().c_str(),
            std::min(sizeof(header.channels[header_offset].name) - 1, ss.str().size() + 1));

        memcpy(&images[header_offset][0], &emissiveFramebuffer(s)[i](0), width() * height() * sizeof(float));

        ++header_offset;
      }
    }

    if (reflective())
    {
      // Export the main diagonal
      std::stringstream ss;
      ss << "T"
         << "." << _wavelengths[i] << "nm";

      memcpy(
          header.channels[header_offset].name,
          ss.str().c_str(),
          std::min(sizeof(header.channels[header_offset].name) - 1, ss.str().size() + 1));

      memcpy(&images[header_offset][0], reflectiveFramebuffer()[i].data(), width() * height() * sizeof(float));

      ++header_offset;

      if (bispectral())
      {
        for (size_t j = i + 1; j < _wavelengths.size(); j++)
        {
          std::stringstream ss_b;
          ss_b << ss.str() << "." << _wavelengths[j] << "nm";

          memcpy(
              header.channels[header_offset].name,
              ss_b.str().c_str(),
              std::min(sizeof(header.channels[header_offset].name) - 1, ss.str().size() + 1));

          memcpy(&images[header_offset][0], reradiationFrameBuffer(i, j).data(), width() * height() * sizeof(float));

          ++header_offset;
        }
      }
    }
  }

  image.images = (unsigned char **)images;

  // Write the EXR
  const char *err;
  int         ret = SaveEXRImageToFile(&image, &header, filepath.c_str(), &err);

  if (ret != TINYEXR_SUCCESS)
  {
    return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_ERROR_SAVING_FILE;
  }

  // Lines below can be avoided
  // free(header.channels);
  // free(header.pixel_types);
  // free(header.requested_pixel_types);
  // USE tineyexr directly
  FreeEXRHeader(&header);

  // This frees the images buffer
  FreeEXRImage(&image);

  return MRF_NO_ERROR;
}


bool EXRSpectralImage::isSpectral(std::string const &filepath)
{
  // 1. Read EXR version.
  EXRVersion exr_version;

  int ret = ParseEXRVersionFromFile(&exr_version, filepath.c_str());
  if (ret != 0)
  {
    std::cerr << "Invalid EXR file: " << filepath << std::endl;
    throw MRF_ERROR_LOADING_FILE;
  }

  if (exr_version.multipart)
  {
    // must be multipart flag is false.
    throw MRF_ERROR_DECODING_FILE;
  }

  // 2. Read EXR header
  EXRHeader exr_header;
  InitEXRHeader(&exr_header);

  const char *err;
  ret = ParseEXRHeaderFromFile(&exr_header, &exr_version, filepath.c_str(), &err);
  if (ret != 0)
  {
    std::cerr << "Parse EXR err: " << err << std::endl;
    throw MRF_ERROR_DECODING_FILE;
  }

  // 3. Determine if there is spectral channels
  // This allows going slightly out of our specs with channels outside a proper
  // view. In our Spectral EXR specs, S0 shall contains the intensity when not
  // in a polarised form. S0, S1 S2 & S3 are populated all togheter otherwise.
  int                      polarisation_component;
  double                   wl, wl_rerad;
  radiometry::SpectrumType spectrumType;

  for (int c = 0; c < exr_header.num_channels; c++)
  {
    const std::string channel_name = exr_header.channels[c].name;

    if (isSpectralChannel(channel_name, polarisation_component, wl, wl_rerad, spectrumType))
    {
      return true;
    }
  }

  return false;
}


bool EXRSpectralImage::isSpectralChannel(
    const std::string &       s,
    int &                     polarisation_component,
    double &                  wavelength_nm,
    double &                  wavelength_to_nm,
    radiometry::SpectrumType &type)
{
  const std::string exprRefl   = "T";
  const std::string exprStokes = "S([0-3])";
  const std::string exprPola   = "((" + exprStokes + ")|" + exprRefl + ")";
  const std::string exprValue  = "(\\d*,?\\d*([Ee][+-]?\\d+)?)";
  const std::string exprUnits  = "(Y|Z|E|P|T|G|M|k|h|da|d|c|m|u|n|p)?(m|Hz)";

  const std::regex exprDiagonal("^" + exprPola + "\\." + exprValue + exprUnits + "$");
  const std::regex exprRerad("^" + exprRefl + "\\." + exprValue + exprUnits + "\\." + exprValue + exprUnits + "$");

  std::smatch matches;

  if (std::regex_search(s, matches, exprDiagonal))
  {
    if (matches.size() != 8)
    {
      // Something went wrong with the parsing. This shall not occur.
      return false;
    }

    std::string       central_value_str = matches[4].str();
    const std::string prefix            = matches[6].str();
    const std::string units             = matches[7].str();

    switch (matches[1].str()[0])
    {
      case 'S':
        polarisation_component = std::stoi(matches[3].str());
        type                   = radiometry::SPECTRUM_EMISSIVE;

        if (polarisation_component > 0)
        {
          type = type | radiometry::SPECTRUM_POLARISED;
        }

        break;
      case 'T':
        type = radiometry::SPECTRUM_REFLECTIVE;
        break;
      default:
        return false;
    }

    // Get value
    std::replace(central_value_str.begin(), central_value_str.end(), ',', '.');
    float value = std::stof(central_value_str);

    // Apply multiplier
    wavelength_nm = strToNanometers(value, prefix, units);
    return true;
  }
  else if (std::regex_search(s, matches, exprRerad))
  {
    if (matches.size() != 9)
    {
      // Something went wrong with the parsing. This shall not occur.
      return false;
    }

    // Get value illumination
    std::string centralValueStrI(matches[1].str());
    std::replace(centralValueStrI.begin(), centralValueStrI.end(), ',', '.');
    const float value_i = std::stof(centralValueStrI);

    wavelength_nm = strToNanometers(
        value_i,
        matches[3].str(),   // Unit multiplier
        matches[4].str()    // Units
    );

    // Get value reradiation
    std::string centralValueStrO(matches[5].str());
    std::replace(centralValueStrO.begin(), centralValueStrO.end(), ',', '.');
    const float value_o = std::stof(centralValueStrO);

    wavelength_to_nm = strToNanometers(
        value_o,
        matches[7].str(),   // Unit multiplier
        matches[8].str()    // Units
    );

    type = radiometry::SPECTRUM_BISPECTRAL;
    return true;
  }

  return false;
}

double EXRSpectralImage::strToNanometers(const double &value, const std::string &prefix, const std::string &units)
{
  if (prefix == "n" && units == "m") return value;

  double wavelength_nm = value;

  const std::map<std::string, double> unit_prefix
      = {{"Y", 1e24},
         {"Z", 1e21},
         {"E", 1e18},
         {"P", 1e15},
         {"T", 1e12},
         {"G", 1e9},
         {"M", 1e6},
         {"k", 1e3},
         {"h", 1e2},
         {"da", 1e1},
         {"d", 1e-1},
         {"c", 1e-2},
         {"m", 1e-3},
         {"u", 1e-6},
         {"n", 1e-9},
         {"p", 1e-12}};

  // Apply multiplier
  if (prefix.size() > 0)
  {
    wavelength_nm *= unit_prefix.at(prefix);
  }

  // Apply units
  if (units == "Hz")
  {
    wavelength_nm = 299792458. / wavelength_nm * 1e9;
  }
  else if (units == "m")
  {
    wavelength_nm = wavelength_nm * 1e9;
  }
  else
  {
    // Unknown unit
    // Something went wrong with the parsing. This shall not occur.
    throw std::out_of_range("Unknown unit");
  }

  return wavelength_nm;
}

//Protected Methods
void EXRSpectralImage::updateMetadata(EXRHeader const &exr_header)
{
  std::map<std::string, std::string> meta_data;

  for (int i = 0; i < exr_header.num_custom_attributes; i++)
  {
    std::string const name(exr_header.custom_attributes[i].name);
    std::string const value(
        reinterpret_cast<char const *>(exr_header.custom_attributes[i].value),
        exr_header.custom_attributes[i].size);

    std::pair<std::string, std::string> const a_pair = std::make_pair(name, value);
    _metadata.insert(a_pair);
  }
}

}   // namespace image
}   // namespace mrf
