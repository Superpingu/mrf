/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/exr_color_image.hpp>
#include <mrf_core/io/exr_io.hpp>

#include <tinyexr/tinyexr.h>

#include <utility>
#include <functional>
#include <array>

using mrf::color::Color;

namespace mrf
{
namespace image
{
EXRColorImage::EXRColorImage(size_t width, size_t height): ColorImage(width, height) {}

EXRColorImage::EXRColorImage(const std::string &file_path, std::function<void(int)> progress): ColorImage()
{
  progress(0);

  // -------------------------------------------------------------------------
  // 1. Read EXR version.
  // -------------------------------------------------------------------------

  EXRVersion exr_version;

  int ret = ParseEXRVersionFromFile(&exr_version, file_path.c_str());
  if (ret != 0)
  {
    std::cerr << "Invalid EXR file: " << file_path << std::endl;
    throw MRF_ERROR_LOADING_FILE;
  }

  if (exr_version.multipart)
  {
    // must be multipart flag is false.
    throw MRF_ERROR_DECODING_FILE;
  }

  // -------------------------------------------------------------------------
  // 2. Read EXR header
  // -------------------------------------------------------------------------

  EXRHeader exr_header;
  InitEXRHeader(&exr_header);

  const char *err;
  ret = ParseEXRHeaderFromFile(&exr_header, &exr_version, file_path.c_str(), &err);
  if (ret != 0)
  {
    std::cerr << "Parse EXR error: " << err << std::endl;
    throw MRF_ERROR_DECODING_FILE;
  }

  // -------------------------------------------------------------------------
  // 3. Determine channels position
  // -------------------------------------------------------------------------

  // Channels default to use first layer.
  // If no alpha is found, we set later the alpha value to 1.f
  // For grayscale EXR we set R = G = B.
  std::array<int, 4> idx_rgba  = {};
  bool               haveAlpha = false;

  for (int c = 0; c < exr_header.num_channels; c++)
  {
    if (strcmp(exr_header.channels[c].name, "R") == 0)
    {
      idx_rgba[0] = c;
    }
    else if (strcmp(exr_header.channels[c].name, "G") == 0)
    {
      idx_rgba[1] = c;
    }
    else if (strcmp(exr_header.channels[c].name, "B") == 0)
    {
      idx_rgba[2] = c;
    }
    else if (strcmp(exr_header.channels[c].name, "A") == 0)
    {
      haveAlpha   = true;
      idx_rgba[3] = c;
    }
  }

  updateMetadata(exr_header);


  EXRImage exr_image;
  InitEXRImage(&exr_image);

  // Read HALF channels as FLOAT.
  for (int i = 0; i < exr_header.num_channels; i++)
  {
    if (exr_header.pixel_types[i] == TINYEXR_PIXELTYPE_HALF)
    {
      exr_header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT;
    }
  }

  // -------------------------------------------------------------------------
  // 4. Access image data
  // -------------------------------------------------------------------------

  ret = LoadEXRImageFromFile(&exr_image, &exr_header, file_path.c_str(), &err);
  if (ret != 0)
  {
    std::cerr << "Load EXR error: " << err << std::endl;
    throw MRF_ERROR_DECODING_FILE;
  }


  MultiChannelImage::init(exr_image.width, exr_image.height);

  // `exr_image.images` will be filled when EXR is scanline format.
  if (exr_image.images != NULL)
  {
    const int n_channels = (haveAlpha) ? 4 : 3;
    for (int c = 0; c < n_channels; c++)
    {
      switch (exr_header.pixel_types[idx_rgba[c]])
      {
        case TINYEXR_PIXELTYPE_FLOAT:
        {
          float *buffer = (float *)(exr_image.images[idx_rgba[c]]);

          #pragma omp parallel for schedule(static)
          for (int i = 0; i < exr_image.width * exr_image.height; i++)
          {
            (*this)[i][c] = buffer[i];
          }
        }
        break;

        case TINYEXR_PIXELTYPE_UINT:
        {
          unsigned int *buffer = (unsigned int *)(exr_image.images[idx_rgba[c]]);

          #pragma omp parallel for schedule(static)
          for (int i = 0; i < exr_image.width * exr_image.height; i++)
          {
            (*this)[i][c] = float(buffer[i]) / float(std::numeric_limits<unsigned int>::max());
          }
        }
        break;

        default:
          FreeEXRImage(&exr_image);
          throw MRF_ERROR_WRONG_PIXEL_FORMAT;
          break;
      }
    }
  }
  // `exr_image.tiled` will be filled when EXR is tiled format.
  else if (exr_image.tiles != NULL)
  {
    // Not sure if necessary...
    // Maybe the width / height are the same for every tile
    int prev_width  = 0;
    int prev_height = 0;

    for (int i = 0; i < exr_image.num_tiles; i++)
    {
      const int tile_width  = exr_image.tiles[i].width;
      const int tile_height = exr_image.tiles[i].height;

      int curr_x = exr_image.tiles[i].offset_x * prev_width;
      int curr_y = exr_image.tiles[i].offset_y * prev_height;

      prev_width  = tile_width;
      prev_height = tile_height;

      const int n_channels = (haveAlpha) ? 4 : 3;
      for (int c = 0; c < n_channels; c++)
      {
        switch (exr_header.pixel_types[idx_rgba[c]])
        {
          case TINYEXR_PIXELTYPE_FLOAT:
          {
            float *buffer = (float *)(exr_image.tiles[i].images[idx_rgba[c]]);

            for (int y = 0, local_y = curr_y; y < tile_height && local_y < int(height()); y++, local_y++)
            {
              for (int x = 0, local_x = curr_x; x < tile_width && local_x < int(width()); x++, local_x++)
              {
                const size_t local_i   = local_y * width() + local_x;
                const size_t foreign_i = y * tile_width + x;

                (*this)[local_i][c] = buffer[foreign_i];
              }
            }
          }
          break;

          case TINYEXR_PIXELTYPE_UINT:
          {
            unsigned int *buffer = (unsigned int *)(exr_image.tiles[i].images[idx_rgba[c]]);

            for (int y = 0, local_y = curr_y; y < tile_height && local_y < int(height()); y++, local_y++)
            {
              for (int x = 0, local_x = curr_x; x < tile_width && local_x < int(width()); x++, local_x++)
              {
                const size_t local_i   = local_y * width() + local_x;
                const size_t foreign_i = y * tile_width + x;

                (*this)[local_i][c] = float(buffer[foreign_i]) / float(std::numeric_limits<unsigned int>::max());
              }
            }
          }
          break;

          default:
            FreeEXRImage(&exr_image);
            throw MRF_ERROR_WRONG_PIXEL_FORMAT;
            break;
        }
      }
    }
  }
  else
  {
    FreeEXRImage(&exr_image);
    throw MRF_ERROR_DECODING_FILE;
  }

  // In case no alpha channel exists in the EXR, set it to full opacity
  if (!haveAlpha)
  {
    #pragma omp parallel for schedule(static)
    for (int i = 0; i < int(width() * height()); i++)
    {
      (*this)[i][3] = 1.F;
    }
  }

  // -------------------------------------------------------------------------
  // 5. Free image data
  // -------------------------------------------------------------------------

  FreeEXRImage(&exr_image);

  progress(100);
}


EXRColorImage::EXRColorImage(const ColorImage &img): ColorImage(img) {}


EXRColorImage::~EXRColorImage() {}


IMAGE_LOAD_SAVE_FLAGS EXRColorImage::save(
    const std::string &      file_path,
    const std::string &      additional_comments,
    std::function<void(int)> progress) const
{
  progress(0);
  std::vector<float> images[3];

  for (int i = 0; i < 3; i++)
  {
    images[i].resize(_width * _height);
  }

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(_width * _height); i++)
  {
    for (int c = 0; c < 3; c++)
    {
      images[c][i] = _pixels[i][c];
    }
  }

  // TODO: Support for XYZ
  progress(100);

  return io::saveAsExr(file_path, images, _width, _height, additional_comments);
}

//Protected Methods
void EXRColorImage::updateMetadata(EXRHeader const &exr_header)
{
  std::map<std::string, std::string> meta_data;

  for (int i = 0; i < exr_header.num_custom_attributes; i++)
  {
    std::string const name(exr_header.custom_attributes[i].name);
    std::string const value(
        reinterpret_cast<char const *>(exr_header.custom_attributes[i].value),
        exr_header.custom_attributes[i].size);

    std::pair<std::string, std::string> const a_pair = std::make_pair(name, value);
    _metadata.insert(a_pair);
  }
}
}   // namespace image
}   // namespace mrf
