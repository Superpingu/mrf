/**
 * Author: Romain Pacanowski @ institutoptique . fr
 * Copyright CNRS 2020
 *
 * Modifications:
 *
 * Alban Fichet @ institutoptique . fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core/image/image_module.hpp>
#include <mrf_core/util/string_parsing.hpp>

#include <array>
#include <string>
#include <iostream>
#include <fstream>
#include <functional>
#include <memory>

namespace mrf
{
namespace image
{
/**
 * Definitions of image formats
 */
static constexpr char const *PNG_EXTENSION = "png";
static constexpr char const *PFM_EXTENSION = "pfm";

static constexpr char const *EXR_EXTENSION  = "exr";
static constexpr char const *WARD_EXTENSION = "hdr";

static constexpr char const *ART_EXTENSION         = "artraw";
static constexpr char const *ENVY_HEADER_EXTENSION = "hdr";
static constexpr char const *ENVY_RAW_EXTENSION    = "raw";

/**
 * Static List of image extensions according to their meaning
 */
static constexpr std::array<char const *const, 1> LDR_EXTENSIONS = {PNG_EXTENSION};

static constexpr std::array<char const *const, 3> HDR_EXTENSIONS = {EXR_EXTENSION, WARD_EXTENSION, PFM_EXTENSION};

static constexpr std::array<char const *const, 4> SPECTRAL_EXTENSIONS
    = {ART_EXTENSION, ENVY_HEADER_EXTENSION, ENVY_RAW_EXTENSION, EXR_EXTENSION};


// ---------------------------------------------------------------------------
// Utility Functions
// ---------------------------------------------------------------------------

inline static bool isSameExtension(char const *const ext1, char const *const ext2)
{
  std::string ext1_str(ext1);
  std::string ext2_str(ext2);

  std::transform(ext1_str.begin(), ext1_str.end(), ext1_str.begin(), [](unsigned char c) -> unsigned char {
    return static_cast<unsigned char>(std::tolower(c));
  });
  std::transform(ext2_str.begin(), ext2_str.end(), ext2_str.begin(), [](unsigned char c) -> unsigned char {
    return static_cast<unsigned char>(std::tolower(c));
  });

  return ext1_str == ext2_str;
}


template<class ARRAY_TYPE>
inline static bool belongToExtension(ARRAY_TYPE const &extensions, char const *const ext)
{
  for (size_t i = 0; i < extensions.size(); i++)
  {
    if (isSameExtension(extensions[i], ext))
    {
      return true;
    }
  }

  return false;
}

/**
 * @brief Checks if the provided extension is a supported low dynamic
 * range format.
 *
 * @param ext File extension without a dot
 * @return true File is supported
 * @return false File is either not supported or not an LDR image file
 */
inline static bool isLDRExtension(const char *const ext)
{
  return belongToExtension(LDR_EXTENSIONS, ext);
}


/**
 * @brief Checks if the provided extension may be a supported high dynamic
 * range format.
 *
 * @param ext File extension without a dot
 * @return true File is supported
 * @return false File is either not supported or not an HDR image file
 */
inline static bool isHDRExtension(const char *const ext)
{
  return belongToExtension(HDR_EXTENSIONS, ext);
}


/**
 * @brief Checks if the provided extension may be a supported color image
 * format (LDR or HDR).
 *
 * @param ext File extension without a dot
 * @return true File is supported
 * @return false File is either not supported or not an color image file
 */
inline static bool isColorExtension(const char *const ext)
{
  return isLDRExtension(ext) || isHDRExtension(ext);
}


/**
 * @brief Checks if the provided extension may be a supported spectral
 * image format.
 *
 * @param ext File extension without a dot
 * @return true File is supported
 * @return false File is either not supported or not an spectral image file
 */
inline static bool isSpectralExtension(const char *const ext)
{
  return belongToExtension(SPECTRAL_EXTENSIONS, ext);
}


/**
 * @brief Checks if the provided extension may be a supported image format.
 *
 * @param ext File extension without a dot
 * @return true File is supported
 * @return false File is either not supported or not an image file
 */
inline static bool isSupportedExtension(const char *const ext)
{
  return belongToExtension(LDR_EXTENSIONS, ext) || belongToExtension(HDR_EXTENSIONS, ext)
         || belongToExtension(SPECTRAL_EXTENSIONS, ext);
}


/**
 * @brief Checks if the provided file may be a supported image format.
 *
 * @param ext Complete filepath
 * @return true File is supported
 * @return false File is either not supported or not an image file
 */
inline static bool isSupportedFile(const std::string &filepath)
{
  std::string ext;
  mrf::util::StringParsing::getFileExtension(filepath.c_str(), ext);

  return isSupportedExtension(ext.c_str());
}


/**
 * @brief Checks whether a file with .hdr extension is an radiance file
 * (Greg Ward Format) or something else.
 *
 * @param ext Complete filepath
 * @return true File is radiance file
 * @return false otherwise
 */
inline static bool isRadianceHDR(const std::string &file_path)
{
  std::ifstream reader;
  reader.open(file_path, std::ios::in | std::ios::binary);

  if (!reader.is_open())
  {
    std::cerr << "Failed to open " << std::string(file_path) << std::endl;
    return false;
  }

  char start_data[2] = {0};

  reader.read(start_data, 2 * sizeof(char));
  reader.close();

  if (start_data[0] == '#' && start_data[1] == '?')
  {
    return true;
  }

  return false;
}


/**
 * @brief Checks whether a file with .exr extension is an spectral file:
 * OpenEXR image with spectral data information.
 *
 * @param ext Complete filepath
 * @return true File is a spectral EXR
 * @return false otherwise
 */
inline static bool isSpectralEXR(std::string const &file_path)
{
  return EXRSpectralImage::isSpectral(file_path);
}


/**
 * @brief Checks whether a file may be a supported color image format
 * (LDR or HDR).
 *
 * @param ext Complete filepath
 * @return true File is supported
 * @return false File is either not supported or not an color image file
 */
inline static bool isColorImage(const std::string &filepath)
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(filepath.c_str(), extension);

  if (isSameExtension(extension.c_str(), "hdr"))
  {
    return isRadianceHDR(filepath);
  }

  return isColorExtension(extension.c_str());
}


/**
 * @brief Checks whether a file may be a supported spectral image format.
 *
 * @param ext Complete filepath
 * @return true File is supported
 * @return false File is either not supported or not an spectral image file
 */
inline static bool isSpectralImage(const std::string &filepath)
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(filepath.c_str(), extension);

  if (isSameExtension(extension.c_str(), EXR_EXTENSION))
  {
    return isSpectralEXR(filepath);
  }

  if (isSameExtension(extension.c_str(), "hdr"))
  {
    return !isRadianceHDR(filepath);
  }

  return isSpectralExtension(extension.c_str());
}

/**
 * @brief Get the Color Image For Save object
 *
 * Depending on the file type,
 * the correct ColorImage object is created.
 *
 * @param file_path Path that the image is going to be saved to
 * @return std::unique_ptr<ColorImage>
 */
inline std::unique_ptr<ColorImage> getColorImageForSave(std::string const &file_path)
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(file_path, extension);

  if (extension == PNG_EXTENSION)
  {
    return std::unique_ptr<ColorImage>(new PNGColorImage);
  }
  else if (extension == PFM_EXTENSION)
  {
    return std::unique_ptr<ColorImage>(new PFMColorImage);
  }
  else if (extension == EXR_EXTENSION)
  {
    return std::unique_ptr<ColorImage>(new EXRColorImage);
  }
  else if (extension == WARD_EXTENSION)
  {
    return std::unique_ptr<ColorImage>(new HDRColorImage);
  }
  else
  {
    throw MRF_ERROR_WRONG_EXTENSION;
  }
}


/**
 * @brief Saves a color image
 *
 * Given an existing ColorImage, save it to the provided path with the right
 * format. The format is chosen from the file extension.
 *
 * @param img Image to save
 * @param file_path File to save the image to
 * @param saveLinearWhenLDR When using an LDR format, allows to save with no
 * gamma correction applied.
 * @param additional_comments Additional comments to add to the file metadata
 * if supported by the format.
 * @param progress Callback to monitor the saving progress (from 0 to 100
 * percent).
 * @return IMAGE_LOAD_SAVE_FLAGS
 */
inline IMAGE_LOAD_SAVE_FLAGS save(
    const ColorImage &       img,
    const std::string &      file_path,
    const std::string &      additional_comments = "",
    bool                     saveLinearWhenLDR   = false,
    std::function<void(int)> progress            = [](int) {
    })
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(file_path, extension);
  std::unique_ptr<ColorImage> output_img;

  if (extension == PNG_EXTENSION)
  {
    return PNGColorImage(img).save(file_path, saveLinearWhenLDR, additional_comments, progress);
  }
  else if (extension == PFM_EXTENSION)
  {
    output_img = std::unique_ptr<ColorImage>(new PFMColorImage(img));
  }
  else if (extension == EXR_EXTENSION)
  {
    output_img = std::unique_ptr<ColorImage>(new EXRColorImage(img));
  }
  else if (extension == WARD_EXTENSION)
  {
    output_img = std::unique_ptr<ColorImage>(new HDRColorImage(img));
  }
  else
  {
    return MRF_ERROR_WRONG_EXTENSION;
  }

  return output_img->save(file_path, additional_comments, progress);
}


/**
 * @brief Saves a spectral image
 *
 * Given an existing UniformSpectralImage, save it to the provided path with
 * the right format. The format is chosen from the file
 * extension.
 *
 * @param img Image to save
 * @param file_path File to save the image to
 * @param additional_comments Additional comments to add to the file metadata
 * if supported by the format.
 * @param progress Callback to monitor the saving progress (from 0 to 100
 * percent).
 * @return IMAGE_LOAD_SAVE_FLAGS
 */
inline IMAGE_LOAD_SAVE_FLAGS save(
    const UniformSpectralImage &img,
    const std::string &         file_path,
    const std::string &         additional_comments = "",
    std::function<void(int)>    progress            = [](int) {
    })
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(file_path, extension);
  std::unique_ptr<UniformSpectralImage> output_img;

  if (extension == ENVY_RAW_EXTENSION || extension == ENVY_HEADER_EXTENSION)
  {
    output_img = std::unique_ptr<UniformSpectralImage>(new EnviSpectralImage(img));
  }
  else if (extension == ART_EXTENSION)
  {
    output_img = std::unique_ptr<UniformSpectralImage>(new ArtRawSpectralImage(img));
  }
  else if (extension == EXR_EXTENSION)
  {
    output_img = std::unique_ptr<UniformSpectralImage>(new EXRSpectralImage(img));
  }
  else
  {
    return MRF_ERROR_WRONG_EXTENSION;
  }

  return output_img->save(file_path, additional_comments, progress);
}


/**
 * @brief Loads a color image
 *
 * Creates a ColorImage object from the provided filepath if the file format
 * is supported.
 *
 * @param file_path Path to the image to load.
 * @param loadLinearWhenLDR Specifies if we shall consider the color values as
 * linear (no gamma correction) when loading an LDR file format.
 * @param progress Callback to monitor the loading progress (from 0 to 100
 * percent).
 * @return std::unique_ptr<ColorImage>
 */
inline std::unique_ptr<ColorImage> loadColorImage(
    std::string const &      file_path,
    bool                     loadLinearWhenLDR = false,
    std::function<void(int)> progress          = [](int) {
    })
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(file_path, extension);
  std::transform(extension.begin(), extension.end(), extension.begin(), std::tolower);

  if (extension == PNG_EXTENSION)
  {
    return std::unique_ptr<ColorImage>(new PNGColorImage(file_path, progress, loadLinearWhenLDR));
  }
  else if (extension == PFM_EXTENSION)
  {
    return std::unique_ptr<ColorImage>(new PFMColorImage(file_path, progress));
  }
  else if (extension == EXR_EXTENSION)
  {
    return std::unique_ptr<ColorImage>(new EXRColorImage(file_path, progress));
  }
  else if (extension == WARD_EXTENSION)
  {
    if (!isRadianceHDR(file_path))
    {
      throw MRF_ERROR_WRONG_EXTENSION;
    }
    return std::unique_ptr<ColorImage>(new HDRColorImage(file_path, progress));
  }
  else
  {
    throw MRF_ERROR_WRONG_EXTENSION;
  }
}


/**
 * @brief Loads a color image
 *
 * Creates a UniformSpectralImage object from the provided filepath if the file format
 * is supported.
 *
 * @param file_path Path to the spectral image to load.
 * @param progress Callback to monitor the loading progress (from 0 to 100
 * percent).
 * @return std::unique_ptr<UniformSpectralImage>
 */
inline std::unique_ptr<UniformSpectralImage> loadUniformSpectralImage(
    std::string const &      file_path,
    std::function<void(int)> progress = [](int) {
    })
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(file_path, extension);

  if (extension == ENVY_RAW_EXTENSION || extension == ENVY_HEADER_EXTENSION)
  {
    if (isRadianceHDR(file_path))
    {
      throw MRF_ERROR_WRONG_EXTENSION;
    }
    return std::unique_ptr<UniformSpectralImage>(new EnviSpectralImage(file_path, progress));
  }
  else if (extension == ART_EXTENSION)
  {
    return std::unique_ptr<UniformSpectralImage>(new ArtRawSpectralImage(file_path, progress));
  }
  else if (extension == EXR_EXTENSION)
  {
    return std::unique_ptr<UniformSpectralImage>(new EXRSpectralImage(file_path, progress));
  }
  else
  {
    throw MRF_ERROR_WRONG_EXTENSION;
  }
}


/**
 * @brief Load an image
 *
 * These functions are wrappers to directly load an image depending on the the
 * compilation mode used. Usefull to avoid additional defines in the renderer
 * class.
 *
 * @param file_path Path to the image to load.
 * @param progress Callback to monitor the loading progress (from 0 to 100
 * percent).
 */
#ifdef MRF_RENDERING_MODE_SPECTRAL
inline std::unique_ptr<UniformSpectralImage> load(
    std::string const &      file_path,
    std::function<void(int)> progress = [](int) {
    })
{
  return loadUniformSpectralImage(file_path, progress);
}
#else
inline std::unique_ptr<ColorImage> load(
    std::string const &      file_path,
    std::function<void(int)> progress = [](int) {
    })
{
  return loadColorImage(file_path, false, progress);
}
#endif

}   // namespace image
}   // namespace mrf
