/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/
#pragma once

/**
 * Header to defines some constant related to Cuda
 *
 * MRF_DEVICE_CONSTANT is expanded to __device__ __constant__ when nvcc is called
 * MDF_DEVICE_FUNC  is expanded to __host__ __device__ when nvcc is called
 * otherwise these constants are not expanded and replaced by nothing
 *
 **/

#if defined(__CUDACC__)
#  define MRF_DEVICE_CONSTANT __device__ __constant__
#  define MRF_DEVICE_FUNC     __host__ __device__
#  define MRF_DEVICE          __device__
#else
#  define MRF_DEVICE_CONSTANT
#  define MRF_DEVICE_FUNC
#  define MRF_DEVICE
#endif
