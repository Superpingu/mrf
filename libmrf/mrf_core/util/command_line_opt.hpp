/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/
#pragma once

#include <string>
#include <vector>
#include <cassert>

#include <mrf_core_dll.hpp>

namespace mrf
{
namespace util
{
/**
 * @brief      This class represent an Option parsed on the CommandLine
 */
class MRF_CORE_EXPORT CommandLineOpt
{
private:
  std::string              _optionName;   // The name of the option regardless of the "-" or "--" in front of it
  std::vector<std::string> _parameters;   // The list of parameters as strings
                                          //std::string              _helpMessage; // A help Message ???

public:
  inline CommandLineOpt() {}

  inline CommandLineOpt(std::string const &option_name): _optionName(option_name) {}

  inline CommandLineOpt(std::string const &option_name, std::vector<std::string> const &params)
    : _optionName(option_name)
    , _parameters(params)
  {}

  inline void addParameter(std::string const &a_parameter) { _parameters.push_back(a_parameter); }

  inline unsigned int nbParameter() const { return static_cast<unsigned int>(_parameters.size()); }

  inline bool hasParameter() const { return _parameters.size() > 0; }

  inline std::string const &parameter(unsigned int param_index) const
  {
    assert(param_index < _parameters.size());
    return _parameters[param_index];
  }

  inline std::vector<std::string> const &parameters() const { return _parameters; }

  inline std::string const &name() const { return _optionName; }
};

// std::ostream& operator<<( std::ostream & os, CommandLineOpt const & cmdline_opt )
// {
// os << cmdline_opt.name() << ": [" ;
// for(unsigned int i=0; i < cmdline_opt.parameters().size(); i++)
// {
// os << cmdline_opt.parameter ( i ) <<  "; " ;
// }
// os << " ] " ;
// return os;
// }

}   // namespace util
}   // namespace mrf