/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Precision timer usin c++11
 * measures with precision of 1 nanoseconds
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <chrono>
#include <iostream>
#include <iomanip>

namespace mrf
{
namespace util
{
class MRF_CORE_EXPORT PrecisionTimer
{
public:
  /**
   * default constructor
   **/
  inline PrecisionTimer();
  /**
   * start
   **/
  inline void start();
  /**
   * stop
   **/
  inline void stop();
  /**
   * reset
   **/
  inline void reset();
  /**
   * elapsed time in seconds
   **/
  inline float elapsed();
  /*
   * return precision in miliseconds
   **/
  inline float getPrecisionMS() const;

private:
  std::chrono::high_resolution_clock::time_point _start_time;
  std::chrono::high_resolution_clock::time_point _stop_time;
  double                                         _seconds_elapsed;
};

inline PrecisionTimer::PrecisionTimer(): _seconds_elapsed(0) {}

inline void PrecisionTimer::start()
{
  _start_time = std::chrono::high_resolution_clock::now();
}

inline void PrecisionTimer::stop()
{
  _stop_time       = std::chrono::high_resolution_clock::now();
  _seconds_elapsed = 1e-9 * std::chrono::duration_cast<std::chrono::nanoseconds>(_stop_time - _start_time).count();
}

inline void PrecisionTimer::reset()
{
  _seconds_elapsed = 0;
}

inline float PrecisionTimer::elapsed()
{
  auto temp        = std::chrono::high_resolution_clock::now();
  _seconds_elapsed = 1e-9 * std::chrono::duration_cast<std::chrono::nanoseconds>(temp - _start_time).count();
  return static_cast<float>(_seconds_elapsed);
}

inline std::ostream &operator<<(std::ostream &os, PrecisionTimer &timer)
{
  os << std::setprecision(15) << "Done in " << timer.elapsed() << " seconds " << std::endl;
  return os;
}

inline float PrecisionTimer::getPrecisionMS() const
{
  //return precision in miliseconds
  return 1e3f * static_cast<float>(std::chrono::high_resolution_clock::period::num)
         / std::chrono::high_resolution_clock::period::den;
}
}   // namespace util
}   // namespace mrf
