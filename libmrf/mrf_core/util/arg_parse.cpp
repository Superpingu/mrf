#include <mrf_core/util/arg_parse.hpp>
#include <mrf_core/io/camera_parser.hpp>
#include <mrf_core/util/path.hpp>
#include <mrf_core_dll.hpp>

#include <string>

namespace mrf
{
namespace util
{
ArgsParsing::ArgsParsing()
{
  _has_argument = nullptr;
  _value        = nullptr;
}

ArgsParsing::~ArgsParsing()
{
  delete[] _has_argument;
  delete[] _value;
}

ArgsParsing::ArgsParsing(int argc, char **argv)
{
  CommandLine command_line = CommandLine(argc, argv);
  _has_argument            = new bool[LAST];
  for (int i = 0; i < LAST; ++i)
    _has_argument[i] = false;
  _value = new std::vector<std::string>[LAST];

  /*To add a boolean argument:
   * if (command_line.hasOption("name_of_the_arg"))
   * {
   *   updateArg(NAME_OF_ARG, 0);
   * }
   *
   *To add a non-boolean argument:
   * if (command_line.hasOptionAndValue("name_of_the_arg"))
   * {
   *   updateArg(NAME_OF_ARG, command_line.option("name_of_the_arg").parameter(0));
   * }
   * else //If you want a default value
   * {
   *   updateArg(NAME_OF_ARG, default_value);
   * }
   */

  if (command_line.hasOption("help") || command_line.hasOption("h"))
  {
    updateArg(HELP, std::to_string(0));
  }

  if (command_line.hasOption("i") || command_line.hasOption("interactive")) updateArg(INTERACTIVE, std::to_string(0));

  if (command_line.hasOptionAndValue("logging"))
  {
    updateArg(LOGGING, command_line.option("logging").parameter(0));

    std::string log_level = getSingleValue(ARG_LIST::LOGGING);

    //Singleton must be instanced as soon as possible...

    //No_Log = 0, Fatal = 1, Warning = 2, Debug = 5, Trace = 4, Info = 3
    if (log_level == "No_log" || log_level == "0")
      mrf::gui::fb::Loger::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::No_Log);
    if (log_level == "Fatal" || log_level == "1")
      mrf::gui::fb::Loger::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Fatal);
    if (log_level == "Warning" || log_level == "2")
      mrf::gui::fb::Loger::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Warning);
    if (log_level == "Info" || log_level == "3")
      mrf::gui::fb::Loger::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Info);
    if (log_level == "Trace" || log_level == "4")
      mrf::gui::fb::Loger::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Trace);
    if (log_level == "Debug" || log_level == "5")
      mrf::gui::fb::Loger::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Debug);
  }

  if (command_line.hasOptionAndValue("logfile")) updateArg(LOGFILE, command_line.option("logfile").parameter(0));

  if (command_line.hasOptionAndValue("scene"))
  {
    std::string scene_file = command_line.option("scene").parameter(0);
    std::string ext;
    mrf::util::StringParsing::getFileExtension(scene_file, ext);
    if (ext == "msf")
    {
      std::ifstream file(scene_file.c_str());
      if (file.good())
      {
        updateArg(SCENE, scene_file);
      }
      else
      {
        throw std::runtime_error("Could not open the scene file, please check that it exits.");
      }
    }
    else
    {
      throw std::runtime_error("The provided scene file is not a MSF file, aborting.");
    }
  }
  else if (argc == 2)
  {
    std::string test_scene = argv[1];
    std::string ext;
    mrf::util::StringParsing::getFileExtension(test_scene, ext);
    if (ext == "msf")
    {
      std::ifstream file(test_scene.c_str());
      if (file.good())
      {
        updateArg(SCENE, test_scene);

        //Start in interactive if launch by drag&drop and compiled with interactive support.
#ifdef RENDERER_INTERACTIVE
        updateArg(INTERACTIVE, std::to_string(0));
#endif
      }
    }
    else
    {
//TODO : RP you cannot start in interctive mode if malia has not been compiled with interactive support!
// This will cause segfault !
#ifdef RENDERER_INTERACTIVE
      updateArg(INTERACTIVE, std::to_string(0));
#else
      std::cout << "No scene specified!";
      std::string const EXECUTION_PATH     = mrf::util::Path::getExecutionPath().name();
      std::string const DEFAULT_SCENE_PATH = EXECUTION_PATH + "scenes/demo.msf";
      std::ifstream     file(DEFAULT_SCENE_PATH.c_str());
      if (file.good())
      {
        std::cout << "Attempting to start with: " << DEFAULT_SCENE_PATH << std::endl;
        updateArg(SCENE, DEFAULT_SCENE_PATH);
      }
      else
      {
        throw std::runtime_error("No scene specified and demo scene is not found. Aborting. Try -scene your_scene.msf");
        std::exit(-1);
      }
#endif
    }
  }
  else if (argc > 1 && !_has_argument[INTERACTIVE])
  {
    std::string const EXECUTION_PATH     = mrf::util::Path::getExecutionPath().name();
    std::string const DEFAULT_SCENE_PATH = EXECUTION_PATH + "scenes/demo.msf";
    std::ifstream     file(DEFAULT_SCENE_PATH.c_str());
    if (file.good())
    {
      std::cout << "No scene specified, using default one: " << DEFAULT_SCENE_PATH << std::endl;
      updateArg(SCENE, DEFAULT_SCENE_PATH);
    }
  }
  else
  {
    std::cout << "No scene specified";

    // #ifdef RENDERER_INTERACTIVE
    // std::cout << " starting in interactive mode." << std::endl;
    // FORCING INTERACTIVE
    updateArg(INTERACTIVE, std::to_string(0));
    // #else
    std::cout << " starting with default Scene." << std::endl;
    std::string const EXECUTION_PATH     = mrf::util::Path::getExecutionPath().name();
    std::string const DEFAULT_SCENE_PATH = EXECUTION_PATH + "scenes/demo.msf";
    std::ifstream     file(DEFAULT_SCENE_PATH.c_str());
    if (file.good())
    {
      std::cout << "Attempting to start with: " << DEFAULT_SCENE_PATH << std::endl;
      updateArg(SCENE, DEFAULT_SCENE_PATH);
    }
    else
    {
      throw std::runtime_error("No scene specified and demo scene is not found. Aborting. Try -scene your_scene.msf ");
      std::exit(-1);
    }
    //#endif
  }

  // CHECK if there is a camera file provided on the command line
  // if not we will try to use the scene filename as guess
  if (command_line.hasOptionAndValue("camera"))
    updateArg(CAMERA, command_line.option("camera").parameter(0));
  else if (command_line.hasOptionAndValue("cam"))
    updateArg(CAMERA, command_line.option("cam").parameter(0));
  else if (_has_argument[SCENE])
  {
    std::string camera_file;
    StringParsing::getFileNameWithoutExtension(_value[SCENE][0], camera_file);
    camera_file += ".mcf";
    updateArg(CAMERA, camera_file);
  }

  if (command_line.hasOptionAndValue("startingcam"))
  {
    updateArg(FIRST_CAMERA, command_line.option("startingcam").parameter(0));
  }
  else   // no Starting Camera so we set it to Zero
  {
    updateArg(FIRST_CAMERA, std::to_string(0));
  }

  if (command_line.hasOptionAndValue("numcam"))
  {
    std::cout << "[WARNING] -numcam option has changed meaning. IT SPECIFIES THE NUMBER OF VIEW POINT TO RENDER "
              << std::endl;
    updateArg(NUM_CAMERAS, command_line.option("numcam").parameter(0));
  }

  if (_has_argument[SCENE])
  {
    // The current camera
    mrf::rendering::Camera *                                  camera;
    std::unordered_map<std::string, mrf::rendering::Camera *> cameras;
    //RP: We don't CHECK for EXCEPTIONS WEIRD !!!!
    mrf::io::CameraParser::loadCameras(_value[CAMERA][0], cameras);

    if (command_line.hasOptionAndValue("numcam"))
    {
      std::cout << "[WARNING] -numcam option has changed meaning. IT SPECIFIES THE NUMBER OF VIEW POINT TO RENDER "
                << std::endl;
      updateArg(NUM_CAMERAS, command_line.option("numcam").parameter(0));
    }

    // We update the current camera
    camera = cameras.begin()->second;

    // IF startingcam was provided we are updating the current camera according
    // to the requested value
    if (command_line.hasOptionAndValue("startingcam"))
    {
      int start_cam = 0;
      for (auto &cam : cameras)
      {
        if (start_cam != std::stoi(_value[FIRST_CAMERA][0]))
        {
          camera = cam.second;
          ++start_cam;
        }
        else
        {
          break;
        }
      }
    }   // end if an index was specified for the starting camera

    //if (cameras.size() != 0)
    //{
    //  updateArg(HEIGHT, std::to_string(camera->sensor().resolution().y()));
    //  updateArg(WIDTH, std::to_string(camera->sensor().resolution().x()));
    //  updateArg(SAMPLES, std::to_string(camera->sensorSampler().spp()));
    //}
  }   // end of hasArgument[SCENE]

  if (command_line.hasOptionAndValue("height"))   //Override sensor height if specified
  {
    updateArg(HEIGHT, command_line.option("height").parameter(0));
    //updateArg(OVERRIDE_HEIGHT, std::to_string(0));
  }
  //else if (!_has_argument[HEIGHT])
  //{
  //  updateArg(HEIGHT, std::to_string(512));
  //}

  if (command_line.hasOptionAndValue("width"))   //Override sensor width if specified
  {
    updateArg(WIDTH, command_line.option("width").parameter(0));
    //updateArg(OVERRIDE_WIDTH, std::to_string(0));
  }
  //else if (!_has_argument[WIDTH])
  //{
  //  updateArg(WIDTH, std::to_string(512));
  //}

  if (command_line.hasOptionAndValue("samples"))   //Override sensor sampler if specified
  {
    updateArg(SAMPLES, command_line.option("samples").parameter(0));
    //updateArg(OVERRIDE_SAMPLES, std::to_string(0));
  }
  //else if (!_has_argument[SAMPLES])
  //{
  //  updateArg(SAMPLES, "100");
  //}

  if (command_line.hasOptionAndValue("samples_per_frame"))
    updateArg(SPF, command_line.option("samples_per_frame").parameter(0));
  else if (command_line.hasOptionAndValue("spf"))
    updateArg(SPF, command_line.option("spf").parameter(0));
  else
    updateArg(SPF, "1");

  if (command_line.hasOptionAndValue("max_time")) updateArg(MAX_TIME, command_line.option("max_time").parameter(0));

  if (command_line.hasOptionAndValue("debug_pixels"))
    updateArg(DEBUG_PIXELS, command_line.option("logfile").parameter(0));

  if (command_line.hasOptionAndValue("next_event"))
    updateArg(NEXT_EVENT, command_line.option("next_event").parameter(0));
  else
    updateArg(NEXT_EVENT, "one");

  if (command_line.hasOptionAndValue("envmap_is")) updateArg(ENVMAP_IS, command_line.option("envmap_is").parameter(0));

  if (command_line.hasOptionAndValue("max_path_length"))
    updateArg(MPL, command_line.option("max_path_length").parameter(0));
  else if (command_line.hasOptionAndValue("mpl"))
    updateArg(MPL, command_line.option("mpl").parameter(0));
  else
    updateArg(MPL, std::to_string(10));

  if (command_line.hasOptionAndValue("rng"))
    updateArg(RNG_METHOD, command_line.option("rng").parameter(0));
  else
    updateArg(RNG_METHOD, "halton");

  if (command_line.hasOptionAndValue("rng_seed"))
    updateArg(RNG_SEED, command_line.option("rng_seed").parameter(0));
  else
    updateArg(RNG_SEED, std::to_string(0));

  if (command_line.hasOptionAndValue("wavelengths"))
  {
    std::vector<std::string> wavelengths;
    for (mrf::uint i = 0; i < command_line.option("wavelengths").nbParameter(); i++)
    {
      wavelengths.push_back(command_line.option("wavelengths").parameter(i));
    }

    std::sort(wavelengths.begin(), wavelengths.end());

    updateArg(WAVELENGTH, wavelengths);
  }
  else
  {
    if (command_line.hasOptionAndValue("wavelength_range"))
    {
      if (!_has_argument[WAVELENGTH])
        updateArg(WAVELENGTH, wavelengthRange(command_line.option("wavelength_range").parameter(0)));
    }
    else if (command_line.hasOptionAndValue("wr"))
    {
      if (!_has_argument[WAVELENGTH]) updateArg(WAVELENGTH, wavelengthRange(command_line.option("wr").parameter(0)));
    }
    else
    {
      updateArg(WAVELENGTH, wavelengthRange("400:680:40"));
    }
  }

  if (command_line.hasOptionAndValue("wavelengths_per_pass"))
    updateArg(WPP, command_line.option("wavelengths_per_pass").parameter(0));
  else if (command_line.hasOptionAndValue("wpp"))
    updateArg(WPP, command_line.option("wpp").parameter(0));
  else if (!command_line.hasOptionAndValue("wr") && !command_line.hasOptionAndValue("wavelength_range"))
    updateArg(WPP, std::to_string(8));   // forcing to wpp = 8 when no wavelength range is speciied
  else
    updateArg(WPP, std::to_string(1));

  if (command_line.hasOption("continuous_sampling"))
  {
    updateArg(CONTINUOUS_SAMPLING, std::to_string(0));
  }

  if (command_line.hasOption("save_temp"))
  {
    updateArg(TEMP, std::to_string(0));
  }

  if (command_line.hasOptionAndValue("o"))
  {
    std::vector<std::string> output;
    for (unsigned int i = 0; i < command_line.option("o").nbParameter(); ++i)
      output.push_back(command_line.option("o").parameter(0));
    updateArg(OUTPUT, output);
  }
  else if (command_line.hasOptionAndValue("out"))
  {
    std::vector<std::string> output;
    for (unsigned int i = 0; i < command_line.option("out").nbParameter(); ++i)
      output.push_back(command_line.option("out").parameter(0));
    updateArg(OUTPUT, output);
  }
  else if (_has_argument[SCENE])
  {
    //Rp: BY default we output a simple name with hdr extension for Malia and .exr for Malia_RGB

    std::string output_file;
    StringParsing::getFileNameWithoutExtension(_value[SCENE][0], output_file);
//if (_has_argument[INTERACTIVE])
#ifdef MRF_RENDERING_MODE_SPECTRAL
    output_file += ".hdr";
#else
    output_file += ".exr";
#endif

    //else
    //output_file += "_" + _value[SAMPLES][0] + "spp.exr";
    updateArg(OUTPUT, output_file);
  }

  if (command_line.hasOptionAndValue("cudafile")) updateArg(CUDA_FILE, command_line.option("cudafile").parameter(0));

  if (command_line.hasOption("nogui"))
  {
    updateArg(INTERACTIVE, std::to_string(0));
    updateArg(NO_GUI, std::to_string(0));
  }

  if (command_line.hasOption("env_rot"))
  {
    if (command_line.option("env_rot").nbParameter() > 2 || command_line.option("env_rot").nbParameter() < 1)
    {
      throw std::runtime_error(
          " The number of arguments for the option -env_rot is wrong. Either 1 or 2 arguments are expected");
    }

    std::vector<std::string> angles;
    for (unsigned int i = 0; i < command_line.option("env_rot").nbParameter(); i++)
    {
      angles.push_back(command_line.option("env_rot").parameter(i));
    }

    updateArg(ENV_ROT, angles);

  }   //end of option env_rot

  if (command_line.hasOption("log_variance"))
  {
    updateArg(LOG_VARIANCE, std::to_string(0));
  }

  if (command_line.hasOption("runtime_ptx"))
  {
    updateArg(RUNTIME_PTX_COMPIL, std::to_string(0));
  }

  if (command_line.hasOption("overwrite_ptx"))
  {
    updateArg(OVERWRITE_PTX, std::to_string(0));
  }
}

bool ArgsParsing::hasArgument(ARG_LIST arg)
{
  return _has_argument[arg];
}

std::string ArgsParsing::getSingleValue(ARG_LIST arg, int i)
{
  return _value[arg][i];
}

std::vector<std::string> ArgsParsing::getMultipleValue(ARG_LIST arg)
{
  return _value[arg];
}

void ArgsParsing::updateArg(ARG_LIST arg, std::string value)
{
  if (value.size() > 0)
  {
    _has_argument[arg] = true;
    if (_value[arg].size() == 1)
    {
      _value[arg][0] = value;
    }
    else
    {
      _value[arg].push_back(value);
    }
  }
}

void ArgsParsing::updateArg(ARG_LIST arg, std::vector<std::string> values)
{
  if (values.size() > 0)
  {
    _has_argument[arg] = true;
    for (size_t i = 0; i < values.size(); ++i)
      _value[arg].push_back(values[i]);
  }
}

std::vector<std::string> ArgsParsing::wavelengthRange(std::string input)
{
  std::vector<std::string> wavelengths;

  std::vector<std::string> tokens;

  StringParsing::tokenize(input, tokens, ":");

  if (tokens.size() == 3)
  {
    int step  = stoi(tokens[2]);
    int w_min = stoi(tokens[0]);
    int w_max = stoi(tokens[1]);
    if (step > 0 && w_min >= 0 && w_max >= w_min)
    {
      for (int i = w_min; i <= w_max; i += step)
      {
        wavelengths.push_back(std::to_string(i));
      }
    }
  }

  return wavelengths;
}
}   // namespace util
}   // namespace mrf