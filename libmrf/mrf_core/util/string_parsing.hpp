/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>



#include <string>
#include <sstream>
#include <vector>
#include <cctype>
#include <algorithm>
#include <string.h>
#include <cassert>

#ifdef DEBUG
#  include <iostream>
#endif

namespace mrf
{
namespace util
{
extern MRF_CORE_EXPORT char const *SPACE;
extern MRF_CORE_EXPORT char const *DOUBLE_SPACE;
extern MRF_CORE_EXPORT char const *TAB;
extern MRF_CORE_EXPORT char const *NEW_LINE_UNIX;
extern MRF_CORE_EXPORT char const *NEW_LINE_WIN;
extern MRF_CORE_EXPORT char const *NEW_LINE_MAC;
extern MRF_CORE_EXPORT char const *FULL_STOP;


extern MRF_CORE_EXPORT char const *SLASH;
extern MRF_CORE_EXPORT char const *ANTI_SLASH;
extern MRF_CORE_EXPORT char const *BACK_SLASH;   // same as previous
extern MRF_CORE_EXPORT std::string const EMPTY_STRING;

extern MRF_CORE_EXPORT std::string const CURRENT_DIRECTORY;

class MRF_CORE_EXPORT StringParsing
{
public:
  static void tokenize(std::string const &str, std::vector<std::string> &tokens, std::string const &delimiters = SPACE);

  static void tokenize(char const *str, std::vector<std::string> &tokens, std::string const &delimiters = SPACE);

  // number_of_delimiters give the number of delimiters
  // that are in str
  static void tokenize(
      std::string const &       str,
      std::vector<std::string> &tokens,
      unsigned int &            number_of_delimiters,
      std::string const &       delimiters = SPACE);

  // number_of_delimiters give the number of delimiters
  // that are in str
  static void tokenize(
      char const *              str,
      std::vector<std::string> &tokens,
      unsigned int &            number_of_delimiters,
      std::string const &       delimiters = SPACE);

  /**
   * Replaces all  strings equal to "target" with "replacement" in the given string str
   **/
  static void replaceAllOccurences(std::string &str, std::string const &target, std::string const &replacement);


  // Returns false if no extension found or empty extension
  static bool getFileExtension(char const *str, std::string &extension);

  // Returns false if no extension found or empty extension
  static bool getFileExtension(std::string const &str, std::string &extension);

  /**
   * Returns the name of a filename without its extension
   * If the filename has no extension the given filename is returned
   **/
  static bool getFileNameWithoutExtension(char const *filename, std::string &name);
  //overload
  static bool getFileNameWithoutExtension(std::string const &filename, std::string &name);

  static bool pathIsAbsolute(std::string const &path);

  static bool pathIsRelative(std::string const &path);

  /**
   * This returns the filename (with the extension) of an absolute path
   **/
  static bool fileNameFromAbsPath(char const *filename, std::string &name);
  static bool fileNameFromAbsPathWithoutExtension(char const *filename, std::string &name);
  static bool extensionFromAbsPath(char const *filename, std::string &name);

  static bool getDirectory(char const *str, std::string &directory);

  static void incrementFilename(char const *filename, std::string &newName);

  static void changeExtension(std::string &filename, char const *newExt)
  {
    filename.erase(filename.rfind('.'));
    filename += newExt;
  }


  static bool isStringEmpty(std::string const &str);

  static bool convertStringToInt(char const *token, int &value_converted);

  static bool convertStringToUInt(char const *a_string, unsigned int &value_converted);

  static bool convertStringToUInt(std::string const &a_string, unsigned int &value_converted);

  static bool convertStringToDouble(char const *token, double &value_converted);

  static bool convertStringToFloat(char const *token, float &value_converted);

  static void transformString(char const *src, std::string &dst, int (*f)(int))
  {
    unsigned int i = 0;
    do
    {
      dst[i] = static_cast<char>(f(src[i]));
      ++i;
    } while (src[i] != '\0' && i < dst.size());

    for (; src[i] != '\0'; ++i)
      dst.append(1, static_cast<char>(f(src[i])));
  }

  static void toLower(char const *src, std::string &dst) { transformString(src, dst, &tolower); }

  static void toUpper(char const *src, std::string &dst) { transformString(src, dst, &toupper); }




  template<typename T>
  static inline bool numericsToString(T i, std::string &str)
  {
    std::ostringstream s1;
    s1 << i;
    str = s1.str();
    return s1.good();
  }



  //No Check
  template<typename T>
  static inline std::string numericsToString(T i)
  {
    std::string str;

    std::ostringstream s1;
    s1 << i;
    str = s1.str();
    return str;
  }



  //NO CHECK
  template<typename T>
  static inline T convertStringToNumerics(std::string const &string)
  {
    using namespace std;
    istringstream stream1(string);

    T numerics;
    stream1 >> numerics;

    return numerics;
  }

  template<typename T>
  static inline T convertStringToNumerics(std::string const &string, bool &conversion_ok)
  {
    using namespace std;
    istringstream stream1(string);

    T numerics;
    stream1 >> numerics;

    conversion_ok = true;
    if (stream1.fail())
    {
      conversion_ok = false;
    }

    return numerics;
  }
};

}   //end of namespace util
}   //end of namespace mrf