#include <mrf_core/util/string_parsing.hpp>

#include <cerrno>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <string>

//#ifdef DEBUG
#include <iostream>
//#endif

namespace mrf
{
namespace util
{
using namespace std;

char const *SPACE         = " ";
char const *DOUBLE_SPACE  = "  ";
char const *TAB           = "\t";
char const *NEW_LINE_UNIX = "\n";
char const *NEW_LINE_WIN  = "\r\n";
char const *NEW_LINE_MAC  = "\r";
char const *FULL_STOP     = ".";

char const * SLASH             = "/";
char const * ANTI_SLASH        = "\\";
char const * BACK_SLASH        = ANTI_SLASH;
string const EMPTY_STRING      = string("");
string const CURRENT_DIRECTORY = string("./");

void StringParsing::tokenize(std::string const &str, std::vector<std::string> &tokens, std::string const &delimiters)
{
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos)
  {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
}

void StringParsing::tokenize(char const *str, std::vector<std::string> &tokens, std::string const &delimiters)
{
  std::string a_string(str);
  tokenize(a_string, tokens, delimiters);
}


void StringParsing::tokenize(
    std::string const &       str,
    std::vector<std::string> &tokens,
    unsigned int &            number_of_delimiters,
    std::string const &       delimiters)
{
  number_of_delimiters = 0;

  // Count the number of delimiters
  std::string::size_type index = str.find_first_of(delimiters, 0);

  while (index != str.npos)
  {
    number_of_delimiters++;
    index = str.find_first_of(delimiters, index + 1);
  }

  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);

  // Find first "non-delimiter".
  std::string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos)
  {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);

    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
}


void StringParsing::tokenize(
    char const *              str,
    std::vector<std::string> &tokens,
    unsigned int &            number_of_delimiters,
    std::string const &       delimiters)
{
  std::string a_string(str);
  tokenize(a_string, tokens, number_of_delimiters, delimiters);
}


void StringParsing::replaceAllOccurences(std::string &str, std::string const &target, std::string const &replacement)
{
#ifdef MRF_UTIL_STRINGPARSING_DEBUG
  std::cout << " Initial String  " << str << std::endl;
#endif

  string::size_type pos = str.find(target);

  while (pos != string::npos)
  {
    str.replace(pos, replacement.size(), replacement);
    pos = str.find(target, pos + 1);
  }


#ifdef MRF_UTIL_STRINGPARSING_DEBUG
  std::cout << " String is now " << str << std::endl;
#endif
}


bool StringParsing::getFileExtension(char const *filename, std::string &extension)
{
  std::vector<std::string> filename_tokens;
  tokenize(filename, filename_tokens, FULL_STOP);

  if (filename_tokens.size() > 1)
  {
    extension = filename_tokens.back();
    return true;
  }
  extension = "";
  return false;
}


bool StringParsing::getFileExtension(std::string const &filename, std::string &extension)
{
  return StringParsing::getFileExtension(filename.c_str(), extension);
}


bool StringParsing::getFileNameWithoutExtension(string const &filename, std::string &name)
{
  std::string::size_type pos = filename.find_last_of(FULL_STOP);

  if (std::string::npos != pos)
  {
    name = filename.substr(0, pos);
    return true;
  }
  return false;
}


bool StringParsing::pathIsAbsolute(std::string const &path)
{
  if (path.length() > 0)
  {
    if (path[0] == '/') return true;
  }
#ifdef WIN32
  if (path.length() > 1)
  {
    if ((path[0] >= 'a' && path[0] <= 'z' && path[1] == ':') || (path[0] >= 'A' && path[0] <= 'Z' && path[1] == ':'))
      return true;
  }
#endif
  return false;
}


bool StringParsing::pathIsRelative(std::string const &path)
{
  return !pathIsAbsolute(path);
}


bool StringParsing::getFileNameWithoutExtension(char const *filename, std::string &name)
{
  return getFileNameWithoutExtension(string(filename), name);
}


bool StringParsing::fileNameFromAbsPath(char const *filename, std::string &name)
{
  std::string       path = filename;
  string::size_type pos  = path.find_last_of("\\/");

  if (pos == std::string::npos)
  {
    name = path;
    return true;
  }


  name = path.substr(pos + 1);
  return true;
}


bool StringParsing::fileNameFromAbsPathWithoutExtension(char const *filename, std::string &name)
{
  return (fileNameFromAbsPath(filename, name) && getFileNameWithoutExtension(name.c_str(), name));
}


bool StringParsing::extensionFromAbsPath(char const *filename, std::string &name)
{
  return (fileNameFromAbsPath(filename, name) && getFileExtension(name.c_str(), name));
}


bool StringParsing::getDirectory(char const *fullpath, std::string &directory)
{
  std::string       path = fullpath;
  string::size_type pos  = path.find_last_of("\\/");

  if (pos == std::string::npos)
  {
    directory = CURRENT_DIRECTORY;
    return true;
  }

  directory = path.substr(0, pos) + "/";
  return true;
}


bool StringParsing::isStringEmpty(std::string const &str)
{
  if (str.size() == 0)
  {
    return true;
  }
  else if (mrf::util::SPACE == str)
  {
    return true;
  }
  else if (mrf::util::TAB == str)
  {
    return true;
  }
  else if (mrf::util::NEW_LINE_UNIX == str)
  {
    return true;
  }
  else if (mrf::util::NEW_LINE_MAC == str)
  {
    return true;
  }
  else if (mrf::util::NEW_LINE_WIN == str)
  {
    return true;
  }
  else
  {
    return false;
  }
}


bool StringParsing::convertStringToInt(char const *token, int &value_converted)
{
  errno        = 0;
  char *endptr = nullptr;

  long int tmp   = strtol(token, &endptr, 10);
  int      errsv = errno;

  if (((errsv == ERANGE) && ((tmp == LONG_MIN) || (tmp == LONG_MAX))) || (errsv != 0 && tmp == 0))
  {
    return false;
  }

  value_converted = static_cast<int>(tmp);

  if ((endptr != nullptr) && (string(endptr).size() > 0))
  {
    return false;
  }


  return true;
}


bool StringParsing::convertStringToUInt(char const *a_string, unsigned int &value_converted)
{
  errno           = 0;
  char *   endptr = nullptr;
  long int tmp    = strtol(a_string, &endptr, 10);

  if ((errno == ERANGE && (tmp == LONG_MAX || tmp == LONG_MIN)) || (errno != 0 && tmp == 0))
  {
    return false;
  }

  value_converted = static_cast<unsigned int>(tmp);

  if ((endptr != nullptr) && (string(endptr).size() > 0))
  {
    return false;
  }


  return true;
}


bool StringParsing::convertStringToUInt(std::string const &a_string, unsigned int &value_converted)
{
  return convertStringToUInt(a_string.c_str(), value_converted);
}


bool StringParsing::convertStringToDouble(char const *token, double &value_converted)
{
  errno        = 0;
  char *endptr = nullptr;

  value_converted = strtod(token, &endptr);

  int errsv = errno;
  if ((errsv == ERANGE) || (value_converted == HUGE_VALL) || (value_converted == HUGE_VALF))
  {
    return false;
  }

  if ((endptr != nullptr) && (string(endptr).size() > 0))
  {
    return false;
  }


  return true;
}


bool StringParsing::convertStringToFloat(char const *token, float &value_converted)
{
  //Define in the C header
  errno        = 0;
  char *endptr = nullptr;

  value_converted = strtof(token, &endptr);

  int errsv = errno;
  if ((errsv == ERANGE) || (value_converted == HUGE_VALL) || (value_converted == HUGE_VALF))
  {
    return false;
  }

  if ((endptr != nullptr) && (string(endptr).size() > 0))
  {
    return false;
  }

  return true;
}


void StringParsing::incrementFilename(char const *filename, std::string &newName)
{
  newName        = filename;
  unsigned int i = static_cast<uint>(newName.rfind('.') - 1);   //dir.size () + name.size () - 1;

  while (newName[i] == '9')
  {
    newName[i] = '0';
    if (i == 0) break;
    i--;
  }

  if (newName[i] >= '0' && newName[i] < '9')
    newName[i]++;
  else
    newName.insert(i + 1, "1");
}

}   // namespace util
}   // namespace mrf
