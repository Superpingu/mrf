/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>

#include <mrf_core/util/command_line_opt.hpp>

#include <stdexcept>
#include <iostream>
#include <string>
#include <vector>
#include <map>

namespace mrf
{
namespace util
{
/**
 * @brief      This class represents a parsed version of command-line of programmer
 *             Each key of the  map _cmdLineMap represents the option_name while the
 *             value of type CommandLineOpt represents the parsed parameters of the option
 *
 * @see         CommandLineOpt
 * @note        CommandLine and CommandLineOpt are only string representations of the command-line
 *              and do not provide complex validations methods (@see class ProgOptions for that)
 *
 */
class MRF_CORE_EXPORT CommandLine
{
private:
  // Stores the option token "-option_name" or "--option_nane" associated with the CommandLineOpt
  std::map<std::string, CommandLineOpt> _cmdLineMap;

  // these are the arguments on the command line with any -option_name
  std::vector<std::string> _directArguments;

public:
  inline CommandLine(int argc, char **argv)
  {
    using namespace std;

    for (unsigned int i = 1; i < static_cast<unsigned int>(argc);)
    {
      string current_arg(argv[i]);

      unsigned int first_letter_option_name_index;

      if (isAnOption(current_arg, first_letter_option_name_index))
      {
        std::string const option_token = current_arg;
        std::string const option_name  = current_arg.substr(first_letter_option_name_index, current_arg.size() - 1);

        auto it = _cmdLineMap.find(option_name);
        if (it == _cmdLineMap.end())   // option_name does not exist we will store it
        {
          //_cmdLineMap[option_name];
          //CommandLineOpt a_cmdline_opt(option_name);
          _cmdLineMap[option_name] = CommandLineOpt(option_name);

          unsigned int nb_arg_opt = 0;
          for (unsigned int k = (i + 1); k < static_cast<unsigned int>(argc); k++)
          {
            string current_str(argv[k]);
            if (current_str[0] != '-')
            {
              _cmdLineMap[option_name].addParameter(current_str);
              nb_arg_opt++;
            }
            else
            {
              break;
            }
          }
          i += nb_arg_opt + 1;
          continue;
        }
        else
        {
          std::cerr << " Warning option on the command line already defined " << std::endl;
          ++i;
        }
      }
      else
      {
        _directArguments.push_back(argv[i]);
        i++;
      }
    }
  }

  inline bool hasOption(char const *option_name) const
  {
    auto it = _cmdLineMap.find(std::string(option_name));
    return !(it == _cmdLineMap.end());
  }

  inline bool hasOption(std::string const &option_name) const
  {
    auto it = _cmdLineMap.find(option_name);
    return !(it == _cmdLineMap.end());
  }

  inline bool hasOptionAndValue(char const *option_name) const
  {
    auto it = _cmdLineMap.find(std::string(option_name));

    return (!(it == _cmdLineMap.end()) && it->second.nbParameter() > 0);
  }

  inline bool hasOptionAndValue(std::string const &option_name) const
  {
    auto it = _cmdLineMap.find(option_name);
    return (!(it == _cmdLineMap.end()) && it->second.nbParameter() > 0);
  }

  inline std::map<std::string, CommandLineOpt> const &options() const { return _cmdLineMap; }


  inline CommandLineOpt const &option(std::string const &option_name) const
  {
    auto it = _cmdLineMap.find(option_name);
    if (it != _cmdLineMap.end())
    {
      return it->second;
    }

    throw std::logic_error("Option with name" + option_name + " not found");
  }

  inline std::string const &argument(unsigned int index) const { return _directArguments[index]; }

  inline unsigned int nbArguments() const { return static_cast<unsigned int>(_directArguments.size()); }


private:
  bool isAnOption(std::string const &an_argument, unsigned int &index_first_option_name) const
  {
    if (an_argument[0] == '-')
    {
      if (an_argument[1] == '-')
      {
        index_first_option_name = 2;
      }
      else
      {
        index_first_option_name = 1;
      }
      return true;
    }
    return false;
  }
};


//std::ostream& operator<<( std::ostream & os, CommandLine const & cmd )
//{
//  using namespace std;
//
//  os << "CommandLine: { Direct Arguments: " ;
//
//  for( unsigned int i=0; i < cmd.nbArguments(); i++)
//  {
//    os << cmd.argument( i ) << " " ;
//  }
//  os << std::endl << " Options: { " << std::endl;
//
//  for(auto it= cmd.options().begin(); it != cmd.options().end(); ++it)
//  {
//    os << it->first << "  --> " << it->second << std::endl;
//  }
//
//  os << " } " ;
//
//  os << " }" << std::endl;
//  return os;
//}



}   // namespace util
}   // namespace mrf
