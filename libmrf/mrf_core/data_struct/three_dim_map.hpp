/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 *
 **/

#pragma once

#include <mrf_core/data_struct/two_dim_map.hpp>

#include <mrf_core/math/math.hpp>

#include <iostream>
#include <map>
#include <utility>

namespace mrf
{
namespace util
{
template<typename T, class DATA_TYPE>
class _ThreeDimMap
{
protected:
  std::map<T, _TwoDimMap<T, DATA_TYPE>> _map;

public:
  inline unsigned int size() const;

  inline typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator begin() const;
  inline typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator end() const;

  inline std::map<T, _TwoDimMap<T, DATA_TYPE>> const &map() const;
  inline std::map<T, _TwoDimMap<T, DATA_TYPE>> &      map();


  /**
   * Insert an element into the map
   **/
  inline bool insert(T key1, T key2, T key3, DATA_TYPE const &data);

  /**
   * Returns true if key1 exists in the map
   **/
  inline bool      findkey(T key1) const;
  inline DATA_TYPE linear(T t_key1, T t_key2, T t_key3) const;


  inline DATA_TYPE infInfLinear(T t_key1, T t_key2, T t_key3) const;
  inline DATA_TYPE infInfNearest(T t_key1, T t_key2, T t_key3) const;




  /**
   * Returns the nearest element
   **/
  DATA_TYPE const &operator()(T key1, T key2, T t_key3) const;


  inline void keys(std::vector<T> &first_keys, std::vector<T> &second_keys, std::vector<T> &third_keys) const;

  inline void displayKeys(bool verbose) const;
};


template<typename T, class DATA_TYPE>
inline unsigned int _ThreeDimMap<T, DATA_TYPE>::size() const
{
  unsigned int size = 0;

  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator it;

  for (it = begin(); it != end(); ++it)
  {
    size += it->second.size();
  }

  return size;
}



template<typename T, class DATA_TYPE>
inline typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator _ThreeDimMap<T, DATA_TYPE>::begin() const
{
  return _map.begin();
}


template<typename T, class DATA_TYPE>
inline typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator _ThreeDimMap<T, DATA_TYPE>::end() const
{
  return _map.end();
}


template<typename T, class DATA_TYPE>
inline std::map<T, _TwoDimMap<T, DATA_TYPE>> const &_ThreeDimMap<T, DATA_TYPE>::map() const
{
  return _map;
}


template<typename T, class DATA_TYPE>
inline std::map<T, _TwoDimMap<T, DATA_TYPE>> &_ThreeDimMap<T, DATA_TYPE>::map()
{
  return _map;
}




template<typename T, class DATA_TYPE>
inline bool _ThreeDimMap<T, DATA_TYPE>::insert(T key1, T key2, T key3, DATA_TYPE const &data)
{
  if (findkey(key1))
  {
    //std::cout << " ThreeDim Map. A Key is already there " << std::endl;
    return _map[key1].insert(key2, key3, data);
  }
  else
  {
    _TwoDimMap<T, DATA_TYPE> a_map;
    a_map.insert(key2, key3, data);
    std::pair<typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::iterator, bool> inserted;
    inserted = _map.insert(std::make_pair(key1, a_map));
    return inserted.second;
  }
}


template<typename T, class DATA_TYPE>
inline bool _ThreeDimMap<T, DATA_TYPE>::findkey(T key1) const
{
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator a_cit;
  a_cit = _map.find(key1);
  return a_cit != _map.end();
}




template<typename T, class DATA_TYPE>
DATA_TYPE const &_ThreeDimMap<T, DATA_TYPE>::operator()(T key1, T key2, T key3) const
{
  using namespace mrf::math;

  typedef typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator IT_TYPE;
  IT_TYPE                                                                upper_it = _map.lower_bound(key1);
  IT_TYPE                                                                lower_it = upper_it;
  lower_it--;

  IT_TYPE n_it;   //nearest iterator

  if (upper_it == _map.begin())   //  < min_map
  {
    n_it = upper_it;
  }
  else if (upper_it == _map.end() && lower_it != _map.end())   // > max_map
  {
    n_it = lower_it;
  }
  else   //between keys
  {
    T const diff_upper = _Math<T>::absVal(upper_it->first - key1);
    T const diff_lower = _Math<T>::absVal(lower_it->first - key1);

    if (diff_upper <= diff_lower)
    {
      n_it = upper_it;
    }
    else
    {
      n_it = lower_it;
    }
  }

  return n_it->second.nearestValue(key2, key3);
}



template<typename T, class DATA_TYPE>
inline DATA_TYPE _ThreeDimMap<T, DATA_TYPE>::linear(T t_key1, T t_key2, T t_key3) const
{
  using namespace mrf::math;

  // element which is >= targeted_key
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.lower_bound(t_key1);
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;

  typedef typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator CONST_MAP_IT;


  if (upper_it == _map.begin())   //  < min_map
  {
    if (_map.size() >= 2)
    {
      CONST_MAP_IT up_upper_it = upper_it;
      up_upper_it++;

      T const         x1 = upper_it->first;
      DATA_TYPE const y1 = upper_it->second.linear(t_key2, t_key3);

      T const         x2 = up_upper_it->first;
      DATA_TYPE const y2 = up_upper_it->second.linear(t_key2, t_key3);

      _Line2D<T, DATA_TYPE> l(x1, y1, x2, y2);

      return l.valueAt(t_key1);
    }
    else   //fall-back to nearest
    {
      return upper_it->second.linear(t_key2, t_key3);
    }
  }
  else
  {
    lower_it--;

    if (upper_it == _map.end() && lower_it != _map.end())   // > max_map
    {
      if (_map.size() >= 2)
      {
        CONST_MAP_IT low_lower_it = lower_it;
        low_lower_it--;

        T const         x1 = low_lower_it->first;
        DATA_TYPE const y1 = low_lower_it->second.linear(t_key2, t_key3);

        T const         x2 = lower_it->first;
        DATA_TYPE const y2 = lower_it->second.linear(t_key2, t_key3);

        _Line2D<T, DATA_TYPE> l(x1, y1, x2, y2);

        return l.valueAt(t_key1);
      }
      else   //fall-back to nearest
      {
        return lower_it->second.linear(t_key2, t_key3);
      }
    }
    else if (upper_it != _map.end() && lower_it != _map.end())   // between two keys
    {
      //Bilinear interpolation !
      DATA_TYPE d1 = upper_it->second.linear(t_key2, t_key3);
      DATA_TYPE d2 = lower_it->second.linear(t_key2, t_key3);

      T const diff_upper = _Math<T>::absVal(upper_it->first - t_key1);
      T const diff_lower = _Math<T>::absVal(lower_it->first - t_key1);

      T const il = _Math<T>::absVal(upper_it->first - lower_it->first);

      T const alpha = T(1.0) - (diff_upper / il);
      T const beta  = T(1.0) - (diff_lower / il);

      return d1 * alpha + d2 * beta;
    }
    else   //Map empty???
    {
      assert(0);
    }
  }

}   //end of linear(...) method

template<typename T, class DATA_TYPE>
inline DATA_TYPE _ThreeDimMap<T, DATA_TYPE>::infInfLinear(T t_key1, T t_key2, T t_key3) const
{
  using namespace mrf::math;

  // element which is >= targeted_key
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.upper_bound(t_key1);
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;

  if (lower_it == _map.end())
  {
    std::cout << "  upper_it = " << upper_it->first << std::endl;
    std::cout << "  _map.size() = " << _map.size() << std::endl;
    std::cout << "  t_key1 = " << t_key1 << "  t_key2 = " << t_key2 << std::endl;

    assert(0);
  }

  return lower_it->second.infLinear(t_key2, t_key3);
}


template<typename T, class DATA_TYPE>
inline DATA_TYPE _ThreeDimMap<T, DATA_TYPE>::infInfNearest(T t_key1, T t_key2, T t_key3) const
{
  using namespace mrf::math;

  // element which is >= targeted_key
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator upper_it = _map.upper_bound(t_key1);
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator lower_it = upper_it;
  lower_it--;

  if (lower_it == _map.end())
  {
    assert(0);
  }

  return lower_it->second.infNearest(t_key2, t_key3);
}

template<typename T, class DATA_TYPE>
inline void
_ThreeDimMap<T, DATA_TYPE>::keys(std::vector<T> &first_keys, std::vector<T> &second_keys, std::vector<T> &third_keys)
    const
{
  first_keys.clear();
  second_keys.clear();
  third_keys.clear();

  for (auto it = _map.begin(); it != _map.end(); ++it)
  {
    first_keys.push_back(it->first);

    for (auto second_it = it->second.begin(); second_it != it->second.end(); ++second_it)
    {
      second_keys.push_back(second_it->first);

      for (auto third_it = second_it->second.begin(); third_it != second_it->second.end(); ++third_it)
      {
        third_keys.push_back(third_it->first);
      }
    }
  }
}

template<typename T, class DATA_TYPE>
inline void _ThreeDimMap<T, DATA_TYPE>::displayKeys(bool verbose) const
{
  std::vector<T> first_keys;
  std::vector<T> second_keys;
  std::vector<T> third_keys;

  keys(first_keys, second_keys, third_keys);

  std::cout << " First Dim: " << first_keys.size() << " Keys "
            << " Ranging from " << first_keys[0] << " To " << first_keys[first_keys.size() - 1] << std::endl;

  std::cout << " Second Dim: " << second_keys.size() << " Keys "
            << " Ranging from "
            << " To " << std::endl;

  std::cout << " Third Dim: " << third_keys.size() << " Keys "
            << " Ranging from "
            << " To " << std::endl;


  if (verbose)
  {
    std::cout << "[ ";
    for (unsigned int i = 0; i < first_keys.size(); i++)
    {
      std::cout << first_keys[i] << " ;";
    }

    std::cout << " ] " << std::endl;
  }
}


//-------------------------------------------------------------------------
// Extern Operator
//-------------------------------------------------------------------------
template<typename T, class DATA_TYPE>
inline std::ostream &operator<<(std::ostream &os, _ThreeDimMap<T, DATA_TYPE> const &three_dim_map)
{
  typename std::map<T, _TwoDimMap<T, DATA_TYPE>>::const_iterator it;

  os << " [ ";

  for (it = three_dim_map.begin(); it != three_dim_map.end(); ++it)
  {
    os << " (" << it->first << " , " << it->second << ") " << std::endl;
  }

  os << " ] " << std::endl;

  return os;
}


}   // namespace util

}   // namespace mrf
