/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2018
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <cassert>
#include <vector>
#include <iostream>

namespace mrf
{
namespace data_struct
{
#ifdef MRF_WITH_EIGEN_SUPPORT
#  include "eigen_array1d.hpp"
#else
#  include "mrf_array1d.hpp"
#endif

//Include templates definition
#include "data_struct/array.cxx"

typedef _Array<float>        Array;
typedef _Array<double>       ArrayD;
typedef _Array<unsigned int> ArrayUI;


}   // namespace data_struct
}   //end of namespace mrf
