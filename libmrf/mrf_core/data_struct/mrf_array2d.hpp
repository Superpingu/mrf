/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/

#pragma once

#include <cassert>
#include <memory>
#include <iostream>
#include <cstddef>

namespace mrf
{
namespace data_struct
{
/**
 * A simple class that wraps a two dimensional array into a linear one.
 * This is a static array which size can not be modified after construction
 *
 * The array is stored in row major order.
 *
 **/
template<class T>
class _Array2D
{
private:
  size_t               _width;
  size_t               _height;
  std::unique_ptr<T[]> _data;

public:
  //-------------------------------------------------------------------------
  // Constructors
  //-------------------------------------------------------------------------
  _Array2D(size_t array_width = 0, size_t array_height = 0);
  _Array2D(_Array2D<T> const &an_array);
  _Array2D(_Array2D<T> &&an_array);
  ~_Array2D();

  /**
   * Resizing the Array2D
   * This erases all data (in accordance to Eigen Implementation) NONE of the DATA are initialized to a value
   * You can or must call setConstant(...) in order to do so.
   *
   * Note that the first parameter is the height (Number of rows) and the second one is the width (Number of columns)
   *
   */
  inline void resize(size_t nb_rows, size_t nb_columns);


  //-------------------------------------------------------------------------
  // Accessors
  //-------------------------------------------------------------------------
  size_t width() const;
  size_t height() const;
  size_t size() const;

  // Added Operators cols() and rows() to be compatible with Eigen

  /**
   * Returns the width() of the Array
   */
  size_t cols() const;

  /**
   * Returns the Height() of the Array
   */
  size_t rows() const;


  T &      element(size_t i_row_index, size_t j_column_index);
  T const &element(size_t i_row_index, size_t j_column_index) const;

  inline T *      data();
  inline T const *data() const;

  //-------------------------------------------------------------------------
  // DownSample the Array2D to the given Array2D by applying a scaling
  // factor for each dimension
  //-------------------------------------------------------------------------
  inline void downSample(unsigned int factor, _Array2D &an_array) const;

  //-------------------------------------------------------------------------
  // Operators
  //-------------------------------------------------------------------------
  _Array2D &operator=(_Array2D const &an_array);
  _Array2D &operator=(_Array2D &&an_array);

  _Array2D &operator*=(T scalar);
  _Array2D  operator*(T scalar) const;

  _Array2D &operator/=(T scalar);
  _Array2D  operator/(T scalar) const;
  _Array2D  operator/(_Array2D const &an_array) const;

  _Array2D &operator+=(_Array2D const &an_array);
  _Array2D  operator+(_Array2D const &an_array) const;
  _Array2D &operator-=(_Array2D const &an_array);
  _Array2D  operator-(_Array2D const &an_array) const;

  //Per element operations.
  // Each element is affected by the function
  inline _Array2D &square();
  inline _Array2D &sqrt();
  inline _Array2D &pow(T exponent);
  inline _Array2D &abs();

  //set zero to all element of the array
  inline _Array2D &setZero(size_t height, size_t width);
  inline _Array2D &setZero();
  inline _Array2D &setConstant(T const &a_constant_value);
  inline _Array2D &setOnes();

  // Sum all elements of the array
  inline T sum() const;

  T &      operator[](size_t pos);
  const T &operator[](size_t pos) const;

  T &      operator()(size_t pos);
  T const &operator()(size_t pos) const;

  T &      operator()(size_t row, size_t col);
  T const &operator()(size_t row, size_t col) const;
};




//-------------------------------------------------------------------------
// Implementation
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
template<class T>
_Array2D<T>::_Array2D(size_t array_width, size_t array_height)
  : _width(array_width)
  , _height(array_height)
  , _data(std::make_unique<T[]>(array_width * array_height))
{
  setZero();
}

//-------------------------------------------------------------------------
template<class T>
_Array2D<T>::_Array2D(_Array2D<T> const &an_array)
  : _width(an_array.width())
  , _height(an_array.height())
  , _data(std::make_unique<T[]>(_width * _height))
{
  // Now we call the assignment operator=
  *this = an_array;
}

//-------------------------------------------------------------------------
template<class T>
_Array2D<T>::_Array2D(_Array2D<T> &&an_array): _width(an_array._width)
                                             , _height(an_array._height)
{
  _data = std::move(an_array._data);
}

//-------------------------------------------------------------------------
template<class T>
_Array2D<T>::~_Array2D()
{
  // Unique pointer on Data will automatically delete it
}

//-------------------------------------------------------------------------

template<class T>
inline void _Array2D<T>::resize(size_t nb_rows, size_t nb_columns)
{
  if ((nb_rows == _height) && (nb_columns == _width))
  {
    return;
  }

  _data   = std::make_unique<T[]>(nb_rows * nb_columns);
  _width  = nb_columns;
  _height = nb_rows;
}


//-------------------------------------------------------------------------
template<class T>
inline size_t _Array2D<T>::width() const
{
  return _width;
}

//-------------------------------------------------------------------------
template<class T>
inline size_t _Array2D<T>::height() const
{
  return _height;
}

//-------------------------------------------------------------------------
template<class T>
inline size_t _Array2D<T>::size() const
{
  return width() * height();
}

//-------------------------------------------------------------------------
template<class T>
inline size_t _Array2D<T>::cols() const
{
  return width();
}

//-------------------------------------------------------------------------
template<class T>
inline size_t _Array2D<T>::rows() const
{
  return height();
}


//-------------------------------------------------------------------------
template<class T>
inline T const &_Array2D<T>::element(size_t i_row_index, size_t j_column_index) const
{
  assert(i_row_index < _height);
  assert(j_column_index < _width);

  return _data[i_row_index * _width + j_column_index];
}

//-------------------------------------------------------------------------
template<class T>
inline T &_Array2D<T>::element(size_t i_row_index, size_t j_column_index)
{
  assert(i_row_index < _height);
  //#ifdef DEBUG
  //             std::cout << "  j_column_index = " << j_column_index << std::endl ;
  //#endif

  assert(j_column_index < _width);

  return _data[i_row_index * _width + j_column_index];
}

//-------------------------------------------------------------------------
template<class T>
inline T *_Array2D<T>::data()
{
  return _data.get();
}


template<class T>
inline T const *_Array2D<T>::data() const
{
  return _data.get();
}


//-------------------------------------------------------------------------
template<class T>
inline void _Array2D<T>::downSample(unsigned int factor, _Array2D<T> &an_array) const
{
  assert(an_array.width() * factor == _width);
  assert(an_array.height() * factor == _height);

  double const sqr_factor = 1.0 / static_cast<double>(factor * factor);

  for (size_t i = 0; i < an_array.height(); i++)
  {
    for (size_t j = 0; j < an_array.width(); j++)
    {
      unsigned int left_i  = i * factor;
      unsigned int right_i = i * factor + factor;

      unsigned int left_j  = j * factor;
      unsigned int right_j = j * factor + factor;

      T box_value;
      for (unsigned int k = left_i; k < right_i; k++)
      {
        for (unsigned int l = left_j; l < right_j; l++)
        {
          box_value += (*this)(k, l);
        }
      }

      box_value *= sqr_factor;

      an_array(i, j) = box_value;
    }
  }
}




//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
template<class T>
_Array2D<T> &_Array2D<T>::operator=(_Array2D const &an_array)
{
  // Self assignement ?!
  if (this == &an_array)
  {
    return *this;
  }


  // Resize the current Array if they are not the same size
  if ((an_array.width() != _width) || (an_array.height() != _height))
  {
    _width  = an_array.width();
    _height = an_array.height();

    _data = std::make_unique<T[]>(_width * _height);
  }


  const size_t size = _width * _height;
  for (size_t i = 0; i < size; i++)
  {
    _data[i] = an_array[i];
  }

  return *this;
}

//-------------------------------------------------------------------------
template<class T>
_Array2D<T> &_Array2D<T>::operator=(_Array2D &&an_array)
{
  _width  = an_array._width;
  _height = an_array._height;
  _data   = std::move(an_array._data);
  return *this;
}


//-------------------------------------------------------------------------
template<class T>
_Array2D<T> &_Array2D<T>::operator*=(T scalar)
{
  const size_t size = _width * _height;
  for (size_t i = 0; i < size; i++)
  {
    _data[i] *= scalar;
  }
  return *this;
}

template<class T>
_Array2D<T> _Array2D<T>::operator*(T scalar) const
{
  _Array2D<T> result(_width, _height);

  const size_t size = _width * _height;
  for (size_t i = 0; i < size; i++)
  {
    result[i] = scalar * _data[i];
  }
  return result;
}

template<class T>
_Array2D<T> &_Array2D<T>::operator/=(T scalar)
{
  assert(scalar != T(0));

  const size_t size = _width * _height;
  for (size_t i = 0; i < size; i++)
  {
    _data[i] /= scalar;
  }
  return *this;
}

template<class T>
_Array2D<T> _Array2D<T>::operator/(T scalar) const
{
  assert(scalar != T(0));

  _Array2D<T> result(_width, _height);

  unsigned int const size = _width * _height;
  for (unsigned int i = 0; i < size; i++)
  {
    result[i] = scalar / _data[i];
  }
  return result;
}

template<class T>
_Array2D<T> _Array2D<T>::operator/(_Array2D const &an_array) const
{
  //Pre-Condition
  assert(an_array.size() == this->size());

  _Array2D<T> result(_width, _height);

  unsigned int const size = _width * _height;
  for (unsigned int i = 0; i < size; i++)
  {
    assert(an_array[i] != T(0.0));

    result[i] = _data[i] / an_array[i];
  }

  return result;
}

template<class T>
_Array2D<T> &_Array2D<T>::operator+=(_Array2D<T> const &an_array)
{
  assert(an_array.width() == _width);
  assert(an_array.height() == _height);

  const size_t size = _width * _height;
  for (size_t i = 0; i < size; i++)
  {
    _data[i] += an_array._data[i];
  }
  return *this;
}

template<class T>
_Array2D<T> _Array2D<T>::operator+(_Array2D<T> const &an_array) const
{
  assert(an_array.width() == _width);
  assert(an_array.height() == _height);

  _Array2D<T> result(an_array.width(), an_array.height());

  const size_t size = _width * _height;
  for (size_t i = 0; i < size; i++)
  {
    result[i] = _data[i] + an_array[i];
  }
  return result;
}

template<class T>
_Array2D<T> &_Array2D<T>::operator-=(_Array2D<T> const &an_array)
{
  assert(an_array.width() == _width);
  assert(an_array.height() == _height);

  unsigned int const size = _width * _height;
  for (unsigned int i = 0; i < size; i++)
  {
    _data[i] -= an_array._data[i];
  }
  return *this;
}

template<class T>
_Array2D<T> _Array2D<T>::operator-(_Array2D<T> const &an_array) const
{
  assert(an_array.width() == _width);
  assert(an_array.height() == _height);

  _Array2D<T> result(an_array.width(), an_array.height());

  const size_t size = _width * _height;
  for (size_t i = 0; i < size; i++)
  {
    result[i] = _data[i] - an_array[i];
  }
  return result;
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::square()
{
  const size_t size = _width * _height;

  //TODO : OpenMP
  for (size_t i = 0; i < size; i++)
  {
    _data[i] *= _data[i];
  }

  return *this;
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::sqrt()
{
  const size_t size = _width * _height;

  // TODO : OpenMP
  for (size_t i = 0; i < size; i++)
  {
    _data[i] = std::sqrt(_data[i]);
  }

  return *this;
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::pow(T exponent)
{
  const size_t size = _width * _height;

  // TODO : OpenMP
  for (size_t i = 0; i < size; i++)
  {
    _data[i] = std::pow(_data[i], exponent);
  }

  return *this;
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::abs()
{
  const size_t size = _width * _height;

  // TODO : OpenMP
  for (size_t i = 0; i < size; i++)
  {
    _data[i] = mrf::math::_Math<T>::absVal(_data[i]);
  }

  return *this;
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::setZero(size_t height, size_t width)
{
  resize(height, width);

  return setZero();
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::setZero()
{
  return setConstant(T(0.0));

  // FASTER when setting to zero
  // TODO ONLY USE THIS ON NATIVE TYPES !!!
  //std::memset(_data.get(), 0, size() * sizeof(T));
  //return *this;
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::setConstant(T const &a_constant_value)
{
  const size_t size = _width * _height;

  // TODO: OPENMP
  for (size_t i = 0; i < size; i++)
  {
    _data[i] = a_constant_value;
  }
  return *this;
}

//-------------------------------------------------------------------------
template<class T>
inline _Array2D<T> &_Array2D<T>::setOnes()
{
  return setConstant(T(1.0));
}

//-------------------------------------------------------------------------
template<class T>
inline T _Array2D<T>::sum() const
{
  const size_t size = _width * _height;
  T            sum(0.0);

  // TODO: OPENMP
  for (size_t i = 0; i < size; i++)
  {
    sum += _data[i];
  }

  return sum;
}

//-------------------------------------------------------------------------
template<class T>
inline T &_Array2D<T>::operator[](size_t pos)
{
  assert(pos < _width * _height);

  return _data[pos];
}

//-------------------------------------------------------------------------
template<class T>
inline T const &_Array2D<T>::operator[](size_t pos) const
{
  assert(pos < _width * _height);
  return _data[pos];
}

//-------------------------------------------------------------------------
template<class T>
inline T &_Array2D<T>::operator()(size_t pos)
{
  assert(pos < _width * _height);
  return _data[pos];
}


//------------------------------------------------------------------------------
template<class T>
inline const T &_Array2D<T>::operator()(size_t pos) const
{
  assert(pos < _width * _height);
  return _data[pos];
}


//-------------------------------------------------------------------------
template<class T>
inline T &_Array2D<T>::operator()(size_t row, size_t col)
{
  return element(row, col);
}


//-------------------------------------------------------------------------
template<class T>
T const &_Array2D<T>::operator()(size_t row, size_t col) const
{
  assert(row < _height);
  assert(col < _width);

  return _data[row * _width + col];
}

//-------------------------------------------------------------------------
// External Operators
//-------------------------------------------------------------------------


template<class T>
bool operator!=(_Array2D<T> const &a1, _Array2D<T> const &a2)
{
  return !(a1 == a2);
}

template<class T>
bool operator==(_Array2D<T> const &a1, _Array2D<T> const &a2)
{
  if (a1.cols() != a2.cols())
  {
    return false;
  }
  if (a1.rows() != a2.rows())
  {
    return false;
  }

  for (unsigned int l = 0; l < a1.size(); l++)
  {
    if (a1(l) != a2(l))
    {
      return false;
    }
  }

  return true;
}


template<class T>
inline std::ostream &operator<<(std::ostream &os, _Array2D<T> const &t)
{
  os.precision(20);
  os.setf(std::ios_base::fixed, std::ios_base::floatfield);
  //os.setf( std::ios_base::fmtflags(20), std::ios_base::floatfield);

  os << " Array 2D [ width =  " << t.width() << " height = " << t.height() << " data = \n [ ";


  for (size_t i = 0; i < t.height(); i++)
  {
    for (size_t j = 0; j < t.width(); j++)
    {
      os << t(i, j) << "; ";
    }

    os << std::endl;
  }

  os << " ]  ] " << std::endl;

  os.precision(6);
  os.setf(std::ios_base::fixed, std::ios_base::floatfield);

  return os;
}

}   // namespace data_struct
}   // namespace mrf
