/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/radiometry/color_to_spectrum_data.h>
#include <mrf_core/util/string_parsing.hpp>

#include <vector>
#include <limits>
#include <string>
#include <iostream>
#include <fstream>

#ifdef _WIN32
#  undef min
#  undef max
// to avoid warning due to multiple definition of NOMINMAX
#  ifndef NOMINMAX
#    define NOMINMAX
#  endif
#endif

namespace mrf
{
namespace radiometry
{
template<class T_wave, class T_val>
class Spectrum_T
{
  static constexpr T_wave DEFAULT_MIN_WAVELENGTH = T_wave(0);
  static constexpr T_wave DEFAULT_MAX_WAVELENGTH = std::numeric_limits<T_wave>::max();

public:
  Spectrum_T();
  /**
   *construct a spectrum with two wavelengths: DEFAULT_MIN_WAVELENGTH and
   *DEFAULT_MAX_WAVELENGTH with an amplitude of value
   **/
  Spectrum_T(T_val value);

  Spectrum_T(T_val r, T_val g, T_val b);

  /**
   *construct a spectrum with a vector of wavelengths
   *and a vector of values. The two vector must have the same size
   *No internal check on that is made
   **/
  Spectrum_T(std::vector<T_wave> const &wavelengths, std::vector<T_val> const &values);

  /**
   * Add a walength and its corresponding value
   * WARNING: no internal check is done neither any reordering of the spectrum
   * you must always use this function to add wavelengths and values in increasing order
   **/
  void add(T_wave const &wavelength, T_val const &value);

  /*
   * Clear all the data
   **/
  void clear();

  /**
   *Load from a spd file, clear the spectrum first but only if file can be opened
   **/
  bool loadSPDFile(std::string const &file_path);

  /**
   * Load from a spd string contained in a spd file
   * WARNING: does not clear the spectrum first
   **/
  void loadFromSPDString(std::string const &spd_string);

  /**
   * Save a spectrum to a spd file
   * Overwrite existing file
   **/
  void saveToSPDFile(std::string const &filepath);

  //accessors
  typename std::vector<T_val>::iterator        valuesBegin();
  typename std::vector<T_val>::iterator        valuesEnd();
  typename std::vector<T_val>::const_iterator  valuesCbegin() const;
  typename std::vector<T_val>::const_iterator  valuesCend() const;
  typename std::vector<T_wave>::iterator       wavelengthsBegin();
  typename std::vector<T_wave>::iterator       wavelengthsEnd();
  typename std::vector<T_wave>::const_iterator wavelengthsCbegin() const;
  typename std::vector<T_wave>::const_iterator wavelengthsCend() const;

  //Operators
  inline Spectrum_T<T_wave, T_val> &operator*=(float const &f);
  inline Spectrum_T<T_wave, T_val>  operator*(float const &f) const;
  inline Spectrum_T<T_wave, T_val> &operator/=(float const &f);
  inline Spectrum_T<T_wave, T_val>  operator/(float const &f) const;
  inline Spectrum_T<T_wave, T_val> &operator+=(float const &f);
  inline Spectrum_T<T_wave, T_val>  operator+(float const &f) const;
  inline Spectrum_T<T_wave, T_val> &operator-=(float const &f);
  inline Spectrum_T<T_wave, T_val>  operator-(float const &f) const;

  //operators spectrum X spectrum, works only
  //with spectrum defined at the same wavelengths
  inline Spectrum_T<T_wave, T_val> &operator*=(Spectrum_T<T_wave, T_val> const &f);
  inline Spectrum_T<T_wave, T_val>  operator*(Spectrum_T<T_wave, T_val> const &f) const;
  inline Spectrum_T<T_wave, T_val> &operator/=(Spectrum_T<T_wave, T_val> const &f);
  inline Spectrum_T<T_wave, T_val>  operator/(Spectrum_T<T_wave, T_val> const &f) const;
  inline Spectrum_T<T_wave, T_val> &operator+=(Spectrum_T<T_wave, T_val> const &f);
  inline Spectrum_T<T_wave, T_val>  operator+(Spectrum_T<T_wave, T_val> const &f) const;
  inline Spectrum_T<T_wave, T_val> &operator-=(Spectrum_T<T_wave, T_val> const &f);
  inline Spectrum_T<T_wave, T_val>  operator-(Spectrum_T<T_wave, T_val> const &f) const;

  //operators spectrum VS spectrum, works only
  //with spectrum defined at the same wavelengths
  inline bool operator==(Spectrum_T<T_wave, T_val> const &f) const;



  inline Spectrum_T<T_wave, T_val> sqrt() const;
  inline Spectrum_T<T_wave, T_val> pow(float exponent) const;
  inline Spectrum_T<T_wave, T_val> sqr() const;
  inline T_val                     luminance() const;
  inline T_val                     avgCmp() const;

  size_t                     size() const;
  std::vector<T_wave> const &wavelengths() const;
  std::vector<T_wave> &      wavelengths();
  std::vector<T_val> const & values() const;
  std::vector<T_val> &       values();
  //T_val findValue(T_wave wavelength)const;
  T_val &      operator[](std::size_t const index);
  T_val const &operator[](std::size_t const index) const;
  T_val        findLinearyInterpolatedValue(T_wave wavelength, int upper_index = 0) const;
  T_val        normalizedIntegral() const;

  T_val findIntegratedValue(T_wave wavelength, T_wave interval) const;

private:
  //TODO find BOOST and use a flat_map
  //Wavelenghts in nanometer (nm)
  std::vector<T_wave> _wavelengths;
  std::vector<T_val>  _values;
};

#include "spectrum.inl"

typedef Spectrum_T<mrf::uint, float> Spectrum;

}   // namespace radiometry
}   // namespace mrf
