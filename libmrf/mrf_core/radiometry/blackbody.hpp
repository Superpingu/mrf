/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/radiometry/spectrum.hpp>

#include <cmath>
#include <algorithm>

namespace mrf
{
namespace radiometry
{
class BlackBody
{
public:
  inline BlackBody();
  inline BlackBody(uint temperature);
  inline uint                   temperature() const;
  inline void                   setTemperature(uint temperature);
  inline Spectrum<float> &      spectrum();
  inline Spectrum<float> const &spectrum() const;
  /*
   * erase previous spectrum and generates a new one containing
   * values of blackbody emission (depends on temperature)
   * from start_wavelength to end_wavelengt included,
   * default offset betweeen wavelength is one
   * uint is Spectral radiance: W * sr-1 * m-2
   */
  inline void generateSpectrum(uint start_wavelength, uint end_wavelength, uint offset_wavelength = 1);

  /*
   * black body constant for spectrum computation from temperature
   */
  static constexpr float _BLACKBODY_C_1 = 3.74150E-16f;
  static constexpr float _BLACKBODY_C_2 = 1.4388E-2f;

  static constexpr double _SPEED_OF_LIGHT          = 299792458;
  static constexpr double _BOLTZMANN_CONSTANT      = 1.38064852E-23;
  static constexpr double _PLANCK_CONSTANT         = 6.626070040E-34;
  static constexpr double _PLANCK_TIMES_C_CONSTANT = 1.98644568E-25;

private:
  Spectrum<float> _spectrum;
  uint            _temperature;   //temperature in Kelvin
};

#include "blackbody.inl"
};   // namespace radiometry
};   // namespace mrf
