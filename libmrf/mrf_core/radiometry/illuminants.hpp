/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS (c) 2018
 *
 **/
#pragma once

#include <map>
#include <stdexcept>

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/radiometry/d65.hpp>
#include <mrf_core/radiometry/d50.hpp>
#include <mrf_core/radiometry/spectrum.hpp>

namespace mrf
{
namespace radiometry
{
enum MRF_ILLUMINANTS
{
  NONE = 0,
  D65  = 1,
  D50  = 2
};

static std::map<std::string, MRF_ILLUMINANTS> ILLUMINANTS_NAME_TO_TYPE = {{"E", NONE}, {"D65", D65}, {"D50", D50}};

inline MRF_ILLUMINANTS illuminantSpectrum(std::string const &illuminant_name)
{
  auto element = ILLUMINANTS_NAME_TO_TYPE.find(illuminant_name);

  if (element == ILLUMINANTS_NAME_TO_TYPE.end())
  {
    throw std::invalid_argument(
        "Illuminant name could not be found!!! Illuminant " + illuminant_name + " not supported");
  }

  return element->second;
}


inline mrf::radiometry::Spectrum illuminantSpectrum(MRF_ILLUMINANTS const &type_of_illuminants)
{
  switch (type_of_illuminants)
  {
    case NONE:   //Return a flat illuminant. I.e. type E.
    {
      mrf::radiometry::Spectrum E(1.0f);
      return E;
    }

    case D65:
    {
      mrf::radiometry::Spectrum d65_ill;
      for (uint i = 0; i < D_65_ARRAY_SIZE; i += D_65_PRECISION)
      {
        d65_ill.add(D_65_FIRST_WAVELENGTH + i, D_65_SPD[i]);
      }
      return d65_ill;
    }

    case D50:
    {
      mrf::radiometry::Spectrum d50_ill;

      mrf::radiometry::Spectrum d65_ill;
      for (uint i = 0; i < D_50_ARRAY_SIZE; i += D_50_PRECISION)
      {
        d50_ill.add(D_50_FIRST_WAVELENGTH + i, D_50_SPD[i]);
      }

      return d50_ill;
    }

    default:
      return mrf::radiometry::Spectrum();
  }
}


}   // namespace radiometry
}   // namespace mrf
