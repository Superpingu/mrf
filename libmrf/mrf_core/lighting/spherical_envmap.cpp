#include <mrf_core/lighting/spherical_envmap.hpp>

#include <iostream>
#include <cerrno>
#include <cmath>

namespace mrf
{
namespace lighting
{
using namespace std;

using namespace mrf::math;
using namespace mrf::geom;
using namespace mrf::materials;
using namespace mrf::image;


SphericalEnvMap::SphericalEnvMap(
    std::string const &filename,
    float              phi_rotation_in_degree,
    float              theta_rotation_in_degree)
  : EnvMap(phi_rotation_in_degree, theta_rotation_in_degree)
{
  _file_path = filename;
}

SphericalEnvMap::~SphericalEnvMap() {}

mrf::math::Vec2f SphericalEnvMap::uv(mrf::math::Vec3f const &a_dir) const
{
  // From debevec: Thus, if we consider the images to be normalized to have coordinates u=[-1,1], v=[-1,1],
  // we have theta=atan2(v,u), phi=pi*sqrt(u*u+v*v). The unit vector pointing in the corresponding
  // direction is obtained by rotating (0,0,-1) by phi degrees around the y (up) axis and then theta
  // degrees around the -z (forward) axis.
  // If for a direction vector in the world (Dx, Dy, Dz),
  // the corresponding (u,v) coordinate in the light probe image is (Dx*r,Dy*r)
  // where r=(1/pi)*acos(Dz)/sqrt(Dx^2 + Dy^2).
  //
  // RP:  I then translate u and v to [0,1]x[0,1]

  Vec3f       dir   = _invRot * a_dir;
  float const denom = std::sqrt(dir.x() * dir.x() + dir.y() * dir.y());

#ifdef MRF_TEST_RELEASE
  if (Math::absVal(denom) < EPSILON)
  {
    assert(0);
    return Vec2f(0.5f, 0.5f);
  }
#endif

  float const r = static_cast<float>(Math::INV_PI) * std::acos(dir.z()) / denom;
  return Vec2f(dir.x() * r * 0.5f + 0.5f, dir.y() * r * 0.5f + 0.5f);
}

mrf::math::Vec3f SphericalEnvMap::direction(mrf::math::Vec2f const &uvs) const
{
  // TODO: untested code

  // We want uv in [-1;1]
  Vec2f uv = 2.f * uvs - Vec2f(1.f);

  // Get angles theta=atan2(v,u), phi=pi*sqrt(u*u+v*v)
  const float theta = std::atan2(uv.y(), uv.x());
  const float phi   = static_cast<float>(Math::PI) * std::sqrt(uv.x() * uv.x() + uv.y() * uv.y());

  const float sinTheta = std::sin(theta);

  return _rot * mrf::math::Vec3f(sinTheta * std::cos(phi), sinTheta * std::sin(phi), std::cos(theta));
}


mrf::materials::RADIANCE_TYPE
SphericalEnvMap::radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const & /*lf*/) const
{
  return radianceEmitted(emit_out_dir);
}


mrf::materials::RADIANCE_TYPE SphericalEnvMap::radianceEmitted(mrf::math::Vec3f const &emit_out_dir) const
{
  //------------------------------------------------------------------------------
  Vec3f c_eod = emit_out_dir;
  c_eod[0]    = clamp(c_eod[0], -1.f, 1.f);
  c_eod[1]    = clamp(c_eod[1], -1.f, 1.f);
  c_eod[2]    = clamp(c_eod[2], -1.f, 1.f);

  //------------------------------------------------------------------------------
  Vec2f uvs = uv(c_eod);
  uvs[0]    = clamp(uvs[0], 0.0f, 1.0f);
  uvs[1]    = clamp(uvs[1], 0.0f, 1.0f);
  //------------------------------------------------------------------------------
  if (!(uvs[0] >= 0.0f))
  {
    std::cout << " emit_out_dir = " << emit_out_dir << std::endl;
    std::cout << "  uvs = " << uvs << std::endl;
    assert(0);
    std::exit(-1);
  }

  assert(uvs[0] <= 1.0f);
  assert(uvs[1] >= 0.0f);
  assert(uvs[1] <= 1.0f);
  //------------------------------------------------------------------------------

  float const iy = uvs[1] * (height() - 1);
  float const ix = uvs[0] * (width() - 1);

  float const ix_low = std::floor(ix);
  float const iy_low = std::floor(iy);

  unsigned int const index_ix_low = static_cast<unsigned int>(ix_low);
  unsigned int const index_iy_low = static_cast<unsigned int>(iy_low);

  assert(index_iy_low < _envmap->height());
  assert(index_ix_low < _envmap->width());

  unsigned int const index_iy_up = static_cast<unsigned int>(index_iy_low + 1) % height();
  unsigned int const index_ix_up = static_cast<unsigned int>(index_ix_low + 1) % width();

  assert(index_iy_up < _envmap->height());
  assert(index_ix_up < _envmap->width());

  float const alpha = iy - iy_low;
  float const beta  = ix - ix_low;

  // RADIANCE_TYPE const radiance_ix_iy_low = radianceFromPixel( index_ix_low, index_iy_low);
  // assert( radiance_ix_iy_low.power() > 0.0f );

  // RADIANCE_TYPE const radiance_ix_low_iy_up = radianceFromPixel( index_ix_low, index_iy_up );
  // assert( radiance_ix_low_iy_up.power() > 0.0f );

  // RADIANCE_TYPE const radiance_ix_up_iy_low = radianceFromPixel( index_ix_up, index_iy_low);
  // //std::cout << " index_ix_up = " << index_ix_up << " " << index_iy_low << std::endl;
  // assert( radiance_ix_up_iy_low.power() > 0.0f );

  // RADIANCE_TYPE const radiance_ix_up_iy_up  = radianceFromPixel( index_ix_up, index_iy_up );
  // assert( radiance_ix_up_iy_up.power() > 0.0f );


  RADIANCE_TYPE const interpo_x_low = radianceFromPixel(index_ix_low, index_iy_low) * (1.f - beta)
                                      + radianceFromPixel(index_ix_low, index_iy_up) * beta;
  RADIANCE_TYPE const interpo_x_up = radianceFromPixel(index_ix_up, index_iy_low) * (1.f - beta)
                                     + radianceFromPixel(index_ix_up, index_iy_up) * beta;


  RADIANCE_TYPE const interpo_y = interpo_x_low * (1.f - alpha) + interpo_x_up * alpha;


  // if( interpo_y[0] > 0.0 )
  // {
  //     std::cout << "Radiance from envmap = interpo_y = " << interpo_y << std::endl;
  //     std::exit(-1);
  // }

  return interpo_y;
}

/**
 * \todo
 **/
// mrf::materials::RADIANCE_TYPE SphericalEnvMap::computeAverageLuminanceOverHorizon(uint nb_samples) const
// {
//   return mrf::materials::RADIANCE_TYPE(0.0f);
// }

}   // namespace lighting
}   // namespace mrf
