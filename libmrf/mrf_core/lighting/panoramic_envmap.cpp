#include <mrf_core/lighting/panoramic_envmap.hpp>

#include <iostream>
#include <cerrno>
#include <cmath>
#include <iomanip>

namespace mrf
{
namespace lighting
{
using namespace std;

using namespace mrf::math;
using namespace mrf::geom;
using namespace mrf::materials;
using namespace mrf::image;


PanoramicEnvMap::PanoramicEnvMap(
    std::string const &filename,
    float              phi_rotation_in_degree,
    float              theta_rotation_in_degree,
    float              gamma_rotation_in_degree)
  : EnvMap(phi_rotation_in_degree, theta_rotation_in_degree, gamma_rotation_in_degree)
{
  _file_path = filename;
}

PanoramicEnvMap::~PanoramicEnvMap() {}

Vec2f PanoramicEnvMap::uv(Vec3f const &a_dir) const
{
#ifdef PANORAMIC_CLASS_DEBUG
  assert(std::isfinite(a_dir[0]));
  assert(std::isfinite(a_dir[1]));
  assert(std::isfinite(a_dir[2]));
#endif

  Vec3f dir = _invRot * a_dir;
  dir.normalize();

#ifdef PANORAMIC_CLASS_DEBUG
  assert(std::isfinite(dir[0]));
  assert(std::isfinite(dir[1]));
  assert(std::isfinite(dir[2]));
#endif

  float const u = 1.f + (std::atan2(dir[0], -dir[2]) * static_cast<float>(Math::INV_PI));

  float const v1 = std::acos(dir[1]);

#ifdef PANORAMIC_CLASS_DEBUG
  assert(std::isfinite(u));
  assert(std::isfinite(v1));
#endif

  float const v = v1 * static_cast<float>(Math::INV_PI);

  //The previous mapping maps u in [0,2] and v in [0,1]
  return Vec2f(u * 0.5f, v);
}

Vec3f PanoramicEnvMap::direction(Vec2f const &uvs) const
{
  float const u = uvs[0] * 2.0f;
  float const v = uvs[1];

  float const phi   = static_cast<float>(Math::PI) * (u - 1);
  float const theta = static_cast<float>(Math::PI) * v;

  return _rot * Vec3f(sin(theta) * sin(phi), cos(theta), -sin(theta) * cos(phi));
}

RADIANCE_TYPE
PanoramicEnvMap::radianceEmitted(Vec3f const &emit_out_dir, LocalFrame const & /*lf*/) const
{
  return radianceEmitted(emit_out_dir);
}


RADIANCE_TYPE
PanoramicEnvMap::radianceEmitted(Vec3f const &emit_out_dir) const
{
  Vec3f c_eod = emit_out_dir;
  c_eod[0]    = clamp(c_eod[0], -1.0f, 1.0f);
  c_eod[1]    = clamp(c_eod[1], -1.0f, 1.0f);
  c_eod[2]    = clamp(c_eod[2], -1.0f, 1.0f);

  Vec2f uvs = uv(c_eod);

  uvs[0] = clamp(uvs[0], 0.0f, 1.0f);
  uvs[1] = clamp(uvs[1], 0.0f, 1.0f);


  //-------------------------------------------------------------------------
  // NEAREST
  //-------------------------------------------------------------------------
  // //Column Index
  // unsigned int const j = static_cast<unsigned int>( uvs[0] * width() );
  // //Row Index
  // unsigned int const i = static_cast<unsigned int>( uvs[1] * height() );

  // return radianceFromPixel( i, j );
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  // BILINEAR
  //-------------------------------------------------------------------------
  float const ix = uvs[1] * (height() - 1);
  float const iy = uvs[0] * (width() - 1);

  float const ix_low = std::floor(ix);
  float const iy_low = std::floor(iy);


  unsigned int const index_ix_low = static_cast<unsigned int>(ix_low);
  unsigned int const index_iy_low = static_cast<unsigned int>(iy_low);

  assert(index_ix_low < _envmap->height());
  assert(index_iy_low < _envmap->width());


  unsigned int const index_ix_up = static_cast<unsigned int>(index_ix_low + 1) % height();
  unsigned int const index_iy_up = static_cast<unsigned int>(index_iy_low + 1) % width();

  assert(index_ix_up < _envmap->height());
  assert(index_iy_up < _envmap->width());


  float const alpha = ix - ix_low;
  float const beta  = iy - iy_low;


  RADIANCE_TYPE const interpo_x_low = radianceFromPixel(index_iy_low, index_ix_low) * (1.f - beta)
                                      + radianceFromPixel(index_iy_low, index_ix_up) * beta;
  RADIANCE_TYPE const interpo_x_up = radianceFromPixel(index_iy_up, index_ix_low) * (1.f - beta)
                                     + radianceFromPixel(index_iy_up, index_ix_up) * beta;


  RADIANCE_TYPE const interpo_y = interpo_x_low * (1.f - alpha) + interpo_x_up * alpha;


#ifdef PANORAMIC_CLASS_DEBUG
  assert(!interpo_x_low.isInvalid());
  assert(!interpo_x_up.isInvalid());
  assert(!interpo_y.isInvalid());
#endif



  return interpo_y;
}


mrf::materials::RADIANCE_TYPE PanoramicEnvMap::computeAverageLuminanceOverHorizon(uint nb_samples) const
{
  //in spectral mode we need to allocate the
  //resulting spectrum to make the + operator on spectrum works
#ifdef MRF_RENDERING_MODE_SPECTRAL
  mrf::materials::RADIANCE_TYPE res;

  res.values().resize(_envmap->wavelengths().size());
  auto temp_w = _envmap->wavelengths();
  res.wavelengths().resize(temp_w.size());

  uint num_w = 0;
  for (auto wave : temp_w)
  {
    res.wavelengths()[num_w] = static_cast<float>(wave);
    res.values()[num_w]      = 0.f;
    num_w++;
  }
#else
  mrf::materials::RADIANCE_TYPE res = 0.f;
#endif

  std::cout << "LUM OVER HORIZON" << std::endl;
  /**
   * First method iterate over a set of direction
   **/

  int resolution_theta = 90;
  int resolution_phi   = 360;


  uint nb_values = resolution_theta * resolution_phi;
#if 0   //OLD METHOD sampling theta and phi over horizon
  for (int i = 0; i < resolution_phi; i++)
  {
    for (int j = 0; j < resolution_theta; j++)
    {
      float theta = float(j) * float(mrf::math::Math::PI) / 180.f;
      float phi = float(i) * float(mrf::math::Math::PI) / 180.f;
      auto direction = mrf::math::Vec3f(sinf(theta)*sinf(phi), cosf(theta), -sinf(theta)*cosf(phi));


      auto radiance = radianceEmitted(direction);
      res += radiance;

      //std::cout << radianceEmitted(direction) << std::endl;
      //RADIANCE_TYPE radiance;// = radianceEmitted(direction);
      //radiance.wavelengths().resize( res.wavelengths().size());
      //radiance.values().resize( res.wavelengths().size());
      //res += radiance;
    }
  }
  std::cout << "END LUM OVER HORIZON" << res <<std::endl;
  return res / static_cast<float>(nb_values);
//#endif
#endif

#if 0
  uint nb_samples = 10000000;

  auto& rng = mrf::sampling::RandomGenerator::Instance();

  for (uint i = 0; i < nb_samples; i++)
  {
    float r1 = rng.getFloat();
    float r2 = rng.getFloat();

    auto direction = mrf::math::Vec3f(cosf(2.f*M_PI*r1)*sqrtf(1.f-r2*r2), r2, sinf(2.f*M_PI*r1)*sqrtf(1.f - r2*r2));

    auto radiance = radianceEmitted(direction);
    float pdf = 1.f / (2.f*M_PI);
    float cosTheta = r2;
    res += radiance *cosTheta / (pdf);
  }
  return res/nb_samples;
#endif

  //#if 0 //IS METHOD
  //  uint nb_samples = 50000000;

  auto &rng = mrf::sampling::RandomGenerator::Instance();

  nb_values = 0;

  for (uint i = 0; i < nb_samples; i++)
  {
    float r1 = rng.getFloat();
    float r2 = rng.getFloat();

    float pdf_col, pdf_row;

    uint row_number = static_cast<uint>(_cdf_1d_envmap_rows.inverse(r1, pdf_row));
    assert(row_number < height());

    uint col_number = static_cast<uint>(_cdf_1d_envmap_columns[row_number].inverse(r2, pdf_col));
    assert(col_number < width());

    if (row_number > height() / 2)
    {
      //std::cout << "Under horizon row=" << row_number << std::endl;
      continue;
    }
    else
    {
      //std::cout << "Over horizon row=" << row_number << std::endl;
    }

    //float u = float(col_number) / float(width() - 1);
    float v = float(row_number) / float(height() - 1);

    float temp_theta = v * float(Math::PI);


    float pdf = pdf_col * pdf_row / (2.f * float(Math::PI) * float(Math::PI) * sin(temp_theta));

    res += radianceFromPixel(col_number, row_number) * cosf(temp_theta) / (pdf);   // *nb_samples);
    nb_values++;
  }

  //std::cout << "END LUM OVER HORIZON" << res << std::endl;

  //std::cout << "END LUM OVER HORIZON" << res * (nb_samples / static_cast<float>(nb_values)) << std::endl;

  //return res;
  //std::cout << "END LUM OVER HORIZON nb_values =" << nb_values <<"ratio="<< double(nb_values)/ double(nb_samples)<< std::endl;
  return res * (1.f / static_cast<float>(nb_values));
  //return res *(nb_samples / static_cast<float>(nb_values));
  //#endif

#if 0

#  ifdef MRF_RENDERING_MODE_SPECTRAL
  num_w = 0;
  for (auto wave : temp_w)
  {
    res.values()[num_w++] = 0.f;
  }
#  else
  res = 0.f;
#  endif
  resolution_theta = 90 * 100;
  resolution_phi = 360 * 100;
  nb_values = resolution_theta * resolution_phi;

  for (int i = 0; i < resolution_phi; i++)
  {
    for (int j = 0; j < resolution_theta; j++)
    {
      float theta = float(j) * float(mrf::math::Math::PI) / 180.f;
      float phi = float(i) * float(mrf::math::Math::PI) / 180.f;
      auto direction = mrf::math::Vec3f(sinf(theta)*sinf(phi), cosf(theta), -sinf(theta)*cosf(phi));


      auto radiance = radianceEmitted(direction);
      res += radiance;
    }
  }
  std::cout << "END LUM OVER HORIZON" << res << std::endl;
#  ifdef MRF_RENDERING_MODE_SPECTRAL
  num_w = 0;
  for (auto wave : temp_w)
  {
    res.values()[num_w++] = 0.f;
  }
#  else
  res = 0.f;
#  endif
  resolution_theta = 90 * 100 ;
  resolution_phi = 360 *100 ;
  nb_values = resolution_theta * resolution_phi;

  for (int i = 0; i < resolution_phi; i++)
  {
    for (int j = 0; j < resolution_theta; j++)
    {
      float theta = float(j) * float(mrf::math::Math::PI) / 180.f;
      float phi = float(i) * float(mrf::math::Math::PI) / 180.f;
      auto direction = mrf::math::Vec3f(sinf(theta)*sinf(phi), cosf(theta), -sinf(theta)*cosf(phi));


      auto radiance = radianceEmitted(direction);
      res += radiance;
    }
  }
  std::cout << "END LUM OVER HORIZON" << res << std::endl;
#endif



  /**
   * Second method, iterate over all pixels of envmap
   **/
  /*
  int resolution_theta = 90 * 100;
  int resolution_phi = 360 * 100;

  int nb_values = width() * height();

  for (int i = 0; i < height(); i++)
  {
    for (int j = 0; j < width(); j++)
    {
      // Return the RADIANCE value for a given pixel
// i = column index [0-width]
// j = row index [0-height]
// WARNING THis is super slow in SPECTRAL MODE, need to reconstruct a spectrum

      auto radiance = radianceFromPixel(j,i);
      res += radiance;
    }
  }
  std::cout << "END LUM OVER HORIZON" << res << std::endl;

  std::cout << "END LUM OVER HORIZON divided" << res / static_cast<float>(nb_values) << std::endl;
  return res / static_cast<float>(nb_values);
  */
}


}   // namespace lighting
}   // namespace mrf
