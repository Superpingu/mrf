/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/lighting/envmap.hpp>
#include <mrf_core/image/color_image.hpp>
#include <mrf_core/materials/umat.hpp>
#include <mrf_core/sampling/random_generator.hpp>
#include <mrf_core/sampling/jittering.hpp>

//STL
#include <vector>
#include <algorithm>
#include <map>
#include <string>


namespace mrf
{
namespace lighting
{
/**
 * This class represents a panoramical environment map
 * as defined here : http://gl.ict.usc.edu/Data/HighResProbes/
 *
 *
 **/
class MRF_CORE_EXPORT PanoramicEnvMap: public EnvMap
{
public:
  /**
   * Construct an empty Environment Map with given width and height
   **/
  // PanoramicEnvMap(unsigned int width, unsigned int height);

  /**
   * Loads a panoramic envmap from a filename.
   * The file must be a mallia floating point .mfi extension
   *
   **/
  PanoramicEnvMap(
      std::string const &,
      float phi_rotation_in_degree   = 0.0f,
      float theta_rotation_in_degree = 0.0f,
      float gamma_rotation_in_degree = 0.0f);

  /**
   *Destructor
   **/
  ~PanoramicEnvMap();

  /**
   * Return an luminance image of the environment map
   * Usefull to apply some operations like segmentation etc
   **/
  inline void getLuminanceEnvmap(mrf::image::MultiChannelImage<float> &luminance_image, float &mean_luminance) const;

  /**
   * Sets the RADIANCE value for a given pixel
   * i = column index [0-width]
   * j = row index [0-height]
   **/
  inline void setRadiance(unsigned int i, unsigned int j, mrf::materials::RADIANCE_TYPE const &radiance);


  inline mrf::math::Vec3f directionFromPixel(unsigned int i, unsigned int j) const;
  inline void             directionFromPixel(unsigned int i, unsigned int j, mrf::math::Vec3f &dir) const;

  /**
   * Return the RADIANCE value for a given pixel
   * and compute associated direction
   * i = column index [0-width]
   * j = row index [0-height]
   **/
  inline void radianceDirectionFromPixel(
      unsigned int                   i,
      unsigned int                   j,
      mrf::materials::RADIANCE_TYPE &radiance,
      mrf::math::Vec3f &             dir) const;


  inline std::string info() const;

  inline void computeIrradianceMap(PanoramicEnvMap &irr_map) const;
  inline void computeIrradianceMap(PanoramicEnvMap &irr_map, unsigned int num_samples) const;

  //-------------------------------------------------------------------------
  // EnvMap Interface Implementation
  //-------------------------------------------------------------------------
  virtual mrf::math::Vec2f uv(mrf::math::Vec3f const &dir) const;
  virtual mrf::math::Vec3f direction(mrf::math::Vec2f const &uvs) const;
  virtual mrf::materials::RADIANCE_TYPE
  radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const &lf) const;

  virtual mrf::materials::RADIANCE_TYPE radianceEmitted(mrf::math::Vec3f const &emit_out_dir) const;

  /**
   * Compute average luminance emitted over horizon
   * Might take some time has it needs to loop over 360*90 values
   * in the image.
   * \todo This is for now a fixed resolution, use two parameters to choose the resolution in
   * theta and phi.
   * \todo Make it work in spectral mode.
   * This method is usefull for shadow catchers.
   **/
  virtual mrf::materials::RADIANCE_TYPE computeAverageLuminanceOverHorizon(uint nb_samples) const;

  //-------------------------------------------------------------------------
  // SPECIAL FUNCTION
  //-------------------------------------------------------------------------
  // TODO: Only partial implementation provided
  // template<class VECT_TYPE, class POWER_TYPE>
  // void powerBasedSampling(
  //     std::vector<std::pair<VECT_TYPE, POWER_TYPE>> &dir_pow_light_samples,
  //     unsigned int                                   NB_LUMI_COMPONENT) const;
};


inline void PanoramicEnvMap::getLuminanceEnvmap(
    mrf::image::MultiChannelImage<float> & /*luminance_image*/,
    float & /*mean_luminance*/) const
{
  assert(0);
  //TODO
  //_envmap->setLuminanceImage( luminance_image, mean_luminance );
}


inline void PanoramicEnvMap::setRadiance(unsigned int i, unsigned int j, mrf::materials::RADIANCE_TYPE const &radiance)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  assert(radiance.wavelengths().size() == _envmap->wavelengths().size());

  // auto const & temp_w = _envmap.wavelengths();
  // uint num_w = 0;
  // for (auto wave : temp_w)
  // {
  //   //accessor for mrf::Array2D and Eigen::Array are (row,column)
  //   _envmap.data()[num_w](j, i) = radiance.values()[num_w];

  //   num_w++;
  // }

  //Optimized Version with no Compilation warning
  std::vector<uint> const &waves = _envmap->wavelengths();

  //#pragma omp parallel default(none) shared(_envmap)
  for (size_t num_w = 0; num_w < waves.size(); num_w++)
  {
    _envmap->data()[num_w](j, i) = radiance.values()[num_w];
  }


#else
  _envmap->setPixel(i, j, radiance);
#endif
}

inline mrf::math::Vec3f PanoramicEnvMap::directionFromPixel(unsigned int i, unsigned int j) const
{
  mrf::math::Vec2f uvs;
  //Column Index
  uvs[0] = static_cast<float>(i) / width();
  //Row Index
  uvs[1] = static_cast<float>(j) / height();

  return direction(uvs);
}

inline void PanoramicEnvMap::directionFromPixel(unsigned int i, unsigned int j, mrf::math::Vec3f &dir) const
{
  dir = directionFromPixel(i, j);
}



inline void PanoramicEnvMap::radianceDirectionFromPixel(
    unsigned int                   i,
    unsigned int                   j,
    mrf::materials::RADIANCE_TYPE &radiance,
    mrf::math::Vec3f &             dir) const
{
  radiance = radianceFromPixel(i, j);
  dir      = directionFromPixel(i, j);
}



inline std::string PanoramicEnvMap::info() const
{
  std::string info(" Panoramic Environment Map Information [ ...");
  //TODO
  //info.append( _envmap->info() );
  info.append(" ] ");
  return info;
}

inline void PanoramicEnvMap::computeIrradianceMap(PanoramicEnvMap &irr_map, unsigned int num_samples) const
{
  using namespace std;
  using namespace mrf::math;
  using namespace mrf::materials;
  using namespace mrf::sampling;


  JitteredSample2D js2d(num_samples);
  vector<float>    epsilon1;
  epsilon1.resize(num_samples);
  vector<float> epsilon2;
  epsilon2.resize(num_samples);
  vector<Vec3f> directions;


  for (unsigned int i = 0; i < num_samples; i++)
  {
    epsilon1[i] = js2d.getRandomFloat(0, i, RandomGenerator::Instance().getFloat());
    epsilon2[i] = js2d.getRandomFloat(1, i, RandomGenerator::Instance().getFloat());

    directions.push_back(UMat::randomSphereDirection(epsilon1[i], epsilon2[i]));
  }


  std::cout << " Starting to compute Irradiance Map " << std::endl;


  for (unsigned int i = 0; i < height(); i++)
  {
    for (unsigned int j = 0; j < width(); j++)
    {
      Vec3f normal_dir;
      directionFromPixel(j, i, normal_dir);

      RADIANCE_TYPE irradiance;
      RADIANCE_TYPE radiance;

      for (unsigned int d = 0; d < directions.size(); d++)
      {
        radiance = radianceEmitted(directions[d]);
        irradiance += radiance * Math::max(normal_dir.dot(directions[d]), 0.0f);

      }   //end of d loop

      irradiance *= static_cast<float>(Math::FOUR_PI) / (height() * width());
      irr_map.setRadiance(j, i, irradiance);

    }   //end of j loop

    std::cout << " Row " << i << " finished " << std::endl;

  }   //end of i loop
}


inline void PanoramicEnvMap::computeIrradianceMap(PanoramicEnvMap &irr_map) const
{
  using namespace mrf::math;
  using namespace mrf::materials;

  std::cout << " Starting to compute Irradiance Map " << std::endl;


  for (unsigned int i = 0; i < height(); i++)
  {
    for (unsigned int j = 0; j < width(); j++)
    {
      //std::cout << " Starting Pixel " << i << j << std::endl;

      Vec3f normal_dir;
      directionFromPixel(j, i, normal_dir);


      RADIANCE_TYPE irradiance;
      RADIANCE_TYPE radiance;
      Vec3f         dir;

      for (unsigned int ii = 0; ii < height(); ii++)
      {
        for (unsigned int jj = 0; jj < width(); jj++)
        {
          radianceDirectionFromPixel(jj, ii, radiance, dir);

          irradiance += radiance * Math::max(normal_dir.dot(dir), 0.0f);
        }
      }

      irradiance *= static_cast<float>(Math::FOUR_PI) / (height() * width());


      irr_map.setRadiance(j, i, irradiance);




    }   //end of j loop

    std::cout << "Exact Solution Row " << i << " finished " << std::endl;

  }   //end of i loop


}   //end of computeIrradianceMap




// template<class VECT_TYPE, class POWER_TYPE>
// void PanoramicEnvMap::powerBasedSampling(
//     std::vector<std::pair<VECT_TYPE, POWER_TYPE>> &dir_pow_light_samples,
//     unsigned int                                   NB_LUMI_COMPONENT) const
// {
//   std::cerr << " This method is bugged or not finished !!! AT " << __FILE__ << " " << __LINE__ << std::endl;
//   assert(0);
// #if 0
//   unsigned int const height = _envmap.height();
//   unsigned int const width  = _envmap.width();

//   mrf::image::Image<float> luminance_image;
//   luminace_image.init(width, height);
//   float mean_luminance;
//   _envmap.setLuminanceImage( luminance_image, mean_luminance );
//   std::cout << "  mean_luminance = " << mean_luminance << std::endl ;

//   float std_deviation = 0.0f;
//   for( unsigned int i = 0 ; i < luminance_image.size() ; i++ )
//   {
//     std_deviation += (luminance_image[i] - mean_luminance)*(luminance_image[i] - mean_luminance);
//   }

//   std_deviation /= static_cast<float>(luminance_image.size() );
//   std_deviation = std::sqrtf( std_deviation );
//   std_deviation = mean_luminance;


//   std::cout << "  std_deviation = " << std_deviation << std::endl ;
//   std::map< float, unsigned int > lumi_separator;

//   for( unsigned int i = 0 ; i < NB_LUMI_COMPONENT ; i++ )
//   {
//     float const limit = i * std_deviation ;

//     lumi_separator[limit] = i;

//     //#ifdef DEBUG
//     std::cout << "  lumi_separator[" <<  limit << "] = "
//               << lumi_separator[limit] << std::endl ;
//     //#endif
//   }

//   // Storing per pixel the class connected componnent of the pixel
//   mrf::image::Image<unsigned int, 1> cmpt_class_image( luminance_image.width(), luminance_image.height() );
//   mrf::image::Image<unsigned int, 1> cmpt_class_image_output( luminance_image.width(), luminance_image.height() );

//   //#ifdef DEBUG
//   std::cout << "  luminance_image.size() = " << luminance_image.size() << std::endl ;
//   //#endif


//   for( unsigned int i = 0 ; i < luminance_image.size()  ; ++i )
//   {
//     float const curr_lumi = luminance_image[i];
//     std::map< float, unsigned int >::const_iterator pos = lumi_separator.upper_bound( curr_lumi );
//     --pos;

//     cmpt_class_image[ i ] = pos->second;

//     unsigned int const color_code = static_cast<unsigned int>( round( (255.0f * pos->second / static_cast<float>(NB_LUMI_COMPONENT))));

//     assert( color_code <= 255 );

//     cmpt_class_image_output[ i ] = color_code;
//   }

// #  ifdef MRF_USE_FREEIMAGE
//   if( ! mrf::image::ImageSaver::instance().save( cmpt_class_image_output.pixels(),
//       width,
//       height,
//       1,
//       "envmap-cmpt-class.png",
//       true ) )
//   {
//     std::cout << " COULD NOT SAVED the envmap connected component class image per pixel " << std::endl;
//   }
// #  endif


//   //Connected Component number per pixel
//   mrf::image::Image<unsigned int, 1> cmpt_image( luminance_image.width(), luminance_image.height() );
//   cmpt_image.set( (unsigned int) 0, (unsigned int) 0, (unsigned int) 0 );

//   unsigned int conn_cmp_number = 0;
//   mrf::image::ConnCmpt< float > cc;
//   mrf::image::_Color3Pixel<> pixel( 0, 0,
//                                     _envmap->at( 0, 0, 0 ),
//                                     _envmap->at( 0, 0, 1 ),
//                                     _envmap->at( 0, 0, 2 ) );

//   mrf::math::Vec3f current_dir = directionFromPixel( 0, 0 );
//   cc.addPixel( pixel, current_dir );

//   std::map< unsigned int, mrf::image::ConnCmpt<float> * > cmpt_map;
//   typedef std::map< unsigned int, mrf::image::ConnCmpt<float> * >::const_iterator cmpt_map_CIT;
//   typedef std::map< unsigned int, mrf::image::ConnCmpt<float> * >::iterator cmpt_map_IT;

//   cmpt_map[ conn_cmp_number ] = &cc;
//   std::cout << " First Connected Component : " << cc << std::endl;


//   for( unsigned int i = 0 ; i < height ; i++ )
//   {
//     unsigned int j = ( i == 0 ) ? (1):(0);
//     for(  ; j < width ; j++ )
//     {

//       //#ifdef DEBUG
//       std::cout << " Pixel  i = " << i << "  j = " << j << std::endl ;
//       //#endif

//       unsigned int current_class = cmpt_class_image.at( i, j, 0 );
//       mrf::math::Vec3f current_dir = directionFromPixel( i, j );

//       mrf::image::_Color3Pixel<> pixel( i, j,
//                                         _envmap->at( i, j, 0 ),
//                                         _envmap->at( i, j, 1 ),
//                                         _envmap->at( i, j, 2 ) );

//       //Left Neighbor
//       if( (j > 0) &&  cmpt_class_image.at( i, j-1, 0 ) == current_class )
//       {
//         std::cout << " CASE LEFT NEIGHBOR " << std::endl;

//         unsigned int cmpt_neigh;
//         cmpt_neigh = cmpt_image.at( i, j-1, 0 );

//         //#ifdef DEBUG
//         cmpt_map_CIT it = cmpt_map.find( cmpt_neigh );
//         if( it == cmpt_map.end() )
//         {
//           std::cout << " WHAT THE HELL IS GOING ON. KEY NOT FOUND" << std::endl;
//           //#ifdef DEBUG
//           std::cout << "  i = " << i << "  j = " << j << "  current_class = " << current_class << std::endl ;
//           std::cout << "  cmpt_neigh = " << cmpt_neigh << std::endl ;
//           //#endif
//           assert(0);
//         }
//         //#endif

//         (cmpt_map[ cmpt_neigh ])->addPixel(  pixel, current_dir );
//         cmpt_image.set( i, j, 0, cmpt_neigh );

//         std::cout << "  Connected Componnent number " << cmpt_neigh << " is now : " << std::endl
//                   << *(cmpt_map[ cmpt_neigh ]) << std::endl;
//       }
//       // Upper Left
//       else if( (i>0) && (j>0) && cmpt_class_image.at( i-1, j-1, 0 ) == current_class )
//       {
//         std::cout << " UPPER left " << std::endl;

//         unsigned int cmpt_neigh;
//         cmpt_neigh = cmpt_image.at( i-1, j-1, 0 );

//         //#ifdef DEBUG
//         cmpt_map_CIT it = cmpt_map.find( cmpt_neigh );
//         if( it == cmpt_map.end() )
//         {
//           std::cout << " WHAT THE HELL IS GOING ON. KEY NOT FOUND" << std::endl;
//           //#ifdef DEBUG
//           std::cout << "  i = " << i << "  j = " << j << "  current_class = " << current_class << std::endl ;
//           std::cout << "  cmpt_neigh = " << cmpt_neigh << std::endl ;
//           //#endif
//           assert(0);
//         }
//         //#endif

//         (cmpt_map[ cmpt_neigh ])->addPixel(  pixel, current_dir );
//         cmpt_image.set( i, j, 0, cmpt_neigh );

//         std::cout << "  Connected Componnent number " << cmpt_neigh << " is now : " << std::endl
//                   << *(cmpt_map[ cmpt_neigh ]) << std::endl;

//       }
//       //Upper
//       else if( (i>0) && cmpt_class_image.at( i-1, j, 0 ) == current_class )
//       {
//         std::cout << " UPPER  " << std::endl;

//         unsigned int cmpt_neigh;
//         cmpt_neigh = cmpt_image.at( i-1, j, 0 );

//         //#ifdef DEBUG
//         cmpt_map_CIT it = cmpt_map.find( cmpt_neigh );
//         if( it == cmpt_map.end() )
//         {
//           std::cout << " WHAT THE HELL IS GOING ON. KEY NOT FOUND" << std::endl;
//           //#ifdef DEBUG
//           std::cout << "  i = " << i << "  j = " << j << "  current_class = " << current_class << std::endl ;
//           std::cout << "  cmpt_neigh = " << cmpt_neigh << std::endl ;
//           //#endif
//           assert(0);
//         }
//         //#endif

//         (cmpt_map[ cmpt_neigh ])->addPixel(  pixel, current_dir );
//         cmpt_image.set( i, j, 0, cmpt_neigh );


//         std::cout << "  Connected Componnent number " << cmpt_neigh << " is now : " << std::endl
//                   << *(cmpt_map[ cmpt_neigh ]) << std::endl;

//       }
//       //Upper Right
//       else  if( (i>0) && (j < (width-1) ) && cmpt_class_image.at( i-1, j+1, 0 ) == current_class )
//       {
//         std::cout << " UPPER RIGHT " << std::endl;

//         unsigned int cmpt_neigh;
//         cmpt_neigh = cmpt_image.at( i-1, j+1, 0 );

//         //#ifdef DEBUG
//         cmpt_map_CIT it = cmpt_map.find( cmpt_neigh );
//         if( it == cmpt_map.end() )
//         {
//           std::cout << " WHAT THE HELL IS GOING ON. KEY NOT FOUND" << std::endl;
//           //#ifdef DEBUG
//           std::cout << "  i = " << i << "  j = " << j << "  current_class = " << current_class << std::endl ;
//           std::cout << "  cmpt_neigh = " << cmpt_neigh << std::endl ;
//           //#endif
//           assert(0);
//         }
//         //#endif


//         (cmpt_map[ cmpt_neigh ])->addPixel(  pixel, current_dir );
//         cmpt_image.set( i, j, 0, cmpt_neigh );


//         std::cout << "  Connected Componnent number " << cmpt_neigh << " is now : " << std::endl
//                   << *(cmpt_map[ cmpt_neigh ]) << std::endl;

//       }
//       else // PIXEL IS ALONE !
//       {
//         conn_cmp_number++;
//         cmpt_map[ conn_cmp_number ] = new mrf::image::ConnCmpt<>();
//         cmpt_map[ conn_cmp_number ]->addPixel( pixel, current_dir );

//         cmpt_image.set( i, j, 0, conn_cmp_number );

//         std::cout << "  NEW COMPONENT  with number : "  << conn_cmp_number << std::endl;

//       }

//     }
//   }

//   //ALL CONNECTED COMPONENTS HAVE BEEN PROCESSED.
//   std::cout << "  cmpt_map.size() = " << cmpt_map.size() << std::endl ;
//   std::cout << " Normalizing directions " << std::endl;
//   cmpt_map_IT an_it = cmpt_map.begin();
//   for( an_it = cmpt_map.begin(); an_it != cmpt_map.end(); ++an_it )
//   {
//     mrf::image::ConnCmpt<> * current_cc = an_it->second;
//     current_cc->normalizeDir();
//   }


//   mrf::image::FloatImage merged_envmap( width, height );
//   cmpt_map_CIT it = cmpt_map.begin();
//   for( it = cmpt_map.begin(); it != cmpt_map.end(); ++it )
//   {
//     mrf::image::ConnCmpt<> const * current_cc = it->second;

//     std::list< mrf::image::_Color3Pixel<float> > const & pixels = current_cc->pixels();
//     std::list< mrf::image::_Color3Pixel<float> >::const_iterator  pixels_cit;

//     mrf::math::Vec3f  const & mean_color = current_cc->meanColor();

//     //For each pixel in the component
//     for( pixels_cit = pixels.begin(); pixels_cit != pixels.end(); ++pixels_cit )
//     {
//       for( unsigned int k = 0 ; k < 3 ; k++ )
//       {
//         merged_envmap.set( (*pixels_cit).x(),
//                            (*pixels_cit).y(),
//                            k,
//                            mean_color[k] );
//       }
//     }
//   }

// #endif
//   //if( ! mrf::image::ImageSaver::instance().save( merged_envmap,"envmap-lights.mfi" ) )
//   {
//     std::cout << " COULD NOT SAVED the envmap computed as hierarchical lights " << std::endl;
//   }

//   std::cout << " CODE MISSING !!!. FINISH ME PLEASE " << std::endl;
//   assert(0);
// }


}   // namespace lighting
}   // namespace mrf
