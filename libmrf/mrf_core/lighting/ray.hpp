/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/ior.hpp>

#include <iostream>

namespace mrf
{
namespace lighting
{
// #ifndef RAYTRACING_EPSILON
// #define RAYTRACING_EPSILON (1000 * EPSILON)
// #endif

#ifndef RAYTRACING_EPSILON
#  define RAYTRACING_EPSILON   (0.0000001)
#  define INTERSECTION_EPSILON RAYTRACING_EPSILON
#  define PERTURBATION_EPSILON RAYTRACING_EPSILON * 10
#endif


template<typename T>
class _Ray
{
public:
  static double const RAY_EPSILON;
  static unsigned int ray_id;

private:
  mrf::math::Vec3f _origin;
  mrf::math::Vec3f _direction;

  T _min_parametric_distance;
  T _max_parametric_distance;

  unsigned int _diff_depth;
  unsigned int _spec_depth;

  mrf::materials::IOR _ior;   //index of refraction of the Ray

  //TODO :
  // This class represents a shadow ray
  // But for shading ray a subclass carrying two supplementary values
  // couble be usefull
  //
  // RADIANCE_ENERGY_SCALAR_TYPE : the type representing the RADIANCE value without
  // the colorimetric information

  // COLOR_TYPE : the color, which can have 3 or more components and it is normalized
  //


  unsigned int _id;

public:
  _Ray()
    : _min_parametric_distance(T(RAY_EPSILON))
    , _max_parametric_distance(INFINITY)
    , _diff_depth(0)
    , _spec_depth(0)
    , _ior(1.0)
    , _id(ray_id)
  {
    ray_id++;
  }

  _Ray(T origin_x, T origin_y, T origin_z, T dir_x, T dir_y, T dir_z);

  _Ray(mrf::math::Vec3f const &origin, mrf::math::Vec3f const &direction);

  _Ray(mrf::math::Vec3f const &origin, mrf::math::Vec3f const &direction, unsigned int an_id);


  /**
   * Directions is assumed to be normalized
   **/
  _Ray(mrf::math::Vec3f const &origin, mrf::math::Vec3f const &direction, float tmin, float tmax);


  _Ray(
      mrf::math::Vec3f const &origin,
      mrf::math::Vec3f const &direction,
      unsigned int            diff_depth,
      unsigned int            spec_depth);


  _Ray(_Ray const &aray);
  ~_Ray();


  _Ray &operator=(_Ray const &a_ray);

  inline void setOrigin(mrf::math::Vec3f const &new_origin);
  inline void setOrigin(float x, float y, float z);

  inline void setDir(mrf::math::Vec3f const &new_dir);
  inline void setValidDistances(T tmin, T tmax);

  inline mrf::math::Vec3f const &origin() const;
  inline mrf::math::Vec3f const &dir() const;

  inline void validDistances(T &tmin, T &tmax) const;

  inline T tMin() const;
  inline T tMax() const;


  inline void tMin(T min);
  inline void tMax(T max);

  inline unsigned int diffDepth() const;
  inline unsigned int specDepth() const;

  inline void diffDepth(unsigned int diffDepth);
  inline void specDepth(unsigned int specDepth);

  inline mrf::materials::IOR_template<T> ior() const;
  inline void                            ior(mrf::materials::IOR_template<T> const &ior);

  inline void incDiffDepth();
  inline void incSpecDepth();

  inline unsigned int id() const;
  inline void         id(unsigned int an_id);
};

typedef _Ray<float>  Ray;
typedef _Ray<double> RayD;

template<typename T>
double const _Ray<T>::RAY_EPSILON = DBL_EPSILON;

template<typename T>
unsigned int _Ray<T>::ray_id = 1;


template<typename T>
inline void _Ray<T>::setOrigin(mrf::math::Vec3f const &new_origin)
{
  _origin = new_origin;
}

template<typename T>
inline void _Ray<T>::setOrigin(float x, float y, float z)
{
  _origin.set(x, y, z);
}

template<typename T>
inline void _Ray<T>::setDir(mrf::math::Vec3f const &new_dir)
{
  _direction = new_dir;
}

template<typename T>
inline mrf::math::Vec3f const &_Ray<T>::origin() const
{
  return _origin;
}

template<typename T>
inline mrf::math::Vec3f const &_Ray<T>::dir() const
{
  return _direction;
}

template<typename T>
inline void _Ray<T>::setValidDistances(T tmin, T tmax)
{
  _min_parametric_distance = tmin;
  _max_parametric_distance = tmax;
}

template<typename T>
inline void _Ray<T>::validDistances(T &tmin, T &tmax) const
{
  tmin = _min_parametric_distance;
  tmax = _max_parametric_distance;
}

template<typename T>
inline T _Ray<T>::tMin() const
{
  return _min_parametric_distance;
}

template<typename T>
inline T _Ray<T>::tMax() const
{
  return _max_parametric_distance;
}

template<typename T>
inline void _Ray<T>::tMin(T min)
{
  _min_parametric_distance = min;
}

template<typename T>
inline void _Ray<T>::tMax(T max)
{
  _max_parametric_distance = max;
}



template<typename T>
inline unsigned int _Ray<T>::diffDepth() const
{
  return _diff_depth;
}

template<typename T>
inline unsigned int _Ray<T>::specDepth() const
{
  return _spec_depth;
}

template<typename T>
inline void _Ray<T>::diffDepth(unsigned int diffDepth)
{
  _diff_depth = diffDepth;
}

template<typename T>
inline void _Ray<T>::specDepth(unsigned int specDepth)
{
  _spec_depth = specDepth;
}

template<typename T>
inline mrf::materials::IOR_template<T> _Ray<T>::ior() const
{
  return _ior;
}

template<typename T>
inline void _Ray<T>::ior(mrf::materials::IOR_template<T> const &ior)
{
  _ior = ior;
}

template<typename T>
inline void _Ray<T>::incDiffDepth()
{
  _diff_depth++;
}

template<typename T>
inline void _Ray<T>::incSpecDepth()
{
  _spec_depth++;
}

template<typename T>
inline unsigned int _Ray<T>::id() const
{
  return _id;
}

template<typename T>
inline void _Ray<T>::id(unsigned int an_id)
{
  _id = an_id;
}


#include "ray.cxx"

//External Operator
template<typename T>
inline std::ostream &operator<<(std::ostream &os, _Ray<T> const &ray)
{
  return os << " Ray [ origin = " << ray.origin() << " dir = " << ray.dir() << " tmin = " << ray.tMin()
            << " tmax = " << ray.tMax() << " spec depth = " << ray.specDepth() << " diff depth = " << ray.diffDepth()
            << " id =  " << ray.id() << " ] " << std::endl;
}

}   // namespace lighting

}   // namespace mrf
