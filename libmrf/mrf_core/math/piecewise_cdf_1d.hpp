/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/integral_1d.hpp>

#include <vector>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <cstdlib>

namespace mrf
{
namespace math
{
/**
 * @brief      Represents a 1D Piecewise CDF
 *
 * @tparam     T1    Precision of the storage
 */
template<typename T1 = float>
class PiecewiseCDF1D
{
protected:
  /* CDF values */
  std::vector<T1> _cdf;   // _cdf has  (_pdf.size() + 1) values
  std::vector<T1> _pdf;

  /* Position for which the CDF is defined */
  std::vector<T1> _x_values;


public:
  PiecewiseCDF1D();

  template<typename T2>
  PiecewiseCDF1D(std::vector<T2> const &x_values, std::vector<T2> const &y_values);



  /**
   * @brief      Return the size  Piecewise 1D CDF
   *
   * @return     Return the size (i.e., the number of values) of the Piecewise 1D CDF
   */
  inline unsigned int size() const;


  inline T1 valueAt(unsigned int index) const;

  /**
   * Synomym of valueAt method
   */
  inline T1 operator()(unsigned int index) const;

  inline std::vector<T1> const &x() const;

  /**
   * Synonym of values().
   */
  inline std::vector<T1> const &values() const;
  inline std::vector<T1> const &cdf() const;
  inline std::vector<T1> const &pdf() const;


  template<typename T2>
  inline T1 inverse(T2 inv_x_value) const;

  template<typename T2>
  inline T1 inverse(T2 inv_x_value, T2 &pdf_at_x_value) const;
};


template<typename T1>
PiecewiseCDF1D<T1>::PiecewiseCDF1D()
{}

template<typename T1>
template<typename T2>
PiecewiseCDF1D<T1>::PiecewiseCDF1D(std::vector<T2> const &x_values, std::vector<T2> const &y_values)
{
  // Precondition on the input arguments
  assert(x_values.size() == y_values.size());

  _cdf.resize(y_values.size() + 1);


  _pdf.resize(y_values.size());

  _x_values.resize(x_values.size());


  _cdf[0] = T1(0.0);
  //_x_values[0] = static_cast<T1>(x_values[0]);

  /*
  T1 const PDF_NORMALIZATION_FACTOR = static_cast<T1>( Integral1D::trapz( x_values, y_values) );

  //std::cout << " PDF_NORMALIZATION_FACTOR = " << PDF_NORMALIZATION_FACTOR << std::endl;
  //double const INTEGRATION_FACTOR =  (x_values[x_values.size()-1] - x_values[0] ) / (x_values.size()-1);

  for( unsigned int i=0; i < y_values.size(); i++)
  {
    _x_values[i] = static_cast<T1>( x_values[i] );
    _pdf[i] = y_values[i] / PDF_NORMALIZATION_FACTOR;

    // std::cout << "  _x_values[" <<  i << "] = " << _x_values[i]
    //           << " y_values[" << i << "] = " << y_values[i]
    //           << " _pdf[" << i << "] = " << _pdf[i]
    //           << std::endl;
  }
  _cdf[_cdf.size() - 1] = T1(1.0);
  //std::cout <<  " Integral of the PDF:" << Integral1D::trapz( x_values, _pdf) << std::endl;


  */

  for (unsigned int i = 1; i <= y_values.size(); i++)
  {
    _x_values[i - 1] = static_cast<T1>(x_values[i - 1]);
    _cdf[i]          = _cdf[i - 1] + y_values[i - 1] / float(y_values.size());
  }




  float func_integral = _cdf[_cdf.size() - 1];

  if (func_integral == 0.f)
  {
    for (unsigned int i = 1; i < y_values.size(); i++)
    {
      _cdf[i] = float(i) / float(y_values.size());
      _pdf[i] = 0.f;
    }
    _cdf[y_values.size()] = 1.f;
  }
  else
  {
    //_pdf[0] = y_values[0] / (func_integral*y_values.size());
    _pdf[0] = y_values[0] / (func_integral);
    for (unsigned int i = 1; i < y_values.size(); i++)
    {
      _cdf[i] /= func_integral;
      _pdf[i] = y_values[i] / (func_integral);
      //_pdf[i] = y_values[i] / (func_integral*y_values.size());
    }
    _cdf[y_values.size()] = 1.f;
  }
}




template<typename T1>
inline unsigned int PiecewiseCDF1D<T1>::size() const
{
  return _cdf.size();
}

template<typename T1>
inline T1 PiecewiseCDF1D<T1>::valueAt(unsigned int index) const
{
  assert(index < _cdf.size());
  return _cdf[index];
}


template<typename T1>
inline T1 PiecewiseCDF1D<T1>::operator()(unsigned int index) const
{
  return valueAt(index);
}

template<typename T1>
inline std::vector<T1> const &PiecewiseCDF1D<T1>::x() const
{
  return _x_values;
}

template<typename T1>
inline std::vector<T1> const &PiecewiseCDF1D<T1>::values() const
{
  return _cdf;
}

template<typename T1>
std::vector<T1> const &PiecewiseCDF1D<T1>::cdf() const
{
  return _cdf;
}

template<typename T1>
std::vector<T1> const &PiecewiseCDF1D<T1>::pdf() const
{
  return _pdf;
}



template<typename T1>
template<typename T2>
inline T1 PiecewiseCDF1D<T1>::inverse(T2 inv_x_value) const
{
  assert(inv_x_value <= T1(1.0));
  assert(inv_x_value >= T1(0.0));

  auto first_elem = std::lower_bound(_cdf.begin(), _cdf.end(), inv_x_value);

  if (first_elem != _cdf.end())
  {
    // Max to avoid -1 when inv_x_value  = 0.0
    int const index_element = std::max(0, static_cast<int>(std::distance(_cdf.begin(), first_elem) - 1));

    if (index_element >= _x_values.size() - 1)
    {
      return _x_values[_x_values.size() - 1];
    }

    T1 const weight         = (inv_x_value - _cdf[index_element]) / (_cdf[index_element + 1] - _cdf[index_element]);
    T1 const interpolated_x = _x_values[index_element] * (T1(1.0) - weight) + _x_values[index_element + 1] * weight;
    return interpolated_x;
  }

  throw(" ERROR. OUT OF BOUNDARIES while accessing CDF::inverse Method ");
}


template<typename T1>
template<typename T2>
inline T1 PiecewiseCDF1D<T1>::inverse(T2 inv_x_value, T2 &pdf_at_x_value) const
{
  assert(inv_x_value <= T1(1.0));
  assert(inv_x_value >= T1(0.0));

  auto first_elem = std::lower_bound(_cdf.begin(), _cdf.end(), inv_x_value);


  if (first_elem != _cdf.end())
  {
    // Max to avoid -1 when inv_x_value  = 0.0
    int const index_element = std::max(0, static_cast<int>(std::distance(_cdf.begin(), first_elem) - 1));

    //Faster:
    // std::cout << std::endl
    //           << " ############################################################################################ " << std::endl;
    // std::cout << " inv_x_value = " << inv_x_value << std::endl;
    // std::cout << " orignal index_element = " << static_cast<int>( std::distance( _cdf.begin(), first_elem ) ) << std::endl;
    // std::cout << " index_element = " << index_element << std::endl;
    // std::cout << " *first_element = " << *first_elem << std::endl;
    // std::cout << " _xvalues " << _x_values[ index_element ] << std::endl;
    // std::cout << " _pdf value " << _pdf[ index_element ] << std::endl;
    // std::cout << " _x_values.size() - 1 = " << _x_values.size() -1 << std::endl;

    //Case we are at the end. Forcing the answer
    if (index_element >= static_cast<int>(_x_values.size() - 1))
    {
      pdf_at_x_value = _pdf[_x_values.size() - 1];
      return _x_values[_x_values.size() - 1];
    }

    T1 const weight         = (inv_x_value - _cdf[index_element]) / (_cdf[index_element + 1] - _cdf[index_element]);
    T1 const interpolated_x = _x_values[index_element] * (T1(1.0) - weight) + _x_values[index_element + 1] * weight;

    pdf_at_x_value = _pdf[index_element] * (T1(1.0) - weight) + _pdf[index_element + 1] * weight;

    return interpolated_x;
  }


  throw(" ERROR. OUT OF BOUNDARIES while accessing CDF::inverse Method ");
}


/**
 * External Operator to display the PiecewiseCDF on the console
 */
template<typename T1>
std::ostream &operator<<(std::ostream &ios, PiecewiseCDF1D<T1> const &a_cdf)
{
  std::vector<T1> const &x_values   = a_cdf.x();
  std::vector<T1> const &cdf_values = a_cdf.values();
  std::vector<T1> const &pdf_values = a_cdf.pdf();

  ios << " # PiecewiseCDF with " << x_values.size()
      << " Sample Positions. Format per row : x  cdf_value_at_x pdf_value_at_x" << std::endl;
  for (int i = 0; i < x_values.size(); ++i)
  {
    ios << x_values[i] << " " << cdf_values[i] << " " << pdf_values[i] << std::endl;
  }
  return ios;
}


// External Operator to display the PiecewiseCDF on the console



}   // namespace math
}   // namespace mrf
