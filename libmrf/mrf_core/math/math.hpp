﻿/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/


#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//STL
#include <climits>
#include <cfloat>
#include <cmath>
#include <limits>

//MRF
#include <mrf_core/mrf_cuda_support.hpp>

#include <mrf_core/math/vec2.hpp>
#include <mrf_core/math/vec3.hpp>
#include <mrf_core/math/vec4.hpp>

#include <mrf_core/math/mat2.hpp>
#include <mrf_core/math/mat3.hpp>
#include <mrf_core/math/mat4.hpp>

#include <mrf_core/math/quat.hpp>
#include <mrf_core/math/ref.hpp>

#ifdef _WIN32
#  undef min
#  undef max
//to avoid warning due to multiple definition of NOMINMAX
#  ifndef NOMINMAX
#    define NOMINMAX
#  endif
#endif

namespace mrf
{
namespace math
{
//Some constants
#ifndef INFINITY
#  define INFINITY (std::numeric_limits<float>::max())
#endif

#ifndef EPSILON
#  define EPSILON (10.0f * std::numeric_limits<float>::epsilon())
#endif



#ifndef DBL_EPSILON
#  define DBL_EPSILON (2 * std::numeric_limits<double>::epsilon())
#endif

#ifndef LONG_DBL_EPSILON
#  define LONG_DBL_EPSILON (2 * std::numeric_limits<long double>::epsilon())
#endif




template<typename T>
class _Math
{
public:
  static double const PI;
  static double const PI_2;       // pi /2
  static double const PI_4;       // pi / 4
  static double const TWO_PI;     // 2 * pi
  static double const FOUR_PI;    // 4* pi
  static double const EIGHT_PI;   // 8 * pi
  static double const INV_PI;     // 1 / pi
  static double const INV_2PI;
  static double const INV_4PI;
  static double const INV_8PI;   // 1 / 8*pi

  static double const TWO_INV_PI;          // = 2 / Pi
  static double const SQUARE_PI;           // Pi^{2}
  static double const FOUR_SQUARE_PI;      // 4 * Pi^{2}
  static double const SIXTEEN_SQUARE_PI;   // 16 * Pi^{2}
  static double const CUBIC_PI;            // Pi^{3}
  static double const SQRT_PI;             // sqrt( PI )
  static double const SQRT_TWO_INV_PI;     // sqrt( 2 / PI );


  static double const E;
  static double const DEG_TO_RAD;
  static double const PI_OVER_180;
  static double const RAD_TO_DEG;

  inline static T epsilon();
  inline static T max(T v1, T v2);
  inline static T max(T v1, T v2, T v3);

  /**
   * Set  the max
   * Returns which of the three numbers is the max
   **/
  inline static unsigned int max(T v1, T v2, T v3, T &max);

  inline static T min(T v1, T v2);
  inline static T min(T v1, T v2, T v3);

  inline static float mean(float v1, float v2);
  inline static float mean(float v1, float v2, float v3);

  inline static double mean(double v1, double v2);
  inline static double mean(double v1, double v2, double v3);

  inline static float  round(float v1);
  inline static double round(double v1);

  /**
   * Computes a+t(b−a), i.e. the linear interpolation between a and b for the
   * parameter t in [0, 1].
   */
  inline static T lerp(T a, T b, T t);

  /**
   * Get parameter t in [0, 1] from range [x0, x1].
   * Used for interpolation.
   */
  inline static T alpha(T x0, T x1, T x);


  inline static T interp(T x, T x0, T x1, T y0, T y1)
  {
    return mrf::math::_Math<T>::lerp(y0, y1, mrf::math::_Math<T>::alpha(static_cast<T>(x0), static_cast<T>(x1), x));
  }

  /**
   * Returns
   * -1 if a < 0
   * 1
   *
   * @param a : the given number which sign is tested
   * @return the -1 1
   *
   * @see sign0( T a)
   **/
  static inline T sign(T a);

  /**
   * Returns
   * -1 if a < 0
   * 1  if a > 0
   * 0 if a == 0
   *
   * @param a : the given number which sign is tested
   * @return the -1, 0 or 1
   *
   * @see sign( T a)
   **/
  static inline T sign0(T a);


  inline static T toDegree(T a_radian_angle);
  inline static T toRadian(T a_degree_angle);

  /**
   * Return the spherical  coordinates
   * for a given cartesian coordinates
   *
   * @param x : the given cartesian abscissa coordinate
   * @param y : the given cartesian ordinate coordinate
   * @param z : the given cartesian height coordinate
   *
   * @param phi : the returned azimutal angle
   * @param theta: the returned elevation angle
   * @param r : the returned radius
   *
   **/
  inline static void sphericalCoord(T x, T y, T z, T &phi, T &theta, T &r);

  /**
   * Return the cartesian coordinates
   * for a given spherical coordinates
   *
   *@param phi : the given azimutal angle
   *@param theta: the given elevation angle
   *@param r : the given radius
   *
   *@param x : the returned cartesian abscissa coordinate
   *@param y : the returned cartesian ordinate coordinate
   *@param z : the returned cartesian height coordinate
   **/
  inline static void cartesianCoord(T phi, T theta, T r, T &x, T &y, T &z);

  //             inline static int clamp( int val, int min , int max );
  //             inline static float clamp( float val, float min, float max);
  //             inline static float clamp( float val, float min, float max);



  //This is the greatest integer < value
  inline static int intPart(float value);
  inline static int intPart(double value);

  //The fractional part of value
  inline static float  fracPart(float value);
  inline static double fracPart(double value);


  static bool intervalOverlap(
      T a,
      T b,   // First interval [a,b]
      T c,
      T d);   // Second interval [c,d]

  static bool intervalOverlap(
      T  a,
      T  b,   // First interval [a,b]
      T  c,
      T  d,   // Second interval [c,d]
      T &t1,
      T &t2);   // Intersection interval [t1,t2]

  template<typename T1>
  static inline void swap(T1 &a, T1 &b);

  static inline T absVal(T a);

  static inline T sqrt(T a);
  static inline T cos(T a);
  static inline T sin(T a);
  static inline T atan2(T a, T b);


  static inline bool is_not_a_number(T a_number)
  {
#if __cplusplus >= 201103L
    return !std::isfinite(a_number);
#else
    std::cerr << " THIS FUNCTION NEEDS C++ 2011 support at the moment behavior is wrong" << std::endl;
    std::cerr << __FILE__ << " at  " << __LINE__ << std::endl;
    assert(0);
    return false;
#endif
  }

  static inline bool is_not_a_number(Vec3<T> const &a_vec)
  {
    return is_not_a_number(a_vec[0]) || is_not_a_number(a_vec[1]) || is_not_a_number(a_vec[2]);
  }


};   //end of Math Class


typedef _Math<float>        Math;
typedef _Math<double>       MathD;
typedef _Math<int>          MathI;
typedef _Math<unsigned int> MathUI;

template<typename T>
double const _Math<T>::PI = 3.1415926535897932384626433832795;

template<typename T>
double const _Math<T>::PI_2 = 1.5707963267948966192313216916397514421;

template<typename T>
double const _Math<T>::PI_4 = 0.7853981633974482789994908671360462904;

template<typename T>
double const _Math<T>::TWO_PI = 6.283185307179586476925286766559;

template<typename T>
double const _Math<T>::FOUR_PI = 12.566370614359172953850573533118011537;

template<typename T>
double const _Math<T>::EIGHT_PI = 25.132741228718344927983707748353481293;


template<typename T>
double const _Math<T>::INV_PI = 0.31830988618379068394603385008604030832;


template<typename T>
double const _Math<T>::INV_2PI = 0.15915494309189533576888376337251436203;

template<typename T>
double const _Math<T>::INV_4PI = 0.079577471545947667884441881686257181017;

template<typename T>
double const _Math<T>::INV_8PI = 0.03978873577297383549325423126075503854;



template<typename T>
double const _Math<T>::TWO_INV_PI = 0.63661977236758134307553505349006;

template<typename T>
double const _Math<T>::CUBIC_PI = 31.006276680299820175476315067101395202;

template<typename T>
double const _Math<T>::SQUARE_PI = 9.8696044010893586188344909998761511353;

template<typename T>
double const _Math<T>::FOUR_SQUARE_PI = 4.0 * SQUARE_PI;

template<typename T>
double const _Math<T>::SIXTEEN_SQUARE_PI = 16.0 * SQUARE_PI;

template<typename T>
double const _Math<T>::SQRT_PI = std::sqrt(PI);

template<typename T>
double const _Math<T>::SQRT_TWO_INV_PI = std::sqrt(TWO_INV_PI);

template<typename T>
double const _Math<T>::E = 2.7182818284590452353602874713527;

template<typename T>
double const _Math<T>::DEG_TO_RAD = 0.017453292519943295769236907684886;

template<typename T>
double const _Math<T>::PI_OVER_180 = DEG_TO_RAD;

template<typename T>
double const _Math<T>::RAD_TO_DEG = 57.295779513082320876798154814105;


//Methods of Math Class
template<typename T>
inline T _Math<T>::epsilon()
{
  return std::numeric_limits<T>::epsilon();
}


template<typename T>
inline T _Math<T>::max(T v1, T v2)
{
  return ((v1 > v2) ? (v1) : (v2));
}

template<typename T>
inline T _Math<T>::max(T v1, T v2, T v3)
{
  return _Math<T>::max(_Math<T>::max(v1, v2), v3);
}

template<typename T>
inline unsigned int _Math<T>::max(T v1, T v2, T v3, T &max)
{
  if (v1 > v2)
  {
    if (v1 > v3)
    {
      max = v1;
      return 0;
    }
    else
    {
      max = v3;
      return 2;
    }
  }
  else
  {
    if (v2 > v3)
    {
      max = v2;
      return 1;
    }
    else
    {
      max = v3;
      return 2;
    }
  }
}

template<typename T>
inline T _Math<T>::min(T v1, T v2)
{
  return ((v1 < v2) ? (v1) : (v2));
}

template<typename T>
inline T _Math<T>::min(T v1, T v2, T v3)
{
  return min(v1, min(v2, v3));
}


template<typename T>
inline float _Math<T>::mean(float v1, float v2)
{
  return (v1 + v2) / 2.0f;
}

template<typename T>
inline float _Math<T>::mean(float v1, float v2, float v3)
{
  return (v1 + v2 + v3) / 3.0f;
}

template<typename T>
inline double _Math<T>::mean(double v1, double v2)
{
  return (v1 + v2) / 2.0;
}

template<typename T>
inline double _Math<T>::mean(double v1, double v2, double v3)
{
  return (v1 + v2 + v3) / 3.0;
}

template<typename T>
inline float _Math<T>::round(float v1)
{
  return floor(float(v1 + 0.5));
}

template<typename T>
inline double _Math<T>::round(double v1)
{
  return floor(double(v1 + 0.5));
}

template<typename T>
inline T _Math<T>::lerp(T a, T b, T t)
{
  return a + t * (b - a);
}

template<typename T>
inline T _Math<T>::alpha(T x0, T x1, T x)
{
  return (x - x0) / (x1 - x0);
}


template<typename T>
inline T _Math<T>::sign(T a)
{
  if (a < T(0))
  {
    return T(-1);
  }
  else
  {
    return T(1);
  }
}

template<typename T>
inline T _Math<T>::sign0(T a)
{
  if (a < -EPSILON)
  {
    return T(-1);
  }
  else if (a > EPSILON)
  {
    return T(1);
  }
  else
  {
    return T(0);
  }
}



template<typename T>
inline T _Math<T>::toDegree(T a_radian_angle)
{
  return static_cast<T>(a_radian_angle * RAD_TO_DEG);
  //180 / M_PI;
}

template<typename T>
inline T _Math<T>::toRadian(T a_degree_angle)
{
  return static_cast<T>(a_degree_angle * DEG_TO_RAD);
  //M_PI / 180.0f;
}


template<typename T>
inline void _Math<T>::sphericalCoord(T x, T y, T z, T &phi, T &theta, T &r)
{
  r = std::sqrt(x * x + y * y + z * z);

  T denom = x * x + z * z;

  if (denom != 0)
  {
    phi = toDegree(std::acos(z / std::sqrt(denom)));
    if (x < 0)
    {
      phi = 360 - phi;
    }
  }
  else
  {
    phi = 0;
  }

  theta = toDegree(std::acos(y / r));
}

template<typename T>
inline void _Math<T>::cartesianCoord(T phi, T theta, T r, T &x, T &y, T &z)
{
  //TODO : Check me
  //Strange, does not fit with the inverse function below!
  x = r * cos(phi) * sin(theta);
  y = r * sin(phi) * sin(theta);
  z = r * cos(theta);
}

//This is the greatest integer < value
template<typename T>
inline int _Math<T>::intPart(float value)
{
  return static_cast<int>(floorf(value));
}

template<typename T>
inline int _Math<T>::intPart(double value)
{
  return static_cast<int>(floor(value));
}

//The fractional part of value
template<typename T>
inline float _Math<T>::fracPart(float value)
{
  return value - intPart(value);
}

template<typename T>
inline double _Math<T>::fracPart(double value)
{
  return value - intPart(value);
}

template<typename T>
bool _Math<T>::intervalOverlap(
    T a,
    T b,   // First interval [a,b]
    T c,
    T d)   // Second interval [c,d]
{
  return ((a > d) || (c > b)) ? (false) : (true);
}

template<typename T>
bool _Math<T>::intervalOverlap(T a, T b, T c, T d, T &t1, T &t2)
{
  if ((a > d) || (c > b))
  {
    return false;
  }
  else   // (a < d) && (c < b)
  {
    if (c > a)
    {
      if (d < b)
      {
        t1 = c;
        t2 = d;
      }
      else
      {
        t1 = c;
        t2 = b;
      }
    }
    else
    {
      if (d < b)
      {
        t1 = a;
        t2 = d;
      }
      else
      {
        t1 = a;
        t2 = b;
      }
    }

    return true;
  }
}   //end of interalOverlap


template<typename T>
template<typename T1>
inline void _Math<T>::swap(T1 &a, T1 &b)
{
  T1 tmp = b;
  b      = a;
  a      = tmp;
}

template<typename T>
inline T _Math<T>::absVal(T a)
{
  return (a < 0) ? (-a) : (a);
}

template<typename T>
inline T _Math<T>::sqrt(T a)
{
  return std::sqrt(a);
}

template<typename T>
inline T _Math<T>::cos(T a)
{
  return std::cos(a);
}

template<typename T>
inline T _Math<T>::sin(T a)
{
  return std::sin(a);
}

template<typename T>
inline T _Math<T>::atan2(T a, T b)
{
  return std::atan2(a, b);
}




//-------------------------------------------------------------------------
// STATIC FUNCTIONS
//-------------------------------------------------------------------------

//
//
//These are utility methods to convert Vector from LAIR to MRF format
template<class T>
inline Vec3<T> Vec4ToVec3(const Vec4<T> &_v)
{
  return Vec3<T>(_v[0], _v[1], _v[2]);
}

template<class T>
inline Vec4<T> Vec3ToVec4(const Vec3<T> &_v, const T &_w)
{
  return Vec4<T>(_v[0], _v[1], _v[2], _w);
}


// Utility functions that are portable between MacOS X, Win32 and Unices
template<class T>
static inline bool isnan(T value)
{
#ifdef _WIN32
  return _isnan(value);
#else
#  if __cplusplus >= 201103L
  return std::isnan(value);
#  else
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  return isnan(value);
#  endif
#endif
}

template<class T>
static inline bool isinf(T value)
{
#ifdef _WIN32
  return (_finite(value) == 0);
#else
#  if (__cplusplus <= 201103L)
  return std::isinf(value);
#  else
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  return isinf(value);
#  endif
#endif
}

template<typename T>
static inline T clamp(T val, T min, T max)
{
  if (val < min)
  {
    return min;
  }
  else if (val > max)
  {
    return max;
  }
  else
  {
    return val;
  }
}


//-------------------------------------------------------------------------
// Function to Solve Quadric Equations
//-------------------------------------------------------------------------

/**
 * Solve a given quadratic equation whose form is simple.
 * Simple Form : t^2 + B*t + C where (B,C) are reals
 *
 *
 *
 * @param t0 : the first solution of the quadratic form
 * @param t1 : the second solution of the quadratic form
 *
 * Note that the returned values are such that : t0 < t1
 *
 * @return true if the quadratic has real root solution and false
 * if the solutions are complex.
 *
 **/
template<typename T1>
static inline bool solveQuadratic(T1 const &B, T1 const &C, T1 &t0, T1 &t1)
{
  T1 const discr = B * B - 4 * C;

  if (discr < 0.0)
  {
    return false;
  }

  T1 const root_discr = sqrt(discr);
  T1 const half       = T1(-0.5);

  T1 q;
  if (B < 0.0)
  {
    q = half * (B - root_discr);
  }
  else
  {
    q = half * (B + root_discr);
  }


  t0 = q;
  t1 = C / q;

  if (t0 > t1)
  {
    _Math<T1>::swap(t0, t1);
  }

  return true;
}


/**
 * Solve a given quadratic equation whose form is general
 *
 * General Form : A*t^2 + B*t + C where (A,B,C) are reals
 *
 **/
template<typename T1>
static inline bool solveQuadratic(T1 A, T1 B, T1 C, T1 &t0, T1 &t1)
{
  T1 const discr = B * B - 4 * A * C;

  if (discr < DBL_EPSILON)
  {
    return false;
  }

  T1 const root_discr = sqrt(discr);
  T1 const half       = T1(-0.5);

  T1 q;
  if (B < DBL_EPSILON)
  {
    q = half * (B - root_discr);
  }
  else
  {
    q = half * (B + root_discr);
  }


  t0 = q / A;
  t1 = C / q;

  if (t0 > t1)
  {
    swap(t0, t1);
  }

  return true;
}


/**
 *
 * Returns the two spherical angles (in radian)  for a given direction
 * Direction here means that x^2 + y^2 + z^2 = 1
 *
 * All angles are returned in radians  with
 *
 * Theta is in [0, Pi]
 * Phi is in [0,2 Pi]
 *
 * theta = 0 when the direction (x,y,z) is colinear with the Y axis
 * in this case phi is undefined.
 *
 * theta = Pi / 2 means that the direction lies the in XZ plane
 *
 *
 * @param theta : represents the elevation  measured from the Y axis
 * @param phi :  represents the azimuth angle measured in the XZ plane
 **/
template<typename T1>
static inline void sphCoordNR(T1 x, T1 y, T1 z, T1 &theta, T1 &phi)
{
  theta = std::acos(y);

  if (theta < T1(EPSILON))
  {
    phi = T1(0.0);
  }
  else
  {
    phi = std::atan2(z, x);

    phi += T1(Math::PI);
  }
}


/**
 * Same as previous function except that the vector (x,y,z) is not
 * assumed to ne normalized
 *
 * Warning : this function does not return the radius of the vector
 * @see sphCCoordR
 **/
template<typename T1>
static inline void sphCoordR(T1 x, T1 y, T1 z, T1 &theta, T1 &phi)
{
  T1 const sqr_radius = x * x + y * y + z * z;
  theta               = std::acos(y / std::sqrt(sqr_radius));
  phi                 = std::atan2(z, x) + Math::PI;
}

/**
 * Same as previous function except that the radius of the given
 * vector is also returned
 *
 **/
template<typename T1>
static inline void sphCCoordR(T1 x, T1 y, T1 z, T1 &theta, T1 &phi, T1 &radius)
{
  T1 const sqr_radius = x * x + y * y + z * z;
  radius              = std::sqrt(sqr_radius);
  theta               = std::acos(y / radius);
  phi                 = std::atan2(z, x) + Math::PI;
}


/**
 * Computes Cartesian Coordinates of a given spherical direction
 *
 * It is assumed that x*x + y*y + z*z = 1  to have a proper direction
 *
 * phi is in [0,2Pi]
 * theta is in [0,Pi]
 *
 * IMPORTANT : Notice that the theta angle is measured from the Y axis
 **/
template<typename T1>
static inline void cartesianCoordN(T1 phi, T1 theta, T1 &x, T1 &y, T1 &z)
{
  //#ifdef DEBUG
  //std::cout << "  sin(theta) = " << sin(theta) << std::endl ;
  //#endif
  y = std::cos(theta);
  x = std::sin(phi) * std::sin(theta);
  z = std::cos(phi) * std::sin(theta);
  //#ifdef DEBUG
  //std::cout << "  x = " << x << "  y = " << y << "  z = " << z << std::endl ;
  //#endif
}

template<typename T1>
static inline void cartesianCoordN(T1 phi, T1 theta, mrf::math::Vec3<T1> &direction)
{
  cartesianCoordN(phi, theta, direction[0], direction[1], direction[2]);
}




/**
 * Inverse Paraboloidal Mapping (Front FACE)
 * [ u ,v ] in [-1,1] ===> [dx, dy,dz] representing a direction
 **/
template<typename T1>
static inline mrf::math::Vec3<T1> inverseFrontParaboloidalMapping(mrf::math::Vec2<T1> const &uvs)
{
  T1 const uvs_sqrlength = uvs.sqrLength();
  T1 const denom         = 1.0f / (uvs_sqrlength + 1);

  return mrf::math::Vec3<T1>(2 * uvs[0] * denom, 2 * uvs[1] * denom, (uvs_sqrlength - 1) * denom);
}

/**
 * Inverse Paraboloidal Mapping (Back FACE)
 * [ u ,v ] in [-1,1] ===> [dx, dy,dz] representing a direction
 **/
template<typename T1>
static inline mrf::math::Vec3<T1> inverseBackParaboloidalMapping(mrf::math::Vec2<T1> const &uvs)
{
  T1 const uvs_sqrlength = uvs.sqrLength();
  T1 const denom         = 1.0f / (uvs_sqrlength + 1);

  return mrf::math::Vec3<T1>(-2 * uvs[0] * denom, -2 * uvs[1] * denom, (-uvs_sqrlength + 1) * denom);
}

/**
 * Return the square of a given value
 **/
template<typename T1>
static inline T1 sqr(T1 v1)
{
  return v1 * v1;
}




//------------------------------------------------------------------------------
//!
template<class T>
static inline Vec3<T> rotateX(const Vec3<T> &v, const T &angle)
{
  T halfAngle = (T)(0.5) * angle;
  T sina      = (T)(std::sin(halfAngle));
  T cosa      = (T)(std::cos(halfAngle));

  // calculate coefficients
  T x2 = sina + sina;
  T xx = sina * x2;
  T wx = cosa * x2;

  return Vec3<T>(v(0), v(1) * (1 - xx) - v(2) * wx, v(1) * wx + v(2) * (1 - xx));
}


//------------------------------------------------------------------------------
//!
template<class T>
static inline Vec3<T> rotateY(const Vec3<T> &v, const T &angle)
{
  T halfAngle = (T)(0.5) * angle;
  T sina      = (T)(std::sin(halfAngle));
  T cosa      = (T)(std::cos(halfAngle));

  // calculate coefficients
  T y2 = sina + sina;
  T yy = sina * y2;
  T wy = cosa * y2;

  return Vec3<T>(v(0) * (1 - yy) + v(2) * wy, v(1), v(2) * (1 - yy) - v(0) * wy);
}


//------------------------------------------------------------------------------
//!
template<class T>
static inline Vec3<T> rotateZ(const Vec3<T> &v, const T &angle)
{
  T halfAngle = (T)(0.5) * angle;
  T sina      = (T)(std::sin(halfAngle));
  T cosa      = (T)(std::cos(halfAngle));

  // calculate coefficients.
  T z2 = sina + sina;
  T zz = sina * z2;
  T wz = cosa * z2;

  return Vec3<T>(v(0) * (1 - zz) - v(1) * wz, v(0) * wz + v(1) * (1 - zz), v(2));
}



// > pour une triangle spherique de trois directlion (d0 d1 d2)
// >
// > float a = d0*d1;
// > float b = d0*d2;
// > float c = d1*d2;
// >
// > float area = (acos((c-a*b)/sqrt((1-a*a)*(1-b*b)))+
// >           acos((b-a*c)/sqrt((1-a*a)*(1-c*c)))+
// >          acos((a-b*c)/sqrt((1-c*c)*(1-b*b)))
// >          -M_PI);
// >


//-------------------------------------------------------------------------
//!
// Returns the area of a spherical triangle given by its three directions
template<class T>
static inline T sphericalTriangleArea(Vec3<T> const &v0, Vec3<T> const &v1, Vec3<T> const &v2)
{
  T const a = Vec3<T>::dot(v0, v1);
  T const b = Vec3<T>::dot(v0, v2);
  T const c = Vec3<T>::dot(v1, v2);

  T const area
      = (std::acos((c - a * b) / std::sqrt((1 - a * a) * (1 - b * b)))
         + std::acos((b - a * c) / std::sqrt((1 - a * a) * (1 - c * c)))
         + std::acos((a - b * c) / std::sqrt((1 - c * c) * (1 - b * b))) - _Math<T>::PI);



  return area;
}

/**
 * Computes the half vector between two vectors.
 * When the vectors are opposite the vector (0,1,0) is returned
 **/
template<typename T>
static inline Vec3<T> halfVector(Vec3<T> const &in, Vec3<T> const &out)
{
  // Compute half vector.
  mrf::math::Vec3<T> h = (in + out);

#ifdef DEBUG
  if (std::isnan(h(0)) || std::isnan(h(1)) || std::isnan(h(2)))
  {
    std::cout << "  h = " << h << std::endl;
    std::cout << " in = " << in << " out = " << out << std::endl;
    assert(0);
  }
#endif


  T const h_length = h.length();

  if (h_length < T(LONG_DBL_EPSILON))
  {
    //Setting h to the normal
    h = mrf::math::Vec3<T>(T(0.0), T(1.0), T(0.0));
  }
  else
  {
    h /= h_length;
  }

  return h;
}




template<class T>
static inline bool equals(T const a, T const b, T const epsilon = T(EPSILON))
{
  return _Math<T>::absVal(a - b) < epsilon;
}


template<class T>
static inline bool equalsZero(T const a, T const epsilon = T(EPSILON))
{
  return _Math<T>::absVal(a) < epsilon;
}

template<class T>
static inline bool roundTo(T &value_to_be_rounded, T const target, T const epsilon = T(EPSILON))
{
  if (equals(value_to_be_rounded, target, epsilon))
  {
    value_to_be_rounded = target;
    return true;
  }
  return false;
}

template<class T>
static inline bool roundToZero(T &a, T const epsilon = T(EPSILON))
{
  if (equalsZero(a, epsilon))
  {
    a = T(0.0);
    return true;
  }
  return false;
}

template<class T>
static inline void clampEps(T &a, T min, T max, T const epsilon = T(EPSILON))
{
  if (a < min || equals(a, min, epsilon))
  {
    a = min;
  }
  else if (a > max || equals(a, max, epsilon))
  {
    a = max;
  }
}


static inline unsigned int factorial(unsigned int N)
{
  if (N == 0)
  {
    return 1;
  }

  unsigned int res = 1;
  for (unsigned int i = 1; i <= N; i++)
  {
    res *= i;
  }

  return res;
}

/**
 * Performs a % b
 * This method is meant to be used with floating value
 **/
template<class T>
static inline T modulo(T a, T b)
{
  T const q = a / b;
  T const r = a - q * b;

  return r;
}


// Functions from LAIR

/**
 * @brief Returns a v if it's a postive value or zero in other cases.
 **/
template<typename T>
static inline T gpos(T v)
{
  return (v >= T(0.0)) ? (v) : T(0.0);
}


/**
 * @brief Returns 0 if v is positive or null and  -v if v is negative
 **/
template<typename T>
static inline T gneg(T v)
{
  return (v < T(0.0)) ? (-v) : T(0.0);
}

/**
 *
 * @brief Returns the absolute value of v
 *
 **/
template<typename T>
static inline T gabs(T v)
{
  return (v >= T(0.0)) ? (v) : (-v);
}




}   // namespace math
}   // namespace mrf
