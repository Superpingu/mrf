/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/quat.hpp>

namespace mrf
{
namespace math
{
/**
 * ==============================================================================
 * CLASS Ref
 *
 * BY default the Referential is initialized to the identity matrix
 * and is positionned in zero
 * ==============================================================================
 **/


//! The referential is an axial system with a position in space.
static mrf::math::Vec3f const X_AXIS(1.0f, 0.0f, 0.0f);
static mrf::math::Vec3f const Y_AXIS(0.0f, 1.0f, 0.0f);
static mrf::math::Vec3f const Z_AXIS(0.0f, 0.0f, 1.0f);

template<class T>
class Ref
{
public:
  /*----- static methods -----*/

  inline static Ref identity();

  /*----- methods -----*/

  Ref() {}
  Ref(const Quat<T> &orientation, const Vec3<T> &pos);
  Ref(const Vec3<T> &pos);
  Ref(const Ref<T> &ref);
  ~Ref() {}

  Vec3<T> &      getPosition();
  Vec3<T> const &getPosition() const;
  Vec3<T> const &position() const;


  Quat<T> &      getOrientation();
  const Quat<T> &getOrientation() const;

  void setPosition(const Vec3<T> &pos);
  void setPosition(const T &x, const T &y, const T &z);
  void setOrientation(const Vec3<T> &x, const Vec3<T> &y, const Vec3<T> &z);
  void setOrientation(const Quat<T> &quat);

  Ref  rotate(const Vec3<T> &p, const Vec3<T> &axis, const T &angle) const;
  Ref  rotate(const Vec3<T> &axis, const T &angle) const;
  Ref &rotateEq(const Vec3<T> &p, const Vec3<T> &axis, const T &angle);
  Ref &rotateEq(const Vec3<T> &axis, const T &angle);
  Ref &rotateEq(const Quat<T> &q);
  Ref  translate(const Vec3<T> &vec) const;
  Ref &translateEq(const Vec3<T> &vec);

  Ref inverse() const;

  Ref slerp(const Ref &ref, T a) const;

  /**
   * Returns the Matrix to go to the world coordinates system
   * From Referential to World
   */
  Mat4<T> getMatrixFrom() const;

  /**
   * Returns the Matrix to go to the local coordinates system
   * World TO Referential
   */
  Mat4<T> getMatrixTo() const;


  Mat4<T> localToWorldMatrix() const;
  Mat4<T> worldToLocalMatrix() const;


  Ref operator*(const Ref<T> &ref) const;

  Ref &operator*=(const Ref<T> &ref);
  Ref &operator=(const Ref<T> &ref);


private:
  /*----- data members -----*/

  Quat<T> _orientation;
  Vec3<T> _pos;
};

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> Ref<T>::identity()
{
  return Ref(Quat<T>::identity(), Vec3<T>::zero());
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T>::Ref(const Quat<T> &orientation, const Vec3<T> &pos): _orientation(orientation)
                                                                  , _pos(pos)
{}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T>::Ref(const Vec3<T> &pos): _orientation(Quat<T>::identity())
                                      , _pos(pos)
{}


//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T>::Ref(const Ref<T> &ref): _orientation(ref._orientation)
                                     , _pos(ref._pos)
{}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Ref<T>::getPosition()
{
  return _pos;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const Vec3<T> &Ref<T>::getPosition() const
{
  return _pos;
}

template<class T>
inline const Vec3<T> &Ref<T>::position() const
{
  return _pos;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Ref<T>::getOrientation()
{
  return _orientation;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const Quat<T> &Ref<T>::getOrientation() const
{
  return _orientation;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Ref<T>::setPosition(const Vec3<T> &pos)
{
  _pos = pos;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Ref<T>::setPosition(const T &x, const T &y, const T &z)
{
  _pos(0) = x;
  _pos(1) = y;
  _pos(2) = z;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Ref<T>::setOrientation(const Vec3<T> &x, const Vec3<T> &y, const Vec3<T> &z)
{
  _orientation.fromAxes(x, y, z);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Ref<T>::setOrientation(const Quat<T> &orientation)
{
  _orientation = orientation;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> Ref<T>::rotate(const Vec3<T> &p, const Vec3<T> &axis, const T &angle) const
{
  // create the quaternion
  Quat<T> quat;
  quat.fromAxisAngle(axis, angle);

  // rotate the point
  Vec3<T> pos  = _pos - p;
  Vec3<T> pos2 = quat.getMatrix() * pos;
  pos2 += p;

  // rotate the quaternion
  return Ref<T>(quat * _orientation, pos2);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> Ref<T>::rotate(const Vec3<T> &axis, const T &angle) const
{
  // create the quaternion
  Quat<T> quat;
  quat.fromAxisAngle(axis, angle);

  // rotate the quaternion
  return Ref<T>(quat * _orientation, _pos);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> &Ref<T>::rotateEq(const Vec3<T> &p, const Vec3<T> &axis, const T &angle)
{
  // create the quaternion
  Quat<T> quat;
  quat.fromAxisAngle(axis, angle);

  // rotate the point
  Vec3<T> pos = _pos - p;
  _pos        = quat.getMatrix() * pos;
  _pos += p;

  // rotate the quaternion
  _orientation = quat * _orientation;

  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> &Ref<T>::rotateEq(const Vec3<T> &axis, const T &angle)
{
  // create the quaternion
  Quat<T> quat;
  quat.fromAxisAngle(axis, angle);

  // rotate the quaternion
  _orientation = quat * _orientation;

  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> &Ref<T>::rotateEq(const Quat<T> &q)
{
  // rotate the quaternion
  _orientation = q * _orientation;

  return *this;
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> Ref<T>::translate(const Vec3<T> &vec) const
{
  return Ref<T>(_orientation, _pos + vec);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> &Ref<T>::translateEq(const Vec3<T> &vec)
{
  _pos += vec;

  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> Ref<T>::inverse() const
{
  Quat<T> invq = _orientation.inverse();
  return Ref<T>(invq, invq * -_pos);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> Ref<T>::slerp(const Ref &ref, T a) const
{
  return Ref<T>(_orientation.slerp(ref._orientation, a), (1 - a) * _pos + a * ref._pos);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat4<T> Ref<T>::getMatrixFrom() const
{
  return _orientation.getMatrix().translateEq(_pos);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat4<T> Ref<T>::getMatrixTo() const
{
  return (_orientation.inverse()).getMatrix().translateBeforeEq(-_pos);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat4<T> Ref<T>::localToWorldMatrix() const
{
  return getMatrixFrom();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat4<T> Ref<T>::worldToLocalMatrix() const
{
  return getMatrixTo();
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> Ref<T>::operator*(const Ref<T> &ref) const
{
  return Ref<T>(_orientation * ref._orientation, _pos + (_orientation * ref._pos));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> &Ref<T>::operator*=(const Ref<T> &ref)
{
  _orientation *= ref._orientation;
  _pos += (_orientation * ref._pos);

  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Ref<T> &Ref<T>::operator=(const Ref<T> &ref)
{
  _orientation = ref._orientation;
  _pos         = ref._pos;

  return *this;
}

/*==============================================================================
TYPEDEF
==============================================================================*/

typedef Ref<float>  Reff;
typedef Ref<double> Refd;


template<class T>
inline std::ostream &operator<<(std::ostream &os, Ref<T> const &ref)
{
  os << " Ref [  pos = " << ref.getPosition() << "; orientation = " << ref.getOrientation() << " ] " << std::endl;

  return os;
}


}   // namespace math
}   // namespace mrf
