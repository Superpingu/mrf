#ifndef CLASS_MAT2_HPP
#define CLASS_MAT2_HPP

#include <mrf_core/math/vec2.hpp>

namespace mrf
{
namespace math
{
/*==============================================================================
  CLASS Mat2
  ==============================================================================*/

//! 2D Matrix class.
//!
//! The matrix is define in column major but the constructor
//! and the accessor use a row major definition.
//!
//! internal: ( 0 2 )  external: ( 0 1 )
//!           ( 1 3 )            ( 2 3 )

template<class T>
class Mat2
{
public:
  /*----- static methods -----*/

  static Mat2 identity();
  static Mat2 rotation(const T &rad);
  static Mat2 rotation(const T &cos, const T &sin);

  /*----- methods -----*/

  template<class S>
  Mat2(const Mat2<S> &m)
  {
    _e[0] = m._e[0];
    _e[1] = m._e[1];
    _e[2] = m._e[2];
    _e[3] = m._e[3];
  }

  Mat2() {}
  Mat2(const Mat2<T> &m);
  Mat2(const T &e00, const T &e01, const T &e10, const T &e11);
  ~Mat2() {}

  T *      ptr();
  const T *ptr() const;

  Mat2  inverse() const;
  Mat2 &inverseEq();

  Mat2 operator+(const Mat2<T> &m) const;
  Mat2 operator-(const Mat2<T> &m) const;
  Mat2 operator*(const T &val) const;
  Mat2 operator*(const Mat2<T> &m) const;
  Mat2 operator/(const T &val) const;

  Vec2<T> operator*(const Vec2<T> &vec) const;

  Mat2 &operator+=(const Mat2<T> &m);
  Mat2 &operator-=(const Mat2<T> &m);
  Mat2 &operator*=(const T &val);
  Mat2 &operator*=(const Mat2<T> &m);
  Mat2 &operator/=(const T &val);
  Mat2 &operator=(const Mat2<T> &m);

  T &      operator()(int line, int col);
  const T &operator()(int line, int col) const;

private:
  /*----- data members -----*/

  T _e[4];
};

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::identity()
{
  return Mat2<T>(1, 0, 0, 1);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::rotation(const T &rad)
{
  return rotation(cos(rad), sin(rad));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::rotation(const T &cos, const T &sin)
{
  return Mat2<T>(cos, -sin, sin, cos);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T>::Mat2(const Mat2<T> &m)
{
  *this = m;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T>::Mat2(const T &e00, const T &e01, const T &e10, const T &e11)
{
  _e[0] = e00;
  _e[1] = e10;
  _e[2] = e01;
  _e[3] = e11;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T *Mat2<T>::ptr()
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T *Mat2<T>::ptr() const
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::inverse() const
{
  double det = _e[0] * _e[3] - _e[2] * _e[1];

  if (det < 1e-12) return Mat2<T>();

  double idet = 1.0 / det;

  return Mat2<T>(idet * _e[3], -idet * _e[2], -idet * _e[1], idet * _e[0]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> &Mat2<T>::inverseEq()
{
  double det = _e[0] * _e[3] - _e[2] * _e[1];

  if (det < 1e-12) return Mat2<T>();

  double idet = 1.0 / det;

  T e0 = _e[0];

  _e[0] = idet * _e[3];
  _e[1] = -idet * _e[1];
  _e[2] = -idet * _e[2];
  _e[3] = idet * e0;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::operator+(const Mat2<T> &m) const
{
  return Mat2<T>(_e[0] + m._e[0], _e[2] + m._e[2], _e[1] + m._e[1], _e[3] + m._e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::operator-(const Mat2<T> &m) const
{
  return Mat2<T>(_e[0] - m._e[0], _e[2] - m._e[2], _e[1] - m._e[1], _e[3] - m._e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::operator*(const T &val) const
{
  return Mat2<T>(_e[0] * val, _e[2] * val, _e[1] * val, _e[3] * val);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::operator*(const Mat2<T> &m) const
{
  return Mat2<T>(
      _e[0] * m._e[0] + _e[2] * m._e[1],
      _e[0] * m._e[2] + _e[2] * m._e[3],
      _e[1] * m._e[0] + _e[3] * m._e[1],
      _e[1] * m._e[2] + _e[3] * m._e[3]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> Mat2<T>::operator/(const T &val) const
{
  T ival = (T)1 / val;
  return Mat2<T>(_e[0] * ival, _e[2] * ival, _e[1] * ival, _e[3] * ival);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Mat2<T>::operator*(const Vec2<T> &vec) const
{
  return Vec2<T>(_e[0] * vec(0) + _e[2] * vec(1), _e[1] * vec(0) + _e[3] * vec(1));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> &Mat2<T>::operator+=(const Mat2<T> &m)
{
  _e[0] += m._e[0];
  _e[1] += m._e[1];
  _e[2] += m._e[2];
  _e[3] += m._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> &Mat2<T>::operator-=(const Mat2<T> &m)
{
  _e[0] -= m._e[0];
  _e[1] -= m._e[1];
  _e[2] -= m._e[2];
  _e[3] -= m._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> &Mat2<T>::operator*=(const T &val)
{
  _e[0] *= val;
  _e[1] *= val;
  _e[2] *= val;
  _e[3] *= val;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> &Mat2<T>::operator*=(const Mat2<T> &m)
{
  T e0 = _e[0];
  T e1 = _e[1];

  _e[0] = e0 * m._e[0] + _e[2] * m._e[1];
  _e[1] = e1 * m._e[0] + _e[3] * m._e[1];
  _e[2] = e0 * m._e[2] + _e[2] * m._e[3];
  _e[3] = e1 * m._e[2] + _e[3] * m._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> &Mat2<T>::operator/=(const T &val)
{
  T ival = (T)1 / val;
  _e[0] *= ival;
  _e[1] *= ival;
  _e[2] *= ival;
  _e[3] *= ival;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> &Mat2<T>::operator=(const Mat2<T> &m)
{
  _e[0] = m._e[0];
  _e[1] = m._e[1];
  _e[2] = m._e[2];
  _e[3] = m._e[3];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Mat2<T>::operator()(int line, int col)
{
  return _e[line + (col << 1)];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Mat2<T>::operator()(int line, int col) const
{
  return _e[line + (col << 1)];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat2<T> operator*(const T &val, const Mat2<T> &m)
{
  return Mat2<T>(m(0, 0) * val, m(0, 1) * val, m(1, 0) * val, m(1, 1) * val);
}

/*==============================================================================
  TYPEDEF
  ==============================================================================*/

typedef Mat2<int>    Mat2i;
typedef Mat2<float>  Mat2f;
typedef Mat2<double> Mat2d;

}   // namespace math
}   // namespace mrf

#endif
