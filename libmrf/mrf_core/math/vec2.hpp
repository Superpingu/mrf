/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015,2017
 *
 * Copyright INRIA 2022
 *
 **/


#pragma once

#include <mrf_core/mrf_cuda_support.hpp>

#include <cmath>
#include <iostream>

namespace mrf
{
namespace math
{
//! Forward Declaration.
template<typename T>
class _Math;


/*==============================================================================
  CLASS Vec2
  ==============================================================================*/

//! 2D vector class.

template<class T>
class Vec2
{
public:
  /*----- methods -----*/

  inline static Vec2 zero();
  inline static Vec2 xaxis();
  inline static Vec2 yaxis();


  /*----- methods -----*/

  template<class S>
  Vec2(const Vec2<S> &vec)
  {
    _e[0] = (T)vec(0);
    _e[1] = (T)vec(1);
  }

  MRF_DEVICE_FUNC Vec2() { _e[0] = _e[1] = 0; }
  MRF_DEVICE_FUNC Vec2(const Vec2<T> &vec);
  MRF_DEVICE_FUNC Vec2(const T &e0, const T &e1);
  MRF_DEVICE_FUNC Vec2(const T);
  MRF_DEVICE_FUNC Vec2(const T vec[]);

  ~Vec2() {}

  T *      ptr();
  const T *ptr() const;

  T *      getArray();
  const T *getArray() const;

  T length() const;
  T sqrLength() const;
  T norm() const;    //an alias for length
  T norm2() const;   //an alias for sqrLength

  T dot(const Vec2<T> &vec) const;

  // Sets all components to their absolute value
  Vec2 &absVal();

  /**
   *  Set all component to their square value
   *  @return *this
   */
  Vec2 &squareEq();
  /**
   *  Returns a vector which components are square values of
   *  the calling argument
   */
  Vec2 square() const;

  Vec2 sqrt() const;


  Vec2  normal() const;
  Vec2 &normalEq();
  Vec2 &normalEq(const T len);
  Vec2 &negateEq();
  Vec2 &clampToMaxEq(const T &max);

  /**
   * Round vector components to zero if they are below the epsilon threshold
   */
  void epsilonRoundZero(T const epsilon);


  /** Given a candidate vector, replace
      elements whose corresponding elements in Candidate are
      smaller.  This function is used for finding objects' bounds,
      among other things.
  */
  inline void minimize(Vec2<T> const &Candidate);

  /** Given a candidate vector, replace
      elements whose corresponding elements in Candidate are
      larger.  This function is used for finding objects' bounds,
      among other things.
  */
  inline void maximize(Vec2<T> const &Candidate);

  inline void clampToZero(T EPSILON);
  inline bool allPosOrNull(T EPSILON);

  inline bool isInfOrNan() const;
  inline bool isFinite() const;


  Vec2 operator+(const Vec2<T> &rhs) const;
  Vec2 operator-(const Vec2<T> &rhs) const;
  Vec2 operator-() const;
  Vec2 operator*(const T &rhs) const;
  Vec2 operator*(const Vec2<T> &rhs) const;
  Vec2 operator/(const T &rhs) const;
  Vec2 operator/(const Vec2<T> &rhs) const;

  Vec2 &operator+=(const Vec2<T> &rhs);
  Vec2 &operator-=(const Vec2<T> &rhs);
  Vec2 &operator*=(const T &rhs);
  Vec2 &operator*=(const Vec2<T> &rhs);
  Vec2 &operator/=(const T &rhs);
  Vec2 &operator/=(const Vec2<T> &rhs);
  Vec2 &operator=(const Vec2<T> &rsh);
  bool  operator>(const Vec2<T> &_v) const;
  bool  operator>=(const Vec2<T> &_v) const;
  bool  operator<(const Vec2<T> &_v) const;
  bool  operator<=(const Vec2<T> &_v) const;

  bool operator==(const Vec2<T> &rhs) const;
  bool operator!=(const Vec2<T> &rhs) const;

  T &      operator()(int idx);
  const T &operator()(int idx) const;

  T &      operator[](int idx);
  const T &operator[](int idx) const;

  void setX(const T &);
  void setY(const T &);

  void set(T const x, T const y);
  void setValues(T const x, T const y);
  void setConstant(T cte_value);

  inline T sum() const;
  inline T mean() const;

  /* Apply a std::pow power function on each component:
   */
  inline Vec2<T> &pow(T const &power);



  T &x();
  T &y();

  MRF_DEVICE_FUNC T x() const;
  MRF_DEVICE_FUNC T y() const;
  /**
   * Returns always zero
   */
  MRF_DEVICE_FUNC T z() const;


  static inline T       dot(Vec2<T> const &v0, Vec2<T> const &v1);
  static inline Vec2<T> interpolate(const Vec2<T> &u, const Vec2<T> &v, T alpha);


private:
  /*----- data members -----*/

  T _e[2];
};

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::zero()
{
  return Vec2(T(0), T(0));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::xaxis()
{
  return Vec2(T(1), T(0));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::yaxis()
{
  return Vec2(T(0), T(1));
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T>::Vec2(const Vec2<T> &vec)
{
  _e[0] = vec._e[0];
  _e[1] = vec._e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T>::Vec2(const T &e0, const T &e1)
{
  _e[0] = e0;
  _e[1] = e1;
}

//------------------------------------------------------------------------------
//!

template<class T>
inline Vec2<T>::Vec2(const T e)
{
  _e[0] = e;
  _e[1] = e;
}

template<class T>
inline Vec2<T>::Vec2(const T vec[])
{
  _e[0] = vec[0];
  _e[1] = vec[1];
}




//------------------------------------------------------------------------------
//!
template<class T>
inline T *Vec2<T>::ptr()
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T *Vec2<T>::ptr() const
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T *Vec2<T>::getArray()
{
  return this->ptr();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T *Vec2<T>::getArray() const
{
  return this->ptr();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::length() const
{
  return std::sqrt(_e[0] * _e[0] + _e[1] * _e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::sqrLength() const
{
  return _e[0] * _e[0] + _e[1] * _e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::norm() const
{
  return this->length();
}
//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::norm2() const
{
  return this->sqrLength();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::dot(const Vec2<T> &vec) const
{
  return _e[0] * vec._e[0] + _e[1] * vec._e[1];
}
//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::absVal()
{
  _e[0] = mrf::math::_Math<T>::absVal(_e[0]);
  _e[1] = mrf::math::_Math<T>::absVal(_e[1]);
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::squareEq()
{
  _e[0] = _e[0] * _e[0];
  _e[1] = _e[1] * _e[1];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::square() const
{
  return Vec2<T>(_e[0] * _e[0], _e[1] * _e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::sqrt() const
{
  return Vec2<T>(std::sqrt(_e[0]), std::sqrt(_e[1]));
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::normal() const
{
  T tmp = (T)1 / length();
  return Vec2<T>(_e[0] * tmp, _e[1] * tmp);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::normalEq()
{
  T tmp = (T)1 / length();
  _e[0] *= tmp;
  _e[1] *= tmp;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::normalEq(const T len)
{
  T tmp = len / length();
  _e[0] *= tmp;
  _e[1] *= tmp;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::negateEq()
{
  _e[0] = -_e[0];
  _e[1] = -_e[1];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::clampToMaxEq(const T &max)
{
  if (_e[0] > max)
  {
    _e[0] = max;
  }
  if (_e[1] > max)
  {
    _e[1] = max;
  }
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec2<T>::epsilonRoundZero(T const epsilon)
{
  _e[0] = (_Math<T>::absVal(_e[0]) < epsilon) ? T(0) : _e[0];
  _e[1] = (_Math<T>::absVal(_e[1]) < epsilon) ? T(0) : _e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec2<T>::minimize(Vec2<T> const &Candidate)
{
  if (Candidate[0] < _e[0]) _e[0] = Candidate[0];
  if (Candidate[1] < _e[1]) _e[1] = Candidate[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec2<T>::maximize(Vec2<T> const &Candidate)
{
  if (Candidate[0] > _e[0]) _e[0] = Candidate[0];
  if (Candidate[1] > _e[1]) _e[1] = Candidate[1];
}


//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec2<T>::clampToZero(T EPSILON)
{
  _e[0] = (_Math<T>::absVal(_e[0]) < EPSILON) ? T(0.0) : _e[0];
  _e[1] = (_Math<T>::absVal(_e[1]) < EPSILON) ? T(0.0) : _e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::allPosOrNull(T EPSILON)
{
  return (_e[0] > -EPSILON) && (_e[1] > -EPSILON);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::isInfOrNan() const
{
  // These conversion to double is due to the lack of compliance of MS VS 2017 and 2019
  double const e0 = static_cast<double>(_e[0]);
  double const e1 = static_cast<double>(_e[1]);

  return std::isnan(e0) || std::isnan(e1) || std::isinf(e0) || std::isinf(e1);
}


//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::isFinite() const
{
  return !isInfOrNan();
}



//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::operator+(const Vec2<T> &rhs) const
{
  return Vec2<T>(_e[0] + rhs._e[0], _e[1] + rhs._e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::operator-(const Vec2<T> &rhs) const
{
  return Vec2<T>(_e[0] - rhs._e[0], _e[1] - rhs._e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::operator-() const
{
  return Vec2<T>(-_e[0], -_e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::operator*(const T &rhs) const
{
  return Vec2<T>(_e[0] * rhs, _e[1] * rhs);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::operator*(const Vec2<T> &rhs) const
{
  return Vec2<T>(_e[0] * rhs._e[0], _e[1] * rhs._e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::operator/(const T &rhs) const
{
  return Vec2<T>(_e[0] / rhs, _e[1] / rhs);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::operator/(const Vec2<T> &rhs) const
{
  return Vec2<T>(_e[0] / rhs._e[0], _e[1] / rhs._e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::operator+=(const Vec2<T> &rhs)
{
  _e[0] += rhs._e[0];
  _e[1] += rhs._e[1];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::operator-=(const Vec2<T> &rhs)
{
  _e[0] -= rhs._e[0];
  _e[1] -= rhs._e[1];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::operator*=(const T &rhs)
{
  _e[0] *= rhs;
  _e[1] *= rhs;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::operator*=(const Vec2<T> &rhs)
{
  _e[0] *= rhs._e[0];
  _e[1] *= rhs._e[1];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::operator/=(const T &rhs)
{
  _e[0] /= rhs;
  _e[1] /= rhs;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::operator/=(const Vec2<T> &rhs)
{
  _e[0] /= rhs._e[0];
  _e[1] /= rhs._e[1];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::operator=(const Vec2<T> &rhs)
{
  _e[0] = rhs._e[0];
  _e[1] = rhs._e[1];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::operator>(const Vec2<T> &_v) const
{
  return !(*this <= _v);

  //_e[0] > _v._e[0] && _e[1] > _v._e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::operator>=(const Vec2<T> &_v) const
{
  return _e[0] >= _v._e[0] && _e[1] >= _v._e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::operator<(const Vec2<T> &_v) const
{
  return !(*this >= _v);
  //_e[0] < _v._e[0] && _e[1] < _v._e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::operator<=(const Vec2<T> &_v) const
{
  return _e[0] <= _v._e[0] && _e[1] <= _v._e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::operator==(const Vec2<T> &rhs) const
{
  return _e[0] == rhs._e[0] && _e[1] == rhs._e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec2<T>::operator!=(const Vec2<T> &rhs) const
{
  return !(*this == rhs);
  // _e[0] != rhs._e[0] || _e[1] != rhs._e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec2<T>::operator()(int idx)
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec2<T>::operator()(int idx) const
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec2<T>::operator[](int idx)
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec2<T>::operator[](int idx) const
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec2<T>::setX(const T &x)
{
  _e[0] = x;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec2<T>::setY(const T &y)
{
  _e[1] = y;
}


//------------------------------------------------------------------------------
//!
template<class T>
void Vec2<T>::set(T const x, T const y)
{
  _e[0] = x;
  _e[1] = y;
}

//------------------------------------------------------------------------------
//!
template<class T>
void Vec2<T>::setValues(T const x, T const y)
{
  this->set(x, y);
}

//------------------------------------------------------------------------------
//!
template<class T>
void Vec2<T>::setConstant(T cte_value)
{
  this->set(cte_value, cte_value);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::sum() const
{
  return (_e[0] + _e[1]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::mean() const
{
  return this->sum() / T(2.0);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> &Vec2<T>::pow(T const &power)
{
  _e[0] = std::pow(_e[0], power);
  _e[1] = std::pow(_e[1], power);

  return *this;
}




//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec2<T>::x()
{
  return _e[0];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec2<T>::y()
{
  return _e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::x() const
{
  return _e[0];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::y() const
{
  return _e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::z() const
{
  return T(0);
}


//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec2<T>::dot(Vec2<T> const &v0, Vec2<T> const &v1)
{
  return v0.dot(v1);
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> Vec2<T>::interpolate(const Vec2<T> &u, const Vec2<T> &v, T alpha)
{
  return (u * (T(1.0) - alpha) + v * alpha);
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec2<T> operator*(const T &val, const Vec2<T> &vec)
{
  return Vec2<T>(vec(0) * val, vec(1) * val);
}

/*==============================================================================
  TYPEDEF
  ==============================================================================*/

typedef Vec2<int>            Vec2i;
typedef Vec2<short>          Vec2s;
typedef Vec2<unsigned int>   Vec2u;
typedef Vec2<size_t>         Vec2sz;
typedef Vec2<unsigned short> Vec2us;
typedef Vec2<float>          Vec2f;
typedef Vec2<double>         Vec2d;
typedef Vec2<bool>           Vec2b;

//External Operator
template<class T>
inline std::ostream &operator<<(std::ostream &os, Vec2<T> const &v)
{
  return os << " [ " << v.x() << "; " << v.y() << " ] " << std::endl;
}



}   // namespace math
}   // namespace mrf
