/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once


#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/math/math.hpp>

#include <vector>
#include <cassert>

namespace mrf
{
namespace math
{
class Integral1D
{
public:
  /**
   * @brief      Compute the Integral of a discreet 1D function using the Trapezoidal rule
   *
   * @param      x_values  the positions for which the function is given *ASSUMING*
   *             that they are given in ascending order but not necessarily uniformly spaced
   *
   * @param      values    the different values of the function at x_values position
   *
   * @tparam     T         Type/Precision of the data
   *
   * @return     The integral of the function bewtween x_values[0] and x_values[ x_values.size() -1 ]
   */
  template<typename T1>
  static inline T1 trapz(std::vector<T1> const &x_values, std::vector<T1> const &values);

  template<typename TX, typename TY>
  static inline TY trapz(std::vector<TX> const &x_values, std::vector<TY> const &values);
};


template<typename T1>
inline T1 Integral1D::trapz(std::vector<T1> const &x_values, std::vector<T1> const &values)
{
  assert(x_values.size() == values.size());

  if (x_values.size() == 0)
  {
    return T1();
  }

  if (x_values.size() == 1)
  {
    return values.front();
  }

  T1 integral = T1(0.0);

  //TODO:  Optimize it using OpenMP
  for (unsigned int i = 0; i < values.size() - 1; i++)
  {
    integral += (x_values[i + 1] - x_values[i]) * (values[i + 1] + values[i]);
  }

  return integral * T1(0.5);
}


template<typename TX, typename TY>
inline TY Integral1D::trapz(std::vector<TX> const &x_values, std::vector<TY> const &values)
{
  assert(x_values.size() == values.size());

  if (x_values.size() == 0)
  {
    return TY();
  }

  if (x_values.size() == 1)
  {
    return values.front();
  }

  TY integral = TY(0.0);

  //TODO:  Optimize it using OpenMP
  for (unsigned int i = 0; i < values.size() - 1; i++)
  {
    integral += (x_values[i + 1] - x_values[i]) * (values[i + 1] + values[i]);
  }

  return integral * TY(0.5);
}

}   // namespace math


}   // namespace mrf
