/**
 *
 * Authors:
 *  Romain Pacanowski: romain.pacanowski@institutoptique.fr
 * Copyright(C) CNRS. 2015, 2016.
 *  David Murray: david.murray@institutoptique.fr
 * Copyright(C) CNRS. 2021.
 *
 */

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/materials/materials.hpp>

#include <exception>
#include <iostream>
#include <stdexcept>

#include <ghc/fs_std_fwd.hpp>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
/**
 * @brief      Class providing some generic function to help parse some common markups, while keeping track of various scene-related paths.
 */
class MRF_CORE_EXPORT ParserHelper
{
public:
  std::string              _scene_path;
  std::vector<std::string> _ext_paths;


  ParserHelper();
  ParserHelper(std::string &);
  ParserHelper(std::string &, std::vector<std::string> &);

  void setExtPath(std::vector<std::string> &ext_path);

  mrf::math::Vec3f vec3_from_markup(tinyxml2::XMLElement *parent_markup_element, char const *const markup_name)
  {
    return vec3_from_markup<mrf::math::Vec3f>(parent_markup_element, markup_name);
  }

  mrf::math::Vec2f vec2_for_width_and_height(tinyxml2::XMLElement *parent_markup_element, char const *const markup_name)
  {
    return vec2_for_width_and_height<float, mrf::math::Vec2>(parent_markup_element, markup_name);
  }

  float scalar_from_attribute_name(
      tinyxml2::XMLElement *parent_markup_element,
      char const *const     markup_name,
      char const *const     attribute_name)
  {
    return scalar_from_attribute_name<float>(parent_markup_element, markup_name, attribute_name);
  }

  float scalar_from_value_in_mk(
      tinyxml2::XMLElement *parent_markup_element,
      char const *const     markup_name,
      char const *const     attribute_name = SceneLexer::VALUE_AT)
  {
    return scalar_from_value_in_mk<float>(parent_markup_element, markup_name, attribute_name);
  }

  float scalar_from_value(tinyxml2::XMLElement *markup_element);

  mrf::radiometry::Spectrum_T<float, float> spectrumFromMarkup(tinyxml2::XMLElement *spectrum_markup);

  //Helper functions

  /**
   * @brief      This method parse a <rgb_color> markup to retrieve the red, green blue
   * attributes from it and sets the given color with the retrieved values
   *
   * @param      rgb_markup  the TiXmlElement representing the <rgb_color>
   *
   * @return     The color sets with the attributes.
   */
  mrf::color::Color rgbFromMarkup(tinyxml2::XMLElement *rgb_markup);

  /**
   * @brief      This method parse an element, attempting to retrieve a color from it, either as RGB or a spectrum (doing the conversion if needed).
   *
   * @param      elem  the element from which a color is to be retrived.
   * @param      reflective  specify wether the color is reflective asset or not.
   *
   * @return     The color sets with the attributes.
   */
  mrf::materials::COLOR retrieveColor(tinyxml2::XMLElement *elem, bool reflective = true);

  /**
   * @brief      This method parse an element, attempting to retrieve multiple colors from it, either as RGB or a spectrum (doing the conversion if needed).
   *
   * @param      elem  the element from which the colors are to be retrived.
   * @param      colors  the color vector to be filled.
   * @param      reflective  specify wether the color is reflective asset or not.
   *
   * @return     true if the colors are succesfully parsed.
   */
  bool retrieveColors(tinyxml2::XMLElement *elem, std::vector<mrf::materials::COLOR> &colors, bool reflective = true);

  /**
   * @brief      This method parse a Fresnel element (markup), attempting to retrieve Fresnel coefficients.
   *
   * @param      elem  the element from which the coefficient are to be retrived.
   * @param      eta  the eta component (real part of the Fresnel coefficients).
   * @param      k  the kappa component (imaginary part of the Fresnel coefficients).
   *
   * @return     true if both eta and kappa are found.
   */
  bool retrieveFresnel(tinyxml2::XMLElement *elem, mrf::materials::COLOR &eta, mrf::materials::COLOR &k);

  /**
   * @brief      This method attempt to look for an albedo in a material markup element.
   *
   * @param      elem  the element from which the albedo is to be retrived.
   * @param      albedo
   *
   * @return     true if the material has an albedo.
   */
  bool retrieveReflectivity(tinyxml2::XMLElement *elem, float &albedo);

  std::string retrieveEmittanceType(tinyxml2::XMLElement *elem);

  /**
   * @brief      This method attempt to look for a position vector (attributes x, y, z) in an element.
   *
   * @param      elem  the element from which the albedo is to be retrived.
   * @param      pos  the position to be filled by the x, y and z attributes.
   *
   * @return     true if the position is retrieved.
   */
  bool retrievePosition(tinyxml2::XMLElement *elem, mrf::math::Vec3f &pos);

  /**
   * @brief      This method attempt to look for a normal vector (attributes x, y, z) in an element.
   *
   * @param      elem  the element from which the albedo is to be retrived.
   * @param      normal  the normal to be filled by the x, y and z attributes.
   *
   * @return     true if the normal is retrieved.
   */
  bool retrieveNormal(tinyxml2::XMLElement *elem, mrf::math::Vec3f &normal);

  /**
   * @brief      This method attempt to look for a direction vector (attributes x, y, z) in an element.
   *
   * @param      elem  the element from which the albedo is to be retrived.
   * @param      dir  the direction to be filled by the x, y and z attributes.
   *
   * @return     true if the direction is retrieved.
   */
  bool retrieveDirection(tinyxml2::XMLElement *elem, mrf::math::Vec3f &dir);

  //TODO, move the function from scene_parser to this class.
  //bool retrieveFloatVector(tinyxml2::XMLElement *element, std::vector<float> &v);

  //TODO, move the function from scene_parser to this class.
  //mrf::math::Mat4f retrieveTransform(tinyxml2::XMLElement *elem);

  /**
   * @brief      This method convert a MRF image flag returned after loading a texture to a boolean.
   *
   * @param      flag  the flag to test.
   * @param      name  the name of the texture.
   *
   * @return     true if the texture is loaded.
   */
  bool checkErrorOnTextureLoading(mrf::image::IMAGE_LOAD_SAVE_FLAGS flag, std::string const &name)
  {
    using namespace mrf::image;
    if (flag == MRF_NO_ERROR)
    {
      mrf::gui::fb::Loger::getInstance()->info("Successfully loaded ", name);
      return true;
    }
    if (flag == MRF_ERROR_WRONG_EXTENSION)
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG EXTENSION file=", name);
      return false;
    }
    if (flag == MRF_ERROR_WRONG_FILE_PATH)
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG FILE PATH file=", name);
      return false;
    }
    if (flag == MRF_ERROR_LOADING_FILE)
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error LOADING FILE file=", name);
      return false;
    }
    if (flag == MRF_ERROR_DECODING_FILE)
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error DECODING FILE file=", name);
      return false;
    }
    if (flag == MRF_ERROR_WRONG_PIXEL_FORMAT)
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG PIXEL FORMAT file=", name);
      return false;
    }
    if (flag == MRF_ERROR_WRONG_IMAGE_SIZE)
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Cannot load image error WRONG IMAGE SIZE file=", name);
      return false;
    }
    mrf::gui::fb::Loger::getInstance()->fatal("Unrecognized error flag on load image flag=", flag);
    return false;
  }

private:
  /**
   * @brief      Parse from a given markup the
   *             "x", "y" "z" attributes to return a VEC3_TYPE
   *
   * @param      parent_markup_element  Pointer to TiXmlElement markup that is the parent markup
   * @param      markup_name the name of the child markup to be looked for.
   *
   * @tparam     VEC3_TYPE    Template parameter representing the 3D Vector type
   *
   * @return     The Vector type parsed
   *
   * @throw      <exception_object> A logic_error is thrown
   *             if the markup attributes could not be parsed of if <the markup_name>
   *             does not exist
   */
  template<class VEC3_TYPE>
  VEC3_TYPE vec3_from_markup(tinyxml2::XMLElement *parent_markup_element, char const *const markup_name);

  /**
   * @brief      Parse a given markup for the "width" and "height" attribute
   *             and returns a 2D Vector
   *
   * @param      parent_markup_element  Pointer to TiXmlElement markup that is the parent marku
   * @param      markup_name            the name of the child markup to be looked for.
   *
   * @tparam     VEC2_TYPE              Template parameter representing the 2D Vector type
   *
   * @return     { description_of_the_return_value }
   * @throw      logic_error A logic_error is the markup_name could not be found
   *             or if the attributes "width" and "height" could not be parsed
   */
  template<typename T, template<typename = T> class VEC2_TYPE>
  VEC2_TYPE<T> vec2_for_width_and_height(tinyxml2::XMLElement *parent_markup_element, char const *const markup_name);

  template<typename T>
  T scalar_from_attribute_name(
      tinyxml2::XMLElement *parent_markup_element,
      char const *const     markup_name,
      char const *const     attribute_name);

  /**
   * @brief      Returns a scalar parser from the markup named markup_name from the parent_markup_element.
   *             The value returned is parsed from the attribute name "value"
   *
   * @param      parent_markup_element  { parameter_description }
   * @param      markup_name            { parameter_description }
   * @param      attribute_name         { parameter_description }
   *
   * @tparam     T                      { description }
   *
   * @return     the parsed scalar
   * @throw      logic_error A logic_error exception is thrown in case of error
   */
  template<typename T>
  T scalar_from_value_in_mk(
      tinyxml2::XMLElement *parent_markup_element,
      char const *const     markup_name,
      char const *const     attribute_name = SceneLexer::VALUE_AT);

  /**
   * @brief      Returns a scalar parser from the markup_element.
   *             The value returned is parsed from the attribute name "value"
   *
   * @param      markup_element  { parameter_description }
   *
   * @tparam     T                      { description }
   *
   * @return     the parsed scalar
   * @throw      logic_error A logic_error exception is thrown in case of error
   */
  template<typename T>
  T scalar_from_value(tinyxml2::XMLElement *markup_element);
};

}   // namespace io
}   // namespace mrf
