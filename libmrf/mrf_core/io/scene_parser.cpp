/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include <mrf_core/io/scene_parser.hpp>


#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/feedback/loger.hpp>
#include <mrf_core/io/extensions.hpp>
//#include <mrf_core/io/ply_loader.hpp>
#include <mrf_core/io/obj_parser.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/radiometry/d65.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/lighting/i_light.hpp>
#include <mrf_core/lighting/panoramic_envmap.hpp>
#include <mrf_core/lighting/spherical_envmap.hpp>
#include <mrf_core/geometry/point.hpp>
#include <mrf_core/geometry/sphere.hpp>
#include <mrf_core/geometry/direction.hpp>
#include <mrf_core/geometry/mesh.hpp>
#include <mrf_core/geometry/shape.hpp>
#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/rendering/color_background_manager.hpp>
#include <mrf_core/io/alta_reader.h>

#include <tinyply/tinyply.h>

//STL
#include <iostream>
#include <vector>
#include <typeinfo>
#include <algorithm>


#include <mrf_core/util/precision_timer.hpp>

using std::cerr;
using std::cout;
using std::endl;
using std::make_pair;
using std::map;
using std::pair;
using std::string;
using std::vector;

using namespace mrf::gui::fb;
//using namespace mrf::io::token;
using namespace mrf::materials;
using namespace mrf::rendering;
using namespace mrf::lighting;
using namespace mrf::geom;
using namespace mrf::util;
using mrf::materials::COLOR;
using mrf::math::Mat4f;
using mrf::math::Quatf;
using mrf::math::Reff;
using mrf::math::Vec3f;
using mrf::radiometry::Spectrum;

using tinyxml2::XML_SUCCESS;
using tinyxml2::XMLElement;
using tinyxml2::XMLNode;

namespace mrf
{
namespace io
{
//-------------------------------------------------------------------------
//public STATIC
//-------------------------------------------------------------------------
SceneParser &SceneParser::Instance()
{
  static SceneParser s_scene_parser = SceneParser();
  return s_scene_parser;
}

bool SceneParser::loadScene(std::string const &scene_file, mrf::rendering::Scene *&scene)
{
  SceneParser &parser = SceneParser::Instance();
  scene               = new Scene();
  if (!parser.loadScene(scene_file.c_str(), scene))
  {
    cerr << " Scene file: " << scene_file << " could NOT be loaded." << endl;
    return false;
  }
  return true;
}

bool SceneParser::loadScene(mrf::rendering::Scene *&scene)
{
  std::string ext;
  mrf::util::StringParsing::getFileExtension(_scene_path.c_str(), ext);
  if (!(ext == "msf"))
  {
    mrf::gui::fb::Loger::getInstance()->fatal("The provided scene file is not a MSF file, aborting.");
    return false;
  }
  else
  {
    std::ifstream f(_scene_path.c_str());
    if (!f.good())
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Could not open the scene file, please check that it exits.");
      return false;
    }
  }

  bool res = false;

  if (_doc.LoadFile(_scene_path.c_str()) != XML_SUCCESS)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::XML_LOAD_FILE);
    mrf::gui::fb::Loger::getInstance()->fatal("At line", _doc.ErrorLineNum());
    mrf::gui::fb::Loger::getInstance()->fatal(" CHECK THAT YOUR XML FILE IS VALID !!! ");

    return false;
  }

  _doc_handle = tinyxml2::XMLHandle(&_doc);

  StringParsing::getDirectory(_scene_path.c_str(), _current_directory);

  _parser = ParserHelper(_current_directory);

  mrf::gui::fb::Loger::getInstance()->trace("  filename = ", _scene_path.c_str());
  mrf::gui::fb::Loger::getInstance()->trace("  current directory = ", _current_directory);

  if (!scene) scene = new Scene();
  res = parsingScene(*scene);

  mrf::gui::fb::Loger::getInstance()->trace(" Scene HAS BEEN SUCCESSFULLY PARSED. ");
  return res;
}

bool SceneParser::loadScene(char const *filename, mrf::rendering::Scene *&scene)
{
  _scene_path = filename;
  return loadScene(scene);
}

bool SceneParser::parsingScene(Scene &scene)
{
  mrf::gui::fb::Loger::getInstance()->trace("Starting to parse the scene file ");


  //Clear All Maps and BRDF counter
  _brdf_map.clear();
  clearEnvmaps();


  //-------------------------------------------------------------------------
  // This Will parse :
  // OBJ File (as scene)
  // PLY File (as scene)
  //-------------------------------------------------------------------------

  // RP: Note used at the moment so commented
  //bool correctly_parsed_resources_path = true;
  if (!parseResourcesPath(scene))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Problem with resources path ");
    //correctly_parsed_resources_path = false;
  }

  // RP: Note used at the moment so commented
  //bool correctly_parsed_extresources = true;
  if (!parseExternalResources(scene))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Problem with external resources ");
    //correctly_parsed_extresources = false;
  }
  else
  {
    _parser.setExtPath(_ext_paths);
  }

  bool correctly_parsed_background = true;
  if (!parseBackground(scene))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Problem with background management. Assigning default black color ");
    ColorBckgManager *cbm = new ColorBckgManager();
    scene.setBckManager(cbm);
    correctly_parsed_background = true;
  }

  bool correctly_parsed_global_material = true;
  if (!parseGlobalMaterials(scene))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Problem while parsing materials ");
    correctly_parsed_global_material = false;
  }

  bool correctly_parsed_lights = true;
  if (!parseLights(scene))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Problem while parsing lights ");
    correctly_parsed_lights = false;
  }

  bool correctly_parsed_shapes = true;
  if (!parseShapes(scene))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Problem while parsing shapes ");
    correctly_parsed_shapes = false;
  }

  // Some Checking before continuing
  if (scene.shapes().size() == 0)
  {
    mrf::gui::fb::Loger::getInstance()->warn(" NO SHAPES INSIDE SCENE !!! ");
    //return false;
  }




  //we do not return the error due to bad parsing
  //of ext resources to allow the user to still render an
  //image even though some envmaps or other ext resources
  //can't be loaded
  return   //correctly_parsed_extresources &&
      correctly_parsed_shapes && correctly_parsed_lights && correctly_parsed_global_material
      && correctly_parsed_background;
}

bool SceneParser::parseGlobalMaterials(Scene &scene)
{
  mrf::gui::fb::Loger::getInstance()->trace("Looking for materials ");

  XMLElement *material = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                             .FirstChildElement(SceneLexer::MATERIALS_MK)
                             .FirstChildElement()
                             .ToElement();

  if (material)
  {
    mrf::gui::fb::Loger::getInstance()->trace("Materials Found ! Parsing them....");

    MaterialSceneParser matParser(_doc_handle, _plugins.ids, _plugins.fcts, _parser);
    if (matParser.parsingScene(scene))
    {
      _brdf_map = matParser.getMaterialMap();
    }

    //for (; material; material = material->NextSiblingElement())
    //{
    //  std::string element_name = material->Name();

    //  if (element_name.compare(SceneLexer::MATERIAL_MK) == 0)
    //  {
    //    if (!addMaterial(material, scene))
    //    {
    //      mrf::gui::fb::Loger::getInstance()->warn("Problem while tying to add one material");
    //    }
    //  }
    //  else if (element_name.compare(SceneLexer::EMITTANCE_MK) == 0)
    //  {
    //    if (!addEmittance(material, scene))
    //    {
    //      mrf::gui::fb::Loger::getInstance()->warn("Problem while trying to add an emittance in parse materials");
    //    }
    //  }
    //  else
    //  {
    //    mrf::gui::fb::Loger::getInstance()->warn("Unrecognized element in materials ");
    //  }
    //}

    //if (_multi_mat_list.size() >= 1)
    //{
    //  for (int i = 0; i < _multi_mat_list.size(); ++i)
    //  {
    //    auto multi_mat = _multi_mat_list[i];
    //    bool rOK       = true;
    //    for (int j = 0; j < multi_mat->size(); ++j)
    //    {
    //      std::string name = multi_mat->getAllNames()[j];
    //      rOK &= _brdf_map.count(name) == 1;
    //      if (rOK)
    //        multi_mat->addMRFIdEntry(_brdf_map[name]);
    //      else
    //        break;
    //    }
    //    if (!rOK) multi_mat = nullptr;
    //  }
    //}
  }
  else
  {
    // THIS IS NOT AN ERROR because A scene can contain only light sources
    mrf::gui::fb::Loger::getInstance()->warn(ParsingErrors::NO_MATERIALS);
  }
  return true;
}

bool SceneParser::parseLights(mrf::rendering::Scene &scene)
{
  mrf::gui::fb::Loger::getInstance()->trace("Looking for Lights ");

  //Point Lights
  XMLElement *plights = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                            .FirstChildElement(SceneLexer::LIGHTS_MK)
                            .FirstChildElement(SceneLexer::POINT_LIGHT_MK)
                            .ToElement();

  if (plights)
  {
    mrf::gui::fb::Loger::getInstance()->trace("PointLight(s) Found ! Parsing it....");

    for (; plights; plights = plights->NextSiblingElement(SceneLexer::POINT_LIGHT_MK))
    {
      addPointLight(plights, scene);
    }
  }

  // Directional Light
  XMLElement *dir_lights = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                               .FirstChildElement(SceneLexer::LIGHTS_MK)
                               .FirstChildElement(SceneLexer::DIR_LIGHT_MK)
                               .ToElement();
  if (dir_lights)
  {
    for (; dir_lights; dir_lights = dir_lights->NextSiblingElement(SceneLexer::DIR_LIGHT_MK))
    {
      mrf::gui::fb::Loger::getInstance()->trace("Directional Light(s) Found ! Parsing it....");
      addDirectionalLight(dir_lights, scene);
    }
  }

  // Quad Lights
  XMLElement *area_lights = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                                .FirstChildElement(SceneLexer::LIGHTS_MK)
                                .FirstChildElement(SceneLexer::AREA_LIGHT_MK)
                                .ToElement();
  if (area_lights)
  {
    for (; area_lights; area_lights = area_lights->NextSiblingElement(SceneLexer::AREA_LIGHT_MK))
    {
      mrf::gui::fb::Loger::getInstance()->trace("Area Light(s) Found ! Parsing it....");
      addQuadLight(area_lights, scene);
    }
  }

  // Sphere Lights
  XMLElement *sphere_lights = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                                  .FirstChildElement(SceneLexer::LIGHTS_MK)
                                  .FirstChildElement(SceneLexer::SPHERE_LIGHT_MK)
                                  .ToElement();
  if (sphere_lights)
  {
    for (; sphere_lights; sphere_lights = sphere_lights->NextSiblingElement(SceneLexer::SPHERE_LIGHT_MK))
    {
      mrf::gui::fb::Loger::getInstance()->trace("Sphere Light(s) Found ! Parsing it....");
      addSphereLight(sphere_lights, scene);
    }
  }

  if (scene.nbLights() == 0)
  {
    mrf::gui::fb::Loger::getInstance()->warn(
        " Warning no lights defined...Scene is going to look dark unless a background or an environment map is defined !!! ");
  }
  return true;
}

bool SceneParser::retrieveFloatVector(XMLElement *element, std::vector<float> &v)
{
  char const *data = element->GetText();

  //REPLACE ALL NEW LINE OR CARIAGE RETURN TO SPACE
  string cleaned_data(data);
  cleanString(cleaned_data);


  vector<string> tokens;
  StringParsing::tokenize(cleaned_data, tokens);


  mrf::gui::fb::Loger::getInstance()->trace("Number of tokens retrieved ", tokens.size());

  for (unsigned int i = 0; i < tokens.size(); i++)
  {
    float vc;
    if (!StringParsing::convertStringToFloat(tokens[i].c_str(), vc))
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Could not convert a vertex coordinates to float ");
      return false;
    }

    v.push_back(vc);
  }

  return true;
}

bool SceneParser::retrieveMultipleTransforms(XMLElement *elem, mrf::math::Mat4f &mat)
{
  mat = mrf::math::Mat4f::identity();

  XMLElement *transforms = elem->FirstChildElement(SceneLexer::TRANSFORM_MK);

  if (transforms)
  {
    mrf::gui::fb::Loger::getInstance()->trace("Multiple Transformations ! Trying to Parsing them....");

    for (; transforms; transforms = transforms->NextSiblingElement(SceneLexer::TRANSFORM_MK))
    {
      mrf::math::Mat4f a_transform;

      if (retrieveTransform(transforms, a_transform))
      {
        mat *= a_transform;

        mrf::gui::fb::Loger::getInstance()->trace("SceneParser::retrieveMultipleTransforms with matrix");
        mrf::gui::fb::Loger::getInstance()->trace(" matrix = ", mat);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->warn("Failed to retrieve a transformation");
        return false;
      }
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Empty transforms markup ");
  }

  return true;
}

bool SceneParser::retrieveMultipleTransforms(XMLElement *elem, mrf::math::Reff &ref, mrf::math::Vec3f &scale)
{
  ref   = mrf::math::Reff::identity();
  scale = mrf::math::Vec3f(1.0f, 1.0f, 1.0f);

  XMLElement *transforms = elem->FirstChildElement(SceneLexer::TRANSFORM_MK);

  if (transforms)
  {
    mrf::gui::fb::Loger::getInstance()->trace("Multiple Transformations ! Trying to Parsing them....");

    for (; transforms; transforms = transforms->NextSiblingElement(SceneLexer::TRANSFORM_MK))
    {
      mrf::math::Reff  a_ref   = mrf::math::Reff::identity();
      mrf::math::Vec3f a_scale = mrf::math::Vec3f(1.0f, 1.0f, 1.0f);

      if (retrieveTransform(transforms, a_ref, a_scale))
      {
        ref.rotateEq(a_ref.getOrientation());
        ref.translateEq(a_ref.getPosition());

        scale *= a_scale;

        mrf::gui::fb::Loger::getInstance()->trace("SceneParser::retrieveMultipleTransforms");
        mrf::gui::fb::Loger::getInstance()->trace(" ref.getMatrixFrom() ", ref.getMatrixFrom());
        mrf::gui::fb::Loger::getInstance()->trace(" Scale: ", scale);
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->warn("Failed to retrieve a transformation");
        return false;
      }
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->warn(" Empty transforms markup ");
  }

  return true;
}

bool SceneParser::retrieveTransform(XMLElement *elem, mrf::math::Mat4f &mat)
{
  mat                           = mrf::math::Mat4f::identity();
  mrf::math::Mat4f scaleMat     = mrf::math::Mat4f::identity();
  mrf::math::Mat4f translateMat = mrf::math::Mat4f::identity();
  mrf::math::Mat4f rotationMat  = mrf::math::Mat4f::identity();

  //-----------------------------------------------------------------------
  // Scale Transformation
  //-----------------------------------------------------------------------
  XMLElement *scaleElem = elem->FirstChildElement(SceneLexer::SCALE_MK);
  if (scaleElem)
  {
    float scale_factor;
    int   scaleOK = scaleElem->QueryFloatAttribute(SceneLexer::FACTOR_AT, &scale_factor);

    if (scaleOK != XML_SUCCESS)
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Non uniform scale ");

      float scale_factor_x = 1.0f;
      float scale_factor_y = 1.0f;
      float scale_factor_z = 1.0f;

      int scaleX_OK = scaleElem->QueryFloatAttribute(SceneLexer::X_AT, &scale_factor_x);

      int scaleY_OK = scaleElem->QueryFloatAttribute(SceneLexer::Y_AT, &scale_factor_y);

      int scaleZ_OK = scaleElem->QueryFloatAttribute(SceneLexer::Z_AT, &scale_factor_z);

      if ((scaleX_OK != XML_SUCCESS) || (scaleY_OK != XML_SUCCESS) || (scaleZ_OK != XML_SUCCESS))
      {
        mrf::gui::fb::Loger::getInstance()->warn("Culd not retrieve Scale Transformation. Ignoring it");
        return false;
      }

      mat *= mrf::math::Mat4f::scale(scale_factor_x, scale_factor_y, scale_factor_z);
      scaleMat *= mrf::math::Mat4f::scale(scale_factor_x, scale_factor_y, scale_factor_z);
    }
    else
    {
      mat *= mrf::math::Mat4f::scale(scale_factor, scale_factor, scale_factor);
      scaleMat *= mrf::math::Mat4f::scale(scale_factor, scale_factor, scale_factor);
    }
  }

  mrf::gui::fb::Loger::getInstance()->trace(" After scale Transformation is ", mat);


  //-----------------------------------------------------------------------
  // Rotation Transformation
  //-----------------------------------------------------------------------

  //Does it exist ?
  if (elem->FirstChildElement(SceneLexer::ROTATION_MK))
  {
    XMLElement *angleElem
        = elem->FirstChildElement(SceneLexer::ROTATION_MK)->FirstChildElement(SceneLexer::ROT_ANGLE_MK);

    XMLElement *axisElem = elem->FirstChildElement(SceneLexer::ROTATION_MK)->FirstChildElement(SceneLexer::ROT_AXIS_MK);


    if (angleElem && axisElem)
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Rotation Detected ");

      float angle   = 0.0f;
      int   angleOK = angleElem->QueryFloatAttribute(SceneLexer::VALUE_AT, &angle);

      float axis_x = 0.0f;
      float axis_y = 1.0f;
      float axis_z = 0.0f;

      int axis_X_OK = axisElem->QueryFloatAttribute(SceneLexer::X_AT, &axis_x);
      int axis_Y_OK = axisElem->QueryFloatAttribute(SceneLexer::Y_AT, &axis_y);
      int axis_Z_OK = axisElem->QueryFloatAttribute(SceneLexer::Z_AT, &axis_z);

      if ((angleOK != XML_SUCCESS) || (axis_X_OK != XML_SUCCESS) || (axis_Y_OK != XML_SUCCESS)
          || (axis_Z_OK != XML_SUCCESS))
      {
        mrf::gui::fb::Loger::getInstance()->warn("Rotation could not be retrieved");
        return false;
      }


      float            angleRad = mrf::math::Math::toRadian(angle);
      mrf::math::Vec3f axis(axis_x, axis_y, axis_z);



      mrf::gui::fb::Loger::getInstance()->trace(" Angle of rotation in radian ", angleRad);

      mrf::gui::fb::Loger::getInstance()->trace(" axis of rotation ", axis);


      mrf::math::Quatf q1;
      q1.fromAxisAngle(axis, angleRad);


      mrf::gui::fb::Loger::getInstance()->trace(" Rotation Matrix", q1.getMatrix());


      mat *= q1.getMatrix();
      rotationMat = q1.getMatrix();
    }
  }


  XMLElement *transElem = elem->FirstChildElement(SceneLexer::TRANSLATION_MK);
  if (transElem)
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Translation detected ");

    float x1 = 0.0f;
    float x2 = 0.0f;
    float x3 = 0.0f;

    int x1OK = transElem->QueryFloatAttribute(SceneLexer::X_AT, &x1);
    int x2OK = transElem->QueryFloatAttribute(SceneLexer::Y_AT, &x2);
    int x3OK = transElem->QueryFloatAttribute(SceneLexer::Z_AT, &x3);

    if ((x1OK != XML_SUCCESS) || (x2OK != XML_SUCCESS) || (x3OK != XML_SUCCESS))
    {
      mrf::gui::fb::Loger::getInstance()->warn(" A translation transformation could not be retrieved");
      return false;
    }

    mat.translateEq(mrf::math::Vec3f(x1, x2, x3));
    translateMat.translateEq(mrf::math::Vec3f(x1, x2, x3));
  }

  return true;
}

bool SceneParser::retrieveTransform(XMLElement *elem, mrf::math::Reff &ref, mrf::math::Vec3f &scale)
{
  ref   = mrf::math::Reff::identity();
  scale = mrf::math::Vec3f(1.0f, 1.0f, 1.0f);

  //-----------------------------------------------------------------------
  // Scale Transformation
  //-----------------------------------------------------------------------
  XMLElement *scaleElem = elem->FirstChildElement(SceneLexer::SCALE_MK);
  if (scaleElem)
  {
    float scale_factor;
    int   scaleOK = scaleElem->QueryFloatAttribute(SceneLexer::FACTOR_AT, &scale_factor);

    if (scaleOK != XML_SUCCESS)
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Non uniform scale ");

      float scale_factor_x = 0.0f;
      float scale_factor_y = 0.0f;
      float scale_factor_z = 0.0f;

      int scaleX_OK = scaleElem->QueryFloatAttribute(SceneLexer::X_AT, &scale_factor_x);

      int scaleY_OK = scaleElem->QueryFloatAttribute(SceneLexer::Y_AT, &scale_factor_y);

      int scaleZ_OK = scaleElem->QueryFloatAttribute(SceneLexer::Z_AT, &scale_factor_z);

      if ((scaleX_OK != XML_SUCCESS) || (scaleY_OK != XML_SUCCESS) || (scaleZ_OK != XML_SUCCESS))
      {
        mrf::gui::fb::Loger::getInstance()->warn("Culd not retrieve Scale Transformation. Ignoring it");
        return false;
      }

      scale.set(scale_factor_x, scale_factor_y, scale_factor_z);
    }
    else
    {
      scale.set(scale_factor, scale_factor, scale_factor);
    }
  }


  //-----------------------------------------------------------------------
  // Rotation Transformation
  //-----------------------------------------------------------------------

  //Does it exist ?
  if (elem->FirstChildElement(SceneLexer::ROTATION_MK))
  {
    XMLElement *angleElem
        = elem->FirstChildElement(SceneLexer::ROTATION_MK)->FirstChildElement(SceneLexer::ROT_ANGLE_MK);

    XMLElement *axisElem = elem->FirstChildElement(SceneLexer::ROTATION_MK)->FirstChildElement(SceneLexer::ROT_AXIS_MK);


    if (angleElem && axisElem)
    {
      mrf::gui::fb::Loger::getInstance()->trace(" Rotation Detected ");

      float angle   = 0.0f;
      int   angleOK = angleElem->QueryFloatAttribute(SceneLexer::VALUE_AT, &angle);

      float axis_x    = 0.0f;
      float axis_y    = 1.0f;
      float axis_z    = 0.0f;
      int   axis_X_OK = axisElem->QueryFloatAttribute(SceneLexer::X_AT, &axis_x);
      int   axis_Y_OK = axisElem->QueryFloatAttribute(SceneLexer::Y_AT, &axis_y);
      int   axis_Z_OK = axisElem->QueryFloatAttribute(SceneLexer::Z_AT, &axis_z);

      if ((angleOK != XML_SUCCESS) || (axis_X_OK != XML_SUCCESS) || (axis_Y_OK != XML_SUCCESS)
          || (axis_Z_OK != XML_SUCCESS))
      {
        mrf::gui::fb::Loger::getInstance()->warn("Rotation could not be retrieved");
        return false;
      }

      float            angleRad = mrf::math::Math::toRadian(angle);
      mrf::math::Vec3f axis(axis_x, axis_y, axis_z);



      mrf::gui::fb::Loger::getInstance()->trace(" Angle of rotation in radian ", angleRad);
      mrf::gui::fb::Loger::getInstance()->trace(" axis of rotation ", axis);

      ref.rotateEq(axis, angleRad);
    }
  }


  XMLElement *transElem = elem->FirstChildElement(SceneLexer::TRANSLATION_MK);
  if (transElem)
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Translation detected ");

    float x1 = 0.0f;
    float x2 = 0.0f;
    float x3 = 0.0f;

    int x1OK = transElem->QueryFloatAttribute(SceneLexer::X_AT, &x1);
    int x2OK = transElem->QueryFloatAttribute(SceneLexer::Y_AT, &x2);
    int x3OK = transElem->QueryFloatAttribute(SceneLexer::Z_AT, &x3);

    if ((x1OK != XML_SUCCESS) || (x2OK != XML_SUCCESS) || (x3OK != XML_SUCCESS))
    {
      mrf::gui::fb::Loger::getInstance()->warn(" A translation transformation could not be retrieved");
      return false;
    }

    ref.translateEq(mrf::math::Vec3f(x1, x2, x3));
  }

  return true;
}

bool SceneParser::parseShapes(mrf::rendering::Scene &scene)
{
  XMLElement *shapes = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                           .FirstChildElement(SceneLexer::SHAPES_MK)
                           .FirstChildElement(SceneLexer::SHAPE_MK)
                           .ToElement();

  if (shapes)
  {
    //Temp vector to check used materials
    std::vector<bool> is_mat_used(scene.umatSize(), false);

    mrf::gui::fb::Loger::getInstance()->trace("Shape(s) Found ! Parsing it (them)....");

    for (; shapes; shapes = shapes->NextSiblingElement(SceneLexer::SHAPE_MK))
    {
      Reff  ref   = Reff::identity();
      Vec3f scale = Vec3f(1.0, 1.0, 1.0);
      Mat4f transform_matrix;

      //Check if a transformations element exists
      XMLElement *transform_elem                   = shapes->FirstChildElement(SceneLexer::TRANSFORMATIONS_MK);
      bool        has_global_vertex_transformation = false;

      if (transform_elem)
      {
        if (retrieveMultipleTransforms(transform_elem, ref, scale)
            && retrieveMultipleTransforms(transform_elem, transform_matrix))
        {
          has_global_vertex_transformation = true;

#ifdef MRF_IO_SCENEPARSER_DEBUG
          Quatf q;
          q.fromMatrix(transform_matrix);

          std::cout << "FROM TRANSFORM MATRIX" << std::endl;
          std::cout << "X axis = " << q.getAxisX() << std::endl;
          std::cout << "Y axis =  " << q.getAxisY() << std::endl;
          std::cout << "Z axis =  " << q.getAxisZ() << std::endl;
          std::cout << "position =  " << transform_matrix[12] << " " << transform_matrix[13] << " "
                    << transform_matrix[14] << std::endl;
          std::cout << "matrix" << std::endl;
          std::cout << transform_matrix << std::endl;
#endif
        }
      }   // end-if a <transform> markup was found
      else
      {
        mrf::gui::fb::Loger::getInstance()->trace(" No transformation found in shape element ");
      }

      // Retreive mesh_ref
      string ref_mesh = "";

      if (shapes->Attribute(SceneLexer::REF_MESH_AT))
      {
        ref_mesh = std::string(shapes->Attribute(SceneLexer::REF_MESH_AT));
        mrf::gui::fb::Loger::getInstance()->trace("Current mesh is an instance of " + ref_mesh);
      }

      //--------------------------------------------------------------
      // Check if there is a material reference by the shape
      //--------------------------------------------------------------
      uint current_material_index = 0;

      if (!retrieveMaterialIndex(shapes, current_material_index))
      {
        mrf::gui::fb::Loger::getInstance()->fatal(
            " Wrong material reference provided ! Can't add shape (" + ref_mesh + ")");
        mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::UNDEFINED_MATERIAL);

        if (shapes->Attribute(SceneLexer::REF_MATERIAL_AT))
        {
          string const ref_material = shapes->Attribute(SceneLexer::REF_MATERIAL_AT);
          mrf::gui::fb::Loger::getInstance()->fatal("Name provided was: " + ref_material);
        }
        //return false;
        continue;
      }

      std::shared_ptr<mrf::geom::Intersectable> intersectable;


      //Try to retrieve the associated mesh from a ref_mesh
      if (ref_mesh.size() > 0)
      {
        intersectable = scene.mesh(ref_mesh);
        if (intersectable) mrf::gui::fb::Loger::getInstance()->trace("Successfully instanced " + ref_mesh);
      }
      //otherwise try from a child <mesh>
      else if (shapes->FirstChildElement(SceneLexer::MESH_MK))
      {
        XMLElement *meshes = shapes->FirstChildElement(SceneLexer::MESH_MK)->ToElement();

        if (meshes)
        {
          auto res_pos = addMesh(meshes, scene, current_material_index);

          //retrieve mesh name
          if (res_pos != scene.meshes().end())
          {
            intersectable = res_pos->second;
          }
          else
          {
            mrf::gui::fb::Loger::getInstance()->fatal("FAIL to add mesh in parseShapes()");
            intersectable = nullptr;
          }
        }
      }

      if (intersectable)
      {
        mrf::geom::Shape *shape;
        if (has_global_vertex_transformation)
        {
          shape = new mrf::geom::Shape(ref, scale, current_material_index);
        }
        else
        {
          shape = new mrf::geom::Shape();
          shape->setMaterialIndex(current_material_index);
        }

        shape->setMesh(intersectable);
        scene.addShape(shape);

        is_mat_used[current_material_index] = true;
        auto multi_mat = dynamic_cast<mrf::materials::MultiMaterial *>(&(scene.umaterial(current_material_index)));
        if (multi_mat)
        {
          for (auto &name : multi_mat->getAllNames())
          {
            auto current = _brdf_map.find(name);
            auto end     = _brdf_map.end();
            if (current != end) is_mat_used[current->second] = true;
          }
        }
      }
      else if (shapes->FirstChildElement(SceneLexer::SPHERE_MK))
      {
        mrf::gui::fb::Loger::getInstance()->debug("Found a sphere Markup ");

        XMLElement *a_sphere = shapes->FirstChildElement(SceneLexer::SPHERE_MK);

        // PARSE SPHERE
        if (!addSphere(a_sphere, scene, current_material_index, ref, scale))
        {
          mrf::gui::fb::Loger::getInstance()->warn(" COULD NOT ADD SPHERE SHAPE");
          return false;
        }

        is_mat_used[current_material_index] = true;

        mrf::gui::fb::Loger::getInstance()->debug(" Sphere Shape added ");
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Failed to parse a mesh ! ref_mesh = " + ref_mesh);
      }
    }

    for (int i = 0; i < is_mat_used.size(); ++i)
    {
      if (!is_mat_used[i] && !scene.umaterial(i).isEmissive())
      {
        mrf::gui::fb::Loger::getInstance()->trace(
            "Material " + std::to_string(i) + " is unused by any shape, removing it.");
        scene.markMaterialUnused(i);
      }
    }
  }
  return true;
}

mrf::rendering::mesh_map::iterator
SceneParser::addMesh(XMLElement *a_mesh_element, mrf::rendering::Scene &scene, int current_material_index)
{
  mrf::gui::fb::Loger::getInstance()->trace("One Mesh detected");

  mrf::rendering::mesh_map::iterator res_position;

  //--------------------------------------------------------------
  //Check whether or not this Mesh has a name
  //--------------------------------------------------------------
  string mesh_name = "";

  if (a_mesh_element->Attribute(SceneLexer::NAME_AT))
  {
    mesh_name = a_mesh_element->Attribute(SceneLexer::NAME_AT);
    mrf::gui::fb::Loger::getInstance()->trace("Name for MESH retrieved ", mesh_name);
  }

  //--------------------------------------------------------------
  // Parse now the different type of mesh
  // ONLY One type of mesh is allowed inside a <mesh> </mesh> markup
  //--------------------------------------------------------------
  XMLElement *ply_elem = a_mesh_element->FirstChildElement(SceneLexer::PLY_OBJECT_MK);
  XMLElement *obj_elem = a_mesh_element->FirstChildElement(SceneLexer::OBJ_OBJECT_MK);

  /**
   * \todo Parse Sphere object here
   * //SPHERE
   *bool spheres_parsed_correctly = true;
   *
   *XMLElement* spheres = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK).FirstChildElement(SceneLexer::SHAPES_MK).FirstChildElement(SceneLexer::SPHERE_MK).ToElement();
   *if (spheres)
   *{
   *  mrf::gui::fb::Loger::getInstance()->trace(" Spheres Found in a shape =============> Parsing it (them) ... ");
   *  for (; spheres; spheres = spheres->NextSiblingElement(SceneLexer::SPHERE_MK))
   *  {
   *    spheres_parsed_correctly &= addSphere(spheres, scene);
   *  }
   *}//END OF SPHERES PARSING
   **/

  std::string obj_filename;
  std::string obj_folder_path;
  string      obj_relative_path;

  if (ply_elem)
  {
    obj_relative_path = (ply_elem->Attribute(SceneLexer::FILE_PATH_AT));
  }
  if (obj_elem)
  {
    obj_relative_path = (obj_elem->Attribute(SceneLexer::FILE_PATH_AT));
  }
  mrf::util::StringParsing::fileNameFromAbsPath(obj_relative_path.c_str(), obj_filename);
  mrf::util::StringParsing::getDirectory(obj_relative_path.c_str(), obj_folder_path);
  obj_folder_path       = _current_directory + obj_folder_path;
  std::string full_path = obj_folder_path + obj_filename;


  std::shared_ptr<Intersectable> a_mesh;

  if (ply_elem)   // PLY Element
  {
    mrf::gui::fb::Loger::getInstance()->trace(" PLY OBJECT DETECTED ");
    mrf::gui::fb::Loger::getInstance()->trace(" About to load a PLY File with filename =", obj_relative_path);
    mrf::gui::fb::Loger::getInstance()->trace(" from directory =", _current_directory);

    std::vector<Vec3f>        vertices;
    std::vector<Vec3f>        normals;
    std::vector<Vec3f>        uvs;
    std::vector<unsigned int> enforced_materials;
    std::vector<uint>         index_faces;

    try
    {
      std::ifstream ss(full_path, std::ios::binary);
      if (ss.fail()) throw std::runtime_error("failed to open " + full_path);

      tinyply::PlyFile file;
      file.parse_header(ss);

      mrf::gui::fb::Loger::getInstance()->trace(
          "........................................................................");
      for (auto c : file.get_comments())
        mrf::gui::fb::Loger::getInstance()->trace("Comment: ", c);
      for (auto e : file.get_elements())
      {
        mrf::gui::fb::Loger::getInstance()->trace("element - ", e.name);
        mrf::gui::fb::Loger::getInstance()->trace("\t", e.size);
        for (auto p : e.properties)
        {
          mrf::gui::fb::Loger::getInstance()->trace("\tproperty - ", p.name);
          mrf::gui::fb::Loger::getInstance()->trace("\t", tinyply::PropertyTable[p.propertyType].str);
        }
      }
      mrf::gui::fb::Loger::getInstance()->trace(
          "........................................................................");

      // Tinyply treats parsed data as untyped byte buffers. See below for examples.
      std::shared_ptr<tinyply::PlyData> tiny_vertices, tiny_normals, tiny_faces, tiny_texcoords, tiny_mat;

      // The header information can be used to programmatically extract properties on elements
      // known to exist in the header prior to reading the data. For brevity of this sample, properties
      // like vertex position are hard-coded:
      try
      {
        tiny_vertices = file.request_properties_from_element("vertex", {"x", "y", "z"});
      }
      catch (const std::exception &e)
      {
        mrf::gui::fb::Loger::getInstance()->fatal("tinyply exception: ", e.what());
        return scene.meshes().end();
      }

      try
      {
        tiny_normals = file.request_properties_from_element("vertex", {"nx", "ny", "nz"});
      }
      catch (const std::exception &e)
      {
        mrf::gui::fb::Loger::getInstance()->fatal("tinyply exception: ", e.what());
        // TODO
        return scene.meshes().end();
      }

      try
      {
        tiny_texcoords = file.request_properties_from_element("vertex", {"u", "v"});
      }
      catch (const std::exception &e)
      {
        mrf::gui::fb::Loger::getInstance()->warn("tinyply exception: ", e.what());
        mrf::gui::fb::Loger::getInstance()->warn("attempting to look for (s,t) instead");
        //Allow to load mesh without UVs
        //return scene.meshes().end();
        tiny_texcoords = nullptr;
      }

      if (tiny_texcoords == nullptr)
      {
        try
        {
          tiny_texcoords = file.request_properties_from_element("vertex", {"s", "t"});
        }
        catch (const std::exception &e)
        {
          mrf::gui::fb::Loger::getInstance()->warn("tinyply exception: ", e.what());
          mrf::gui::fb::Loger::getInstance()->warn("attempting to look for (texture_u,texture_v) instead");
          //Allow to load mesh without UVs
          //return scene.meshes().end();
          tiny_texcoords = nullptr;
        }
      }

      if (tiny_texcoords == nullptr)
      {
        try
        {
          tiny_texcoords = file.request_properties_from_element("vertex", {"texture_u", "texture_v"});
        }
        catch (const std::exception &e)
        {
          mrf::gui::fb::Loger::getInstance()->warn("tinyply exception: ", e.what());
          //Allow to load mesh without UVs
          //return scene.meshes().end();
          tiny_texcoords = nullptr;
        }
      }

      // Providing a list size hint (the last argument) is a 2x performance improvement. If you have
      // arbitrary ply files, it is best to leave this 0.
      try
      {
        tiny_faces = file.request_properties_from_element("face", {"vertex_indices"}, 3);
      }
      catch (const std::exception &e)
      {
        mrf::gui::fb::Loger::getInstance()->fatal("tinyply exception: ", e.what());
      }

      try
      {
        tiny_mat = file.request_properties_from_element("face", {"mat_idx"});
      }
      catch (const std::exception &e)
      {
        mrf::gui::fb::Loger::getInstance()->warn("tinyply exception: ", e.what());
        //Allow to load mesh without UVs
        //return scene.meshes().end();
        tiny_mat = nullptr;
      }

      mrf::util::PrecisionTimer timer;
      timer.start();
      file.read(ss);
      timer.stop();
      mrf::gui::fb::Loger::getInstance()->trace("Read ply in " + std::to_string(timer.elapsed()));

      timer.start();
      if (tiny_vertices) mrf::gui::fb::Loger::getInstance()->trace("\tRead vertices:", tiny_vertices->count);
      if (tiny_normals) mrf::gui::fb::Loger::getInstance()->trace("\tRead normals:", tiny_normals->count);
      if (tiny_texcoords) mrf::gui::fb::Loger::getInstance()->trace("\tRead texcoords:", tiny_texcoords->count);
      if (tiny_faces) mrf::gui::fb::Loger::getInstance()->trace("\tRead faces(triangles):", tiny_faces->count);
      if (tiny_mat) mrf::gui::fb::Loger::getInstance()->trace("\tRead material indices per face:", tiny_mat->count);

      assert(tiny_vertices->buffer.size_bytes() == tiny_vertices->count * 3 * sizeof(float));
      assert(tiny_normals->buffer.size_bytes() == tiny_normals->count * 3 * sizeof(float));

      const float *ply_vertices = reinterpret_cast<float *>(tiny_vertices->buffer.get());
      const float *ply_normals  = reinterpret_cast<float *>(tiny_normals->buffer.get());

      vertices.resize(tiny_vertices->count);
      normals.resize(tiny_normals->count);

      for (size_t v_idx = 0; v_idx < vertices.size(); v_idx++)
        vertices[v_idx] = Vec3f(ply_vertices[3 * v_idx], ply_vertices[3 * v_idx + 1], ply_vertices[3 * v_idx + 2]);

      for (size_t i_idx = 0; i_idx < normals.size(); i_idx++)
        normals[i_idx] = Vec3f(ply_normals[3 * i_idx], ply_normals[3 * i_idx + 1], ply_normals[3 * i_idx + 2]);


      if (tiny_texcoords)
      {
        const size_t numUVsBytes = tiny_texcoords->buffer.size_bytes();
        const float *ply_uvs     = reinterpret_cast<float *>(tiny_texcoords->buffer.get());

        uvs.resize(tiny_texcoords->count);

        // MRF Meshes uses vec3 for UV (u,v,w).
        // If the ply file only contains (u,v), we must explicitly add the w component for consistency.
        if (numUVsBytes % 2 == 0)
        {
          assert(numUVsBytes == tiny_texcoords->count * 2 * sizeof(float));

          for (size_t uv_idx = 0; uv_idx < normals.size(); uv_idx++)
            uvs[uv_idx] = Vec3f(ply_uvs[2 * uv_idx], ply_uvs[2 * uv_idx + 1], 0.f);
        }
        else
        {
          assert(numUVsBytes == tiny_texcoords->count * 3 * sizeof(float));

          for (size_t uv_idx = 0; uv_idx < normals.size(); uv_idx++)
            uvs[uv_idx] = Vec3f(ply_uvs[3 * uv_idx], ply_uvs[3 * uv_idx + 1], ply_uvs[3 * uv_idx + 2]);
        }
      }
      else
      {
        uvs.resize(normals.size(), mrf::math::Vec3f());
      }

      if (tiny_faces->t != tinyply::Type::UINT32 && tiny_faces->t != tinyply::Type::INT32)
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" Unsupported PLY index faces type", obj_filename);
        return scene.meshes().end();
      }

      index_faces.resize(3 * tiny_faces->count);

      if (tiny_faces->t != tinyply::Type::UINT32)
      {
        uint *temp_faces_buffer = reinterpret_cast<uint *>(tiny_faces->buffer.get());

        for (uint i_index_faces = 0; i_index_faces < index_faces.size(); i_index_faces++)
        {
          index_faces[i_index_faces] = temp_faces_buffer[i_index_faces];
        }
      }
      else if (tiny_faces->t != tinyply::Type::INT32)
      {
        int *temp_faces_buffer = reinterpret_cast<int *>(tiny_faces->buffer.get());

        for (uint i_index_faces = 0; i_index_faces < index_faces.size(); i_index_faces++)
        {
          index_faces[i_index_faces] = uint(temp_faces_buffer[i_index_faces]);
        }
      }

      if (tiny_mat && tiny_mat->t == tinyply::Type::UINT32)
      {
        assert(tiny_mat->count == tiny_faces->count);
        const size_t numMatBytes = tiny_mat->buffer.size_bytes();
        const uint * ply_mat     = reinterpret_cast<uint *>(tiny_mat->buffer.get());

        enforced_materials.resize(tiny_mat->count);
        for (size_t face_idx = 0; face_idx < enforced_materials.size(); face_idx++)
          enforced_materials[face_idx] = ply_mat[face_idx];
      }
      else
      {
        //Retrieve the current material
        enforced_materials.resize(index_faces.size() / 3);
        std::fill(enforced_materials.begin(), enforced_materials.end(), current_material_index);
      }

      // Check if the mesh has a tangent method attribute

      enum TangentMethods tangent_method;

      if (a_mesh_element->Attribute(SceneLexer::TANGENT_MTHD_AT))
      {
        mrf::gui::fb::Loger::getInstance()->trace("Tangent method detected !");
        string str_tangent_method = a_mesh_element->Attribute(SceneLexer::TANGENT_MTHD_AT);

        if (str_tangent_method == "withUV")
        {
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
          tangent_method = withUV;
        }
        else if (str_tangent_method == "withProjection")
        {
          tangent_method = withProjection;
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
        }
        else if (str_tangent_method == "withJCGT")
        {
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
          tangent_method = withJCGT;
        }
        else if (str_tangent_method == "asBRDFExplorer")
        {
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
          tangent_method = asBRDFExplorer;
        }
        else
          mrf::gui::fb::Loger::getInstance()->fatal("Invalid tangent method name !!");
      }
      else
      {
        tangent_method = withUV;
        mrf::gui::fb::Loger::getInstance()->trace("Tangent method set on fromUV by default");
      }

      a_mesh = std::make_shared<Mesh>(Mesh(vertices, normals, uvs, index_faces, enforced_materials, tangent_method));
    }
    catch (const std::exception &e)
    {
      std::cerr << "Caught tinyply exception: " << e.what() << std::endl;
      return scene.meshes().end();
    }
  }
  else if (obj_elem)
  {
    mrf::gui::fb::Loger::getInstance()->trace("Tracing the New OBJ PARSER ");
    mrf::gui::fb::Loger::getInstance()->trace(" About to load an OBJ File with filename =", obj_relative_path);
    mrf::gui::fb::Loger::getInstance()->trace(" from directory =", _current_directory);


    bool result
        = OBJParser::Instance().load(obj_filename, obj_folder_path, mrf::gui::fb::Loger::getInstance()->level());

    if (result)
    {
      OBJParser::Instance().objscene().triangulateScene();

      bool const uvs_loaded_from_file = (OBJParser::Instance().objscene().uvs().size() > 0) ? true : false;
      mrf::gui::fb::Loger::getInstance()->trace(" UVs loaded from file: ", uvs_loaded_from_file);

      unsigned int const uvs_size_loaded_from_file
          = static_cast<unsigned int>(OBJParser::Instance().objscene().uvs().size());
      mrf::gui::fb::Loger::getInstance()->trace(" #UVs  loaded from file: ", uvs_size_loaded_from_file);

      int        ok = -1;
      bool const uvw_as_tangent
          = (obj_elem->QueryIntAttribute(SceneLexer::UVW_AS_TANGENTS, &ok) == XML_SUCCESS) && (ok == 1);

      mrf::gui::fb::Loger::getInstance()->trace("uvw_as_tangent is ", uvw_as_tangent);
      mrf::gui::fb::Loger::getInstance()->trace(" ok = ", ok);

      if (uvw_as_tangent && OBJParser::Instance().objscene().uvs().size() == 0)
      {
        mrf::gui::fb::Loger::getInstance()->warn(
            " Tangents should be sets from UVs but no UVS found. CHECK YOUR MESH !!!");
      }

      //Temporary vectors to store the linear version of the mesh (no vertex sharing here)
      std::vector<mrf::math::Vec3f> positions;
      std::vector<mrf::math::Vec3f> new_normals;
      std::vector<mrf::math::Vec3f> uvs;
      std::vector<unsigned int>     new_faces;   // Triangular faces
      std::vector<OBJMaterial>      materials;
      std::vector<unsigned int>     per_face_material;   //For each face, stores its material
      std::vector<unsigned int>     material_strides;


      mrf::gui::fb::Loger::getInstance()->trace(" EXPORTING TO linear buffers ");
      OBJParser::Instance()
          .objscene()
          .exportLinearBuffers(positions, new_normals, uvs, new_faces, materials, per_face_material, material_strides);

      mrf::gui::fb::Loger::getInstance()->trace(" AFTER linear buffers export ");
      mrf::gui::fb::Loger::getInstance()->trace(" Nb Vertex positions", positions.size());
      mrf::gui::fb::Loger::getInstance()->trace(" Nb Normals", new_normals.size());
      mrf::gui::fb::Loger::getInstance()->trace(" Nb UVs", uvs.size());


      //if (materials.size() > 0)
      //{
      //  mrf::gui::fb::Loger::getInstance()->warn(" OVERRIDING the materials in the objfile and replacing them with a MRF material");
      //}

      //Retrieve the current material
      std::vector<unsigned int> enforced_materials;
      auto multi_mat = dynamic_cast<mrf::materials::MultiMaterial *>(&(scene.umaterial(current_material_index)));
      if (per_face_material.size() > 0 && multi_mat)
      {
        //Need to check consistency between indices and remap if necessary.

        std::vector<unsigned int> remapping;

        for (int i = 0; i < materials.size(); ++i)
        {
          uint index = 0;
          while (multi_mat->getAllNames()[index] != materials[i].name() && index < multi_mat->getAllNames().size() - 1)
          {
            ++index;
          }
          if (index < multi_mat->getAllNames().size()) remapping.push_back(index);
        }

        enforced_materials.resize(per_face_material.size());

        #pragma omp parallel for schedule(static)
        for (int i = 0; i < enforced_materials.size(); ++i)
        {
          unsigned int prev     = per_face_material[i];
          enforced_materials[i] = remapping[prev];
        }

        assert(enforced_materials.size() == per_face_material.size());
      }
      else
      {
        enforced_materials.resize(new_faces.size() / 3);
        std::fill(enforced_materials.begin(), enforced_materials.end(), current_material_index);
      }

      // Check if the mesh has a tangent method attribute

      enum TangentMethods tangent_method;

      if (a_mesh_element->Attribute(SceneLexer::TANGENT_MTHD_AT))
      {
        mrf::gui::fb::Loger::getInstance()->trace("Tangent method detected !");
        string str_tangent_method = a_mesh_element->Attribute(SceneLexer::TANGENT_MTHD_AT);

        if (str_tangent_method == "withUV")
        {
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
          tangent_method = withUV;
        }
        else if (str_tangent_method == "withProjection")
        {
          tangent_method = withProjection;
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
        }
        else if (str_tangent_method == "withJCGT")
        {
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
          tangent_method = withJCGT;
        }
        else if (str_tangent_method == "asBRDFExplorer")
        {
          mrf::gui::fb::Loger::getInstance()->trace("Tangent method retrieved ! It's ", str_tangent_method);
          tangent_method = asBRDFExplorer;
        }
        else
          mrf::gui::fb::Loger::getInstance()->fatal("Invalid tangent method name !!");
      }
      else
      {
        tangent_method = withUV;
        mrf::gui::fb::Loger::getInstance()->trace("Tangent method set on fromUV by default");
      }

      if (!uvw_as_tangent)
      {
        a_mesh = std::make_shared<Mesh>(Mesh(
            std::move(positions),
            std::move(new_normals),
            std::move(uvs),
            std::move(new_faces),
            std::move(enforced_materials),
            tangent_method));
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->trace(" UVW coordinates are interpreted as tangents and enforced ");
        auto temp_tangents = uvs;
        a_mesh             = std::make_shared<Mesh>(Mesh(
            std::move(positions),
            std::move(new_normals),
            std::move(uvs),
            std::move(temp_tangents),
            std::move(new_faces),
            std::move(enforced_materials),
            tangent_method));
      }
    }   // end of If obj file correctly loaded
    else
    {
      mrf::gui::fb::Loger::getInstance()->fatal(" COULD NOT LOAD OBJ FILE", obj_filename);
      return scene.meshes().end();
    }
  }

  if (a_mesh)
  {
    res_position = scene.addIntersectable(a_mesh, mesh_name);

    if (res_position == scene.meshes().end())
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Can't add the mesh: " + mesh_name);
      mrf::gui::fb::Loger::getInstance()->fatal("From FILE", obj_filename);
      //delete a_mesh;
      //a_mesh = nullptr;
      return res_position;
    }

    mrf::gui::fb::Loger::getInstance()->trace(" One Mesh added in the scene, name =  " + mesh_name);
    mrf::gui::fb::Loger::getInstance()->trace(" Mesh AABBOX", a_mesh->bbox());
    mrf::gui::fb::Loger::getInstance()->trace(" ", a_mesh.get());

    dynamic_cast<Mesh *>(a_mesh.get())->absoluteFilepath() = full_path;

    return res_position;
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" NO DATA INSIDE MESHES !!! ");
  }

  return scene.meshes().end();
}

bool SceneParser::addSphere(
    XMLElement *            a_sphere,
    mrf::rendering::Scene & scene,
    mrf::uint               material_index_from_shape,
    mrf::math::Reff const & a_ref,
    mrf::math::Vec3f const &a_scale)

{
  if (!a_sphere->Attribute(SceneLexer::RADIUS_AT))
  {
    mrf::gui::fb::Loger::getInstance()->warn(" COULD NOT A SPHERE BECAUSE RADIUS ATTRIBUTE IS MISSING");
  }

  // if ( !a_sphere->Attribute(SceneLexer::REF_MATERIAL_AT) )
  // {
  //   mrf::gui::fb::Loger::getInstance()->warn(" NO MATERIAL ASSIGNED TO SPHERE !!!!");
  //   return false;
  // }

  // unsigned int material_index;
  // if (! retrieveMaterialIndex( a_sphere, material_index))
  // {
  //   mrf::gui::fb::Loger::getInstance()->warn(" UNKNOWN MATERIAL !!!!!");
  //   return false;
  // }

  Vec3f a_position;
  float pos_x, pos_y, pos_z;
  pos_x = pos_y = pos_z = 0.0f;

  if (!retrievePositionAttributes(a_sphere, pos_x, pos_y, pos_z))
  {
    if (!retrievePosition(a_sphere, a_position))
    {
      mrf::gui::fb::Loger::getInstance()->warn(" COULD NOT RETRIEVE POSITION MARKUP NOR ATTRIBUTES");
      mrf::gui::fb::Loger::getInstance()->warn(" Assigning default values (0,0,0) ");
    }
  }
  else   // We did find some attributes example : <sphere x="1.0" y="2.0" z="3.0" />
  {
    mrf::gui::fb::Loger::getInstance()->debug("FOund ATTRIBUTES for position of the sphere");
    a_position.set(pos_x, pos_y, pos_z);
  }

  float radius = 1.0f;
  if (a_sphere->QueryFloatAttribute(SceneLexer::RADIUS_AT, &radius) != XML_SUCCESS)
  {
    mrf::gui::fb::Loger::getInstance()->warn("Could not retrieve radius sphere Assigning default value as 1.0");
  }


  //.Scale the radius according to radius
  // Average uniform scale. This is a sphere not an ellipsoid
  radius *= (a_scale.x() + a_scale.y() + a_scale.z()) / 3.0f;

  // Translate Sphere according to Transform retrieved
  a_position += a_ref.getPosition();

  mrf::gui::fb::Loger::getInstance()->debug("After transform Sphere parsed with radius ", radius);
  mrf::gui::fb::Loger::getInstance()->debug(" position = ", a_position);

  Sphere *sphere = new Sphere(material_index_from_shape, a_position, radius);


  scene.addShape(sphere);

  return true;
}

bool SceneParser::retrieveValue(XMLElement *an_element, float &value)
{
  float r;
  int   valueOK = an_element->QueryFloatAttribute(SceneLexer::VALUE_AT, &r);

  if (valueOK != XML_SUCCESS)
  {
    mrf::gui::fb::Loger::getInstance()->warn(" COULD NOT RETRIEVE VALUE Attribute ");
    return false;
  }

  value = r;
  return true;
}

UMat *SceneParser::addEmittance(XMLElement *an_emitt_elem, mrf::rendering::Scene &scene)
{
  if (!an_emitt_elem->Attribute(SceneLexer::NAME_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_LIGHT_MATERIAL_NAME);
    return nullptr;
  }
  string emittance_name(an_emitt_elem->Attribute(SceneLexer::NAME_AT));

  mrf::gui::fb::Loger::getInstance()->trace("emittance_name", emittance_name);

  string emittance_type;
  retrieveEmittanceType(an_emitt_elem, emittance_type);

  mrf::gui::fb::Loger::getInstance()->trace("emittance_type ", emittance_type);


  for (auto registered_mat : _plugins.ids)
  {
    if (emittance_type.compare(registered_mat) == 0)
    {
      mrf::gui::fb::Loger::getInstance()->trace(std::string(registered_mat) + " Detected");
      _brdf_map[emittance_name] = scene.addMaterial(
          _plugins.fcts[std::string(registered_mat)]->parse(an_emitt_elem, emittance_name, _parser));
      return scene.getAllMaterials()[_brdf_map[emittance_name]];
    }
  }

  return nullptr;
}

bool SceneParser::rgbFromMarkup(XMLElement *rgb_markup, mrf::color::Color &c)
{
  //float r = 0.0f;
  //float g = 0.0f;
  //float b = 0.0f;

  //int rOK = rgb_markup->QueryFloatAttribute(SceneLexer::RED_AT, &r);
  //int gOK = rgb_markup->QueryFloatAttribute(SceneLexer::GREEN_AT, &g);
  //int bOK = rgb_markup->QueryFloatAttribute(SceneLexer::BLUE_AT, &b);

  //if ((rOK != XML_SUCCESS) || (gOK != XML_SUCCESS) || (bOK != XML_SUCCESS))
  //{
  //  mrf::gui::fb::Loger::getInstance()->warn("RGB COLOR COULD NOT BE RETRIEVED. DEFAULT COLOR WILL BE USED ");
  //  return false;
  //}

  //c = mrf::color::Color(r, g, b);
  //mrf::gui::fb::Loger::getInstance()->trace(" Color Retrieved : ", c);
  c = _parser.rgbFromMarkup(rgb_markup);
  return true;
}

bool SceneParser::spectrumFromMarkup(XMLElement *spectrum_markup, mrf::radiometry::Spectrum_T<float, float> &s)
{
  //First try to load from file
  //if (spectrum_markup->Attribute(SceneLexer::FILE_PATH_AT))
  //{
  //string spectrum_relative_path = spectrum_markup->Attribute(SceneLexer::FILE_PATH_AT);

  //std::string spectrum_filename;
  //mrf::util::StringParsing::fileNameFromAbsPath(spectrum_relative_path.c_str(), spectrum_filename);
  //std::string spectrum_folder_path;
  //mrf::util::StringParsing::getDirectory(spectrum_relative_path.c_str(), spectrum_folder_path);
  //spectrum_folder_path = _current_directory + spectrum_folder_path;

  //string spectrum_absolut_path = spectrum_folder_path + spectrum_filename;

  //mrf::gui::fb::Loger::getInstance()->trace(" Spectrum load SPD file: ", spectrum_absolut_path);
  //if (!s.loadSPDFile(spectrum_absolut_path))
  //{
  //  mrf::gui::fb::Loger::getInstance()->warn("Cannot load file");
  //}
  //else
  //{
  //  mrf::gui::fb::Loger::getInstance()->trace("Spectrum successfully loaded", s);
  //  return true;
  //}
  //}
  //else

  ////try to load from value in scene file
  //if (!spectrum_markup->Attribute(SceneLexer::VALUE_AT))
  //{
  //  mrf::gui::fb::Loger::getInstance()->warn(" Spectrum has NO VALUE and no file specified");
  //  return false;
  //}

  //s.clear();
  //string spectrum_str = spectrum_markup->Attribute(SceneLexer::VALUE_AT);
  //s.loadFromSPDString(spectrum_str);

  //mrf::gui::fb::Loger::getInstance()->trace(" Spectrum Retrieved : ", s);
  //ParserHelper::setParsingDir(_current_directory);

  //ParserHelper::s_parsing_directory = _current_directory;
  s = _parser.spectrumFromMarkup(spectrum_markup);

  return true;
}

bool SceneParser::retrieveColor(XMLElement *an_elem, mrf::materials::COLOR &c)
{
  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //  const char *elem_str[2] = {SceneLexer::SPECTRUM_MK, SceneLexer::RGB_COLOR_MK};
  //#else
  //  const char *elem_str[2] = {SceneLexer::RGB_COLOR_MK, SceneLexer::SPECTRUM_MK};
  //#endif
  //
  //  XMLElement *color_markup = nullptr;
  //  int         color_type   = 0;
  //  bool        success      = false;
  //
  //  // We try to find the first color markup specified depending on the rendering mode:
  //  //-spectral: look for spectrum first, then for rgb if no spectrum
  //  //-rgb: look for rgb first, then for spectrum if no rgb
  //  for (int i = 0; i < 2; i++)
  //  {
  //    color_markup = an_elem->FirstChildElement(elem_str[i]);
  //
  //    if (color_markup != nullptr)
  //    {
  //      color_type = i;
  //      break;
  //    }
  //  }
  //
  //  if (color_markup == nullptr)
  //  {
  //    // Display more information on the current XML markup and its parent
  //    string const xml_elem_name_wo_color_spd(an_elem->Name());
  //
  //    XMLNode const *parent_node     = an_elem->Parent();
  //    string         xml_parent_name = "";
  //    if (parent_node)
  //    {
  //      XMLElement const *parent_elem = dynamic_cast<XMLElement const *>(parent_node);
  //      xml_parent_name               = string(" from parent ") + string(parent_elem->Name());
  //    }
  //
  //    mrf::gui::fb::Loger::getInstance()->warn(" NO COLOR IN MARKUP " + xml_elem_name_wo_color_spd + xml_parent_name);
  //
  //    return false;
  //  }
  //
  //
  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //  if (color_type == 0)
  //  {
  //    // We have a spectral value
  //    success = spectrumFromMarkup(color_markup, c);
  //  }
  //  else
  //  {
  //    // We have an RGB value, trying to uplift
  //    mrf::gui::fb::Loger::getInstance()->warn("No spectral value found! Basic RGB uplifting");
  //    mrf::color::Color col;
  //    success = rgbFromMarkup(color_markup, col);
  //    c       = mrf::materials::COLOR(col.r(), col.g(), col.b());
  //  }
  //#else
  //  if (color_type == 0)
  //  {
  //    // We have an RGB value
  //    success = rgbFromMarkup(color_markup, c);
  //  }
  //  else
  //  {
  //    // We have a spectral value, converting to RGB
  //    mrf::radiometry::Spectrum_T<float, float> spec;
  //    success = spectrumFromMarkup(color_markup, spec);
  //    mrf::color::_SpectrumConverter<float, float> sc;
  //    c = sc.reflectiveSpectrumToXYZ(
  //        spec,
  //        mrf::radiometry::D_65_SPD,
  //        mrf::radiometry::D_65_FIRST_WAVELENGTH,
  //        mrf::radiometry::D_65_PRECISION,
  //        mrf::radiometry::D_65_ARRAY_SIZE);
  //    c = c.XYZtoLinearRGB();
  //  }
  //#endif
  //
  //  if (!success)
  //  {
  //    string const xml_elem_name_wo_color_spd(an_elem->Name());
  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //    mrf::gui::fb::Loger::getInstance()->fatal(" COULD NOT READ SPECTRUM IN THIS MARKUP " + xml_elem_name_wo_color_spd);
  //#else
  //    mrf::gui::fb::Loger::getInstance()->fatal(" COULD NOT READ COLOR IN THIS MARKUP " + xml_elem_name_wo_color_spd);
  //#endif
  //
  //    return false;
  //  }
  //  return true;

  //ParserHelper::setParsingDir(_current_directory);
  //ParserHelper::s_parsing_directory = _current_directory;
  c = _parser.retrieveColor(an_elem);
  return true;
}

bool SceneParser::retrieveColors(XMLElement *an_elem, std::vector<mrf::materials::COLOR> &colors)
{
  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //  const char *elem_str[2] = {SceneLexer::SPECTRUM_MK, SceneLexer::RGB_COLOR_MK};
  //#else
  //  const char *elem_str[2] = {SceneLexer::RGB_COLOR_MK, SceneLexer::SPECTRUM_MK};
  //#endif
  //
  //  for (int i = 0; i < 2; i++)
  //  {
  //    for (XMLElement *child = an_elem->FirstChildElement(elem_str[i]); child; child = child->NextSiblingElement())
  //    {
  //      if (string(child->Name()) != string(elem_str[i])) continue;
  //
  //      COLOR current_color;
  //      bool  success = false;
  //
  //#ifdef MRF_RENDERING_MODE_SPECTRAL
  //      if (i == 0)
  //      {
  //        // We have a spectral value
  //        success = spectrumFromMarkup(child, current_color);
  //      }
  //      else
  //      {
  //        // We have an RGB value, trying to uplift
  //        mrf::gui::fb::Loger::getInstance()->warn("No spectral value found! Basic RGB uplifting");
  //        mrf::color::Color col;
  //        success       = rgbFromMarkup(child, col);
  //        current_color = mrf::materials::COLOR(col.r(), col.g(), col.b());
  //      }
  //#else
  //      if (i == 0)
  //      {
  //        // We have an RGB value
  //        success = rgbFromMarkup(child, current_color);
  //      }
  //      else
  //      {
  //        // We have a spectral value, converting to RGB
  //        mrf::radiometry::Spectrum_T<float, float> spec;
  //        success = spectrumFromMarkup(child, spec);
  //        mrf::color::_SpectrumConverter<float, float> sc;
  //        current_color = sc.reflectiveSpectrumToXYZ(
  //            spec,
  //            mrf::radiometry::D_65_SPD,
  //            mrf::radiometry::D_65_FIRST_WAVELENGTH,
  //            mrf::radiometry::D_65_PRECISION,
  //            mrf::radiometry::D_65_ARRAY_SIZE);
  //        current_color = current_color.XYZtoLinearRGB();
  //      }
  //#endif
  //
  //      if (success)
  //      {
  //        colors.push_back(current_color);
  //      }
  //    }
  //
  //    if (colors.size() != 0)
  //    {
  //      return true;
  //    }
  //  }
  //
  //  if (colors.size() == 0)
  //  {
  //    mrf::gui::fb::Loger::getInstance()->fatal(" NO COLORS IN THIS MARKUP ");
  //  }
  //
  //  return false;

  //ParserHelper::setParsingDir(_current_directory);

  //ParserHelper::s_parsing_directory = _current_directory;
  _parser.retrieveColors(an_elem, colors);
  return true;
}

bool SceneParser::retrieveFresnel(XMLElement *an_elem, COLOR &eta, COLOR &k)
{
  return _parser.retrieveFresnel(an_elem, eta, k);
  //bool res = true;
  ////Fresnel
  //XMLElement *sub_spec_elem = an_elem->FirstChildElement(SceneLexer::FRESNEL_MK);
  //if (sub_spec_elem)
  //{
  //  XMLElement *eta_element = sub_spec_elem->FirstChildElement(SceneLexer::ETA_MK);
  //  if (eta_element)
  //  {
  //    retrieveColor(eta_element, eta);
  //    mrf::gui::fb::Loger::getInstance()->trace("Fresnel retrieve color eta=", eta);
  //  }
  //  else
  //  {
  //    res = false;
  //  }

  //  XMLElement *kappa_element = sub_spec_elem->FirstChildElement(SceneLexer::KAPPA_MK);
  //  if (kappa_element)
  //  {
  //    retrieveColor(kappa_element, k);
  //    mrf::gui::fb::Loger::getInstance()->trace("Fresnel retrieve color kappa=", k);
  //  }
  //  else
  //  {
  //    //For compatibility woth old scenes using k instead of kappa
  //    XMLElement *k_element = sub_spec_elem->FirstChildElement(SceneLexer::K_MK);
  //    if (k_element)
  //    {
  //      retrieveColor(k_element, k);
  //      mrf::gui::fb::Loger::getInstance()->trace("Fresnel retrieve color kappa=", k);
  //    }
  //    else
  //    {
  //      res = false;
  //    }
  //  }
  //}
  //else
  //{
  //  res = false;
  //}

  //if (res)
  //{
  //  mrf::gui::fb::Loger::getInstance()->trace(" Fresnel eta = ", eta);
  //  mrf::gui::fb::Loger::getInstance()->trace(" Fresnel k = ", k);
  //}

  //return res;
}

bool SceneParser::retrieveReflectivity(XMLElement *an_element, float &reflectivity)
{
  //XMLElement *elem = an_element->FirstChildElement(SceneLexer::ALBEDO_MK);

  //if (!elem || !retrieveValue(elem, reflectivity))
  //{
  //  mrf::gui::fb::Loger::getInstance()->warn(" COULD NOT RETRIEVE REFLECTIVITY. DEFAULT VALUE WILL BE USED ");
  //  return false;
  //}
  //assert(reflectivity >= 0.0);
  //assert(reflectivity <= 1.0);
  return _parser.retrieveReflectivity(an_element, reflectivity);
}

//Light MANAGEMENT
bool SceneParser::addPointLight(XMLElement *an_element, mrf::rendering::Scene &scene)
{
  Vec3f position;
  //We dont check return value because default values exist for PLight class
  retrievePosition(an_element, position);
  mrf::gui::fb::Loger::getInstance()->trace("Point light position is :", position);

  XMLElement *emittance_element = an_element->FirstChildElement(SceneLexer::EMITTANCE_MK);

  if (!emittance_element)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_LIGHT_MATERIAL);
    return false;
  }

  auto emittance_material = addEmittance(emittance_element, scene);

  if (!emittance_material)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(
        " ABORTING while trying to add a Point Light: could not retrieve the emittance.");
    return false;
  }

  //retrieve type here to check its compatibility with the point light source
  string emittance_type;
  retrieveEmittanceType(emittance_element, emittance_type);

  //TODO find a way to check this properly
  //if (emittance_type.compare(EmittanceTypes::CONIC) == 0 || emittance_type.compare(EmittanceTypes::DIRAC) == 0
  //    || emittance_type.compare(EmittanceTypes::UNIFORM) == 0)
  {
    Point *a_point = new Point(position.x(), position.y(), position.z());
    Light *a_light = new Light(a_point, (Emittance *)emittance_material, scene.umatSize() - 1, Light::POINT_LIGHT);

    scene.addPointLight(a_light);
  }
  //else
  //{
  //  mrf::gui::fb::Loger::getInstance()->fatal(" EMITTANCE TYPE NOT SUPPORTED FOR PointLight source");
  //  return false;
  //}

  mrf::gui::fb::Loger::getInstance()->trace(" UMat size value is : ", scene.umatSize());
  mrf::gui::fb::Loger::getInstance()->info(" One Point Light added ");
  return true;
}

bool SceneParser::addQuadLight(XMLElement *an_area_elem, mrf::rendering::Scene &scene)
{
  XMLElement *emittance_element = an_area_elem->FirstChildElement(SceneLexer::EMITTANCE_MK);


  if (!emittance_element)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_LIGHT_MATERIAL);
    return false;
  }

  UMat *emittance_material = addEmittance(emittance_element, scene);

  if (!emittance_material)
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Can't retrieve emittance in area light parsing");
    return false;
  }

  Reff  parsed_ref = Reff::identity();
  Vec3f scale      = Vec3f(1.0f, 1.0f, 1.0f);

  Mat4f transform_matrix;

  //Check if a transformations element exists
  XMLElement *transform_elem = an_area_elem->FirstChildElement(SceneLexer::TRANSFORMATIONS_MK);

  if (transform_elem)
  {
    if (retrieveMultipleTransforms(transform_elem, parsed_ref, scale))
    {
      retrieveMultipleTransforms(transform_elem, transform_matrix);
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->trace(
        " No Transformation found in area light, default area light placement: scale 1, looking towards -Y. ");
  }


  //emittance material already added by addEmittance(), it's at scene.umatSize()-1
  RQuad *a_rquad = new RQuad(parsed_ref, scale, 1.0, 1.0, scene.umatSize() - 1);

  //This Surface is a geometry intersectable
  std::string mesh_name = "";

  /**
   * \todo fix this using a shared pointer for lights
   **/
  std::shared_ptr<Intersectable> temp_shared(a_rquad);

  if (scene.addIntersectable(temp_shared, mesh_name) == scene.meshes().end())
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Can't add the rquad: " + mesh_name);
    mrf::gui::fb::Loger::getInstance()->fatal("From add area light");
    delete a_rquad;
    a_rquad = nullptr;
    return false;
  }

  Light *a_light = new Light(a_rquad, (Emittance *)emittance_material, scene.umatSize() - 1, Light::QUAD_LIGHT);

  //It is also a light
  scene.addQuadLight(a_light);

  mrf::gui::fb::Loger::getInstance()->trace(" Surfacic Light ADDED ");

  return true;
}

bool SceneParser::addSphereLight(XMLElement *a_sphere_elem, mrf::rendering::Scene &scene)
{
  XMLElement *emittance_element = a_sphere_elem->FirstChildElement(SceneLexer::EMITTANCE_MK);

  if (!emittance_element)
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Can't retrieve emittance in sphere light parsing");
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_LIGHT_MATERIAL);
    return false;
  }

  UMat *emittance_material = addEmittance(emittance_element, scene);

  if (!emittance_material)
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Can't retrieve emittance in sphere light parsing");
    return false;
  }


  mrf::math::Vec3f position;
  if (!retrievePosition(a_sphere_elem, position))
  {
    mrf::gui::fb::Loger::getInstance()->warn("SPHERE LIGHT: COULD NOT RETRIEVE POSITION");
    return false;
  }

  float radius   = 1.f;
  int   radiusOK = a_sphere_elem->QueryFloatAttribute(SceneLexer::RADIUS_AT, &radius);

  if (radiusOK != XML_SUCCESS)
  {
    mrf::gui::fb::Loger::getInstance()->warn(" SPHERE LIGHT: RADIUS ATTRIBUTE IS MISSING. Set it to 1.0.");
  }


  //emittance material already added by addEmittance(), it's at scene.umatSize()-1
  Sphere *a_sphere = new Sphere(scene.umatSize() - 1, position, radius);

  //This Surface is a geometry intersectable
  std::string mesh_name = "";

  /**
   * \todo fix this using a shared pointer for lights
   **/
  std::shared_ptr<Intersectable> temp_shared(a_sphere);

  if (scene.addIntersectable(temp_shared, mesh_name) == scene.meshes().end())
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Can't add the sphere: " + mesh_name);
    mrf::gui::fb::Loger::getInstance()->fatal("From add sphere light");
    delete a_sphere;
    a_sphere = nullptr;
    return false;
  }

  Light *a_light = new Light(a_sphere, (Emittance *)emittance_material, scene.umatSize() - 1, Light::SPHERE_LIGHT);

  //It is also a light
  scene.addSphereLight(a_light);

  mrf::gui::fb::Loger::getInstance()->trace(" Sphere Light ADDED ");

  return true;
}

bool SceneParser::addDirectionalLight(XMLElement *a_light_elem, mrf::rendering::Scene &scene)
{
  XMLElement *emittance_element = a_light_elem->FirstChildElement(SceneLexer::EMITTANCE_MK);

  if (!emittance_element)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_LIGHT_MATERIAL);
    return false;
  }

  auto emittance_material = addEmittance(emittance_element, scene);

  if (!emittance_material)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(
        " ABORTING while trying to add a Directional Light: could not retrieve the emittance.");
    return false;
  }

  //retrieve type here to check its compatibility with the point light source
  string emittance_type;
  retrieveEmittanceType(emittance_element, emittance_type);

  //if (emittance_type.compare(EmittanceTypes::DIRAC) == 0)
  {
    Vec3f      direction = dynamic_cast<mrf::materials::Emittance *>(emittance_material)->getDirection();
    Direction *geom_dir  = new Direction(direction);
    Light *a_dir_light   = new Light(geom_dir, (Emittance *)emittance_material, scene.umatSize() - 1, Light::DIR_LIGHT);

    scene.addDirLight(a_dir_light);

    mrf::gui::fb::Loger::getInstance()->trace(" DIRECTIONAL Light ADDED ");
    return true;
  }
  //else
  //{
  //  mrf::gui::fb::Loger::getInstance()->fatal(" EMITTANCE TYPE written in the XML file is  NOT SUPPORTED FOR Directionnal light source");
  //  return false;
  //}
  return true;
}   //end of addDirectionalLight Method

bool SceneParser::retrieveEmittanceType(XMLElement *elem, string &emittance_type)
{
  if (!elem->Attribute(SceneLexer::TYPE_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" Emittance HAS NOT TYPE : Assigning default one (Diffuse)");
    return false;
  }

  emittance_type = elem->Attribute(SceneLexer::TYPE_AT);
  return true;
}

bool SceneParser::retrievePositionAttributes(XMLElement *elem, float &pos_x, float &pos_y, float &pos_z)
{
  mrf::math::Vec3f pos;
  bool             ret = _parser.retrievePosition(elem, pos);
  pos_x                = pos.x();
  pos_y                = pos.y();
  pos_z                = pos.z();
  return ret;
}

bool SceneParser::retrievePosition(XMLElement *elem, mrf::math::Vec3f &position)
{
  return _parser.retrievePosition(elem, position);
}

bool SceneParser::retrieveNormal(XMLElement *elem, mrf::math::Vec3f &normal)
{
  mrf::gui::fb::Loger::getInstance()->debug(" Retrieving Normal (Vec3f)  ");


  XMLElement *normal_element = elem->FirstChildElement(SceneLexer::NORMAL_MK);

  normal    = mrf::math::Vec3f(0.0f, 1.0f, 0.0f);
  float n_x = 0.0f;
  float n_y = 1.0f;
  float n_z = 0.0f;
  if (!normal_element)
  {
    mrf::gui::fb::Loger::getInstance()->warn(" No normal for the Element !!!! Assigning default one (0,1,0)");
  }
  else
  {
    int n_X_OK = normal_element->QueryFloatAttribute(SceneLexer::X_AT, &n_x);
    int n_Y_OK = normal_element->QueryFloatAttribute(SceneLexer::Y_AT, &n_y);
    int n_Z_OK = normal_element->QueryFloatAttribute(SceneLexer::Z_AT, &n_z);

    if ((n_X_OK != XML_SUCCESS) || (n_Y_OK != XML_SUCCESS) || (n_Z_OK != XML_SUCCESS))
    {
      mrf::gui::fb::Loger::getInstance()->warn(
          "Normal values could not be retrieved could not be retrieved\n. Assigning default one (0,1,0)");
    }
    else
    {
      normal.set(n_x, n_y, n_z);
    }
  }


  return true;
}

bool SceneParser::retrieveDirection(XMLElement *elem, mrf::math::Vec3f &direction)
{
  XMLElement *direction_element = elem->FirstChildElement(SceneLexer::DIRECTION_MK);


  float pos_x = 0.0f;
  float pos_y = 0.0f;
  float pos_z = 0.0f;

  if (!direction_element)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" No DIRECTION for the Element !!!! ");
    return false;
  }
  else
  {
    int pos_X_OK = direction_element->QueryFloatAttribute(SceneLexer::X_AT, &pos_x);
    int pos_Y_OK = direction_element->QueryFloatAttribute(SceneLexer::Y_AT, &pos_y);
    int pos_Z_OK = direction_element->QueryFloatAttribute(SceneLexer::Z_AT, &pos_z);

    if ((pos_X_OK != XML_SUCCESS) || (pos_Y_OK != XML_SUCCESS) || (pos_Z_OK != XML_SUCCESS))
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Direction values could not be retrieved could not be retrieved\n.");
      return false;
    }
    else
    {
      direction.set(pos_x, pos_y, pos_z);
      float const norm = direction.norm();
      if ((norm != 0.0f) && (norm != 1.0f))
      {
        mrf::gui::fb::Loger::getInstance()->warn(" Vector passed is not a direction. Normalizing it...");
        direction /= norm;
      }
    }
  }

  return true;
}

bool SceneParser::parseResourcesPath(Scene & /*scene*/)
{
  mrf::gui::fb::Loger::getInstance()->trace("Looking for Resources Path ");

  bool status_return = true;

  //Parse envmaps
  XMLElement *ext
      = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK).FirstChildElement(SceneLexer::EXT_RESOURCES_MK).ToElement();
  if (ext && ext->FirstChildElement(SceneLexer::RESOURCES_PATH_MK))
  {
    //Looking for Panoramic Envmap
    XMLElement *elem = ext->FirstChildElement(SceneLexer::RESOURCES_PATH_MK)->ToElement();
    if (elem)
    {
      for (; elem; elem = elem->NextSiblingElement(SceneLexer::RESOURCES_PATH_MK))
      {
        if (elem->Attribute(SceneLexer::PATH_AT))
        {
          string temp_path = elem->Attribute(SceneLexer::PATH_AT);
          if (temp_path.size() > 1 && temp_path[temp_path.size() - 1] != '/')
          {
            temp_path = temp_path + "/";
          }

          _ext_paths.push_back(temp_path);

          mrf::gui::fb::Loger::getInstance()->trace(" Found Ext path: ", temp_path);
        }
        else
        {
          status_return = false;
        }
      }
    }
  }

  return status_return;
}

bool SceneParser::parseExternalResources(Scene & /*scene*/)
{
  mrf::gui::fb::Loger::getInstance()->trace("Looking for External Resources ");

  bool status_return = true;

  //Parse envmaps
  XMLElement *envmaps = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK)
                            .FirstChildElement(SceneLexer::EXT_RESOURCES_MK)
                            .FirstChildElement(SceneLexer::ENVMAPS_MK)
                            .ToElement();
  if (envmaps)
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Found ENVMAPS! Parsing it/them !!! ");

    //Looking for Panoramic Envmap
    XMLElement *panoramic_envmaps = envmaps->FirstChildElement(SceneLexer::PANORAMIC_ENVMAP_MK);
    if (panoramic_envmaps)
    {
      for (; panoramic_envmaps;
           panoramic_envmaps = panoramic_envmaps->NextSiblingElement(SceneLexer::PANORAMIC_ENVMAP_MK))
      {
        status_return &= addPanoramicEnvmap(panoramic_envmaps);
      }
    }

    XMLElement *spherical_envmaps = envmaps->FirstChildElement(SceneLexer::SPHERICAL_ENVMAP_MK);
    if (spherical_envmaps)
    {
      for (; spherical_envmaps;
           spherical_envmaps = spherical_envmaps->NextSiblingElement(SceneLexer::SPHERICAL_ENVMAP_MK))
      {
        status_return &= addSphericalEnvmap(spherical_envmaps);
      }
    }
  }
  return status_return;
}

bool SceneParser::parseEnvmapGenericAttributes(XMLElement *elem, string &name, string &path, float &theta, float &phi)
{
  if (!elem->Attribute(SceneLexer::NAME_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" NO name assigned to this Envmap file. ABORTING !!! ");
    return false;
  }

  name = elem->Attribute(SceneLexer::NAME_AT);
  mrf::gui::fb::Loger::getInstance()->trace("Name for Envmap retrieved ", name);

  if (!elem->Attribute(SceneLexer::FILE_PATH_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" NO FILE PATH SPECIFIED for this envmap. ABORTING !!!");
    return false;
  }

  string env_file = elem->Attribute(SceneLexer::FILE_PATH_AT);

  mrf::gui::fb::Loger::getInstance()->trace(" Found : ENVMAP FILE: ");
  mrf::gui::fb::Loger::getInstance()->trace(env_file);

  if (StringParsing::pathIsAbsolute(env_file))   //Full path to resource has been provided
  {
    if (!mrf::io::file_exists(env_file)) return false;
    path = env_file;
  }
  else   //Trying to locate the file in different directories
  {
    if (!mrf::io::find_file_from_folders(_current_directory, _ext_paths, env_file, path))
    {
      return false;
    }
  }

  mrf::gui::fb::Loger::getInstance()->trace(" Full Path of file is: ", path);

  theta = 0.0f;
  if (elem->Attribute(SceneLexer::THETA_ANGLE_AT))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Found a theta angle for the rotation ");

    int theta_OK = elem->QueryFloatAttribute(SceneLexer::THETA_ANGLE_AT, &theta);

    if (theta_OK != XML_SUCCESS)
    {
      mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve theta angle (Envmap) ");
    }

    mrf::gui::fb::Loger::getInstance()->trace("theta set to:", theta);
  }

  phi = 0.0f;
  if (elem->Attribute(SceneLexer::PHI_ANGLE_AT))
  {
    mrf::gui::fb::Loger::getInstance()->trace(" Found a phi  angle for the rotation ");

    int phi_OK = elem->QueryFloatAttribute(SceneLexer::PHI_ANGLE_AT, &phi);

    if (phi_OK != XML_SUCCESS)
    {
      mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve phi angle (Envmap)");
    }

    mrf::gui::fb::Loger::getInstance()->trace("phi set to:", phi);
  }
  return true;
}

bool SceneParser::addPanoramicEnvmap(XMLElement *elem)
{
  string name, path;
  float  theta, phi;
  if (!parseEnvmapGenericAttributes(elem, name, path, theta, phi))
  {
    return false;
  }

  float gamma = 0.0f;
  if (elem->Attribute(SceneLexer::GAMMA_ANGLE_AT))
  {
    mrf::gui::fb::Loger::getInstance()->info(" Found a gamma  angle for the rotation ");

    int gamma_OK = elem->QueryFloatAttribute(SceneLexer::GAMMA_ANGLE_AT, &gamma);

    if (gamma_OK != XML_SUCCESS)
    {
      mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve gamma angle ");
    }

    mrf::gui::fb::Loger::getInstance()->info("gamma set to:", gamma);
  }


  PanoramicEnvMap *pan_env              = new PanoramicEnvMap(path.c_str(), phi, theta, gamma);
  float            power_scaling_factor = 1.0f;

  if (elem->Attribute(SceneLexer::POWER_SCALING_FACTOR_AT))
  {
    mrf::gui::fb::Loger::getInstance()->info(" Found a Power Scaling Factor Attribute for the Environment Map");

    int pows_OK = elem->QueryFloatAttribute(SceneLexer::POWER_SCALING_FACTOR_AT, &power_scaling_factor);

    if (pows_OK != XML_SUCCESS)
    {
      mrf::gui::fb::Loger::getInstance()->warn(" Could not retrieve Power Scaling Factor ");
    }
  }

  pan_env->setPowerScalingFactor(power_scaling_factor);
  mrf::gui::fb::Loger::getInstance()->trace("Power Scaling Factor set to:", power_scaling_factor);


  //Now STORE IT
  _envmap_map[name] = pan_env;
  return true;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

bool SceneParser::addSphericalEnvmap(XMLElement *elem)
{
  string name, path;
  float  theta, phi;
  if (!parseEnvmapGenericAttributes(elem, name, path, theta, phi))
  {
    return false;
  }

  SphericalEnvMap *sph_env = new SphericalEnvMap(path.c_str(), phi, theta);

  //Now STORE IT
  //Will be used when parsing lights and background
  _envmap_map[name] = sph_env;
  return true;
}

bool SceneParser::parseBackground(mrf::rendering::Scene &scene)
{
  XMLElement *background_elem
      = _doc_handle.FirstChildElement(SceneLexer::SCENE_MK).FirstChildElement(SceneLexer::BCKG_MK).ToElement();
  if (background_elem)
  {
    if (!background_elem->Attribute(SceneLexer::TYPE_AT))
    {
      mrf::gui::fb::Loger::getInstance()->warn(" NO BACKGROUND TYPE FOUND ! DEFAULT BLACK BACKGROUND ASSIGNED ");
    }
    else
    {
      string type_env = background_elem->Attribute(SceneLexer::TYPE_AT);
      mrf::gui::fb::Loger::getInstance()->trace("Background type ");
      mrf::gui::fb::Loger::getInstance()->trace(type_env);

      if (type_env == string(SceneLexer::BCKG_UNIFORM_TYPE_VAL))
      {
        mrf::gui::fb::Loger::getInstance()->trace("Uniform color as background ! ");

        COLOR a_color = _parser.retrieveColor(background_elem, false);

        ColorBckgManager *cbm = new ColorBckgManager(a_color);
        scene.setBckManager(cbm);

        return true;
      }
      else if (type_env == string(SceneLexer::BCKG_ENVMAP_TYPE_VAL))
      {
        mrf::gui::fb::Loger::getInstance()->trace("Background as environment map! ");


        if (!background_elem->Attribute(SceneLexer::REF_ENVMAP_AT))
        {
          mrf::gui::fb::Loger::getInstance()->fatal(" Reference to External Envmap not found ! Ignoring envmap!! ");
          return false;
        }

        string ref_env = background_elem->Attribute(SceneLexer::REF_ENVMAP_AT);
        mrf::gui::fb::Loger::getInstance()->trace("REFERENCE ENV ");
        mrf::gui::fb::Loger::getInstance()->trace(ref_env);


        auto it = _envmap_map.find(ref_env);

        if (it == _envmap_map.end())
        {
          mrf::gui::fb::Loger::getInstance()->fatal(" ENVIRONMENT MAP NOT FOUND! Ignoring envmap!!");
          return false;
        }

        auto envmap_ptr = it->second;

        //now load envmap
        auto error_load = envmap_ptr->load();

        if (error_load != mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR)
        {
          mrf::gui::fb::Loger::getInstance()->fatal(
              "Failed to load the Image Environment Map ",
              envmap_ptr->filePath());
          mrf::gui::fb::Loger::getInstance()->fatal(" error code = ", error_load);
        }
        else   //envmap successfully loaded
        {
          //set it to scene
          scene.setBckManager(envmap_ptr);

          // The scene is now responsible of the deletion of the envmap
          // we can remove it from the map of envmap
          _envmap_map.erase(it);

          //Apply power scaling factor
#ifdef MRF_RENDERING_MODE_SPECTRAL
          // NOTE that the OptiX back-end will apply the scaling power factor later

          // TODO:  Construct a Spectral Image for future CPU BACKENDS
#else
          mrf::image::ColorImage &env_image = envmap_ptr->image();
          env_image *= envmap_ptr->powerScalingFactor();
#endif
          mrf::gui::fb::Loger::getInstance()->trace(" Envmap loaded and constructed path=", envmap_ptr->filePath());
          mrf::gui::fb::Loger::getInstance()->trace(" Maximum Envmap Value is ", envmap_ptr->maxPower());

          return true;
        }
      }
      else
      {
        mrf::gui::fb::Loger::getInstance()->fatal("Envmap Type not supported ! ");
      }
    }
  }


  mrf::gui::fb::Loger::getInstance()->info("No Background found. Assigning a black one");

  ColorBckgManager *cbm = new ColorBckgManager();
  scene.setBckManager(cbm);
  return true;
}

SceneParser::SceneParser(): _doc_handle(nullptr) {}

SceneParser::~SceneParser()
{
  clearEnvmaps();
}

void SceneParser::clearEnvmaps()
{
  for (auto it = _envmap_map.begin(); it != _envmap_map.end(); ++it)
  {
    delete it->second;
  }

  _envmap_map.clear();
}

SceneParser::SceneParser(SceneParser const &ascene): _doc_handle(nullptr) {}

SceneParser &SceneParser::operator=(SceneParser const &)
{
  return *this;
}

void SceneParser::cleanString(std::string &str)
{
  string space(mrf::util::SPACE);
  string double_space(mrf::util::DOUBLE_SPACE);
  string tab(mrf::util::TAB);
  string new_unix_line(NEW_LINE_UNIX);
  string new_win_line(NEW_LINE_WIN);
  string new_mac_line(NEW_LINE_MAC);

  StringParsing::replaceAllOccurences(str, tab, space);
  StringParsing::replaceAllOccurences(str, new_unix_line, space);
  StringParsing::replaceAllOccurences(str, new_mac_line, space);
  StringParsing::replaceAllOccurences(str, new_win_line, double_space);
}

//bool loadScene(rendering::Scene *&scene, std::string const &scene_filename, mrf::gui::fb::Loger::LEVEL loger_level)
//{
//  using namespace mrf::gui::fb;
//
//  mrf::util::PrecisionTimer timer;
//
//  //Load The Scene
//  timer.reset();
//  timer.start();
//
//  SceneParser &parser = SceneParser::Instance(loger_level);
//  scene               = new Scene();
//  if (!parser.loadScene(scene_filename.c_str(), *scene))
//  {
//    cerr << " Scene file: " << scene_filename << " could NOT be loaded." << endl;
//
//    timer.stop();
//    return false;
//  }
//  timer.stop();
//
//  std::cout << " ************************ INFO ************************** " << std::endl;
//  std::cout << "  Time to load scene  :" << timer.elapsed() << " seconds " << std::endl;
//  std::cout << " ******************************************************** \n" << std::endl;
//
//
//  //TEMP TEST IS on envmap
//#if 0
//  auto bck = scene->background();
//  auto envmap = dynamic_cast<mrf::lighting::PanoramicEnvMap*>(bck);
//
//
//
//  if (envmap)
//  {
//    envmap->computeCDF2DAndCDFInverse(100000);
//
//
//    auto path = envmap->filePath();
//
//    auto is_image = envmap->image();
//    auto is_image_frominv_cdf = envmap->image();
//
//
//    auto& rng = mrf::sampling::RandomGenerator::Instance();
//    uint nb_samples = 10000000;
//
//
//    //Try to sample using the cdf
//    for (uint i = 0; i < nb_samples; i++)
//    {
//      float r1 = rng.getFloat();
//      float r2 = rng.getFloat();
//
//      int row_number = int(envmap->_cdf_1d_envmap_rows.inverse(r1));
//      assert(row_number >= 0);
//      assert(row_number < envmap.height());
//
//      int col_number = int(envmap->_cdf_1d_envmap_columns[row_number].inverse(r2));
//
//      assert(col_number >= 0);
//      assert(col_number < envmap.width());
//
//
//#  ifdef MRF_RENDERING_MODE_SPECTRAL
//#  else
//      auto& color = is_image(col_number, row_number);
//      color = mrf::color::Color(0.f,1.f ,0.f);
//#  endif
//      //is_image(col_number, row_number) = color;
//    }
//
//#  ifdef MRF_RENDERING_MODE_SPECTRAL
//#  else
//    is_image.save(scene_filename + "_is_test.exr");
//#  endif
//
//
//
//
//    //Try to sample using the cdf-1
//    auto const & cdf_inv_rows = envmap->getCdfInvRows();
//    auto const & cdf_inv_2d = envmap->getCdfInv2d();
//    auto const & pdf_envmap = envmap->getPdf();
//    for (uint i = 0; i < nb_samples; i++)
//    {
//      float r1 = rng.getFloat();
//      float r2 = rng.getFloat();
//
//      int cdf_inv_x = static_cast<int>( r1 * (cdf_inv_rows.size()-1) );
//      int cdf_inv_y = static_cast<int>( r2 * (cdf_inv_rows.size() - 1));
//
//
//      int row_number = cdf_inv_rows[cdf_inv_x];
//      int col_number = cdf_inv_2d[ row_number*cdf_inv_rows.size() + cdf_inv_y];
//      //float pdf = pdf_envmap[cdf_inv_x];
//
//
//      assert(row_number >= 0);
//      assert(row_number < envmap.height());
//      assert(col_number >= 0);
//      assert(col_number < envmap.width());
//
//
//#  ifdef MRF_RENDERING_MODE_SPECTRAL
//#  else
//      auto& color = is_image_frominv_cdf(col_number, row_number);
//      color = mrf::color::Color(0.f, 1.f, 0.f);
//#  endif
//    }
//
//#  ifdef MRF_RENDERING_MODE_SPECTRAL
//#  else
//    is_image_frominv_cdf.save(scene_filename + "_is_from_cdf_inv_test.exr");
//#  endif
//
//  }
//#endif
//  return true;
//}

}   // namespace io
}   // namespace mrf
