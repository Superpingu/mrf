#pragma once

/**
 *
 * Copyright(C) CNRS. 2015, 2016.
 * Author: Romain Pacnowski: romain.pacanowski@institutoptique.fr
 *
 */

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/io/parsing_errors.hpp>
#include <mrf_core/rendering/camera.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <tinyxml2/tinyxml2.h>

//STL
#include <unordered_map>
#include <iostream>
#include <string>


namespace mrf
{
namespace io
{
/**
 * This Class implements the singleton Pattern and provide functionality to parse
 * an XML file corresponding to a camera file
 *
 * The file extension is supopsed to be .mcf but the parser does not enforce it
 *
 * A camera file contains a least one camera definition and can contain multiple cameras
 *
 */

class MRF_CORE_EXPORT CameraParser
{
  // Attributes
private:
  enum CAMERA_TYPE
  {
    PINHOLE,
    THIN_LENS
  };

  std::map<std::string, CAMERA_TYPE> _string2CameraType_Map = {{"pinhole", PINHOLE}, {"thinlens", THIN_LENS}};

protected:
public:
  static CameraParser &Instance();

  //TODO :  THROW EXECEPTION
  static void
  loadCameras(std::string const &filename, std::unordered_map<std::string, mrf::rendering::Camera *> &cameras);


  //--------------------------------------------------------------------------------------------
  // MEMBER METHODS
  //--------------------------------------------------------------------------------------------

  /**
   * @brief      Parse the given file and fills the given map with the different
   *             Camera found in the file.
   *             The map contains a string ID for each Camera found in the file
   * @throw      exception if any error occurs during parsing
   * @param      filename  the filename to be parsed
   * @param      cameras   the map containing ids and Camera objects that have been
   *             successfully parsed
   */
  //TODO :  THROW EXECEPTION if something wrong happens
  void parseFile(std::string const &filename, std::unordered_map<std::string, mrf::rendering::Camera *> &cameras);

  //Private Methods
private:
  //Hidden due to Singleton Implementation
  CameraParser();
  virtual ~CameraParser();
  CameraParser(CameraParser const &);              // copy ctor is hidden
  CameraParser &operator=(CameraParser const &);   // assign op is hidden
};

}   // namespace io
}   // namespace mrf