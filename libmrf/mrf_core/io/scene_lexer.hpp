/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/io/lexer.hpp>

namespace mrf
{
namespace io
{
/**
 * @brief      Tokens related to scene file
 */
class MRF_CORE_EXPORT SceneLexer: public Lexer
{
public:
  static constexpr char const *const RGB_FILE_PATH_AT      = "file_rgb";
  static constexpr char const *const SPECTRAL_FILE_PATH_AT = "file_spectral";
  static constexpr char const *const FILE_PATH_AT          = "file";

  static constexpr char const *const POSITION_MK = "position";

  static constexpr char const *const NORMAL_MK    = "normal";
  static constexpr char const *const DIRECTION_MK = "direction";

  // NORMAL MAP TYPE DEFINITION
  static constexpr char const *const NM_TYPE_AT = "nmtype";

  //SCENE
  static constexpr char const *const SCENE_MK  = "scene";
  static constexpr char const *const BCKG_MK   = "background";
  static constexpr char const *const ENVMAP_MK = "envmap";

  //GEOMETRY
  static constexpr char const *const MESHES_MK       = "meshes";
  static constexpr char const *const MESH_MK         = "mesh";
  static constexpr char const *const N_MESH_MK       = "nmesh";
  static constexpr char const *const APO_MESH_MK     = "apo_mesh";
  static constexpr char const *const OBJ_SCENE_MK    = "obj_scene";
  static constexpr char const *const VERTICES_MK     = "vertices";
  static constexpr char const *const UVS_MK          = "textures";
  static constexpr char const *const FACES_MK        = "faces";
  static constexpr char const *const TANGENT_MTHD_AT = "tangent_method";

  static constexpr char const *const SHAPES_MK     = "shapes";
  static constexpr char const *const SHAPE_MK      = "shape";
  static constexpr char const *const SHAPE_MESH_MK = "shape_mesh";

  static constexpr char const *const REF_MESH_AT = "ref_mesh";

  static constexpr char const *const SPHERE_MK = "sphere";
  static constexpr char const *const RADIUS_AT = "radius";

  static constexpr char const *const CYLINDER_MK = "cylinder";

  static constexpr char const *const TORUS_MK = "torus";

  // static constexpr char const * const  WIDTH_AT  = "width";
  // static constexpr char const * const  HEIGHT_AT = "height";

  static constexpr char const *const MAPPING_AT          = "mapping";
  static constexpr char const *const PROGRESSIVE_MESH_AT = "progressiveMesh";
  //static constexpr char const * const  IS_REMOTE_AT = "isRemote";
  static constexpr char const *const MESH_TYPE_AT = "mesh_type";

  static constexpr char const *const STORE_NORMAL_AT   = "store_normal";
  static constexpr char const *const STORE_MATERIAL_AT = "store_material";
  static constexpr char const *const REF_MATERIAL_AT   = "ref_material";

  // static constexpr char const * const  X_AT = "x";
  // static constexpr char const * const  Y_AT = "y";
  // static constexpr char const * const  Z_AT = "z";

  static constexpr char const *const UVW_AS_TANGENTS = "uvw_as_tangents";

  //TRANSFORM
  static constexpr char const *const TRANSFORMATIONS_MK = "transformations";
  static constexpr char const *const TRANSFORM_MK       = "transform";
  static constexpr char const *const FACTOR_AT          = "factor";

  static constexpr char const *const ROTATION_MK    = "rotation";
  static constexpr char const *const TRANSLATION_MK = "translation";
  static constexpr char const *const SCALE_MK       = "scale";

  static constexpr char const *const ROT_ANGLE_MK = "angle";
  static constexpr char const *const ROT_AXIS_MK  = "axis";


  //MATERIALS
  static constexpr char const *const MATERIALS_MK     = "materials";
  static constexpr char const *const MATERIAL_MK      = "material";
  static constexpr char const *const MATERIAL_TYPE_AT = "material_type";

  static constexpr char const *const RGB_COLOR_MK = "rgb_color";
  static constexpr char const *const RED_AT       = "r";
  static constexpr char const *const GREEN_AT     = "g";
  static constexpr char const *const BLUE_AT      = "b";

  static constexpr char const *const SPECTRUM_MK = "spectrum";

  static constexpr char const *const ALBEDO_MK = "albedo";
  static constexpr char const *const ALBEDO_AT = "albedo";

  static constexpr char const *const EXTINCTION_CMP_MK = "extinction";
  static constexpr char const *const DIFFUSE_CMP_MK    = "diffuse";
  static constexpr char const *const SPECULAR_CMP_MK   = "specular";
#ifdef MRF_RENDERING_MODE_SPECTRAL
  static constexpr char const *const DIFFUSE_TEXTURE_MK  = "diffuse_texture_spectral";
  static constexpr char const *const SPECULAR_TEXTURE_MK = "specular_texture_spectral";
#else
  static constexpr char const *const DIFFUSE_TEXTURE_MK  = "diffuse_texture";
  static constexpr char const *const SPECULAR_TEXTURE_MK = "specular_texture";
#endif

  static constexpr char const *const UTIA_TEXTURE_MK          = "utia_texture";
  static constexpr char const *const UTIA_INTERPOLATION_AT    = "interpolate";
  static constexpr char const *const UTIA_PARAMETERIZATION_AT = "parameterization";

  static constexpr char const *const UTIA_4D_VAL             = "utia_4d";
  static constexpr char const *const UTIA_2D_VAL             = "utia_2d";
  static constexpr char const *const UTIA_4D_H_VAL           = "utia_halfslice";
  static constexpr char const *const UTIA_4D_H_NL_VAL        = "utia_halfslice_nonlinear";
  static constexpr char const *const RUSINKIEWICZ_4D_VAL     = "rusinkiewicz_4d";
  static constexpr char const *const RUSINKIEWICZ_2D_VAL     = "rusinkiewicz_2d";
  static constexpr char const *const RUSINKIEWICZ_2D_COS_VAL = "rusinkiewicz_2d_cos";
  static constexpr char const *const HEMISPHERICAL_VAL       = "hemispherical";


  static constexpr char const *const UTIA_PARAM_MK   = "utia_parameters";
  static constexpr char const *const UTIA_THETA_V_AT = "theta_v_div";
  static constexpr char const *const UTIA_PHI_V_AT   = "phi_v_div";
  static constexpr char const *const UTIA_THETA_I_AT = "theta_i_div";
  static constexpr char const *const UTIA_PHI_I_AT   = "phi_i_div";

  static constexpr char const *const UTIA_INDEX_MAP_MK = "material_texture";
  static constexpr char const *const UTIA_LIST_MK      = "utia_list";
  static constexpr char const *const UTIA_INDEX_AT     = "index";

  static constexpr char const *const NORMAL_MAP_MK     = "normal_map";
  static constexpr char const *const EXPONENT_MAP_MK   = "exponent_map";
  static constexpr char const *const EXPONENT_AT       = "exponent";
  static constexpr char const *const EXPONENT_X_AT     = "x-exponent";
  static constexpr char const *const EXPONENT_Y_AT     = "y-exponent";
  static constexpr char const *const TEXTURE_REPEAT_AT = "texture_repeat";

  static constexpr char const *const NU_AT      = "nu";
  static constexpr char const *const NV_AT      = "nv";
  static constexpr char const *const FRESNEL_AT = "fresnel";

  static constexpr char const *const TRANS_CMP_MK = "trans";
  static constexpr char const *const IOR_AT       = "ior";

  //For Measured Materials
  static constexpr char const *const ONLY_DIRAC_AT   = "only_dirac";
  static constexpr char const *const ONLY_SCATTER_AT = "only_scatter";
  static constexpr char const *const INTERPOLATE_AT  = "interpolate";


  //For MERL and MATUSIK MATERIALS
  static constexpr char const *const LUMINANCE_MODE_AT = "luminance_mode";

  static constexpr char const *const ALPHA_MK = "alpha";
  //static constexpr char const * const  POWER_MK           = "power";
  static constexpr char const *const F0_MK        = "F0";
  static constexpr char const *const F1_MK        = "F1";
  static constexpr char const *const K_ALPHA_P_MK = "KAlphaP";
  static constexpr char const *const LAMBDA_MK    = "Lambda";
  static constexpr char const *const C_MK         = "c";
  //static constexpr char const * const  C1_MK              = "c1";
  static constexpr char const *const SIGMA_S_MK = "sigma_s";
  static constexpr char const *const K_MK       = "k";
  static constexpr char const *const THETA_O_MK = "theta_o";

  //FOR HAZY GLOSS
  static constexpr char const *const ROUGHNESS_AT   = "roughness";
  static constexpr char const *const ROUGHNESS_X_AT = "roughness_x";
  static constexpr char const *const ROUGHNESS_Y_AT = "roughness_y";

  static constexpr char const *const HAZINESS_AT      = "haziness";
  static constexpr char const *const HAZE_EXTENT_AT   = "haze_extent";
  static constexpr char const *const HAZE_EXTENT_X_AT = "haze_extent_x";
  static constexpr char const *const HAZE_EXTENT_Y_AT = "haze_extent_y";
  static constexpr char const *const DISTRIB_AT       = "distribution";
  static constexpr char const *const SMOOTH_AT        = "smooth";
  static constexpr char const *const REFL_MK          = "reflectivity";
  static constexpr char const *const EDGETINT_MK      = "edgetint";


  static constexpr char const *const FRESNEL_MK = "fresnel";


  //FOR GGX MODEL
  static constexpr char const *const ALPHA_G_AT = "alpha_g";

  //For FresnelGlass Model
  static constexpr char const *const THIN_GLASS_AT = "thin_glass";

  // For Anisotropic GGX Model
  static constexpr char const *const ALPHA_X_AT = "alpha_x";
  static constexpr char const *const ALPHA_Y_AT = "alpha_y";

  //For GGX Diffraction
  static constexpr char const *const GGXD_A_MK = "a";
  static constexpr char const *const GGXD_B_MK = "b";
  static constexpr char const *const GGXD_C_MK = "c";

  //For Low Smooth Model
  static constexpr char const *const LOW_SMOOTH_A_MK   = SceneLexer::GGXD_A_MK;
  static constexpr char const *const LOW_SMOOTH_B_MK   = SceneLexer::GGXD_B_MK;
  static constexpr char const *const LOW_SMOOTH_C_MK   = SceneLexer::GGXD_C_MK;
  static constexpr char const *const LOW_SMOOTH_ETA_MK = "eta";

  //For CTD Model
  static constexpr char const *const CTD_DIFFRACTION_MK         = "diffraction";
  static constexpr char const *const CTD_FRESNEL_MK             = "fresnel";
  static constexpr char const *const CTD_B_MK                   = "b";
  static constexpr char const *const CTD_P_MK                   = "p";
  static constexpr char const *const ENABLE_DIFFUSE_LOBE_AT     = "enable_diffuse_lobe";
  static constexpr char const *const ENABLE_SPECULAR_LOBE_AT    = "enable_specular_lobe";
  static constexpr char const *const ENABLE_DIFFRACTION_LOBE_AT = "enable_diffraction_lobe";

  static constexpr char const *const CHECKERBOARD_REPETITION_AT = "repetition";
  static constexpr char const *const CHECKERBOARD_FREQUENCY_AT  = "frequency";
  static constexpr char const *const CHECKERBOARD_REPEAT_AT     = "repeat";

  //MicroFacet MODEL
  static constexpr char const *const VARIANT_AT = "variant";
  static constexpr char const *const ETA_MK     = "eta";
  static constexpr char const *const KAPPA_MK   = "kappa";

  static constexpr char const *const SIGMA_0_AT  = "sigma_0";
  static constexpr char const *const TAU_AT      = "tau";
  static constexpr char const *const NB_TERMS_AT = "m";

  static constexpr char const *const NANO_SCATTERING_AT = "scattering";
  static constexpr char const *const NANO_DIRAC_AT      = "dirac";

  //Disney Principled Model
  static constexpr char const *const BASE_COLOR_MK     = "base_color";
  static constexpr char const *const ROUGHNESS_MK      = "roughness";
  static constexpr char const *const CLEARCOAT_MK      = "clearcoat";
  static constexpr char const *const CLEARCOATGLOSS_MK = "clearcoatGloss";
  static constexpr char const *const SUBSURFACE_MK     = "subsurface";
  static constexpr char const *const SHEEN_MK          = "sheen";
  static constexpr char const *const SHEENTINT_MK      = "sheenTint";
  static constexpr char const *const SPECULAR_MK       = "specular";
  static constexpr char const *const SPECULARTINT_MK   = "specularTint";
  static constexpr char const *const ANISOTROPIC_MK    = "anisotropic";
  static constexpr char const *const METALLIC_MK       = "metallic";
#ifdef MRF_RENDERING_MODE_SPECTRAL
  static constexpr char const *const BASE_COLOR_TEXTURE_MK  = "base_color_texture_spectral";
#else
  static constexpr char const *const BASE_COLOR_TEXTURE_MK = "base_color_texture";
#endif
  //LIGHTS
  static constexpr char const *const LIGHTS_MK       = "lights";
  static constexpr char const *const POINT_LIGHT_MK  = "point_light";
  static constexpr char const *const AREA_LIGHT_MK   = "area_light";
  static constexpr char const *const SPHERE_LIGHT_MK = "sphere_light";
  static constexpr char const *const DIR_LIGHT_MK    = "dir_light";


  static constexpr char const *const UNIFORM_MK = "uniform";
  static constexpr char const *const POWER_MK   = "power";

  static constexpr char const *const NO_SAMPLES_AT = "ns";


  static constexpr char const *const EMITTANCE_MK = "emittance";
  static constexpr char const *const RADIANCE_MK  = "radiance";

  static constexpr char const *const REF_QUAD_AT = "ref_quad";
  static constexpr char const *const REF_GEOM_AT = "ref_geom";


  //For conic light
  static constexpr char const *const APERTURE_AT = "aperture";

  //External Files
  static constexpr char const *const PLY_OBJECT_MK      = "ply";
  static constexpr char const *const OBJ_OBJECT_MK      = "obj";
  static constexpr char const *const OFF_OBJECT_MK      = "off";
  static constexpr char const *const OBJ_PROG_OBJECT_MK = "objs";


  //EXTERNAL RESOURCES
  static constexpr char const *const EXT_RESOURCES_MK       = "ext_resources";
  static constexpr char const *const RESOURCES_PATH_MK      = "resources_path";
  static constexpr char const *const PATH_AT                = "path";
  static constexpr char const *const ENV_LIGHTS_MK          = "env_lights";
  static constexpr char const *const ENVMAPS_MK             = "envmaps";
  static constexpr char const *const PANORAMIC_ENVMAP_MK    = "panoramic";
  static constexpr char const *const SPHERICAL_ENVMAP_MK    = "spherical";
  static constexpr char const *const PARABOLOIDAL_ENVMAP_MK = "paraboloidal";
  static constexpr char const *const CUBIC_ENVMAP_MK        = "cubic";

#ifdef MRF_RENDERING_MODE_SPECTRAL
  static constexpr char const *const REF_ENVMAP_AT = "ref_envmap_spectral";
#else
  static constexpr char const *const REF_ENVMAP_AT       = "ref_envmap";
#endif
  static constexpr char const *const NORMALIZATION_AT = "normalized";

  //ANGLES
  static constexpr char const *const THETA_ANGLE_AT          = "theta";
  static constexpr char const *const PHI_ANGLE_AT            = "phi";
  static constexpr char const *const GAMMA_ANGLE_AT          = "gamma";
  static constexpr char const *const POWER_SCALING_FACTOR_AT = "power_scaling";


  //CAMERA
  static constexpr char const *const CAM_MK          = "camera";
  static constexpr char const *const ID_AT           = "id";
  static constexpr char const *const CAM_FOVY_MK     = "fovy";
  static constexpr char const *const CAM_LOOKAT_MK   = "lookat";
  static constexpr char const *const CAM_APERTURE_MK = "aperture";
  static constexpr char const *const CAM_POSITION_MK = "position";
  static constexpr char const *const CAM_UP_MK       = "up";

  //Multi material
  static constexpr char const *const TEXTURE_INDEX_MK = "texture";
  static constexpr char const *const MAT_INDEX_AT     = "index";

  //ATTRIBUTE VALUES
  static constexpr char const *const BCKG_UNIFORM_TYPE_VAL = "uniform";
  static constexpr char const *const BCKG_ENVMAP_TYPE_VAL  = "envmap";

  static constexpr char const *const TRUE_VAL  = "true";
  static constexpr char const *const FALSE_VAL = "false";

};   //end of SceneLexer class

}   // namespace io

}   // namespace mrf
