#pragma once
/**
 * Copyright(C) CNRS. 2015, 2016.
 *
 * Author: Romain Pacnowski: romain.pacanowski@institutoptique.fr
 *
 *
 */

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>


namespace mrf
{
namespace io
{
/**
 * @brief      Common tokens for the scene and camera files
 */
class MRF_CORE_EXPORT Lexer
{
public:
  static constexpr char const *const ID_AT   = "id";
  static constexpr char const *const TYPE_AT = "type";

  static constexpr char const *const VALUE_AT = "value";
  static constexpr char const *const NAME_AT  = "name";

  static constexpr char const *const SIZE_MK = "size";
  static constexpr char const *const SIZE_AT = SIZE_MK;

  static constexpr char const *const WIDTH_AT  = "width";
  static constexpr char const *const HEIGHT_AT = "height";

  static constexpr char const *const POSITION_MK = "position";

  static constexpr char const *const X_AT = "x";
  static constexpr char const *const Y_AT = "y";
  static constexpr char const *const Z_AT = "z";
};
}   // namespace io
}   // namespace mrf
