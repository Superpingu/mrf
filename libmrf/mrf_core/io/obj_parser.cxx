template<class FileIOPolicy>
OBJScene &_OBJParser<FileIOPolicy>::objscene()
{
  return _objscene;
}


template<class FileIOPolicy>
void _OBJParser<FileIOPolicy>::resetCurrentStates()
{
  _cOBJFilename      = "";
  _cObjectName       = "";
  _cAbsPathToOBJFile = "";
  _cOBJMaterial      = "";
  _cSmoothMode       = false;

  _cGroups.clear();
  _objscene.clear();
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::load(
    std::string const &        abs_path_obj_filename,
    std::string const &        abs_path_to_objfile,
    mrf::gui::fb::Loger::LEVEL logging_level)
{
  using namespace std;
  using namespace mrf::util;

  //Clear and reset everything
  resetCurrentStates();

  _cOBJFilename      = abs_path_obj_filename;
  _cAbsPathToOBJFile = abs_path_to_objfile;

  std::string const abs_filename_path = _cAbsPathToOBJFile + _cOBJFilename;

  mrf::gui::fb::Loger::getInstance()->debug("Attempting to load ", _cOBJFilename);
  mrf::gui::fb::Loger::getInstance()->debug("From ", abs_filename_path);

  bool status = true;   //Does the reading went good ?

  fstream file_stream;

  file_stream.open(abs_filename_path.c_str(), std::ios::in);

  if (!file_stream.fail() && !file_stream.bad())
  {
    unsigned int line_number = 1;
    while (!file_stream.fail() && !file_stream.eof() && !file_stream.bad())
    {
      std::string line;
      std::getline(file_stream, line);
      std::replace(line.begin(), line.end(), '\t', ' ');   //Tabulations will cause the tokenizer to break
      std::replace(line.begin(), line.end(), '\r', ' ');   //Replace trailing \r from WINDOWS MODE with space


#ifdef MRF_IO_OBJPARSER_DEBUG
      std::cout << "[DEBUG From OBJPARSER] Reading line = " << line << "#end_of_line" << std::endl;
#endif

      if (!mrf::util::StringParsing::isStringEmpty(line) && !isLineAComment(line))
      {
        if (!analyzeOneLine(line, line_number))
        {
#ifdef MRF_IO_OBJPARSER_DEBUG
          std::cout << " status  = " << status << " at " << __FILE__ << " " << __LINE__ << std::endl;
#endif

          std::cout << "[ERROR From OBJPARSER] While reading line " << line_number << std::endl;
          status = false;
          break;
        }
      }

      line_number++;
    }

    if (!file_stream.eof())
    {
      std::cout << "[ERROR From OBJPARSER] Problem while reading the file. ABORTING !!! " << std::endl;
      status = false;
    }

    file_stream.close();
  }
  else
  {
    std::cout << "[ERROR From OBJPARSER] File could not be open. ABORTING !!! " << std::endl;
    status = false;
  }

  file_stream.close();
  return status;
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::analyzeOneLine(std::string const &line, unsigned int /*line_number*/)
{
  using namespace std;
  using namespace mrf::util;

  bool status = true;

  std::vector<std::string> tokens;
  mrf::util::StringParsing::tokenize(line, tokens);

  //Now switch to the appropriate function according to tokens
  if (tokens.size() > 1)
  {
#ifdef MRF_IO_OBJPARSER_DEBUG
    std::cout << __FILE__ << " TRACING TOKENS AT " << __LINE__ << std::endl;
    for (unsigned int i = 0; i < tokens.size(); i++)
    {
      std::cout << tokens[i] << "###end_of_token" << std::endl;
    }
#endif


    if (tokens[0] == ObjLexer::OBJ_OBJECT_NAME)   //Update the current object name
    {
#ifdef MRF_IO_OBJPARSER_DEBUG
      std::cout << " OBJECT NAME DETECTED " << tokens[0] << " " << tokens[1] << std::endl;
#endif

      _cObjectName = tokens[1];

      //If the name of this object is not already stored in thestd::map
      if (_objscene._objectPerFaces.find(_cObjectName) == _objscene._objectPerFaces.end())
      {
        //Store it with an empty list
        std::list<unsigned int> empty_list;
        _objscene._objectPerFaces[_cObjectName] = empty_list;
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_GROUP)   // STORE THE CURRENT GROUPS
    {
      _cGroups.clear();
      for (unsigned int i = 1; i < tokens.size() - 1; i++)
      {
        _cGroups.push_back(tokens[i]);
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_USE_MATERIAL)   // STORE THE CURRENT MATERIAL
    {
      if (_objscene._materials.find(tokens[1]) != _objscene._materials.end())
      {
#ifdef MRF_IO_OBJPARSER_DEBUG
        std::cout << "[INFO] Using material " << tokens[1] << std::endl;
#endif
        _cOBJMaterial = tokens[1];
      }
      else
      {
        std::cout << "[ERROR] Could not find material " << tokens[1] << std::endl;
        status = false;
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_MATERIAL_LIB)   // Parse the material file
    {
      std::cout << "[INFO] Attempting to parse material file: " << tokens[1] << std::endl;
      std::cout << "[INFO] Current Directory: " << _cAbsPathToOBJFile << std::endl;

      mrf::io::OBJMaterialParser &objmp = mrf::io::OBJMaterialParser::Instance();
      status                            = objmp.parse(_cAbsPathToOBJFile, tokens[1], _objscene._materials);

      if (status)   //Store in the material per facesstd::map the name of the different materials
      {
        _objscene.initializeMaterialPerFaces();
        std::cout << "[INFO] Material file parsed successfully " << std::endl;
      }
      else
      {
        std::cout << "[ERROR] Material file could not be loaded " << std::endl;
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_SMOOTHING)
    {
      if (tokens.size() == 2)
      {
        _cSmoothMode = (tokens[1] == ObjLexer::OBJ_ON) ? (true) : false;
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_VERTEX)   // A Vertex has been detected
    {
      mrf::math::Vec4f a_vertex(0.0f, 0.0f, 0.0f, 1.0f);

      if (tokens.size() >= 4)
      {
        status = convertToFloat(tokens[1], tokens[2], tokens[3], a_vertex[0], a_vertex[1], a_vertex[2]);
      }

      if (tokens.size() == 5)   //Homogenous coordinates case
      {
        status &= mrf::util::StringParsing::convertStringToFloat(tokens[4].c_str(), a_vertex[3]);

#ifdef MRF_IO_OBJPARSER_DEBUG
        std::cout << " DETECTED HOMOGENOUS VERTEX COORDINATES " << std::endl;
        std::cout << " WARNING MESSAGE ISSUED FROM " << __FILE__ << " " << __LINE__ << std::endl;
#endif
      }

      if (status)   //If everything went well store the vertex
      {
        _objscene._vertices.push_back(a_vertex);
      }
      else
      {
        std::cout << "[ERROR] Could not convert/store the vertex " << std::endl;
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_VNORMAL)
    {
      mrf::math::Vec3f a_normal;

      if (tokens.size() >= 4)
      {
        status = convertToFloat(tokens[1], tokens[2], tokens[3], a_normal[0], a_normal[1], a_normal[2]);
      }
      else
      {
        status = false;
      }

      if (status)
      {
        _objscene._normals.push_back(a_normal);
      }
      else
      {
        std::cout << "[ERROR] Could not convert/store the normal " << std::endl;
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_VTEXTURE)
    {
      // (u,v,w) coordinates for a texture
      mrf::math::Vec3f a_vtexture(0.0f, 0.0f, 1.0f);   //Putting by default 1.0f to the w coordinates

      if (tokens.size() <= 3)
      {
        status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), a_vtexture[0]);
        status &= mrf::util::StringParsing::convertStringToFloat(tokens[2].c_str(), a_vtexture[1]);
      }
      else if (tokens.size() <= 4)
      {
        status &= mrf::util::StringParsing::convertStringToFloat(tokens[3].c_str(), a_vtexture[2]);
      }
      else
      {
        std::cout << "[ERROR] More than 3 tokens for texture coordinates. SYNTAX ERROR IN THE OBJFILE " << std::endl;
        std::cout << "tokens.size() = " << tokens.size() << std::endl;

        for (unsigned int i = 0; i < tokens.size(); i++)
        {
          std::cout << " tokens[" << i << "] = " << tokens[i] << std::endl;
        }

        status = false;
      }

      if (status)
      {
        _objscene._uvs.push_back(a_vtexture);
      }
      else
      {
        std::cout << "[ERROR] Could not convert/store the texture coordinates " << std::endl;

#ifdef MRF_OBJ_PARSER_DEBUG
        std::cout << __FILE__ << " AT " << __LINE__ << std::endl;
        std::cout << "tokens[0] = " << tokens[0] << "tokens[1] = " << tokens[1] << " tokens[2] = " << tokens[2]
                  << std::endl;
#endif
      }
    }
    else if (tokens[0] == ObjLexer::OBJ_FACE || tokens[0] == ObjLexer::OBJ_FACE2)
    {
      // Remove the first token of the vector tokens and
      // pass the rest of the tokens to the method analyzeLineForFace
      // analyzeLineForFace will store all the vertex information for the current face

      std::vector<std::string>::iterator first_at_it = tokens.begin();
      ++first_at_it;

      std::vector<std::string> sub_tokens(tokens.size() - 1);

      std::copy(first_at_it, tokens.end(), sub_tokens.begin());

#ifdef MRF_OBJ_PARSER_DEBUG
      std::cout << " AT " << __FILE__ << "  " << __LINE__ << std::endl;
      for (unsigned int i = 0; i < sub_tokens.size(); i++)
      {
        std::cout << " sub_tokens[" << i << "] " << sub_tokens[i];
      }
      std::cout << std::endl;
#endif

      status = analyzeLineForFace(sub_tokens);
    }
    else if (tokens[0] == ObjLexer::OBJ_COMMENT)   //IGNORE THIS TYPE OF LINE WITH COMMENT
    {}
    else if (strncmp(&tokens[0][0], ObjLexer::OBJ_COMMENT, 1) == 0)   //IGNORE THIS TYPE OF LINE WITH COMMENT
    {}
    else
    {
      std::cerr << "[ERROR] UNKNOWN or UNSUPPORTED Token:#" << tokens[0] << "#" << std::endl;
      status = false;
    }
  }
  else
  {
    std::cerr << "[ERROR] Strange line in this OBJ file. Unique token retrieved: " << tokens[0] << std::endl;
    status = false;
  }


  return status;
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::analyzeLineForFace(std::vector<std::string> const &tokens)
{
  using namespace std;

  bool status = true;

  OBJFace _cFace;   //Current Face that will be stored

  // Store the elements of the face
  // Each token represents a vertex with its attributes
  for (unsigned int i = 0; i < tokens.size(); i++)
  {
    unsigned int const number_of_slashes = static_cast<uint>(std::count(tokens[i].begin(), tokens[i].end(), '/'));

    if (number_of_slashes == 0)   // Just a vertex index
    {
      unsigned int index_v;
      status = mrf::util::StringParsing::convertStringToUInt(tokens[i].c_str(), index_v);

#ifdef MRF_OBJ_PARSER_DEBUG
      std::cout << " NO SLASH FOR THIS TOKEN. Pushing vertex_index= " << index_v << " as " << index_v - 1 << std::endl;
#endif

      if (status)
      {
        _cFace._vertexIndices.push_back(index_v - 1);
      }
    }
    else if (number_of_slashes == 1)   // Vertex index + Texture coordinate index ==>  v_index/vt_index
    {
      std::vector<std::string> vertex_tokens;
      mrf::util::StringParsing::tokenize(tokens[i], vertex_tokens, "/");

      unsigned int index_v, index_vt;

      status &= mrf::util::StringParsing::convertStringToUInt(vertex_tokens[0], index_v);
      status &= mrf::util::StringParsing::convertStringToUInt(vertex_tokens[1], index_vt);

      if (status)
      {
        _cFace._vertexIndices.push_back(index_v - 1);

        _cFace._textureIndices[i] = index_vt - 1;
        //  _cFace._textureIndices.push_back( index_vt - 1);
      }
    }
    else if (number_of_slashes == 2)   //  v_index//vn_index OR v_index/vt_index/vn_index
    {
      unsigned int const f_pos_slash = static_cast<uint>(tokens[i].find_first_of("/"));   //First position of the slash
      unsigned int const l_pos_slash = static_cast<uint>(tokens[i].find_last_of("/"));    //Last position of the slash

      if ((l_pos_slash - f_pos_slash) == 1)   // Case v_index//vn_index
      {
        std::string const s_v_index  = tokens[i].substr(0, f_pos_slash);
        std::string const s_vn_index = tokens[i].substr(l_pos_slash + 1, tokens[i].size());

        unsigned int index_v, index_vn;

        status &= mrf::util::StringParsing::convertStringToUInt(s_v_index, index_v);
        status &= mrf::util::StringParsing::convertStringToUInt(s_vn_index, index_vn);

        if (status)
        {
          _cFace._vertexIndices.push_back(index_v - 1);

          _cFace._normalIndices[i] = index_vn - 1;
        }
      }
      else   // case v_index/vt_index/vn_index
      {
        std::string const s_v_index  = tokens[i].substr(0, f_pos_slash);
        std::string const s_vt_index = tokens[i].substr(f_pos_slash + 1, l_pos_slash - f_pos_slash - 1);
        std::string const s_vn_index = tokens[i].substr(l_pos_slash + 1, tokens[i].size());

        unsigned int index_v, index_vt, index_vn;

        status &= mrf::util::StringParsing::convertStringToUInt(s_v_index, index_v);
        status &= mrf::util::StringParsing::convertStringToUInt(s_vt_index, index_vt);
        status &= mrf::util::StringParsing::convertStringToUInt(s_vn_index, index_vn);

        if (status)
        {
          _cFace._vertexIndices.push_back(index_v - 1);

          _cFace._textureIndices[i] = index_vt - 1;

          _cFace._normalIndices[i] = index_vn - 1;
        }

#ifdef MRF_OBJ_PARSER_DEBUG
        std::cout << __FILE__ << " " << __LINE__ << std::endl;
        std::cout << " tokens[i] = " << tokens[i] << std::endl;
        std::cout << " f_pos_slash = " << f_pos_slash << std::endl;
        std::cout << " l_pos_slash = " << l_pos_slash << std::endl;
        std::cout << " s_v_index " << s_v_index << std::endl;
        std::cout << " s_vt_index " << s_vt_index << std::endl;
        std::cout << " s_vn_index " << s_vn_index << std::endl;
        std::cout << " index_v " << index_v << std::endl;
        std::cout << " index_vt " << index_vt << std::endl;
        std::cout << " index_vn " << index_vn << std::endl;

#endif
      }
    }
    else
    {
      std::cout << "[ERROR] The number of slashes is incorrect for face number " << _objscene._faces.size()
                << std::endl;
      break;
    }

  }   // end of for each token of the face


  // At this point we can check whether the vertices and other geometric information
  // have been correctly retrieved.
  if (status)
  {
    //Copying information regarding the face
    _cFace._smoothNormal = _cSmoothMode;
    std::copy(_cGroups.begin(), _cGroups.end(), _cFace._groups.begin());

    if (!_cOBJMaterial.empty())   //Can happend when the OBJ File has not any material defined
    {
      _cFace._materialName = _cOBJMaterial;
      // Add this face to the list of faces that have the current material applied to
      if (_objscene._materialPerFaces.find(_cOBJMaterial) != _objscene._materialPerFaces.end())
      {
        _objscene._materialPerFaces[_cOBJMaterial].push_back(static_cast<uint>(_objscene._faces.size()));
      }
      else
      {
        std::cout << "[ERROR] STRANGE Material error at " << __FILE__ << " " << __LINE__ << std::endl;
        status = false;
      }
    }

    if (!_cObjectName.empty())
    {
      _cFace._objectName = _cObjectName;

      if (_objscene._objectPerFaces.find(_cObjectName) != _objscene._objectPerFaces.end())
      {
        _objscene._objectPerFaces[_cObjectName].push_back(static_cast<uint>(_objscene._faces.size()));
      }
      else
      {
        std::cout << "[ERROR] STRANGE OBJectName error at " << __FILE__ << " " << __LINE__ << std::endl;
        status = false;
      }
    }
  }

  // FINALLY STORE THE FACE if everything went good
  if (status)
  {
    _objscene._faces.push_back(_cFace);
  }
  else
  {
    std::cout << "[ERROR] while parsing face number (in file) " << _objscene._faces.size() << std::endl;
  }

  return status;
}


template<class FileIOPolicy>
_OBJParser<FileIOPolicy> &_OBJParser<FileIOPolicy>::Instance()
{
  static _OBJParser theParser;
  return theParser;
}


// Loading methods
template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::load(
    std::string const &        filename,
    std::vector<float> &       vertices,
    std::vector<float> &       v_normals,
    std::vector<float> &       v_texture,
    std::vector<unsigned int> &index_faces,
    bool                       parse_material_lib)
{
  std::vector<unsigned int> materials;


  return load(filename.c_str(), vertices, v_normals, v_texture, materials, index_faces, parse_material_lib);
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::load(
    char const *               filename,
    std::vector<float> &       vertices,
    std::vector<float> &       v_normals,     // normal for the vertex
    std::vector<float> &       v_texture,     // texture coordinates for the vertex
    std::vector<unsigned int> &index_faces,   //only triangles
    bool                       parse_material_lib)
//will be output
{
  std::vector<unsigned int> materials;

  using namespace std;
  using namespace mrf::util;

#ifdef MRF_IO_OBJPARSER_DEBUG
  std::cout << "File : " << __FILE__ << std::endl;
#endif

  bool status = true;   //Does the reading went good ?
  _fileIO.open(filename, std::ios::in);

  if (_fileIO.error())
  {
    std::cerr << " Error when trying to open the file " << filename << std::endl;
    return false;
  }
  else
  {
    unsigned int line_number = 1;
    while (!_fileIO.error() && !_fileIO.eof())
    {
      string line;
      _fileIO.readLine(line);

      if (!mrf::util::StringParsing::isStringEmpty(line) && !isLineAComment(line))
      {
        if (!analyzeOneLine(line, vertices, v_normals, v_texture, materials, index_faces, parse_material_lib))
        {
          std::cerr << "Error while reading line " << line_number << std::endl;
          status = false;
          break;
        }
      }

      line_number++;
    }

    if (!_fileIO.eof())
    {
      std::cerr << "Problem while reading the file " << filename << std::endl;
      status = false;
    }

    _fileIO.close();
  }
  return status;
}


// will process an obj with mtl declaration
template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::load(
    std::string const &    obj_filename,
    std::string const &    obj_file_directory,
    mrf::rendering::Scene &scene,
    std::string const &    material_type,
    bool                   store_normal_per_face)
{
  using namespace std;
  using namespace mrf::util;
  using namespace mrf::geom;

  std::vector<float> vertices;
  std::vector<float> v_normals;   // normal for the vertex
  std::vector<float> v_texture;   // texture coordinates

  //-------------------------------------------------------------------------
  // Parsing Vertex Coordinates, normals and texture coordinates (if any)
  //-------------------------------------------------------------------------

  std::string const obj_file = obj_file_directory + obj_filename;

  if (!parseVertexAndAttributes(obj_file, vertices, v_normals, v_texture))
  {
    return false;
  }

  unsigned int const nb_v  = static_cast<uint>(vertices.size() / 3);
  unsigned int const nb_vt = static_cast<uint>(v_texture.size() / 3);
  unsigned int const nb_vn = static_cast<uint>(v_normals.size() / 3);

  unsigned int nb_f      = 0;
  unsigned int nb_usemtl = 0;
  unsigned int nb_group  = 0;
  std::string  mtl_filename;

  countData(obj_file.c_str(), nb_f, nb_usemtl, mtl_filename, nb_group);

  std::cout << " nb vertices = " << nb_v << std::endl;
  std::cout << " nb normals = " << nb_vn << std::endl;
  std::cout << " nb textures = " << nb_vt << std::endl;
  std::cout << " nb faces = " << nb_f << std::endl;
  std::cout << " nb usemtl = " << nb_usemtl << std::endl;
  std::cout << " mtl_filename = " << mtl_filename << std::endl;
  std::cout << " nb group = " << nb_group << std::endl;


  // Thestd::map holds the id of the material as stored in the Scene Object
  std::map<std::string, unsigned int> materials_map;


  // if a mtl material file is present we process it.
  if (!mtl_filename.empty())
  {
    std::string const full_mtl_file_path = obj_file_directory + mtl_filename;
    std::cout << "full_mtl_file_path :" << full_mtl_file_path << std::endl;

    if (!processMaterialFile(obj_file_directory, mtl_filename, material_type, scene, materials_map))
    {
      return false;
    }
  }
  else
  {
    //-------------------------------------------------------------------------
    // Since no MATERIAL FILE is ASSIGNED
    // We can create the Mesh according to the following rule :
    //
    // if vt_texture.size() != 0   :  create an UV Mesh
    //
    // the type of the associated mesh depends on the store_normal_per_face
    //
    // if store_normal_per_face = false
    //  construct a VertexTMesh
    // else
    //  construct a FaceTMesh
    //
    //-------------------------------------------------------------------------
    //
    // return the status here;
  }

  std::vector<unsigned int> materials;       // material ID associated with each faces (not necessarly used)
  std::vector<unsigned int> index_faces;     // only triangles will be output
  std::vector<unsigned int> index_normals;   // index of the normals associated to a vertex of a face
  std::vector<unsigned int> index_uvs;       // index of the uvs associated to a vertex of a face

  materials.resize(nb_f);

#ifdef DEBUG
  std::cout << "File : " << __FILE__ << std::endl;
#endif

  if (!parseFacesAndMaterial(obj_file, materials_map, index_faces, index_normals, index_uvs, materials))
  {
    return false;
  }

  bool status = true;   //Does the reading went good ?

  //-------------------------------------------------------------------------
  //
  //
  // WE HAVE A MESH MATERIAL WE NEED TO  DECIDE WHAT KIND OF MESH WE NEED
  // TO CONSTRUCT.
  //
  //  if vt_texture.size() != 0   :  create an UV Mesh
  //  if nb_use_material > 1 :
  //   construct a mesh with Material stored per face
  //  else
  //   construct a mesh with a unique material
  //
  //  if nb_group > 1 :  Construct many Meshes ???
  //-------------------------------------------------------------------------

  bool isIntersectable = true;   // TMP

  if (index_uvs.size() > 0)
  {
    if (index_normals.size() > 0)
    {
      //            TMesh* tmesh = new TMesh(vertices, index_faces  );

      //            MatNUVMesh* matnuvmesh = new MatNUVMesh(*tmesh, v_texture, index_uvs, v_normals, index_normals, materials );
      std::cout << "Build a MatNUVMesh" << std::endl;
      if (isIntersectable)
      {
        //               scene.addIntersectable( matnuvmesh );
      }
    }
    else
    {
      std::vector<float> normals;
      mrf::geom::Computation::computeNormals(vertices, index_faces, normals);
      //            VertexTMesh* tmesh = new VertexTMesh(vertices, normals, index_faces  );

      //           MatUVMesh* uvmesh = new MatUVMesh(*tmesh, v_texture, index_uvs, materials );
      std::cout << "Build a MatUVMesh" << std::endl;
      if (isIntersectable)
      {
        //               scene.addIntersectable( uvmesh );
      }
    }
  }
  else
  {
    // MatFTMesh
    if (store_normal_per_face)
    {
      std::cout << "Build a MatFTMesh" << std::endl;
      //            MatFTMesh* mftmesh = new MatFTMesh(vertices, index_faces, materials);

      if (isIntersectable)
      {
        //               scene.addIntersectable( mftmesh );
      }
    }
    else
    {
      // MatFVTMesh
      std::cout << "Build a MatFVTMesh" << std::endl;
      std::vector<float> normals;
      mrf::geom::Computation::computeNormals(vertices, index_faces, normals);
      //           MatFVTMesh* mfvtmesh = new MatFVTMesh(vertices, index_faces, materials);
      //            mfvtmesh->setNormals(normals);

      if (isIntersectable)
      {
        //                scene.addIntersectable( mfvtmesh );
      }
    }
  }

  return status;
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::analyzeTokensForGeometry(
    std::vector<std::string> const &tokens,
    std::vector<float> &            vertices,
    std::vector<float> &            v_normals,
    std::vector<float> &            v_texture)
{
  using namespace std;
  using namespace mrf::util;

  bool status = true;

  if (tokens.size() > 1)
  {
    if (ObjLexer::OBJ_VERTEX == tokens[0])
    {
      if (tokens.size() == 4)
      {
        float x, y, z;

        status = convertToFloat(tokens[1], tokens[2], tokens[3], x, y, z);
        if (status)
        {
          mrf::math::roundToZero(x);
          mrf::math::roundToZero(y);
          mrf::math::roundToZero(z);

          vertices.push_back(x);
          vertices.push_back(y);
          vertices.push_back(z);
        }
      }
      else if (tokens.size() == 5)   //Homogenous coordinates case
      {
        float x, y, z;
        status = getVertex(tokens[1], tokens[2], tokens[3], tokens[4], x, y, z);

        if (status)
        {
          mrf::math::roundToZero(x);
          mrf::math::roundToZero(y);
          mrf::math::roundToZero(z);

          vertices.push_back(x);
          vertices.push_back(y);
          vertices.push_back(z);
        }
      }
      else
      {
        std::cerr << " INVALID vertex definition " << std::endl;
        status = false;
      }
    }
    else if (ObjLexer::OBJ_VNORMAL == tokens[0])
    {
      if (tokens.size() == 4)
      {
        float x, y, z;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], x, y, z);
        if (status)
        {
          v_normals.push_back(x);
          v_normals.push_back(y);
          v_normals.push_back(z);
        }
      }
      else
      {
        std::cerr << " INVALID vertex normal definition " << std::endl;
        status = false;
      }
    }
    else if (ObjLexer::OBJ_VTEXTURE == tokens[0])
    {
      if (tokens.size() == 4)
      {
        float u, v, w;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], u, v, w);
        if (status)
        {
          v_texture.push_back(u);
          v_texture.push_back(v);
          v_texture.push_back(w);
        }
      }
      else if (tokens.size() == 3)
      {
        float u, v;
        status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), u);
        if (status)
        {
#ifdef MRF_OBJ_PARSER_DEBUG
          std::cout << __FILE__ << "  " << __FUNCTION__ << std::endl;
          std::cout << " Pushing one u value for texture coordinate" << std::endl;
          std::cout << "  u = " << u << std::endl;
#endif

          v_texture.push_back(u);
        }

        status = mrf::util::StringParsing::convertStringToFloat(tokens[2].c_str(), v);
        if (status)
        {
#ifdef MRF_OBJ_PARSER_DEBUG
          std::cout << __FILE__ << "  " << __FUNCTION__ << std::endl;
          std::cout << " Pushing one v value for texture coordinate" << std::endl;
          std::cout << " v  = " << v << std::endl;
#endif
          v_texture.push_back(v);
        }

        v_texture.push_back(0.0f);
      }
      else
      {
        std::cerr << " INVALID vertex texture definition " << std::endl;
        status = false;
      }
    }
  }

  return status;
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::analyzeTokensForFacesAndMaterials(
    std::vector<std::string> const &tokens,
    unsigned int                    current_material_id,
    std::vector<unsigned int> &     index_faces,
    std::vector<unsigned int> &     index_normals,
    std::vector<unsigned int> &     index_uvs,
    std::vector<unsigned int> &     materials)
{
  using namespace std;
  using namespace mrf::util;

  bool status = true;

  if (tokens.size() > 1)
  {
    if (tokens.size() == 5)   // QUAD
    {
      std::cout << " Quad detected " << std::endl;

      for (unsigned int i = 1; i < tokens.size(); i++)
      {
        std::vector<std::string> vertex_tokens;
        mrf::util::StringParsing::tokenize(tokens[i], vertex_tokens, "/");

        if (vertex_tokens.size() > 1)
        {
          std::cout << "Warning ... many properties per vertex index" << __FILE__ << " " << __LINE__ << std::endl;
        }

        unsigned int index_v;
        if (mrf::util::StringParsing::convertStringToUInt(vertex_tokens[0].c_str(), index_v))
        {
          if (i == 4)
          {
            unsigned     current_size = static_cast<uint>(index_faces.size());
            unsigned int p_index      = index_faces[current_size - 1];
            unsigned int n_index      = index_faces[current_size - 3];

            //Now Triangulate the Quad !
            index_faces.push_back(p_index);       //index already good
            index_faces.push_back(index_v - 1);   //align it
            index_faces.push_back(n_index);       //index already good
          }
          else
          {
            index_faces.push_back(index_v - 1);
          }
        }
        else
        {
          std::cerr << " Failed to convert an index for a face  " << __FILE__ << " " << __LINE__ << std::endl;
          status = false;
          break;
        }
      }   //end of for loop

      // TODO : CHECK ME :
      unsigned int const idx1 = static_cast<uint>((index_faces.size() / 3) - 1);
      materials[idx1]         = current_material_id;
      materials[idx1 - 1]     = current_material_id;
    }
    else if (tokens.size() == 4)   //TRIANGLE
    {
      for (unsigned int i = 1; i < tokens.size(); i++)
      {
        std::vector<std::string> face_tokens;
        unsigned int             nb_delimiters = 0;

        mrf::util::StringParsing::tokenize(tokens[i], face_tokens, nb_delimiters, "/");


        if ((face_tokens.size() == 1) && (nb_delimiters == 0))
        {
          // We have: v -> f v v v

          unsigned int index_v;
          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[0].c_str(), index_v))
          {
            //OBJ indices start with 1 and not 0
            //We must realign the indices
            index_faces.push_back(index_v - 1);
          }
        }
        else if ((face_tokens.size() == 2) && (nb_delimiters == 1))
        {
          // We have: v/vt -> f v/vt v/vt v/vt

          // process vertex index
          unsigned int index_v;
          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[0].c_str(), index_v))
          {
            //OBJ indices start with 1 and not 0
            //We must realign the indices
            index_faces.push_back(index_v - 1);
          }
          else
          {
            std::cerr << " Failed to convert a vertex index for a face  " << std::endl;
            status = false;
            break;
          }
          // process uv index
          unsigned int index_vt;

          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[1].c_str(), index_vt))
          {
            index_uvs.push_back(index_vt - 1);
          }
          else
          {
            std::cerr << " Failed to convert an uv index for a face  " << std::endl;
            status = false;
            break;
          }
        }
        else if ((face_tokens.size() == 2) && (nb_delimiters == 2))
        {
          // We have: v//vn -> f v//vn v//vn v//vn
          unsigned int index_v;
          // process vertex index
          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[0].c_str(), index_v))
          {
            //OBJ indices start with 1 and not 0
            //We must realign the indices
            index_faces.push_back(index_v - 1);
          }
          else
          {
            std::cerr << " Failed to convert a vertex index for a face  " << std::endl;
            status = false;
            break;
          }
          // process normal index
          unsigned int index_vn;

          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[1].c_str(), index_vn))
          {
            index_normals.push_back(index_vn - 1);
          }
          else
          {
            std::cerr << " Failed to convert a normal index for a face  " << std::endl;
            status = false;
            break;
          }
        }
        else if ((face_tokens.size() == 3) && (nb_delimiters == 2))
        {
          // We have: v/vt/vn -> f v/vt/vn v/vt/vn v/vt/vn
          unsigned int index_v;
          // process vertex index
          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[0].c_str(), index_v))
          {
            //OBJ indices start with 1 and not 0
            //We must realign the indices
            index_faces.push_back(index_v - 1);
          }
          else
          {
            std::cerr << " Failed to convert a vertex index for a face  " << std::endl;
            status = false;
            break;
          }
          // process uv index
          unsigned int index_vt;

          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[1].c_str(), index_vt))
          {
            index_uvs.push_back(index_vt - 1);
          }
          else
          {
            std::cerr << " Failed to convert an uv index for a face  " << std::endl;
            status = false;
            break;
          }
          // process normal index
          unsigned int index_vn;

          if (mrf::util::StringParsing::convertStringToUInt(face_tokens[2].c_str(), index_vn))
          {
            index_normals.push_back(index_vn - 1);
          }
          else
          {
            std::cerr << " Failed to convert a normal index for a face  " << std::endl;
            status = false;
            break;
          }
        }
        else
        {
          std::cerr << "WARNING : PROBLEM WITH FACE PROPERTIES" << std::endl;
          status = false;
          break;
        }
      }   // end of for loop

      // TODO : CHECK ME :
      unsigned int const idx1 = static_cast<uint>((index_faces.size() / 3) - 1);
      materials[idx1]         = current_material_id;
    }
    else
    {
      std::cerr << "WARNING : Faces with more than 4 vertices not yet supported " << std::endl;
      status = false;
    }
  }

  return status;
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::analyzeOneLine(
    std::string &              line,
    std::vector<float> &       vertices,
    std::vector<float> &       v_normals,
    std::vector<float> &       v_texture,
    std::vector<unsigned int> &materials,
    std::vector<unsigned int> &index_faces,
    bool                       parse_material_lib)
{
  using namespace std;
  using namespace mrf::util;

  bool status = true;

  std::vector<std::string> tokens;
  mrf::util::StringParsing::tokenize(line, tokens);

  //Now switch to the appropriate function according to tokens

  if (tokens.size() > 1)
  {
    if (ObjLexer::OBJ_VERTEX == tokens[0])
    {
      if (tokens.size() == 4)
      {
        float x, y, z;

        status = convertToFloat(tokens[1], tokens[2], tokens[3], x, y, z);
        if (status)
        {
          mrf::math::roundToZero(x);
          mrf::math::roundToZero(y);
          mrf::math::roundToZero(z);

          vertices.push_back(x);
          vertices.push_back(y);
          vertices.push_back(z);
        }
      }
      else if (tokens.size() == 5)   //Homogenous coordinates case
      {
        float x, y, z;
        status = getVertex(tokens[1], tokens[2], tokens[3], tokens[4], x, y, z);

        if (status)
        {
          mrf::math::roundToZero(x);
          mrf::math::roundToZero(y);
          mrf::math::roundToZero(z);

          vertices.push_back(x);
          vertices.push_back(y);
          vertices.push_back(z);
        }
      }
      else
      {
        std::cerr << " INVALID vertex definition " << std::endl;
        status = false;
      }
    }
    else if (ObjLexer::OBJ_VNORMAL == tokens[0])
    {
      if (tokens.size() == 4)
      {
        float x, y, z;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], x, y, z);
        if (status)
        {
          mrf::math::roundToZero(x);
          mrf::math::roundToZero(y);
          mrf::math::roundToZero(z);

          v_normals.push_back(x);
          v_normals.push_back(y);
          v_normals.push_back(z);
        }
      }
      else
      {
        std::cerr << " INVALID vertex normal definition " << std::endl;
        status = false;
      }
    }
    else if (ObjLexer::OBJ_VTEXTURE == tokens[0])
    {
      if (tokens.size() == 4)
      {
        float u, v, w;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], u, v, w);
        if (status)
        {
          v_texture.push_back(u);
          v_texture.push_back(v);
          v_texture.push_back(w);
        }
      }
      else if (tokens.size() == 3)
      {
        float u, v;
        status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), u);
        if (status)
        {
#ifdef MRF_OBJ_PARSER_DEBUG
          std::cout << __FILE__ << "  " << __FUNCTION__ << std::endl;
          std::cout << " Pushing one u value for texture coordinate" << std::endl;
          std::cout << "  u = " << u << std::endl;
#endif

          v_texture.push_back(u);
        }

        status = mrf::util::StringParsing::convertStringToFloat(tokens[2].c_str(), v);
        if (status)
        {
#ifdef MRF_OBJ_PARSER_DEBUG
          std::cout << __FILE__ << "  " << __FUNCTION__ << std::endl;
          std::cout << " Pushing one v value for texture coordinate" << std::endl;
          std::cout << " v  = " << v << std::endl;
#endif
          v_texture.push_back(v);
        }

        v_texture.push_back(0.0f);
      }
      else
      {
        std::cerr << " INVALID vertex texture definition " << std::endl;
        status = false;
      }
    }
    else if ((ObjLexer::OBJ_FACE == tokens[0]) || (ObjLexer::OBJ_FACE2 == tokens[0]))
    {
      if (tokens.size() == 5)   // QUAD
      {
        for (unsigned int i = 1; i < tokens.size(); i++)
        {
          std::vector<std::string> vertex_tokens;
          mrf::util::StringParsing::tokenize(tokens[i], vertex_tokens, "/");

          if (vertex_tokens.size() > 1)
          {
#ifdef MRF_OBJ_PARSER_DEBUG
            std::cout << "Warning ... many properties per vertex index" << __FILE__ << "  " << __LINE__ << std::endl;
#endif
          }

          unsigned int index_v;
          if (mrf::util::StringParsing::convertStringToUInt(vertex_tokens[0].c_str(), index_v))
          {
            if (i == 4)
            {
              uint current_size = static_cast<uint>(index_faces.size());
              uint p_index      = index_faces[current_size - 1];
              uint n_index      = index_faces[current_size - 3];

              //Now Triangulate the Quad !
              index_faces.push_back(p_index);       //index already good
              index_faces.push_back(index_v - 1);   //align it
              index_faces.push_back(n_index);       //index already good
            }
            else
            {
              index_faces.push_back(index_v - 1);
            }
          }
          else
          {
            std::cerr << " Failed to convert an index for a face  " << std::endl;
            status = false;
            break;
          }
        }
      }
      else if (tokens.size() == 4)   //TRIANGLE
      {
        for (unsigned int i = 1; i < tokens.size(); i++)
        {
          std::vector<std::string> vertex_tokens;
          mrf::util::StringParsing::tokenize(tokens[i], vertex_tokens, "/");

          if (vertex_tokens.size() > 1)
          {
#ifdef MRF_OBJ_PARSER_DEBUG
            std::cout << "Warning ... many properties par vertex index" << std::endl;
#endif
          }

          unsigned int index_v;
          if (mrf::util::StringParsing::convertStringToUInt(vertex_tokens[0].c_str(), index_v))
          {
            //OBJ indices start with 1 and not 0
            //We must realign the indices
            index_faces.push_back(index_v - 1);
          }
          else
          {
            std::cerr << " Failed to convert an index for a face  " << std::endl;
            status = false;
            break;
          }

        }   // end of for loop
      }
      else
      {
        std::cerr << "WARNING : Faces with more than 4 vertices not yet supported " << std::endl;
      }
    }
    else if (ObjLexer::OBJ_GROUP == tokens[0])
    {
#ifdef MRF_IO_OBJPARSER_DEBUG
      std::cout << " Group detected : NO SUPPORT " << std::endl;
#endif
    }
    else if (ObjLexer::OBJ_USE_MATERIAL == tokens[0])
    {
#ifdef MRF_IO_OBJPARSER_DEBUG
      std::cout << " MATERIAL detected : NO SUPPORT " << std::endl;
#endif
    }
    else if (ObjLexer::OBJ_MATERIAL_LIB == tokens[0] && parse_material_lib)
    {}
    else if (ObjLexer::OBJ_COMMENT == tokens[0])
    {}
    else if (strncmp(&tokens[0][0], ObjLexer::OBJ_COMMENT, 1) == 0)
    {}
    else
    {
      std::cerr << " UNKNOWN or UNSUPPORTED Token " << std::endl;
      std::cerr << " tokens[0] = " << tokens[0] << std::endl;
    }
  }
  else if (ObjLexer::OBJ_GROUP == tokens[0])
  {
    std::cout << " WARNING group name empy !" << std::endl;
    status = true;
  }
  else
  {
    std::cerr << " Strange line in this OBJ file "
              << " Unique token retrieved " << tokens[0] << std::endl;
    status = false;
  }

  return status;
}


template<class FileIOPolicy>
inline bool _OBJParser<FileIOPolicy>::getVertex(
    std::string const &s_x,
    std::string const &s_y,
    std::string const &s_z,
    std::string const &s_w,
    float &            x,
    float &            y,
    float &            z)
{
  using namespace mrf::util;
  float tmp_x, tmp_y, tmp_z;
  bool  status1 = convertToFloat(s_x, s_y, s_z, tmp_x, tmp_y, tmp_z);
  bool  status2 = false;
  if (status1)
  {
    float w;
    bool  status2 = mrf::util::StringParsing::convertStringToFloat(s_w.c_str(), w);
    if (status2)
    {
      x = tmp_x / w;
      y = tmp_y / w;
      z = tmp_z / w;
    }
  }
  return (status1) && (status2);
}


template<class FileIOPolicy>
inline bool _OBJParser<FileIOPolicy>::convertToFloat(
    std::string const &s_x,
    std::string const &s_y,
    std::string const &s_z,
    float &            x,
    float &            y,
    float &            z)
{
  using namespace mrf::util;

  bool status1 = mrf::util::StringParsing::convertStringToFloat(s_x.c_str(), x);
  bool status2 = mrf::util::StringParsing::convertStringToFloat(s_y.c_str(), y);
  bool status3 = mrf::util::StringParsing::convertStringToFloat(s_z.c_str(), z);

  return (status1) && (status2) && (status3);
}


//-------------------------------------------------------------------------
// TODO : This method should return a boolean value
//-------------------------------------------------------------------------
template<class FileIOPolicy>
inline void _OBJParser<FileIOPolicy>::countData(
    char const *  filename,
    unsigned int &nb_f,
    unsigned int &nb_usemtl,
    std::string & mtl_filename,
    unsigned int &nb_group)
{
  _fileIO.open(filename, std::ios::in);

  if (_fileIO.error())
  {
    std::cerr << " Error when trying to open the file " << filename << std::endl;
  }
  else
  {
    while (!_fileIO.error() && !_fileIO.eof())
    {
      std::string line;
      _fileIO.readLine(line);


      if (!mrf::util::StringParsing::isStringEmpty(line) && !isLineAComment(line))
      {
        std::vector<std::string> tokens;
        mrf::util::StringParsing::tokenize(line, tokens);

        //Now switch to the appropriate function according to tokens
        if (tokens.size() > 1)
        {
          if (ObjLexer::OBJ_MATERIAL_LIB == tokens[0])
          {
            mtl_filename = tokens[1];
          }
          // else if ( token::OBJ_VERTEX == tokens[0] )
          // {
          //     nb_v++;
          // }
          // else if ( token::OBJ_VNORMAL == tokens[0] )
          // {
          //     nb_vn++;
          // }
          // else if ( token::OBJ_VTEXTURE == tokens[0] )
          // {
          //     nb_vt++;
          // }
          else if (ObjLexer::OBJ_FACE == tokens[0])
          {
            nb_f++;
          }
          else if (ObjLexer::OBJ_USE_MATERIAL == tokens[0])
          {
            nb_usemtl++;
          }
          if (ObjLexer::OBJ_GROUP == tokens[0])
          {
            nb_group++;
          }

        }   //if

      }   //if

    }   //while

    if (!_fileIO.eof())
    {
      std::cerr << "Problem while reading the file " << filename << std::endl;
    }

    _fileIO.close();

  }   //if
}


template<class FileIOPolicy>
inline bool _OBJParser<FileIOPolicy>::processMaterialFile(
    std::string const &                  directory_path,
    std::string const &                  mtl_file_name,
    std::string const &                  material_type_mode,
    mrf::rendering::Scene &              scene,
    std::map<std::string, unsigned int> &materials_map)

{
  using namespace std;
  using namespace mrf::materials;

  std::cout << " MATERIAL LIBRARY (mtllib) detected " << std::endl;

  std::string mtl_file_name_path(directory_path + mtl_file_name);

  mrf::io::OBJMaterialParserCIO &objmp = mrf::io::OBJMaterialParserCIO::Instance();

  std::map<std::string, OBJMaterial> materials;

  if (!objmp.parse(directory_path, mtl_file_name, materials))
  {
    std::cerr << " ERROR WHILE PARSING MATERIAL " << std::endl;
    return false;
  }


  mrf::materials::MaterialBuilder &matbuilder = mrf::materials::MaterialBuilder::Instance();

  std::map<std::string, OBJMaterial>::iterator it = materials.begin();

  while (it != materials.end())
  {
    //matbuilder
    OBJMaterial const &gm = it->second;
    UMat *             mat;

    if ((gm.ni().avgCmp() != 1.0f) || (gm.tf().avgCmp() != 1.0f))
    {
      // should build a refractive material
      mat = matbuilder.createMaterial(BTDFTypes::FBTDF, it->second);
    }
    else
    {
      //unsigned int idx = (*it).second;
      mat = matbuilder.createMaterial(material_type_mode, it->second);
    }
    materials_map[it->first] = scene.addMaterial(mat);

    it++;
  }

  return true;
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::parseVertexAndAttributes(
    std::string const & filename,
    std::vector<float> &vertices,
    std::vector<float> &v_normals,
    std::vector<float> &v_texture)
{
  bool status = true;

  _fileIO.open(filename.c_str(), std::ios::in);

  if (_fileIO.error())
  {
    std::cerr << " Error when trying to open the file " << filename << std::endl;
    status = false;
  }
  else
  {
    unsigned int line_number = 1;
    while (!_fileIO.error() && !_fileIO.eof())
    {
      std::string line;
      _fileIO.readLine(line);

      if (!mrf::util::StringParsing::isStringEmpty(line) && !isLineAComment(line))
      {
        std::vector<std::string> tokens;
        mrf::util::StringParsing::tokenize(line, tokens);

        status &= analyzeTokensForGeometry(tokens, vertices, v_normals, v_texture);
      }
      line_number++;
    }   //end of while


    if (!_fileIO.eof())
    {
      std::cerr << "Problem while reading the file " << filename << std::endl;
      status = false;
    }
  }   //end of else


  _fileIO.close();
  return status;
}


template<class FileIOPolicy>
bool _OBJParser<FileIOPolicy>::parseFacesAndMaterial(
    std::string const &                        filename,
    std::map<std::string, unsigned int> const &materials_map,
    std::vector<unsigned int> &                index_faces,
    std::vector<unsigned int> &                index_normals,
    std::vector<unsigned int> &                index_uvs,
    std::vector<unsigned int> &                materials)
{
  bool status = true;

  _fileIO.open(filename.c_str(), std::ios::in);

  if (_fileIO.error())
  {
    std::cerr << " Error when trying to open the file " << filename << std::endl;
    status = false;
  }
  else
  {
    unsigned int line_number         = 1;
    int          current_material_id = -1;

    while (!_fileIO.error() && !_fileIO.eof())
    {
      std::string line;
      _fileIO.readLine(line);


      if (!mrf::util::StringParsing::isStringEmpty(line) && !isLineAComment(line))
      {
        std::vector<std::string> tokens;
        mrf::util::StringParsing::tokenize(line, tokens);


        if (ObjLexer::OBJ_USE_MATERIAL == tokens[0])
        {
          std::map<std::string, unsigned int>::const_iterator current = materials_map.find(tokens[1]);
          std::map<std::string, unsigned int>::const_iterator end     = materials_map.end();



          if (current == end)
          {
            std::cerr << " MATERIAL NOT FOUND !!! " << std::endl;
            status &= false;
          }
          else
          {
#ifdef DEBUG
            std::cout << " MATERIAL FOUND " << std::endl;
#endif

            current_material_id = current->second;


#ifdef DEBUG
            std::cout << "  current_material_id = " << current_material_id << std::endl;
#endif
          }
        }
        else if ((ObjLexer::OBJ_FACE2 == tokens[0]) || (ObjLexer::OBJ_FACE == tokens[0]))
        {
          //WE NEED TO HAVE A CURRENT MATERIAL
          assert(current_material_id != -1);

          status &= analyzeTokensForFacesAndMaterials(
              tokens,
              current_material_id,
              index_faces,
              index_normals,
              index_uvs,
              materials);
        }
      }


      line_number++;
    }


    if (!_fileIO.eof())
    {
      std::cerr << "Problem while reading the file " << filename << std::endl;
      status = false;
    }
  }

  _fileIO.close();

  return status;
}


//Hidden methods for singleton method
template<class FileIOPolicy>
_OBJParser<FileIOPolicy>::_OBJParser(): _cSmoothMode(false)
{}


template<class FileIOPolicy>
_OBJParser<FileIOPolicy>::~_OBJParser()
{}


template<class FileIOPolicy>
_OBJParser<FileIOPolicy>::_OBJParser(_OBJParser const &other)
  : _objscene(other._objscene)
  , _cOBJFilename(other._cOBJFilename)
  , _cAbsPathToOBJFile(other._cAbsPathToOBJFile)
  , _cOBJMaterial(other._cOBJMaterial)
  , _cSmoothMode(other._cSmoothMode)
  , _cGroups(other._cGroups)
  , _cObjectName(other._cObjectName)
{}

template<class FileIOPolicy>
_OBJParser<FileIOPolicy> &_OBJParser<FileIOPolicy>::operator=(_OBJParser const &other)
{
  this->_objscene          = other._objscene;
  this->_cOBJFilename      = other._cOBJFilename;
  this->_cAbsPathToOBJFile = other._cAbsPathToOBJFile;
  this->_cOBJMaterial      = other._cOBJMaterial;
  this->_cSmoothMode       = other._cSmoothMode;
  this->_cGroups           = other._cGroups;
  this->_cObjectName       = other._cObjectName;

  return *this;
}
