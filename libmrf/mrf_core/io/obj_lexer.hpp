/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace io
{
class MRF_CORE_EXPORT ObjLexer
{
public:
  static constexpr char const *const OBJ_COMMENT         = "#";
  static constexpr char const *const OBJ_VERTEX_MAP      = "#vm";
  static constexpr char const *const OBJ_VERTEX_MAP_SIZE = "#m";
  static constexpr char const *const OBJ_VERTEX          = "v";
  static constexpr char const *const OBJ_VNORMAL         = "vn";   //Vertex Normal
  static constexpr char const *const OBJ_VTEXTURE        = "vt";   //Vertex coordinate texture

  static constexpr char const *const OBJ_FACE  = "f";
  static constexpr char const *const OBJ_FACE2 = "fo";

  static constexpr char const *const OBJ_GROUP        = "g";
  static constexpr char const *const OBJ_OBJECT_NAME  = "o";
  static constexpr char const *const OBJ_USE_MATERIAL = "usemtl";
  static constexpr char const *const OBJ_MATERIAL_LIB = "mtllib";

  static constexpr char const *const OBJ_SMOOTHING = "s";     // OBJ Smoothing operator for the group "s"
  static constexpr char const *const OBJ_ON        = "on";    // string = "on"
  static constexpr char const *const OBJ_OFF       = "off";   // string = "off"
};
}   // namespace io
}   // namespace mrf