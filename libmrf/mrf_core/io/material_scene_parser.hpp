/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/materials/materials.hpp>
#include <mrf_core/materials/all_materials.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>
#include <mrf_core/io/base_plugin_parser.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <string>
#include <vector>
#include <map>

#include <tinyxml2/tinyxml2.h>


namespace mrf
{
namespace io
{
/**
 * @brief This class is designed to load materials from the XML "materials" markup into MRF representation.
 **/
class MRF_CORE_EXPORT MaterialSceneParser
{
public:
protected:
  tinyxml2::XMLDocument _doc;
  tinyxml2::XMLHandle   _doc_handle;

  std::string _current_directory;

  /**
   * Handling Materials mapping betweeen id stored in the Scene class
   * and their name in the .mrf scene
   * No need to delete the materials in this class
   * they are always added to the scene object. The scene object
   * is responsible of their deletion
   **/
  std::map<std::string, unsigned int> _brdf_map;

  std::vector<mrf::materials::MultiMaterial *> _multi_mat_list;

  /**
   * For external paths
   **/
  std::vector<std::string> _ext_paths;

  //MaterialRegistrator*

  std::vector<std::string>                                        _id_map;
  std::map<std::string, BasePluginParser<mrf::materials::UMat> *> _parse_map;

  ParserHelper _parser;

public:
  MaterialSceneParser();

  MaterialSceneParser(tinyxml2::XMLHandle doc_handle);

  MaterialSceneParser(
      tinyxml2::XMLHandle                                             doc_handle,
      std::vector<std::string>                                        id_fct_map,
      std::map<std::string, BasePluginParser<mrf::materials::UMat> *> parse_fct_map,
      ParserHelper &                                                  parser);

  ~MaterialSceneParser();

  bool parsingScene(mrf::rendering::Scene &scene);

  std::map<std::string, unsigned int> getMaterialMap() { return _brdf_map; }

protected:
  bool parseGlobalMaterials(mrf::rendering::Scene &scene);

  mrf::materials::UMat *addEmittance(tinyxml2::XMLElement *an_emitt_elem, mrf::rendering::Scene &scene);

  bool addMaterial(tinyxml2::XMLElement *a_mat_element, mrf::rendering::Scene &scene);

  bool parseMultiMaterial(tinyxml2::XMLElement *a_mat_element, mrf::materials::MultiMaterial *multi_mat);

  bool parseTexture(tinyxml2::XMLElement *a_mat_element, char const *texture_markup, std::string &path);

  bool retrieveEmittanceType(tinyxml2::XMLElement *elem, std::string &emittance_type);

  bool retrieveMaterialIndex(tinyxml2::XMLElement *elem, unsigned int &material_index) const;
};

}   // namespace io
}   // namespace mrf
