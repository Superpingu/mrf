/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/io/parser_helper.hpp>

#include <string>
#include <vector>
#include <map>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
/**
 * @brief This class is the base for all plugin. Use the template parameter for either UMat (or BRDF, or BSDF or emittance), Light, Background, Camera, etc.
 **/

template<class T>
class BasePluginParser
{
public:
  /*
   * @brief Static method to create an instance of the plugin parser.
   * MUST BE REDECLARED in any inherited class for a specific plugin.
   */
  static BasePluginParser *create() { return nullptr; }

  /**
   * @brief      Abstract method to parse an element from a plugin. Must be implemented by all plugins.
   *
   * @param      element  the element of a markup of the plugin type.
   * @param      name  the name in the markup attribute "name". Mostly useful for material plugins.
   * @param      parser  the instance of the parser helper to use any helper function.
   *
   * @return     an instance of the plugin type, with its properties set.
   */
  virtual T *parse(tinyxml2::XMLElement *element, std::string &name, ParserHelper &parser) = 0;

  /**
   * @brief      Abstract method to parse an element from a plugin. Must be implemented by all plugins.
   *
   * @param      element  the element of a markup of the plugin type.
   * @param      object  an instance of the plugin type to be filled by the plugin parser.
   * @param      parser  the instance of the parser helper to use any helper function.
   *
   * @return     true is the element has been parsed into the object.
   */
  virtual bool parse(tinyxml2::XMLElement *element, T *object, ParserHelper &parser) = 0;

  inline void setCurrentDirectory(std::string dir)
  {
     _current_directory = dir;
  }

protected:
  std::string _current_directory;
};

/**
 * Base for material plugins.
 **/
//typedef MRF_CORE_EXPORT BasePluginParser<mrf::materials::UMat> BaseMaterialParser;

template<class T>
class BaseMaterialParser: public BasePluginParser<mrf::materials::UMat>
{
public:
  /*
   * @brief Static method to create an instance of the plugin parser.
   * MUST BE REDECLARED in any inherited class for a specific plugin.
   */
  static BasePluginParser<mrf::materials::UMat> *create() { return nullptr; }

  virtual mrf::materials::UMat *parse(tinyxml2::XMLElement *a_mat_element, std::string &name, ParserHelper &parser)
  {
    T *mat = new T(name);

    if (!parse(a_mat_element, mat, parser)) return nullptr;

    return mat;
  }

  /**
   * @brief      Abstract method to parse an element from a plugin. Must be implemented by all plugins.
   *
   * @param      element  the element of a markup of the plugin type.
   * @param      object  an instance of the plugin type to be filled by the plugin parser.
   * @param      parser  the instance of the parser helper to use any helper function.
   *
   * @return     true is the element has been parsed into the object.
   */
  virtual bool parse(tinyxml2::XMLElement *element, mrf::materials::UMat *object, ParserHelper &parser) = 0;
};

/**
 * @brief The container struct in which the material plugins are registered.
 * It stores:
 * - the list ID of all register material plugins
 * - the map of parser instances based on IDs.
 **/
struct MaterialPlugins
{
  std::vector<std::string>                                        ids;
  std::map<std::string, BasePluginParser<mrf::materials::UMat> *> fcts;
};

}   // namespace io
}   // namespace mrf
