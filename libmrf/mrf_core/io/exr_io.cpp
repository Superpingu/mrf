/**
 * Copyright(C) INRIA. 2018.
 *
 * Author: Arthur Dufay
 *
 * Modifications made by :
 * Romain Pacanowski added saving of additional information in EXR files
 * Copyright(C) CNRS. 2020
 *
 * Alban Fichet @ institutoptique.fr
 * Cleaned up code and removed some C style allocations
 * Copyright CNRS 2020
 *
 */

#include <mrf_core/io/exr_io.hpp>

#include <cstring>
#include <string>
#include <memory>
#include <cstddef>

namespace mrf
{
namespace io
{
mrf::image::IMAGE_LOAD_SAVE_FLAGS saveAsExr(
    std::string const &file_path,
    std::vector<float> images[3],
    size_t             width,
    size_t             height,
    std::string        additional_information)
{
  EXRHeader header;
  InitEXRHeader(&header);

  if (additional_information != "")
  {
    //ADD in the Header the additional_information string
    header.num_custom_attributes = 1;
    header.custom_attributes     = new EXRAttribute[1];

    EXRAttribute &attrib = header.custom_attributes[0];

    std::string const mrf_info = "mrf_additional_information";
    memcpy(attrib.name, mrf_info.c_str(), sizeof(char *) * mrf_info.size());

    std::string const mrf_info_type = "string";
    memcpy(attrib.type, mrf_info_type.c_str(), sizeof(char *) * mrf_info.size());

    attrib.size = static_cast<int>(additional_information.size());

    unsigned char *tmp_ptr = new unsigned char[additional_information.size()];
    attrib.value           = tmp_ptr;
    memcpy(attrib.value, additional_information.data(), attrib.size * sizeof(unsigned char));
  }

  EXRImage image;
  InitEXRImage(&image);

  image.num_channels           = 3;
  image.width                  = static_cast<int>(width);
  image.height                 = static_cast<int>(height);
  header.num_channels          = image.num_channels;
  header.channels              = new EXRChannelInfo[header.num_channels];
  header.pixel_types           = new int[header.num_channels];
  header.requested_pixel_types = new int[header.num_channels];

  // Must be (A)BGR order, since most of EXR viewers expect this channel order.
  float *image_ptr[3];

  image.images = (unsigned char **)image_ptr;

  for (int i = 0; i < header.num_channels; i++)
  {
    image_ptr[i] = &(images[2 - i].at(0));

    header.channels[i].name[0]      = "BGR"[i];
    header.channels[i].name[1]      = '\0';
    header.pixel_types[i]           = TINYEXR_PIXELTYPE_FLOAT;   // pixel type of input image
    header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT;   // pixel type of output image to be stored in .EXR
  }

  const char *err;
  int         ret = SaveEXRImageToFile(&image, &header, file_path.c_str(), &err);

  if (ret != TINYEXR_SUCCESS)
  {
    return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_ERROR_SAVING_FILE;
  }

  // Lines below can be avoided
  // free(header.channels);
  // free(header.pixel_types);
  // free(header.requested_pixel_types);
  // USE tineyexr directly
  FreeEXRHeader(&header);

  return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

}   // namespace io
}   // namespace mrf
