
template<class FileIOPolicy>
_OBJMaterialParser<FileIOPolicy> &_OBJMaterialParser<FileIOPolicy>::Instance()
{
  static _OBJMaterialParser theParser;
  return theParser;
}


template<class FileIOPolicy>
bool _OBJMaterialParser<FileIOPolicy>::parse(
    std::string const &                 directory_path,
    std::string const &                 mtl_file_name,
    std::map<std::string, OBJMaterial> &materials)
{
  using namespace std;

  //Some initializations
  bool   status = true;   //Went the reading  good ?
  string mtfl_file(directory_path + mtl_file_name);

  std::cout << " Loading Material Filename::::" << mtfl_file << "###### " << std::endl;


  fstream file_stream;

  file_stream.open(mtfl_file.c_str(), std::ios::in);

  if (file_stream.fail() || file_stream.bad())
  {
    std::cout << " Error when trying to open the file " << mtfl_file << std::endl;
    return false;
  }
  else
  {
    unsigned int line_number = 1;
    while (!file_stream.fail() && !file_stream.bad() && !file_stream.eof())
    {
      string line;
      std::getline(file_stream, line);
      std::replace(line.begin(), line.end(), '\t', ' ');   //Tabulations will cause the tokenizer to break
      std::replace(line.begin(), line.end(), '\r', ' ');   //Replace trailing \r from WINDOWS MODE with space

#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << "  line = " << line << std::endl;
#endif

      if (!mrf::util::StringParsing::isStringEmpty(line) && !isLineAComment(line))
      {
        if (!analyzeOneLine(line, directory_path, materials, line_number))
        {
          std::cout << " Error while reading line " << line_number << std::endl;
          status = false;
          break;
        }
      }
      line_number++;
    }   //end of while


    if (!file_stream.eof())
    {
      std::cout << " Problem while reading the file " << mtfl_file << std::endl;
      std::cout << " Problem occured at line : " << line_number << std::endl;
      status = false;
    }

    file_stream.close();
  }
  return status;
}


template<class FileIOPolicy>
bool _OBJMaterialParser<FileIOPolicy>::analyzeOneLine(
    std::string const &                 line,
    std::string const &                 directory_path,
    std::map<std::string, OBJMaterial> &materials,
    unsigned int                        current_line_number)

{
  using namespace std;
  //using namespace mrf::materials;


  bool status = true;

  std::vector<std::string> tokens;
  mrf::util::StringParsing::tokenize(line, tokens);

  //Now switch to the appropriate function according to tokens
  if (tokens.size() > 1)
  {
    if (ObjMaterialLexer::OBJ_MATERIAL_DEF == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " New material definition !!! "
                << " Tokens size is " << tokens.size() << std::endl
                << " material name is " << tokens[1] << std::endl
                << " tokens1 c str " << tokens[1].c_str() << std::endl;
#endif


      //Test if we already have this definition
      std::map<std::string, OBJMaterial>::iterator current = materials.find(tokens[1]);
      std::map<std::string, OBJMaterial>::iterator end     = materials.end();


      if (current != end)
      {
        std::cout << " WARNING : this material name is  already used !!!" << std::endl
                  << " Ignoring this material definition "
                  << " Check line: " << current_line_number << std::endl;
      }
      else
      {
        //adding it
        _current_material_name = tokens[1];

        //Material vector updated
        OBJMaterial g_mat(_current_material_name);
        materials[tokens[1]] = g_mat;
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_KA == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " KA DEFINITION " << std::endl;
#endif

      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            //Update Current Material properties
            /*Color3F emission(r,g,b);
              GenericMaterial* curr_mat = materials[_current_material_counter-1];
              assert(curr_mat);
              curr_mat->( emission );
            */
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << std::endl;
          status = false;
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_KD == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " KD DEFINITION " << std::endl;
      std::cout << "  NUmber of tokens  = " << tokens.size() << std::endl;
#endif


      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            OBJMaterial &curr_mat = materials[_current_material_name];
            curr_mat.setKd(r, g, b);
          }
#ifdef OBJMATERIAL_PARSER_DEBUG
          else
          {
            std::cout << "FAILED TO CONVERT SOME RGB values while parsing KD" << std::endl;
          }
#endif
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << " Check line " << current_line_number << std::endl;
          status = false;
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_KS == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " KS DEFINITION " << std::endl;
#endif

      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            OBJMaterial &curr_mat = materials[_current_material_name];
            curr_mat.setKs(r, g, b);
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << " Check line " << current_line_number << std::endl;
          status = false;
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_KE == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " KE DEFINITION " << std::endl;
#endif

      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            OBJMaterial &curr_mat = materials[_current_material_name];
            curr_mat.setKe(r, g, b);
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << " Check line " << current_line_number << std::endl;
          status = false;
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_TF == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " TF DEFINITION " << std::endl;
#endif

      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported "
                  << "Check line " << current_line_number << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            OBJMaterial &curr_mat = materials[_current_material_name];
            curr_mat.setTf(r, g, b);
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << "Check line " << current_line_number << std::endl;
          status = false;
        }
      }
    }   // end of if OBJ_MATERIAL_TF
    else if (ObjMaterialLexer::OBJ_MATERIAL_NS == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " NS DEFINITION " << std::endl;
#endif

      if (tokens.size() == 4)
      {
        float nr, ng, nb;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], nr, ng, nb);
        if (status)
        {
          OBJMaterial &curr_mat = materials[_current_material_name];
          curr_mat.setNs(nr, ng, nb);
        }
      }
      else if (tokens.size() == 2)
      {
        float ns;
        status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), ns);

        if (status)
        {
          OBJMaterial &curr_mat = materials[_current_material_name];
          curr_mat.setNs(ns);
        }
      }
      else
      {
        std::cout << " Wrong number of token for Specular exponent"
                  << " Expected 1 or 3  got " << tokens.size() << "Check line " << current_line_number << std::endl;
        status = false;
      }

    }   // end of if OBJ_MATERIAL_NS specular exponent
    else if (ObjMaterialLexer::OBJ_MATERIAL_IOR == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " IOR DEFINITION " << std::endl;
#endif

      if (tokens.size() == 4)
      {
        float iorr, iorg, iorb;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], iorr, iorg, iorb);
        if (status)
        {
          OBJMaterial &curr_mat = materials[_current_material_name];
          curr_mat.setNi(iorr, iorg, iorb);
        }
      }
      else if (tokens.size() == 2)
      {
        float ior;
        status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), ior);

        if (status)
        {
          OBJMaterial &curr_mat = materials[_current_material_name];
          curr_mat.setNi(ior);
        }
      }
      else
      {
        std::cout << " Wrong number of token for index of refraction IOR component"
                  << " Expected 1 or 3  got " << tokens.size() << " CHECK LINE" << current_line_number << std::endl;

        status = false;
      }


    }   // end of if OBJ_MATERIAL_IOR index of refraction
    else if (
        ObjMaterialLexer::OBJ_MATERIAL_DISSOLVE == tokens[0] || ObjMaterialLexer::OBJ_MATERIAL_DISSOLVE_2 == tokens[0])
    {
      float d;
      status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), d);
      if (status)
      {
        OBJMaterial &curr_mat = materials[_current_material_name];
        curr_mat.setDissolveCoeff(d);
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_KA == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << "map_Ka definition found " << std::endl;
#endif

      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');
      materials[_current_material_name].setMapKa(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_KD == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << "map_Kd definition found " << std::endl;
#endif

      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');
      materials[_current_material_name].setMapKd(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_KS == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << "map_Ks definition found" << std::endl;
#endif

      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');
      materials[_current_material_name].setMapKs(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_KE == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << "map_Ke definition found " << std::endl;
#endif

      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');
      materials[_current_material_name].setMapKe(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_NS == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << "map_Ns definition found" << std::endl;
#endif

      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');
      materials[_current_material_name].setMapNs(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (ObjMaterialLexer::OBJ_BUMPMAP == tokens[0] || ObjMaterialLexer::OBJ_BUMPMAP_2 == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << "Bumpmap definition found" << std::endl;
#endif

      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');
      materials[_current_material_name].setBumpmap(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_D == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " Alpha Texture (map_d) definition found " << std::endl;
#endif

      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');

      materials[_current_material_name].setMapD(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (
        ObjMaterialLexer::OBJ_MATERIAL_REFLECTION_MAP == tokens[0]
        || ObjMaterialLexer::OBJ_MATERIAL_REFLECTION_MAP_2 == tokens[0])
    {
      //TODO: do not ignore options
      std::string texture_map_path(directory_path + tokens[tokens.size() - 1]);
      std::replace(texture_map_path.begin(), texture_map_path.end(), '\\', '/');
      materials[_current_material_name].setReflectionMap(texture_map_path);

      if (tokens.size() > 2)
      {
        std::cout << "[WARNING] Optional argument detected but ignored at " << current_line_number << std::endl;
      }
    }
    else if (ObjMaterialLexer::OBJ_ILL_MODEL == tokens[0])
    {
      std::string current_token;

      if (tokens.size() == 2)
      {
        current_token = tokens[1];
      }
      else if (tokens.size() == 3 && ObjMaterialLexer::OBJ_HALO == tokens[1])
      {
        current_token = tokens[2];
      }

      if (!current_token.empty())
      {
        unsigned int model_type;
        status = mrf::util::StringParsing::convertStringToUInt(current_token.c_str(), model_type);
        if (status)
        {
          materials[_current_material_name].setIllModel(model_type);
        }
      }
      else
      {
        std::cout << " Unsupported Dissolve definition. Skipping. Default definition applied "
                  << " Check line" << current_line_number << std::endl;
      }
    }
    else
    {
      std::cout << " Warning unsupported token " << tokens[0] << " ===> Ignoring it. Check line " << current_line_number
                << std::endl;
    }
  }
  else if (tokens.size() == 1)
  {
    std::cout << "[WARNING] SUSPECIOUS LINE WITH ONLY 1 token. Check Line #" << current_line_number << std::endl;
  }
  else
  {
    status = false;
  }


#ifdef OBJMATERIAL_PARSER_DEBUG
  std::cout << " ONE LINE PARSED .  STATUS=" << status << std::endl;
#endif


  return status;
}




////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// POUR LE MOMENT LE MATERIEL EST DEFINI EN DUR.
// PAR LA SUITE IL DEVRA ETRE DYNAMIQUE (PASSE EN PARAMETRE).
////////////////////////////////////////////////////////////////////////////////

template<class FileIOPolicy>
bool _OBJMaterialParser<FileIOPolicy>::analyzeOneLine(
    std::string &                        line,
    std::map<std::string, unsigned int> &material_id,
    std::vector<OBJMaterial> &           materials)
{
  bool status = true;

  std::vector<std::string> tokens;
  mrf::util::StringParsing::tokenize(line, tokens);

  //Now switch to the appropriate function according to tokens
  if (tokens.size() > 1)
  {
    if (ObjMaterialLexer::OBJ_MATERIAL_DEF == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " New material definition !!! "
                << " Tokens size is " << tokens.size() << std::endl
                << " material name is " << tokens[1] << std::endl
                << " tokens1 c str " << tokens[1].c_str() << std::endl;
#endif

      if (material_id.empty())   //First material we encounter
      {
#ifdef OBJMATERIAL_PARSER_DEBUG
        std::cout << " First Material !!!" << std::endl;
#endif

        //IDstd::map updated
        material_id[tokens[1]] = _current_material_counter;
        _current_material_counter++;

        //Material vector updated
        materials.push_back(OBJMaterial(tokens[1]));
      }
      else   //Not the first material
      {
        //Test if we already have this definition
        std::map<std::string, unsigned int>::iterator current = material_id.find(tokens[1]);
        std::map<std::string, unsigned int>::iterator end     = material_id.end();


        if (current != end)
        {
          std::cout << " WARNING : this material name is  already used !!!" << std::endl
                    << " Ignoring this material definition " << std::endl;

#ifdef OBJMATERIAL_PARSER_DEBUG
          std::cout << "first = " << (*current).first << std::endl;
          std::cout << "second = " << (*current).second << std::endl;
#endif
        }
        else   //adding it
        {
#ifdef OBJMATERIAL_PARSER_DEBUG
          std::cout << "  adding one material "
                    << " _current_material_counter " << _current_material_counter << std::endl;
#endif

          material_id[tokens[1]] = _current_material_counter;
          _current_material_counter++;

          //Material vector updated
          materials.push_back(OBJMaterial(tokens[1]));
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_KA == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " KA DEFINITION " << std::endl;
#endif

      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not yet supported " << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not yet supported " << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            //Set Ambient
            OBJMaterial &curr_mat = materials[_current_material_counter - 1];
            curr_mat.setKa(r, g, b);
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << std::endl;
          status = false;
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_KD == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " KD DEFINITION " << std::endl;
#endif


      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported " << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported " << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            //set Diffus
            OBJMaterial curr_mat = materials[_current_material_counter - 1];
            curr_mat.setKd(r, g, b);
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << std::endl;
          status = false;
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_KS == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " KS DEFINITION " << std::endl;
#endif

      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported " << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported " << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            //set Specular
            OBJMaterial &curr_mat = materials[_current_material_counter - 1];
            curr_mat.setKs(r, g, b);
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << std::endl;
          status = false;
        }
      }
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_TF == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " TF DEFINITION " << std::endl;
#endif

      if (ObjMaterialLexer::OBJ_MATERIAL_SPEC == tokens[1])
      {
        std::cout << " Warning Spectral Color definition is not supported " << std::endl;
      }
      else if (ObjMaterialLexer::OBJ_MATERIAL_XYZ == tokens[1])
      {
        std::cout << " Warning XYZ Color definition is not supported " << std::endl;
      }
      else
      {
        if (tokens.size() == 4)
        {
          float r, g, b;
          status = convertToFloat(tokens[1], tokens[2], tokens[3], r, g, b);
          if (status)
          {
            //set  transmittance
            OBJMaterial &curr_mat = materials[_current_material_counter - 1];
            curr_mat.setTf(r, g, b);
          }
        }
        else
        {
          std::cout << " Wrong number of token to retrieve a rgb color."
                    << " Expected 4 got " << tokens.size() << std::endl;
          status = false;
        }
      }
    }   // end of if OBJ_MATERIAL_TF
    else if (ObjMaterialLexer::OBJ_MATERIAL_NS == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " NS DEFINITION " << std::endl;
#endif

      if (tokens.size() == 4)
      {
        float nr, ng, nb;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], nr, ng, nb);
        if (status)
        {
          std::cout << " Warning Specular Component (Ns) definition with 3 values is not supported " << std::endl;
          //set Shininess
          /*OBJMaterial curr_mat = materials[_current_material_counter-1];
            assert(curr_mat);
            curr_mat->setNs( shininess );
          */
        }
      }
      else if (tokens.size() == 2)
      {
        float ns;
        status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), ns);

        if (status)
        {
          //Color3F shininess(ns,ns,ns);
          OBJMaterial &curr_mat = materials[_current_material_counter - 1];
          curr_mat.setNs(ns);
        }
      }
      else
      {
        std::cout << " Wrong number of token for Specular exponent"
                  << " Expected 1 or 3  got " << tokens.size() << std::endl;
        status = false;
      }

    }   // end of if OBJ_MATERIAL_NS specular exponent
    else if (ObjMaterialLexer::OBJ_MATERIAL_IOR == tokens[0])
    {
#ifdef OBJMATERIAL_PARSER_DEBUG
      std::cout << " IOR DEFINITION " << std::endl;
#endif

      if (tokens.size() == 4)
      {
        float iorr, iorg, iorb;
        status = convertToFloat(tokens[1], tokens[2], tokens[3], iorr, iorg, iorb);
        if (status)
        {
          std::cout << " Warning Index of Refraction (Ni) definition with 3 values is not supported " << std::endl;
          /*Color3F ior(iorr,iorg,iorb);
            GenericMaterial* curr_mat = materials[_current_material_counter-1];
            assert(curr_mat);
            curr_mat->ior( ior );
          */
        }
      }
      else if (tokens.size() == 2)
      {
        float ior;
        status = mrf::util::StringParsing::convertStringToFloat(tokens[1].c_str(), ior);

        if (status)
        {
          OBJMaterial &curr_mat = materials[_current_material_counter - 1];
          curr_mat.setNi(ior);
        }
      }
      else
      {
        std::cout << " Wrong number of token for index of refraction IOR component"
                  << " Expected 1 or 3  got " << tokens.size() << std::endl;
        status = false;
      }


    }   // end of if OBJ_MATERIAL_IOR index of refraction

    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_KA == tokens[0])
    {
      std::cout << "Warningstd::map_Ka definition is not yet supported " << std::endl;
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_KD == tokens[0])
    {
      std::cout << "Warningstd::map_Kd definition is not yet supported " << std::endl;
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_KS == tokens[0])
    {
      std::cout << "Warningstd::map_Ks definition is not yet supported " << std::endl;
    }
    else if (ObjMaterialLexer::OBJ_MATERIAL_MAP_NS == tokens[0])
    {
      std::cout << "Warningstd::map_Ns definition is not yet supported " << std::endl;
    }
    else
    {
      std::cout << " Warning unsupported token " << tokens[0] << std::endl << " Ignoring it" << std::endl;
    }
  }
  else
  {
    status = false;
  }
  return status;
}




template<class FileIOPolicy>
inline bool _OBJMaterialParser<FileIOPolicy>::convertToFloat(
    std::string const &s_x,
    std::string const &s_y,
    std::string const &s_z,
    float &            x,
    float &            y,
    float &            z)
{
  using namespace mrf::util;


  // #ifdef OBJMATERIAL_PARSER_DEBUG
  //std::cout << " s x = " << s_x << std::endl;
  //std::cout << " s y = " << s_y << std::endl;
  //std::cout << " s z = " << s_z << std::endl;
  // #endif


  bool status1 = mrf::util::StringParsing::convertStringToFloat(s_x.c_str(), x);
  bool status2 = mrf::util::StringParsing::convertStringToFloat(s_y.c_str(), y);
  bool status3 = mrf::util::StringParsing::convertStringToFloat(s_z.c_str(), z);

  // #ifdef OBJMATERIAL_PARSER_DEBUG
  //std::cout << " true = " << true << std::endl;
  //std::cout << " status1 = " << status1 << std::endl;
  //std::cout << " status2 = " << status2 << std::endl;
  //std::cout << " status3 = " << status3 << std::endl;
  // #endif



  return (status1) && (status2) && (status3);
}




//Hidden methods for singleton method
template<class FileIOPolicy>
_OBJMaterialParser<FileIOPolicy>::_OBJMaterialParser()
{}

template<class FileIOPolicy>
_OBJMaterialParser<FileIOPolicy>::~_OBJMaterialParser()
{}

template<class FileIOPolicy>
_OBJMaterialParser<FileIOPolicy>::_OBJMaterialParser(_OBJMaterialParser const &other)
  : _current_material_counter(other._current_material_counter)
  , _current_material_name(other._current_material_name)
{}

template<class FileIOPolicy>
_OBJMaterialParser<FileIOPolicy> &_OBJMaterialParser<FileIOPolicy>::operator=(_OBJMaterialParser const &other)
{
  this->_current_material_counter = other.current_material_counter;
  this->_current_material_name    = other.current_material_name;

  return *this;
}
