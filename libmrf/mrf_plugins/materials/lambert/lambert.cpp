/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include "lambert.hpp"
#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace math;


Lambert::Lambert(std::string const &name, COLOR const &color, float reflectivity)
  : BRDF(name, BRDFTypes::LAMBERT)
  , _color(color)
  , _reflectivity(reflectivity)
{
  _nColoredReflectivity = (_color * _reflectivity) / static_cast<float>(Math::PI);
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
Lambert::Lambert(std::string const &name, float reflectivity)
  : BRDF(name, BRDFTypes::LAMBERT)
  , _reflectivity(reflectivity)
{}
#else

Lambert::Lambert(std::string const &name, float color_r, float color_g, float color_b, float reflectivity)
  : BRDF(name, BRDFTypes::LAMBERT)
  , _color(color_r, color_g, color_b)
  , _reflectivity(reflectivity)
{
  //Normalize the Color to avoid energy Creation
  //TODO : use a better normalization way ?
  //_color.normalize();
  _nColoredReflectivity = (_color * _reflectivity) / static_cast<float>(Math::PI);
}

#endif



Lambert::~Lambert() {}


//-------------------------------------------------------------------
float Lambert::brdfValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return _reflectivity / static_cast<float>(Math::PI);
}


RADIANCE_TYPE
Lambert::coloredBRDFValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return _nColoredReflectivity;
}




float Lambert::diffuseAlbedo() const
{
  return _reflectivity;
}

float Lambert::diffuseAlbedo(mrf::geom::LocalFrame const & /*lf*/) const
{
  return diffuseAlbedo();
}

bool Lambert::diffuseComponent() const
{
  return true;
}



float Lambert::specularAlbedo() const
{
  return 0.0f;
}

float Lambert::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0;
}

bool Lambert::specularComponent() const
{
  return false;
}

float Lambert::transmissionAlbedo() const
{
  return 0.0f;
}

float Lambert::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0;
}

bool Lambert::transmissionComponent() const
{
  return false;
}


float Lambert::nonDiffuseAlbedo() const
{
  return 0.0f;
}

bool Lambert::nonDiffuseComponent() const
{
  return false;
}

//TODO: remove me since this implemented in UMAT ?
float Lambert::totalAlbedo() const
{
  return _reflectivity;
}

//-------------------------------------------------------------------
float Lambert::diffPDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const & /*out_dir*/) const

{
#ifdef DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "  gpos(lf.normal().dot(in_dir)) = " << gpos(lf.normal().dot(in_dir)) << std::endl;

  std::cout << "  lf.normal() = " << lf.normal() << std::endl;
  std::cout << "  in_dir = " << in_dir << std::endl;

#endif
  //return gpos(lf.normal().dot(in_dir))/Math::PI;

  return gneg(lf.normal().dot(in_dir)) / static_cast<float>(Math::PI);
}

float Lambert::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float Lambert::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float Lambert::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}



//-------------------------------------------------------------------
ScatterEvent::SCATTER_EVT Lambert::generateScatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE &incident_radiance,
    Vec3f &        outgoing_dir) const
{
  float e1 = RandomGenerator::Instance().getFloat();

  //TODO IMPROVE ME : check whether or not the norma of
  // incident_radiance * _color is > EPSILON
  // if not RETURN IMMEDIATELY ON ABSORPTION EVENT !!!

  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR
  // LAMBERTIAN SURFACE
  {
    //#ifdef DEBUG
    // std::cout << " HEere = " << __FILE__ << " "<<  __LINE__  << std::endl;
    // std::cout << "  _color = " << _color << std::endl ;
    //#endif


    //We use a pdf proportional to the cosine
    incident_radiance *= _color;
    e1 /= diffuseAlbedo();

    float e2     = RandomGenerator::Instance().getFloat();
    outgoing_dir = randomHemiCosDirection(lf, e1, e2);

    //#ifdef DEBUG
    // std::cout << "  outgoing_dir = " << outgoing_dir << std::endl ;
    //#endif

    return ScatterEvent::DIFFUSE_REFL;
  }
  else
  {
    return ScatterEvent::ABSORPTION;
  }
}

ScatterEvent::SCATTER_EVT Lambert::scatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f &outgoing_dir,
    float &           proba_outgoing_dir,
    float             e1,
    float             e2) const
{
  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR
  // LAMBERTIAN SURFACE
  {
    //We use a pdf proportional to the cosine
    float const e3     = e1 / diffuseAlbedo();
    outgoing_dir       = randomHemiCosDirection(lf, e2, e3);
    proba_outgoing_dir = static_cast<float>(Math::INV_PI) * diffuseAlbedo();

    return ScatterEvent::DIFFUSE_REFL;
  }
  else
  {
    return ScatterEvent::ABSORPTION;
  }
}



ScatterEvent::SCATTER_EVT Lambert::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    float             e1,
    float             e2,
    mrf::math::Vec3f &outgoing_dir,
    RADIANCE_TYPE &   brdf_corrected_by_pdf) const
{
  if (e1 < diffuseAlbedo())   // TOTAL ALBEDO = DIFFUSE ALBEDO FOR LAMBERTIAN SURFACE
  {
    //We use a pdf proportional to the cosine
    float const e3 = e1 / diffuseAlbedo();
    outgoing_dir   = randomHemiCosDirection(lf, e2, e3);
    //proba_outgoing_dir = Math::INV_PI * diffuseAlbedo();

    // Since the BRDF / pdf = 1.0, we just return the _color (which is normalized)
    float const dot_NL = outgoing_dir.dot(lf.normal());
    if (dot_NL <= 0.0f)
    {
      brdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
      return ScatterEvent::ABSORPTION;
    }

    brdf_corrected_by_pdf = _color / dot_NL;

    return ScatterEvent::DIFFUSE_REFL;
  }
  return ScatterEvent::ABSORPTION;
}

}   // namespace materials
}   // namespace mrf
