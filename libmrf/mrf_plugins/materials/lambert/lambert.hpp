/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *LAMBERT = "Lambert";   // Perfectly Diffuse Material
};                                        // namespace BRDFTypes

//-------------------------------------------------------------------------
// Lambert Reflector
//-------------------------------------------------------------------------

class MRF_PLUGIN_EXPORT Lambert: public BRDF
{
protected:
  COLOR _color;

  float _reflectivity;   // rho_d in Adv. Global Ill. Dutr� et al.
  // _reflectivity is between 0 and 1
  // reflectivity is the same as albedo

  RADIANCE_TYPE _nColoredReflectivity;   //The normalized Colored Reflectivity

public:
  Lambert(std::string const &name, COLOR const &color, float reflectivity = 1.f);
#ifdef MRF_RENDERING_MODE_SPECTRAL
  Lambert(std::string const &name, float reflectivity = 1.f);
#else
  Lambert(
      std::string const &name,
      float              color_r      = 0.8f,
      float              color_g      = 0.8f,
      float              color_b      = 0.8f,
      float              reflectivity = 1.f);
#endif

  virtual ~Lambert();


  //-------------------------------------------------------------------------
  // Some Accessors
  //-------------------------------------------------------------------------
  inline void setReflectivity(float a_reflectivity);
  //inline float reflectivity() const;

  inline void         setColor(COLOR const &a_color);
  inline COLOR const &getColor() const;

  //-------------------------------------------------------------------------
  // UMat Interface Methods Implementation
  //-------------------------------------------------------------------------

  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;


  virtual float diffuseAlbedo() const;
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  diffuseComponent() const;

  virtual float specularAlbedo() const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  specularComponent() const;

  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;


  virtual float totalAlbedo() const;

  //-------------------------------------------------------------------------
  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      float &                      proba_outgoing_dir,
      float                        e1,
      float                        e2) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;
};


inline void Lambert::setReflectivity(float a_reflectivity)
{
  _reflectivity         = a_reflectivity;
  _nColoredReflectivity = (_color * _reflectivity) / static_cast<float>(mrf::math::Math::PI);
}

//inline float reflectivity() const;

inline void Lambert::setColor(COLOR const &a_color)
{
  _color = a_color;
  //_color.normalize();


  // std::cout << "  name = " << _name << std::endl ;
  // std::cout << "  _color = " << _color << __FILE__ << " " << __LINE__ << std::endl ;
  // std::cout << "  _reflectivity = " << _reflectivity << std::endl ;

  _nColoredReflectivity = (_color * _reflectivity) / static_cast<float>(mrf::math::Math::PI);
}

inline COLOR const &Lambert::getColor() const
{
  return _color;
}

}   // namespace materials
}   // namespace mrf
