#include "normal_shader.hpp"

namespace mrf
{
namespace materials
{
NormalShader::NormalShader(std::string const &name): BRDF(name, BRDFTypes::NORMAL) {}

NormalShader::~NormalShader() {}


RADIANCE_TYPE
NormalShader::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return RADIANCE_TYPE(lf.normal().x() * 0.5f + 0.5f, lf.normal().y() * 0.5f + 0.5f, lf.normal().z() * 0.5f + 0.5f);
}



}   // namespace materials
}   // namespace mrf
