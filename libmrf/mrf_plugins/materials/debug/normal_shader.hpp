/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 *  Normal Shader
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/materials/bsdf.hpp>
#include <mrf_plugins/materials/debug/flat.hpp>
#include <mrf_core/geometry/local_frame.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *NORMAL = "Normal";
};   // namespace BRDFTypes

class MRF_PLUGIN_EXPORT NormalShader: public BRDF
{
public:
  NormalShader(std::string const &name);
  virtual ~NormalShader();

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;
};
}   // namespace materials
}   // namespace mrf
