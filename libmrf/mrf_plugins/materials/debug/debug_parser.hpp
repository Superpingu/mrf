#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/materials/materials.hpp>

#include <mrf_core/io/base_plugin_parser.hpp>

#include <mrf_plugins/materials/debug/flat.hpp>
#include <mrf_plugins/materials/debug/normal_shader.hpp>
#include <mrf_plugins/materials/debug/tangent_shader.hpp>
#include <mrf_plugins/materials/debug/uv_shader.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
template<class T>
class DebugParser: public BaseMaterialParser<T>
{
public:
  DebugParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return nullptr; }

  static std::string getID() { return ""; }

  virtual bool parse(tinyxml2::XMLElement *, mrf::materials::UMat *a_mat, ParserHelper &)
  {
    T *debug_mat = dynamic_cast<T *>(a_mat);

    return debug_mat != nullptr;
  }

protected:
private:
};

class FlatParser: public DebugParser<mrf::materials::FlatBRDF>
{
public:
  FlatParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new FlatParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::FLAT; }

protected:
private:
};

class NormalParser: public DebugParser<mrf::materials::NormalShader>
{
public:
  NormalParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new NormalParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::NORMAL; }

protected:
private:
};

class TangentParser: public DebugParser<mrf::materials::TangentShader>
{
public:
  TangentParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new TangentParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::TANGENT; }

protected:
private:
};

class UVParser: public DebugParser<mrf::materials::UVShader>
{
public:
  UVParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new UVParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::UV; }

protected:
private:
};

}   // namespace io
}   // namespace mrf
