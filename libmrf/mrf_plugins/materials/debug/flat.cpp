#include <mrf_plugins/materials/debug/flat.hpp>

namespace mrf
{
namespace materials
{
FlatBRDF::FlatBRDF(std::string const &name, COLOR const &color): BRDF(name, BRDFTypes::FLAT), _color(color) {}
FlatBRDF::FlatBRDF(std::string const &name, std::string const &type, COLOR const &color)
  : BRDF(name, type)
  , _color(color)
{}



FlatBRDF::~FlatBRDF() {}


//-------------------------------------------------------------------------
// UMat Interface Methods Implementation
//-------------------------------------------------------------------------
float FlatBRDF::brdfValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return _color.avgCmp();
}


RADIANCE_TYPE
FlatBRDF::coloredBRDFValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return _color;
}



float FlatBRDF::diffuseAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float FlatBRDF::diffuseAlbedo(mrf::geom::LocalFrame const &) const
{
  return diffuseAlbedo();
}


bool FlatBRDF::diffuseComponent() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return false;
}


float FlatBRDF::specularAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


float FlatBRDF::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

bool FlatBRDF::specularComponent() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return false;
}


float FlatBRDF::transmissionAlbedo() const
{
  return 0.0f;
}

float FlatBRDF::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0f;
}

bool FlatBRDF::transmissionComponent() const
{
  return false;
}


float FlatBRDF::nonDiffuseAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


bool FlatBRDF::nonDiffuseComponent() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return false;
}

float FlatBRDF::totalAlbedo() const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


//-------------------------------------------------------------------------
float FlatBRDF::diffPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float FlatBRDF::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float FlatBRDF::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}

float FlatBRDF::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return 0.0f;
}


ScatterEvent::SCATTER_EVT FlatBRDF::generateScatteringDirection(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE & /*incident_radiance*/,
    mrf::math::Vec3f & /*outgoing_dir*/) const
{
  std::cerr << " WHAT THE HELL ARE YOU DOING " << __FILE__ << " " << __LINE__ << std::endl;
  assert(0.0);
  return ScatterEvent::ABSORPTION;
}




}   // namespace materials
}   // namespace mrf
