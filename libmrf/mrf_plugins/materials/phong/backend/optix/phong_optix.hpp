/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_material_plugin.hpp>
#include <mrf_optix/optix_light_plugin.hpp>

#include <mrf_plugins/materials/phong/l_phong.hpp>
#include <mrf_plugins/materials/phong/l_phong_textured.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <vector>
#include <map>


namespace mrf
{
namespace optix_backend
{
class MRF_PLUGIN_EXPORT OptixPhong: public OptixBRDFMaterial
{
public:
  OptixPhong(optix::Material mat): OptixBRDFMaterial(mat) { _kernel_path = "phong.cu"; }
  OptixPhong(std::vector<std::string> const &cuda_compile_options): OptixBRDFMaterial(cuda_compile_options)
  {
    _kernel_path = "phong.cu";
  }
  OptixPhong(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixBRDFMaterial(mat, cuda_compile_options)
  {
    _kernel_path = "phong.cu";
  }
  OptixPhong(mrf::materials::UMat *mrf_mat, optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixBRDFMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _kernel_path = "phong.cu";
  }
  OptixPhong(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
    : OptixBRDFMaterial(mrf_mat, mat, cuda_compile_options, ptx_cfg)
  {
    _kernel_path = "phong.cu";
  }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &context, std::vector<uint> wavelengths);
#else
  virtual void               updateTextures(optix::Context &context);
#endif
};

///////////////////////////////////////////////////////////////////////////////////
////////////////PLUGINS////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

class MRF_PLUGIN_EXPORT PhongPlugin: public OptixMaterialPlugin
{
public:
  PhongPlugin() {}
  ~PhongPlugin() {}

  static OptixMaterialPlugin *create() { return new PhongPlugin; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg,
      std::vector<uint>               wavelengths);
#else
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg);
#endif

private:
};

}   // namespace optix_backend
}   // namespace mrf
