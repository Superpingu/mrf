//MRF Lib
#include "l_phong.hpp"
#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace math;


//-------------------------------------------------------------------//
//---------------        Physical Phong Model by Lafortune CLASS   --//
//-------------------------------------------------------------------//
LPhysicalPhong::LPhysicalPhong(
    std::string const &name,
    COLOR const &      dcolor,
    float const        dalbedo,
    COLOR const &      scolor,
    float const        salbedo,
    unsigned int const exp)
  : BRDF(name, BRDFTypes::PHONG_NORMALIZED)
  , _dalbedo(dalbedo)
  , _dcolor(dcolor)
  , _salbedo(salbedo)
  , _scolor(scolor)
  , _exp(exp)
{
  //Normalize the Color to avoid energy Creation
  //TODO : use a better normalization way ?
  //_dcolor.normalize();
  //_scolor.normalize();
}


LPhysicalPhong::~LPhysicalPhong() {}


//-------------------------------------------------------------------
float LPhysicalPhong::brdfValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const &     out_dir) const
{
  mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  return static_cast<float>(
      powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp)) * _salbedo * static_cast<float>(_exp + 2)
          / (2.f * Math::PI)
      + _dalbedo / Math::PI);
}


RADIANCE_TYPE
LPhysicalPhong::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const &     out_dir) const
{
  //TODO CHECK ME
  mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  return _scolor
             * (powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp)) * _salbedo * (_exp + 2.f)
                / (2.f * static_cast<float>(Math::PI)))
         + _dcolor * (_dalbedo / static_cast<float>(Math::PI));
}




float LPhysicalPhong::diffuseAlbedo() const
{
  return _dalbedo;
}

float LPhysicalPhong::diffuseAlbedo(mrf::geom::LocalFrame const &) const
{
  return diffuseAlbedo();
}

bool LPhysicalPhong::diffuseComponent() const
{
  return true;
}



float LPhysicalPhong::specularAlbedo() const
{
  return _salbedo;
}

float LPhysicalPhong::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  return specularAlbedo();
}

bool LPhysicalPhong::specularComponent() const
{
  return true;
}

float LPhysicalPhong::transmissionAlbedo() const
{
  return 0.0f;
}

float LPhysicalPhong::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0;
}

bool LPhysicalPhong::transmissionComponent() const
{
  return false;
}


float LPhysicalPhong::nonDiffuseAlbedo() const
{
  return specularAlbedo();
}

bool LPhysicalPhong::nonDiffuseComponent() const
{
  return true;
}


float LPhysicalPhong::totalAlbedo() const
{
  return diffuseAlbedo() + specularAlbedo();
}

//-------------------------------------------------------------------
float LPhysicalPhong::diffPDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return gneg(lf.normal().dot(in_dir)) / static_cast<float>(Math::PI);
}

float LPhysicalPhong::specPDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const &     out_dir) const
{
  //mrf::math::Vec3f const reflectedDir = (-2.f*(in_dir.dot(lf.normal())))*lf.normal()+in_dir;
  mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  return static_cast<float>(
      powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp)) * static_cast<float>(_exp + 2)
      / (2.f * Math::PI));
}

float LPhysicalPhong::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float LPhysicalPhong::nonDiffusePDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const &     out_dir) const
{
  //mrf::math::Vec3f const reflectedDir = (-2.f*(in_dir.dot(lf.normal())))*lf.normal()+in_dir;
  mrf::math::Vec3f const reflectedDir = 2.f * (in_dir.dot(lf.normal())) * lf.normal() - in_dir;
  return static_cast<float>(
      powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp)) * static_cast<float>(_exp + 2)
      / (2.f * Math::PI));
}


//-------------------------------------------------------------------
ScatterEvent::SCATTER_EVT LPhysicalPhong::generateScatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    RADIANCE_TYPE &              incident_radiance,
    Vec3f &                      outgoing_dir) const
{
  float e1 = RandomGenerator::Instance().getFloat();

  if (e1 < diffuseAlbedo())   // DIFFUSE ALBEDO
  {
    //We use a pdf proportional to the cosine
    incident_radiance *= _dcolor;
    e1 /= diffuseAlbedo();

    float e2     = RandomGenerator::Instance().getFloat();
    outgoing_dir = randomHemiCosDirection(lf, e1, e2);
    return ScatterEvent::DIFFUSE_REFL;
  }
  else if (e1 < totalAlbedo())   // SPECULAR ALBEDO
  {
    e1 -= diffuseAlbedo();
    e1 /= specularAlbedo();
    float e2 = RandomGenerator::Instance().getFloat();


    //CHECK ME HERE
    mrf::math::Vec3f const reflectedDir = (2.f * (in_dir.dot(lf.normal()))) * lf.normal() - in_dir;
    outgoing_dir                        = randomHemiPowerCosDirection(reflectedDir, _exp, e1, e2);
    float f                             = (lf.normal().dot(outgoing_dir));
    if (f < 0)
    {
      outgoing_dir -= (2 * f) * lf.normal();
      f *= -1.;
    }

    incident_radiance *= f * (float(_exp + 2) / float(_exp + 1));
    incident_radiance *= _scolor;

    return ScatterEvent::SPECULAR_REFL;
  }
  else
  {
    return ScatterEvent::ABSORPTION;
  }
}
}   // namespace materials
}   // namespace mrf
