#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>

#include <mrf_plugins/materials/principled/principled.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
class PrincipledParser: public BaseMaterialParser<mrf::materials::PrincipledDisney>
{
public:
  PrincipledParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new PrincipledParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::PRINCIPLED; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::PrincipledDisney *principled_mat = dynamic_cast<mrf::materials::PrincipledDisney *>(a_mat);

    if (a_mat_element->FirstChildElement(SceneLexer::BASE_COLOR_MK))
    {
      tinyxml2::XMLElement *base_color_elem = a_mat_element->FirstChildElement(SceneLexer::BASE_COLOR_MK);

      mrf::materials::COLOR baseColor = parser.retrieveColor(base_color_elem);
      principled_mat->setBaseColor(baseColor);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::ROUGHNESS_MK))
    {
      tinyxml2::XMLElement *roughness_elem = a_mat_element->FirstChildElement(SceneLexer::ROUGHNESS_MK);
      float                 roughness      = 0.5f;
      int                   roughnessOK    = roughness_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &roughness);
      if (roughnessOK != tinyxml2::XML_SUCCESS)
      {
        roughness = 0.5f;
      }
      principled_mat->setRoughness(roughness);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::CLEARCOAT_MK))
    {
      tinyxml2::XMLElement *clearcoat_elem = a_mat_element->FirstChildElement(SceneLexer::CLEARCOAT_MK);
      float                 clearcoat      = 0.f;
      int                   clearcoatOK    = clearcoat_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &clearcoat);
      if (clearcoatOK != tinyxml2::XML_SUCCESS)
      {
        clearcoat = 0.f;
      }
      principled_mat->setClearcoat(clearcoat);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::CLEARCOATGLOSS_MK))
    {
      tinyxml2::XMLElement *clearcoatGloss_elem = a_mat_element->FirstChildElement(SceneLexer::CLEARCOATGLOSS_MK);
      float                 clearcoatGloss      = 0.f;
      int clearcoatGlossOK = clearcoatGloss_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &clearcoatGloss);
      if (clearcoatGlossOK != tinyxml2::XML_SUCCESS)
      {
        clearcoatGloss = 0.f;
      }
      principled_mat->setClearcoatGloss(clearcoatGloss);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::SUBSURFACE_MK))
    {
      tinyxml2::XMLElement *subsurface_elem = a_mat_element->FirstChildElement(SceneLexer::SUBSURFACE_MK);
      float                 subsurface      = 0.f;
      int                   subsurfaceOK    = subsurface_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &subsurface);
      if (subsurfaceOK != tinyxml2::XML_SUCCESS)
      {
        subsurface = 0.f;
      }
      principled_mat->setSubsurface(subsurface);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::SHEEN_MK))
    {
      tinyxml2::XMLElement *sheen_elem = a_mat_element->FirstChildElement(SceneLexer::SHEEN_MK);
      float                 sheen      = 0.f;
      int                   sheenOK    = sheen_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &sheen);
      if (sheenOK != tinyxml2::XML_SUCCESS)
      {
        sheen = 0.f;
      }
      principled_mat->setSheen(sheen);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::SHEENTINT_MK))
    {
      tinyxml2::XMLElement *sheenTint_elem = a_mat_element->FirstChildElement(SceneLexer::SHEENTINT_MK);
      float                 sheenTint      = 0.f;
      int                   sheenTintOK    = sheenTint_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &sheenTint);
      if (sheenTintOK != tinyxml2::XML_SUCCESS)
      {
        sheenTint = 0.f;
      }
      principled_mat->setSheenTint(sheenTint);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::SPECULAR_MK))
    {
      tinyxml2::XMLElement *specular_elem = a_mat_element->FirstChildElement(SceneLexer::SPECULAR_MK);
      float                 specular      = 0.f;
      int                   specularOK    = specular_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &specular);
      if (specularOK != tinyxml2::XML_SUCCESS)
      {
        specular = 0.f;
      }
      principled_mat->setSpecular(specular);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::SPECULARTINT_MK))
    {
      tinyxml2::XMLElement *specularTint_elem = a_mat_element->FirstChildElement(SceneLexer::SPECULARTINT_MK);
      float                 specularTint      = 0.f;
      int specularTintOK = specularTint_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &specularTint);
      if (specularTintOK != tinyxml2::XML_SUCCESS)
      {
        specularTint = 0.f;
      }
      principled_mat->setSpecularTint(specularTint);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::ANISOTROPIC_MK))
    {
      tinyxml2::XMLElement *anisotropic_elem = a_mat_element->FirstChildElement(SceneLexer::ANISOTROPIC_MK);
      float                 anisotropic      = 0.f;
      int                   anisotropicOK = anisotropic_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &anisotropic);
      if (anisotropicOK != tinyxml2::XML_SUCCESS)
      {
        anisotropic = 0.f;
      }
      principled_mat->setAnisotropic(anisotropic);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::METALLIC_MK))
    {
      tinyxml2::XMLElement *metallic_elem = a_mat_element->FirstChildElement(SceneLexer::METALLIC_MK);
      float                 metallic      = 0.f;
      int                   metallicOK    = metallic_elem->QueryFloatAttribute(SceneLexer::VALUE_AT, &metallic);
      if (metallicOK != tinyxml2::XML_SUCCESS)
      {
        metallic = 0.f;
      }
      principled_mat->setMetallic(metallic);
    }
    if (a_mat_element->FirstChildElement(SceneLexer::BASE_COLOR_TEXTURE_MK))
    {
      tinyxml2::XMLElement *bcolor_tex_elem = a_mat_element->FirstChildElement(SceneLexer::BASE_COLOR_TEXTURE_MK);
      const char           *bcolorTex;
      int                   bcolorTexOK = bcolor_tex_elem->QueryStringAttribute(SceneLexer::VALUE_AT, &bcolorTex);
      if (bcolorTexOK == tinyxml2::XML_SUCCESS)
      {
        //TODO: properly join pathes (std::filesystem in C++17??)
        principled_mat->loadBaseColorTexture(_current_directory + bcolorTex);
      }
    }
    if (a_mat_element->FirstChildElement(SceneLexer::NORMAL_MAP_MK))
    {
      tinyxml2::XMLElement *nmap_elem  = a_mat_element->FirstChildElement(SceneLexer::NORMAL_MAP_MK);
      const char           *nmap      = "";
      int                   nmapOK     = nmap_elem->QueryStringAttribute(SceneLexer::VALUE_AT, &nmap);
      if (nmapOK == tinyxml2::XML_SUCCESS)
      {
        //TODO: properly join pathes (std::filesystem in C++17??)
        principled_mat->loadNormalMap(_current_directory + nmap);
      }
      
    }
    return true;
  }

protected:
private:
};
}   // namespace io
}   // namespace mrf
