/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/principled/backend/optix/principled_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *PrincipledPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *PrincipledPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  //if (!material) return;
  optix::Material optix_mat       = context->createMaterial();
  auto            compile_options = cuda_compile_options;

  auto principled = dynamic_cast<mrf::materials::PrincipledDisney *>(material);
  if (principled)
  {
    optix_mat["_roughness"]->setFloat(principled->getRoughness());
    optix_mat["_subsurface"]->setFloat(principled->getSubsurface());
    optix_mat["_sheen"]->setFloat(principled->getSheen());
    optix_mat["_sheenTint"]->setFloat(principled->getSheenTint());
    optix_mat["_clearcoat"]->setFloat(principled->getClearcoat());
    optix_mat["_clearcoatGloss"]->setFloat(principled->getClearcoatGloss());
    optix_mat["_specular"]->setFloat(principled->getSpecular());
    optix_mat["_specularTint"]->setFloat(principled->getSpecularTint());
    optix_mat["_anisotropic"]->setFloat(principled->getAnisotropic());
    optix_mat["_metallic"]->setFloat(principled->getMetallic());
    optix_mat["_baseColorTint"]->setFloat(principled->getBaseColorTint());

    compile_options.push_back("-DPRINCIPLED");
    OptixPrincipled *ret_mat = new OptixPrincipled(principled, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string>                   variable_names = {"_base_color"};
    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(principled->getBaseColor());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif
#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->updateTextures(context, wavelengths);
#else
    ret_mat->updateTextures(context);
#endif
    if (ret_mat->hasTexture("base_color"))
    {
      optix_mat["_base_color_tex_id"]->setInt(ret_mat->getTexture("base_color")->getId());
    }
    else
    {
      optix_mat["_base_color_tex_id"]->setInt(-1);
    }
    if (ret_mat->hasTexture("normal"))
    {
      optix_mat["_normal_map_id"]->setInt(ret_mat->getTexture("normal")->getId());
    }
    else
    {
      optix_mat["_normal_map_id"]->setInt(-1);
    }
    //compileBRDF(optix_mat, compile_options);

    //return optix_mat;
    //return new OptixPrincipled(optix_mat, compile_options);
    return ret_mat;
  }

  return nullptr;
}
#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixPrincipled::updateTextures(optix::Context &context, std::vector<uint> wavelengths)
#else
void OptixPrincipled::updateTextures(optix::Context &context)
#endif
{
  RTformat tex_format;
#ifdef MRF_RENDERING_MODE_SPECTRAL
  tex_format = RT_FORMAT_FLOAT;
#else
  tex_format = RT_FORMAT_FLOAT4;
#endif

  //_loger.trace("UPDATE PHONG TEXTURES");
  //mrf::util::PrecisionTimer timer;
  //timer.start();
  optix::TextureSampler tex_sampler;

  auto principled = dynamic_cast<mrf::materials::PrincipledDisney *>(_mrf_material);
  if (!principled) return;   //Current phong is not textured

  if (principled->hasBaseColorTexture())
  {
    if (principled->getBaseColorTexture().width() > 0 && principled->getBaseColorTexture().height() > 0)
    {
      //Check if diffuse tex and its sampler need to be created
      if (_textures.find("base_color") == _textures.end())
      {
#ifdef MRF_RENDERING_MODE_SPECTRAL
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            tex_format,
            principled->getBaseColorTexture().width(),
            principled->getBaseColorTexture().height(),
            wavelengths.size());
#else
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            tex_format,
            principled->getBaseColorTexture().width(),
            principled->getBaseColorTexture().height());
#endif
        _textures["base_color"] = tex_sampler;
      }
      else   //Otherwise, map the existing one to the tex sampler
      {
        tex_sampler = _textures["base_color"];
      }
#ifdef MRF_RENDERING_MODE_SPECTRAL
      mrf::optix_backend::updateTextureFromSpectralImage(principled->getBaseColorTexture(), tex_sampler, wavelengths);
#else
      mrf::optix_backend::updateTextureFromImage(principled->getBaseColorTexture(), tex_sampler);
#endif
    }
  }

  if (principled->hasNormalTexture())
  {
    if (principled->getNormalMap().width() > 0 && principled->getNormalMap().height() > 0)
    {
      if (_textures.find("normal") == _textures.end())
      {
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            RT_FORMAT_FLOAT4,
            principled->getNormalMap().width(),
            principled->getNormalMap().height());
        _textures["normal"] = tex_sampler;

        mrf::optix_backend::updateTextureFromImage(principled->getNormalMap(), tex_sampler);
      }
      else
      {
        tex_sampler = _textures["normal"];
      }
    }
  }
}


}   // namespace optix_backend
}   // namespace mrf
