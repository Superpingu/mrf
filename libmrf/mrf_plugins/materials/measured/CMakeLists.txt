set(SOURCES 
  ${SOURCES}
    ${CMAKE_CURRENT_LIST_DIR}/measured_isotropic.cpp
  PARENT_SCOPE)

set(HEADERS 
  ${HEADERS}
    ${CMAKE_CURRENT_LIST_DIR}/measured_isotropic.hpp
    ${CMAKE_CURRENT_LIST_DIR}/measured_parser.hpp
  PARENT_SCOPE
  )

add_subdirectory(backend)

set(BCK_SOURCES ${BCK_SOURCES} PARENT_SCOPE)
set(BCK_HEADERS ${BCK_HEADERS} PARENT_SCOPE)
set(BCK_CU_SOURCES ${BCK_CU_SOURCES} PARENT_SCOPE)