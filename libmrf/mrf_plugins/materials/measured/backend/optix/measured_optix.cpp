/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/measured/backend/optix/measured_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *MeasuredPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *MeasuredPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  //if (!material) return;
  optix::Material    optix_mat = context->createMaterial();
  OptixBaseMaterial *ret_mat;
  auto               compile_options = cuda_compile_options;

  auto measured = dynamic_cast<mrf::materials::MeasuredIsotropic *>(material);
  if (measured)
  {
    compile_options.push_back("-DMEASURED_ISOTROPIC_REFL");
    //Maybe latter : use compile options
    //if (measured->getInterpolate())
    //{
    //compile_options.push_back("-DINTERPOLATE_DIRAC");
    //compile_options.push_back("-DINTERPOLATE_DIFFUS");
    //}
    optix_mat["_interpolation"]->setInt(measured->getInterpolate());

    ret_mat = new OptixMeasured(measured, optix_mat, compile_options, ptx_cfg);
#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->update(context, wavelengths);
#else
    ret_mat->update(context);
#endif

    if (ret_mat->hasBuffer("diffuse")) ret_mat->getMat()["brdf_data"]->set(ret_mat->getBuffer("diffuse"));

    if (ret_mat->hasBuffer("dirac")) ret_mat->getMat()["brdf_data_dirac"]->set(ret_mat->getBuffer("dirac"));

    return ret_mat;
  }

  return nullptr;
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixMeasured::update(optix::Context &context, std::vector<uint> wavelengths)
#else
void OptixMeasured::update(optix::Context &context)
#endif
{
  //_loger.trace("UPDATE MATERIALS");
  //mrf::util::PrecisionTimer all_mat_timer;
  //all_mat_timer.start();

  //Measured isotropic is a special case, it requires a gpu buffer for rgb mode, spectral mode and spectral
  //multiplexed mode
  mrf::materials::MeasuredIsotropic *measured = dynamic_cast<mrf::materials::MeasuredIsotropic *>(_mrf_material);
  if (measured)
  {
    // uint resolution_theta = std::get<0>(measured->getDataSize());
    uint resolution_theta_v = std::get<0>(measured->getDataSize());
    uint resolution_theta_l = std::get<1>(measured->getDataSize());

    uint resolution_phi = std::get<2>(measured->getDataSize());

    if (_buffers.size() == 0)
    {
      auto buffer = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_USER);
#ifdef MRF_RENDERING_MODE_SPECTRAL
      buffer->setElementSize(wavelengths.size() * sizeof(float));
#else
      //RTbuffer buffer;cd /D
      typedef struct
      {
        float r;
        float g;
        float b;
      } rgb;

      buffer->setElementSize(sizeof(rgb));
#endif
      uint total_size = resolution_theta_v * resolution_theta_l * resolution_phi;
      if (total_size == 0)   //if no diffuse/scattering BRDF data in the file, create a dummy buffer of size [1,1,1]
        buffer->setSize(1, 1, 1);
      else
        buffer->setSize(resolution_theta_v, resolution_theta_l, resolution_phi);

      auto buffer_dirac = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_USER);
#ifdef MRF_RENDERING_MODE_SPECTRAL
      buffer_dirac->setElementSize(wavelengths.size() * sizeof(float));
#else
      buffer_dirac->setElementSize(sizeof(rgb));
#endif

      uint resolution_dirac = uint(measured->getDiracSize());

      //_loger.info("Create buffer for Measured Isotropic BRDF, resolution:");
      //_loger.info("    theta_l = ", resolution_theta_l);
      //_loger.info("    theta_v = ", resolution_theta_v);
      //_loger.info("    phi = ", resolution_phi);
      //_loger.info("    dirac size = ", resolution_dirac);

      //if no dirac data in the scene, create a dummy buffer of size 1
      buffer_dirac->setSize(std::max(uint(1), resolution_dirac));

      //_measured_materials_buffer[material] = std::make_pair(buffer, buffer_dirac);
      _buffers["diffuse"] = buffer;
      _buffers["dirac"]   = buffer_dirac;
    }

    auto &buffer       = _buffers["diffuse"];
    auto &buffer_dirac = _buffers["dirac"];

    //_loger.info("UPDATE A MEASURED ISOTROPIC MATERIAL");
    mrf::util::PrecisionTimer timer;
    timer.start();

    void * data       = buffer->map();
    float *float_data = (float *)data;

    uint id_alta = 0;

    const auto &alta_data = measured->getColor();

    for (uint d_phi = 0; d_phi < resolution_phi; d_phi++)
    {
      for (uint theta_l = 0; theta_l < resolution_theta_l; theta_l++)
      {
        for (uint theta_v = 0; theta_v < resolution_theta_v; theta_v++)
        {
#ifdef MRF_RENDERING_MODE_SPECTRAL
          id_alta = (d_phi * (resolution_theta_v * resolution_theta_l) + theta_l * resolution_theta_v + theta_v);

          for (uint wave_loop = 0; wave_loop < wavelengths.size(); wave_loop++)
          {
            *float_data++ = alta_data[id_alta].findLinearyInterpolatedValue(float(wavelengths[wave_loop]));
          }
#else
          id_alta = 3 * (d_phi * (resolution_theta_v * resolution_theta_l) + theta_l * resolution_theta_v + theta_v);

          *float_data++ = alta_data[id_alta];
          *float_data++ = alta_data[id_alta + 1];
          *float_data++ = alta_data[id_alta + 2];
#endif
        }
      }
    }

    buffer->unmap();

    //_loger.info("   update diffuse part took ", timer.elapsed());
    timer.start();

    uint resolution_dirac = uint(measured->getDiracSize());
    if (resolution_dirac)
    {
      data       = buffer_dirac->map();
      float_data = (float *)data;

      for (uint array_idx = 0; array_idx < resolution_dirac; array_idx++)
      {
        auto color = measured->getDiracColor(array_idx);
#ifdef MRF_RENDERING_MODE_SPECTRAL
        for (uint wave_loop = 0; wave_loop < wavelengths.size(); wave_loop++)
        {
          *float_data++ = color.findLinearyInterpolatedValue(float(wavelengths[wave_loop]));
        }
#else
        *float_data++ = color.r();
        *float_data++ = color.g();
        *float_data++ = color.b();
#endif
      }

      buffer_dirac->unmap();
    }
    //_loger.info("   update specular part took ", timer.elapsed());
    //_measured_materials_buffer[material] = std::make_pair(buffer, buffer_dirac);
    _buffers["diffuse"] = buffer;
    _buffers["dirac"]   = buffer_dirac;
  }
  //float elapsed = all_mat_timer.elapsed();
  //_loger.trace("    Update Materials took: " + std::to_string(elapsed) + "s");
}


}   // namespace optix_backend
}   // namespace mrf
