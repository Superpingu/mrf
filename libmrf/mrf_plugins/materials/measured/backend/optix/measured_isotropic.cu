//TODO DO THE THING CORRECTLY HERE
rtBuffer<COLOR_B, 3> brdf_data;
rtBuffer<COLOR_B, 1> brdf_data_dirac;

rtDeclareVariable(int, _interpolation, , );

#include "include/distribution/cosine.h"
#include "include/parameterization.h"
#include "include/eval_alta_diffuse.h"
#include "include/eval_alta_dirac.h"

rtDeclareVariable(uint, _parameterization, , );

bool bsdf_is_dirac()
{
  size_t3 data_size  = brdf_data.size();
  int     total_size = data_size.x * data_size.y * data_size.z;
  return total_size == 1;
}

float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  //Return only pdf of diffuse part, because dirac is not sampled by MIS
  //otherwise we would have return the sum of the two pdfs divide by 2
  //see pbrtv3 line 770 in reflection.cpp function
  //Float BSDF::Pdf(const Vector3f &woWorld, const Vector3f &wiWorld, BxDFType flags) const
  // float cosTheta = dot(n,light);
  // if(cosTheta>=0)
  // return cosTheta/M_PIf;
  // return 0;

  float   pdf_dirac  = 0.5f;
  size_t3 data_size  = brdf_data.size();
  int     total_size = data_size.x * data_size.y * data_size.z;
  if (total_size <= 1) pdf_dirac = 1.f;

  if (lobe == SPECULAR_REFL)
  {
    return pdf_dirac;
  }
  else if (lobe == ANY_REFL)
  {
    return (1.f - pdf_dirac) * cosine_pdf(ffnormal, eye, light);
  }
  return 0.f;
}

COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  //dirac can't be sampled here
  //evalDirac(current_prd.direction, ffnormal);
  //optix::Onb onb( n );
  //float3 tangent = onb.m_tangent;
  //float3 bitangent = -onb.m_binormal;///
  // float3 bitangent = cross(tangent,n);
  // return computeColorFromDirection(n, tangent, bitangent, eye, light);

  //TODO !!!
  if (lobe == SPECULAR_REFL)
  {
    return evalDirac(light, ffnormal);
  }
  else if (lobe == ANY_REFL)
  {
    float3 bitangent = cross(tangent, ffnormal);
    return computeColorFromDirection(ffnormal, tangent, bitangent, eye, light);
  }

  return COLOR(0.f);
}

COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  if (lobe == SPECULAR_REFL)
  {
    float pdf = bsdf_pdf(ffnormal, eye, light, lobe);
    if (pdf > 0.f) return evalDirac(light, ffnormal) / pdf;
  }
  else if (lobe == ANY_REFL)
  {
    float3 bitangent = cross(tangent, ffnormal);
    float  pdf       = bsdf_pdf(ffnormal, eye, light, lobe);
    if (pdf > 0.f) return computeColorFromDirection(ffnormal, tangent, bitangent, eye, light) / pdf;
  }

  return COLOR(0.f);
}

float3 generate_half_vector(float3 n, float3 in_dir)
{
  return n;
}

float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  float2 rnd2d = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);
  float z1 = rnd2d.x;
  float z2 = rnd2d.y;
  float z3 = 0.f;

  //50% of samples on dirac
  float pdf_dirac = 0.5f;

  //weight of dirac and diffuse

  size_t3 data_size  = brdf_data.size();
  int     total_size = data_size.x * data_size.y * data_size.z;

  //if no diffuse data is present in the scene,
  // a dummy buffer of size [1,1,1] is passed to this kernel
  if (total_size <= 1)
  {
    pdf_dirac = 1.f;
  }
  else
  {
    z3 = sampling1D(
        current_prd.num_sample,
        current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF,
        current_prd.offset_sampling.x,
        current_prd.seed);
  }

  if (z3 < pdf_dirac)
  {
    float3 out_dir = optix::reflect(in_dir, n);
    return make_float4(out_dir, SPECULAR_REFL);
  }
  else
  {
    float3 p = cosine_sample(z1, z2, in_dir, n);
    return make_float4(normalize(p), ANY_REFL);
  }
}
