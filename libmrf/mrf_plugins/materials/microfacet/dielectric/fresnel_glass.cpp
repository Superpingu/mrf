/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2018
 *
 *
 **/

#include "fresnel_glass.hpp"

namespace mrf
{
namespace materials
{
FresnelGlass::FresnelGlass(std::string const &name)
  : BSDF(name, BRDFTypes::FRESNEL_GLASS)
  , _eta(RADIANCE_TYPE(0.f))
  , _kappa(RADIANCE_TYPE(0.f))
  , _extinction(RADIANCE_TYPE(0.f))
  , _thin_glass(false)
{}

FresnelGlass::FresnelGlass(
    std::string const &  name,
    RADIANCE_TYPE const &eta,
    RADIANCE_TYPE const &kappa,
    RADIANCE_TYPE const &extinction,
    bool                 thin_glass)
  : BSDF(name, BRDFTypes::FRESNEL_GLASS)
  , _eta(eta)
  , _kappa(kappa)
  , _extinction(extinction)
  , _thin_glass(thin_glass)
{}

FresnelGlass::~FresnelGlass() {}

mrf::materials::COLOR const &FresnelGlass::eta() const
{
  return _eta;
}

mrf::materials::COLOR const &FresnelGlass::kappa() const
{
  return _kappa;
}

mrf::materials::COLOR const &FresnelGlass::extinction() const
{
  return _extinction;
}

bool const &FresnelGlass::thinGlass() const
{
  return _thin_glass;
}

mrf::materials::IOR FresnelGlass::ior() const
{
  return mrf::materials::IOR();
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
std::vector<float> FresnelGlass::getTransCDF(std::vector<uint> wavelengths)
{
  if (_trans_CDF.size() == 0) initTransCDF(wavelengths);
  return _trans_CDF;
}

mrf::materials::RADIANCE_TYPE const FresnelGlass::getInvTransPDF(std::vector<uint> wavelengths)
{
  if (_inv_trans_PDF.size() == 0) initTransCDF(wavelengths);
  return _inv_trans_PDF;
}

bool FresnelGlass::initTransCDF(std::vector<uint> wavelengths)
{
  if (_trans_CDF.size() > 0) return true;

  uint first = wavelengths[0];
  uint last  = wavelengths[wavelengths.size() - 1];
  if (last == first) return false;   //Monochromatic, no need for a CDF.
  uint nb_bin                = wavelengths.size();
  uint nb_wavelength_per_bin = (last - first) / (wavelengths.size() - 1);
  uint step                  = 1;   //1 nm resolution for the CDF

  int spectrum_interval = wavelengths[1] - wavelengths[0];

  //First get extinction on a 1nm basis.
  std::vector<float> extinction;
  float              variations = 0.f;
  float              tmp_sigma  = _kappa.findLinearyInterpolatedValue(first) * 4.f * 3.141592f / (float)first;
  //float tmp_sigma = _kappa.findIntegratedValue(first, spectrum_interval) * 4.f * 3.141592f / (float)first;
  float tmp_user_ext = _extinction.findLinearyInterpolatedValue(first);
  //float tmp_user_ext = _extinction.findIntegratedValue(first, spectrum_interval);
  //float              tmp_ext      = std::fmin(std::fmaxf(exp(-tmp_sigma) * (1.f - tmp_user_ext), 0.0001f), 0.9999f);
  float tmp_ext = exp(-tmp_sigma) * (1.f - tmp_user_ext);
  extinction.push_back(tmp_ext);
  int count = 1;
  while (first < last)
  {
    tmp_sigma = _kappa.findLinearyInterpolatedValue(first) * 4.f * 3.141592f / (float)first;
    //tmp_sigma = _kappa.findIntegratedValue(first, spectrum_interval) * 4.f * 3.141592f / (float)first;
    tmp_user_ext = _extinction.findLinearyInterpolatedValue(first);
    //tmp_user_ext = _extinction.findIntegratedValue(first, spectrum_interval);
    //tmp_ext      = std::fmin(std::fmaxf(exp(-tmp_sigma) * (1.f - tmp_user_ext), 0.0001f), 0.9999f);
    tmp_ext = exp(-tmp_sigma) * (1.f - tmp_user_ext);
    extinction.push_back(tmp_ext);
    variations += abs(extinction[count] - extinction[count - 1]);
    first += step;
    ++count;
  }

  if (variations < 0.1f) return false;   //Function is nearly uniform, no need to importance sample.

  //Compute CDF on a per bin basis
  float int_trans_PDF = 0.f;
  for (uint i = 0; i < nb_bin - 1; ++i)
  {
    float bin_transmittance = 0.f;
    for (uint j = 0; j < nb_wavelength_per_bin; ++j)
    {
      bin_transmittance += extinction[i * nb_wavelength_per_bin + j];
      _trans_CDF.push_back(int_trans_PDF + bin_transmittance);
    }
    int_trans_PDF += bin_transmittance;

    _inv_trans_PDF.add(wavelengths[i], bin_transmittance);
  }

  //Normalize CDF
  for (auto it = _trans_CDF.begin(); it < _trans_CDF.end(); ++it)
  {
    *it /= int_trans_PDF;
  }

  //Normalize and inverse PDF
  for (uint i = 0; i < nb_bin - 1; ++i)
  {
    if (_inv_trans_PDF[i] <= 0.00001f)
      _inv_trans_PDF[i] = 0.f;
    else
    {
      _inv_trans_PDF[i] = int_trans_PDF / _inv_trans_PDF[i];
    }
  }

  return (_trans_CDF.size() > 0 && _inv_trans_PDF.size() > 0);
}
#endif

//------------------------------------------------------------------------------------------------
// Implementation of the UMat interface
//------------------------------------------------------------------------------------------------

// RADIANCE_TYPE
// FresnelGlass::coloredBRDFValue(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   //TODO
//   return RADIANCE_TYPE();
// }


// ScatterEvent::SCATTER_EVT FresnelGlass::scatteringDirectionWithBRDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     float                        e1,
//     float                        e2,
//     mrf::math::Vec3f &           outgoing_dir,
//     RADIANCE_TYPE &              brdf_corrected_by_pdf) const
// {
//   //TODO
//   return ScatterEvent::SCATTER_EVT::ABSORPTION;
// }


// TODO:  Generalize this for conductor as well
// TODO: Refactor this to improve speed?

// 1: Sample   the Half direction for Reflection
// 2: Evaluate the Fresnel Term for dot_HV = cos_theta_diff
// 3: Choose according to Fresnel Term if the material reflects or refracts
// 4: Compute outgoing_dir according to scattering event (reflection or refraction)
// 5: Evaluate bsdf_corrected_by_pdf accordingly
// 6: Return the scattering event

// ScatterEvent::SCATTER_EVT FresnelGlass::scatteringDirectionWithBSDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     float                        e1,
//     float                        e2,
//     mrf::math::Vec3f &           outgoing_dir,
//     RADIANCE_TYPE &              bsdf_corrected_by_pdf,
//     mrf::materials::IOR const &  in_ior,
//     mrf::materials::IOR const &  out_ior) const
// {
//   //TODO
//   return ScatterEvent::SCATTER_EVT::ABSORPTION;
// }



// mrf::math::Vec3f FresnelGlass::sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const
// {
//   //TODO
//   return mrf::math::Vec3f(0.f);
// }


// bool FresnelGlass::tDir(
//     mrf::math::Vec3f const &   V,   // The View Vector pointing away from the Macro-Geometry
//     mrf::math::Vec3f const &   H,   // Half vector pointing toward the lenss dense medium
//     float                      dot_VH,
//     mrf::materials::IOR const &in_ior,
//     mrf::materials::IOR const &out_ior,
//     float                      dot_NV,   // NO CLAMPING
//     mrf::math::Vec3f &         transmitted_dir) const
// {
//   //TODO
//   return true;
// }


}   // namespace materials

}   // namespace mrf
