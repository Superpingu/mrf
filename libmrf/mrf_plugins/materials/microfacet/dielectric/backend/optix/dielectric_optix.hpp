/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_material_plugin.hpp>
#include <mrf_optix/optix_light_plugin.hpp>

#include <mrf_plugins/materials/microfacet/dielectric/fresnel_glass.hpp>
#include <mrf_plugins/materials/microfacet/dielectric/walter_bsdf.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <vector>
#include <map>


namespace mrf
{
namespace optix_backend
{
class MRF_PLUGIN_EXPORT OptixMicrofacetDielectric: public OptixBSDFMaterial
{
public:
  OptixMicrofacetDielectric(optix::Material mat): OptixBSDFMaterial(mat) { _kernel_path = "microfacet_dielectric.cu"; }
  OptixMicrofacetDielectric(std::vector<std::string> const &cuda_compile_options)
    : OptixBSDFMaterial(cuda_compile_options)
  {
    _kernel_path = "microfacet_dielectric.cu";
  }
  OptixMicrofacetDielectric(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixBSDFMaterial(mat, cuda_compile_options)
  {
    _kernel_path = "microfacet_dielectric.cu";
  }
  OptixMicrofacetDielectric(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options)
    : OptixBSDFMaterial(mrf_mat, mat, cuda_compile_options)
  {
    _kernel_path = "microfacet_dielectric.cu";
  }
  OptixMicrofacetDielectric(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
    : OptixBSDFMaterial(mrf_mat, mat, cuda_compile_options, ptx_cfg)
  {
    _kernel_path = "microfacet_dielectric.cu";
  }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void update(optix::Context &context, std::vector<uint> wavelengths);
#else
  virtual void               update(optix::Context &context);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &context, std::vector<uint> wavelengths);
#else
  virtual void               updateTextures(optix::Context &) {};
#endif

private:
#ifdef MRF_RENDERING_MODE_SPECTRAL
  void updateCDFs(std::vector<uint> wavelengths);
#endif
};

///////////////////////////////////////////////////////////////////////////////////
////////////////PLUGINS////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

class MRF_PLUGIN_EXPORT MicrofacetDielectricPlugin: public OptixMaterialPlugin
{
public:
  MicrofacetDielectricPlugin() {}
  ~MicrofacetDielectricPlugin() {}

  static OptixMaterialPlugin *create() { return new MicrofacetDielectricPlugin; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg,
      std::vector<uint>               wavelengths);
#else
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg);
#endif

private:
};

}   // namespace optix_backend
}   // namespace mrf
