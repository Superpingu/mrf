#include "include/fresnel.h"

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> _eta;
rtBuffer<float, 1> _kappa;
rtBuffer<float, 1> _extinction;
#  else
rtDeclareVariable(float, _eta, , );
rtDeclareVariable(float, _kappa, , );
rtDeclareVariable(float, _extinction, , );
#  endif
rtDeclareVariable(int, _trans_cdf_tex_id, , "CDF of transmittance texture id");
rtBuffer<float, 1> _inv_transmittance_PDF;
#else
rtDeclareVariable(float3, _eta, , );
rtDeclareVariable(float3, _kappa, , );
rtDeclareVariable(float3, _extinction, , );
#endif

rtDeclareVariable(float, _alpha_g, , );
rtDeclareVariable(float, _sqr_alpha_g, , );   // K is either View or Light directions
rtDeclareVariable(int, _is_conductor, , );    // K is either View or Light directions


#include "include/microfacet.h"


#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
__host__ __device__ uint findTransCDFInverse(float rand)
{
  uint begin = 0;
  uint end   = rtTexSize(_trans_cdf_tex_id).x - 1;
  uint count = end - begin;
  while (count > 0)
  {
    uint step = count / 2;
    uint id   = begin + step;
    //TODO use fetch (not working)? or stay with texture interpolation for smaller CDF ?
    float fid = id / float(end);
    if (optix::rtTex1D<float>(_trans_cdf_tex_id, fid) < rand)
    {
      begin = id + 1;
      count = count - (step + 1);
    }
    else
    {
      count = step;
    }
  }
  return begin;
}
#endif

int sampleWavelength()
{
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  if (current_prd.id_wavelength == -1)
  {
    float z1 = sampling1D(
        current_prd.num_sample,
        current_prd.depth * PRNG_BOUNCE_NUM + PRNG_FILTER_V,
        current_prd.offset_sampling.x,
        current_prd.seed);

    if (_trans_cdf_tex_id == -1)
    {
      return int(z1 * NB_MAX_WAVES_MULTIPLEXED);
    }
    else
    {
      const uint bin_size
          = (_wavelengths[last_wave_index] - _wavelengths[first_wave_index]) / (NB_MAX_WAVES_MULTIPLEXED - 1);
      //Inverse CDF return a wavelength between first and last, with first being shifted to 0.
      //Divide by nb_wavelength per pass to get correct ID.
      // const uint id_wavelength = findTransCDFInverse(z1) / bin_size;
      return findTransCDFInverse(z1) / bin_size;
      ;
    }
  }
#endif   //MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  return current_prd.id_wavelength;
}


#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
float inv_decimation_pdf(int id_wavelength)
{
  if (_trans_cdf_tex_id == -1)
    return float(NB_MAX_WAVES_MULTIPLEXED);
  else
  {
    return _inv_transmittance_PDF[id_wavelength];
  }
}
#endif

//Return in ior for a given bin
float getIOR_in(int id_wavelength)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  if (current_prd.is_inside) return lerp(_eta[id_wavelength], _eta[id_wavelength + 1], current_prd.wavelength_offset);

  return lerp(current_prd.ior[id_wavelength], current_prd.ior[id_wavelength + 1], current_prd.wavelength_offset);
#  else
  return (current_prd.is_inside) ? _eta : current_prd.ior;
#  endif
#else
  return (current_prd.is_inside) ? _eta.x : current_prd.ior.value.x;
#endif
}

//Return out ior for a given bin
float getIOR_out(int id_wavelength)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

  if (current_prd.is_inside) return 1.f;

  return lerp(_eta[id_wavelength], _eta[id_wavelength + 1], current_prd.wavelength_offset);
#  else
  return (current_prd.is_inside) ? 1.f : _eta;
#  endif
#else
  return (current_prd.is_inside) ? 1.f : _eta.x;
#endif
}

float3 refractionHalfVector(float3 eye, float3 light, float3 n)
{
  float ior_in  = getIOR_in(current_prd.id_wavelength);
  float ior_out = getIOR_out(current_prd.id_wavelength);

  float3 h     = ior_in * eye + ior_out * light;
  float  normH = length(h);
  if (normH > 0.0001f)   // && ior_in != ior_out)
    return -h / normH;

  return n;
}

void update_ior(float3 real_normal, float3 eye, float3 light)
{
  if (dot(real_normal, eye) > 0.f && dot(real_normal, light) < 0.f)   //Current hit was a transmission into the material
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED   //_eta is a buffer, need to handle manually here
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
    {
      current_prd.ior.value[i] = _eta[i];
    }
#else
    current_prd.ior = _eta;
#endif
    current_prd.is_inside = true;
  }
  else if (
      dot(real_normal, eye) < 0.f && dot(real_normal, light) > 0.f)   //Transmission outside of the material -> into air
  {
    current_prd.ior       = COLOR_B(1.f);
    current_prd.is_inside = false;
  }
  else   //NdotL > 0 && NdotV > 0 or NdotL < 0 && NdotV < 0 ->Current hit is a reflection event, no need to change ior;
  {
    return;
  }
}

/**
 * @brief      Computes the reflection pdf. In spectral mode, also samples a wavelength.
 *
 * @param      normal          the face-forward normal vector.
 * @param      in_dir          the incoming direction, pointing away from the hitpoint.
 *
 * @return     The value of the pdf and the index of the sampled wavelength if any.
 */
float2 reflection_pdf(float3 normal, float3 eye)
{
  if (_is_conductor)
    return make_float2(1.f, -1.f);
  else
  {
    int id_wavelength = sampleWavelength();

    float cos_theta_i = dot(normal, eye);
    float ior_in      = getIOR_in(id_wavelength);
    float ior_out     = getIOR_out(id_wavelength);

    return make_float2(fresnelDielectricDielectric(ior_in, cos_theta_i, ior_out), id_wavelength);
  }
}

/**
 * @brief      Compute the BSDF pdf for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BRDF, lobe = -1.
 *
 * @return     The value of the pdf.
 */
float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  if (_alpha_g == 0.f)   //BRDF is a dirac
  {
    //We should check if we are aligned on the dirac or otherwise return 0.
    //However, this function should also never be called in such a case (no next event on dirac).
    float dot_NL = dot(light, ffnormal);
    return (dot_NL < 0.f) ? 0.f : 1.f;
  }
  else
  {
    float dot_NL = dot(light, ffnormal);

    float3 h = (dot_NL >= 0.f) ? halfVector(eye, light, ffnormal) : refractionHalfVector(eye, light, ffnormal);
    // h = normalize(h);
    float dot_NH = dot(h, ffnormal);
    if (dot_NH <= 0.f) return 0.f;
    float distribution_value = distribution(_sqr_alpha_g, dot_NH);

    float temp_float;   //Compute the adapted jacobien term dw_h / dw_o
    if (dot_NL < 0.f)   // refraction
    {
      float  ior_in  = getIOR_in(current_prd.id_wavelength);
      float  ior_out = getIOR_out(current_prd.id_wavelength);
      float3 full_h  = -(ior_in * eye + ior_out * light);   //Need un-normalized h.
      float  dot_HH  = dot(full_h, full_h);
      temp_float     = ior_out * ior_out * abs(dot(light, h)) / (dot_HH);   //Jacobien term
    }
    else
    {
      temp_float = 1.f / (4.f * abs(dot(light, h)));   // 1 / 4 |o dot h| -> jacobien term
    }
    return temp_float * distribution_value * abs(dot_NH);
  }
}

/**
 * @brief      Compute the transmittance factor for a refraction. Should be call only when exiting the material.
 *
 * @param      length        the distance between entry and exit point.
 *
 * @return     The transmittance factor.
 */
COLOR bsdf_transmittance(float length)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  float kappa
      = lerp(_kappa[current_prd.id_wavelength], _kappa[current_prd.id_wavelength + 1], current_prd.wavelength_offset);
  float wavelength = lerp(
      _wavelengths[current_prd.id_wavelength],
      _wavelengths[current_prd.id_wavelength + 1],
      current_prd.wavelength_offset);

  float sigma      = 4.f * M_PIf * kappa / wavelength;
  float extinction = lerp(
      _extinction[current_prd.id_wavelength],
      _extinction[current_prd.id_wavelength + 1],
      current_prd.wavelength_offset);
  return COLOR(optix::expf(-sigma * length) * (1.f - extinction));
  // return optix::expf(-_extinction[current_prd.id_wavelength]);
#else
  COLOR sigma    = _kappa / _wavelengths * -4.f * M_PIf;
  COLOR user_ext = (COLOR(1.f) - _extinction);
  mrf::spectrum::clamp_negative_color(user_ext);
  return mrf::spectrum::expf(sigma * length) * user_ext;
#endif
}

/**
 * @brief      Compute the BSDF for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BRDF, lobe = -1.
 *
 * @return     The value of the BSDF.
 */
COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  COLOR res;

  float dot_NV = dot(ffnormal, eye);
  float dot_NL = dot(ffnormal, light);

  if (dot_NV == 0.f || dot_NL == 0.f)
  {
    return COLOR(0.f);
  }

  float  dot_HV = dot_NV;
  float  dot_NH = 1.f;
  float  dot_HL = dot_NL;
  float3 h      = (dot_NL >= 0.f) ? halfVector(eye, light, ffnormal) : refractionHalfVector(eye, light, ffnormal);
  if (_alpha_g > 0.f)
  {
    // h = normalize(h);
    dot_HV = dot(h, eye);
    dot_NH = dot(ffnormal, h);
    dot_HL = dot(h, light);
    if (dot_HV == 0.f || dot_NH <= 0.f)
    {
      return COLOR(0.f);
    }
  }

  COLOR F(0.f);
  //TIR is handled by Fresnel function.
  float ior_in  = getIOR_in(current_prd.id_wavelength);
  float ior_out = getIOR_out(current_prd.id_wavelength);
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  //Refraction -> wavelengths have been decimated, compute only for the current one.
  int id_wavelength = current_prd.id_wavelength;
  if (id_wavelength > -1)   // A wavelength decimation has occured, compute only Fresnel for the current one.
  {
    F[id_wavelength] = fresnelDielectricDielectric(ior_in, dot_HV, ior_out);
  }
  else
  {
    for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
    {
      float ior_in  = getIOR_in(i);
      float ior_out = getIOR_out(i);
      F[i]          = fresnelDielectricDielectric(ior_in, dot_HV, ior_out);
    }
  }
#else
  F = fresnelDielectricDielectric(COLOR(ior_in), dot_HV, COLOR(ior_out));
#endif


  //dot_NL < 0 -> ray is refracted (normal is face forward).
  if (dot_NL < 0.f)
  {
    F = COLOR(1.f) - F;
  }

  if (_alpha_g > 0.f)
  {
    float D = distribution(_sqr_alpha_g, dot_NH);
    // float G          = geometricTerm(dot_HV, dot_NV, dot_HL, dot_NL, _sqr_alpha_g);
    float G          = geometricTerm(h, ffnormal, eye, light, make_float2(_alpha_g));
    float temp_float = D * G;

    if (dot_NL < 0.f)
    {
      float ior_in  = getIOR_in(current_prd.id_wavelength);
      float ior_out = getIOR_out(current_prd.id_wavelength);

      float3 full_h      = -(ior_in * eye + ior_out * light);   //Need un-normalized h.
      float  dot_HH      = dot(full_h, full_h);
      float  temp_float2 = ior_out * ior_out * abs(dot_HV * dot_HL) / (dot_HH);

      temp_float *= temp_float2;
    }
    else
      temp_float /= 4.f;

    temp_float /= dot_NL * dot_NV;

    F *= temp_float;
  }
  else   //Smooth -> ideal reflection/refraction, only fresnel + corrective terms
  {
    //No need to check alignment on the dirac.
    //We assume it is handled beforehand and current call is valid.
    if (dot_NL < 0.f)   // refraction
    {
      const float ior_in  = getIOR_in(current_prd.id_wavelength);
      const float ior_out = getIOR_out(current_prd.id_wavelength);

      const float ior_ratio = ior_out / ior_in;
      F *= ior_ratio * ior_ratio;
    }
    else   // reflection
    {
      // float temp_float = 1.f / (dot_NL);
      F *= 1.f / (dot_NV);
    }
  }

  return F;
}

/**
 * @brief      Compute the optimized term BSDF * n dot l / pdf, for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BSDF, lobe = -1.
 * @param      external_pdf    an pdf previously computed if needed.
 *
 * @return     The value of BSDF * n dot l / pdf.
 */
COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  //Lobe is unused, only one lobe in pure microfacet

  COLOR res;

  float dot_NV = dot(ffnormal, eye);
  float dot_NL = dot(ffnormal, light);

  if (dot_NV == 0.f || dot_NL == 0.f)
  {
    return COLOR(0.f);
  }

  float dot_HV = dot_NV;
  float dot_NH = 1.f;
  float dot_HL = dot_NL;
  if (_alpha_g > 0.f)
  {
    // float3 h2 = (dot_NL >= 0.f) ? halfVector(eye, light, ffnormal) : refractionHalfVector(eye, light, ffnormal);
    dot_HV = dot(eye, h);
    dot_NH = dot(ffnormal, h);
    dot_HL = dot(light, h);
    if (dot_HV == 0.f || dot_NH <= 0.f)
    {
      return COLOR(0.f);
    }
  }

  COLOR F(1.f);   //Init to 1 because of simplification by fresnel pdf for dielectrics.

  if (_alpha_g > 0.f)
  {
    // float G  = geometricTerm(dot_HV, dot_NV, dot_HL, dot_NL, _sqr_alpha_g);
    float G = geometricTerm(h, ffnormal, eye, light, make_float2(_alpha_g));
    // float G1 = v_geometricTerm(dot_HV, dot_NV, dot_HL, dot_NL, _sqr_alpha_g);
    //D is cancelled out by pdf.
    float temp_float = G * abs(dot_HV / (dot_NV * dot_NH));
    F *= temp_float;
    // float temp_float = G / G1;
    // F *= (G1 > 0.f) ? temp_float : 0.f;
  }
  // else
  // {
  // float ior_in         = getIOR_in(current_prd.id_wavelength);
  // float ior_out        = getIOR_out(current_prd.id_wavelength);
  // const float ior_ratio = ior_out / ior_in;
  // F *= ior_ratio * ior_ratio;
  // }

  return F;
}

/**
 * @brief      Generates a sample according to the chosen microfacet distribution, in the case of a transmission.
 *
 * @param      h        the half-vector.
 * @param      n        the normal vector.
 * @param      in_dir          the incoming direction, pointing toward the hitpoint.
 *
 * @return     The sample direction (xyz component) and the sampled lobe (w component).
 */
float4 generate_transmissive_sample(float3 h, float3 n, float3 in_dir)
{
  float ior_in         = getIOR_in(current_prd.id_wavelength);
  float ior_out        = getIOR_out(current_prd.id_wavelength);
  float ior_in_ior_out = ior_in / ior_out;

  float cos_in  = dot(h, -in_dir);
  float cos_out = sqrtf(1.f - ior_in_ior_out * ior_in_ior_out * (1.f - cos_in * cos_in));
  if (dot(n, -in_dir) < 0.f) cos_out = -cos_out;

  float3 out_dir = normalize(ior_in_ior_out * in_dir + (ior_in_ior_out * cos_in - cos_out) * h);

  return make_float4(out_dir, -1.f);
}
