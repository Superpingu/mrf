/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/microfacet/conductor/backend/optix/conductor_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *MicrofacetConductorPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *MicrofacetConductorPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  optix::Material optix_mat       = context->createMaterial();
  auto            compile_options = cuda_compile_options;

  auto fresnel_mirror = dynamic_cast<mrf::materials::FresnelMirror *>(material);
  if (fresnel_mirror)
  {
    optix_mat["_alpha_g"]->setFloat(0.f);   //Fresnel mirror is perfectly flat microfacet
    optix_mat["_sqr_alpha_g"]->setFloat(0.f);
    optix_mat["_distribution"]->setInt(0);   //HARDCODED FOR NOW: 0 = GGX distribution.

    compile_options.push_back("-DMICROFACET_CONDUCTOR");

    OptixMicrofacetConductor *ret_mat
        = new OptixMicrofacetConductor(fresnel_mirror, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string> variable_names = {"_eta", "_kappa"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(fresnel_mirror->eta());
    variable_values.push_back(fresnel_mirror->kappa());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

    return ret_mat;
  }

  auto mirror = dynamic_cast<mrf::materials::PerfectMirror *>(material);
  if (mirror)
  {
    optix_mat["_alpha_g"]->setFloat(0.f);   //Fresnel mirror is perfectly flat microfacet
    optix_mat["_sqr_alpha_g"]->setFloat(0.f);
    optix_mat["_distribution"]->setInt(0);   //HARDCODED FOR NOW: 0 = GGX distribution.

    compile_options.push_back("-DMICROFACET_CONDUCTOR");
    OptixMicrofacetConductor *ret_mat = new OptixMicrofacetConductor(mirror, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string> variable_names = {"_eta", "_kappa"};

    //Perfect mirror is smooth "air" microfacet
    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(mrf::materials::RADIANCE_TYPE(1.f));
    variable_values.push_back(mrf::materials::COLOR(0.f));

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

    return ret_mat;
  }

  auto ggx = dynamic_cast<mrf::materials::GGX *>(material);
  if (ggx)
  {
    optix_mat["_alpha_g"]->setFloat(ggx->alpha_g());
    optix_mat["_sqr_alpha_g"]->setFloat(ggx->alpha_g() * ggx->alpha_g());
    optix_mat["_distribution"]->setInt(0);   //HARDCODED FOR NOW: 0 = GGX distribution.

    compile_options.push_back("-DMICROFACET_CONDUCTOR");
    OptixMicrofacetConductor *ret_mat = new OptixMicrofacetConductor(ggx, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string> variable_names = {"_eta", "_kappa"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(ggx->eta());
    variable_values.push_back(ggx->k());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

    return ret_mat;
  }

  return nullptr;
}

}   // namespace optix_backend
}   // namespace mrf
