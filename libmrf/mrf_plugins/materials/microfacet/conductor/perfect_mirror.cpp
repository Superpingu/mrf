/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include "perfect_mirror.hpp"

namespace mrf
{
namespace materials
{
PerfectMirror::PerfectMirror(std::string const &material_name): BRDF(material_name, BRDFTypes::PERFECT_MIRROR) {}

PerfectMirror::~PerfectMirror() {}

bool PerfectMirror::diffuseComponent() const
{
  return false;
}

float PerfectMirror::diffuseAlbedo() const
{
  return 0.0f;
}

float PerfectMirror::specularAlbedo() const
{
  return 1.0f;
}

bool PerfectMirror::specularComponent() const
{
  return true;
}

float PerfectMirror::nonDiffuseAlbedo() const
{
  return 1.0f;
}

bool PerfectMirror::nonDiffuseComponent() const
{
  return true;
}

float PerfectMirror::diffPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float PerfectMirror::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 1.0f;
}

float PerfectMirror::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 1.0f;
}

RADIANCE_TYPE
PerfectMirror::coloredBRDFValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  assert(0);
  return mrf::materials::RADIANCE_TYPE();
}

ScatterEvent::SCATTER_EVT PerfectMirror::generateScatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    RADIANCE_TYPE & /*incident_radiance*/,
    mrf::math::Vec3f &outgoing_dir) const
{
  using namespace mrf::math;

  Vec3f const normal = lf.normal();
  outgoing_dir       = 2.0f * (-in_dir.dot(normal)) * normal + in_dir;

  return ScatterEvent::SPECULAR_REFL;
  ;
}



ScatterEvent::SCATTER_EVT PerfectMirror::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float /*e1*/,
    float /*e2*/,
    mrf::math::Vec3f &outgoing_dir,
    RADIANCE_TYPE &   brdf_corrected_by_pdf) const
{
  RADIANCE_TYPE tmp;
  generateScatteringDirection(lf, in_dir, tmp, outgoing_dir);

  brdf_corrected_by_pdf = RADIANCE_TYPE(1.0f);

  return ScatterEvent::SPECULAR_REFL;
  ;
}



}   // namespace materials

}   // namespace mrf
