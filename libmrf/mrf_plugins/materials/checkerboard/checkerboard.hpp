/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *CHECKERBOARD = "Checkerboard";
}   // namespace BRDFTypes

/**
 *
 * A simple class that implements a checkerboard pattern which
 * frequency can be controlled by the user.
 *
 * This is a form of procedural texturing
 *
 */
class MRF_PLUGIN_EXPORT CheckerBoard: public BRDF
{
private:
  /**
   * The repetition in [1,N] that controls the number of
   * checker repetitions
   */
  uint          _repetition;
  RADIANCE_TYPE _color1;
  RADIANCE_TYPE _color2;


public:
  CheckerBoard(
      std::string const &  name,
      uint                 repetition = 1,
      RADIANCE_TYPE const &color_1    = RADIANCE_TYPE(1.0),
      RADIANCE_TYPE const &color_2    = RADIANCE_TYPE(0.0));
  virtual ~CheckerBoard();

  inline void         setColor1(COLOR const &a_color);
  inline COLOR const &getColor1() const;
  inline void         setColor2(COLOR const &a_color);
  inline COLOR const &getColor2() const;
  inline void         setRepetition(uint const &repetition);
  inline uint         getRepetition() const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;

  // For Importance Sampling
  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;
};

inline void CheckerBoard::setColor1(COLOR const &a_color)
{
  _color1 = a_color;
}

inline COLOR const &CheckerBoard::getColor1() const
{
  return _color1;
}
inline void CheckerBoard::setColor2(COLOR const &a_color)
{
  _color2 = a_color;
}

inline COLOR const &CheckerBoard::getColor2() const
{
  return _color2;
}

inline void CheckerBoard::setRepetition(uint const &repetition)
{
  _repetition = repetition;
}

inline uint CheckerBoard::getRepetition() const
{
  return _repetition;
}
}   // namespace materials
}   // namespace mrf
