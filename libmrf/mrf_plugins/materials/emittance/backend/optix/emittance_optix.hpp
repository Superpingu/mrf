/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_material_plugin.hpp>
#include <mrf_optix/optix_light_plugin.hpp>

#include <mrf_plugins/materials/emittance/diffuse_emittance.hpp>
#include <mrf_plugins/materials/emittance/conical_emittance.hpp>
#include <mrf_plugins/materials/emittance/dirac_emittance.hpp>
#include <mrf_plugins/materials/emittance/uniform_emittance.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <vector>
#include <map>


namespace mrf
{
namespace optix_backend
{
class MRF_PLUGIN_EXPORT OptixDiffuseEmittance: public OptixEmittanceMaterial
{
public:
  OptixDiffuseEmittance(optix::Material mat): OptixEmittanceMaterial(mat) {}
  OptixDiffuseEmittance(std::vector<std::string> const &cuda_compile_options)
    : OptixEmittanceMaterial(cuda_compile_options)
  {}
  OptixDiffuseEmittance(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixEmittanceMaterial(mat, cuda_compile_options)
  {}

  OptixDiffuseEmittance(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options)
    : OptixEmittanceMaterial(mrf_mat, mat, cuda_compile_options)
  {}
  OptixDiffuseEmittance(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
    : OptixEmittanceMaterial(mrf_mat, mat, cuda_compile_options, ptx_cfg)
  {}

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &, std::vector<uint>) {};
#else
  virtual void               updateTextures(optix::Context &) {};
#endif
};

class MRF_PLUGIN_EXPORT OptixDiracEmittance: public OptixEmittanceMaterial
{
public:
  OptixDiracEmittance(optix::Material mat): OptixEmittanceMaterial(mat) {}
  OptixDiracEmittance(std::vector<std::string> const &cuda_compile_options)
    : OptixEmittanceMaterial(cuda_compile_options)
  {}
  OptixDiracEmittance(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
    : OptixEmittanceMaterial(mat, cuda_compile_options)
  {}

  OptixDiracEmittance(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options)
    : OptixEmittanceMaterial(mrf_mat, mat, cuda_compile_options)
  {}
  OptixDiracEmittance(
      mrf::materials::UMat *          mrf_mat,
      optix::Material                 mat,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg)
    : OptixEmittanceMaterial(mrf_mat, mat, cuda_compile_options, ptx_cfg)
  {}

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateTextures(optix::Context &, std::vector<uint>) {};
#else
  virtual void               updateTextures(optix::Context &) {};
#endif

  virtual void compile(optix::Context &context) {}
};


///////////////////////////////////////////////////////////////////////////////////
////////////////PLUGINS////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

class MRF_PLUGIN_EXPORT DiffuseEmittancePlugin: public OptixMaterialPlugin
{
public:
  DiffuseEmittancePlugin() {}
  ~DiffuseEmittancePlugin() {}

  static OptixMaterialPlugin *create() { return new DiffuseEmittancePlugin; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg,
      std::vector<uint>               wavelengths);
#else
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg);
#endif

private:
};

class MRF_PLUGIN_EXPORT DiracEmittancePlugin: public OptixMaterialPlugin
{
public:
  DiracEmittancePlugin() {}
  ~DiracEmittancePlugin() {}

  static OptixMaterialPlugin *create() { return new DiracEmittancePlugin; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg,
      std::vector<uint>               wavelengths);
#else
  virtual OptixBaseMaterial *createMaterialFromMRF(
      mrf::materials::UMat *          material,
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      PTXConfig                       ptx_cfg);
#endif

private:
};

}   // namespace optix_backend
}   // namespace mrf
