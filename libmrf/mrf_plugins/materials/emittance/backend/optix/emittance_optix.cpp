/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/emittance/backend/optix/emittance_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *DiffuseEmittancePlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *DiffuseEmittancePlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  //if (!material) return;
  optix::Material    optix_mat = context->createMaterial();
  OptixBaseMaterial *ret_mat;
  auto               compile_options = cuda_compile_options;

  auto diffuse_emittance = dynamic_cast<mrf::materials::DiffuseEmittance *>(material);
  if (diffuse_emittance)
  {
    optix_mat["registered_as_light"]->setInt(0);

    ret_mat = new OptixDiffuseEmittance(diffuse_emittance, optix_mat, compile_options, ptx_cfg);

    float                    radiance       = diffuse_emittance->getRadiance();
    std::vector<std::string> variable_names = {"emission_color"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(diffuse_emittance->getColor() * radiance);

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif
    //compileEmittance(optix_mat, compile_options);

    //return optix_mat;
    return ret_mat;
  }

  return nullptr;
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *DiracEmittancePlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *DiracEmittancePlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  auto dirac_emittance = dynamic_cast<mrf::materials::DiracEmittance *>(material);
  if (dirac_emittance)
  {
    optix::Material optix_mat = context->createMaterial();
    return new OptixDiracEmittance(dirac_emittance, optix_mat, cuda_compile_options, ptx_cfg);
  }

  return nullptr;
}

}   // namespace optix_backend
}   // namespace mrf
