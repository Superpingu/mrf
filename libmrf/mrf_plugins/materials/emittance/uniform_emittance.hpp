/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_plugins/materials/emittance/diffuse_emittance.hpp>


namespace mrf
{
namespace materials
{
namespace EmittanceTypes
{
static char const *UNIFORM = "uniform";
};   // namespace EmittanceTypes

//-------------------------------------------------------------------------
// Uniform Emittance Class
//-------------------------------------------------------------------------

/**
 * This is an emitter which emits radiance in all direction
 * with the same intensity.
 * Usefull for Sphere or Point Light sources
 *
 **/
class MRF_PLUGIN_EXPORT UniformEmittance: public DiffuseEmittance
{
public:
  UniformEmittance(std::string const &name, float radiance = 1.0f, COLOR const &color = COLOR(1.0f));


  virtual RADIOSITY_TYPE
  particleEmission(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2, mrf::math::Vec3f &direction)
      const;

protected:
  virtual void computeInternalValues();
};

}   // namespace materials
}   // namespace mrf
