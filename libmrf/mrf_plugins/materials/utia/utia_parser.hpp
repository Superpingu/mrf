#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>

#include <mrf_core/io/base_plugin_parser.hpp>
#include <mrf_plugins/materials/utia/measured_utia.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
class UTIAParser: public BaseMaterialParser<mrf::materials::UTIA>
{
public:
  UTIAParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new UTIAParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::UTIA; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::UTIA *utia_mat = dynamic_cast<mrf::materials::UTIA *>(a_mat);

    bool        texture_parsed = false;
    std::string texture_path;

    tinyxml2::XMLElement *texture_elem = a_mat_element->FirstChildElement(SceneLexer::UTIA_TEXTURE_MK);
    if (texture_elem)
    {
      std::string file;
      if (!texture_elem->Attribute(SceneLexer::FILE_PATH_AT))
      {
        mrf::gui::fb::Loger::getInstance()->fatal(" NO FILE PATH SPECIFIED for this texture. ABORTING !!!");
      }
      else
      {
        std::string texture_file = texture_elem->Attribute(SceneLexer::FILE_PATH_AT);

        mrf::gui::fb::Loger::getInstance()->trace(" Found : TEXTURE FILE: ", texture_file);
        texture_path = parser._scene_path + texture_file;
        mrf::gui::fb::Loger::getInstance()->trace(" Full Path of file is: ", texture_path);

        texture_parsed = true;
      }
    }

    if (texture_parsed)
    {
      if (!parser.checkErrorOnTextureLoading(utia_mat->loadUTIAData(texture_path), texture_path))
      {
        utia_mat = nullptr;
        return false;
      }


      unsigned int interpolate = 0;
      int          interpolOK  = texture_elem->QueryUnsignedAttribute(SceneLexer::UTIA_INTERPOLATION_AT, &interpolate);
      if (interpolOK == tinyxml2::XML_SUCCESS) utia_mat->setInterpolate((bool)interpolate);

      //int param;
      //int paramOK = a_tex_element->QueryIntAttribute(SceneLexer::UTIA_PARAMETERIZATION_AT, &param);
      //if (paramOK != XML_SUCCESS)
      //{
      //  if (utia_mat->getPhiVDiv() * utia_mat->getPhiIDiv() == 1)
      //    utia_mat->setParameterization(UTIA::ISOTROPIC_DH);
      //  else
      //    utia_mat->setParameterization(UTIA::STANDARD);
      //}
      //else
      //{
      //  //TODO use string ?
      //  utia_mat->setParameterization(param);
      //}

      const char *param;
      int         paramOK = texture_elem->QueryStringAttribute(SceneLexer::UTIA_PARAMETERIZATION_AT, &param);
      if (paramOK != tinyxml2::XML_SUCCESS)
      {
        if (utia_mat->getPhiVDiv() * utia_mat->getPhiIDiv() == 1)
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_2D);
        else
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D);
      }
      else
      {
        std::string param_str = std::string(param);
        if (param_str.compare(SceneLexer::UTIA_2D_VAL) == 0)
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_2D);
        }
        else if (param_str.compare(SceneLexer::RUSINKIEWICZ_2D_VAL) == 0)
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_2D);
        }
        else if (param_str.compare(SceneLexer::RUSINKIEWICZ_2D_COS_VAL) == 0)
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_2D_COS);
        }
        else if (param_str.compare(SceneLexer::RUSINKIEWICZ_4D_VAL) == 0)
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::RUSINKIEWICZ_4D);
        }
        else if (param_str.compare(SceneLexer::UTIA_4D_H_VAL) == 0)
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D_H);
        }
        else if (param_str.compare(SceneLexer::UTIA_4D_H_NL_VAL) == 0)
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D_H_NL);
        }
        else if (param_str.compare(SceneLexer::HEMISPHERICAL_VAL) == 0)
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::HEMISPHERICAL_ALBEDO);
        }
        else
        {
          utia_mat->setParameterization(mrf::materials::MeasuredMaterial::UTIA_4D);
        }
      }

      tinyxml2::XMLElement *div_elem = texture_elem->FirstChildElement(SceneLexer::UTIA_PARAM_MK);

      unsigned int theta_v = (unsigned int)utia_mat->width();
      unsigned int phi_v   = 1;
      unsigned int theta_i = (unsigned int)utia_mat->height();
      unsigned int phi_i   = 1;
      if (div_elem)
      {
        //values are not modified if attribute does not exist.
        int divOK = div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_THETA_V_AT, &theta_v);
        divOK &= div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_PHI_V_AT, &phi_v);
        divOK &= div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_THETA_I_AT, &theta_i);
        divOK &= div_elem->QueryUnsignedAttribute(SceneLexer::UTIA_PHI_I_AT, &phi_i);
      }
      else
      {
        if (utia_mat->getParameterization() == mrf::materials::UTIA::UTIA_2D
            || utia_mat->getParameterization() == mrf::materials::UTIA::RUSINKIEWICZ_2D
            || utia_mat->getParameterization() == mrf::materials::UTIA::RUSINKIEWICZ_2D_COS)
        {
          //If isotropic DH requested, only warn that image dimension are used, do not force the V/I parameterization.
          mrf::gui::fb::Loger::getInstance()->trace(
              "No parameter markup detected, using image resolution as theta (D/H) resolution.");
        }
        else
        {
          mrf::gui::fb::Loger::getInstance()->trace(
              "No parameter markup detected, using isotropic parameterization with image resolution as theta (V/I) resolution.");
          //Force isotropic parameterization.
          utia_mat->setParameterization(mrf::materials::UTIA::UTIA_2D);
        }
      }

      utia_mat->setThetaVDiv(theta_v);
      utia_mat->setPhiVDiv(phi_v);
      utia_mat->setThetaIDiv(theta_i);
      utia_mat->setPhiIDiv(phi_i);

      mrf::gui::fb::Loger::getInstance()->trace("Parsed and loaded single UTIA texture with param:");
      mrf::gui::fb::Loger::getInstance()->trace(
          "Theta V: " + std::to_string(theta_v) + "| Phi V : " + std::to_string(phi_v));
      mrf::gui::fb::Loger::getInstance()->trace(
          "Theta I: " + std::to_string(theta_i) + "| Phi I : " + std::to_string(phi_i));
    }
    return true;
  }

protected:
private:
};

class UTIAIndexedParser: public BaseMaterialParser<mrf::materials::IndexedUTIA>
{
public:
  UTIAIndexedParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new UTIAIndexedParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::UTIA_INDEXED; }

  virtual bool parse(tinyxml2::XMLElement *, mrf::materials::UMat *a_mat, ParserHelper &)
  {
    mrf::materials::IndexedUTIA *utia_mat = dynamic_cast<mrf::materials::IndexedUTIA *>(a_mat);

    return utia_mat != nullptr;
  }

protected:
private:
};

}   // namespace io
}   // namespace mrf
