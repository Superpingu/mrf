/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <memory>

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/measured_material.hpp>

#include <mrf_core/image/color_image.hpp>
#include <mrf_core/image/uniform_spectral_image.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
// (http://btf.utia.cas.cz/?intro)
static char const *UTIA         = "UTIA";
static char const *UTIA_INDEXED = "IndexedUTIA";
};   // namespace BRDFTypes

//-------------------------------------------------------------------------
// List of UTIA materials, indexed by a map.
//-------------------------------------------------------------------------

class MRF_PLUGIN_EXPORT UTIA: public MeasuredMaterial
{
protected:
#ifdef MRF_RENDERING_MODE_SPECTRAL
  std::unique_ptr<mrf::image::UniformSpectralImage> _utia_data;
#else
  std::unique_ptr<mrf::image::ColorImage> _utia_data;
#endif
  std::string       _file;
  mrf::Doublet_uint _data_size;

  unsigned int _thetaVdiv;
  unsigned int _phiVdiv;
  unsigned int _thetaIdiv;
  unsigned int _phiIdiv;

public:
  UTIA(): MeasuredMaterial("default_utia", BRDFTypes::UTIA) {};

  UTIA(std::string const &name);

  virtual ~UTIA();


  unsigned int getThetaVDiv() const { return _thetaVdiv; }
  void         setThetaVDiv(unsigned int value) { _thetaVdiv = value; }

  unsigned int getPhiVDiv() const { return _phiVdiv; }
  void         setPhiVDiv(unsigned int value) { _phiVdiv = value; }

  unsigned int getThetaIDiv() const { return _thetaIdiv; }
  void         setThetaIDiv(unsigned int value) { _thetaIdiv = value; }

  unsigned int getPhiIDiv() const { return _phiIdiv; }
  void         setPhiIDiv(unsigned int value) { _phiIdiv = value; }


  bool hasTexture() const { return _utia_data.get() != nullptr; }

  size_t width() const { return _utia_data->width(); }
  size_t height() const { return _utia_data->height(); }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  mrf::image::UniformSpectralImage const &getUTIAData() const { return *_utia_data.get(); }
#else
  mrf::image::ColorImage const &          getUTIAData() const { return *_utia_data.get(); }
#endif

  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadUTIAData(std::string const &filename);

  virtual float brdfValue(mrf::geom::LocalFrame const &, mrf::math::Vec3f const &, mrf::math::Vec3f const &) const
  {
    return 0.f;
  }

  virtual RADIANCE_TYPE
  coloredBRDFValue(mrf::geom::LocalFrame const &, mrf::math::Vec3f const &, mrf::math::Vec3f const &) const
  {
    return RADIANCE_TYPE(0.f);
  }
};


class MRF_PLUGIN_EXPORT IndexedUTIA: public BRDF
{
protected:
  std::map<int, UTIA *> _utia_materials;

  std::unique_ptr<mrf::image::ColorImage> _utia_index_map;

  std::map<int, unsigned int> _utia_index_list;

  bool _interpolate;

public:
  IndexedUTIA(std::string const &name);

  virtual ~IndexedUTIA();

  unsigned int getMatIndex(const int index = 0) const { return _utia_index_list.at(index); }
  void         setMatIndex(unsigned int value, const int index = 0) { _utia_index_list[index] = value; }

  unsigned int getThetaVDiv(const int index = 0) const { return _utia_materials.at(index)->getThetaVDiv(); }
  void         setThetaVDiv(unsigned int value, const int index = 0) { _utia_materials[index]->setThetaVDiv(value); }

  unsigned int getPhiVDiv(const int index = 0) const { return _utia_materials.at(index)->getPhiVDiv(); }
  void         setPhiVDiv(unsigned int value, const int index = 0) { _utia_materials[index]->setPhiVDiv(value); }

  unsigned int getThetaIDiv(const int index = 0) const { return _utia_materials.at(index)->getThetaIDiv(); }
  void         setThetaIDiv(unsigned int value, const int index = 0) { _utia_materials[index]->setThetaIDiv(value); }

  unsigned int getPhiIDiv(const int index = 0) const { return _utia_materials.at(index)->getPhiIDiv(); }
  void         setPhiIDiv(unsigned int value, const int index = 0) { _utia_materials[index]->setPhiIDiv(value); }

  unsigned int getParameterization(const int index = 0) const
  {
    return _utia_materials.at(index)->getParameterization();
  }
  void setParameterization(MeasuredMaterial::Parameterization value, const int index = 0)
  {
    _utia_materials[index]->setParameterization(value);
  }

  bool getInterpolate() const { return _interpolate; }
  bool getInterpolate(const int index) const { return _utia_materials.at(index)->getInterpolate(); }
  void setInterpolate(bool value) { _interpolate = value; }
  void setInterpolate(bool value, const int index) { _utia_materials.at(index)->setInterpolate(value); }

  bool hasTexture(const int index = 0) const { return _utia_materials.at(index)->hasTexture(); }
  bool hasMap() const { return _utia_index_map.get() != nullptr; }

  size_t width(const int index = 0) const { return _utia_materials.at(index)->width(); }
  size_t height(const int index = 0) const { return _utia_materials.at(index)->height(); }
  size_t depth() const { return _utia_materials.size(); }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  mrf::image::UniformSpectralImage const &getUTIAData(const int index = 0) const
  {
    return _utia_materials.at(index)->getUTIAData();
  }
#else
  mrf::image::ColorImage const &          getUTIAData(const int index = 0) const
  {
    return _utia_materials.at(index)->getUTIAData();
  }
#endif

  mrf::image::ColorImage const &getUTIAMap() const { return *_utia_index_map.get(); }

  void normalizeMapIndices(std::string exportPath = "");

  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadUTIAData(std::string const &filename, const int index = 0);

  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadUTIAMap(std::string const &filename);

  virtual float brdfValue(mrf::geom::LocalFrame const &, mrf::math::Vec3f const &, mrf::math::Vec3f const &) const
  {
    return 0.f;
  }

  virtual RADIANCE_TYPE
  coloredBRDFValue(mrf::geom::LocalFrame const &, mrf::math::Vec3f const &, mrf::math::Vec3f const &) const
  {
    return RADIANCE_TYPE(0.f);
  }
};

}   // namespace materials
}   // namespace mrf
