//-----------------------------------------------------------------------------
//
//  Template for closest-hit
//  To be included in a central kernel located at
//  (libmrf/backends/optix/)mrf_optix_programs/BRDF_kernel.cu or BSDF_kernel.cu
//  WARNING: the include is to be performed by the cpp class.
//
//-----------------------------------------------------------------------------

#include "include/distribution/cosine.h"


//These variables/buffers are uploaded by template_optix.
//The type (buffer, float or float3) is automatically handled by the cpp side.
#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> a_color;
rtBuffer<float, 1> another_color;
#  else
rtDeclareVariable(float, a_color, , );
rtDeclareVariable(float, another_color, , );
#  endif
#else
rtDeclareVariable(float3, a_color, , );
rtDeclareVariable(float3, another_color, , );
#endif

//This variable is uploaded by template_optix.
rtDeclareVariable(float, a_variable, , );

//Mandatory. True is the BRDF/BSDF is a perfect reflector/transmitter.
bool bsdf_is_dirac()
{
  return false;
}


//Mandatory.
COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  float NdotL = max(dot(ffnormal, light), 0.0f);
  float NdotV = max(dot(ffnormal, eye), 0.0f);
  if (NdotL <= 0.f || NdotV <= 0.f) return COLOR(0.f);

  COLOR tmp;
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

//In multiplexed mode, we need to get interpolated value.
#  ifdef SOME_DEFINE
  tmp = colorFromAsset(a_color, current_prd.wavelength_offset, current_prd.id_wavelength);
#  else
  tmp = colorFromAsset(another_color, current_prd.wavelength_offset, current_prd.id_wavelength);
#  endif   //SOME_DEFINE

#else

#  ifdef SOME_DEFINE
  tmp = a_color;
#  else
  tmp = another_color;
#  endif   //SOME_DEFINE

#endif   //MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

  return tmp * INV_PIf * NdotL;
}

//Mandatory.
//bsdf_eval_optim should return the BSDF, already multiplied by n dot l and divided by sample pdf (bsdf_pdf).
COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  float NdotL = max(dot(ffnormal, light), 0.0f);
  float NdotV = max(dot(ffnormal, eye), 0.0f);
  if (NdotL <= 0.f || NdotV <= 0.f) return COLOR(0.f);

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

//In multiplexed mode, we need to get interpolated value.
#  ifdef SOME_DEFINE
  return colorFromAsset(a_color, current_prd.wavelength_offset, current_prd.id_wavelength);
#  else
  return colorFromAsset(another_color, current_prd.wavelength_offset, current_prd.id_wavelength);
#  endif   //SOME_DEFINE

#else

#  ifdef SOME_DEFINE
  return a_color;
#  else
  return another_color;
#  endif   //SOME_DEFINE

#endif   //MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
}

//Mandatory.
float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  return cosine_pdf(ffnormal, eye, light);
}

//Mandatory.
float3 generate_half_vector(float3 n, float3 in_dir)
{
  return n;
}

//Mandatory.
float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  float2 rnd2d = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);
  float z1 = rnd2d.x;
  float z2 = rnd2d.y;

  return make_float4(cosine_sample(z1, z2, in_dir, n), ANY_REFL);
}
