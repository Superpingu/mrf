/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_material_plugin.hpp>
#include <mrf_optix/optix_light_plugin.hpp>

#include <mrf_plugins/materials/emittance/diffuse_emittance.hpp>
#include <mrf_plugins/materials/emittance/conical_emittance.hpp>
#include <mrf_plugins/materials/emittance/dirac_emittance.hpp>
#include <mrf_plugins/materials/emittance/uniform_emittance.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <vector>
#include <map>


namespace mrf
{
namespace optix_backend
{
////////////////////////////////////////////////////////////////////////
//////////////////////LIGHTS////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


class MRF_PLUGIN_EXPORT OptixQuadLight: public OptixBaseLight
{
public:
  OptixQuadLight(mrf::lighting::Light *light);
  virtual ~OptixQuadLight() {};

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateLight(std::vector<uint> const &wavelengths);
#else
  virtual void updateLight();
#endif

  virtual optix::GeometryInstance
  getGeometryInstance(optix::Context &context, std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_cfg);

  virtual BaseLight *getAsStruct()
  {
    QuadLight *qL = new QuadLight;
    qL->corner    = _corner;
    qL->normal    = _normal;
#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
    qL->emission = _emission;
#endif
    qL->v1 = _v1;
    qL->v2 = _v2;
    return qL;
  }

protected:
  optix::float3 _normal;
  optix::float3 _corner;
  optix::float3 _v1;
  optix::float3 _v2;
};

class MRF_PLUGIN_EXPORT OptixSphereLight: public OptixBaseLight
{
public:
  OptixSphereLight(mrf::lighting::Light *light);
  virtual ~OptixSphereLight() {};

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateLight(std::vector<uint> const &wavelengths);
#else
  virtual void updateLight();
#endif

  virtual optix::GeometryInstance
  getGeometryInstance(optix::Context &context, std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_cfg);

  virtual BaseLight *getAsStruct()
  {
    SphereLight *sL = new SphereLight;
#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
    sL->emission = _emission;
#endif
    sL->position = _position;
    sL->radius   = _radius;
    return sL;
  }

protected:
  optix::float3 _position;
  float         _radius;
};

class MRF_PLUGIN_EXPORT OptixDirectionnalLight: public OptixBaseLight
{
public:
  OptixDirectionnalLight(mrf::lighting::Light *light);
  virtual ~OptixDirectionnalLight() {};

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual void updateLight(std::vector<uint> const &wavelengths);
#else
  virtual void updateLight();
#endif

  virtual optix::GeometryInstance
  getGeometryInstance(optix::Context &context, std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_cfg);

  virtual BaseLight *getAsStruct()
  {
    DirectionnalLight *dL = new DirectionnalLight;
#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
    dL->emission = _emission;
#endif
    dL->direction = _direction;
    return dL;
  }

protected:
  optix::float3 _direction;
};

class MRF_PLUGIN_EXPORT QuadLightPlugin: public OptixLightPlugin
{
public:
  QuadLightPlugin() {};
  virtual ~QuadLightPlugin() {};

  static OptixLightPlugin *create() { return new QuadLightPlugin; }

  virtual OptixBaseLight *createFromMRF(mrf::lighting::Light *light);

protected:
};

class MRF_PLUGIN_EXPORT SphereLightPlugin: public OptixLightPlugin
{
public:
  SphereLightPlugin() {};
  virtual ~SphereLightPlugin() {};

  static OptixLightPlugin *create() { return new SphereLightPlugin; }

  virtual OptixBaseLight *createFromMRF(mrf::lighting::Light *light);

protected:
};

class MRF_PLUGIN_EXPORT DirectionnalLightPlugin: public OptixLightPlugin
{
public:
  DirectionnalLightPlugin() {};
  virtual ~DirectionnalLightPlugin() {};

  static OptixLightPlugin *create() { return new DirectionnalLightPlugin; }

  virtual OptixBaseLight *createFromMRF(mrf::lighting::Light *light);

protected:
};

}   // namespace optix_backend
}   // namespace mrf
