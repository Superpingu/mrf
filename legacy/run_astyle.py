################################################
## Author # Arthur Dufay
##
## Indents (recursively) all the files of folder given as parameter
################################################

import os, sys

folder = "."

if len(sys.argv)>1:
	folder = sys.argv[1]

print("Folder path set to "+folder)

if os.path.isdir(folder)==False:
	print("Error folder does not exist")
	sys.exit()

print("")
print("=================================================")
print(" Applying AStyle automatic indentation to sources")
print("=================================================")
print("")

os.system("astyle --options=astyle.options "+folder+'"/*.cpp"' )
os.system("astyle --options=astyle.options "+folder+'"/*.cxx"' )
os.system("astyle --options=astyle.options "+folder+'"/*.h"' )
os.system("astyle --options=astyle.options "+folder+'"/*.inl"' )
os.system("astyle --options=astyle.options "+folder+'"/*.hpp"' )
os.system("astyle --options=astyle.options "+folder+'"/*.cu"' )

print("")
print("=================================================")
print("")

#end
