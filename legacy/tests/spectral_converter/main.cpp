//TEST SPECTRUM INTEGRATION
std::vector<uint> wavelengths;
std::vector<float> values;

wavelengths.push_back(300);
wavelengths.push_back(305);
wavelengths.push_back(310);

values.push_back(10);
values.push_back(20);
values.push_back(10);

SpectrumConverter converter;

auto value = converter.spectrumToGrayRGB(wavelengths, values);
//150
std::cout << value << std::endl;

value = converter.spectrumToGrayRGB(wavelengths, values,306,309);
//45
std::cout << value << std::endl;

value = converter.spectrumToGrayRGB(wavelengths, values, 306, 310);
//56
std::cout << value << std::endl;

value = converter.spectrumToGrayRGB(wavelengths, values, 306, 311);
//56
std::cout << value << std::endl;