
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <vector>

#include "mrf/util/precision_timer.hpp"
#include "mrf/sampling/random_generator.hpp"


#include "pcg_random.hpp"

using namespace std;
using namespace mrf::util;
using namespace mrf::sampling;


long long int NB_RANDOM_NUMBERS = 400000000;

void bench1()
{
  PrecisionTimer a_timer;

  RandomGenerator &rnd_gen = RandomGenerator::Instance();

  std::vector<float> samples;
  samples.reserve(4 * NB_RANDOM_NUMBERS);

  a_timer.reset();
  a_timer.start();
  for (long long int i = 0; i < NB_RANDOM_NUMBERS; i++)
  {
    samples.push_back(rnd_gen.getFloat());
  }
  a_timer.stop();
  float elapsed_time = a_timer.elapsed();
  cout << " [BENCH 1] MRF RandomGenerator. Pushing " << NB_RANDOM_NUMBERS << " in " << elapsed_time << " On a one thread (i.e., " << NB_RANDOM_NUMBERS / elapsed_time << " random numbers per second" << endl;
}


void bench2()
{
  RandomGenerator &rnd_gen = RandomGenerator::Instance();


  PrecisionTimer a_timer;
  a_timer.reset();
  a_timer.start();
  for (long long int i = 0; i < NB_RANDOM_NUMBERS; i++)
  {
    rnd_gen.getFloat();
  }
  a_timer.stop();
  float elapsed_time = a_timer.elapsed();
  cout << " [BENCH 2] MRF RandomGenerator.  Generation only " << NB_RANDOM_NUMBERS << " in " << elapsed_time << " On a one thread (i.e., " << NB_RANDOM_NUMBERS / elapsed_time << " random numbers per second )" << endl;
}

void bench3( )
{

  PrecisionTimer a_timer;

  RandomGenerator &rnd_gen = RandomGenerator::Instance();
  

  std::vector<float> samples;
  samples.resize( NB_RANDOM_NUMBERS);

  a_timer.reset();
  a_timer.start();
  for (long long int i = 0; i < NB_RANDOM_NUMBERS; i++)
  {
    samples[i] = rnd_gen.getFloat();
  }

  a_timer.stop();

  float elapsed_time = a_timer.elapsed();
  cout << " [BENCH 3] MRF RandomGenerator. Assignment " << NB_RANDOM_NUMBERS << " in " << elapsed_time << " On a one thread (i.e., " << NB_RANDOM_NUMBERS / elapsed_time << " random numbers per second )" << endl;
}



template<class GENERATOR_TYPE>
void bench4( std::string const & add_message_info ) 
{

  std::uniform_real_distribution<float>  uni_dist(0.0f, 1.0f);

  GENERATOR_TYPE a_gen( time(NULL) );


  PrecisionTimer a_timer;
  std::vector<float> samples;
  samples.resize(NB_RANDOM_NUMBERS);

  a_timer.reset();
  a_timer.start();
  for (long long int i = 0; i < NB_RANDOM_NUMBERS; i++)
  {
    samples[i] = uni_dist( a_gen );
  }

  float elapsed_time = a_timer.elapsed();
  cout << " [BENCH 4] STL RandomGenerator:" << add_message_info << " Assignment "  " in " << elapsed_time << endl;
  cout << " On a one thread (i.e., " << NB_RANDOM_NUMBERS / elapsed_time << " random numbers per second )" << endl;
  cout << endl;
}

template<class PCG_GENERATOR_TYPE>
void benchPCG( std::string const & add_message_info )
{
  // Seed with a real random value, if available
  pcg_extras::seed_seq_from<std::random_device> seed_source;

  // Make a random number engine
  PCG_GENERATOR_TYPE rng(seed_source);

  // Choose a random mean between 1 and 6
  std::uniform_real_distribution<float> uniform_dist(0.0f, 1.0f);
  
  PrecisionTimer a_timer;
  std::vector<float> samples;
  samples.resize(NB_RANDOM_NUMBERS);

  a_timer.reset();
  a_timer.start();
  for (long long int i = 0; i < NB_RANDOM_NUMBERS; i++)
  {
    samples[i] =  uniform_dist(rng);
  }  
  float elapsed_time = a_timer.elapsed();
  cout << " [BENCH 5] PCG RandomGenerator: " <<  add_message_info <<  " Assignment in " << elapsed_time << endl;
  cout << " On a one thread (i.e., " << NB_RANDOM_NUMBERS / elapsed_time << " random numbers per second )" << endl;
  cout << endl;

}


int main(int argc, char** argv)
{
  cout << "[INFO] Starting . Testing with " << NB_RANDOM_NUMBERS << endl;

  bench1();
  bench2();
  bench3();

  //typedef std::mt19937_64 GENERATOR_TYPE;

  bench4<std::mt19937_64>("std::mt19937_64");
  bench4<std::mt19937>("std::mt19937");

  bench4<std::minstd_rand>("std::minstd_rand");

  
  bench4<std::ranlux24_base>("std::ranlux24_base");
  bench4<std::ranlux48_base>("std::ranlux48_base");
  
  //These twos are super ;pw
  // bench4<std::ranlux48>("std::ranlux24");
  // bench4<std::ranlux48>("std::ranlux48");
  
  bench4<std::knuth_b>( "std::knuth_b");

  benchPCG<pcg32>( "PCG32");
  benchPCG<pcg32_oneseq>( "pcg32_oneseq");
  benchPCG<pcg32_unique>("pcg32_unique");
  benchPCG<pcg32_fast>("pcg32_fast");
  

  benchPCG<pcg64>( "PCG64");
  benchPCG<pcg64_fast>( "pcg64_fast");



  //SEEDING WITH 1000
  std::cout<< " Testing 10 random numbers with seed = 1000 " << std::endl;
  pcg32 rng(1000);
  std::uniform_real_distribution<float> uniform_dist(0.0f, 1.0f);

  for(unsigned int i=0 ; i < 10; i++)
  {
    std::cout << " Number " << i << ": " << uniform_dist( rng ) << " ";
  }
  std::cout << std::endl;


  //SEEDING WITH 1000
  std::cout<< " Testing 10 random numbers with new seed every time " << std::endl;
  pcg_extras::seed_seq_from<std::random_device> seed_source;

  pcg32 rng2(seed_source);
  std::uniform_real_distribution<float> uniform_dist2(0.0f, 1.0f);

  for(unsigned int i=0 ; i < 10; i++)
  {
    std::cout << " Number " << i << ": " << uniform_dist2( rng2 ) << " ";
  }
  std::cout << std::endl;


  cout <<"[INFO] End of benchmarking" << endl;


  return EXIT_SUCCESS;
}