
Simple code to test and benchmark random number generators

 - From the STL
   - Mersenne Twister


 All tests are performed with one or multiple-thread.
 The data are exported as a single 1D binary floating point array.

This array can after be loaded and visualized using numpy/matplotlib
or matlab to verify the statistics properties of the generated numbers
