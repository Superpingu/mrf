/*
*
* author : Arthur Dufay @ inria.fr
* Copyright INRIA 2017
*
*
**/



//test tiny png
#include <string>
#include <iostream>
#include <chrono>

#include "mrf/image/colorimage.hpp"
#include "externals/tinypng/lodepng.h"
#include "mrf/radiometry/d65.hpp"
#include "mrf/image/uniformspectralimage.hpp"

#include "externals/tinypng/lodepng.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace mrf;
using namespace mrf::image;
using namespace mrf::color;
using namespace mrf::radiometry;


typedef std::chrono::high_resolution_clock Clock;

int main(int argc, char *argv[])
{
  int num_thread = omp_get_max_threads();

  ofstream my_file;
  my_file.open("result.csv");
  //my_file << "Result\n";

  UniformSpectralImage _image;

  auto t1 = Clock::now();

  string file_path;
  string folder_path = "D:\\GitStorage\\MRF_test\\blackbody_spectral\\";
  uint max_string_size = 3;
  for (int i = 0; i <= 480; i++)
  {
    std::string file_number = std::to_string(i);
    file_path = file_number;

    //append 0 at the begining of file name
    //to have files ordered by wavelengths
    for (int j = 0; j < max_string_size - file_number.length(); j++)
    {
      file_path = "0" + file_path;
    }

    file_path = folder_path + file_path + ".png";
    _image.addValuesFromGrayImage(file_path, 300 + i);
  }

  auto t2 = Clock::now();

  auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

  my_file <<"loading "<< nanoseconds <<" "<< nanoseconds/1000.f << " " << nanoseconds/(1E6)<<" "<<nanoseconds/(1E9) << "\n";

  SpectrumConverter spectrum_converter;
  spectrum_converter.init(mrf::color::CIE_1931_2DEG);
  ColorImage rgb_image;

  t1 = Clock::now();
  _image.convertToColorImage(spectrum_converter, rgb_image);
  t2 = Clock::now();

  nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

  my_file << "converting to rgb " << nanoseconds << " " << nanoseconds / 1000.f << " " << nanoseconds / (1E6) << " " << nanoseconds / (1E9) << "\n";


  //save image as png
  std::vector<unsigned char> png_data_for_output;
  png_data_for_output.resize(rgb_image.width() * rgb_image.height() * 3);//3 channels image


  for (uint i = 0; i < rgb_image.width(); i++)
  {
    for (uint j = 0; j < rgb_image.height(); j++)
    {
      Color color = rgb_image(i,j).xyzToSrgbD65();
      float norm = 1.f / std::max(std::max(color.r(), color.g()), color.b());
      color *= norm;
      color = color.clamped(0.0, 1.0);

      png_data_for_output[3 * (j*rgb_image.width() + i)] = unsigned char(color.r() * 255);
      png_data_for_output[3 * (j*rgb_image.width() + i) + 1] = unsigned char(color.g() * 255);
      png_data_for_output[3 * (j*rgb_image.width() + i) + 2] = unsigned char(color.b() * 255);
    }
  }



  uint error = lodepng::encode("./blackbody.png", png_data_for_output,
                               rgb_image.width(), rgb_image.height(), LCT_RGB);

  my_file.close();







  cin >> nanoseconds;
}