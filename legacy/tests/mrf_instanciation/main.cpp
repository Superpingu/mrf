#include <cstdlib>
#include <iostream>


#include "io/scene_parser.hpp"
#include "rendering/scene.hpp"
#include "util/cpu_memory.hpp"
#include "mrf/gui/feedback/loger.hpp"

using std::string;
using namespace mrf;
using namespace mrf::rendering;


int main(int argc, char** argv)
{
  const char* mrf_dir = std::getenv("MRF_DIR");

  std::string scene_file = string(mrf_dir) + "\\assets\\scenes\\buddha_fractal.msf";

  mrf::gui::fb::Loger::LEVEL loger_level = mrf::gui::fb::Loger::LEVEL::Info;

  auto memory_before = mrf::util::CPUMemory::usedMemory();

  mrf::rendering::Scene* scene = nullptr;

  int x;
  //std::cin >> x;
  {


    if (!mrf::io::loadScene(scene, scene_file, loger_level))
    {
      std::cout << "CANNOT LOAD SCENE" << std::endl;
      return -1;
    }
    else
    {
      //std::cin >> x;
      auto memory_with_scene_loaded = mrf::util::CPUMemory::usedMemory();
      delete scene;
      scene = nullptr;
      mrf::io::loadScene(scene, scene_file, loger_level);
      delete scene;
      scene = nullptr;
      //std::cin >> x;
      auto memory_after = mrf::util::CPUMemory::usedMemory();

      std::cout << "Memory before " << memory_before << std::endl;
      std::cout << "Memory with scene loaded " << memory_with_scene_loaded << std::endl;
      std::cout << "Memory after " << memory_after << std::endl;

    }
  }
  int y = 2 * 5;
  return 0;
}
