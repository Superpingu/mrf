import csv
import sys


startWave = 380
stepWave = 10
endWave = 730+stepWave





#inputFileName = "in.spd"
#outputFileName = "out.spd"
# with open(inputFileName, 'r', newline='') as fileIn:
  
#   inputText = fileIn.read()
#   values = inputText.split(" ")

#   with open(outputFileName, 'w', newline='') as fileOut:
		
#     j = 0
	
#     for i in range(startWave, endWave, stepWave ):
#       fileOut.write(str(i))
#       fileOut.write(":")
#       val = values[j]
#       j += 1 
#       fileOut.write(str(val))
#       if(i < endWave-stepWave):
#         fileOut.write(", ")


filenamesIn = ["temp_dark_skin","temp_light_skin","temp_blue_sky","temp_foliage","temp_blue_flower","temp_bluish_green","temp_orange","temp_purplish_blue","temp_moderate_red","temp_purple","temp_yellow_green","temp_orange_yellow","temp_blue","temp_green","temp_red","temp_yellow","temp_magenta","temp_cyan","temp_white_9.5","temp_neutral_8","temp_neutral_6.5","temp_neutral_5","temp_neutral_3.5","temp_black_2"]
filenamesOut = ["dark_skin", "light_skin", "blue_sky", "foliage", "blue_flower", "bluish_green", "orange", "purplish_blue", "moderate_red", "purple", "yellow_green", "orange_yellow", "blue", "green", "red", "yellow", "magenta", "cyan", "white_9.5", "neutral_8", "neutral_6.5", "neutral_5", "neutral_3.5", "black_2"]

k = 0

for i in filenamesIn:
  
  inputFileName = i + ".spd"
  outputFileName = filenamesOut[k] + ".spd"
  k += 1

  with open(inputFileName, 'r', newline='') as fileIn:
  
    inputText = fileIn.read()
    values = inputText.split(" ")

    with open(outputFileName, 'w', newline='') as fileOut:
      
      j = 0
    
      for w in range(startWave, endWave, stepWave ):
        fileOut.write(str(w))
        fileOut.write(":")
        val = values[j]
        j += 1 
        fileOut.write(str(val))
        if(w < endWave-stepWave):
          fileOut.write(", ")
