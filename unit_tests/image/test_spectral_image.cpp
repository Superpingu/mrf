
/**
 * Author: Romain Pacanowski @ inria DOT fr
 * Copyright INRIA : 2022
 **/

#include <mrf_core/image/png_color_image.hpp>
#include <mrf_core/image/exr_color_image.hpp>
#include <mrf_core/image/spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/radiometry/spectrum.hpp>


#include <cassert>
#include <cstdlib>
#include <string>

//----
std::vector<mrf::image::ColorImage *> loadPNGImages(char **argv, size_t NB_IMAGES)
{
  std::vector<mrf::image::ColorImage *> png_images;

  for (size_t i = 0; i < NB_IMAGES; i++)
  {
    std::unique_ptr<mrf::image::ColorImage> curr_img_png = mrf::image::loadColorImage(argv[i + 1]);

    png_images.push_back(curr_img_png.release());
  }

  return png_images;
}

void testAddValuesFromColorImage(mrf::image::UniformSpectralImage *const spec_image)
{
  using namespace std;
  using namespace mrf::image;

  EXRColorImage an_exr_color_image(spec_image->width(), spec_image->height());


  std::vector<mrf::data_struct::Array2D> const &spec_image_data = spec_image->data();


  for (size_t x = 0; x < spec_image->width(); x++)
  {
    for (size_t y = 0; y < spec_image->height(); y++)
    {
      float red   = spec_image_data[0](y, x);
      float green = spec_image_data[1](y, x);
      float blue  = spec_image_data[2](y, x);


      an_exr_color_image.setPixel(x, y, mrf::color::Color(red, green, blue));
    }
  }

  an_exr_color_image.save("spect-test-exrcolor-save.exr");


  mrf::image::UniformSpectralImage *a_spectral_image
      = new EXRSpectralImage(spec_image->width(), spec_image->height(), 400, 420, 3);


  std::tuple<mrf::uint, mrf::uint, mrf::uint> wavelengths = std::tuple<mrf::uint, mrf::uint, mrf::uint>(400, 410, 420);
  a_spectral_image->addValuesFromColorImage(an_exr_color_image, wavelengths);

  std::cout << " a_spectral_image->wavelengths.size() = " << a_spectral_image->wavelengths().size() << std::endl;


  // Now construct a spectral images from the original spectral images spec_image BUT with only the first three wavelengths
  mrf::image::UniformSpectralImage *verify_image
      = new EXRSpectralImage(spec_image->width(), spec_image->height(), 400, 420, 3);

  verify_image->addValuesFromBuffer(spec_image_data[0].data(), spec_image->width(), spec_image->height(), 400);
  verify_image->addValuesFromBuffer(spec_image_data[1].data(), spec_image->width(), spec_image->height(), 410);
  verify_image->addValuesFromBuffer(spec_image_data[2].data(), spec_image->width(), spec_image->height(), 420);


  // Now verify_image and a_spectral_image should be the sa,e
  assert(*verify_image == *a_spectral_image);


  //--
  // Another test from a colored image filename we just created.
  mrf::image::UniformSpectralImage *another_spectral_image
      = new EXRSpectralImage(spec_image->width(), spec_image->height(), 400, 420, 3);
  another_spectral_image->addValuesFromColorImage("spect-test-exrcolor-save.exr", wavelengths, true);

  assert(*another_spectral_image == *a_spectral_image);

  // Same test as before but should fail with a non-existent filename
  mrf::image::UniformSpectralImage *another_spectral_image2
      = new EXRSpectralImage(spec_image->width(), spec_image->height(), 400, 420, 3);
  assert(
      another_spectral_image2->addValuesFromColorImage("spect-test-exrcolor-FOO-FOO.exr", wavelengths, true)
      != MRF_NO_ERROR);
}



void testSpectralDownsampling(mrf::image::UniformSpectralImage *const spec_image)
{
  mrf::image::EXRSpectralImage a_cpy_image(*spec_image);

  a_cpy_image.spectralDownsampling(2);

  assert(a_cpy_image.wavelengths().size() == (spec_image->wavelengths().size() / 2));
}


void testSaveAsPngGrayImages(mrf::image::UniformSpectralImage *const spec_image)
{
  //  assert( spec_image->saveAsGrayPngImages("save_test_folder") == mrf::image::MRF_NO_ERROR );

  // Why is this method returning an error related to disk write ...?
  std::cout << spec_image->saveAsGrayPngImages("./save_test_folder") << std::endl;

  //Improve this test by loading all Png images and compare the reconstructed spectra image
  // with the spec_image
}

void testSavePixelToFile(mrf::image::UniformSpectralImage *const spec_image)
{
  using namespace mrf::radiometry;

  assert(spec_image->savePixelToFile(10, 15, "spec-test-pixel-export.spd") == mrf::image::MRF_NO_ERROR);

  //Now load the spd file and compare its Spectrum with spec_image->getPixel(10,15)
  std::vector<float> initial_spectrum = spec_image->getPixel(10, 15);
  std::cout << initial_spectrum[0] << " " << initial_spectrum[1] << " " << initial_spectrum[2] << "  "
            << initial_spectrum[3] << std::endl;

  Spectrum original_spectrum(spec_image->wavelengths(), initial_spectrum);

  Spectrum from_the_file;
  from_the_file.loadSPDFile("spec-test-pixel-export.spd");

  //RP: Pain in the ass to not have a proper way two compare two spectrum
  //TODO : add operator== in spectrum.hpp and also external static function to compare
  // two spectrums up to an epsilon

  std::cout << " from_the_file.values()[0] AND original_spectrum.values()[0] = " << from_the_file.values()[0] << ", "
            << original_spectrum.values()[0] << std::endl;
  for (size_t w = 0; w < from_the_file.wavelengths().size(); w++)
  {
    assert(mrf::math::_Math<float>::absVal(from_the_file.values()[w] - original_spectrum.values()[w]) < 1e-4);
  }

  assert(from_the_file.wavelengths() == original_spectrum.wavelengths());
}

void testSetPixel(mrf::image::UniformSpectralImage *const spec_image)
{
  mrf::image::EXRSpectralImage *a_cpy1 = new mrf::image::EXRSpectralImage(*spec_image);


  mrf::image::EXRSpectralImage *a_cpy2 = new mrf::image::EXRSpectralImage();

  a_cpy2->allocateMemory(spec_image->width(), spec_image->height(), spec_image->wavelengths());

  for (size_t x = 0; x < spec_image->width(); x++)
  {
    for (size_t y = 0; y < spec_image->height(); y++)
    {
      std::vector<float> spectrum_data = a_cpy1->getPixel(x, y);
      a_cpy2->setPixel(x, y, spectrum_data);
    }
  }

  assert(*a_cpy2 == *a_cpy1);
  assert(*a_cpy1 == *spec_image);
}


//----
int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf::image;

  // Using the provided images in the Repository but they are downsampled versions
  // of https://www.cs.columbia.edu/CAVE/databases/multispectral/real_and_fake/

  if (argc < 5)
  {
    std::cout << " This test requires 4 PNG images " << std::endl;
  }

  std::cout << " argc = " << argc << std::endl;
  int const NB_IMAGES = 4;


  try
  {
    std::vector<mrf::image::ColorImage *> png_images = loadPNGImages(argv, NB_IMAGES);
    std::cout << " png_images[0] = " << (png_images[0])->width() << std::endl;

    ColorImage *curr_img_png = png_images[0];

    //std::vector<PNGColorImage*> png_images;
    UniformSpectralImage *spec_image = nullptr;
    spec_image                       = new EXRSpectralImage(curr_img_png->width(), curr_img_png->height(), 400, 10, 0);

    assert(spec_image->width() == 0);
    assert(spec_image->height() == 0);


    // Constructing a spectral Image from  5 PNG file
    // AT the same time we are exporting the PNG file to EXR file
    for (size_t i = 0; i < NB_IMAGES; i++)
    {
      spec_image->addValuesFromGrayImage(*(png_images[i]), static_cast<mrf::uint>(400 + i * 10));

      mrf::image::save(*(png_images[i]), "spec-test" + std::to_string(i + 1) + ".exr");
    }

    std::cout << " Spec_image from PNG constructed" << std::endl;
    std::cout << " spec_image->wavelengths().size() = " << spec_image->wavelengths().size() << std::endl;

    testAddValuesFromColorImage(spec_image);
    testSpectralDownsampling(spec_image);
    testSaveAsPngGrayImages(spec_image);
    testSavePixelToFile(spec_image);
    testSetPixel(spec_image);


    UniformSpectralImage *spec_image2 = new EXRSpectralImage(*spec_image);
    assert(*spec_image2 == *spec_image);


    std::cout << __FILE__ << " " << __LINE__ << std::endl;

    spec_image2->clear();

    //Now reloading all test1.exr test2.exr etc images
    for (size_t i = 0; i < NB_IMAGES; i++)
    {
      spec_image2->addValuesFromGrayImage(
          "spec-test" + std::to_string(i + 1) + ".exr",
          static_cast<mrf::uint>(400 + i * 10));
    }

    std::cout << " spec_image2 loaded" << std::endl;

    //TESTING that both spectral images are the same
    assert(*spec_image2 == *spec_image);


    // TESTING AccumulativeData
    spec_image->generateAccumulativeData();
    spec_image2->generateAccumulativeData();

    assert(spec_image2->cumulativeData() == spec_image->cumulativeData());


    //TESTING Expected Memory occupancy
    // !!! --> mem_occ is MB not bytes
    double mem_occ       = mrf::image::UniformSpectralImage::expectedMemoryOccupancy(128, 128, 4);
    double test_in_bytes = 128 * 128 * 4 * sizeof(float) + 4 * sizeof(mrf::uint);

    std::cout << " mem_occ = " << mem_occ * 1000 * 1000 << " test in bytes = " << test_in_bytes << std::endl;

    assert((mem_occ * 1000 * 1000) == test_in_bytes);
    assert(mem_occ < spec_image2->memoryOccupancy());


    //Trying to add a non-existing Gray image
    assert(spec_image2->addValuesFromGrayImage("foo.exr", 440) != MRF_NO_ERROR);


    //Trying to add a Color Image that is not consistent with image dimensions
    //std::unique_ptr<mrf::image::ColorImage> another_png_image = mrf::image::loadColorImage();




    delete spec_image2;



    //Now saving the  first 3 wavelengthsd of the Spectral image into an EXRColor



    //---------------------------------------------------------------------------------------------------------------
    // TODO : Fix me.
    //
    // This test will fail.
    // Maybe the fact to add three times the same value is not really supported.
    // UniformSpectralImage* spec_image3 = nullptr;

    // for (size_t i = 0; i < NB_IMAGES; i++)
    // {
    //   std::unique_ptr<ColorImage> curr_img_png = loadColorImage(argv[i + 1]);

    //   if( spec_image3 == nullptr)
    //   {
    //     spec_image3 = new EXRSpectralImage(curr_img_png->width(), curr_img_png->height(), 400, 10, 0);
    //   }


    //   std::tuple<uint, uint, uint> wavelengths = std::tuple<uint,uint,uint>(400+i*10, 400+i*10, 400+i*10);
    //   if( spec_image3->addValuesFromColorImage(*curr_img_png, wavelengths ) != MRF_NO_ERROR )
    //   {
    //     assert(0);
    //   }

    //   // std::cout << " spec_image3->wavelengths().size() = " << spec_image3->wavelengths().size() << std::endl;
    //   // std::cout << __FILE__ << " " << __LINE__ << std::endl;
    // }

    // std::cout << " spec_image3->wavelengths().size() = " << spec_image3->wavelengths().size() << std::endl;
    // std::cout << " spec_image3->wavelengths()[0,1,2,3] = "
    //           << spec_image3->wavelengths()[0] << "  " << spec_image3->wavelengths()[1] << "  "
    //           << spec_image3->wavelengths()[2] << "  " << spec_image3->wavelengths()[3] << "  "
    //           << std::endl;

    // std::cout << " spec_image->wavelengths()[0,1,2,3] = "
    //           << spec_image->wavelengths()[0] << "  " << spec_image->wavelengths()[1] << "  "
    //           << spec_image->wavelengths()[2] << "  " << spec_image->wavelengths()[3] << "  "
    //           << std::endl;


    // assert( *spec_image3 == *spec_image );
    //---------------------------------------------------------------------------------------------------------------




    //--
    // Clean the allocations
    png_images.clear();
  }
  catch (std::exception const &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }


  return EXIT_SUCCESS;
}