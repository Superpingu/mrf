/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/color_image.hpp>

#include <cassert>
#include <cstdlib>
#include <string>
#include <memory>

int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf::image;

  assert(isHDRExtension("exr"));
  assert(isHDRExtension("hdr"));
  assert(argc > 1);

  try
  {
    std::unique_ptr<ColorImage> img1 = loadColorImage(argv[1]);

    mrf::image::IMAGE_LOAD_SAVE_FLAGS ret = save(*img1, "test_save_ward.hdr");
    assert(ret == MRF_NO_ERROR);
    ret = save(*img1, "test_save_exr.exr");
    assert(ret == MRF_NO_ERROR);

    ret = img1->save("test_save_exr_with_comments.exr", "TEST_MRF");
    assert(ret == MRF_NO_ERROR);

    //Load test_save_exr_with_comments.exr check that the comments is THERE
    std::unique_ptr<ColorImage>               img2              = loadColorImage("test_save_exr_with_comments.exr");
    std::map<std::string, std::string> const &metdata_from_img2 = img2->metadata();

    auto found_it = metdata_from_img2.find("mrf_additional_information");
    assert(found_it != metdata_from_img2.end());
    assert(found_it->second == "TEST_MRF");
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
