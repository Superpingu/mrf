
/**
 * Author: Romain Pacanowski @ inria DOT fr
 * Copyright INRIA : 2022
 **/

#include <mrf_core/math/vec3.hpp>
#include <mrf_core/image/png_color_image.hpp>

#include <cassert>
#include <cstdlib>
#include <string>



float const EPSILON_TEST = 1e-4;

mrf::math::Vec3f const VEC3_EPSILON_TEST(EPSILON_TEST);

mrf::color::Color const ONE(1.f);


int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf::image;

  try
  {
    //Construct Empty PNG image
    ColorImage *a_img = new PNGColorImage();

    //Fill it with one everywhere
    a_img->init(10, 10, false);

    for (size_t i = 0; i < a_img->size(); i++)
    {
      (*a_img)(i) = ONE;
    }

    // TESTING METHODS fro ColorImage.cpp about statistics
    mrf::color::Color s = ONE * a_img->size();

    assert(a_img->totalPower() == s.luminance());
    assert(a_img->maxValue() == 1.f);

    a_img->computeStatistics();
    ;


    mrf::math::Vec3f red_stats = a_img->redStatistics();
    std::cout << " red_stats = " << red_stats << std::endl;

    assert(red_stats == mrf::math::Vec3f(1.f));

    mrf::math::Vec3f green_stats = a_img->greenStatistics();
    assert(green_stats == mrf::math::Vec3f(1.f));

    mrf::math::Vec3f blue_stats = a_img->blueStatistics();
    assert(blue_stats == mrf::math::Vec3f(1.f));


    mrf::math::Vec3f lumi_stats = a_img->lumStatistics();
    assert(lumi_stats == ONE.luminance());




    // TESTING RAW ACCESS
    mrf::color::Color *raw_data1 = a_img->ptr();

    assert(*raw_data1 == ONE);

    const mrf::color::Color *raw_data2 = a_img->ptr();

    assert(*raw_data1 == *raw_data2);

    //TESTING operator *=
    ColorImage *img2 = new PNGColorImage(*a_img);
    (*img2) *= 10.f;


    //TESTING EQUALITY (operator==) on a FAILURE because of value
    bool img1_is_diff_img2 = (*img2 == *a_img);
    assert(!img1_is_diff_img2);

    //TESTING EQUALITY (operator==) on a FAILURE because of image dimensions
    ColorImage *img3 = new PNGColorImage(5, 5);
    assert(!(*img3 == *a_img));



    const ColorImage *       a_cte_img     = a_img;
    const mrf::color::Color &testing_color = (*a_cte_img)(2, 4);
    assert(testing_color == ONE);

    const mrf::color::Color *raw_from_cte_img = a_cte_img->ptr();
    assert(*raw_data1 == *raw_from_cte_img);

    //TESTING Pixels accessors and setters
    assert(a_img->getPixel(4, 3) == ONE);

    ColorImage &r_a_img = *a_img;

    assert(r_a_img.getPixel(4, 3) == r_a_img(4, 3));

    r_a_img.setPixel(4, 3, 0.5f);
    mrf::color::Color a_color = ONE;
    std::cout << " a_color = " << a_color << std::endl;
    a_color = 0.5f;
    std::cout << " a_color = " << a_color << std::endl;

    std::cout << " r_a_img.getPixel( 4, 3) = " << r_a_img.getPixel(4, 3) << std::endl;

    assert(r_a_img.getPixel(4, 3) == r_a_img(4, 3));


    const mrf::color::Color &a_color_4_3 = r_a_img(4, 3);

    assert(a_color_4_3 == r_a_img.getPixel(4, 3));

    r_a_img.setPixel(4, 0.5f);

    assert(r_a_img.getPixel(4) == r_a_img[4]);



    a_img->init(10, 10, true);



    delete img2;
    delete a_img;
  }
  catch (std::exception const &e)
  {
    std::cout << "In Test file test_color_image.cpp exception :  " << e.what() << std::endl;
    return EXIT_FAILURE;
  }




  return EXIT_SUCCESS;
}