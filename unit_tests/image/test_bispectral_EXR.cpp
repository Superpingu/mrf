/**
 * Author: Romain Pacanowski @ inria DOT fr
 * Copyright INRIA : 2022
 **/

#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/exr_spectral_image.hpp>
#include <mrf_core/image/polarised_spectral_image.hpp>
#include <mrf_core/image/image_diff.hpp>

#include <cassert>
#include <cstdlib>
#include <string>


float const EPSILON_TEST = 1e-3;


void myprogress_function_test(int progress)
{
  std::cout << "[TEST] Progress is : " << progress << " ";
}


int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf::image;

  try
  {
    assert(isSpectralExtension("exr"));

    assert(isColorExtension("exr"));

    assert(isSpectralEXR(argv[1]));

    assert(isSpectralImage(argv[1]));


    EXRSpectralImage img1(argv[1]);

    assert(img1.bispectral());

    std::string additional_comments("A FOO BISPECTRAL ADDITIONAL COMMENTS");
    img1.save("test-saving-bispectral.exr", additional_comments);
  }
  catch (...)
  {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}