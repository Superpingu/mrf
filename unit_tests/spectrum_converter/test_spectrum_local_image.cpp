/**
 * Author: Alban Fichet @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <cassert>
#include <cstdlib>
#include <string>
#include <vector>

#include <iostream>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/math/vec3.hpp>
#include <mrf_core/radiometry/d65.hpp>
#include <mrf_core/radiometry/spectrum.hpp>

int main(int argc, char **argv)
{
  if (argc != 2)
  {
    std::cout << "This tests requires an input image " << std::endl;
    return EXIT_FAILURE;
  }

  std::unique_ptr<mrf::image::UniformSpectralImage> spectral_image;

  try
  {
    std::string const filename(argv[1]);
    std::cout << "[INFO] filename = " << filename << std::endl;

    spectral_image = mrf::image::loadUniformSpectralImage(filename);
  }
  catch (const mrf::image::IMAGE_LOAD_SAVE_FLAGS &f)
  {
    std::cerr << "Error code: " << f << std::endl;
    return EXIT_FAILURE;
  }

  const std::vector<mrf::uint>               spectrumWavelengths = spectral_image->wavelengths();
  std::vector<mrf::color::SpectrumConverter> converter;
  converter.push_back(mrf::color::SpectrumConverter(mrf::color::CIE_1931_2DEG));
  converter.push_back(mrf::color::SpectrumConverter(mrf::color::CIE_XYZ_1964_10DEG));

  const float *   illu_spd       = mrf::radiometry::D_65_SPD;
  const mrf::uint illu_first_wl  = mrf::radiometry::D_65_FIRST_WAVELENGTH;
  const mrf::uint illu_array_sz  = mrf::radiometry::D_65_ARRAY_SIZE;
  const mrf::uint illu_precision = mrf::radiometry::D_65_PRECISION;

  for (const auto &sc : converter)
  {
    // --------------------------------
    // Gray
    // --------------------------------
    mrf::data_struct::Array2D grayImage;
    mrf::color::SpectrumConverter::spectralImageToGrayImage(
        *spectral_image,
        grayImage,
        spectrumWavelengths.front(),
        spectrumWavelengths.back());

    // --------------------------------
    // Emissive
    // --------------------------------

    // We use conversion the method which output to Array2D
    mrf::data_struct::Array2D xEmitImage, yEmitImage, zEmitImage;
    sc.emissiveSpectralImageToXYZImage(*spectral_image, xEmitImage, yEmitImage, zEmitImage);

    // We use conversion the method which output to an image. We just need an handle to color image
    std::unique_ptr<mrf::image::ColorImage> xyzEmitImage = mrf::image::getColorImageForSave("foo.exr");
    sc.emissiveSpectralImageToXYZImage(*spectral_image, *xyzEmitImage);

    // --------------------------------
    // Reflective
    // --------------------------------

    // We use conversion the method which output to Array2D
    mrf::data_struct::Array2D xReflImage, yReflImage, zReflImage;
    sc.reflectiveSpectralImageToXYZImage(
        *spectral_image,
        illu_spd,
        illu_first_wl,
        illu_array_sz,
        illu_precision,
        xReflImage,
        yReflImage,
        zReflImage);

    // We use conversion the method which output to an image. We just need an handle to color image
    std::unique_ptr<mrf::image::ColorImage> xyzReflImage = mrf::image::getColorImageForSave("foo.exr");
    sc.reflectiveSpectralImageToXYZImage(
        *spectral_image,
        illu_spd,
        illu_first_wl,
        illu_array_sz,
        illu_precision,
        *xyzReflImage);

    const float tol = 1e-4f;

    // Now, we compare constistency
    for (size_t y = 0; y < spectral_image->height(); y++)
    {
      for (size_t x = 0; x < spectral_image->width(); x++)
      {
        // Conversion of the spectrum at the given pixel
        const mrf::radiometry::Spectrum pixelSpectrum(spectral_image->wavelengths(), spectral_image->getPixel(x, y));

        // --------------------------------
        // Gray
        // --------------------------------

        const float grayValue = mrf::color::SpectrumConverter::spectrumIntegral(pixelSpectrum);
        assert(mrf::math::_Math<float>::absVal(grayImage(y, x) - grayValue) < tol);

        // --------------------------------
        // Emissive
        // --------------------------------

        const mrf::color::Color xyzEmitValue = sc.emissiveSpectrumToXYZ(pixelSpectrum);

        assert(mrf::math::_Math<float>::absVal(xEmitImage(y, x) - xyzEmitValue.x()) < tol);
        assert(mrf::math::_Math<float>::absVal(yEmitImage(y, x) - xyzEmitValue.y()) < tol);
        assert(mrf::math::_Math<float>::absVal(zEmitImage(y, x) - xyzEmitValue.z()) < tol);

        assert((xyzEmitImage->getPixel(x, y) - xyzEmitValue).length() < tol);


        // --------------------------------
        // Reflective
        // --------------------------------

        const mrf::color::Color xyzReflValue
            = sc.reflectiveSpectrumToXYZ(pixelSpectrum, illu_spd, illu_first_wl, illu_array_sz, illu_precision);

        assert(mrf::math::_Math<float>::absVal(xReflImage(y, x) - xyzReflValue.x()) < tol);
        assert(mrf::math::_Math<float>::absVal(yReflImage(y, x) - xyzReflValue.y()) < tol);
        assert(mrf::math::_Math<float>::absVal(zReflImage(y, x) - xyzReflValue.z()) < tol);

        assert((xyzReflImage->getPixel(x, y) - xyzReflValue).length() < tol);
      }
    }
  }

  return EXIT_SUCCESS;
}
