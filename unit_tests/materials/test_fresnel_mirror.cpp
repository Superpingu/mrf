/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::FresnelMirror a_mat("a_fresnel_mirror");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::FRESNEL_MIRROR);
  assert(a_mat.name() == "a_fresnel_mirror");


  //Test setter/getter
  assert(a_mat.eta() == mrf::materials::COLOR(0.f));
  assert(a_mat.kappa() == mrf::materials::COLOR(0.f));

  mrf::materials::COLOR eta(1.5f);
  mrf::materials::COLOR kappa(3.5f);

  a_mat.setEta(eta);
  assert(a_mat.eta() == eta);

  a_mat.setKappa(kappa);
  assert(a_mat.kappa() == kappa);

  mrf::materials::COLOR eta2(1.5f);
  mrf::materials::COLOR kappa2(3.5f);
  a_mat.setFresnel(eta2, kappa2);
  assert(a_mat.eta() == eta2);
  assert(a_mat.kappa() == kappa2);

  //Test other constructors
  mrf::materials::FresnelMirror a_mat2("a_fresnel_mirror", eta, kappa);
  assert(a_mat2.getType() == mrf::materials::BRDFTypes::FRESNEL_MIRROR);
  assert(a_mat2.name() == "a_fresnel_mirror");
  assert(a_mat2.eta() == eta);
  assert(a_mat2.kappa() == kappa);

  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
