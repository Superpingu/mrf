/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 *
 * Author:  Romain Pacanowski @ inria DOT fr
 * Copyright INRIA : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/materials.hpp>
#include <mrf_core/math/math.hpp>

#include <mrf_plugins/register_mrf_plugins.hpp>


int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::Lambert a_mat("a_default_lambert");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::LAMBERT);
  assert(a_mat.name() == "a_default_lambert");

  //Test setter/getter
  assert(a_mat.diffuseAlbedo() == 1.f);

  float albedo = 0.5f;
  a_mat.setReflectivity(albedo);
  assert(a_mat.diffuseAlbedo() == albedo);

  //Only rgb mode sets a color by default, test it only in this case.
#ifndef MRF_RENDERING_MODE_SPECTRAL
  assert(a_mat.getColor() == mrf::materials::COLOR(0.8f));
#endif

  mrf::materials::COLOR a_color(0.5f);
  a_mat.setColor(a_color);
  assert(a_mat.getColor() == a_color);

  //Test other constructors
  mrf::materials::Lambert a_mat2("a_default_lambert", a_color);
  assert(a_mat2.getType() == mrf::materials::BRDFTypes::LAMBERT);
  assert(a_mat2.name() == "a_default_lambert");
  assert(a_mat2.getColor() == a_color);
  assert(a_mat2.diffuseAlbedo() == 1.f);

  mrf::materials::Lambert a_mat3("a_default_lambert", a_color, albedo);
  assert(a_mat3.getType() == mrf::materials::BRDFTypes::LAMBERT);
  assert(a_mat3.name() == "a_default_lambert");
  assert(a_mat3.getColor() == a_color);
  assert(a_mat3.diffuseAlbedo() == albedo);


#ifndef MRF_RENDERING_MODE_SPECTRAL
  mrf::materials::Lambert a_mat4("a_default_lambert", 1.f, 0.5f, 0.8f, albedo);
  assert(a_mat4.getType() == mrf::materials::BRDFTypes::LAMBERT);
  assert(a_mat4.name() == "a_default_lambert");
  assert(a_mat4.getColor() == mrf::materials::COLOR(1.f, 0.5f, 0.8f));
  assert(a_mat4.diffuseAlbedo() == albedo);
#endif

  //More tests on  different accessors (CPU backend)

  mrf::geom::LocalFrame lf;
  assert(a_mat3.diffuseComponent());
  assert(!a_mat3.specularComponent());
  assert(!a_mat3.transmissionComponent());
  assert(!a_mat3.nonDiffuseComponent());

  assert(a_mat3.diffuseAlbedo(lf) == albedo);

  assert(a_mat3.specularAlbedo() == 0.0f);
  assert(a_mat3.specularAlbedo(lf) == 0.0f);
  assert(a_mat3.nonDiffuseAlbedo() == 0.0f);

  assert(a_mat3.totalAlbedo() == albedo);


  assert(a_mat3.transmissionAlbedo(lf) == 0.0f);
  assert(a_mat3.transmissionAlbedo() == 0.0f);


  assert(a_mat3.totalAlbedo() == (a_mat3.diffuseAlbedo() + a_mat3.specularAlbedo() + a_mat3.transmissionAlbedo()));


  //-----
  // Now testing Diffuse PDF
  // The Lambert Material implements a cosine sampling
  //-----

  // Retro-reflection Test
  mrf::math::Vec3f in(0.0f, 0.0f, -1.0f);
  mrf::math::Vec3f out(0.0f, 0.0f, 1.0f);

  lf.normal(out);

  float const diffuse_pdf = a_mat3.diffPDF(lf, in, out);

  std::cout << " diffuse+pdf = " << diffuse_pdf << "  1.f / static_cast<float>(M_PI) "
            << 1.f / static_cast<float>(mrf::math::Math::PI) << std::endl;
  assert(diffuse_pdf == (1.f / static_cast<float>(mrf::math::Math::PI)));

  //Grazing angle should not be sampled , hence PDF is zero
  mrf::math::Vec3f in_grazing(1.f, 0.f, 0.f);
  assert(a_mat3.diffPDF(lf, in_grazing, out) == 0.0f);

  //-----
  // Now testing Specular, Transparent and Non-diffuse PDF
  //-----
  float const spec_pdf = a_mat3.specPDF(lf, in, out);
  assert(spec_pdf == 0.0f);

  float const trans_pdf = a_mat3.transPDF(lf, in, out);
  assert(trans_pdf == 0.0f);

  float const non_diff_pdf = a_mat3.nonDiffusePDF(lf, in, out);
  assert(non_diff_pdf == 0.0f);

  //-----
  // Testing BRDF Values
  //-----
  float const brdf_value_mat3 = a_mat3.brdfValue(lf, in, out);
  assert(brdf_value_mat3 == (a_mat3.diffuseAlbedo() / static_cast<float>(mrf::math::Math::PI)));

  mrf::materials::RADIANCE_TYPE radiance_brdf_value_mat3 = a_mat3.coloredBRDFValue(lf, in, out);

  assert(
      radiance_brdf_value_mat3
      == (a_mat3.getColor() * a_mat3.diffuseAlbedo() / static_cast<float>(mrf::math::Math::PI)));


  //-----
  // Testing Scattering Direction and BRDF Values
  //-----

  // Note that we pass hard-coded values for random variables

  mrf::math::Vec3f                          outgoing_dir;
  float                                     proba_outgoing_dir;
  mrf::materials::ScatterEvent::SCATTER_EVT event
      = a_mat3.scatteringDirection(lf, in, outgoing_dir, proba_outgoing_dir, 0.f, 1.f);

  std::cout << " outgoing_dir  = " << outgoing_dir << "  proba=  = " << proba_outgoing_dir << std::endl;
  //--
  assert(event == mrf::materials::ScatterEvent::DIFFUSE_REFL);
  assert(outgoing_dir.dot(lf.normal()) > 0.0f);


  //--
  event = a_mat3.scatteringDirection(lf, in, outgoing_dir, proba_outgoing_dir, 1.f, 0.f);
  assert(event == mrf::materials::ScatterEvent::ABSORPTION);


  //--
  mrf::materials::RADIANCE_TYPE brdf_corrected_by_pdf;

  event = a_mat3.scatteringDirectionWithBRDF(lf, in, 0.f, 1.f, outgoing_dir, brdf_corrected_by_pdf);

  assert(event == mrf::materials::ScatterEvent::DIFFUSE_REFL);
  assert(outgoing_dir.dot(lf.normal()) > 0.0f);
  //TODO test more precisely the tougoing direction since we have e1=0.f, and e2=1.f

  //--
  event = a_mat3.scatteringDirectionWithBRDF(lf, in, 1.f, 0.f, outgoing_dir, brdf_corrected_by_pdf);
  assert(event == mrf::materials::ScatterEvent::ABSORPTION);

  //--
  mrf::materials::RADIANCE_TYPE incident_radiance(1.f);

  // NOT Sure we are going to keep this function
  event = a_mat3.generateScatteringDirection(lf, in, incident_radiance, outgoing_dir);

  assert((event == mrf::materials::ScatterEvent::ABSORPTION) || (event == mrf::materials::ScatterEvent::DIFFUSE_REFL));

  if (event == mrf::materials::ScatterEvent::DIFFUSE_REFL)
  {
    assert(outgoing_dir.dot(lf.normal()) > 0.0f);
  }


  return EXIT_SUCCESS;
}
