/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::PerfectMirror a_mat("a_perfect_mirror");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::PERFECT_MIRROR);
  assert(a_mat.name() == "a_perfect_mirror");

  //Test setter/getter

  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
