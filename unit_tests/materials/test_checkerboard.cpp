/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::CheckerBoard a_mat("a_default_checker");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::CHECKERBOARD);
  assert(a_mat.name() == "a_default_checker");

  //Test setter/getter
  assert(a_mat.getColor1() == mrf::materials::RADIANCE_TYPE(1.0));
  assert(a_mat.getColor2() == mrf::materials::RADIANCE_TYPE(0.0));
  assert(a_mat.getRepetition() == 1);

  mrf::materials::RADIANCE_TYPE color1 = mrf::materials::RADIANCE_TYPE(0.8f);
  mrf::materials::RADIANCE_TYPE color2 = mrf::materials::RADIANCE_TYPE(0.3f);

  a_mat.setColor1(color1);
  assert(a_mat.getColor1() == color1);
  a_mat.setColor2(color2);
  assert(a_mat.getColor2() == color2);

  unsigned int repetition = 2;
  a_mat.setRepetition(repetition);
  assert(a_mat.getRepetition() == repetition);

  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
