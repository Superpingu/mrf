/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2021
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::FresnelGlass a_mat("a_fresnel_glass");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::FRESNEL_GLASS);
  assert(a_mat.name() == "a_fresnel_glass");

  //Test setter/getter
  assert(a_mat.eta() == mrf::materials::COLOR(0.f));
  assert(a_mat.kappa() == mrf::materials::COLOR(0.f));
  assert(a_mat.extinction() == mrf::materials::COLOR(0.f));
  assert(a_mat.thinGlass() == false);

  mrf::materials::COLOR eta(1.5f);
  mrf::materials::COLOR kappa(3.5f);
  mrf::materials::COLOR extinction(0.1f);

  a_mat.setEta(eta);
  assert(a_mat.eta() == eta);

  a_mat.setKappa(kappa);
  assert(a_mat.kappa() == kappa);

  a_mat.setExtinction(extinction);
  assert(a_mat.extinction() == extinction);

  mrf::materials::RADIANCE_TYPE eta2(1.5f);
  mrf::materials::RADIANCE_TYPE kappa2(3.5f);
  a_mat.setFresnel(eta2, kappa2);
  assert(a_mat.eta() == eta2);
  assert(a_mat.kappa() == kappa2);

  a_mat.defineAsThin();
  assert(a_mat.thinGlass() == true);

  a_mat.defineAsThick();
  assert(a_mat.thinGlass() == false);

  //Test other constructors
  mrf::materials::FresnelGlass a_mat2("a_fresnel_glass", eta, kappa, extinction);
  assert(a_mat2.getType() == mrf::materials::BRDFTypes::FRESNEL_GLASS);
  assert(a_mat2.name() == "a_fresnel_glass");
  assert(a_mat2.eta() == eta);
  assert(a_mat2.kappa() == kappa);
  assert(a_mat2.extinction() == extinction);
  assert(a_mat2.thinGlass() == false);

  mrf::materials::FresnelGlass a_mat3("a_fresnel_glass", eta2, kappa2, extinction, true);
  assert(a_mat3.getType() == mrf::materials::BRDFTypes::FRESNEL_GLASS);
  assert(a_mat3.name() == "a_fresnel_glass");
  assert(a_mat3.eta() == eta2);
  assert(a_mat3.kappa() == kappa2);
  assert(a_mat3.extinction() == extinction);
  assert(a_mat3.thinGlass() == true);


  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
