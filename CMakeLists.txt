option(BUILD_MALIA "Build malia renderer" ON)
option(ENABLE_TEST "Enable tests" OFF)
option(BUILD_SHARED_LIBS "Install as DLL" OFF)
if(WIN32 AND BUILD_MALIA)
  option(BUILD_INSTALLER "Create an installer (for deploy only)" OFF)
endif()

if(BUILD_INSTALLER AND BUILD_SHARED_LIBS)
  set(GLFW_INSTALL ON)
endif()

cmake_policy(SET CMP0048 NEW)
cmake_policy(SET CMP0015 NEW)
cmake_policy(SET CMP0060 NEW)

cmake_minimum_required(VERSION 3.9)

project(MRF 
  VERSION 1.0.3
  LANGUAGES CXX C #CUDA
)

include(CheckCXXCompilerFlag)
include(GNUInstallDirs)

if(APPLE)
  set(CMAKE_INSTALL_RPATH "@loader_path/../lib;@loader_path")
elseif(UNIX)
  set(CMAKE_INSTALL_RPATH "$ORIGIN/../lib:$ORIGIN/")
endif()

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# Default build type
if((NOT CMAKE_BUILD_TYPE) AND (NOT ENABLE_TEST))
  set(CMAKE_BUILD_TYPE "Release")
elseif(ENABLE_TEST)
  set(CMAKE_BUILD_TYPE "Debug")
  
  # Configuration for coverage flags
  if(UNIX)
    set(CMAKE_CXX_FLAGS
        ${CMAKE_CXX_FLAGS}
        "-fprofile-arcs -ftest-coverage")
    set(CMAKE_C_FLAGS
        ${CMAKE_C_FLAGS}
        "-fprofile-arcs -ftest-coverage")
    set(CMAKE_EXE_LINKER_FLAGS
        ${CMAKE_EXE_LINKER_FLAGS}
        "-fprofile-arcs -ftest-coverage")
    set(CMAKE_SHARED_LINKER_FLAGS
        ${CMAKE_SHARED_LINKER_FLAGS}
        "-fprofile-arcs -ftest-coverage")
  endif()  
  enable_testing()
  list(APPEND CMAKE_CTEST_ARGUMENTS "--output-on-failure")
endif()

if(NOT BUILD_SHARED_LIBS)
  add_definitions(-DMRF_STATIC)
endif()


set(CMAKE_POSITION_INDEPENDENT_CODE ON)

message("-- System Processor ${CMAKE_SYSTEM_PROCESSOR} ")
message("-- Compiler ${CMAKE_CXX_COMPILER_ID}")
message("-- Building in " ${CMAKE_BUILD_TYPE} " Mode ")

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# ----------------------------------------------------------------------------
# RPATH STUFF
# ----------------------------------------------------------------------------

# Note from RP:
# Comment/uncomment the following lines if your want a different behavior 
# regarding RPATH
# See here for more details: 
# https://gitlab.kitware.com/cmake/community/wikis/doc/cmake/RPATH-handling

# By default, on Linux,
# 1) when building the application we want to resolve the link  to the 
# different shared libaries (i.e., absolute links will be used)
# 2) when installing the application the link will not be resolved, external
# shared libraries must be use i.e. don't skip the full RPATH for the build 
# tree
set(CMAKE_SKIP_BUILD_RPATH  FALSE)

# When building, don't use the install RPATH already
# (but later on when installing)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

# Include the current directory of the executable to RPATH. (this is always 
# the case for Windows)
set(CMAKE_INSTALL_RPATH "\$ORIGIN")

# Add the automatically determined parts of the RPATH which point to 
# directories outside the build tree to the install RPATH
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH FALSE)

# the RPATH to be used when installing, but only if it's not a system directory
#LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
#IF("${isSystemDir}" STREQUAL "-1")
#SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
#ENDIF("${isSystemDir}" STREQUAL "-1")

# ----------------------------------------------------------------------------
# MRF library
# ----------------------------------------------------------------------------

add_subdirectory(libmrf)

# ----------------------------------------------------------------------------
# Applications
# ----------------------------------------------------------------------------

find_package(Eigen3)
# This is hard defined
message("-- MRF with Eigen ${EIGEN3_FOUND}")

if(ENABLE_TEST AND EIGEN3_FOUND)
  message("-- Test mode enabled, Eigen3 found. We add an extra build w/o Eigen3")
endif()

if(Eigen3_FOUND)
  add_subdirectory(apps/rgb2spec)
endif()

if(BUILD_MALIA) 
  add_subdirectory(apps/malia)
endif()

add_subdirectory(apps/mdiff)
add_subdirectory(apps/snr)
add_subdirectory(apps/mic)
# add_subdirectory(apps/thinfilm)

if(Eigen3_FOUND AND BUILD_MALIA)
  add_subdirectory(blender_bridge)
endif()

# ----------------------------------------------------------------------------
# Tests
# ----------------------------------------------------------------------------

if (ENABLE_TEST)
  add_subdirectory(unit_tests)
	# Util module Tests
endif()

# ----------------------------------------------------------------------------
# CLANG FORMAT
# ----------------------------------------------------------------------------

if(CMAKE_VERSION VERSION_GREATER "3.6")
  file(
    GLOB_RECURSE
    FORMAT_SOURCE
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/*.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/*.cxx
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/*.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/*.inl
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/backends/optix/*.cu
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/backends/optix/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/mrf_plugins/*.cu
    ${CMAKE_CURRENT_SOURCE_DIR}/libmrf/mrf_plugins/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.cxx
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.inl
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.glsl
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.frag
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.comp
    ${CMAKE_CURRENT_SOURCE_DIR}/apps/*.vert
    ${CMAKE_CURRENT_SOURCE_DIR}/unit_tests/*.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/unit_tests/*.cxx
    ${CMAKE_CURRENT_SOURCE_DIR}/unit_tests/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/unit_tests/*.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/unit_tests/*.inl
    ${CMAKE_CURRENT_SOURCE_DIR}/unit_tests/*.h)

  list(FILTER FORMAT_SOURCE EXCLUDE REGEX ".*ext.*$")
  list(FILTER FORMAT_SOURCE EXCLUDE REGEX ".*sobol.cpp$")

  if(CMAKE_HOST_UNIX)
    add_custom_target(clang-format)

    foreach(_file ${FORMAT_SOURCE})
      add_custom_command(
        TARGET clang-format
        COMMENT "Formating: ${_file}"
        COMMAND "sed" -i "s/#pragma omp/\\/\\/#pragma omp/g" ${_file}
        COMMAND "clang-format" -i -style=file ${_file}
        COMMAND "sed" -i "s/\\/\\/ *#pragma omp/#pragma omp/g" ${_file}
        VERBATIM DEPENDS ${_file})
    endforeach()
  elseif(CMAKE_HOST_WIN32)
    add_custom_target(clang-format)
    foreach(_file ${FORMAT_SOURCE})
      add_custom_command(
        TARGET clang-format
        COMMENT "Formating: ${_file}"
        COMMAND "wsl" "sed" -i "s/#pragma omp/\\/\\/#pragma omp/g" ${_file}
        COMMAND "wsl" "clang-format" -i -style=file ${_file}
        COMMAND "wsl" "sed" -i "s/\\/\\/ *#pragma omp/#pragma omp/g" ${_file}
        VERBATIM DEPENDS ${_file})
    endforeach()
  endif()
endif()

# ----------------------------------------------------------------------------
# Package options
# ----------------------------------------------------------------------------

if(BUILD_MALIA)
  if(WIN32 AND BUILD_INSTALLER)
    set(CPACK_NSIS_PACKAGE_NAME "Malia")
    set(CPACK_NSIS_DISPLAY_NAME "Malia")
    
    set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)
    set(CPACK_NSIS_MODIFY_PATH ON)
    
    set(CPACK_NSIS_MUI_ICON "${CMAKE_CURRENT_SOURCE_DIR}/deploy/malia.ico")
    set(CPACK_NSIS_MUI_FINISHPAGE_RUN "bin\\\\malia.exe")
    set(CPACK_NSIS_MUI_UNIICON "${CPACK_NSIS_MUI_ICON}")
    set(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\malia.exe")
    
    set(CPACK_NSIS_HELP_LINK "https://gitlab.com/mrf-devteam/mrf")
    set(CPACK_NSIS_URL_INFO_ABOUT "${CPACK_NSIS_HELP_LINK}")
    set(CPACK_NSIS_EXECUTABLES_DIRECTORY "${CMAKE_INSTALL_BINDIR}")
    set(CPACK_NSIS_MENU_LINKS 
        "bin\\\\malia.exe" "Malia"
        "bin\\\\malia_rgb.exe" "Malia RGB"
        )
        
    set(CPACK_COMPONENTS_ALL Libraries MRF Malia mdiff mic rgbToSpec)
    
    set(CPACK_COMPONENT_LIBRARIES_DISABLED)
    set(CPACK_COMPONENT_LIBRARIES_DISPLAY_NAME "System libraries")
    set(CPACK_COMPONENT_LIBRARIES_DESCRIPTION
      "System dynamic libraries required for runtime. REQUIRED If you do not have Microsoft Visual C++ 2015 installed on your computer."
      )

    set(CPACK_COMPONENT_MRF_DISPLAY_NAME "MRF")
    set(CPACK_COMPONENT_MRF_REQUIRED)
    set(CPACK_COMPONENT_MRF_HIDDEN)
    set(CPACK_COMPONENT_MRF_DESCRIPTION "Malia Rendering Framework core libraries.")
    
    set(CPACK_COMPONENT_MALIA_GROUP "Applications")
    set(CPACK_COMPONENT_MALIA_DISPLAY_NAME "Malia Renderer")
    set(CPACK_COMPONENT_MALIA_DEPENDS "MRF")    
    
    set(CPACK_COMPONENT_MDIFF_GROUP "Applications")
    set(CPACK_COMPONENT_MDIFF_DISPLAY_NAME "mdiff")
    set(CPACK_COMPONENT_MDIFF_DEPENDS "MRF")
    set(CPACK_COMPONENT_MDIFF_DISABLED)
    set(CPACK_COMPONENT_MDIFF_DESCRIPTION "Command-line tool to compare images (spectral or RGB).")
    
    set(CPACK_COMPONENT_MIC_GROUP "Applications")
    set(CPACK_COMPONENT_MIC_DISPLAY_NAME "mic")
    set(CPACK_COMPONENT_MIC_DEPENDS "MRF")
    set(CPACK_COMPONENT_MIC_DISABLED)
    set(CPACK_COMPONENT_MIC_DESCRIPTION "Command-line tool to convert images (spectral -> RGB).")
    
    set(CPACK_COMPONENT_RGBTOSPEC_GROUP "Applications")
    set(CPACK_COMPONENT_RGBTOSPEC_DISPLAY_NAME "rgb2spec")
    set(CPACK_COMPONENT_RGBTOSPEC_DEPENDS "MRF")
    set(CPACK_COMPONENT_RGBTOSPEC_DISABLED)
    set(CPACK_COMPONENT_RGBTOSPEC_DESCRIPTION "Command-line tool to upsampling an RGB asset to a spectral one.")
    
    set(CPACK_COMPONENT_GROUP_APPLICATIONS_DESCRIPTION "MRF tools and applications.")
  
        
    set(CPACK_GENERATOR "NSIS64;7Z")
  else()    
    set(CPACK_GENERATOR "TGZ")
  endif()
  
  set(CPACK_PACKAGE_NAME                "Malia")
  set(CPACK_PACKAGE_VENDOR              "Manao - INRIA")
  set(CPACK_PACKAGE_VERSION_MAJOR       1)
  set(CPACK_PACKAGE_VERSION_MINOR       0)
  set(CPACK_PACKAGE_VERSION_PATCH       3)
  set(CPACK_PACKAGE_DESCRIPTION         "Predictive physically-realistic rendering")
  set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Predictive physically-realistic rendering")
  set(CPACK_PACKAGE_HOMEPAGE_URL        "https://gitlab.com/mrf-devteam/mrf")
  set(CPACK_PACKAGE_INSTALL_DIRECTORY   "MRF")
  set(CPACK_RESOURCE_FILE_LICENSE       "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
  set(CPACK_RESOURCE_FILE_README        "${CMAKE_CURRENT_SOURCE_DIR}/README.md")

  include(CPack)
endif()