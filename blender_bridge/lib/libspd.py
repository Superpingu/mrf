import os
import platform
import subprocess
from shutil import which

def writeSpd(fileName,spd_array):
  if len(spd_array) <= 0 :
    return

  with open(fileName, 'w', newline='') as fileOut:
    for x in spd_array:
      fileOut.write(str(x[0]))
      fileOut.write(":")
      fileOut.write(str(x[1]))
      fileOut.write(", ")

  #remove last 2 characters (, )
  with open(fileName, 'rb+') as filehandle:
    filehandle.seek(-2, os.SEEK_END)
    filehandle.truncate()


def readSpd(fileName,spd_array):
  with open(fileName, 'r') as fileIn:
    for line in fileIn.readlines():
      line_splitted = line.split(",")
      #print(line)
      for val in line_splitted:
        sub_val = val.split(":")
        if len(sub_val) == 2:
          #print("wave "+ str(sub_val[0]) + " val " + str(sub_val[1]))
          temp = [sub_val[0],sub_val[1]]
          spd_array.append(temp)

def addValueToSpd(fileNameIn, fileNameOut, value):
  """Read the input file, add value to all wavelengths and save spd
  """
  spd_array = []
  readSpd(fileNameIn,spd_array)
  for s in spd_array:
    s[1] = float(s[1]) + float(value)

  writeSpd(fileNameOut,spd_array)


def multiplyValueOfSpd(fileNameIn, fileNameOut, value):
  """Read the input file, multiply value of all wavelengths by value and save spd
  """
  spd_array = []
  readSpd(fileNameIn,spd_array)
  for s in spd_array:
    s[1] = float(s[1]) * float(value)

  writeSpd(fileNameOut,spd_array)


def rgb_to_spd(r, g, b, spd_path, mrf_tools, is_a_brdf = False):
  RGB2SPEC_EXE = mrf_tools['RGB2SPEC_EXE']

  if mrf_tools['RGB2SPEC_IS_AVAILABLE']:
    converter_args = [
      RGB2SPEC_EXE,
      "-in_color", str(r), str(g), str(b),
      "-nbw", "450",
      "-out", spd_path
    ]
    
    if is_a_brdf:
      converter_args.append("--reflectivity E")

    print("[DEBUG] RGB2SEPC Call is:", ' '.join(converter_args))

    process_converter = subprocess.Popen(converter_args)
    process_converter.wait()
  else:
    print("[WARNING] RGB2SPEC not available Spectrum was not exported")

def color_image_to_spectral_image(in_file, out_path, mrf_tools, nbw = 12, is_a_brdf = False):
  RGB2SPEC_EXE = mrf_tools['RGB2SPEC_EXE']

  if mrf_tools['RGB2SPEC_IS_AVAILABLE']:
    converter_args = [
      RGB2SPEC_EXE,
      "-in_file", in_file,
      "-nbw", str(nbw),
      "-out", out_path
    ]
    
    if is_a_brdf:
      converter_args.append("--reflectivity E")

    print("[DEBUG] RGB2SEPC Call is:", ' '.join(converter_args))

    process_converter = subprocess.Popen(converter_args)
    process_converter.wait()
  else:
    print("[WARNING] RGB2SPEC not available Spectrum was not exported")