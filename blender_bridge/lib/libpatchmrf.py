import os
import xml.etree.ElementTree as ET

def patch_materials(msf_root_node, patch_root):
	materials = msf_root_node.find('materials')

	#look at each material (in materials) in the patch
	for patch_node in patch_root.iter('material'):
		if 'name' in patch_node.keys():
			mat_name = patch_node.get('name')
			
			#Remove all materials with this name
			for material in materials:
				if 'name' in material.keys() and  material.get('name') == mat_name:
					materials.remove(material)

			#Add the material from the patch
			materials.append(patch_node)


def patch_emittances(msf_root_node, patch_root):
	materials = msf_root_node.find('materials')
	lights = msf_root_node.find('lights')

	#look at each emittance (in lights and materials) in the patch
	for emittance_new in patch_root.iter('emittance'):
		if 'name' in emittance_new.keys():
			emittance_name_new = emittance_new.get('name')
	
			#Remove all lights with a emittance that match this name
			for light in lights:
				emittance_old = light.find("./emittance")
				if not emittance_old:
					continue
				if 'name' in emittance_old.keys() and  emittance_old.get('name') == emittance_name_new:
					#light = patch_node.copy()
					#print(emittance)
					#print(emittance.get('name'))
					#parent = emittance.findall("..")
					#print(parent)
					light.remove(emittance_old)
					light.append(emittance_new)
					#msf_root_node.remove(light)

			#Remove all materials with this name
			for emittance_old in materials:
				if 'name' in emittance_old.keys() and  emittance_old.get('name') == emittance_name_new and  emittance_old.tag == "emittance":
					print(emittance_old.get('name'))
					materials.remove(emittance_old)
					materials.append(emittance_new)


def patch_ext_resources(msf_root_node, patch_root):
	patch_ext = patch_root.find("./ext_resources")
	ext = msf_root_node.find("./ext_resources")
	if patch_ext!=None and ext!=None:
		msf_root_node.remove(ext)
	if patch_ext!=None:
		msf_root_node.append(patch_ext)


def patch_background(msf_root_node, patch_root):
	#look at the background node
	patch_bck = patch_root.find("./background")
	bck = msf_root_node.find("./background")
	#print("patch bck")
	if patch_bck!=None and bck!=None:
		msf_root_node.remove(bck)
		#print("bck found")
	if patch_bck!=None:
		msf_root_node.append(patch_bck)
		#print("patch bck found")


# Tries to open the patch_filename and path the exported msf from blender
def patch_mrf_scene_after_export(msf_root_node, patch_filename):
	if not os.path.exists(patch_filename):
		print("CANNOT FIND PATCH "+patch_filename)
		return

	print("-----Applying PATCH "+patch_filename+" -----")
	

	patch_tree = ET.parse(patch_filename)
	patch_root = patch_tree.getroot()

	patch_emittances(msf_root_node,patch_root)
	patch_materials(msf_root_node,patch_root)
	patch_background(msf_root_node,patch_root)
	patch_ext_resources(msf_root_node,patch_root)
