import os,sys
import platform
import shutil
import logging
import stat

# a_logger = logging.getLogger("mrftools")
# a_logger.setLevel(logging.DEBUG)

def find_executable_from_path( an_executable_name, a_path):

    a_logger=logging.getLogger(__name__)

    full_path_to_exe = os.path.abspath(a_path + "/" + an_executable_name)
    a_logger.debug("Checking existence of %s", full_path_to_exe)

    if os.path.exists(full_path_to_exe):
        a_logger.debug("!!! FOUND !!!")
        return full_path_to_exe
    else:
        return None
#end_def


def find_tool_executable( an_executable_name , additional_paths_to_look_for):

    a_logger=logging.getLogger(__name__)

    #Check if the executable_name is in path
    found = shutil.which(an_executable_name)
    if found is None:
        #Check the additional paths
        for a_path in additional_paths_to_look_for:
            found_path = find_executable_from_path(an_executable_name, a_path)
            if found_path is not(None):
                return found_path
            #end_if
        #end_for
        return None
    else:
        return found
#end_def
def check_and_adjust_file_permission( path_to_exe ):

    a_logger=logging.getLogger(__name__)

    filemode = os.stat(path_to_exe).st_mode
    print(filemode)
    ret = bool(filemode & stat.S_IXUSR or filemode & stat.S_IXGRP or filemode & stat.S_IXOTH)

    if ret:
        a_logger.debug("%s is executable ", path_to_exe)
    else:
        a_logger.debug("Forcing %s to be executable ", path_to_exe)
        filemode |= filemode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH
        os.chmod(path_to_exe, filemode)

#end_def


def find_mrf_tools( additional_paths_to_look_for ):

    a_logger=logging.getLogger(__name__)

    MALIA_NAME = "malia"
    if platform.system()=="Windows":
        MALIA_NAME += ".exe"

    MALIA_EXE      = find_tool_executable( MALIA_NAME, additional_paths_to_look_for)
    MALIA_IS_AVAILABLE = False if (MALIA_EXE is None) else True

    MALIA_RGB_NAME = "malia_rgb"
    if platform.system()=="Windows":
        MALIA_RGB_NAME += ".exe"

    MALIA_RGB_EXE      = find_tool_executable( MALIA_RGB_NAME, additional_paths_to_look_for)
    MALIA_RGB_IS_AVAILABLE = False if (MALIA_RGB_EXE is None) else True


    RGB2SPEC_NAME = "rgb2spec"
    if platform.system()=="Windows":
        RGB2SPEC_NAME += ".exe"

    RGB2SPEC_EXE      = find_tool_executable( RGB2SPEC_NAME, additional_paths_to_look_for)
    RGB2SPEC_IS_AVAILABLE = False if (RGB2SPEC_EXE is None) else True

    MIC_NAME = "mic"
    if platform.system()=="Windows":
        MIC_NAME += ".exe"

    MIC_EXE      = find_tool_executable( MIC_NAME, additional_paths_to_look_for)
    MIC_IS_AVAILABLE = False if (MIC_EXE is None) else True


    SPECTRALVIEWER_NAME = "SpectralViewer"
    if platform.system()=="Windows":
        SPECTRALVIEWER_NAME = "SpectralViewer.exe"

    SPECTRALVIEWER_EXE      = find_tool_executable( SPECTRALVIEWER_NAME, additional_paths_to_look_for)
    if SPECTRALVIEWER_EXE is None:
        SPECTRALVIEWER_EXE = shutil.which(SPECTRALVIEWER_NAME)
    SPECTRALVIEWER_IS_AVAILABLE = False if (SPECTRALVIEWER_EXE is None) else True


    if MALIA_IS_AVAILABLE:
        a_logger.info('FOUND Malia  at: %s'         ,  MALIA_EXE )
        check_and_adjust_file_permission( MALIA_EXE)

    if MALIA_RGB_IS_AVAILABLE:
        a_logger.info('FOUND Malia RGB version at: %s',  MALIA_RGB_EXE )
        check_and_adjust_file_permission(MALIA_RGB_EXE)

    if RGB2SPEC_IS_AVAILABLE:
        a_logger.info('FOUND rgb2spec at: %s ' ,  RGB2SPEC_EXE )
        check_and_adjust_file_permission(RGB2SPEC_EXE)
    else:
        a_logger.warning("rgb2spec NO FOUND")

    if MIC_IS_AVAILABLE :
        a_logger.info('FOUND mic (malia image converter) at: %s ' ,  MIC_EXE )
        check_and_adjust_file_permission(MIC_EXE)

    if SPECTRALVIEWER_IS_AVAILABLE:
        a_logger.info("Found SpectralViewer in %s", SPECTRALVIEWER_EXE)
        # we dont check permission because Spectral Viewer is supposed to be installed and therefore executable


    return {'MALIA_IS_AVAILABLE'          : MALIA_IS_AVAILABLE,
            'MALIA_EXE'                   : MALIA_EXE,
            'MALIA_RGB_IS_AVAILABLE'      : MALIA_RGB_IS_AVAILABLE,
            'MALIA_RGB_EXE'               : MALIA_RGB_EXE,
            'RGB2SPEC_IS_AVAILABLE'       : RGB2SPEC_IS_AVAILABLE,
            'RGB2SPEC_EXE'                : RGB2SPEC_EXE,
            'MIC_IS_AVAILABLE'            : MIC_IS_AVAILABLE,
            'MIC_EXE'                     : MIC_EXE,
            'SPECTRALVIEWER_IS_AVAILABLE' : SPECTRALVIEWER_IS_AVAILABLE,
            'SPECTRALVIEWER'              : SPECTRALVIEWER_EXE}

#   BIN_DIRECTORY = a_path;
#   a_logger.debug('BIN_DIRECTORY:  %s ',  BIN_DIRECTORY )

#   MALIA_NAME = "malia"
#   if platform.system()=="Windows":
#     MALIA_NAME += ".exe"



#     MALIA_EXE = os.path.abspath(BIN_DIRECTORY +"/" + MALIA_NAME)
#     MALIA_IS_AVAILABLE= os.path.exists(  MALIA_EXE )
#     if( MALIA_IS_AVAILABLE ):
#         a_logger.info('FOUND Malia  at: %s'         ,  MALIA_EXE )
#   #end_if

#   MALIA_RGB_NAME = "malia_rgb"
#   if platform.system()=="Windows":
#     MALIA_RGB_NAME += ".exe"

#     MALIA_RGB_EXE = os.path.abspath(BIN_DIRECTORY + "/" + MALIA_RGB_NAME)
#     MALIA_RGB_IS_AVAILABLE= os.path.exists(  MALIA_RGB_EXE )

#     if( MALIA_RGB_IS_AVAILABLE ):
#         a_logger.info('FOUND Malia RGB version at: %s',  MALIA_RGB_EXE )
#   #end_if

#   RGB2SPEC_NAME = "rgb2spec"
#   if platform.system()=="Windows":
#     RGB2SPEC_NAME += ".exe"

#     RGB2SPEC_EXE = os.path.abspath(BIN_DIRECTORY + "/" + RGB2SPEC_NAME)
#     RGB2SPEC_IS_AVAILABLE= os.path.exists(  RGB2SPEC_EXE )

#     if( RGB2SPEC_IS_AVAILABLE ):
#         a_logger.info('FOUND rgb2spec at: %s ' ,  RGB2SPEC_EXE )
#   #end_if

#   MIC_NAME = "mic"
#   if platform.system()=="Windows":
#     MIC_NAME += ".exe"


#     MIC_EXE = os.path.abspath(BIN_DIRECTORY+"/" + MIC_NAME )
#     MIC_IS_AVAILABLE= os.path.exists(  MIC_EXE )

#     if( MIC_IS_AVAILABLE ):
#         a_logger.info('FOUND mic (malia image converter) at: %s ' ,  MIC_EXE )
#   #end_if

#   spectral_viewer = ""
#   if platform.system()=="Windows":
#     spectral_viewer = "SpectralViewer.exe"
# else:
#     spectral_viewer = "SpectralViewer"

#     SPECTRAL_VIEWER_IS_AVAILABLE = False;
#     SPECTRALVIEWER_EXE=""

#     if which(spectral_viewer) is None :
#         a_logger.warning("Cannot find SpectralViewer in your PATH")
#     else:
#         SPECTRALVIEWER_EXE = os.path.abspath(which(spectral_viewer))

#         a_logger.info("Found SpectralViewer in %s", SPECTRALVIEWER_EXE)

#         SPECTRAL_VIEWER_IS_AVAILABLE = True;


#         return {'MALIA_IS_AVAILABLE'          : MALIA_IS_AVAILABLE,
#         'MALIA_EXE'                   : MALIA_EXE,
#         'MALIA_RGB_IS_AVAILABLE'      : MALIA_RGB_IS_AVAILABLE,
#         'MALIA_RGB_EXE'               : MALIA_RGB_EXE,
#         'RGB2SPEC_IS_AVAILABLE'       : RGB2SPEC_IS_AVAILABLE,
#         'RGB2SPEC_EXE'                : RGB2SPEC_EXE,
#         'MIC_IS_AVAILABLE'            : MIC_IS_AVAILABLE,
#         'MIC_EXE'                     : MIC_EXE,
#         'SPECTRALVIEWER_IS_AVAILABLE' : SPECTRAL_VIEWER_IS_AVAILABLE,
#         'SPECTRALVIEWER'              : SPECTRALVIEWER_EXE}

#end_find_mrf_tools