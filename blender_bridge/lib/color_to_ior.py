import numpy as np
import os,sys
import math

from libspd import readSpd
from libspd import writeSpd

def floatToIor(value):
  """Converts spectral color to IOR

  This function is used to convert a spectral color to a spectral
  ior of a conductor.  It converts a real float value to a complex float
  ior, the input value comes from a color spectrum, it is its power at
  one wavelength

  note from RP: This is the Guidbransen function assuming that the
  reflectivity and edgetint colors are the same See here for more
  details http://jcgt.org/published/0003/04/03/paper.pdf
  """
  
  f0 = float(value)
  g = float(value)

  if f0 >= 1.0:
    f0 = 0.999999999

  if f0 < 0.0:
    f0 = 0.0

  eta = ( (1.0-g)*(1.0+math.sqrt(f0)) / ( 1.0-math.sqrt(f0) ) ) + ( ( (1.0-f0)/(1.0+f0) )*g )

  sqr_eta_plus_one = (eta+1.0)*(eta+1.0)
  sqr_eta_minus_one = (eta-1.0)*(eta-1.0)

  temp = (f0 * sqr_eta_plus_one - sqr_eta_minus_one)

  if temp < 0:
    temp = 0.

  kappa = math.sqrt(temp / (1.0-f0) )

  if g == 0.0 :
    kappa = 0.0

  return (eta,kappa)


def colorToIor(rgb):
  """Converts a rgb color to a complex rgb ior of a conductor"""
  
  rgb = np.array(rgb)
  ONE = np.ones((3,))
  eta = np.ones((3,))

  f0 = rgb
  g = rgb
  f0[ f0 >= 1.0] = 0.9999999

  eta = ( (ONE-g)*(ONE+np.sqrt(f0)) / ( ONE-np.sqrt(f0) ) ) + ( ( (ONE-f0)/(ONE+f0) )*g )

  sqr_eta_plus_one = (eta+ONE)*(eta+ONE)
  sqr_eta_minus_one = (eta-ONE)*(eta-ONE)

  temp = (f0 * sqr_eta_plus_one - sqr_eta_minus_one)
  temp[temp<0] = 0.
  kappa = np.sqrt(temp / (ONE-f0) )

  if( g[0] == 0.0 ):
    kappa[0] = 0.0
  if( g[1] == 0.0 ):
    kappa[1] = 0.0
  if( g[2] == 0.0 ):
    kappa[2] = 0.0

  return ( eta,kappa )

def spectralColorToIor(spd_file_path):
  """Convert spectral color to IOR from .spd file

  Converts a spectral color stored in a .spd file to a complex
  spectral ior of a conductor Stores the eta and kappa as two separate
  files, where names are built from the input file, adding _eta or
  _kappa at the end.
  """

  if not os.path.exists(spd_file_path):
    return
  spectrum = []
  spectrum_eta = []
  spectrum_kappa = []
  readSpd(spd_file_path, spectrum)
  for value in spectrum:
    eta,kappa = floatToIor( float(value[1]) )

    spectrum_eta.append( [value[0], eta] )
    spectrum_kappa.append( [value[0], kappa] )

  writeSpd(spd_file_path[:-4]+"_eta.spd", spectrum_eta)
  writeSpd(spd_file_path[:-4]+"_kappa.spd", spectrum_kappa)

