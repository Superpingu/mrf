#version 450 compatibility

uniform sampler2D display_buffer;

uniform vec2 viewport;

uniform int   is_srgb;
uniform float gamma;
uniform float exposure;

layout(location = 0) out vec4 out_color;

vec3 tosRGB(vec3 rgb)
{
  vec3 res = rgb;

  //sRGB companding
  for (uint i = 0; i < 3; i++)
  {
    if (res[i] < 0.0031308)
      res[i] = max(res[i] * 12.92, 0.);
    else
      res[i] = 1.055 * pow(res[i], 1. / 2.4) - 0.055;
  }
  return res;
}

vec3 blurr(ivec2 fragCoord)
{
  vec3 c = texelFetch(display_buffer, ivec2(fragCoord), 0).rgb;

  vec3 n = texelFetch(display_buffer, ivec2(fragCoord + ivec2(1, 0)), 0).rgb;
  vec3 s = texelFetch(display_buffer, ivec2(fragCoord + ivec2(-1, 0)), 0).rgb;
  vec3 e = texelFetch(display_buffer, ivec2(fragCoord + ivec2(0, 1)), 0).rgb;
  vec3 w = texelFetch(display_buffer, ivec2(fragCoord + ivec2(0, -1)), 0).rgb;

  return 0.5 * c + 0.125 * (n + s + e + w);
}

float rgbToRad(vec3 rgb)
{
  return rgb.r * 0.265 + rgb.g * 0.67 + rgb.b * 0.065;
}

float weightBilateral(ivec2 y_x, vec3 rgb_x, vec3 rgb_y, float sqr_sigma_d, float sqr_sigma_r)
{
  float i_x   = rgbToRad(rgb_x);
  float i_y   = rgbToRad(rgb_y);
  float delta = length(y_x);

  return exp(-delta * delta / (2.0 * sqr_sigma_d)) * exp(-(i_y - i_x) * (i_y - i_x) / (2.0 * sqr_sigma_r));
}

vec3 bilateralFiltering(ivec2 fragCoord)
{
  //TODO implement size varying bilateralFiltering
  vec3 c         = texelFetch(display_buffer, ivec2(fragCoord), 0).rgb;
  vec3 ret_value = c;

  float sigma_d = 0.5;
  float sigma_r = 0.5;

  float normTerm = 1.0;
  float weight;

  vec3 n = texelFetch(display_buffer, fragCoord + ivec2(1, 0), 0).rgb;
  weight = weightBilateral(ivec2(1, 0), c, n, sigma_d * sigma_d, sigma_r * sigma_r);
  ret_value += weight * n;
  normTerm += weight;

  vec3 s = texelFetch(display_buffer, fragCoord + ivec2(-1, 0), 0).rgb;
  weight = weightBilateral(ivec2(-1, 0), c, s, sigma_d * sigma_d, sigma_r * sigma_r);
  ret_value += weight * s;
  normTerm += weight;

  vec3 e = texelFetch(display_buffer, fragCoord + ivec2(0, 1), 0).rgb;
  weight = weightBilateral(ivec2(0, 1), c, e, sigma_d * sigma_d, sigma_r * sigma_r);
  ret_value += weight * e;
  normTerm += weight;

  vec3 w = texelFetch(display_buffer, fragCoord + ivec2(0, -1), 0).rgb;
  weight = weightBilateral(ivec2(0, -1), c, w, sigma_d * sigma_d, sigma_r * sigma_r);
  ret_value += weight * w;
  normTerm += weight;

  return ret_value / normTerm;
}

void main(void)
{
  ivec2 pixel_coord = ivec2(gl_FragCoord.xy);
  vec3  ret_color   = texelFetch(display_buffer, pixel_coord, 0).rgb;
  // vec3 ret_color = blurr(pixel_coord);
  // vec3 ret_color = bilateralFiltering(pixel_coord);

  ret_color *= exp2(exposure);

  if (is_srgb == 1)
    ret_color = tosRGB(ret_color);
  else
    ret_color = pow(ret_color, vec3(1.0 / gamma));

  out_color = vec4(ret_color, 1.0);
}
