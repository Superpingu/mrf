/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017-2019
 *
 * Modifications :
 *  - David Murray @ inria.fr
 *  - Romain Pacanowski @ institutoptique.fr
 *  Copyright  CNRS 2018-2019
 *
 *
 **/

#include <mrf_core/mrf_config.h>
#include <optix_backend_config.h>

#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>

#include <mrf_core/image/color_image.hpp>
#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>

#include <mrf_core/util/arg_parse.hpp>
#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/io/scene_parser.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <mrf_plugins/register_mrf_plugins.hpp>
#include <mrf_plugins/register_optix_plugins.hpp>


using std::string;
using std::vector;
using namespace mrf;
using namespace mrf::image;
using namespace mrf::color;
using namespace mrf::radiometry;
using namespace mrf::rendering;
using mrf::gui::fb::Loger;
using mrf::util::ARG_LIST;

using mrf::uint;

//------------------------------------------------------------------------------
//
// Globals
//
//------------------------------------------------------------------------------

//Context        context = 0;
mrf::rendering::Renderer *_renderer;

//------------------------------------------------------------------------------
//
// Forward decls
//
//------------------------------------------------------------------------------

#ifdef RENDERER_INTERACTIVE

#  ifdef __APPLE__
#    include <GLFW/glfw3.h>
#  else
#    include <GL/glew.h>
#    if defined(_WIN32)
#      include <GLFW/glfw3.h>
#    else
#      include <GLFW/glfw3.h>
#    endif
#  endif

#  include "gui/mrf_baseUI.h"
#  include "gui/mrf_interactiveUI.h"

mrf::gui::BaseUI *_gui = nullptr;
void              installRenderingFunc(GLFWwindow *win);
void              resizeWrapper(GLFWwindow *win, int w, int h);
void              mousePressWrapper(GLFWwindow *win, int button, int action, int mods);
void              mouseMotionWrapper(GLFWwindow *win, double x, double y);
void              keyboardPressWrapper(GLFWwindow *win, int key, int scancode, int action, int mods);
void              charPressWrapper(GLFWwindow *win, unsigned int codepoint);
void              mouseScrollWrapper(GLFWwindow *window, double xoffset, double yoffset);
void              minimizeWrapper(GLFWwindow *window, bool iconified);

//------------------------------------------------------------------------------
//
//  GL callbacks (wrapper)
//
//------------------------------------------------------------------------------

void resizeWrapper(GLFWwindow * /*win*/, int w, int h)
{
  _gui->resize(w, h);
}


void mousePressWrapper(GLFWwindow * /*win*/, int button, int action, int mods)
{
  _gui->mousePress(button, action, mods);
}


void mouseMotionWrapper(GLFWwindow * /*win*/, double x, double y)
{
  _gui->mouseMotion(x, y);
}


void keyboardPressWrapper(GLFWwindow * /*win*/, int key, int scancode, int action, int mods)
{
  _gui->keyboardPress(key, scancode, action, mods);
}


void charPressWrapper(GLFWwindow * /*win*/, unsigned int codepoint)
{
  _gui->charPress(codepoint);
}


void mouseScrollWrapper(GLFWwindow * /*win*/, double xoffset, double yoffset)
{
  _gui->mouseScroll(xoffset, yoffset);
}


void dropWrapper(GLFWwindow * /*win*/, int count, const char **paths)
{
  _gui->dropEvent(count, paths);
}


void minimizeWrapper(GLFWwindow * /*win*/, int iconified)
{
  _gui->minimizeEvent(iconified);
}


void installRenderingFunc(GLFWwindow *win)
{
  glfwSetFramebufferSizeCallback(win, resizeWrapper);
  glfwSetKeyCallback(win, keyboardPressWrapper);
  glfwSetCharCallback(win, charPressWrapper);
  glfwSetCursorPosCallback(win, mouseMotionWrapper);
  glfwSetMouseButtonCallback(win, mousePressWrapper);
  glfwSetScrollCallback(win, mouseScrollWrapper);
  glfwSetDropCallback(win, dropWrapper);
  glfwSetWindowIconifyCallback(win, minimizeWrapper);
}


#endif   // RENDERER_INTERACTIVE

void printUsage()
{
  std::cout << R"(
********************* Help message for malia renderer ************************
)"

#ifdef MRF_RENDERING_MODE_SPECTRAL
            << R"(
Usage: malia [mandatory_options] [optional_options]
)"
#else
            << R"(
Usage: malia_rgb [mandatory_options] [optional_options]
)"
#endif
            << R"(
Where [mandatory_options] are:
  -scene {string}
      Scene file name
)"
#ifdef MRF_RENDERING_MODE_SPECTRAL
            << R"(
And one of the following options to specify sampled wavelengths
  -wavelength_range/-wr {uint}start:{uint}end:{uint}step
      Range of wavelengths sampled, from start to end (both included) by step.

  -wavelengths_per_pass/-wpp{uint}
      Number of wavelengths sampled per pass of rendering. Default=1.
      A Value > 1 means spectral multiplexing.

  -continuous_sampling
      Use continuous sampling when processing a single wavelength for a most accurate and unbiased result.
      CAUTION: rendering may be slow for scene with textures and environment maps.

  -save_temp
      Save temporary files (one after each pass).
)"
#endif
            << R"(
Optional options [optional_options] are:
  -cam/-camera {string}
      Camera file name.
      If not specified optix renderer looks for a.mcf file which name is the
      same as the scene.
      If none is found a default ThinLens camera is constructed.

  -height {uint}
      Height in pixels of the image.

  -width {uint}
      Width in pixels of the image.

  -cudafile {string}
      Path to path tracer cuda file.

  -runtime_ptx
      Disable ptx usage and force recompilation at runtime.

  -overwrite_ptx
      Force recompilation at runtime and reexport (overwrite) ptx.
)"
#ifdef RENDERER_INTERACTIVE
            << R"(
  -i/-interactive
      Interactive rendering mode.

  -nogui
      Interactive rendering mode without a GUI.
)"
#endif   // RENDERER_INTERACTIVE
            << R"(
  -mpl/-max_path_length
      Maximum Level of Recursion of a path (default=10)

  -next_event
      Activate the MIS.
      Possible values are one, none (default = one).

  -envmap_is
      Activate/deactivate envmap importance sampling.
      Possible values are: on, off (default = on).

  -max_time
      Maximum Rendering time in seconds.
      A value of -1 deactivate this termination criterion.
      Default value is 3600 (one hour).
      This parameter is not used in interactive mode.

  -o/-out {string}
      Names to save the rendered image. Multiple names can be specified.
      If not specified the name of the scene file will be used.

  -rng {string}
      Type of random number generator (default=halton).
      Possible values are: optix sobol halton.
      WARNING: choosing halton limits the paths to 10 bounces.

  -rng_seed {unsigned int}
      Seed for RNG, doesn't work with optix RNG.

  -debug_pixels i
      Debuging pixels.

  -log_variance
      Compute per pixel, per "wavelength" variance. The result is saved along
      with the output image ("name_of_output" + "_variance") using the same
      extension.

  -samples {uint}
      Number of samples per pixel(SPP). This parameter is not used in
      interactive mode.

  -spf/-samples_per_frame {uint}
      Number of samples per frame and per pixel (only enabled for interactive
      mode).

  -startingcam {uint}
      Start rendering at a specific camera num.
      Cameras are in range [0,n-1] in .mcf.

  -numcam {uint}
      The total number of camera to render.
      Cameras are in range [0,n-1] in .mcf.
      This parameter is not used in interactive mode.

  -logging {string}
      Defines the level of the logging system.
      Possible values are : No_Log (0) Fatal (1) Warning (2) Info (3)
      Trace (4) Debug (5).

  -logfile {string}
      Path to a log file, append the spp and render time at the end of this
      file.

  -env_rot {float1 float2}
      Rotate (if any)  the environment map of the scene with the given phi and
      theta angles specified as argument.

  -help/-h
      Print this help message.

******************************************************************************
)";
}

//------------------------------------------------------------------------------
//
// Main
//
//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  mrf::util::ArgsParsing parser(argc, argv);

  if (parser.hasArgument(ARG_LIST::HELP))
  {
    printUsage();
    return EXIT_SUCCESS;
  }

  mrf::gui::fb::Loger::getInstance()->message("Malia Rendering Engine Started\n");

  try
  {
    mrf::io::MaterialPlugins matplugins;
    mrf::io::register_all_mat(matplugins);

    mrf::optix_backend::OptixMaterialPlugins optixmatplugins;
    mrf::optix_backend::register_all_mat(optixmatplugins);

    mrf::optix_backend::OptixLightPlugins optixlightplugins;
    mrf::optix_backend::register_all_lights(optixlightplugins);

    _renderer = new mrf::optix_backend::OptixRenderer(
        optixmatplugins,
        optixlightplugins,
        parser.hasArgument(ARG_LIST::RUNTIME_PTX_COMPIL),
        parser.hasArgument(ARG_LIST::OVERWRITE_PTX));

    if (parser.hasArgument(ARG_LIST::CUDA_FILE))
    {
      _renderer->setExternalFile(parser.getSingleValue(ARG_LIST::CUDA_FILE));
      mrf::gui::fb::Loger::getInstance()->info(
          "Using a custom Path Tracer (cuda): ",
          parser.getSingleValue(ARG_LIST::CUDA_FILE));
    }

    if (parser.hasArgument(ARG_LIST::MAX_TIME))
    {
      _renderer->setMaxRenderingTimeSeconds(stoi(parser.getSingleValue(ARG_LIST::MAX_TIME)));
    }

    //do these 4 calls before create render context
    //TODO be able to change theme after creating render context
    //need some work in rng table generation, both sobol andmrf::rendering::HALTON

    // By default Importance Samplign for envmap is on
    _renderer->setEnvmapImportanceSampling(true);
    if (parser.hasArgument(ARG_LIST::ENVMAP_IS))
    {
      if (parser.getSingleValue(ARG_LIST::ENVMAP_IS) == "off" || parser.getSingleValue(ARG_LIST::ENVMAP_IS) == "OFF"
          || parser.getSingleValue(ARG_LIST::ENVMAP_IS) == "false"
          || parser.getSingleValue(ARG_LIST::ENVMAP_IS) == "FALSE")
      {
        _renderer->setEnvmapImportanceSampling(false);
      }
    }

    NEXT_EVENT  next_event_type = NEXT_EVENT::ONE;
    std::string ne              = parser.getSingleValue(ARG_LIST::NEXT_EVENT);
    if (ne == "none")
    {
      next_event_type = NEXT_EVENT::NONE;
    }
    else if (ne == "all")
    {
      next_event_type = NEXT_EVENT::ALL;
    }
    else if (ne == "one")
    {
      next_event_type = NEXT_EVENT::ONE;
    }
    _renderer->setNextEvent(next_event_type);

    RENDERER_RNG rng_type = mrf::rendering::RENDERER_RNG::HALTON;
    std::string  rng      = parser.getSingleValue(ARG_LIST::RNG_METHOD);
    if (rng == "optix")
      rng_type = mrf::rendering::RENDERER_RNG::OPTIX;
    else if (rng == "halton")
      rng_type = mrf::rendering::RENDERER_RNG::HALTON;
    else if (rng == "sobol")
      rng_type = mrf::rendering::RENDERER_RNG::SOBOL;

    _renderer->setRngType(rng_type);
    _renderer->setMaxPathLength(stoi(parser.getSingleValue(ARG_LIST::MPL)));
    _renderer->setNumSamplesPerFrame(stoi(parser.getSingleValue(ARG_LIST::SPF)));

    //Can be done after creating render context but more efficient
    //to do it before, it doesn't regenerate cranley patterson buffer
    if (parser.hasArgument(ARG_LIST::RNG_SEED))
      _renderer->setRNGSeed((mrf::rendering::RENDERER_RNG)stoi(parser.getSingleValue(ARG_LIST::RNG_SEED)));

    _renderer->setVarianceLog(parser.hasArgument(ARG_LIST::LOG_VARIANCE));

#ifdef MRF_RENDERING_MODE_SPECTRAL
    //always create render context before importing scene and setting spectral wavelengths
    _renderer->init(parser.hasArgument(ARG_LIST::INTERACTIVE), stoi(parser.getSingleValue(ARG_LIST::WPP)));
    vector<mrf::uint> wavelengths;
    for (size_t i = 0; i < parser.getMultipleValue(ARG_LIST::WAVELENGTH).size(); ++i)
      wavelengths.push_back(stoi(parser.getMultipleValue(ARG_LIST::WAVELENGTH)[i]));
    _renderer->setSpectralWavelengths(wavelengths);
#else
    //always create render context before importing scene
    _renderer->init(parser.hasArgument(ARG_LIST::INTERACTIVE));
#endif

    mrf::util::PrecisionTimer timer;
    std::cout << "Starting to load Scene " << std::endl;
    timer.start();

    if (parser.hasArgument(ARG_LIST::HEIGHT))
      _renderer->overrideSensorHeight(stoi(parser.getSingleValue(ARG_LIST::HEIGHT)));
    if (parser.hasArgument(ARG_LIST::WIDTH))
      _renderer->overrideSensorWidth(stoi(parser.getSingleValue(ARG_LIST::WIDTH)));

    mrf::io::SceneParser *sceneLoader = new mrf::io::SceneParser(matplugins);
    if (parser.hasArgument(ARG_LIST::SCENE) && parser.hasArgument(ARG_LIST::CAMERA))
    {
      sceneLoader->setPath(parser.getSingleValue(ARG_LIST::SCENE));
      //_renderer->importMrfScene(
      //    sceneLoader,
      //    parser.getSingleValue(ARG_LIST::CAMERA),
      //    stoi(parser.getSingleValue(ARG_LIST::FIRST_CAMERA)),
      //    stoi(parser.getSingleValue(ARG_LIST::NUM_CAMERAS)));
      _renderer->importMrfScene(sceneLoader, parser.getSingleValue(ARG_LIST::CAMERA));
    }

    if (parser.hasArgument(ARG_LIST::SAMPLES))
      _renderer->overrideSensorSamples(stoi(parser.getSingleValue(ARG_LIST::SAMPLES)));

    _renderer->setContinuousSampling(parser.hasArgument(ARG_LIST::CONTINUOUS_SAMPLING));
    mrf::gui::fb::Loger::getInstance()->info(
        "Continuous sampling set to" + std::to_string(parser.hasArgument(ARG_LIST::CONTINUOUS_SAMPLING)));
    auto elapsed = timer.elapsed();

    mrf::gui::fb::Loger::getInstance()->info("Time to import scene in OptiX Backend (seconds): ", elapsed);
    mrf::gui::fb::Loger::getInstance()->info("Number of view points Loaded: ", _renderer->getAllCamera().size());

    //-----------------------------------------------------------------------------------------------------------------------------------------
    // Configure more options of the renderer if they are coming from the command line
    //-----------------------------------------------------------------------------------------------------------------------------------------

    // Rotation of the environment map
    std::vector<std::string> const angles = parser.getMultipleValue(ARG_LIST::ENV_ROT);

    if (angles.size() >= 1)
    {
      float const phi_angle = std::stof(angles[0]);
      _renderer->setRotationPhi(phi_angle);

      mrf::gui::fb::Loger::getInstance()->trace(
          "Overriding the orientation of the environment map with angle phi set to: ",
          phi_angle);
    }

    if (angles.size() == 2)
    {
      float const theta_angle = std::stof(angles[1]);
      _renderer->setRotationTheta(theta_angle);

      mrf::gui::fb::Loger::getInstance()->trace(
          "Overriding the orientation of the environment map with angle theta set to: ",
          theta_angle);
    }
//----------------------------------------------------------------------------------------------------------------


/*
//TODO
_renderer.setInstancing(true);
_renderer.setRenderingMaxSamples(max_samples);
_renderer.setMaxRenderingTime(max_rendering_time);
*/
#ifdef RENDERER_INTERACTIVE

    if (parser.hasArgument(ARG_LIST::INTERACTIVE))
    {
      if (parser.hasArgument(ARG_LIST::NO_GUI))
      {
        _gui = new mrf::gui::BaseUI();
      }
      else
      {
        _gui = new mrf::gui::InteractiveUI();
      }

      mrf::gui::fb::Loger::getInstance()->info(_renderer->getInfos());

      std::string temp_image_name;
      std::string temp_camera_name;
      std::string temp_file_extension;
      if (parser.hasArgument(ARG_LIST::OUTPUT))
      {
        mrf::util::StringParsing::fileNameFromAbsPathWithoutExtension(
            parser.getSingleValue(ARG_LIST::OUTPUT).c_str(),
            temp_image_name);
        mrf::util::StringParsing::getFileExtension(
            parser.getSingleValue(ARG_LIST::OUTPUT).c_str(),
            temp_file_extension);
        string dir;
        mrf::util::StringParsing::getDirectory(parser.getSingleValue(ARG_LIST::OUTPUT).c_str(), dir);

        temp_camera_name = dir + temp_image_name + "_interactive_camera.mcf";
        temp_image_name  = dir + temp_image_name + "." + temp_file_extension;
      }

      if (parser.hasArgument(ARG_LIST::SCENE)) _gui->setSceneInput(parser.getSingleValue(ARG_LIST::SCENE));
      _gui->setImageOutput(temp_image_name);
      _gui->setCameraOutput(temp_camera_name);

      bool v_sync = false;   //No v-sync
      _gui->initializeUI(_renderer, v_sync);
      installRenderingFunc(_gui->getWindow());

      _gui->run();
    }
    else   // INTERACTIVE MODE OR NOT
    {
#endif   // RENDERER_INTERACTIVE

      if (!parser.hasArgument(ARG_LIST::SCENE))
        mrf::gui::fb::Loger::getInstance()->fatal("Starting offline renderer without a scene, ABORTING.");

      std::ofstream logfile_stream;
      if (parser.hasArgument(ARG_LIST::LOGFILE))
        logfile_stream.open(parser.getSingleValue(ARG_LIST::LOGFILE), std::ios::out | std::ios::app);

      std::vector<Camera *> cams = _renderer->getAllCamera();

      size_t starting_camera
          = parser.hasArgument(ARG_LIST::FIRST_CAMERA) ? std::stoi(parser.getSingleValue(ARG_LIST::FIRST_CAMERA)) : 0u;

      size_t number_of_cameras = parser.hasArgument(ARG_LIST::NUM_CAMERAS)
                                     ? std::stoi(parser.getSingleValue(ARG_LIST::NUM_CAMERAS))
                                     : cams.size() - starting_camera;

      if (number_of_cameras > _renderer->nbCameras())
      {
        mrf::gui::fb::Loger::getInstance()->warn("Command line requested more view points than available.");
        mrf::gui::fb::Loger::getInstance()->warn("Assigning the maximum number of available cameras in the .mcf file");
        //Requested " + std::to_string(number_of_cameras) + "read" + std::to_string(_renderer->nbCameras()) );
        number_of_cameras = _renderer->nbCameras();
      }

      mrf::gui::fb::Loger::getInstance()->info("Number of View points to render:", number_of_cameras);

      mrf::gui::fb::Loger::getInstance()->info("Initial Starting Camera Index:", starting_camera);

      if (starting_camera >= _renderer->nbCameras())
      {
        mrf::gui::fb::Loger::getInstance()->warn(
            "Command line requested a starting camera index which does not exist!.");
        mrf::gui::fb::Loger::getInstance()->warn("Assigning zero as default value");
        starting_camera = 0;
      }

      // Update the Renderer with current Camera!
      _renderer->setCurrentCamera(starting_camera);
      if ((starting_camera + number_of_cameras) > _renderer->nbCameras())
      {
        mrf::gui::fb::Loger::getInstance()->warn(
            "Cannot render the number of requested cameras (" + std::to_string(number_of_cameras)
            + " ) from starting index: " + std::to_string(starting_camera));
      }

      mrf::gui::fb::Loger::getInstance()->info("Starting rendering...");

      // Deal with impossible request
      const size_t max_index = ((starting_camera + number_of_cameras) < _renderer->nbCameras())
                                   ? (starting_camera + number_of_cameras)
                                   : _renderer->nbCameras();

      for (size_t cam_index = starting_camera; cam_index < max_index; cam_index++)
      {
        if (_renderer->getTotalNumPasses() == 1 && _renderer->getCurrentCameraIndex() > 0)
        {
          _renderer->disableAssetsUpdate();
          mrf::gui::fb::Loger::getInstance()->trace(
              "The total number of spectral pass is equal to 1 disabling assets update");
        }

        //_renderer->setDebugPixels(debug_pixels);

        mrf::gui::fb::Loger::getInstance()->info(_renderer->getInfos());

        _renderer->enableAssetsUpdate();
        //for (int env_phi = 0; env_phi < 1; ++env_phi)
        {
          //_renderer->setRotationPhi(env_phi);
          auto total_rendering_time = _renderer->renderSamples();
          //_renderer->disableAssetsUpdate();

          std::string temp_file_name;
          std::string temp_file_extension;
          string      dir;

          // Sets the image output file will be overriden IF -o is passed fro mthe command line
          mrf::util::StringParsing::fileNameFromAbsPathWithoutExtension(
              parser.getSingleValue(ARG_LIST::SCENE).c_str(),
              temp_file_name);
          mrf::util::StringParsing::getDirectory(parser.getSingleValue(ARG_LIST::SCENE).c_str(), dir);

          mrf::gui::fb::Loger::getInstance()->trace(" Temp file name ", temp_file_name);
          mrf::gui::fb::Loger::getInstance()->trace(" temp_file_extension", temp_file_extension);
          mrf::gui::fb::Loger::getInstance()->trace(" dir ", dir);

//RGB rendering can save under multiples formats
#ifndef MRF_RENDERING_MODE_SPECTRAL
          //multiples filename is usefull to save an image under several formats
          // RP: IS this code robust when -o is not specified on the command line?
          for (mrf::uint i = 0; i < parser.getMultipleValue(ARG_LIST::OUTPUT).size(); i++)
          {
            mrf::util::StringParsing::fileNameFromAbsPathWithoutExtension(
                parser.getMultipleValue(ARG_LIST::OUTPUT)[i].c_str(),
                temp_file_name);
            mrf::util::StringParsing::getFileExtension(
                parser.getMultipleValue(ARG_LIST::OUTPUT)[i].c_str(),
                temp_file_extension);
            mrf::util::StringParsing::getDirectory(parser.getMultipleValue(ARG_LIST::OUTPUT)[i].c_str(), dir);
#else   //spectral rendering mode

        string const tmp = parser.getSingleValue(ARG_LIST::OUTPUT);
        mrf::util::StringParsing::fileNameFromAbsPathWithoutExtension(tmp.c_str(), temp_file_name);
        mrf::gui::fb::Loger::getInstance()->trace("Current output image filename is = ", temp_file_name);

        mrf::util::StringParsing::getDirectory(parser.getSingleValue(ARG_LIST::OUTPUT).c_str(), dir);
        mrf::util::StringParsing::getFileExtension(
            parser.getSingleValue(ARG_LIST::OUTPUT).c_str(),
            temp_file_extension);
#endif

            if (temp_file_extension == "")
            {
              mrf::gui::fb::Loger::getInstance()->warn(
                  "File extension not specified for output. Spectral with Envy Format assigned as default!");
              temp_file_extension = "hdr";
            }

            string samples_information = "";
            if (temp_file_extension == PNG_EXTENSION)
            {
              samples_information = "_" + std::to_string(_renderer->numSamplesPerPixel()) + "spp";
            }

            //string env_info = "_envPhi";
            //if(env_phi < 10) env_info += std::to_string(0);
            //if(env_phi < 100) env_info += std::to_string(0);
            //  env_info += std::to_string(env_phi);

            if (number_of_cameras > 1)
            {
              // Format the Ouptut filename
              unsigned int const numberOfDigits
                  = number_of_cameras ? static_cast<unsigned int>(std::log10(number_of_cameras)) + 1 : 1;
              std::stringstream ss;
              ss << std::setw(numberOfDigits + 1) << std::setfill('0') << cam_index;
              std::string const s = ss.str();

              temp_file_name = dir + ss.str() + "_" + temp_file_name + samples_information + "." + temp_file_extension;
            }
            else
            {
              temp_file_name = dir + temp_file_name + samples_information + "." + temp_file_extension;
            }

            mrf::gui::fb::Loger::getInstance()->trace("Current output image filename is = ", temp_file_name);

            std::string img_comments = "  Rendered from Camera_ID: " + cams[cam_index]->idName() + "\n";
            std::string comment_rendering_time
                = "  Total rendering time = " + std::to_string(total_rendering_time) + " s\n";
            _renderer->saveImage(temp_file_name, img_comments + comment_rendering_time);

#ifndef MRF_RENDERING_MODE_SPECTRAL
          }   //endfor multiple filenames RGB mode
#endif

          if (logfile_stream.is_open())
          {
            logfile_stream << parser.getSingleValue(ARG_LIST::SCENE) << ", " << std::to_string(total_rendering_time)
                           << "," << parser.getSingleValue(ARG_LIST::SAMPLES) << ","
                           << parser.getSingleValue(ARG_LIST::SPF) << std::endl;
          }

          std::cout << "Image saved to: " << temp_file_name << std::endl;
          std::cout << "Rendering Time: " << std::to_string(total_rendering_time) << " seconds" << std::endl;
        }

        _renderer->nextCamera();

      }   //for loop on cameras

      _renderer->shutdown();
      delete _renderer;
      _renderer = nullptr;

      if (logfile_stream.is_open())
      {
        logfile_stream.close();
      }


#ifdef RENDERER_INTERACTIVE
      if (_gui)
      {
        delete _gui;
      }
    }    // end of else for INTERACTIVE MODE
#endif   // RENDERER_INTERACTIVE
  }      // try - catch
  catch (optix::Exception &optix_exception)
  {
    mrf::gui::fb::Loger::getInstance()->fatal("Optix Exception CAUGHT !!! ");
    mrf::gui::fb::Loger::getInstance()->fatal(" Error Message: ", optix_exception.what());
    mrf::gui::fb::Loger::getInstance()->fatal(" ");
    mrf::gui::fb::Loger::getInstance()->fatal(
        " If your error is related to Optix Library that could not be loaded => DO YOU HAVE A NVIDIA CARD? ");
    mrf::gui::fb::Loger::getInstance()->fatal(" If you have a NVIDIA card check your driver version.");

#ifdef MALIA_FOR_KEPLER_ARCHITECTURE
    mrf::gui::fb::Loger::getInstance()->fatal(" This version of Malia was compiled with Optix backend KEPLER");
    mrf::gui::fb::Loger::getInstance()->fatal(" Minimum GPU architecture is KEPLER.");
    mrf::gui::fb::Loger::getInstance()->fatal(" Minimum driver versions are for Optix 5.1.1:");
    mrf::gui::fb::Loger::getInstance()->fatal("    - Windows : 396.65");
    mrf::gui::fb::Loger::getInstance()->fatal("    - Linux: 390.42");
#endif

#ifdef MALIA_FOR_MAXWELL_ARCHITECTURE
    mrf::gui::fb::Loger::getInstance()->fatal(" This version of Malia was compiled with Optix backend MAXWELL");
    mrf::gui::fb::Loger::getInstance()->fatal(" Minimum GPU architecture is MAXWELL.");
    mrf::gui::fb::Loger::getInstance()->fatal(
        " For Optix 6.0.0. Min GPU Architecture MAXWELL. Minimum driver versions are:");
    mrf::gui::fb::Loger::getInstance()->fatal("    - Windows: 418.81");
    mrf::gui::fb::Loger::getInstance()->fatal("    - Linux: 418.30");
    mrf::gui::fb::Loger::getInstance()->fatal(
        " For Optix 6.5.0. Min GPU Architecture MAXWELL. Minimum driver versions are:");
    mrf::gui::fb::Loger::getInstance()->fatal("    - Windows: 436.02");
    mrf::gui::fb::Loger::getInstance()->fatal("    - Linux: 435.17");
#endif

#ifdef MALIA_FOR_VOLTA_ARCHITECTURE
    mrf::gui::fb::Loger::getInstance()->fatal(" This version of Malia was compiled with Optix backend for VOLTA");
#endif

#ifdef MALIA_FOR_VOLTA_ARCHITECTURE
    mrf::gui::fb::Loger::getInstance()->fatal(" This version of Malia was compiled with Optix backend for TURING");
#endif

#ifdef MALIA_FOR_AMPERE_ARCHITECTURE
    mrf::gui::fb::Loger::getInstance()->fatal(" This version of Malia was compiled with Optix backend for AMPERE");
#endif

    return EXIT_FAILURE;
  }
  catch (std::exception &e)
  {
    mrf::gui::fb::Loger::getInstance()->fatal(" Error while Running Malia: " + string(e.what()));

    return EXIT_FAILURE;
  }


  return EXIT_SUCCESS;
}
