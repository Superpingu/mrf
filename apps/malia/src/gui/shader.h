#pragma once

#include <mrf_core_dll.hpp>

#include <iostream>
#include <map>

#ifndef __APPLE__
#  include <externals/GL/glew.h>
#endif
#include <GLFW/glfw3.h>

namespace mrf
{
namespace gui
{
enum ShaderObject
{
  VERTEX,
  FRAGMENT,
  COMPUTE
};

class Shader
{
public:
  Shader(): _programID(GL_INVALID_INDEX), _is_valid(false) {}

  /*!
   * Compiles and links the shader from 2 source files.
   * \param vtx_file vertex shader ("" if no vertex shader)
   * \param frag_file fragment shader ("" if no fragment shader)
   * \return true if no error occurs
   */
  bool loadFromFiles(const std::string &vtx_file, const std::string &frag_file);

  bool loadFromFile(ShaderObject obj, const std::string &obj_file);

  /*!
   * Compiles and links the shader from 2 strings.
   * \param vtx_src vertex shader ("" if no vertex shader)
   * \param frag_src fragment shader ("" if no fragment shader)
   * \return true if no error occurs
   */
  bool loadSources(const std::string &vtx_src, const std::string &frag_src);

  /*!
   * Enable the shader
   */
  void activate() const;

  /*!
   * Disable the shader
   */
  void deactivate() const;

  /*!
   * Simple Getter.
   *
   * \param name
   * \return the index of the uniform variable \a name
   */
  int getUniformLocation(const char *name) const;

  /*!
   * Simple Getter.
   *
   * \return the index of the generic attribute \a name
   */
  int getAttribLocation(const char *name) const;

  /*!
   * Simple Getter.
   *
   * \return the OpenGL object id of the GLSL program
   */
  int id() const { return _programID; }

  void setAdditionalHeader(ShaderObject obj, std::string header);
  bool createShaderProgram();

  /*!
   * Reloads and compiles the shader program from the previous vertex and fragment shader files.
   * \return true if no error occurs
   */
  bool reload();

protected:
  /*!
   * Log in the terminal any error about a program.
   *
   * /param objectID : ID of the program.
   */
  void printProgramInfoLog(GLuint objectID);

  /*!
   * Log in the terminal any error about a shader.
   *
   * /param objectID : ID of the shader.
   */
  void printShaderInfoLog(GLuint objectID);


  bool createShaderObject(ShaderObject obj, const std::string &obj_src);

protected:
  std::map<int, std::string> _obj_file;
  std::map<int, GLuint>      _obj_ID;
  std::map<int, bool>        _obj_compiled;

  //To be used only for dynamic layout command (e.g. "layout (binding = 0, rgba32f) ...). Content will be automatically added at the top of the shader, below #version.
  std::map<int, std::string> _obj_additional_header;

  GLuint _programID;
  bool   _is_valid;
};
}   // namespace gui
}   // namespace mrf