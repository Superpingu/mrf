#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//------------------------------------------------------------------------------
//
// IMGUI STUFF
//
//------------------------------------------------------------------------------


#include "mrf_baseUI.h"

namespace mrf
{
namespace gui
{
class InteractiveUI: public BaseUI
{
public:   //METHODS
  /*!
   *  Constructor.
   */
  InteractiveUI();

  /*!
   *  Initializes the GUI, its context and its parameters depending on the chosen backend.
   *  \param renderer : the rendering backend. Default will use tge MRF CPU backend (TBA).
   *  \param v_sync : enable vertical synchronization (true).
   */
  virtual void initializeUI(rendering::Renderer *renderer = nullptr, bool v_sync = true);

  /*!
   *  Starts ImGui and the rendering loop (see mainLoop()).
   */
  virtual void run();

  /*!
   * UI's panel width setter.
   * \param width.
   */
  virtual void setUIPanelWidth(unsigned int width) { _ui_panel_width = width; }
  /*!
   * Simple getter.
   * \return UI's panel width.
   */
  virtual unsigned int UIPanelwidth() const { return (unsigned int)_ui_panel_width; }

  /*!
   * Handles and propagates resize events to the backend.
   * \param w : the new width.
   * \param h : the new height.
   */
  virtual void resize(int w, int h, bool force = false);

  /*!
   * Handles and propagates mouse button events to the backend.
   * \param button : the GLFW button identifier that toggled the event.
   * \param action : the GLFW action identifier that toggled the event.
   * \param mods : the GLFW mods identifier. Unused but necessary for GLFW compatibility.
   */
  virtual void mousePress(int button, int action, int mods);

  /*!
   * Handles and propagates mouse movement events to the backend. Mouse movements done after a mouse click modify the camera position (left click) or the camera target (right click).
   * \param x : the new x position of the cursor.
   * \param y : the new y position of the cursor.
   */
  virtual void mouseMotion(double x, double y);

  /*!
   * Handles and propagates mouse scroll events to the backend. Horizontal scrolling is currently not supported. Vertical scrolling is used to control zoom parameters.
   * \param xoffset : x scrolling value. Unused but necessary for GLFW compatibility.
   * \param yoffset : y scrolling value.
   */
  virtual void mouseScroll(double xoffset, double yoffset);

  /*!
   * Handles and propagates keyboard characters events to the backend. The full list is presented by printInteractiveCommandHelp().
   * \param codepoint : the GLFW character identifier that toggled the event.
   */
  virtual void charPress(unsigned int codepoint);

  /*!
   * Handles and propagates keyboard non-character (e.g. escape or space bar) events to the backend. The full list is presented by printInteractiveCommandHelp().
   * \param key : the GLFW key identifier that toggled the event.
   * \param scancode : the GLFW character identifier. Unused but necessary for GLFW compatibility.
   * \param action : the GLFW character identifier that toggled the event.
   * \param mods : the GLFW mods identifier. Unused but necessary for GLFW compatibility.
   */
  virtual void keyboardPress(int key, int scancode, int action, int mods);

  /*!
   * Handles and propagates file drop events to the backend. Currently, only .msf files are handled. If multiple files are dropped, only the first valid one is processed.
   * \param count : the number of file dropped.
   * \param paths : the paths of the dropped files.
   */
  virtual void dropEvent(int count, const char **paths);

  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  virtual void resetCamera();
  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  virtual void previousCamera();
  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  virtual void nextCamera();

  /*!
   * Convenience functions.
   */
  virtual int height() const { return (int)std::max(_frame_height, _ui_min_height); }
  virtual int width() const { return (int)(_frame_width + _ui_panel_width); }

protected:   //METHODS
  /*!
   * Displays the resulting frame of the current backend. This is done using the OpenGL pipeline along with shaders to apply post-processes.
   */
  virtual void display();

  enum UI_MODE
  {
    INIT,
    RESET,
    RUN
  };

  /*!
   * Displays the resulting frame of the current backend. This is done using the OpenGL pipeline along with shaders to apply post-processes.
   */
  virtual void controlPanel(UI_MODE mode);

private:   //METHODS
  /*!
   * ImGui pop-up widget to export the current camera in a .mcf file, by specifying the full path.
   */
  void saveCamera();

  /*!
   * ImGui pop-up widget to save the current frame as an image, by specifying the full path. Possible format are hdr (spectral only), exr (rgb), jpg (rgb), png (rgb).
   */
  void saveImage();

  /*!
   * ImGui pop-up widget to load a scene by specifying the full path.
   */
  void openScene();

private:   //MEMBERS
  size_t _ui_panel_width;
  size_t _ui_max_width;
  size_t _ui_min_height;

  UI_MODE _current_mode;
};
}   // namespace gui
}   // namespace mrf