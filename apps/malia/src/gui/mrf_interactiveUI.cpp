#include "mrf_interactiveUI.h"
#include "gl_helper.h"

#include <mrf_core/util/precision_timer.hpp>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

namespace mrf
{
namespace gui
{
InteractiveUI::InteractiveUI(): BaseUI(), _ui_panel_width(384), _ui_min_height(128)
{
  _renderer   = nullptr;
  _clearColor = math::Vec4f(0.45f, 0.55f, 0.60f, 1.00f);
}

void InteractiveUI::initializeUI(rendering::Renderer *renderer, bool v_sync)
{
  //TODO later: if nullptr, create a full initialized renderer ? Or add a check to disable most events as long as no scene has been imported.
  if (renderer == nullptr)
    exit(-1);
  else
    _renderer = renderer;

  _frame_width  = (_renderer->hasScene()) ? _renderer->getWidth() : _ui_panel_width;
  _frame_height = (_renderer->hasScene()) ? std::max(_ui_min_height, _renderer->getHeight()) : _ui_min_height;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  _spectrum_size = static_cast<int>(_renderer->getSpectralWavelengths().size());
#else
  _spectrum_size = 1;
#endif

  initializeUIRenderingResources(v_sync);
}

void InteractiveUI::run()
{
  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO &io = ImGui::GetIO();
  (void)io;
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;      // Enable Keyboard Controls
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableSetMousePos;   // Enable Mouse Controls
  io.IniFilename = NULL;

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  //ImGui::StyleColorsClassic();
  ImGuiStyle *style                 = &ImGui::GetStyle();
  ImVec4 *    colors                = style->Colors;
  colors[ImGuiCol_Header]           = ImVec4(0.3f, 0.3f, 0.3f, 1.f);
  colors[ImGuiCol_HeaderHovered]    = ImVec4(0.2f, 0.2f, 0.2f, 1.f);
  colors[ImGuiCol_HeaderActive]     = ImVec4(0.5f, 0.5f, 0.5f, 1.f);
  colors[ImGuiCol_Button]           = ImVec4(0.45f * 0.8f, 0.55f * 0.8f, 0.60f * 0.8f, 1.f);
  colors[ImGuiCol_ButtonHovered]    = ImVec4(0.45f * 0.6f, 0.55f * 0.6f, 0.6f * 0.6f, 1.f);
  colors[ImGuiCol_ButtonActive]     = ImVec4(0.45f * 0.9f, 0.55f * 0.9f, 0.6f * 0.9f, 1.f);
  colors[ImGuiCol_SliderGrab]       = ImVec4(0.45f * 0.8f, 0.55f * 0.8f, 0.6f * 0.8f, 1.f);
  colors[ImGuiCol_SliderGrabActive] = ImVec4(0.45f * 0.6f, 0.55f * 0.6f, 0.6f * 0.6f, 1.f);
  colors[ImGuiCol_FrameBg]          = ImVec4(0.45f * 0.6f, 0.55f * 0.6f, 0.6f * 0.6f, 1.f);
  colors[ImGuiCol_PopupBg]          = ImVec4(0.45f * 0.6f, 0.55f * 0.6f, 0.6f * 0.6f, 1.f);

  // Setup Platform/Renderer bindings
  ImGui_ImplGlfw_InitForOpenGL(_window, true);
  ImGui_ImplOpenGL3_Init();

  _current_mode = INIT;

  mainLoop();

  // Cleanup
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();

  glfwDestroyWindow(_window);
  glfwTerminate();
}

void InteractiveUI::controlPanel(UI_MODE mode)
{
  if (mode == INIT || mode == RESET)
  {
    ImGui::SetNextWindowPos(ImVec2(static_cast<float>(_frame_width), 0.f));
    ImGui::SetNextWindowSize(ImVec2(static_cast<float>(_ui_panel_width), static_cast<float>(height())));
    ImGui::SetNextWindowCollapsed(false);
  }
  ImGui::Begin(
      "Settings",
      NULL,
      ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize
          | ImGuiWindowFlags_NoCollapse   //Don't use "no decoration" to keep scrollbar
          | ImGuiWindowFlags_NoBackground
          | ImGuiWindowFlags_NoMove);   // Create a window called "Settings" and append into it.

  if (_current_mode == RESET) _current_mode = RUN;

  if (mode == INIT) ImGui::SetNextTreeNodeOpen(true);
  if (ImGui::CollapsingHeader("Tone-mapping and display settings"))
  {
    float f = _renderer->getExposure();
    if (ImGui::SliderFloat("Exposure", &f, -10.0f, 10.0f))   // Edit 1 float using a slider from 0.0f to 10.0f
      _update_parameters[DISPLAY] = true;
    _renderer->setExposure(f);

    float g = _renderer->getGamma();
    if (ImGui::SliderFloat("Gamma", &g, 0.0f, 10.0f)) _update_parameters[DISPLAY] = true;
    _renderer->setGamma(g);

#ifdef MRF_RENDERING_MODE_SPECTRAL
    bool        update_spectral = false;
    const char *items_illum[]   = {"D50", "D65"};
    if (ImGui::Combo("Illuminant", &_current_illuminant, items_illum, IM_ARRAYSIZE(items_illum)))
    {
      update_spectral             = true;
      _update_parameters[PROCESS] = true;
    }

    //ImGui::SameLine();

    const char *items_curve[] = {"CIE_1931_2DEG", "CIE_XYZ_1964_10DEG"};
    if (ImGui::Combo("Sensitivity curve", &_current_sensitivity, items_curve, IM_ARRAYSIZE(items_curve)))
    {
      update_spectral             = true;
      _update_parameters[PROCESS] = true;
    }

    if (update_spectral)
    {
      initSensitivityBuffer();
      // initIlluminantBuffer();
      // computeSensitivityNorm();
    }

    const char *items_space[] = {"sRGB", "Adobe RGB"};
    if (ImGui::Combo("Color space", &_current_space, items_space, IM_ARRAYSIZE(items_space)))
    {
      update_spectral             = true;
      _update_parameters[DISPLAY] = true;
      _update_parameters[PROCESS] = true;
    }

    // if (ImGui::Checkbox("Integrate on full spectrum (1nm step)", &_full_spectrum_integration))
    // {
    //   _render_on_pause = true;
    //   _update_parameters = true;
    //   _shader_prog->reload();
    //   _render_on_pause = false;
    // }
#else
    const char *items_space[] = {"sRGB", "Gamma"};
    if (ImGui::Combo("Color space", &_current_space, items_space, IM_ARRAYSIZE(items_space)))
    {
      _update_parameters[DISPLAY] = true;
      _update_parameters[PROCESS] = true;
    }
#endif
  }

  ImGui::Separator();
  if (mode == INIT) ImGui::SetNextTreeNodeOpen(false);
  if (_renderer->hasScene())
  {
    if (ImGui::CollapsingHeader("Camera settings"))
    {
      mrf::rendering::Camera *   main_camera      = _renderer->getMainCamera();
      mrf::rendering::CameraType main_camera_type = main_camera->getType();

      ImGui::Text(
          "lookAt   : %f,%f,%f",
          main_camera->lookAt().x(),
          main_camera->lookAt().y(),
          main_camera->lookAt().z());
      ImGui::Text(
          "position : %f,%f,%f",
          main_camera->position().x(),
          main_camera->position().y(),
          main_camera->position().z());
      ImGui::Text("up       : %f,%f,%f", main_camera->up().x(), main_camera->up().y(), main_camera->up().z());

      ImGui::Separator();

      switch (main_camera_type)
      {
        case mrf::rendering::CameraType::Pinhole:
        {
          mrf::rendering::Pinhole *cam = dynamic_cast<mrf::rendering::Pinhole *>(main_camera);

          float fovy = cam->fovy();
          if (ImGui::SliderFloat("Vertical FOV", &fovy, 0., 180.))
          {
            _camera_changed = true;
            cam->fovy(fovy);
          }

          break;
        }
        case mrf::rendering::CameraType::ThinLens:
        {
          mrf::rendering::ThinLens *cam = dynamic_cast<mrf::rendering::ThinLens *>(main_camera);

          float fovy = cam->fovy();
          if (ImGui::SliderFloat("Vertical FOV", &fovy, 0., 180.))
          {
            _camera_changed = true;
            cam->fovy(fovy);
          }

          float aperture = cam->aperture();
          if (ImGui::SliderFloat("Aperture", &aperture, 0., 30.))
          {
            _camera_changed = true;
            cam->aperture(aperture);
          }

          float dist_to_focus = (float)cam->focalPlaneDistance();
          if (ImGui::SliderFloat("Distance to focus", &dist_to_focus, 0., 1000.))
          {
            _camera_changed = true;
            cam->focalPlaneDistance((double)dist_to_focus);
          }

          break;
        }
        case mrf::rendering::CameraType::Orthographic:
        {
          mrf::rendering::Orthographic *cam = dynamic_cast<mrf::rendering::Orthographic *>(main_camera);

          float ss[2];
          ss[0] = cam->sensor().size().x();
          ss[1] = cam->sensor().size().y();
          if (ImGui::SliderFloat2("Sensor size", ss, 0., 400.))
          {
            _camera_changed = true;
            cam->sensor().setSize(mrf::math::Vec2d(ss[0], ss[1]));
          }

          break;
        }
        case mrf::rendering::CameraType::Gonio:
        {
          break;
        }
        case mrf::rendering::CameraType::Environment:
        {
          mrf::rendering::Environment *cam = dynamic_cast<mrf::rendering::Environment *>(main_camera);

          float theta = cam->theta();
          if (ImGui::SliderFloat("Theta", &theta, 0., 3.141593))
          {
            _camera_changed = true;
            cam->theta(theta);
          }
          float phi = cam->phi();
          if (ImGui::SliderFloat("Phi", &phi, -3.141593, 3.141593))
          {
            _camera_changed = true;
            cam->phi(phi);
          }
          break;
        }
        default:
          break;
      }


      if (ImGui::Button("Make head straight (aligned with (0,1,0)"))
      {
        makeHeadStraight();
      }

      ImGui::Text(
          "Current camera: %li over %li",
          _renderer->getCurrentCameraIndex() + 1,
          _renderer->getAllCamera().size());
      if (ImGui::Button("Previous Camera"))
      {
        previousCamera();
        resizeAppWindow(int(_renderer->getWidth() + _ui_panel_width), int(_renderer->getHeight()));
      }
      ImGui::SameLine();
      if (ImGui::Button("Next Camera"))
      {
        nextCamera();
        resizeAppWindow(int(_renderer->getWidth() + _ui_panel_width), int(_renderer->getHeight()));
      }
      if (ImGui::Button("Save Camera"))
      {
        _render_on_pause = true;
        ImGui::OpenPopup("Camera file name");
      }
      ImGui::SameLine();
      if (ImGui::Button("Reset Camera"))
      {
        resetCamera();
        resizeAppWindow(int(_renderer->getWidth() + _ui_panel_width), int(_renderer->getHeight()));
      }
    }

    if (_renderer->hasEnvmap())
    {
      ImGui::Separator();
      if (mode == INIT) ImGui::SetNextTreeNodeOpen(false);
      if (ImGui::CollapsingHeader("Envmap settings"))
      {
        if (ImGui::Button("Envmap Phi +"))
        {
          mrf::gui::fb::Loger::getInstance()->info("envmap rot phi = ", _renderer->increaseRotationPhi(5.f));
          _camera_changed = true;
        }
        ImGui::SameLine();
        ImGui::Text("%f", _renderer->getRotationPhi());
        ImGui::SameLine();
        if (ImGui::Button("Envmap Phi -"))
        {
          mrf::gui::fb::Loger::getInstance()->info("envmap rot phi = ", _renderer->increaseRotationPhi(-5.f));
          _camera_changed = true;
        }

        if (ImGui::Button("Envmap Theta +"))
        {
          mrf::gui::fb::Loger::getInstance()->info("envmap rot theta = ", _renderer->increaseRotationTheta(5.f));
          _camera_changed = true;
        }
        ImGui::SameLine();
        ImGui::Text("%f", _renderer->getRotationTheta());
        ImGui::SameLine();
        if (ImGui::Button("Envmap Theta -"))
        {
          mrf::gui::fb::Loger::getInstance()->info("envmap rot theta = ", _renderer->increaseRotationTheta(-5.f));
          _camera_changed = true;
        }
      }
    }
  }

  ImGui::Separator();
  if (mode == INIT) ImGui::SetNextTreeNodeOpen(true);
  if (ImGui::CollapsingHeader("Utilities"))
  {
    if (_render_on_pause)
    {
      if (ImGui::Button("Resume Rendering"))
      {
        _render_on_pause = false;
      }
    }
    else
    {
      if (ImGui::Button("Pause Rendering"))
      {
        _render_on_pause = true;
      }
    }

    if (ImGui::Button("Save image"))
    {
      _render_on_pause = true;
      ImGui::OpenPopup("Image file name");
    }

    ImGui::SameLine();
    if (ImGui::Button("Load scene"))
    {
      _render_on_pause = true;
      ImGui::OpenPopup("Scene file name");
    }

    if (ImGui::Button("Reload OptiX kernels"))
    {
      mrf::gui::fb::Loger::getInstance()->warn("TBA in futur version !");
    }
    ImGui::SameLine();
    if (ImGui::Button("Reload GLSL shaders"))
    {
      _render_on_pause = true;
      for (std::pair<int, Shader *> prog : _shader_prog)
      {
        prog.second->reload();
      }
      _update_parameters[DISPLAY] = true;
      _update_parameters[PROCESS] = true;
      _render_on_pause            = false;
    }
  }

  ImGui::Separator();
  if (mode == INIT) ImGui::SetNextTreeNodeOpen(true);
  if (ImGui::CollapsingHeader("Rendering Info"))
  {
    ImGui::Text("Current Camera Sensor Resolution: %li x %li pixels", _renderer->getWidth(), _renderer->getHeight());
    ImGui::Text("Rays per pixel: %.2f", 1.f / (_renderer->getSpatialFactor() * _renderer->getSpatialFactor()));
    //ImGui::Text(
    //    "Samples computed per frame: %u",
    //    _renderer->numSamplesPerFrame() * _renderer->getWidth() * _renderer->getHeight()
    //        / (_renderer->getSpatialFactor() * _renderer->getSpatialFactor()));
    //ImGui::Text("Rendered %u samples", _renderer->numSamplesPerFrame() * _renderer->getWidth() * _renderer->getHeight() * _renderer->getNumFrame());
    //float const local_spp_for_display = static_cast<float>(_renderer->numSamplesPerFrame()) * _renderer->getNumFrame();
    //ImGui::Text(
    //    "Rendered %lu samples per pixel",
    //    static_cast<unsigned long>(std::round(local_spp_for_display)));
    ImGui::Text("Rendered %.1lf samples per pixel", _sample_count);
    ImGui::Text("Max path length: %i", _renderer->getMaxPathLength());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    if (_renderer->getNbMaxWavelengthsPerPass() > 1)
      ImGui::Text(
          "Rendering %li wavelengths at %li wavelengths per pass.",
          _renderer->getNbWavelengths(),
          _renderer->getNbMaxWavelengthsPerPass());
    else
      ImGui::Text(
          "Rendering %li wavelengths at %li wavelength per pass.",
          _renderer->getNbWavelengths(),
          _renderer->getNbMaxWavelengthsPerPass());
    uint const start = _renderer->getSpectralWavelengths()[0];
    uint const end   = _renderer->getSpectralWavelengths().back();
    uint const step  = _renderer->getSpectralWavelengths()[1] - start;
    ImGui::Text("Wavelength range: first %u, last %u, step %u.", start, end, step);
#endif

    ImGui::Text("Rendering time: %.3f", _total_rendering_time);
    //ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / _samples_per_second, _samples_per_second);
    ImGui::Text(
        "Application average %.3f ms/frame (%.1f FPS)",
        1000.0f / ImGui::GetIO().Framerate,
        ImGui::GetIO().Framerate);

    ImGui::Checkbox("Optimize FPS", &_optimize_FPS);
    if (_optimize_FPS) ImGui::SliderInt("Target FPS", &_target_FPS, 10, 60);


    //ImGui::Text("Factor: %i", _renderer->getSpatialFactor());
    //if (ImGui::Button("Factor -"))
    //{
    //  if (_renderer->getSpatialFactor() > 1) _renderer->setSpatialFactor(_renderer->getSpatialFactor() - 1);
    //  _update_parameters[PROCESS] = true;
    //}
    //ImGui::SameLine();
    //if (ImGui::Button("Factor +"))
    //{
    //  if (_renderer->getSpatialFactor() < 4) _renderer->setSpatialFactor(_renderer->getSpatialFactor() + 1);
    //  _update_parameters[PROCESS] = true;
    //}
  }

  saveCamera();
  saveImage();
  openScene();

  ImGui::End();
}

void InteractiveUI::display()
{
  // Start the Dear ImGui frame
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  ImGui::NewFrame();
  controlPanel(_current_mode);
  ImGui::EndFrame();

  // Rendering
  ImGui::Render();

  //ImGui windows have been set and the command passed, we can now do our custom rendering.
  //First resize viewport to fit the texture.
  glViewport(0, 0, GLsizei(_frame_width), GLsizei(height()));
  //Use parent method to display the texture.
  BaseUI::display();
  //Then resize viewport to full window size.
  GLsizei w = GLsizei(width());
  GLsizei h = GLsizei(height());
  glViewport(0, 0, w, h);

  //Our rendering have been done, now ImGui will execute its drawing command (must be done in this order to ensure correct behavior of popups).
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

  if (_current_mode == INIT) _current_mode = RUN;
}

void InteractiveUI::mousePress(int button, int action, int mods)
{
  ImGuiIO &io = ImGui::GetIO();

  if (!io.WantCaptureMouse)   // Avoid passing event to the application when a ImGui widget is active
    BaseUI::mousePress(button, action, mods);
}

void InteractiveUI::mouseMotion(double x, double y)
{
  ImGuiIO &io = ImGui::GetIO();

  if (!io.WantCaptureMouse)   // Avoid passing moving events to the application when an ImGui widget is active
    BaseUI::mouseMotion((double)x, (double)y);
}

void InteractiveUI::mouseScroll(double xoffset, double yoffset)
{
  ImGuiIO &io = ImGui::GetIO();

  if (!io.WantCaptureMouse)   // Avoid passing moving events to the application when an ImGui widget is active
    BaseUI::mouseScroll(xoffset, yoffset);
}

void InteractiveUI::charPress(unsigned int codepoint)
{
  ImGuiIO &io = ImGui::GetIO();

  if (!io.WantTextInput)   // Avoid passing keyboard events to the application when a text input is expected by ImGui.
    BaseUI::charPress(codepoint);
}

void InteractiveUI::keyboardPress(int key, int scancode, int action, int mods)
{
  ImGuiIO &io = ImGui::GetIO();

  if (!io.WantTextInput)   // Avoid passing keyboard events to the application when a text input is expected by ImGui.
    BaseUI::keyboardPress(key, scancode, action, mods);
}

void InteractiveUI::resize(int w, int h, bool force)
{
  if (w == (int)width() && h == (int)height() && !force) return;
  bool force_resize = false;

  _camera_changed = true;

  if (w > _ui_panel_width)
    _frame_width = (size_t)w - _ui_panel_width;
  else
  {
    _frame_width = 1llu;
    force_resize = true;
  }
  if (h >= _ui_min_height)
    _frame_height = h;
  else
  {
    _frame_height = _ui_min_height;
    force_resize  = true;
  }

  if (force_resize)
  {
    mrf::gui::fb::Loger::getInstance()->warn("You hit the minimal windows size allowed for Malia.");
    resizeAppWindow(width(), height());
  }

  mrf::gui::fb::Loger::getInstance()->debug(
      "W: " + std::to_string(_ui_panel_width) + " | " + std::to_string(_frame_width) + " | " + std::to_string(w));
  mrf::gui::fb::Loger::getInstance()->debug(
      "H: " + std::to_string(_ui_min_height) + " | " + std::to_string(_frame_height) + " | " + std::to_string(h));

  _renderer->getMainCamera()->aspectRatio() = static_cast<float>(_frame_width) / static_cast<float>(_frame_height);
  _renderer->getMainCamera()->sensor().setResolution(_frame_width, _frame_height);

  _renderer->readyBufferForUpdate(
      _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
      _texture_list[TEXTURE::OUTPUT_BUFFER].getName());
  _texture_list[TEXTURE::OUTPUT_BUFFER].resize(_frame_width, _frame_height, _spectrum_size);
  _renderer->resizeRenderTarget(_frame_width, _frame_height);
  _renderer->readyBufferForRendering(
      _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
      _texture_list[TEXTURE::OUTPUT_BUFFER].getName());

  _texture_list[TEXTURE::PROCESS_BUFFER].resize(_frame_width, _frame_height, 1);

  glViewport(0, 0, GLsizei(width()), GLsizei(height()));
  _current_mode               = RESET;
  _update_parameters[PROCESS] = true;
  _update_parameters[DISPLAY] = true;
}

void InteractiveUI::resetCamera()
{
  BaseUI::resetCamera();
  _current_mode = RESET;
}

void InteractiveUI::previousCamera()
{
  BaseUI::previousCamera();
  _current_mode = RESET;
}

void InteractiveUI::nextCamera()
{
  BaseUI::nextCamera();
  _current_mode = RESET;
}

void InteractiveUI::dropEvent(int count, const char **paths)
{
  BaseUI::dropEvent(count, paths);
  _current_mode = RESET;
}

/**************
** GUI POP-UP FUNCTIONS
**************/

void InteractiveUI::openScene()
{
  static std::string s_temp_scene_file = _scene_file;
  if (ImGui::BeginPopupModal(
          "Scene file name",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Input name:");
    ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.45f * 0.8f, 0.55f * 0.8f, 0.6f * 0.8f, 1.f));
    std::string input_text_scene;
    input_text_scene.assign(s_temp_scene_file.begin(), s_temp_scene_file.end());
    input_text_scene.resize(256);
    ImGui::InputText("##edit", (char *)input_text_scene.c_str(), input_text_scene.capacity() + 1);

    if (ImGui::IsItemActivated()) ImGui::SetKeyboardFocusHere(-1);

    ImGui::PopStyleColor();
    s_temp_scene_file.assign(input_text_scene.begin(), input_text_scene.end());
    if (input_text_scene.size() == 0)
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("No name");
    }
    else
    {
      bool close = ImGui::Button("Open");
      ImGui::SameLine();
      bool cancel = ImGui::Button("Cancel");
      if (close)
      {
        ImGui::CloseCurrentPopup();
        ImGui::EndPopup();
        ImGui::OpenPopup("Opening scene");
      }
      else if (cancel)
      {
        ImGui::CloseCurrentPopup();
        ImGui::EndPopup();
      }
      else
      {
        ImGui::EndPopup();
      }
    }
  }
  if (ImGui::BeginPopupModal(
          "Opening scene",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Opening scene.");
    if (checkScene(s_temp_scene_file))
    {
      ImGui::CloseCurrentPopup();
      resizeAppWindow(width(), height());
      ImGui::EndPopup();
      ImGui::OpenPopup("Scene success");
    }
    else
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("Scene failed");
    }
  }
  if (ImGui::BeginPopupModal(
          "Scene success",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Opened scene successfully.");
    if (ImGui::Button("Close", ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
      _render_on_pause = false;
    }
    ImGui::EndPopup();
  }
  if (ImGui::BeginPopupModal(
          "Scene failed",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Failed to open scene.");
    if (ImGui::Button("Close", ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
      _render_on_pause = false;
    }
    ImGui::EndPopup();
  }
}

void InteractiveUI::saveCamera()
{
  if (ImGui::BeginPopupModal(
          "Camera file name",
          NULL,
          ImGuiInputTextFlags_EnterReturnsTrue | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration
              | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Output name:");
    ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.45f * 0.8f, 0.55f * 0.8f, 0.6f * 0.8f, 1.f));
    std::string input_text_cam;
    input_text_cam.assign(_camera_output_file.begin(), _camera_output_file.end());
    input_text_cam.resize(256);
    ImGui::InputText("##edit", (char *)input_text_cam.c_str(), input_text_cam.capacity() + 1);

    if (ImGui::IsItemActivated()) ImGui::SetKeyboardFocusHere(-1);

    ImGui::PopStyleColor();
    _camera_output_file.assign(input_text_cam);
    if (_camera_output_file.size() == 0)
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("No name");
    }
    else
    {
      bool close = ImGui::Button("Save");
      ImGui::SameLine();
      bool cancel = ImGui::Button("Cancel");
      if (close)
      {
        ImGui::CloseCurrentPopup();
        ImGui::EndPopup();
        ImGui::OpenPopup("Saving camera");
        //_image_output_file = std::string(name);
      }
      else if (cancel)
      {
        ImGui::CloseCurrentPopup();
        ImGui::EndPopup();
      }
      else
      {
        ImGui::EndPopup();
      }
    }
  }
  if (ImGui::BeginPopupModal(
          "Saving camera",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Saving camera.");
    if (saveCameraToFile())
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("Camera success");
    }
    else
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("Camera failed");
    }
  }
  if (ImGui::BeginPopupModal(
          "Camera success",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Saved camera successfully.");
    if (ImGui::Button("Close", ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
  if (ImGui::BeginPopupModal(
          "Camera failed",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Failed to save camera.");
    if (ImGui::Button("Close", ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
}

void InteractiveUI::saveImage()
{
  if (ImGui::BeginPopupModal(
          "Image file name",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    //char* name = (char*)_image_output_file.c_str();
    ImGui::Text("Output name:");
    ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.45f * 0.8f, 0.55f * 0.8f, 0.6f * 0.8f, 1.f));
    std::string input_text_image;
    input_text_image.assign(_image_output_file.begin(), _image_output_file.end());
    input_text_image.resize(256);
    bool close = ImGui::InputText(
        "##edit",
        (char *)input_text_image.c_str(),
        input_text_image.size() + 1,
        ImGuiInputTextFlags_EnterReturnsTrue);
#ifdef MRF_RENDERING_MODE_SPECTRAL
    ImGui::Text("Note: To save a spectral image, please use the .hdr extension (ENVI format).");
    ImGui::Text("Any other extension will save an RGB image.");
#endif

    ImGui::PopStyleColor();

    _image_output_file.assign(input_text_image);
    if (_image_output_file.size() == 0)
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("No name");
    }
    else
    {
      close = close | ImGui::Button("Save");
      ImGui::SameLine();
      bool cancel = ImGui::Button("Cancel");
      if (close)
      {
        ImGui::CloseCurrentPopup();
        ImGui::EndPopup();
        if (_renderer->readyBufferForUpdate(_texture_list[TEXTURE::OUTPUT_BUFFER].getID(), "output_buffer"))
        {
          ImGui::OpenPopup("Saving in process");
          _renderer->readyBufferForRendering(_texture_list[TEXTURE::OUTPUT_BUFFER].getID(), "output_buffer");
        }
        else
          ImGui::OpenPopup("Saving failed");
      }
      else if (cancel)
      {
        ImGui::CloseCurrentPopup();
        ImGui::EndPopup();
      }
      else
      {
        ImGui::EndPopup();
      }
    }
  }
  if (ImGui::BeginPopupModal(
          "Saving in process",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Saving image: %s", _image_output_file.c_str());
    if (_renderer->saveImage(_image_output_file))
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("Saving done");
    }
    else
    {
      ImGui::CloseCurrentPopup();
      ImGui::EndPopup();
      ImGui::OpenPopup("Saving failed");
    }
  }
  if (ImGui::BeginPopupModal(
          "No name",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("No name specified to save image, saving aborted.");
    if (ImGui::Button("Close", ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
  if (ImGui::BeginPopupModal(
          "Saving done",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Saved image: %s successfully.", _image_output_file.c_str());
    if (ImGui::Button("Close", ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
  if (ImGui::BeginPopupModal(
          "Saving failed",
          NULL,
          ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove))
  {
    ImGui::Text("Failed to save image: %s", _image_output_file.c_str());
    if (ImGui::Button("Close", ImVec2(120, 0)))
    {
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
}
}   // namespace gui
}   // namespace mrf