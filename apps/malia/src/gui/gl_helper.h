#include <iostream>

#ifndef __APPLE__
#  include <externals/GL/glew.h>
#endif
#include <GLFW/glfw3.h>

/** Log OpenGL errors on stderr */
//TODO log in loger ?
inline void _check_gl_error(const char *file, int line)
{
  GLenum error = glGetError();
  if (error != GL_NO_ERROR)
  {
    std::cout << "error: " << error << " in " << file << " at l." << line << std::endl;
    if (error == GL_OUT_OF_MEMORY)
    {
      std::cerr << "Not enough GPU memory, aborting. Please use either a smaller image or less wavelengths."
                << std::endl;
      abort();
    }
  }
}
#define checkErrorGL() _check_gl_error(__FILE__, __LINE__)
