#include "texture.h"
#include "gl_helper.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>

namespace mrf
{
namespace gui
{

static int nextFreeUnit = 0;

Texture::Texture()
  : _textureType(GL_TEXTURE_2D)
  , _textureID(GL_INVALID_INDEX)
  , _uniformName("")
  , _width(1)
  , _height(1)
  , _depth(1)
{
  _textureUnit = nextFreeUnit++;
  _hasData     = false;

  // _textureType = GL_TEXTURE_2D;
  _textureFormat = GL_RGBA32F_ARB;
  _pixelFormat   = GL_RGBA;
  _dataType      = GL_FLOAT;
}

Texture::Texture(size_t width, GLenum format, const char *name, bool use_buffer, void *data)
  : _textureType(GL_TEXTURE_1D)
  , _textureID(GL_INVALID_INDEX)
  , _uniformName(name)
  , _width(width)
  , _height(1)
  , _depth(1)
{
  glFormat(format);
  _textureUnit = nextFreeUnit++;

  glGenTextures(1, &_textureID);
  glBindTexture(_textureType, _textureID);

  glTexParameteri(_textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(_textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glTexParameteri(_textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glBindTexture(_textureType, 0);

  if (use_buffer)
  {
    _internal_buffer = GLBuffer(_width, _height, _depth, _textureFormat);
    updateTextureFromInternalBuffer();
  }
  else
  {
    glBindTexture(_textureType, _textureID);
    glTexImage1D(_textureType, 0, _textureFormat, GLsizei(_width), 0, _pixelFormat, _dataType, data);
    glBindTexture(_textureType, 0);
  }

  checkErrorGL();
}

Texture::Texture(size_t width, size_t height, GLenum format, const char *name, bool use_buffer, void *data)
  : _textureType(GL_TEXTURE_2D)
  , _textureID(GL_INVALID_INDEX)
  , _uniformName(name)
  , _width(width)
  , _height(height)
  , _depth(1)
{
  glFormat(format);
  _textureUnit = nextFreeUnit++;

  glGenTextures(1, &_textureID);
  glBindTexture(_textureType, _textureID);

  glTexParameteri(_textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(_textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glTexParameteri(_textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glBindTexture(_textureType, 0);

  if (use_buffer)
  {
    _internal_buffer = GLBuffer(_width, _height, _depth, _textureFormat);
    updateTextureFromInternalBuffer();
  }
  else
  {
    glBindTexture(_textureType, _textureID);
    glTexImage2D(_textureType, 0, _textureFormat, GLsizei(_width), GLsizei(_height), 0, _pixelFormat, _dataType, data);
    glBindTexture(_textureType, 0);
  }

  checkErrorGL();
}

Texture::Texture(
    size_t      width,
    size_t      height,
    size_t      depth,
    GLenum      format,
    const char *name,
    bool        use_buffer,
    void *      data)
  : _textureType(GL_TEXTURE_3D)
  , _textureID(GL_INVALID_INDEX)
  , _uniformName(name)
  , _width(width)
  , _height(height)
  , _depth(depth)
{
  glFormat(format);
  _textureUnit = nextFreeUnit++;

  glGenTextures(1, &_textureID);
  glBindTexture(_textureType, _textureID);

  glTexParameteri(_textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(_textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glTexParameteri(_textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glBindTexture(_textureType, 0);

  if (use_buffer)
  {
    _internal_buffer = GLBuffer(_width, _height, _depth, _textureFormat);
    updateTextureFromInternalBuffer();
  }
  else
  {
    glBindTexture(_textureType, _textureID);
    glTexImage3D(_textureType, 0, _textureFormat, GLsizei(_width), GLsizei(_height), GLsizei(_depth), 0, _pixelFormat, _dataType, data);
    glBindTexture(_textureType, 0);
  }

  checkErrorGL();
}

void Texture::updateTexture(size_t width, GLenum format, bool use_buffer, void *data)
{
  _width  = width;
  _height = 0;
  _depth  = 0;
  glFormat(format);

  //glDeleteTextures(1, &_textureID);
  //glGenTextures(1, &_textureID);
  glBindTexture(_textureType, _textureID);

  glTexParameteri(_textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(_textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glTexParameteri(_textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glBindTexture(_textureType, 0);

  if (use_buffer)
  {
    _internal_buffer = GLBuffer(_width, _height, _depth, _textureFormat);
    updateTextureFromInternalBuffer();
  }
  else
  {
    glBindTexture(_textureType, _textureID);
    glTexImage1D(_textureType, 0, _textureFormat, GLsizei(_width), 0, _pixelFormat, _dataType, data);
    glBindTexture(_textureType, 0);
  }

  checkErrorGL();
}

void Texture::updateTexture(size_t width, size_t height, GLenum format, bool use_buffer, void *data)
{
  _width  = width;
  _height = height;
  _depth  = 0;
  glFormat(format);

  //glDeleteTextures(1, &_textureID);
  //glGenTextures(1, &_textureID);
  glBindTexture(_textureType, _textureID);

  glTexParameteri(_textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(_textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glTexParameteri(_textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glBindTexture(_textureType, 0);

  if (use_buffer)
  {
    _internal_buffer = GLBuffer(_width, _height, _depth, _textureFormat);
    updateTextureFromInternalBuffer();
  }
  else
  {
    glBindTexture(_textureType, _textureID);
    glTexImage2D(_textureType, 0, _textureFormat, GLsizei(_width), GLsizei(_height), 0, _pixelFormat, _dataType, data);
    glBindTexture(_textureType, 0);
  }

  checkErrorGL();
}

void Texture::updateTexture(size_t width, size_t height, size_t depth, GLenum format, bool use_buffer, void *data)
{
  _width  = width;
  _height = height;
  _depth  = depth;
  glFormat(format);

  //glDeleteTextures(1, &_textureID);
  //glGenTextures(1, &_textureID);
  glBindTexture(_textureType, _textureID);

  glTexParameteri(_textureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(_textureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glTexParameteri(_textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glBindTexture(_textureType, 0);

  if (use_buffer)
  {
    _internal_buffer = GLBuffer(_width, _height, _depth, _textureFormat);
    updateTextureFromInternalBuffer();
  }
  else
  {
    glBindTexture(_textureType, _textureID);
    glTexImage3D(_textureType, 0, _textureFormat, GLsizei(_width), GLsizei(_height), GLsizei(_depth), 0, _pixelFormat, _dataType, data);
    glBindTexture(_textureType, 0);
  }

  checkErrorGL();
}

Texture::~Texture() {}

void Texture::glFormat(GLenum format)
{
  switch (format)
  {
    case GL_R32F:
    {
      _textureFormat = GL_R32F;
      _pixelFormat   = GL_RED;
      _dataType      = GL_FLOAT;
      break;
    }
    case GL_RG32F:
    {
      _textureFormat = GL_RG32F;
      _pixelFormat   = GL_RG;
      _dataType      = GL_FLOAT;
      break;
    }
    case GL_RGB32F_ARB:
    {
      _textureFormat = GL_RGB32F_ARB;
      _pixelFormat   = GL_RGB;
      _dataType      = GL_FLOAT;
      break;
    }
    case GL_RGBA32F_ARB:
    default:
    {
      _textureFormat = GL_RGBA32F_ARB;
      _pixelFormat   = GL_RGBA;
      _dataType      = GL_FLOAT;
      break;
    }
  }
}

void Texture::activate(Shader *prg, std::string uniform_name)
{
  glActiveTexture(GL_TEXTURE0 + _textureUnit);
  glBindTexture(_textureType, _textureID);
  if (uniform_name.size() == 0)
    glUniform1i(prg->getUniformLocation(_uniformName), _textureUnit);
  else
    glUniform1i(prg->getUniformLocation(uniform_name.c_str()), _textureUnit);
  checkErrorGL();
}

void Texture::activateImage()
{
  checkErrorGL();
  glBindImageTexture(_textureUnit, _textureID, 0, true, 0, GL_READ_WRITE, GL_RGBA32F);
  checkErrorGL();
}

void Texture::deactivate()
{
  glActiveTexture(GL_TEXTURE0 + _textureUnit);
  glBindTexture(_textureType, 0);
  checkErrorGL();
}

void Texture::deactivateImage()
{
  checkErrorGL();
  glBindImageTexture(_textureUnit, 0, 0, true, 0, GL_READ_WRITE, GL_RGBA32F);
  checkErrorGL();
}

bool Texture::initTextureFromImage(image::ColorImage const &image, const char *name)
{
  _uniformName = name;
  _width       = image.width();
  _height      = image.height();

  _textureFormat = GL_RGB32F_ARB;
  _dataType      = GL_FLOAT;
  _pixelFormat   = GL_RGB;

  glGenTextures(1, &_textureID);
  glBindTexture(_textureType, _textureID);

  // Change these to GL_LINEAR for super- or sub-sampling
  glTexParameteri(_textureType, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(_textureType, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  // GL_CLAMP_TO_BORDER for linear filtering, not relevant for nearest.
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(_textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glBindTexture(_textureType, _textureID);
  glTexImage2D(_textureType, 0, _textureFormat, GLsizei(_width), GLsizei(_height), 0, _pixelFormat, _dataType, image.ptr());

  glBindTexture(_textureType, 0);

  checkErrorGL();
  return (glGetError() == GL_NO_ERROR);
}

void Texture::resize(size_t width, size_t height, size_t depth)
{
  _width  = width;
  _height = height;
  _depth  = depth;
  if (_internal_buffer.getID() != GL_INVALID_INDEX) _internal_buffer.resizeBuffer(width, height, depth);
  else
  {
    if (_height * _depth == 1) updateTexture(_width, _textureFormat, false);
    else if (_depth == 1) updateTexture(_width, _height, _textureFormat, false, nullptr);
    else updateTexture(_width, _height, _depth, _textureFormat, false, nullptr);
  }
}

void Texture::updateTextureFromInternalBuffer()
{
  glBindTexture(_textureType, _textureID);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _internal_buffer.getID());

  const size_t elmt_size = _internal_buffer.getElementSize();
  if (elmt_size % 8 == 0)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 8);
  else if (elmt_size % 4 == 0)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
  else if (elmt_size % 2 == 0)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
  else
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  if (_textureType == GL_TEXTURE_1D)
    glTexImage1D(_textureType, 0, _textureFormat, GLsizei(_width), 0, _pixelFormat, _dataType, 0);
  else if (_textureType == GL_TEXTURE_2D)
    glTexImage2D(_textureType, 0, _textureFormat, GLsizei(_width), GLsizei(_height), 0, _pixelFormat, _dataType, 0);
  else if (_textureType == GL_TEXTURE_2D_ARRAY || _textureType == GL_TEXTURE_3D)
    glTexImage3D(
        _textureType,
        0,
        _textureFormat,
        GLsizei(_width),
        GLsizei(_height),
        GLsizei(_depth),
        0,
        _pixelFormat,
        _dataType,
        0);

  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
  glBindTexture(_textureType, 0);

  checkErrorGL();
}

void Texture::displayTextureDirectly()
{
  if (_internal_buffer.getID() != 0) updateTextureFromInternalBuffer();

  glBindTexture(_textureType, _textureID);

  glEnable(_textureType);

  glBegin(GL_QUADS);
  glTexCoord2f(0.0f, 0.0f);
  glVertex2f(0.0f, 0.0f);

  glTexCoord2f(1.0f, 0.0f);
  glVertex2f(1.0f, 0.0f);

  glTexCoord2f(1.0f, 1.0f);
  glVertex2f(1.0f, 1.0f);

  glTexCoord2f(0.0f, 1.0f);
  glVertex2f(0.0f, 1.0f);
  glEnd();

  glDisable(_textureType);

  glBindTexture(_textureType, 0);

  checkErrorGL();
}
}   // namespace gui
}   // namespace mrf
