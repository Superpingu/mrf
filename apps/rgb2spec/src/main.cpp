/**
 * @file main.cpp
 * @author Romain Pacanowski
 * @brief
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright CNRS (c) 2018
 *
 */


#include <iostream>
#include <cstdlib>
#include <exception>



//MRF
#include <mrf_core/feedback/loger.hpp>
#include <mrf_core/math/vec3.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/util/prog_options.hpp>

#include "rgb2spec.hpp"

static float const COLOR_EPSILON = 0.00001f;

//----- GLOBAL VARIABLES HERE -----//
#ifdef RGB2SPEC_DEBUG
mrf::gui::fb::Loger *general_loger = mrf::gui::fb::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Trace);
#else
mrf::gui::fb::Loger *general_loger = mrf::gui::fb::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Info);
#endif

//mrf::util::ProgOptions rgb2spec_options("rgb2spec");
//----- END OF GLOBAL VARAIBLES -----//


void retrieve_color_from_cmdline(mrf::util::ProgOptions rgb2spec_options, mrf::math::Vec3f &rgb_colors)
{
  rgb2spec_options.option("in_color").paramValue(0, rgb_colors(0));
  rgb2spec_options.option("in_color").paramValue(1, rgb_colors(1));
  rgb2spec_options.option("in_color").paramValue(2, rgb_colors(2));

  general_loger->trace("Rgb colors retrieved: ", rgb_colors);
}

void retrieve_filename_from_cmdline(mrf::util::ProgOptions rgb2spec_options, std::string &filename)
{
  rgb2spec_options.option("in_file").paramValue(0, filename);
  general_loger->trace("Filename retrieved: ", filename);
}

void retrieve_nb_wavelength_from_cmdline(mrf::util::ProgOptions rgb2spec_options, unsigned int &nbw)
{
  rgb2spec_options.option("nbw").paramValue(0, nbw);
  general_loger->trace("Number of Wavelengths Requested: ", nbw);
}

void process_command_line_options(mrf::util::ProgOptions& rgb2spec_options, int argc, char **argv)
{
  using namespace std;
  using namespace mrf::util;



  rgb2spec_options.addOption("in_color", ProgArg(false, "{float} Red Green Blue values ", 3));
  rgb2spec_options.addOption("in_file", ProgArg(false, "{string} filename to a LDR or HDR image", 1));
  rgb2spec_options.addOption(
      "out",
      ProgArg(false, "{string} filename where the spectrum will be saved. Default is out.spd", 1));
  rgb2spec_options.addOption(
      "xyz_curves",
      ProgArg(false, "{string: CIE_1931_2DEG || CIE_XYZ_1964_10DEG} Type of XYZ sensitivity curves.", 1));
  rgb2spec_options.addOption(
      "nbw",
      ProgArg(
          false,
          "{uint : number (between 3 and and 461) of wavelengths for the spectrum defined between 360 and 430nm. Works only for Image",
          1));

  //rgb2spec_options.addOption("reflectivity", ProgArg(false, "{string} Illuminant Name. Can be E, D65 or D50. E by default. Forces  the Spectrum to be a Reflectance Spectrum under the given illuminant. All values should be between 0 and 1", 1));
  rgb2spec_options.addOption(
      "reflectivity",
      ProgArg(
          false,
          "{string} Illuminant Name. Only E (=1 everywhere) support for now). All values should be between 0 and 1",
          1));

  // GET SOME HELP!
  // rgb2spec_options.addOption("h", ProgArg(false, "Print this help message", 0));
  // rgb2spec_options.addOption("help", ProgArg(false, "Print this help message", 0));

  if (!mrf::util::validateProgramOptions(argc, argv, rgb2spec_options))
  {
    general_loger->fatal(" Bad options");
    throw runtime_error("command line options : bad options error");
  }

  if (!rgb2spec_options.hasOption("in_color") && !rgb2spec_options.hasOption("in_file")
      && !rgb2spec_options.hasOption("h") && !rgb2spec_options.hasOption("help"))
  {
    general_loger->fatal(
        "One mandatory option was not provided. Use -in_color or -in_file to specify what must be converted!");
    throw runtime_error("command line options : mandatory options");
  }
}


int main(int argc, char **argv)
{
  using namespace std;


  general_loger->info("STARTING");

  mrf::util::ProgOptions rgb2spec_options("rgb2spec");

  try
  {
    process_command_line_options(rgb2spec_options, argc, argv);

    if ((rgb2spec_options.hasOption("h") && rgb2spec_options.option("h").wasProvided())
        || (rgb2spec_options.hasOption("help") && rgb2spec_options.option("help").wasProvided()))
    {
      rgb2spec_options.displayHelp();
      return EXIT_SUCCESS;
    }

    string output_filename = "out.spd";


    if (rgb2spec_options.option("out").wasProvided())
    {
      rgb2spec_options.option("out").paramValue(0, output_filename);
      general_loger->info(" Spectrum file will be saved to ", output_filename);
    }

    mrf::color::SensitivityCurveName curve_specs = mrf::color::CIE_1931_2DEG;
    if (rgb2spec_options.option("xyz_curves").wasProvided())
    {
      string xyz_curve_str;
      rgb2spec_options.option("xyz_curves").paramValue(0, xyz_curve_str);
      general_loger->info(" XYZ Sensitivity curves used: ", xyz_curve_str);


      if (xyz_curve_str == "CIE_1931_2DEG")
      {
        curve_specs = mrf::color::CIE_1931_2DEG;
      }
      else if (xyz_curve_str == "CIE_XYZ_1964_10DEG")
      {
        curve_specs = mrf::color::CIE_XYZ_1964_10DEG;
      }
      else
      {
        general_loger->warn("XYZ Sensitivity curve OPTION not recognized. switching back to CIE 1931 2-degree.");
      }
    }

    unsigned int nbw;
    if (rgb2spec_options.option("nbw").wasProvided())
    {
      retrieve_nb_wavelength_from_cmdline(rgb2spec_options, nbw);
    }
    else
    {
      nbw = 12;
    }

    if (rgb2spec_options.option("in_color").wasProvided())
    {
      mrf::math::Vec3f          rgb_colors;
      mrf::radiometry::Spectrum result;
      retrieve_color_from_cmdline(rgb2spec_options, rgb_colors);

      // Check if this is a constant Spectrum
      if ((mrf::math::_Math<float>::absVal(rgb_colors(0) - rgb_colors(1)) < COLOR_EPSILON)
          && (mrf::math::_Math<float>::absVal(rgb_colors(1) - rgb_colors(2)) < COLOR_EPSILON))
      {
        std::cout << "[INFO] Input is a constant color. Returning Constant Spectrum " << std::endl;

        result.add(360, rgb_colors(0));
        result.add(830, rgb_colors(0));
      }
      else if (rgb2spec_options.option("reflectivity").wasProvided())
      {
        string illuminant_name = "E";

        // Forcing E illuminant for now
        rgb2spec_options.option("reflectivity").paramValue(0, illuminant_name);

        mrf::radiometry::MRF_ILLUMINANTS illuminant_type;
        try
        {
          illuminant_type = mrf::radiometry::illuminantSpectrum(illuminant_name);
        }
        catch (const std::exception &e)
        {
#ifdef RGB2SPEC_DEBUG
          std::cerr << e.what() << '\n';
#endif

          std::cout
              << "[WARNING] No VALID emittance Spectrum Provided for Reflectance Spectrum. Using the constant illumiant (E)"
              << std::endl;
          illuminant_type = mrf::radiometry::NONE;
        }

        //std::cout << "[WARNING] Currently only E illuminant supported" << std::endl;
        //illuminant_type = mrf::radiometry::NONE;
        convert_rgb_to_brdf_spectrum(rgb_colors, result, illuminant_type, curve_specs);
      }
      else
      {
        std::cout << "[INFO] Solving for Emittance Spectrum" << std::endl;
        try
        {
          convert_rgb_to_ill_spectrum(rgb_colors, result, curve_specs);
        }
        catch (...)
        {
          return EXIT_FAILURE;
        }
      }
      std::cout << "[INFO] Saving SPD to " << output_filename << std::endl;
      result.saveToSPDFile(output_filename);
    }
    else if (rgb2spec_options.option("in_file").wasProvided())
    {
      std::string image_filename;
      retrieve_filename_from_cmdline(rgb2spec_options, image_filename);

      std::unique_ptr<mrf::image::ColorImage> img;

      try
      {
        img = mrf::image::loadColorImage(image_filename);
      }
      catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS &)
      {
        //TODO PROCESS THE IMAGE
        general_loger->fatal("COULD NOT LOAD image: ", image_filename);
        return EXIT_FAILURE;
      }


      // For now we convert from lambda=360 to 831
      //mrf::image::UniformSpectralImage spectral_image(img.width(), img.height(), 360, 471);
      std::cout << " Unsigned int nbw = " << nbw << std::endl;

      std::unique_ptr<mrf::image::UniformSpectralImage> spectral_image_ptr;

      std::string extension = output_filename.substr(output_filename.find_last_of('.') + 1);
      if (extension == mrf::image::ART_EXTENSION)
      {
        spectral_image_ptr = std::unique_ptr<mrf::image::UniformSpectralImage>(
            new mrf::image::ArtRawSpectralImage(img->width(), img->height(), 360, (830 - 360) / (nbw - 1), nbw));
      }
      else if (extension == mrf::image::EXR_EXTENSION)
      {
        spectral_image_ptr = std::unique_ptr<mrf::image::UniformSpectralImage>(
            new mrf::image::EXRSpectralImage(img->width(), img->height(), 360, (830 - 360) / (nbw - 1), nbw));
      }
      else
      {
        spectral_image_ptr = std::unique_ptr<mrf::image::UniformSpectralImage>(
            new mrf::image::EnviSpectralImage(img->width(), img->height(), 360, (830 - 360) / (nbw - 1), nbw));
      }

      export_color_image_to_spectral(*img, *spectral_image_ptr, curve_specs, output_filename);
    }
    else
    {
      general_loger->fatal("NOT YET SUPPORTED DUDE");
      return EXIT_FAILURE;
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }


  general_loger->info(" ... DONE");

  return EXIT_SUCCESS;
}
