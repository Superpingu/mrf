#
#
#Copyright (c) 2015, Romain Pacanowski romain.pacanowski@institutoptique.fr
#Copyright (c) 2021, David Murray david.murray@institutoptique.fr
#
# Try to find Malia Rendering Framework, RGB or spectral.
# The variable MRF_MODE must be set to RGB or SPECTRAL before calling find_package(MRF) to get the correct mode.
# Otherwise, the default is SPECTRAL.
# The following variables are set here:
# - MRF_FOUND            MRF is installed.
# - MRF_INCLUDE_DIRS     set to MRF_CORE_INCLUDE_DIR by default.
# - MRF_LIBRARIES/MRF_LIBRARIES_DEBUG        set to MRF_CORE_LIBRARY by default.
# - MRF_DLL/MRF_DLL_DEBUG              set to MRF_CORE_LIBRARY by default.
#
# The following are also generated for convenience:
# - MRF_INSTALL_DIR      pointing to the root of MRF (where you installed it).
# - MRF_BIN_DIR          pointing to the binary directory containing the different applications of MRF (likely MRF_CORE_INSTALL_DIR/bin).
# - MRF_CORE_INCLUDE_DIR pointing to the include directory of the chosen MRF_MODE (likely MRF_CORE_INSTALL_DIR/include/mrf_MODE).
# - MRF_CORE_LIBRARY     pointing to the library path of the chosen MRF_MODE (likely MRF_CORE_INSTALL_DIR/lib/mrf_core_MODE.lib).
# - MRF_CORE_LIBRARY_DEBUG     pointing to the library path of the chosen MRF_MODE, in debug configuration (likely MRF_CORE_INSTALL_DIR/lib/mrf_core_MODE_d.lib).
#
# To include MRF core into your project, only MRF_CORE_INCLUDE_DIR and MRF_CORE_LIBRARY are necessary.
# Basic usage example:
#   set(MRF_MODE rgb)
#   find_package(MRF REQUIRED)
#   target_include_directories(${PROJECT_NAME} PUBLIC ${MRF_INCLUDE_DIRS})
#   target_link_libraries(${PROJECT_NAME} PUBLIC ${MRF_LIBRARIES})
#
# MRF offers a optix GPU path tracing application, to use, request the OPTIX component.
# In that case, MRF_OPTIX_INCLUDE_DIR and MRF_OPTIX_LIBRARY are also set, and appended to MRF_INCLUDE_DIRS and MRF_LIBRARIES.
# FEATURE NOT YET SUPPORTED with the find_package process.


if(NOT DEFINED MRF_MODE)
  message("MRF_MODE is not defined, defaulting to SPECTRAL")
  set(MRF_MODE spectral)
else()
  message("MRF_MODE is " ${MRF_MODE})
endif()

#Identify MRF_INSTALL_DIR to later locate libs and includes
if(DEFINED ENV{MRF_DIR})
  message("Environment Variable MRF_DIR is defined. Checking location $ENV{MRF_DIR} for MRF install.")
  file(GLOB locateMRF $ENV{MRF_DIR})
  set(MRF_INSTALL_DIR $ENV{MRF_DIR})
else()
  message("Environment Variable MRF_DIR is undefined. Checking default install directory.")
  if(WIN32)
    file(GLOB locateMRF "C:/Program Files/mrf")
    if(locateMRF)
      set(MRF_INSTALL_DIR ${locateMRF} CACHE PATH "Path to MRF installed location.")
      message("MRF installation found at " ${MRF_INSTALL_DIR})
    else()
      file(GLOB locateMRF2 "C:/Program Files (x86)/mrf")
      if(locateMRF2)
        set(MRF_INSTALL_DIR ${locateMRF2} CACHE PATH "Path to MRF installed location.")
        message("MRF installation found at " ${MRF_INSTALL_DIR})
      else()
        message("MRF installation not found in C:/Program Files/mrf or C:/Program Files (x86)/mrf, please set it manually to your install direction or define the MRF_DIR environment variable to your install location.")
        set(MRF_INSTALL_DIR "" CACHE PATH "Path to MRF installed location.")
      endif()
    endif()


  elseif(UNIX AND NOT APPLE)

    #TODO: automatic detection if installed elsewhere than /usr/local ?
    set(MRF_INSTALL_DIR "/usr/local" CACHE PATH "Path to MRF installed location.")

  else()
    set(MRF_INSTALL_DIR "" CACHE PATH "Path to MRF installed location.")
  endif()
endif()

function(MRF_find_lib_release name mrf_lib install_dir)
  if(EXISTS ${install_dir})
    if(WIN32)
      find_library(${name}_LIBRARY ${mrf_lib}.lib
        HINTS "${MRF_INSTALL_DIR}/lib")

      find_file(${name}_DLL NAMES ${mrf_lib}.dll
        PATHS "${MRF_INSTALL_DIR}/bin" "${MRF_INSTALL_DIR}/lib")
    else()
    	find_library(${name}_LIBRARY lib${mrf_lib}.a HINTS "${MRF_INSTALL_DIR}/lib")

      find_file(${name}_DLL NAMES lib${mrf_lib}.so PATHS "${MRF_INSTALL_DIR}/lib")
    endif()
  endif()
endfunction()

function(MRF_find_lib_debug name mrf_lib install_dir)
  if(EXISTS ${install_dir})
    if(WIN32)
      find_library(${name}_LIBRARY_DEBUG ${mrf_lib}_d.lib
        HINTS "${MRF_INSTALL_DIR}/lib")

      find_file(${name}_DLL_DEBUG NAMES ${mrf_lib}_d.dll
        PATHS "${MRF_INSTALL_DIR}/bin" "${MRF_INSTALL_DIR}/lib")
    else()
    	find_library(${name}_LIBRARY_DEBUG lib${mrf_lib}_d.a HINTS "${MRF_INSTALL_DIR}/lib")

      find_file(${name}_DLL_DEBUG NAMES lib${mrf_lib}_d.so PATHS "${MRF_INSTALL_DIR}/lib")
    endif()
  endif()
endfunction()

function(MRF_find_lib name mrf_lib install_dir)
  MRF_find_lib_release(${name} ${mrf_lib} ${install_dir})
  MRF_find_lib_debug(${name} ${mrf_lib} ${install_dir})
endfunction()

MRF_find_lib(MRF_CORE mrf_core_${MRF_MODE} ${MRF_INSTALL_DIR})
MRF_find_lib(MRF_CORE mrf_core_${MRF_MODE} ${MRF_INSTALL_DIR})
MRF_find_lib(MRF_EXTERNAL mrf_externals ${MRF_INSTALL_DIR})

#Now set the other variables
if(EXISTS ${MRF_INSTALL_DIR})

  #Attempt to locate include dir by locating one of MRF header in default install path.
  find_path(MRF_CORE_INCLUDE_DIR "mrf_core_dll.hpp"
    HINTS "${MRF_INSTALL_DIR}/include")
  set(MRF_CORE_INCLUDE_DIR ${MRF_CORE_INCLUDE_DIR} ${MRF_CORE_INCLUDE_DIR}/mrf_${MRF_MODE})

  find_path(MRF_EXTERNAL_INCLUDE_DIR "externals/rgbe/rgbe.h"
      HINTS "${MRF_INSTALL_DIR}/include/mrf_externals")
  if(MRF_EXTERNAL_INCLUDE_DIR)
    set(MRF_EXTERNAL_INCLUDE_DIR ${MRF_EXTERNAL_INCLUDE_DIR} ${MRF_EXTERNAL_INCLUDE_DIR}/externals)
  endif()

endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MRF DEFAULT_MSG MRF_CORE_INCLUDE_DIR MRF_CORE_LIBRARY)

mark_as_advanced(MRF_CORE_INCLUDE_DIR MRF_EXTERNAL_INCLUDE_DIR MRF_CORE_LIBRARY MRF_EXTERNAL_LIBRARY MRF_CORE_DLL MRF_EXTERNAL_DLL)
mark_as_advanced(MRF_CORE_LIBRARY_DEBUG MRF_EXTERNAL_LIBRARY_DEBUG MRF_CORE_DLL_DEBUG MRF_EXTERNAL_DLL_DEBUG)

set(MRF_INCLUDE_DIRS ${MRF_CORE_INCLUDE_DIR} "${MRF_CORE_INCLUDE_DIR}/.." ${MRF_EXTERNAL_INCLUDE_DIR})
set(MRF_LIBRARIES ${MRF_CORE_LIBRARY} ${MRF_EXTERNAL_LIBRARY})
set(MRF_LIBRARIES_DEBUG ${MRF_CORE_LIBRARY_DEBUG} ${MRF_EXTERNAL_LIBRARY_DEBUG})

if(EXISTS ${MRF_CORE_DLL})
  set(MRF_DLL ${MRF_CORE_DLL})
endif()
if(EXISTS ${MRF_EXTERNAL_DLL})
  set(MRF_DLL ${MRF_DLL} ${MRF_EXTERNAL_DLL})
endif()

if(EXISTS ${MRF_CORE_DLL_DEBUG})
  set(MRF_DLL_DEBUG ${MRF_CORE_DLL_DEBUG})
endif()
if(EXISTS ${MRF_EXTERNAL_DLL_DEBUG})
  set(MRF_DLL_DEBUG ${MRF_DLL_DEBUG} ${MRF_EXTERNAL_DLL_DEBUG})
endif()
